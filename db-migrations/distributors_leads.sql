
CREATE TABLE `distributors_leads` (
 `id` int(8) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `email` varchar(255) DEFAULT NULL,
 `state` varchar(255) DEFAULT NULL,
 `city` varchar(255) DEFAULT NULL,
 `fax` varchar(255) DEFAULT NULL,
 `messages` varchar(255) DEFAULT NULL,
 `phone` varchar(255) NOT NULL,
 `comment` text NOT NULL,
 `date` date NOT NULL,
 `timestamp` datetime DEFAULT NULL,
 `req_by` varchar(100) NOT NULL DEFAULT '',
 `status` int(10) NOT NULL,
 `updated_by` int(10) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1