CREATE TABLE IF NOT EXISTS `partner_operator_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_acc_no` varchar(20) NOT NULL,
  `operator_id` int(2) NOT NULL,
  `status` int(2) NOT NULL  COMMENT '0=>Blocked , 1=>Open  ',,
  PRIMARY KEY (`id`)
)

ALTER TABLE `shops`.`partner_operator_status` ADD UNIQUE (
`partner_acc_no` ,
`operator_id`
)

INSERT INTO `shops`.`partner_operator_status` (`id`, `partner_acc_no`, `operator_id`, `status`)
 VALUES         (NULL, 'P001', '1', '1'),
                 (NULL, 'P001', '2', '1'),
                (NULL, 'P001', '3', '1'),
                 (NULL, 'P001', '4', '1'),
                 (NULL, 'P001', '5', '1'),
                 (NULL, 'P001', '6', '1'),
                 (NULL, 'P001', '7', '1'),
                 (NULL, 'P001', '8', '1'),
                 (NULL, 'P001', '9', '1'),
                 (NULL, 'P001', '10', '1'),
                 (NULL, 'P001', '11', '1'),
                 (NULL, 'P001', '12', '1'),
                 (NULL, 'P001', '13', '1'),
                  (NULL, 'P001', '14', '1'),
                   (NULL, 'P001', '15', '1'),
                   (NULL, 'P001', '16', '1');