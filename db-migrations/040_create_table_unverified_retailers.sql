CREATE TABLE `unverified_retailers` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `retailer_id` int(11) NOT NULL,
 `name` varchar(30) DEFAULT NULL,
 `shop_name` varchar(50) DEFAULT NULL,
 `area_id` int(11) NOT NULL,
 `area` varchar(30) DEFAULT NULL,
 `address` text NOT NULL,
 `pin_code` varchar(8) DEFAULT NULL,
 `latitude` double DEFAULT NULL,
 `longitude` double DEFAULT NULL,
 `rental_flag` int(1) NOT NULL DEFAULT '0',
 `shop_type` int(4) DEFAULT NULL,
 `shop_type_value` varchar(50) DEFAULT NULL,
 `location_type` int(4) NOT NULL,
 `documents_submitted` int(1) NOT NULL DEFAULT '0',
 `created` datetime NOT NULL,
 `modified` datetime NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `retailer_id` (`retailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

ALTER TABLE `retailers` ADD `trained` INT( 2 ) NOT NULL DEFAULT '0';

ALTER TABLE `retailers_drop` ADD INDEX ( `retailer_id` ) ;