CREATE TABLE `chat_report` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `conversation_id` bigint(20) NOT NULL,
 `from_jid` varchar(255) NOT NULL,
 `to_jid` varchar(255) NOT NULL,
 `date` date NOT NULL,
 `start_time` bigint(20) NOT NULL,
 `end_time` bigint(20) NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `conversation_id` (`conversation_id`),
 KEY `from` (`from_jid`),
 KEY `to` (`to_jid`),
 KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;