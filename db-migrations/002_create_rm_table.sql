CREATE TABLE `rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` INT( 11 ) NOT NULL,
  `super_dist_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active_flag` int(1) NOT NULL DEFAULT '1',
  `block_flag` int(2) NOT NULL DEFAULT '0',
  `password` char(30) NOT NULL DEFAULT '1234',
  
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `idx_sp_dist` (`super_dist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5