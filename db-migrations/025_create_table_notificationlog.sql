
--
-- Database: `shops`
--

-- --------------------------------------------------------

--
-- Table structure for table `notificationlog`
--

CREATE TABLE IF NOT EXISTS `notificationlog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` int(11) NOT NULL,
  `user_key` varchar(512) NOT NULL,
  `msg` text NOT NULL,
  `notify_type` varchar(20) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `response` text,
  `created` datetime NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mobile` (`mobile`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
