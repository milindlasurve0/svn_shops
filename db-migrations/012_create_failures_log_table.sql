
--
-- Table structure for table `failures_log`
--

CREATE TABLE IF NOT EXISTS `failures_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` varchar(50) NOT NULL,
  `mob_dth_no` VARCHAR( 20 ) NOT NULL,
  `amount` INT NOT NULL,
  `err_code` varchar(50) NULL,
  `description` TEXT NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `retailer_id` (`retailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;