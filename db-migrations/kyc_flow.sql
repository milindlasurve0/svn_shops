ALTER TABLE `retailers` ADD `kyc_score` INT( 3 ) NOT NULL DEFAULT '0',
ADD INDEX ( `kyc_score` ) ;
ALTER TABLE `retailers` ADD `shop_type_value` VARCHAR( 50 ) NULL DEFAULT NULL AFTER `shop_type` ;

CREATE TABLE `retailers_docs` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `retailer_id` int(11) NOT NULL,
 `type` varchar(30) NOT NULL,
 `src` varchar(200) NOT NULL,
 `uploader_user_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `retailer_id` (`retailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

 CREATE TABLE `retailers_kyc_states` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `retailer_id` int(11) NOT NULL,
 `section_id` int(2) NOT NULL COMMENT '1 -> basic_info; 2 -> address; 3 -> other_info;',
 `verified_state` int(2) NOT NULL DEFAULT '0' COMMENT '0 -> unverified; 1 -> verified;',
 `document_state` int(2) NOT NULL DEFAULT '0' COMMENT '0 -> submitted; 1 -> rejected; 2 -> approved;',
 `verified_timestamp` datetime NOT NULL,
 `verified_date` date NOT NULL,
 `document_timestamp` datetime NOT NULL,
 `document_date` date NOT NULL,
 `comment` text NOT NULL,
 PRIMARY KEY (`id`),
 KEY `retailer_id` (`retailer_id`,`verified_date`,`document_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

ALTER TABLE `unverified_retailers` ADD `shopname` VARCHAR( 50 ) NULL DEFAULT NULL AFTER `shop_name` ;
ALTER TABLE `unverified_retailers` ADD `pin` VARCHAR( 8 ) NULL DEFAULT NULL AFTER `pin_code` ;