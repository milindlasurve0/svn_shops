CREATE TABLE IF NOT EXISTS `circle_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(3) NOT NULL,
  `c_code_pay1` varchar(5) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `opr_id` int(2) DEFAULT NULL,
  `prod_code_pay1` int(11) NOT NULL,
  `opr_name` varchar(255) NOT NULL,
  `plan_type` varchar(255) NOT NULL,
  `plan_amt` int(5) NOT NULL,
  `plan_validity` int(11) NOT NULL,
  `plan_desc` varchar(1000) NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `opr_circle_indx` (`prod_code_pay1`,`c_code_pay1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `circle_plans_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(3) NOT NULL,
  `c_code_pay1` varchar(5) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `opr_id` int(2) DEFAULT NULL,
  `prod_code_pay1` int(11) NOT NULL,
  `opr_name` varchar(255) NOT NULL,
  `plan_type` varchar(255) NOT NULL,
  `plan_amt` int(5) NOT NULL,
  `plan_validity` int(11) NOT NULL,
  `plan_desc` varchar(1000) NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `opr_circle_indx` (`prod_code_pay1`,`c_code_pay1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
