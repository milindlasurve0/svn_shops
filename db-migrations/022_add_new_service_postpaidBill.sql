INSERT INTO `shops`.`services` (`id`, `name`, `toShow`) VALUES (NULL, 'Mobile Bill Payment PostPaid', '0');


INSERT INTO `shops`.`products` values ( 36,4,"Docomo Postpaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 37,4,"Loop Mobile PostPaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 38,4,"Cellone PostPaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 39,4,"IDEA Postpaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 40,4,"Tata TeleServices PostPaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 41,4,"Vodafone Postpaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 42,4,"Airtel Postpaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );
INSERT INTO `shops`.`products` values ( 43,4,"Reliance Postpaid", NULL, 1 ,NULL ,NULL ,1 ,1 ,10 ,10000 ,NULL ,NULL ,NULL ,0 ,"" ,NULL ,NULL );

ALTER TABLE `products` ADD `blocked_slabs` TEXT NULL DEFAULT NULL AFTER `down_note`;

UPDATE `shops`.`products` SET `blocked_slabs` = '5,8,9,10',
`created` = '2014-03-13 20:00:00' WHERE `products`.`id` in (36,37,38,39,40,41,42,43);


INSERT INTO `shops`.`vendors_commissions` (`id`, `vendor_id`, `product_id`, `discount_commission`, `active`, `oprDown`, `circles_yes`, `circles_no`, `timestamp`, `updated_by`) VALUES (NULL, '8', '36', '0', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '37', '0', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '38', '0', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '39', '0', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '40', '0', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '41', '0', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '42', '0.5', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL), (NULL, '8', '43', '0.5', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL);


ALTER TABLE `slabs_products` ADD `service_charge` FLOAT( 4, 2 ) NULL DEFAULT NULL AFTER `percent` ,
ADD `service_tax` INT( 1 ) NOT NULL DEFAULT '0' AFTER `service_charge`;


INSERT INTO `shops`.`slabs_products` (`id`, `slab_id`, `product_id`, `percent`, `service_charge`, `service_tax`) VALUES (NULL, '3', '36', '0.00', '5', '1'), (NULL, '3', '37', '0.00', '5', '1'), (NULL, '3', '38', '0.00', '5', '1'), (NULL, '3', '39', '0.00', '5', '1'), (NULL, '3', '40', '0.00', '5', '1'), (NULL, '3', '41', '0.00', '5', '1') , (NULL, '3', '42', '0.00', '5', '1'), (NULL, '3', '43', '0.00', '5', '1'), (NULL, '4', '36', '0.00', '5', '1'), (NULL, '4', '37', '0.00', '5', '1'), (NULL, '4', '38', '0.00', '5', '1'), (NULL, '4', '39', '0.00', '5', '1'), (NULL, '4', '40', '0.00', '5', '1'), (NULL, '4', '41', '0.00', '5', '1') , (NULL, '4', '42', '0.00', '5', '1'), (NULL, '4', '43', '0.00', '5', '1'), (NULL, '6', '36', '0.00', '5', '1'), (NULL, '6', '37', '0.00', '5', '1'), (NULL, '6', '38', '0.00', '5', '1'), (NULL, '6', '39', '0.00', '5', '1'), (NULL, '6', '40', '0.00', '5', '1'), (NULL, '6', '41', '0.00', '5', '1') , (NULL, '6', '42', '0.00', '5', '1'), (NULL, '6', '43', '0.00', '5', '1'), (NULL, '7', '36', '0.00', '5', '1'), (NULL, '7', '37', '0.00', '5', '1'), (NULL, '7', '38', '0.00', '5', '1'), (NULL, '7', '39', '0.00', '5', '1'), (NULL, '7', '40', '0.00', '5', '1'), (NULL, '7', '41', '0.00', '5', '1') , (NULL, '7', '42', '0.00', '5', '1'), (NULL, '7', '43', '0.00', '5', '1'), (NULL, '11', '36', '0.00', '5', '1'), (NULL, '11', '37', '0.00', '5', '1'), (NULL, '11', '38', '0.00', '5', '1'), (NULL, '11', '39', '0.00', '5', '1'), (NULL, '11', '40', '0.00', '5', '1'), (NULL, '11', '41', '0.00', '5', '1') , (NULL, '11', '42', '0.00', '5', '1'), (NULL, '11', '43', '0.00', '5', '1');
                
INSERT INTO `shops`.`vendors_commissions` (`id`, `vendor_id`, `product_id`, `discount_commission`, `active`, `oprDown`, `circles_yes`, `circles_no`, `timestamp`, `updated_by`) VALUES (NULL, '19', '41', '0.6', '1', '0', NULL, NULL, CURRENT_TIMESTAMP, NULL);