ALTER TABLE  `retailers_logs` ADD  `opening_balance` FLOAT( 10, 3 ) NOT NULL AFTER  `earning`
ALTER TABLE  `distributors_logs` ADD  `opening_balance` FLOAT( 10, 3 ) NOT NULL AFTER  `earning`
