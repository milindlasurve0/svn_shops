ALTER TABLE `comments` ADD `tag_id` INT( 11 ) NULL DEFAULT NULL AFTER `mobile` ,
ADD INDEX ( `tag_id` ) ;

ALTER TABLE `comments` ADD `call_type_id` INT( 11 ) NULL DEFAULT NULL AFTER `mobile` ,
ADD INDEX ( `call_type_id` ) ;

ALTER TABLE `complaints` ADD `turnaround_time` VARCHAR( 100 ) NULL DEFAULT NULL AFTER `resolve_time` ;

CREATE TABLE `cc_call_types` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(100) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ;

TRUNCATE `taggings`;
ALTER TABLE `taggings` ADD `type` VARCHAR( 100 ) NULL DEFAULT NULL ;
INSERT INTO `taggings` (name, type) VALUES ('Wrong Number Txn','Customer'), ('Wrong Operator Txn','Customer'), ('Wrong Amount txn','Customer'), ('Wrong Subscriber ID txn','Customer'), ('Wrong Benefit Txn','Customer'), ('Reversed Transaction Queries','Customer'), ('Late Successful recharge','Customer'), ('Successful TXN Queries','Customer'), ('NOCR','Customer'), ('In process txn staus or Compalint','Customer'), ('Pull back transaction complaint','Customer'), ('Fraud case','Customer'), ('Double Transaction ','Customer'), ('Direct Complaint','Customer'), ('There Is No Recharge','Retailer'), ('Sms Drop','Retailer'), ('Apps Problem','Retailer'), ('Miss Call Problem','Retailer'), ('Sms Delivery Problem','Retailer'), ('Apps Info Given','Retailer'), ('Opt Down','Retailer'), ('Other','Retailer'), ('wrongly deducted balance','Retailer'), ('Kit Info','Retailer'), ('Stock balance Problem','Retailer'), ('Bill payment inq','Retailer'), ('Retailer & Distributor lades','Retailer'), ('Utility Bill','Retailer'), ('Mobile Post Paied Bill payment','Retailer'), ('Distributor Number Inq','Retailer'), ('Rim CDMA To Port In GSM Manuel Recharge','Retailer'), ('Repeat call for other queries','Retailer'), ('Reset Password','Retailer'), ('CC Confirmation ','Resolution'), ('Scheme Given ','Resolution'), ('Double txn refund','Resolution'), ('VMN Issue refund','Resolution'), ('General ','Resolution'), ('In Process Clear','Resolution') ;

TRUNCATE `taggings`;
ALTER TABLE `shops`.`taggings` DROP INDEX `name` ,
ADD UNIQUE `idx_name_type` ( `name` , `type` ) ;
INSERT INTO `taggings` (name, type) VALUES ('Wrong Number','Customer'), ('Wrong Amount','Customer'), ('Wrong Operator','Customer'), ('Wrong SUB-ID','Customer'), ('There Is No Recharge','Customer'), ('Successful TXN Queries','Customer'), ('Reversal Queries','Customer'), ('NOCR TXN','Customer'), ('In Process TXN','Customer'), ('Wrong Benefit Txn','Customer'), ('Late Successful recharge','Customer'), ('Pull back transaction complain','Customer'), ('General','Customer'), ('Direct Complaint','Customer'), ('Fraud case','Customer'), ('Operator Down','Customer'), ('Reset Password','Customer'), ('Distributor Number Inq','Customer'), ('Number Change','Customer'), ('Limit Details','Customer'), ('Retailer & Distributor lades','Customer'), ('Call disconnected','Customer'), ('Apps Problem','Customer'), ('CC Confirmation','Resolution'), ('Scheme Given','Resolution'), ('Double txn refund','Resolution'), ('VMN Issue refund','Resolution'), ('General Decline','Resolution'), ('In Process Clear','Resolution'), ('Manual Reversal','Resolution'), ('Paytronics Reversal','Resolution'), ('GI Tech reversal','Resolution'), ('Cp Reversal','Resolution'), ('RKIT Reversal','Resolution'), ('A to Z Reversal','Resolution'), ('Join Recharge Reversal','Resolution'), ('Uni Reversal','Resolution'), ('System Reversal','Resolution'), ('Late success Reversal','Resolution'), ('Pull back Reversal','Resolution'), ('Repeat Call','Resolution'), ('Sms Drop','Retailer'), ('Successful TXN Queries','Retailer'), ('There Is No Recharge','Retailer'), ('other queries','Retailer'), ('Miss Call Problem','Retailer'), ('Sms Delivery Problem','Retailer'), ('Reversal Queries','Retailer'), ('Wrong Number','Retailer'), ('Wrong Amount','Retailer'), ('Wrong Operator','Retailer'), ('Wrong SUB-ID','Retailer'), ('Apps Info Given','Retailer'), ('Bill payment inq','Retailer'), ('Kit Inq','Retailer'), ('Utility Bill','Retailer'), ('Rim CDMA To Port In GSM Manuel Recharge','Retailer'), ('Stock Balance Problem','Retailer') ;

 	CREATE TABLE `comments_count` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `ref_code` varchar(16) NOT NULL,
 `count` int(11) NOT NULL DEFAULT '0',
 `vendor_id` int(11) NOT NULL,
 `product_id` int(11) NOT NULL,
 `retailer_id` int(11) DEFAULT NULL,
 `medium` int(2) NOT NULL DEFAULT '-1',
 `date` date DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `idx_ref_code_date` (`ref_code`,`date`),
 KEY `idx_retailer_id` (`retailer_id`),
 KEY `idx_vendor_id` (`vendor_id`),
 KEY `idx_product_id` (`product_id`),
 KEY `idx_date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1

INSERT INTO `taggings` (name, type) VALUES ('Delhi Retailer Missed Call','Customer'), ('Stock Balance Inquire','Retailer'), ('Number Change','Retailer'), ('Hold Repeat TRX','Retailer'), ('Pay1 TOP','Retailer'), ('Pin Rest','Retailer'), ('Taking follow up', 'Resolution');
INSERT INTO `cc_call_types` (name) VALUES ('Backend');

DELETE FROM `taggings` where name = 'Repeated' and type = 'Resolution';
DELETE FROM `taggings` where name = 'Direct Decline' and type = 'Retailer';
INSERT INTO `taggings` (name, type) VALUES ('Repeated', 'Retailer'), ('Direct Decline', 'Resolution');
DELETE FROM `cc_call_types` where name = "Backend";
INSERT INTO `cc_call_types` (name) VALUES ('Auto Complaints'), ('Incoming Complaints'), ('In Process');

DELETE FROM `taggings` where name = 'Repeat call';
DELETE FROM `taggings` where name = 'Direct Complaint';
DELETE FROM `taggings` where name = 'System Reversal';
INSERT INTO `taggings` (name, type) VALUES ('Total sales','Retailer'), ('Distributors no given','Retailer'), ('call transfer','Retailer'), ('Ringing ','Retailer'), ('Mobile switch off / Not reachable ','Retailer'), ('Repeat call','Customer'), ('Reversal done from operator','Resolution'), ('By mistely reversed so pull back done','Resolution'), ('Transfer done Wrong id to right id','Resolution'), ('Wrongly manual success reversal','Resolution') ;

ALTER TABLE `complaints` CHANGE `turnaround_time` `turnaround_time` DATETIME NULL DEFAULT NULL ;
ALTER TABLE `taggings` ADD `is_active` TINYINT( 2 ) NOT NULL DEFAULT '1' ;
INSERT INTO `taggings` (name, type) VALUES ('Modem Reversal', 'Resolution'), ('Manual Reversal', 'Customer'), ('Reopen', 'Resolution');
INSERT INTO `taggings` (name, type) VALUES ('Customer Not Got Balance','Online Complaint'), ('Wrong Operator Recharge','Online Complaint'), ('Wrong Sub ID Recharge','Online Complaint'), ('Wrong Number Recharge','Online Complaint'), ('Wrong Amount Recharge','Online Complaint'), ('Late Recharge Success','Online Complaint'), ('Wrong Benefit Recharge','Online Complaint'), ('Double Recharge Success','Online Complaint') ;
ALTER TABLE `cc_call_types` ADD `is_active` TINYINT( 2 ) NOT NULL DEFAULT '1' ;
INSERT INTO `cc_call_types` (name, is_active) VALUES ('Android Merchant App', 0) ;

INSERT INTO `cc_call_types` (name) VALUES ('Wrongly reversed modem mails') ;
INSERT INTO `taggings` (name, type) VALUES ('Wrongly Reversed By System So Pull Back Done', 'Resolution'), ('It is a Loss', 'Resolution'), ('It Is Not Our Loss .It Is System Problem Same Transaction Id For Both Transaction', 'Resolution') ;

UPDATE `cc_call_types` SET name = 'Recharge Modems Mails' where name = 'Wrongly reversed modem mails';
UPDATE `taggings` SET is_active = 0 where name = 'Delhi Retailer Missed Call';
INSERT INTO `cc_call_types` (name) VALUES ('Delhi Retailer Missed Call') ;
INSERT INTO `taggings` (name, type) VALUES ('Incentive given', 'Resolution') ;

INSERT INTO `cc_call_types` (name) VALUES ('Modem success after failure'), ('Incentive credited to Retailer') ;
INSERT INTO `taggings` (name, type) VALUES ('Late sms issued so pull back done', 'Resolution'), ('Wrongly reversed by Executive so pull back done', 'Resolution') ;

INSERT INTO `taggings` (name, type) VALUES ('Bank details', 'Retailer'), ('Switch off mobile', 'Retailer'), ('Apps Problem', 'Retailer') ;

INSERT INTO `taggings` (name, type) VALUES ('Escalation Call', 'Retailer'), ('Escalation Call', 'Customer'), ('mypay reversal', 'Resolution'), ('smsdaakreversal', 'Resolution'), ('Wrongly date selected closed', 'Resolution') ;

UPDATE `taggings` set is_active = 0 where name = 'Escalation Call';
INSERT INTO `taggings` (name, type) VALUES ('Opt Dowen', 'Retailer'),  ('Limiet Details', 'Retailer'),  ('schema', 'Retailer'),  ('OPT Code', 'Retailer'),  ('Call disconnected', 'Retailer'),  ('Demo Block', 'Retailer'),  ('commission', 'Retailer'),  ('Online Limet', 'Retailer'),  ('wrong recharge methods', 'Retailer'),  ('Pull Back', 'Retailer'),  ('Recharge format', 'Retailer'),  ('Insufficient Balance', 'Retailer'),  ('Reopen', 'Retailer'),  ('Reopen', 'Customer') ;
INSERT INTO `cc_call_types` (name) VALUES ('Escalation Call') ;

INSERT INTO `taggings` (name, type) VALUES ('rio_2 reversal', 'Resolution'), ('rio reversal', 'Resolution'), ('aporec reversal', 'Resolution') ; 