CREATE TABLE `mpos_transactions` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `shop_transaction_id` int(11) NOT NULL,
 `success` tinyint(1) DEFAULT NULL,
 `error` text,
 `setting` text,
 `apps` text,
 `taskList` text,
 `amount` float(10,3) NOT NULL,
 `amountAdditional` float(10,3) NOT NULL,
 `amountOriginal` float(10,3) NOT NULL,
 `amountCashBack` float(10,3) NOT NULL,
 `authCode` varchar(10) DEFAULT NULL,
 `batchNumber` varchar(10) DEFAULT NULL,
 `cardLastFourDigit` varchar(4) DEFAULT NULL,
 `currencyCode` varchar(10) DEFAULT NULL,
 `customerName` varchar(50) DEFAULT NULL,
 `customerMobile` varchar(10) DEFAULT NULL,
 `customerEmail` varchar(50) DEFAULT NULL,
 `customerReceiptUrl` varchar(200) DEFAULT NULL,
 `deviceSerial` varchar(20) DEFAULT NULL,
 `externalRefNumber` varchar(50) DEFAULT NULL,
 `externalRefNumber2` varchar(50) DEFAULT NULL,
 `externalRefNumber3` varchar(50) DEFAULT NULL,
 `formattedPan` varchar(30) DEFAULT NULL,
 `txnId` varchar(30) NOT NULL,
 `latitude` double DEFAULT NULL,
 `longitude` double DEFAULT NULL,
 `merchantName` varchar(50) DEFAULT NULL,
 `mid` varchar(20) DEFAULT NULL,
 `nonce` varchar(50) DEFAULT NULL,
 `nonceStatus` varchar(20) DEFAULT NULL,
 `orgCode` varchar(30) DEFAULT NULL,
 `merchantCode` varchar(30) DEFAULT NULL,
 `payerName` varchar(50) DEFAULT NULL,
 `paymentCardBin` varchar(20) DEFAULT NULL,
 `paymentCardBrand` varchar(30) DEFAULT NULL,
 `paymentCardType` varchar(10) DEFAULT NULL,
 `paymentMode` varchar(10) DEFAULT NULL,
 `pgInvoiceNumber` varchar(30) DEFAULT NULL,
 `postingDate` bigint(15) DEFAULT NULL,
 `processCode` varchar(30) DEFAULT NULL,
 `rrNumber` varchar(30) DEFAULT NULL,
 `settlementStatus` varchar(30) DEFAULT NULL,
 `status` varchar(30) DEFAULT NULL,
 `states` text,
 `transactionId` varchar(30) NOT NULL,
 `tid` varchar(30) NOT NULL,
 `tidLocation` varchar(50) DEFAULT NULL,
 `txnType` varchar(30) DEFAULT NULL,
 `userAgreement` text,
 `signable` tinyint(1) DEFAULT NULL,
 `voidable` tinyint(1) DEFAULT NULL,
 `refundable` tinyint(1) DEFAULT NULL,
 `chargeSlipDate` varchar(30) DEFAULT NULL,
 `readableChargeSlipDate` varchar(30) DEFAULT NULL,
 `cardTxnTypeDesc` varchar(30) DEFAULT NULL,
 `issuerCode` varchar(30) DEFAULT NULL,
 `dxMode` varchar(30) DEFAULT NULL,
 `receiptUrl` varchar(200) DEFAULT NULL,
 `signReqd` tinyint(1) DEFAULT NULL,
 `txnTypeDesc` varchar(30) DEFAULT NULL,
 `cardTxnType` varchar(10) DEFAULT NULL,
 `ezetap_id` varchar(30) NOT NULL,
 `paymentGateway` varchar(30) DEFAULT NULL,
 `txnRequestId` varchar(30) DEFAULT NULL,
 `acquirerCode` varchar(30) DEFAULT NULL,
 `terminalInfoId` int(11) DEFAULT NULL,
 `referenceTransactionId` varchar(30) DEFAULT NULL,
 `stan` varchar(10) DEFAULT NULL,
 `additionalAmount` float(10,3) NOT NULL,
 `orderNumber` varchar(30) DEFAULT NULL,
 `reverseReferenceNumber` varchar(30) DEFAULT NULL,
 `totalAmount` float(10,3) NOT NULL,
 `displayPAN` varchar(30) DEFAULT NULL,
 `nameOnCard` varchar(50) DEFAULT NULL,
 `invoiceNumber` varchar(30) DEFAULT NULL,
 `cardType` varchar(20) DEFAULT NULL,
 `tipEnabled` tinyint(1) DEFAULT NULL,
 `callTC` tinyint(1) DEFAULT NULL,
 `acquisitionId` varchar(30) DEFAULT NULL,
 `acquisitionKey` varchar(50) DEFAULT NULL,
 `taxPresent` tinyint(1) DEFAULT NULL,
 `created` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ;
