CREATE TABLE `trans_pullback` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendors_activations_id` int (11) NOT NULL,
 `vendor_id` int(11) NOT NULL,
 `status` int(4) NOT NULL DEFAULT '0',
 `timestamp` datetime DEFAULT NULL,
  `pullback_by` int(11) DEFAULT NULL,
  `pullback_time` datetime DEFAULT NULL,
   `reported_by` varchar(50) NOT NULL,
 `date` date DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `idx_date` (`date`),
 KEY `idx_vendor_date` (`vendor_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1
