
--
-- Table structure for table `modem_request_log`
--

CREATE TABLE IF NOT EXISTS `modem_request_log` (
  `req_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique modem request id .',
  `input` text,
  `output` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`req_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
