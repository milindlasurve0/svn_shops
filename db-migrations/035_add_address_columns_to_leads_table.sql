ALTER TABLE `leads` ADD `area` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `city` ,
ADD `pin_code` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `area` ;
ALTER TABLE `leads` ADD `interest` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `req_by` ;
INSERT INTO `shops`.`vars` (
`id` ,
`name` ,
`value` ,
`alter`
)
VALUES (
NULL , 'OTA_Fee', '50', NULL ;