ALTER TABLE `cc_call_logging` ADD `distributor_id` INT( 10 ) NOT NULL DEFAULT '0' AFTER `retailer_id` ,
ADD INDEX ( `distributor_id` ); 

ALTER TABLE `cc_call_logging` ADD `type` VARCHAR( 20 ) NULL DEFAULT NULL AFTER `note`;