CREATE TABLE `distributor_app_log` (
 `id` int(11) NOT NULL,
 `distributor_id` int(11) DEFAULT NULL,
 `method` varchar(30) NOT NULL,
 `params` text NOT NULL,
 `description` text,
 `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `date` date NOT NULL,
 KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
