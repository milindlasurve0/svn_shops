INSERT INTO `shops`.`services` (`id`, `name`, `toShow`) VALUES (5, 'Pay1 Wallet', '1');
INSERT INTO `shops`.`products` (`id`, `service_id`, `name`, `price`, `to_show`, `validity`, `code`, `active`, `oprDown`, `min`, `max`, `invalid`, `circle_yes`, `circle_no`, `monitor`, `down_note`, `blocked_slabs`, `created`, `modified`) VALUES (44, '1', 'Pay1 Wallet', NULL, '1', NULL, NULL, '1', '0', '10', '10000', NULL, NULL, NULL, '1', NULL, NULL, '2014-05-03 20:00:00', '2014-05-03 20:00:00');

UPDATE  `shops`.`products` SET  `service_id` =  '5' WHERE  `products`.`id` =44;

INSERT INTO `shops`.`vars` (`id`, `name`, `value`, `alter`) VALUES (NULL, 'b2c_ip', '192.168.0.38', NULL);

INSERT INTO `shops`.`vendors` (`id`, `company`, `shortForm`, `balance`, `ip`, `port`, `update_flag`, `active_flag`, `health_factor`, `machine_id`, `update_time`, `last30bal`) VALUES (22, 'Pay1 B2C', 'pay1', '0.00', NULL, NULL, '0', '1', '0', NULL, NOW(), '0.00');

INSERT INTO `shops`.`vendors_commissions` (`id`, `vendor_id`, `product_id`, `discount_commission`, `active`, `oprDown`, `circles_yes`, `circles_no`, `timestamp`, `updated_by`) VALUES (NULL, '22', '44', '0.00', '1', '1', NULL, NULL, '2014-05-05 00:00:00', NULL);

UPDATE  `shops`.`products` SET  `blocked_slabs` =  '5,8,9,10' WHERE  `products`.`id` =44;

INSERT INTO `shops`.`slabs_products` (`id`, `slab_id`, `product_id`, `percent`, `service_charge`, `service_tax`) VALUES (NULL, '3', '44', '2', NULL, '0'), (NULL, '4', '44', '2', NULL, '0'),  (NULL, '6', '44', '2', NULL, '0'),  (NULL, '7', '44', '2', NULL, '0'),  (NULL, '11', '44', '2', NULL, '0'),  (NULL, '13', '44', '2', NULL, '0');

