
CREATE TABLE `cash_payment_txn` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `cash_client_id` bigint(20) NOT NULL,
 `mobile` bigint(20) NOT NULL,
 `amount` float(12,2) NOT NULL,
 `intime` datetime NOT NULL,
 `client_ref_id` varchar(60) DEFAULT NULL,
 `status` tinyint(4) NOT NULL DEFAULT '0',
 `updated_time` datetime NOT NULL,
 `expiry_time` datetime NOT NULL,
 `shop_txn_id` bigint(20) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `status` (`status`)
) ENGINE=InnoDB


CREATE TABLE `cash_payment_client` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `company_name` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `salt` varchar(100) NOT NULL,
 `callback_api` varchar(200) NOT NULL,
 `status` tinyint(4) NOT NULL,
 `created_date` datetime NOT NULL,
 `commission` float(3,2) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB


CREATE TABLE `cash_payment_settlement` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `cash_payment_id` bigint(20) NOT NULL,
 `txn_amount` float(12,2) NOT NULL,
 `commission_amount` float(12,2) NOT NULL,
 `settlement_amount` float(12,2) NOT NULL,
 `settled_by` bigint(20) NOT NULL,
 `comment` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB


//------------- add new changes

ALTER TABLE `cash_payment_txn` ADD UNIQUE (
`client_ref_id`
)

CREATE TABLE `cash_payment_vpin` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cash_payment_id` bigint(20) NOT NULL,
 `verification_code` varchar(15) NOT NULL,
 `status` tinyint(4) NOT NULL DEFAULT '0',
 `created_date` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB

//-----------

ALTER TABLE `cash_payment_settlement` ADD `settled_date` DATETIME NOT NULL AFTER `settled_by` 

//---
ALTER TABLE `cash_payment_txn` ADD `in_date` DATE NOT NULL AFTER `intime` 
ALTER TABLE `cash_payment_settlement` ADD `date` DATE NOT NULL AFTER `settled_date` 

ALTER TABLE `cash_payment_settlement` ADD INDEX ( `date` ) 
ALTER TABLE `cash_payment_settlement` ADD INDEX ( `cash_payment_id` ) 
ALTER TABLE `cash_payment_txn` ADD INDEX ( `in_date` ) 
ALTER TABLE `cash_payment_txn` ADD INDEX ( `mobile` ) 
ALTER TABLE `cash_payment_txn` ADD INDEX ( `shop_txn_id`)
ALTER TABLE `cash_payment_txn` ADD INDEX ( `in_date` , `cash_client_id` ) 
ALTER TABLE `shops`.`cash_payment_settlement` DROP INDEX `cash_payment_id` ,
ADD UNIQUE `cash_payment_id` ( `cash_payment_id` )