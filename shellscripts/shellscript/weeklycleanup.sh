#!/bin/bash

cd /var/log
rm -f *-2*

#removing last weeks apache log
log_dir=/mnt/logs
d_arc_date=`date --date='1 days ago' +%Y-%m-%d`
mkdir $d_arc_date
cd /var/log/httpd
rmfilelist=`ls *log`
for logf in $rmfilelist
  do
    arc_file_name="$logf".tar.gz
  	tar -czf $arc_file_name $logf
  	echo "/mnt/logs:: moving " $arc_file_name "to $log_dir/$d_arc_date/"
    mv $arc_file_name $log_dir/$d_arc_date/
	cat /dev/null > $logf
  done
  
#remove debug log
cd /var/www/html/shops/app/tmp/logs
rm -f *.log

#removing limits apps log
cd /var/log/apps
CDATE=`date --date='today' +%Y%m%d`
rmfilelist=`ls *.log`
for logf in $rmfilelist
  do
    FDATE=`basename $logf .log | awk -F'_' '{ n=split($0,array,"_")} END{ print $n}'`
    if [ $FDATE -lt $CDATE ]; then
        echo "removing " $logf
        rm -f $logf
    fi
  done
  
#removing /mnt/logs
ARCTHRESHOLD=45
cd /mnt/logs
rmfilelist=`ls`
for logf in $rmfilelist
  do
    FDATE=`stat -c %y $logf | awk -F' ' '{print $1}' | sed 's/-//g'`
    fileage=$(($(($(date -d $CDATE "+%s") - $(date -d $FDATE "+%s"))) / 86400))
    if [ $fileage -gt $ARCTHRESHOLD ]; then
        echo "removing " $logf
        rm -rf $logf
    fi
  done
  

