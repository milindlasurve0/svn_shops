#!/bin/bas

#2 days old archival
arc_date=`date --date='2 days ago' +%Y-%m-%d`
d_arc_date=`date --date='1 days ago' +%Y-%m-%d`
r_arc_date=`date --date='30 days ago' +%Y-%m-%d`

#get file from tmp
tmp_dir="/tmp/"
temp_file=`ls $tmp_dir | grep "$d_arc_date"`

#get file from inventory
inv_dir="/var/www/html/inventory/application/logs/"
inv_file=`ls $inv_dir | grep "$arc_date"`

#tar files from /mnt/logs
log_dir="/mnt/logs/"
log_file=`ls $log_dir | egrep -v ".tar" | egrep "\.[a-z]"`


#make today's dir
cd $log_dir
mkdir $d_arc_date

#tar logs of /mnt/logs
for log_file_name in $log_file
do
  arc_file_name=`echo $log_file_name".tar.gz" | sed 's/-//g'`
  tar -czf $arc_file_name $log_file_name
  echo "/mnt/logs:: moving " $arc_file_name "to $log_dir/$d_arc_date/"
  mv $arc_file_name $log_dir/$d_arc_date/
  rm -f $log_file_name
done

cd $tmp_dir
#arv and move tmp log files to /mnt/logs
for tmp_file_name in $temp_file
do
  arc_file_name=`echo $tmp_file_name".tar.gz" | sed 's/-//g'`
  tar -czf $arc_file_name $tmp_file_name
  mv $arc_file_name $log_dir/$d_arc_date/
  echo "/tmp:: moving " $arc_file_name "to $log_dir/$d_arc_date/"
  rm -f $tmp_file_name
done

cd $inv_dir
#arv and move inventory log files to /mnt/logs
for inv_file_name in $inv_file
do
  arc_file_name=`echo "inv_"$inv_file_name".tar.gz" | sed 's/-//g'`
  tar -czf $arc_file_name $inv_file_name
  echo "/var/www/html/inventory/application/logs/:: moving " $arc_file_name "to $log_dir/$d_arc_date/"
  mv $arc_file_name $log_dir/$d_arc_date/
  rm -f $inv_file_name
done
