#!/bin/bash

LOAD_THRESHOLD=5
RAM_THRESHOLD=500
DISK_THRESHOLD=500
DISK_THRESHOLD_LOG=1000
EMAILTO="nandan@mindsarray.com,ashish@mindsarray.com,milind@mindsarray.com"
MOBILETO="9819032643,9221770571,7738832731"
MAIL_URL="http://www.smstadka.com/users/sendMsgMails?"
SMS_URL="http://www.smstadka.com/redis/insertInQsms?root=payone&sender=&mobile="$MOBILETO"&"
IP=`wget -qO- http://ipecho.net/plain ; echo`
MAIL_SUBJECT="LIVE SERVER HEALTH ( "$IP" )"


system_load=`uptime | head -1 | awk -F'average' '{ print $2 }' | sed 's/,/:/g' | awk -F': ' '{ print $2 }'`

free_ram_available=`free -m | head -2 | tail -1 | awk -F' ' '{ print $4 }'`

disk_space_mb=`df -mT / | tail -1 | awk -F' ' '{ print $5 }'`

disk_space_mb_log=`df -mT /mnt | tail -1 | awk -F' ' '{ print $5 }'`

currentTime=`date`

echo "******************************************"
echo "current DateTime : "$currentTime
echo "current system status "
echo "LOAD      : "$system_load
echo "RAM  (MB) : "$free_ram_available
echo "DISK (MB) : "$disk_space_mb
echo "DISK_LOG (MB) : "$disk_space_mb_log

echo "******************************************"

				
if [ $(echo "$LOAD_THRESHOLD < $system_load" | bc) -ne 0 ]
then
   echo "Load exceeded to threshhold $system_load"
   MAIL_BODY="Load exceeded  to threshhold $system_load"
   URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
   echo $URL
   wget -O/dev/null "$URL"
   SMS_URL1="$SMS_URL"sms=$MAIL_SUBJECT"::"$MAIL_BODY
   echo $SMS_URL1
   wget -O/dev/null "$SMS_URL1"
fi

if [ $RAM_THRESHOLD -gt $free_ram_available ]
then
   `python /var/www/html/shops/shellscripts/syscmd.py freeram >> /mnt/logs/ramfree.log`
   cur_free_ram_available=`free -m | head -2 | tail -1 | awk -F' ' '{ print $4 }'`
   if [ $RAM_THRESHOLD -gt $cur_free_ram_available ]
   then
       echo "RAM usage crossed to threshhold  $cur_free_ram_available"
       MAIL_BODY="RAM usage crossed to threshhold  $cur_free_ram_available"
       URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
       echo $URL
       wget -O/dev/null "$URL"
       SMS_URL1="$SMS_URL"sms=$MAIL_SUBJECT"::"$MAIL_BODY
       echo $SMS_URL1
       wget -O/dev/null "$SMS_URL1"
    fi   
fi

if [ $DISK_THRESHOLD -gt $disk_space_mb ]
then

   `sh /var/www/html/shops/shellscripts/shellscript/weeklycleanup.sh`
   cur_disk_space_mb=`df -mT / | tail -1 | awk -F' ' '{ print $5 }'`

   if [ $DISK_THRESHOLD -gt $cur_disk_space_mb ]
   then
       echo "disk usage crossed  to threshhold  $cur_disk_space_mb"
       MAIL_BODY="disk usage crossed  to threshhold  $cur_disk_space_mb"
       URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
       echo $URL

       wget -O/dev/null "$URL"
       SMS_URL1="$SMS_URL"sms=$MAIL_SUBJECT"::"$MAIL_BODY
       echo $SMS_URL1
       wget -O/dev/null "$SMS_URL1"
   fi

fi

if [ $DISK_THRESHOLD_LOG -gt $disk_space_mb_log ]
then
    
    `sh /var/www/html/shops/shellscripts/shellscript/weeklycleanup.sh`
    cur_disk_space_mb_log=`df -mT /mnt/logs | tail -1 | awk -F' ' '{ print $5 }'`
    
    if [ $DISK_THRESHOLD_LOG -gt $cur_disk_space_mb_log ]
    then
		echo "disk usage of logs crossed  to threshhold  $cur_disk_space_mb_log"
    	MAIL_BODY="disk usage of logs crossed  to threshhold  $cur_disk_space_mb_log"
    	URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
    	echo $URL

    	wget -O/dev/null "$URL"
    	SMS_URL1="$SMS_URL"sms=$MAIL_SUBJECT"::"$MAIL_BODY
    	echo $SMS_URL1
    	wget -O/dev/null "$SMS_URL1"
	fi
fi