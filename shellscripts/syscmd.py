from subprocess import Popen, PIPE, STDOUT
import subprocess
import sys

def command(cmd):
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    return output

total = len(sys.argv)
if total > 1 :
    if sys.argv[1] == 'freeram':
        print "freeram"
        print command('free -m')
        cmd = 'sync && /sbin/sysctl -w vm.drop_caches=3 && /sbin/sysctl -w vm.drop_caches=0'
        print command(cmd)
        print command('free -m')
    else:
        print sys.argv
