ALTER TABLE `distributors` ADD `benchmark_value` INT( 10 ) NOT NULL AFTER `rm_id` 
ALTER TABLE `distributors` ADD `transacting_retailer` INT( 10 ) NOT NULL AFTER `benchmark_value` 
ALTER TABLE `distributors_logs` ADD `primary_txn` INT( 10 ) NOT NULL AFTER `earning` 
