CREATE TABLE IF NOT EXISTS `complaints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_activation_id` int(11) NOT NULL,
  `takenby` int(11) DEFAULT NULL,
  `closedby` int(11) DEFAULT NULL,
  `in_date` date DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `resolve_date` date DEFAULT NULL,
  `resolve_time` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  ;

ALTER TABLE  `shops`.`complaints` ADD INDEX  `idx_in_date` (  `in_date` );
ALTER TABLE  `shops`.`complaints` ADD INDEX  `idx_resolve_date` (  `resolve_date` );




SELECT retailers.* FROM retailers WHERE parent_id = 4 AND id not in (SELECT distinct retailers.id FROM `retailers_logs`,retailers WHERE retailers.id = `retailers_logs`.retailer_id AND retailers.parent_id = 4 AND retailers_logs.date >= '2012-10-11');