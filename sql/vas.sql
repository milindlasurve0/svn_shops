CREATE TABLE IF NOT EXISTS `products_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(4) DEFAULT NULL,
  `validity` varchar(20) DEFAULT NULL,
  `shortDesc` text,
  `longDesc` text,
  `image` text,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;


INSERT INTO `products_info` (`id`, `product_id`, `product_code`, `name`, `price`, `validity`, `shortDesc`, `longDesc`, `image`, `params`) VALUES
(1, 22, 'AjA=', 'Dil Vil Pyar Vyar', 20, '30 days', 'Get daily 3 Romantic Shayaries Messages for a month.', 'Love is life! Get drenched in the rain of love with our special love messages.\r\n1) Message count: 3 Shayaries Wishes per day\r\n2) Validity: 30 days\r\n3) To unsubscribe, sms ''UNSUB LOVE'' to 09223178889.', 'http://www.smstadka.com/img/retailProducts/love.jpg', '{"allParams":{"param":[{"field":"Mobile","type":"VARCHAR","length":"10"}]}}'),
(2, 23, 'V28=', 'Naughty Jokes', 20, '30 days', 'Get daily 3 Adult Jokes Hindi Jokes for a month.', 'People like it dirty! Get hilarious, double meaning jokes and laugh uncontrollably.\r\n1) Message count: 3 Adult Jokes per day.\r\n2) Validity: 30 days.\r\n3) To unsubscribe, sms ''UNSUB FUN18'' to 09223178889.', 'http://www.smstadka.com/img/retailProducts/fun18.jpg', '{"allParams":{"param":[{"field":"Mobile","type":"VARCHAR","length":"10"}]}}'),
(3, 24, 'AjNTYA==', 'PNR Alert', 10, '', 'Get Regular Automated PNR status updates, train delay alerts on your mobile.', 'Holding a wait-listed ticket? Know you PNR status as and when updated. Get train delay alerts.\r\n1) Message count: Automatic periodic alerts on PNR status change till the date of journey, train delay alerts.\r\n2) Validity: Till the date of journey Cancellation.\r\n3) Instant status check: sms ''PNR PNR NUMBER'' to 09223178889.', 'http://www.smstadka.com/img/retailProducts/pnr.jpg', '{"allParams":{"param":[{"field":"Mobile","type":"VARCHAR","length":"10"},{"field":"PNR","type":"VARCHAR","length":"10"}]}}'),
(4, 25, 'U2BRZg==', 'Instant Cricket', 50, '60 days', 'Give a miss call to 02261512211 to get instant score', 'Simply cricket! A product for all cricket lovers. Get live score for all the matches\r\n1) Message count: Unlimited Live Score on match day.\r\n2) Validity: 60 days.\r\n3) To unsubscribe, sms ''UNSUB MCRI'' to 09223178889.', 'http://www.smstadka.com/img/retailProducts/mcri.jpg', '{"allParams":{"param":[{"field":"Mobile","type":"VARCHAR","length":"10"}]}}');


INSERT INTO `products` (`id`, `service_id`, `name`, `price`, `validity`, `code`, `active`, `created`, `modified`) VALUES
(22, 3, 'Dil Vil', '20', NULL, NULL, 1, NULL, NULL),
(23, 3, 'Naughty', '20', NULL, NULL, 1, NULL, NULL),
(24, 3, 'PNR', '10', NULL, NULL, 1, NULL, NULL),
(25, 3, 'Cricket', '50', NULL, NULL, 1, NULL, NULL);


INSERT INTO `vendors_commissions` (`id`, `vendor_id`, `product_id`, `discount_commission`, `active`, `timestamp`) VALUES
(null, 3, 22, 0.00, 1, '2012-03-16 14:45:41'),
(null, 3, 23, 0.00, 1, '2012-03-16 14:45:41'),
(null, 3, 24, 0.00, 1, '2012-03-16 14:45:50'),
(null, 3, 25, 0.00, 1, '2012-03-16 14:45:50');