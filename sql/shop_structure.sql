-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: pay1.coyipz0wacld.us-east-1.rds.amazonaws.com
-- Generation Time: Nov 17, 2014 at 10:06 AM
-- Server version: 5.5.40
-- PHP Version: 5.3.10-1ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `shops`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `removeUSSDLogs`$$
CREATE DEFINER=`root`@`%` PROCEDURE `removeUSSDLogs`()
BEGIN
DECLARE my_time TIMESTAMP;
SET my_time = DATE_SUB(NOW() , INTERVAL 120 SECOND);
INSERT INTO ussds (SELECT * FROM ussd_logs WHERE timestamp < my_time);
DELETE FROM ussd_logs WHERE timestamp < my_time;
END$$

DROP PROCEDURE IF EXISTS `wall`$$
CREATE DEFINER=`root`@`%` PROCEDURE `wall`()
BEGIN
DELETE FROM requests WHERE timestamp < DATE_SUB(NOW() , INTERVAL 300 SECOND);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`),
  KEY `idx_acos_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=802 ;

-- --------------------------------------------------------

--
-- Table structure for table `apiusers`
--

DROP TABLE IF EXISTS `apiusers`;
CREATE TABLE IF NOT EXISTS `apiusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enabled` int(2) NOT NULL COMMENT '1=enabled,0=>disabled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `app_req_log`
--

DROP TABLE IF EXISTS `app_req_log`;
CREATE TABLE IF NOT EXISTS `app_req_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ret_id` int(11) DEFAULT NULL,
  `method` varchar(30) NOT NULL,
  `params` text NOT NULL,
  `description` text,
  `timesatmp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ret_date` (`ret_id`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98464652 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`),
  KEY `idx_aros_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5092 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_aros_acos_aro_id_aco_id` (`aro_id`,`aco_id`),
  KEY `aco_id` (`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cc_call_logging`
--

DROP TABLE IF EXISTS `cc_call_logging`;
CREATE TABLE IF NOT EXISTS `cc_call_logging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(12) NOT NULL,
  `retailer_id` int(10) DEFAULT NULL,
  `distributor_id` int(10) NOT NULL DEFAULT '0',
  `time` time NOT NULL,
  `date` date NOT NULL,
  `expected_pick_time` varchar(10) NOT NULL,
  `cc_id` int(4) DEFAULT NULL,
  `call_start` datetime DEFAULT NULL,
  `call_end` datetime DEFAULT NULL,
  `call_status` int(3) DEFAULT NULL COMMENT '0->in process, 1->picked, 2->not picked, 3->dropped',
  `callback_flag` int(3) DEFAULT NULL COMMENT '1->callback, 2->done, 3->cancelled',
  `callback_time` datetime DEFAULT NULL,
  `note` text,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_date` (`date`),
  KEY `idx_number` (`number`(4)),
  KEY `distributor_id` (`distributor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=247218 ;

-- --------------------------------------------------------

--
-- Table structure for table `cc_login`
--

DROP TABLE IF EXISTS `cc_login`;
CREATE TABLE IF NOT EXISTS `cc_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `state` int(3) NOT NULL DEFAULT '0' COMMENT '0->not ready, 1->ready, 2->busy, 3->idle',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_userid` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Table structure for table `cc_logs`
--

DROP TABLE IF EXISTS `cc_logs`;
CREATE TABLE IF NOT EXISTS `cc_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_misscall_id` int(11) NOT NULL,
  `cc_id` int(4) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '0->propercall, 1-> not picked, 2-> callbackdone, 3-> callbackcancel',
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `cc_misscalls`
--

DROP TABLE IF EXISTS `cc_misscalls`;
CREATE TABLE IF NOT EXISTS `cc_misscalls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(10) NOT NULL,
  `sms_sent` text,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=362637 ;

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

DROP TABLE IF EXISTS `circles`;
CREATE TABLE IF NOT EXISTS `circles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(20) NOT NULL,
  `circle` varchar(80) NOT NULL,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `circle_plans`
--

DROP TABLE IF EXISTS `circle_plans`;
CREATE TABLE IF NOT EXISTS `circle_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(3) NOT NULL,
  `c_code_pay1` varchar(5) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `opr_id` int(2) DEFAULT NULL,
  `prod_code_pay1` int(11) NOT NULL,
  `opr_name` varchar(255) NOT NULL,
  `plan_type` varchar(255) NOT NULL,
  `plan_amt` int(5) NOT NULL,
  `plan_validity` varchar(255) NOT NULL,
  `plan_desc` varchar(1000) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `opr_circle_indx` (`prod_code_pay1`,`c_code_pay1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14929 ;

-- --------------------------------------------------------

--
-- Table structure for table `circle_plans_backup`
--

DROP TABLE IF EXISTS `circle_plans_backup`;
CREATE TABLE IF NOT EXISTS `circle_plans_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(3) NOT NULL,
  `c_code_pay1` varchar(5) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `opr_id` int(2) DEFAULT NULL,
  `prod_code_pay1` int(11) NOT NULL,
  `opr_name` varchar(255) NOT NULL,
  `plan_type` varchar(255) NOT NULL,
  `plan_amt` int(5) NOT NULL,
  `plan_validity` int(11) NOT NULL,
  `plan_desc` varchar(1000) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `opr_circle_indx` (`prod_code_pay1`,`c_code_pay1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14695 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `retailers_id` int(11) DEFAULT NULL,
  `ref_code` varchar(16) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `comments` text NOT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_userid` (`users_id`),
  KEY `idx_retailer` (`retailers_id`),
  KEY `idx_ref` (`ref_code`),
  KEY `idx_mobile` (`mobile`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=970469 ;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

DROP TABLE IF EXISTS `complaints`;
CREATE TABLE IF NOT EXISTS `complaints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_activation_id` int(11) NOT NULL,
  `takenby` int(11) DEFAULT NULL,
  `closedby` int(11) DEFAULT NULL,
  `in_date` date DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `resolve_date` date DEFAULT NULL,
  `resolve_time` time DEFAULT NULL,
  `resolve_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_in_date` (`in_date`),
  KEY `idx_resolve_date` (`resolve_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1030974 ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_vfirst`
--

DROP TABLE IF EXISTS `delivery_vfirst`;
CREATE TABLE IF NOT EXISTS `delivery_vfirst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `status_flag` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `distributors`
--

DROP TABLE IF EXISTS `distributors`;
CREATE TABLE IF NOT EXISTS `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance` float(10,3) DEFAULT NULL,
  `opening_balance` float(10,3) NOT NULL,
  `slab_id` int(4) NOT NULL,
  `margin` float(5,2) NOT NULL DEFAULT '0.00',
  `margin_approved` int(1) NOT NULL DEFAULT '0',
  `pan_number` varchar(12) DEFAULT NULL,
  `parent_id` int(4) DEFAULT NULL,
  `email` char(40) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `area_range` text,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `company` text,
  `address` text,
  `tds_flag` int(1) NOT NULL DEFAULT '1',
  `level` int(2) NOT NULL DEFAULT '1',
  `kits` int(10) NOT NULL DEFAULT '0',
  `retailer_limit` int(5) NOT NULL DEFAULT '200',
  `salesman_limit` int(3) NOT NULL DEFAULT '5',
  `commission_kits_flag` int(1) NOT NULL DEFAULT '0',
  `discount_kit` int(1) NOT NULL DEFAULT '0',
  `discounted_money` int(1) NOT NULL DEFAULT '0',
  `retailer_creation` int(1) NOT NULL DEFAULT '1',
  `rental_amount` int(6) NOT NULL DEFAULT '50',
  `target_amount` int(6) NOT NULL DEFAULT '25000',
  `sd_amt` int(11) DEFAULT NULL,
  `sd_date` date DEFAULT NULL,
  `sd_withdraw_date` date DEFAULT NULL,
  `rm_id` int(11) NOT NULL DEFAULT '0',
  `toshow` int(1) NOT NULL DEFAULT '1',
  `active_flag` int(2) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_parent` (`parent_id`),
  KEY `idx_rm` (`rm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=430 ;

-- --------------------------------------------------------

--
-- Table structure for table `distributors_kits`
--

DROP TABLE IF EXISTS `distributors_kits`;
CREATE TABLE IF NOT EXISTS `distributors_kits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `kits` int(5) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `note` text,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=445 ;

-- --------------------------------------------------------

--
-- Table structure for table `distributors_logs`
--

DROP TABLE IF EXISTS `distributors_logs`;
CREATE TABLE IF NOT EXISTS `distributors_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(5) NOT NULL,
  `retailers` int(5) NOT NULL DEFAULT '0',
  `transacting` int(5) NOT NULL DEFAULT '0',
  `topup_sold` int(10) NOT NULL DEFAULT '0',
  `topup_buy` int(10) NOT NULL DEFAULT '0',
  `topup_unique` int(5) NOT NULL DEFAULT '0',
  `earning` float(10,2) DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dist_date` (`distributor_id`,`date`),
  KEY `idx_dist` (`distributor_id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=140094 ;

-- --------------------------------------------------------

--
-- Table structure for table `earnings_logs`
--

DROP TABLE IF EXISTS `earnings_logs`;
CREATE TABLE IF NOT EXISTS `earnings_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(5) NOT NULL,
  `opening` float(10,2) DEFAULT NULL,
  `closing` float(10,2) DEFAULT NULL,
  `sale` float(10,2) DEFAULT NULL,
  `invested` float(10,2) DEFAULT '0.00',
  `expected_earning` float(10,2) DEFAULT NULL,
  `old_reversal` int(10) DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_vend_date` (`vendor_id`,`date`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9979 ;

-- --------------------------------------------------------

--
-- Table structure for table `float_logs`
--

DROP TABLE IF EXISTS `float_logs`;
CREATE TABLE IF NOT EXISTS `float_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `float` float(10,2) NOT NULL,
  `transferred` float(10,2) NOT NULL,
  `inventory` float(10,2) NOT NULL,
  `sale` float(10,2) NOT NULL,
  `commissions` int(10) DEFAULT NULL,
  `old_reversals` int(10) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `hour` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16263 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `invoice_type` int(11) DEFAULT NULL,
  `invoice_number` varchar(40) DEFAULT NULL,
  `amount` float(10,2) NOT NULL DEFAULT '0.00',
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices_transactions`
--

DROP TABLE IF EXISTS `invoices_transactions`;
CREATE TABLE IF NOT EXISTS `invoices_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `shoptransaction_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `messages` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `req_by` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9098 ;

-- --------------------------------------------------------

--
-- Table structure for table `locator_area`
--

DROP TABLE IF EXISTS `locator_area`;
CREATE TABLE IF NOT EXISTS `locator_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `toShow` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `area` (`city_id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=512 ;

-- --------------------------------------------------------

--
-- Table structure for table `locator_city`
--

DROP TABLE IF EXISTS `locator_city`;
CREATE TABLE IF NOT EXISTS `locator_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `toShow` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `city` (`name`,`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

-- --------------------------------------------------------

--
-- Table structure for table `locator_state`
--

DROP TABLE IF EXISTS `locator_state`;
CREATE TABLE IF NOT EXISTS `locator_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `toShow` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_numbering`
--

DROP TABLE IF EXISTS `mobile_numbering`;
CREATE TABLE IF NOT EXISTS `mobile_numbering` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `number` varchar(4) DEFAULT NULL,
  `operator` varchar(2) DEFAULT NULL,
  `area` varchar(2) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_number` (`number`),
  KEY `number` (`number`(2))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2538 ;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_numbering_area`
--

DROP TABLE IF EXISTS `mobile_numbering_area`;
CREATE TABLE IF NOT EXISTS `mobile_numbering_area` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `area_code` text,
  `area_name` text,
  `latitude` float(4,2) DEFAULT NULL,
  `longitude` float(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udx_area_code` (`area_code`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_numbering_service`
--

DROP TABLE IF EXISTS `mobile_numbering_service`;
CREATE TABLE IF NOT EXISTS `mobile_numbering_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `opr_code` text,
  `opr_name` text,
  `product_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udx_opr_code` (`opr_code`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `modem_request_log`
--

DROP TABLE IF EXISTS `modem_request_log`;
CREATE TABLE IF NOT EXISTS `modem_request_log` (
  `req_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique modem request id .',
  `vendor_id` int(5) DEFAULT NULL,
  `input` text,
  `output` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`req_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6528 ;

-- --------------------------------------------------------

--
-- Table structure for table `msg_templates`
--

DROP TABLE IF EXISTS `msg_templates`;
CREATE TABLE IF NOT EXISTS `msg_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `notificationlog`
--

DROP TABLE IF EXISTS `notificationlog`;
CREATE TABLE IF NOT EXISTS `notificationlog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `msg_id` varchar(50) DEFAULT NULL,
  `mobile` varchar(12) NOT NULL,
  `user_key` varchar(512) NOT NULL,
  `msg` text NOT NULL,
  `notify_type` varchar(20) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `response` text,
  `received` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mobile` (`mobile`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1108276 ;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(10) NOT NULL,
  `offer` varchar(50) NOT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `date` date NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ret_offer` (`mobile`,`offer`(20))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1574 ;

-- --------------------------------------------------------

--
-- Table structure for table `opening_closing`
--

DROP TABLE IF EXISTS `opening_closing`;
CREATE TABLE IF NOT EXISTS `opening_closing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(6) NOT NULL,
  `group_id` int(3) NOT NULL,
  `shop_transaction_id` int(11) NOT NULL,
  `opening` float(12,2) NOT NULL,
  `closing` float(12,2) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_shop_transid` (`shop_id`,`group_id`,`shop_transaction_id`),
  KEY `idx_shop_id` (`shop_transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59055642 ;

-- --------------------------------------------------------

--
-- Table structure for table `oss_rec_codes`
--

DROP TABLE IF EXISTS `oss_rec_codes`;
CREATE TABLE IF NOT EXISTS `oss_rec_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opr_code` varchar(10) DEFAULT NULL,
  `circle_code` varchar(10) DEFAULT NULL,
  `denomination` varchar(15) DEFAULT NULL,
  `rec_code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `opr_code` (`opr_code`,`circle_code`,`denomination`,`rec_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=347 ;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` text,
  `acc_id` varchar(20) NOT NULL,
  `retailer_id` int(11) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ip_addrs` text NOT NULL,
  `status_update_url` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `acc_id` (`acc_id`,`retailer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `partners_log`
--

DROP TABLE IF EXISTS `partners_log`;
CREATE TABLE IF NOT EXISTS `partners_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_req_id` varchar(50) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `vendor_actv_id` varchar(20) DEFAULT NULL,
  `mob_dth_no` varchar(20) NOT NULL,
  `amount` int(11) NOT NULL,
  `err_code` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `date` date DEFAULT NULL,
  `product_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_id` (`partner_id`,`partner_req_id`),
  KEY `vendor_actv_id` (`vendor_actv_id`),
  KEY `mob_dth_no` (`mob_dth_no`),
  KEY `partner_date` (`partner_id`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=646582 ;

-- --------------------------------------------------------

--
-- Table structure for table `partner_operator_status`
--

DROP TABLE IF EXISTS `partner_operator_status`;
CREATE TABLE IF NOT EXISTS `partner_operator_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_acc_no` varchar(20) NOT NULL,
  `operator_id` int(2) NOT NULL,
  `primary_vendor` int(4) DEFAULT NULL,
  `status` int(2) NOT NULL COMMENT '0=>Blocked , 1=>Open  ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_acc_no` (`partner_acc_no`,`operator_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=184 ;

-- --------------------------------------------------------

--
-- Table structure for table `payt_recon`
--

DROP TABLE IF EXISTS `payt_recon`;
CREATE TABLE IF NOT EXISTS `payt_recon` (
  `time` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  `operator` varchar(20) NOT NULL,
  `rechtype` varchar(10) NOT NULL,
  `amount` int(11) NOT NULL,
  `subscriber` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `txnid` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `margin` varchar(10) NOT NULL,
  `curbalance` varchar(10) NOT NULL,
  `response` varchar(255) NOT NULL,
  KEY `idx_orderid` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pg_checks`
--

DROP TABLE IF EXISTS `pg_checks`;
CREATE TABLE IF NOT EXISTS `pg_checks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `min_amount` int(5) NOT NULL,
  `max_amount` int(5) NOT NULL,
  `service_charge` int(3) NOT NULL,
  `active_flag` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `pg_payuIndia`
--

DROP TABLE IF EXISTS `pg_payuIndia`;
CREATE TABLE IF NOT EXISTS `pg_payuIndia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mihpayid` bigint(20) DEFAULT NULL,
  `mode` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `unmappedstatus` varchar(100) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `shop_transaction_id` bigint(20) DEFAULT NULL,
  `server_ip` varchar(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `net_amount_debit` double DEFAULT NULL,
  `addedon` datetime NOT NULL,
  `productinfo` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `udf1` varchar(100) DEFAULT NULL,
  `udf2` varchar(100) DEFAULT NULL,
  `udf3` varchar(100) DEFAULT NULL,
  `udf4` varchar(100) DEFAULT NULL,
  `udf5` varchar(100) DEFAULT NULL,
  `udf6` varchar(100) DEFAULT NULL,
  `udf7` varchar(100) DEFAULT NULL,
  `udf8` varchar(100) DEFAULT NULL,
  `udf9` varchar(100) DEFAULT NULL,
  `udf10` varchar(100) DEFAULT NULL,
  `hash` varchar(500) DEFAULT NULL,
  `field1` varchar(100) DEFAULT NULL,
  `field2` varchar(100) DEFAULT NULL,
  `field3` varchar(100) DEFAULT NULL,
  `field4` varchar(100) DEFAULT NULL,
  `field5` varchar(100) DEFAULT NULL,
  `field6` varchar(100) DEFAULT NULL,
  `field7` varchar(100) DEFAULT NULL,
  `field8` varchar(100) DEFAULT NULL,
  `field9` varchar(100) DEFAULT NULL,
  `payment_source` varchar(100) DEFAULT NULL,
  `PG_TYPE` varchar(100) DEFAULT NULL,
  `bank_ref_num` varchar(100) DEFAULT NULL,
  `bankcode` varchar(100) DEFAULT NULL,
  `error` varchar(100) DEFAULT NULL,
  `error_Message` varchar(100) DEFAULT NULL,
  `cardToken` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(100) DEFAULT NULL,
  `cardnum` varchar(100) DEFAULT NULL,
  `cardhash` varchar(500) DEFAULT NULL,
  `Merchant_UTR` varchar(500) DEFAULT NULL,
  `Settled_At` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_transaction_id` (`shop_transaction_id`),
  UNIQUE KEY `mihpayid` (`mihpayid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=292 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(4) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  `price` varchar(5) DEFAULT NULL,
  `to_show` int(11) NOT NULL DEFAULT '1',
  `validity` varchar(20) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `oprDown` int(2) NOT NULL DEFAULT '0',
  `min` int(6) NOT NULL DEFAULT '10',
  `max` int(8) NOT NULL DEFAULT '10000',
  `invalid` varchar(20) DEFAULT NULL,
  `circle_yes` text,
  `circle_no` text,
  `monitor` int(1) NOT NULL DEFAULT '1',
  `down_note` text,
  `blocked_slabs` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- Table structure for table `products_info`
--

DROP TABLE IF EXISTS `products_info`;
CREATE TABLE IF NOT EXISTS `products_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `price` varchar(20) DEFAULT NULL,
  `validity` varchar(20) DEFAULT NULL,
  `shortDesc` text,
  `longDesc` text,
  `image` text,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE IF NOT EXISTS `promotions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(12) NOT NULL,
  `msg` text NOT NULL,
  `type` varchar(20) NOT NULL,
  `interested` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mobile_date` (`mobile`,`date`),
  KEY `mobile` (`mobile`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6976 ;

-- --------------------------------------------------------

--
-- Table structure for table `pullbacks`
--

DROP TABLE IF EXISTS `pullbacks`;
CREATE TABLE IF NOT EXISTS `pullbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) NOT NULL,
  `retailer_id` int(10) NOT NULL,
  `distributor_id` int(6) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `topup_time` datetime NOT NULL,
  `pullback_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10323 ;

-- --------------------------------------------------------

--
-- Table structure for table `refunds`
--

DROP TABLE IF EXISTS `refunds`;
CREATE TABLE IF NOT EXISTS `refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(5) NOT NULL,
  `shoptrans_id` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `type` int(3) NOT NULL COMMENT '1->monthly scheme, 2-> refund, 3->incentive',
  `note` text,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12843 ;

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

DROP TABLE IF EXISTS `rentals`;
CREATE TABLE IF NOT EXISTS `rentals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) NOT NULL,
  `shoptrans_id` int(11) DEFAULT NULL,
  `rental_amount` int(6) NOT NULL,
  `target_amount` int(6) NOT NULL,
  `rental` int(6) NOT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_date` (`date`),
  KEY `idx_retailer` (`retailer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40499 ;

-- --------------------------------------------------------

--
-- Table structure for table `repeated_transactions`
--

DROP TABLE IF EXISTS `repeated_transactions`;
CREATE TABLE IF NOT EXISTS `repeated_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(10) NOT NULL,
  `msg` varchar(40) NOT NULL,
  `send_flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-> on hold, 1-> process request, 2-> sent, 3-> rejected',
  `type` int(2) NOT NULL COMMENT '1-> burst, 2-> after 5 mins, 3-> Added manually by customer care',
  `added_by` int(11) NOT NULL DEFAULT '0' COMMENT '0 -> means added by system, otherwise it will user_id who added this request',
  `processed_by` int(11) NOT NULL DEFAULT '0' COMMENT '0-> if processed by retailer only, otherwise user_id of the customer care who has processed it',
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=217051 ;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operator` int(5) NOT NULL,
  `method` varchar(40) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '1' COMMENT '0-> via sms, 1-> via app, 2->via ussd',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`mobile`,`amount`,`operator`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2910256 ;

-- --------------------------------------------------------

--
-- Table structure for table `requests_dropped`
--

DROP TABLE IF EXISTS `requests_dropped`;
CREATE TABLE IF NOT EXISTS `requests_dropped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(6) NOT NULL,
  `method` varchar(40) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `amount` float(6,2) NOT NULL,
  `operator` int(4) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=558085 ;

-- --------------------------------------------------------

--
-- Table structure for table `retailers`
--

DROP TABLE IF EXISTS `retailers`;
CREATE TABLE IF NOT EXISTS `retailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `balance` float(10,3) NOT NULL DEFAULT '0.000',
  `opening_balance` float(10,3) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `signature_flag` int(1) DEFAULT '0',
  `signature` text,
  `slab_id` int(5) NOT NULL,
  `pan_number` varchar(12) DEFAULT NULL,
  `mobile` varchar(10) NOT NULL,
  `alternate_number` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `shopname` varchar(50) DEFAULT NULL,
  `area_id` int(11) NOT NULL,
  `area` varchar(30) DEFAULT NULL,
  `address` text NOT NULL,
  `pin` varchar(8) DEFAULT NULL,
  `kyc_flag` int(1) NOT NULL DEFAULT '0',
  `salesman` int(5) NOT NULL,
  `maint_salesman` int(5) DEFAULT NULL,
  `kyc` text,
  `mobile_info` text,
  `app_type` varchar(255) NOT NULL,
  `toshow` int(1) NOT NULL DEFAULT '1',
  `block_flag` int(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subarea_id` int(10) DEFAULT NULL,
  `retailer_type` int(2) DEFAULT '0',
  `rental_flag` int(1) NOT NULL DEFAULT '0',
  `shop_type` int(4) DEFAULT NULL,
  `location_type` int(4) DEFAULT NULL,
  `shop_structure` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  UNIQUE KEY `indx_user_id` (`user_id`),
  KEY `idx_pin` (`pin`),
  KEY `idx_parent` (`parent_id`),
  KEY `idx_salesman` (`maint_salesman`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24408 ;

-- --------------------------------------------------------

--
-- Table structure for table `retailers_logs`
--

DROP TABLE IF EXISTS `retailers_logs`;
CREATE TABLE IF NOT EXISTS `retailers_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) NOT NULL,
  `sale` int(8) NOT NULL DEFAULT '0',
  `app_sale` int(8) DEFAULT NULL,
  `ussd_sale` int(10) NOT NULL DEFAULT '0',
  `android_sale` int(11) DEFAULT '0',
  `java_sale` int(11) DEFAULT '0',
  `windows7_sale` int(11) DEFAULT '0',
  `windows8_sale` int(11) DEFAULT '0',
  `web_sale` int(11) DEFAULT '0',
  `sms_sale` int(10) NOT NULL DEFAULT '0',
  `earning` float(6,2) DEFAULT NULL,
  `transactions` int(4) NOT NULL DEFAULT '0',
  `topup` int(10) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_ret_date` (`retailer_id`,`date`),
  KEY `idx_retailer` (`retailer_id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2732610 ;

-- --------------------------------------------------------

--
-- Table structure for table `rm`
--

DROP TABLE IF EXISTS `rm`;
CREATE TABLE IF NOT EXISTS `rm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `super_dist_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active_flag` int(1) NOT NULL DEFAULT '1',
  `block_flag` int(2) NOT NULL DEFAULT '0',
  `password` char(30) NOT NULL DEFAULT '1234',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `idx_sp_dist` (`super_dist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_collections`
--

DROP TABLE IF EXISTS `salesman_collections`;
CREATE TABLE IF NOT EXISTS `salesman_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesman` int(11) NOT NULL,
  `distributor_id` int(6) NOT NULL DEFAULT '0',
  `payment_type` int(4) NOT NULL COMMENT 'SETUP->1,TOPUP->2,Cash->3,Cheque->4',
  `details` text,
  `date` date NOT NULL,
  `created` datetime NOT NULL,
  `collection_amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_date_type` (`salesman`,`payment_type`,`date`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59660 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_transactions`
--

DROP TABLE IF EXISTS `salesman_transactions`;
CREATE TABLE IF NOT EXISTS `salesman_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_tran_id` int(11) NOT NULL,
  `salesman` int(11) NOT NULL,
  `payment_mode` int(3) NOT NULL COMMENT 'MODE_CASH->1,MODE_CHEQUE->2,MODE_NEFT->3,MODE_DD->4',
  `payment_type` int(11) NOT NULL COMMENT 'SETUP->1,TOPUP->2',
  `details` text,
  `collection_date` date NOT NULL,
  `confirm_flag` int(3) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `billbook_number` varchar(30) DEFAULT NULL,
  `cheque_number` varchar(50) DEFAULT NULL,
  `collection_amount` int(11) NOT NULL DEFAULT '0',
  `bankname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_sales_type` (`salesman`,`payment_type`),
  KEY `idx_shopid` (`shop_tran_id`),
  KEY `idx_collDate` (`collection_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1677508 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesmen`
--

DROP TABLE IF EXISTS `salesmen`;
CREATE TABLE IF NOT EXISTS `salesmen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dist_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `tran_limit` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `setup_pending` int(10) NOT NULL DEFAULT '0',
  `extra` text,
  `created` datetime DEFAULT NULL,
  `active_flag` int(1) NOT NULL DEFAULT '1',
  `block_flag` int(2) NOT NULL DEFAULT '0',
  `password` char(30) NOT NULL DEFAULT '1234',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `idx_dist` (`dist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=828 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesmen_subarea`
--

DROP TABLE IF EXISTS `salesmen_subarea`;
CREATE TABLE IF NOT EXISTS `salesmen_subarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesmen_id` int(11) NOT NULL,
  `subarea_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `toShow` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `shop_transactions`
--

DROP TABLE IF EXISTS `shop_transactions`;
CREATE TABLE IF NOT EXISTS `shop_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref1_id` int(11) DEFAULT NULL,
  `ref2_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` float(10,3) DEFAULT NULL,
  `discount_comission` float(5,2) DEFAULT '0.00',
  `type` int(4) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `date` date DEFAULT NULL,
  `confirm_flag` int(3) NOT NULL DEFAULT '0',
  `type_flag` int(3) NOT NULL DEFAULT '0' COMMENT '1->cash, 2->neft/rtgs, 3->atm, 4->cheque',
  `note` text,
  PRIMARY KEY (`id`),
  KEY `ref1_type` (`ref1_id`,`type`),
  KEY `ref2_type` (`ref2_id`,`type`),
  KEY `user_type` (`user_id`,`type`),
  KEY `type_date` (`type`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103909618 ;

-- --------------------------------------------------------

--
-- Table structure for table `shop_transactions_logs`
--

DROP TABLE IF EXISTS `shop_transactions_logs`;
CREATE TABLE IF NOT EXISTS `shop_transactions_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref1_id` int(11) DEFAULT NULL,
  `ref2_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` float(10,3) DEFAULT NULL,
  `discount_comission` float(5,2) DEFAULT '0.00',
  `type` int(4) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `date` date DEFAULT NULL,
  `confirm_flag` int(3) NOT NULL DEFAULT '0',
  `type_flag` int(3) NOT NULL DEFAULT '0' COMMENT '1->cash, 2->neft/rtgs, 3->atm, 4->cheque',
  `note` text,
  PRIMARY KEY (`id`),
  KEY `ref1_type` (`ref1_id`,`type`),
  KEY `ref2_type` (`ref2_id`,`type`),
  KEY `user_type` (`user_id`,`type`),
  KEY `type_date` (`type`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81866505 ;

-- --------------------------------------------------------

--
-- Table structure for table `slabs`
--

DROP TABLE IF EXISTS `slabs`;
CREATE TABLE IF NOT EXISTS `slabs` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `group_id` int(4) NOT NULL,
  `commission_dist` float(4,2) DEFAULT NULL,
  `active_flag` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `slabs_products`
--

DROP TABLE IF EXISTS `slabs_products`;
CREATE TABLE IF NOT EXISTS `slabs_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slab_id` int(6) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `percent` float(4,2) NOT NULL DEFAULT '0.00',
  `service_charge` float(4,2) DEFAULT NULL,
  `service_tax` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_slab_prod` (`slab_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=617 ;

-- --------------------------------------------------------

--
-- Table structure for table `slabs_users`
--

DROP TABLE IF EXISTS `slabs_users`;
CREATE TABLE IF NOT EXISTS `slabs_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slab_id` int(6) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `group_id` int(4) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43637 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms_templates`
--

DROP TABLE IF EXISTS `sms_templates`;
CREATE TABLE IF NOT EXISTS `sms_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opr_id` int(5) NOT NULL,
  `template` text NOT NULL,
  `template1` text,
  `type` varchar(10) NOT NULL,
  `type_flag` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_opr` (`opr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252 ;

-- --------------------------------------------------------

--
-- Table structure for table `subarea`
--

DROP TABLE IF EXISTS `subarea`;
CREATE TABLE IF NOT EXISTS `subarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `super_distributors`
--

DROP TABLE IF EXISTS `super_distributors`;
CREATE TABLE IF NOT EXISTS `super_distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance` float(10,3) DEFAULT NULL,
  `opening_balance` float(10,3) NOT NULL DEFAULT '0.000',
  `slab_id` int(4) NOT NULL,
  `margin` float(5,2) NOT NULL DEFAULT '0.00',
  `margin_approved` int(1) NOT NULL DEFAULT '0',
  `pan_number` varchar(12) DEFAULT NULL,
  `email` char(40) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `company` text,
  `address` text,
  `tds_flag` int(1) NOT NULL DEFAULT '1',
  `active` int(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `taggings`
--

DROP TABLE IF EXISTS `taggings`;
CREATE TABLE IF NOT EXISTS `taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=736 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_repeattxn`
--

DROP TABLE IF EXISTS `temp_repeattxn`;
CREATE TABLE IF NOT EXISTS `temp_repeattxn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `prods` varchar(20) NOT NULL,
  `amount` int(10) NOT NULL,
  `api_flag` int(2) DEFAULT '0',
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_cols` (`number`,`prods`,`amount`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14332706 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_reversed`
--

DROP TABLE IF EXISTS `temp_reversed`;
CREATE TABLE IF NOT EXISTS `temp_reversed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shoptrans_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_vendorid` (`shoptrans_id`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1907349 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_txn`
--

DROP TABLE IF EXISTS `temp_txn`;
CREATE TABLE IF NOT EXISTS `temp_txn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_code` varchar(15) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ref` (`ref_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42442092 ;

-- --------------------------------------------------------

--
-- Table structure for table `topup_request`
--

DROP TABLE IF EXISTS `topup_request`;
CREATE TABLE IF NOT EXISTS `topup_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `approveStatus` int(2) NOT NULL COMMENT '0=>not approved1=>approved',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=348 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) NOT NULL,
  `password` char(40) NOT NULL,
  `auth_mobile` char(40) DEFAULT NULL,
  `balance` float(10,2) DEFAULT '0.00',
  `group_id` int(11) NOT NULL,
  `email` char(40) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `passflag` tinyint(1) DEFAULT '0',
  `verify` int(4) DEFAULT '1',
  `syspass` varchar(10) DEFAULT NULL,
  `login_count` int(10) NOT NULL DEFAULT '0',
  `dnd_flag` int(1) NOT NULL DEFAULT '-1',
  `ussd_flag` int(1) NOT NULL DEFAULT '0',
  `ncpr_pref` varchar(20) DEFAULT NULL,
  `followup` varchar(100) DEFAULT NULL,
  `update_flag` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `idx_auth` (`auth_mobile`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=16388115 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `gcm_reg_id` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `location_src` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uuid` (`user_id`,`uuid`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62010 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_taggings`
--

DROP TABLE IF EXISTS `user_taggings`;
CREATE TABLE IF NOT EXISTS `user_taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagging_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(16) DEFAULT NULL,
  `timestamp` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_txnid` (`transaction_id`),
  KEY `idx_tagging` (`tagging_id`,`timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=215553 ;

-- --------------------------------------------------------

--
-- Table structure for table `ussds`
--

DROP TABLE IF EXISTS `ussds`;
CREATE TABLE IF NOT EXISTS `ussds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(10) DEFAULT NULL,
  `request` text,
  `sessionid` varchar(25) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `vendor` int(2) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `level` int(2) NOT NULL DEFAULT '0',
  `sent_xml` text,
  `response` text,
  `extra` text,
  `status` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_mobile` (`mobile`),
  KEY `idx_date` (`date`),
  KEY `idx_extra` (`extra`(5))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26141560 ;

-- --------------------------------------------------------

--
-- Table structure for table `ussd_logs`
--

DROP TABLE IF EXISTS `ussd_logs`;
CREATE TABLE IF NOT EXISTS `ussd_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(10) DEFAULT NULL,
  `request` text,
  `sessionid` varchar(25) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `vendor` int(2) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `level` int(2) NOT NULL DEFAULT '0',
  `sent_xml` text,
  `response` text,
  `extra` text,
  `status` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_mobile` (`mobile`),
  KEY `idx_session` (`sessionid`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2591260 ;

-- --------------------------------------------------------

--
-- Table structure for table `vars`
--

DROP TABLE IF EXISTS `vars`;
CREATE TABLE IF NOT EXISTS `vars` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `value` varchar(1000) NOT NULL,
  `alter` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company` text NOT NULL,
  `shortForm` varchar(8) DEFAULT NULL,
  `balance` float(10,2) NOT NULL DEFAULT '0.00',
  `ip` varchar(20) DEFAULT NULL,
  `port` varchar(6) DEFAULT NULL,
  `update_flag` int(1) NOT NULL DEFAULT '0',
  `active_flag` int(1) NOT NULL DEFAULT '1',
  `show_flag` int(2) DEFAULT '0',
  `health_factor` int(10) NOT NULL DEFAULT '0',
  `machine_id` int(4) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `last30bal` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_activations`
--

DROP TABLE IF EXISTS `vendors_activations`;
CREATE TABLE IF NOT EXISTS `vendors_activations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `param` varchar(20) DEFAULT NULL,
  `amount` int(10) DEFAULT '0',
  `discount_commission` float(5,2) DEFAULT '0.00',
  `ref_code` varchar(16) DEFAULT NULL,
  `vendor_refid` varchar(30) DEFAULT NULL,
  `operator_id` varchar(20) DEFAULT NULL,
  `shop_transaction_id` int(11) DEFAULT NULL,
  `retailer_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `status` int(4) NOT NULL DEFAULT '0',
  `prevStatus` varchar(3) DEFAULT NULL,
  `api_flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-> via sms, 1-> via api, 2-> via ussd',
  `cause` text,
  `code` varchar(5) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `extra` varchar(255) DEFAULT NULL,
  `complaintNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_refcode` (`ref_code`(9)),
  KEY `idx_mobile` (`mobile`(10)),
  KEY `idx_param` (`param`(10)),
  KEY `idx_vendorid_status` (`vendor_id`,`status`),
  KEY `idx_date` (`date`),
  KEY `idx_ret_date` (`retailer_id`,`date`),
  KEY `idx_mob` (`mobile`(4)),
  KEY `idx_vendorrefid` (`vendor_refid`(10)),
  KEY `idx_vend_date` (`vendor_id`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53420224 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_activations_ext`
--

DROP TABLE IF EXISTS `vendors_activations_ext`;
CREATE TABLE IF NOT EXISTS `vendors_activations_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_act_id` int(11) NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_vend_act` (`vendor_act_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1616393 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_commissions`
--

DROP TABLE IF EXISTS `vendors_commissions`;
CREATE TABLE IF NOT EXISTS `vendors_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount_commission` float(4,2) NOT NULL DEFAULT '0.00',
  `active` int(1) NOT NULL DEFAULT '0',
  `oprDown` int(1) NOT NULL DEFAULT '0',
  `circle` varchar(4) DEFAULT NULL,
  `circles_yes` text,
  `circles_no` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vendor_id` (`vendor_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=382 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_messages`
--

DROP TABLE IF EXISTS `vendors_messages`;
CREATE TABLE IF NOT EXISTS `vendors_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_tran_id` varchar(30) DEFAULT NULL,
  `vendor_refid` varchar(20) DEFAULT NULL,
  `service_id` int(5) NOT NULL,
  `service_vendor_id` int(11) NOT NULL,
  `internal_error_code` varchar(30) DEFAULT NULL,
  `response` text,
  `status` varchar(10) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_trans` (`shop_tran_id`(15))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=164657090 ;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_number`
--

DROP TABLE IF EXISTS `virtual_number`;
CREATE TABLE IF NOT EXISTS `virtual_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(12) NOT NULL,
  `message` text NOT NULL,
  `virtual_num` varchar(12) NOT NULL,
  `description` text,
  `sms_time` datetime NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mobile` (`mobile`),
  KEY `idx_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14164846 ;

DELIMITER $$
--
-- Events
--
DROP EVENT `mobile`$$
CREATE DEFINER=`root`@`%` EVENT `mobile` ON SCHEDULE EVERY 5 SECOND STARTS '2012-12-30 07:07:47' ON COMPLETION NOT PRESERVE ENABLE DO call wall()$$

DELIMITER ;
