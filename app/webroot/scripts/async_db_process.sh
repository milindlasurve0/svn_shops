#!/bin/bash
cd /var/www/html/shops

if [ "$#" -eq 2 ] && [ "$2" = "vm" ];
then
    nohup /usr/bin/php app/webroot/cron_dispatcher.php crons/db_table_async_process/$1/$2  > /dev/null 2>&1 &
fi

if [ "$#" -eq 2 ] && [ "$2" = "vt" ];
then
    nohup /usr/bin/php app/webroot/cron_dispatcher.php crons/db_table_async_process/$1/$2  > /dev/null 2>&1 &
fi

cd -
