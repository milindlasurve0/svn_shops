<link rel="stylesheet" type="text/css" href="/css/style.css" />
<table  border="0" align="center">
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td valign="top">
		<div class="logo" style="float:left;">
	    	<img src="/img/pay1_logo.svg" height="80%">
	    </div>
	</td>
	<td width="80%">
	<table width="100%" border="0">
	<tr>
	<td bgcolor="#ff0000" align="left"><font face="arial,sans-serif" color="#ffffff" size="3px">&nbsp;&nbsp;<b>Sorry, the page you are looking for is no longer here [Error 404]</b></font></td>
	</tr>
	<tr>
	<td align="left" style="padding-left:10px">
	    We think you will find one of the following links useful:
	    <div style="font-size:0.75em;font-family:Arial,Helvetica,sans-serif;text-decoration:none;padding-top:10px">
	    <a href="/shops/view">Back to Home</a> or <a href="javascript:void(0);" onclick="history.back();">Go back to the previous page</a><br>
	    </div>  
	    <div style="float:left;border-bottom:1px solid #cccccc;margin:10px 0px 10px 0px;width:100%"></div>
	    <strong>You may not be able to find this page because:</strong>
	<ul style="padding-left:15px;">
	    <li>You may have clicked on an expired link.</li>
	    <li>You may have mistyped the address.</li>
	    <li>Some web addresses are case sensitive.</li>
	   
	</ul>
	<div style="float:left;border-bottom:1px solid #cccccc;margin:10px 0px 10px 0px;width:100%"></div>    
	   
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
	<td colspan="2">
	 <div id="footer" class="footer">
   		 	<!-- <span class="rightFloat"><a href="http://www.blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://www.blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://www.blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://www.blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://www.blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Give Feedback</a></span> -->
         	All Rights Reserved © <?php echo date('Y'); ?> Mindsarray Technologies Pvt Ltd
    	</div>
	</td>	
</tr>
</table>		