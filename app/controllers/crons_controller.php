<?php

class CronsController extends AppController {

    var $name = 'Crons';
    var $helpers = array('Html', 'Ajax', 'Javascript', 'Minify', 'GCM');
    var $components = array('RequestHandler', 'Shop','General');
    var $uses = array('Retailer','Slaves');

    function beforeFilter() {
        set_time_limit(0);
        ini_set("memory_limit", "-1");
        parent::beforeFilter();
        $this->Auth->allow('*');
    }

    function issueDetection() {//every 15 mins
        if (date('H') < 8)
            exit;

        $data = $this->Slaves->query("
                   SELECT 
                        count(vendors_activations.id) as ids, 
                        vendors_activations.vendor_id,
                        vendors_activations.product_id,
                        products.name,
                        vendors.shortForm,
                        sum(if( vendors_activations.status != 3,amount,0)) as sale,
                        sum(if(vendors_activations.status =0,1,0)) as process,
                        sum(if(vendors_activations.status =2 OR vendors_activations.status =3,1,0)) as failure,
                        vendors.update_flag 
                   FROM 
                        `vendors_activations`,
                        products,
                        vendors 
                   WHERE 
                        products.id = vendors_activations.product_id 
                   AND 
                        vendors.id = vendors_activations.vendor_id 
                   AND 
                        vendors_activations.date = '" . date('Y-m-d') . "' 
                   AND 
                        vendors_activations.timestamp <= '" . date('Y-m-d H:i:s', strtotime('-3 minutes')) . "' 
                   AND  
                        vendors_activations.timestamp >= '" . date('Y-m-d H:i:s', strtotime('-30 minutes')) . "'                   
                   GROUP BY 
                        vendors_activations.product_id,vendors_activations.vendor_id 
                   ORDER BY 
                        vendors_activations.product_id"
        );
        $dataVC = $this->Slaves->query("
                   SELECT 
                        vendors_activations.vendor_id,
                        vendors.shortForm,
                        sum(if( vendors_activations.status != 3,amount,0)) as sale 
                   FROM 
                        `vendors_activations`,
                        vendors 
                   WHERE 
                        vendors.id = vendors_activations.vendor_id 
                   AND 
                        vendors_activations.date = '" . date('Y-m-d') . "' 
                   
                   GROUP BY 
                        vendors_activations.vendor_id 
                   ORDER BY 
                        vendors.id"
        );

        $vendorConsumption = array();
        foreach ($dataVC as $x) {
            $vendorConsumption[$x['vendors_activations']['vendor_id']] = $x[0]['sale'];
        }


        $prods = array();
        $vendorCurConsmption = array();
        foreach ($data as $dt) {
            if (empty($prods[$dt['vendors_activations']['product_id']])) {
                $prods[$dt['vendors_activations']['product_id']]['total'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['process'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['failure'] = 0;
                //$prods[$dt['vendors_activations']['product_id']]['active'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['success'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['sale'] = 0;
            }

            $prods[$dt['vendors_activations']['product_id']]['total'] += $dt['0']['ids'];
            $prods[$dt['vendors_activations']['product_id']]['process'] += $dt['0']['process'];
            $prods[$dt['vendors_activations']['product_id']]['failure'] += $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['success'] += $dt['0']['ids'] - $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['sale'] += $dt['0']['sale'];

            $prods[$dt['vendors_activations']['product_id']]['name'] = $dt['products']['name']; //$dt['vendors_activations']['name']; //@TODO1


            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['total'] = $dt['0']['ids'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['process'] = $dt['0']['process'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['failure'] = $dt['0']['failure'];

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['sale'] = $dt['0']['sale'];
            if (!isset($vendorCurConsmption[$dt['vendors_activations']['vendor_id']])) {
                $vendorCurConsmption[$dt['vendors_activations']['vendor_id']] = $dt['0']['sale'];
            } else {
                $vendorCurConsmption[$dt['vendors_activations']['vendor_id']] += $dt['0']['sale'];
            }

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['success'] = $dt['0']['ids'] - $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['vendor'] = $dt['vendors']['shortForm']; //$dt['vendors_activations']['shortForm'];//@TODO1
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['vendor_id'] = $dt['vendors_activations']['vendor_id']; //$dt['vendors_activations']['vendor_id'];//@TODO1

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['modem_flag'] = $dt['vendors']['update_flag']; //$dt['vendors_activations']['update_flag'];//@TODO1
        }

        $allDet = $prods;
        $array = array();
        $data = $this->Slaves->query("
                    SELECT 
                        products.id,
                        products.name,
                        vendors_commissions.vendor_id,
                        vendors.update_flag 
                    FROM 
                        products,
                        vendors_commissions,
                        vendors 
                    WHERE 
                        product_id = products.id 
                    AND 
                        vendors_commissions.vendor_id = vendors.id 
                    AND vendors_commissions.active = 1 
                    AND monitor = 1"
        );

        $primary = array();
        foreach ($data as $prod) {
            $primary[$prod['products']['id']] = $prod['vendors_commissions']['vendor_id'];
        }


        foreach ($data as $prod) {
            if (!isset($prods[$prod['products']['id']])) {
                $array['no'][] = $prod['products']['name'];
            } else if (($prods[$prod['products']['id']]['total'] >= 5 && $prods[$prod['products']['id']]['failure'] * 100 / $prods[$prod['products']['id']]['total'] >= 5) || $prods[$prod['products']['id']]['failure'] * 100 / $prods[$prod['products']['id']]['total'] >= 50) {
                $array['failure'][$prod['products']['id']] = $prods[$prod['products']['id']];
            } else if ($prods[$prod['products']['id']]['total'] >= 20 && $prods[$prod['products']['id']]['process'] * 100 / $prods[$prod['products']['id']]['total'] >= 25) {
                $array['process'][$prod['products']['id']] = $prods[$prod['products']['id']];
            }

            $active_vendor = $prod['vendors_commissions']['vendor_id'];
            $prods[$prod['products']['id']]['active_total'] = isset($prods[$prod['products']['id']]['vendors'][$active_vendor]['total']) ? $prods[$prod['products']['id']]['vendors'][$active_vendor]['total'] : 0;

            //if($prods[$prod['products']['id']]['total'] >= 5 && $prods[$prod['products']['id']]['active_total']*100/$prods[$prod['products']['id']]['total'] <= 90){
            $array['secondary'][$prod['products']['id']] = $prods[$prod['products']['id']];
            //}
        }

        $mail_body = "<h3>Pay1 Last 30 minutes transactions status:</h3>";
        if (!empty($array['no'])) {
            $mail_body .= "<b>No transactions happening in:</b><br/>";
            foreach ($array['no'] as $val) {
                $mail_body .= $val . ", ";
            }
        }

        if (!empty($array['failure'])) {
            $mail_body .= "<br/><br/><b>Lot of failures in:</b><br/>";
            foreach ($array['failure'] as $prod => $val) {
                $mail_body .= $val['name'] . "(" . intval($val['failure'] * 100 / $val['total']) . "%), ";
            }
        }

        if (!empty($array['process'])) {
            $mail_body .= "<br/><br/><b>Lot of transactions in process in:</b><br/>";
            foreach ($array['process'] as $prod => $val) {
                $mail_body .= $val['name'] . "(" . intval($val['process'] * 100 / $val['total']) . "%), ";
            }
        }


        $tempArr = array();
        if (!empty($array['secondary'])) {
            $tempStr .= "<br/><br/><b>Secondary routed transactions:</b><br/>";
            foreach ($array['secondary'] as $prod => $val) {
                if (!empty($val['name'])) {
                    $tempStr = "";
                    foreach ($val['vendors'] as $vend) {
                        if ($vend['success'] > 0 && $val['success'] > 0 && intval($vend['success'] * 100 / $val['success']) > 0) {
                            $color = $primary[$prod] == $vend['vendor_id'] ? "BLUE" : "";
                            $color = "1" != $vend['modem_flag'] ? "6e062f" : $color;
                            $tempStr .= "<span style='color:$color'>" . $vend['vendor'] . " (" . intval($vend['success'] * 100 / $val['success']) . "% " . "), " . "</span>";
                        }
                    }
                    $tempArr[$prod] = $tempStr;
                }
            }
        }

        $finalArr = array();
        if (!empty($prods)) {

            foreach ($prods as $pid => $val) {
                if (!empty($val['name'])) {

                    foreach ($val['vendors'] as $vid => $vend) {

                        $per = empty($val['success']) ? 0 : intval($vend['success'] * 100 / $val['success']);
                        if ($vend['modem_flag'] != 1) {// if vendor
                            $finalArr[$vend['vendor']]['today'] = $vendorConsumption[$vend['vendor_id']];
                            $finalArr[$vend['vendor']]['current'] = $vendorCurConsmption[$vend['vendor_id']];
                            $finalArr[$vend['vendor']]['vid'] = $vid;
                            if (($per > 5 && $val['sale'] > 2000) || ($primary[$pid] == $vend['vendor_id'])) { // if($per > 5 && $val['sale'] > 2000){
                                /* $finalArr[$vend['vendor']]['today'] = $vendorConsumption[$vend['vendor_id']];
                                  $finalArr[$vend['vendor']]['current'] =  $vendorCurConsmption[$vend['vendor_id']]; */

                                $finalArr[$vend['vendor']]['arr'][$val['name']]['per'] = $per; //$vend['success'];
                                $finalArr[$vend['vendor']]['arr'][$val['name']]['amt'] = $vend['sale'];
                                $finalArr[$vend['vendor']]['arr'][$val['name']]['sale'] = $val['sale'];

                                $finalArr[$vend['vendor']]['arr'][$val['name']]['vid'] = $vid;
                                $finalArr[$vend['vendor']]['arr'][$val['name']]['pid'] = $pid;

                                if ((!empty($primary[$vend['vendor_id']])) && ($primary[$pid] != $vend['vendor_id'])) {
                                    $finalArr[$vend['vendor']]['arr'][$val['name']]['color'] = "RED";
                                } else {
                                    $finalArr[$vend['vendor']]['arr'][$val['name']]['color'] = "WHITE";
                                }
                            }
                        } else if ($primary[$pid] == $vend['vendor_id'] || $per > 0) {
                            // $finalArr["modem"][$val['name']] = $tempArr[$pid];
                            //$finalArr["modem"][$val['name']] = $per < 20 ? "<span style='color:RED'>".$tempArr[$pid]."</span>" : $tempArr[$pid];

                            $modemBal[$vend['vendor']]['today'] = $vendorConsumption[$vend['vendor_id']];
                            $modemBal[$vend['vendor']]['current'] = $vendorCurConsmption[$vend['vendor_id']];
                            $modemBal[$vend['vendor']]['name'] = $vend['vendor'];

                            $finalArr["modem"][$val['name']]['det'] = $tempArr[$pid];
                            $finalArr["modem"][$val['name']]['color'] = ($primary[$pid] == $vend['vendor_id'] && $per < 20) ? "RED" : "";
                        }
                    }
                }
            }
        }

        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();

        $vendorBal = array();
        $vendors = $this->Slaves->query("SELECT * FROM `vendors` where update_flag = 0 AND active_flag != 2 AND show_flag = 1");
        foreach ($vendors as $vend) {
            $name = $vend['vendors']['shortForm'];
            $method = $name . "Balance";
            if (method_exists($obj, $method)) {
                $vendorBal[$name] = $obj->$method(1);
            }
        }

        $mail_body .= "<br/><br/><b>Secondary routed transactions:</b><br/>";
        foreach ($finalArr as $v => $arr) {
            if ($v != "modem") {
                $mail_body .= "<br/>";
                $mail_body .= "<table width='100%' border='1' style='border-collapse:collapse;font-size: 14px;'><tr ><td colspan='3'><b>" . $v . ":</b> Sale in Last 30 Mins(<b>" . $arr['current'] . "</b>) Today's(<b>" . $arr['today'] . "</b>) Available Balance(<b>" . (empty($vendorBal[$v]) ? 0 : intval($vendorBal[$v]['balance'])) . "</b>)</td></tr>";
                foreach ($arr['arr'] as $opr => $val) {
                    if ($opr != 'today' && $opr != 'current')
                        $mail_body .= "<tr " . ($val['color'] == "RED" ? "style='color:RED'" : "") . "><td width='50%'>" . $opr . " </td><td width='25%'>" . $val['per'] . "%</td><td width='25%'>" . $val['amt'] . " </td></tr>";
                }

                foreach ($primary as $opr => $val) {
                    if ($val == $arr['vid'] && !isset($allDet[$opr]['vendors'][$arr['vid']])) {
                        $oprDet = $this->Shop->getProdInfo($opr);
                        $mail_body .= "<tr style='color:orange'><td width='50%'>" . $oprDet['name'] . " </td><td width='25%'>0%</td><td width='25%'>0</td></tr>";
                    }
                }

                $mail_body .= "</table >";
            }
        }
        $mail_body .= "<br/>";
        $mail_body .= "<table width='100%' border='1' style='border-collapse:collapse;font-size: 14px;'><tr ><td colspan='2'><b>Modem</b></td></tr>";
        foreach ($finalArr["modem"] as $opr => $val) {
            $mail_body .= "<tr " . ($val['color'] == "RED" ? "style='color:RED'" : "") . "><td width='30%'>" . $opr . " </td><td width='70%'>";
            $mail_body .=$val['det'];
            $mail_body .= "</td></tr>";
        }
        $mail_body .= "</table >";

        $mail_body .= "<br/>";
        $mail_body .= "<table width='100%' border='1' style='border-collapse:collapse;font-size: 14px;'><tr ><td><b>Modem</b></td><td><b>Sale in Last 30 Mins</b></td><td><b>Today's Sale</b></td></tr>";
        foreach ($modemBal as $vid => $arr) {
            $mail_body .= "<tr " . ($val['color'] == "RED" ? "style='color:RED'" : "") . ">
                                    <td width='40%'>" . $arr['name'] . "</td>
                                    <td width='30%'>" . $arr['current'] . "</td>
                                    <td width='30%'>" . $arr['today'] . "</td>";
            $mail_body .= "</tr>";
        }

        $mail_body .= "</table >";

        //$this->printArray($array);
        $mail_subject = "(IMP) Pay1 Transactions Status of last 30 minutes";
        //$fh = fopen("/var/www/html/testIssuDetect.txt","a+");
        //fwrite($fh,$mail_body);
        //fclose($fh);
        $this->General->sendMails($mail_subject, $mail_body, array('backend@mindsarray.com','wahid@mindsarray.com',
            'ekta@mindsarray.com','hiteshs@mindsarray.com','khalid@mindsarray.com','dharmesh@mindsarray.com','prerna@mindsarray.com'), 'mail');
        //echo $mail_body;
        $this->autoRender = false;
    }

    /* function pullbackRetailer(){
      $data1 = $this->Retailer->query("SELECT GROUP_CONCAT(ref1_id) as ids FROM  `shop_transactions`
      WHERE  `type` =19
      AND  `date` =  '2014-01-04'");
      $already = explode(",",$data1['0']['0']['ids']);

      $data = $this->Retailer->query("SELECT SUM( rental ) AS srent, COUNT( * ) AS cnt, retailer_id
      FROM  `rentals`
      WHERE DATE =  '2014-01-03'
      GROUP BY retailer_id
      HAVING srent >0
      ORDER BY cnt DESC");

      App::import('Controller', 'Shops');
      $ini = new ShopsController;
      $ini->constructClasses();

      //$ret = 19;
      //$amt = 100;
      //$ini->refundRetailer($ret,$amt);
      foreach($data as $dt){
      $ret = $dt['rentals']['retailer_id'];
      if(in_array($ret,$already))continue;

      echo $ret . "<br/>";
      //$amt = $dt['0']['srent'];
      //$ini->refundRetailer($ret,$amt);
      //sleep(1);
      //usleep(200000);
      }
      $this->autoRender = false;
      } */

    function moneyBackRetailers() {//1st day of month
        //SCHEME: 50000 via app ..Get 100 rs back
        $from = '2014-03-01';
        $to = '2014-03-31';

        $check = $this->Slaves->query("SELECT ref1_id FROM refunds,`shop_transactions` WHERE refunds.shoptrans_id = shop_transactions.id AND refunds.date = '" . date('Y-m-d') . "'");
        $rets = array();
        foreach ($check as $ck) {
            $rets[] = $ck['shop_transactions']['ref1_id'];
        }

        $data = $this->Slaves->query("SELECT sum(sale-sms_sale-ussd_sale) as amts, retailer_id, retailers.mobile FROM  `retailers_logs`,retailers WHERE retailers.id = retailer_id AND kyc_flag = 0 AND date >= '$from' AND date <= '$to' group by retailer_id having (sum(sale-sms_sale-ussd_sale) >= 50000) order by amts desc");
        $i = 0;

        $total = 0;
        $total_rets = 0;
        $highest = 0;
        $High_ret = "";
        $ret_50 = 0;
        $ret_100 = 0;
        $ret_200 = 0;
        $ret_300 = 0;
        $MsgTemplate = $this->General->LoadApiBalance();
        foreach ($data as $dt) {
            $sms = "";
            if (in_array($dt['retailers_logs']['retailer_id'], $rets)) {
                $i++;
                continue;
            }

            if ($i == 0) {
                $highest = $dt['0']['amts'];
                $High_ret = $dt['retailers']['mobile'];
            }

            if ($dt['0']['amts'] >= 300000) {
                $amt = 500;
//              $sms = "Dear Retailer, Aapka last month ka application/internet ka 300000 ka target complete ho gaya hai";
                $target = 300000;
                $ret_300 += 1;
            } else if ($dt['0']['amts'] >= 200000) {
                $amt = 300;
//                $sms = "Dear Retailer, Aapka last month ka application/internet ka 200000 ka target complete ho gaya hai";
                $target = 200000;
                $ret_200 += 1;
            } else if ($dt['0']['amts'] >= 100000) {
                $amt = 100;
//                $sms = "Dear Retailer, Aapka last month ka application/internet ka 100000 ka target complete ho gaya hai";
                $target = 100000;
                $ret_100 += 1;
            } else {
                $amt = 50;
//                $sms = "Dear Retailer, Aapka last month ka application/internet ka 50000 ka target complete ho gaya hai";
                $target = 50000;
                $ret_50 += 1;
            }
            
            if ($amt > 0) {

                
                $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $amt, $dt['retailers_logs']['retailer_id'], RETAILER);
                $bal = $this->Shop->shopBalanceUpdate($amt, 'add', $dt['retailers_logs']['retailer_id'], RETAILER);
                $this->Shop->addOpeningClosing($dt['retailers_logs']['retailer_id'], RETAILER, $trans_id, $bal - $amt, $bal);
//                $sms = "Dear Retailer, Aapka last month ka application/internet ka $target ka target complete ho gaya hai";
//                $sms .= "\nAapka last month ka sale hai: Rs " . $dt['0']['amts'];
//                $sms .= "\nCompany is giving you a bonus of Rs $amt!!. Aap har mahine ye bonus kama sakte ho";
//                $sms .= "\nYour current balance is: Rs. " . $bal;

                $paramdata['TARGET'] = $target;
                $paramdata['AMOUNT'] = $amt;
                $paramdata['SALE_AMOUNT'] = $dt['0']['amts'];
                $paramdata['BALANCE'] = $bal; 
                
                $content =  $MsgTemplate['Retailers_MoneyBack_MSG'];
                $sms   = $this->General->ReplaceMultiWord($paramdata,$content);
                
                $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$amt,1,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
                echo nl2br($sms) . "<br/>";
                $this->General->sendMessage($dt['retailers']['mobile'], $sms, 'notify');
                $total += $amt;
                $total_rets += 1;
                usleep(10000);
            }
            $i++;
        }

        $mail_body = "Total incentive given to app retailers: $total<br/>Total retailers $total_rets<br/>Highest sale: $highest ($High_ret)<br/>No. of retailers (50000 - 100000): $ret_50<br/>No. of retailers (100000 - 200000): $ret_100<br/>No. of retailers (200000 - 300000): $ret_200<br/>No. of retailers (300000+): $ret_300";
        $this->General->sendMails("Incentive App Retailers", $mail_body, array('tadka@mindsarray.com', 'sales@mindsarray.com'), 'mail');

        $this->autoRender = false;
    }

    function moneyBackRetailersSpecial() {//1st day of month
        //SCHEME: 50000 via app ..Get 100 rs back
        $from = '2013-10-01';
        $to = '2013-10-31';

        $data = $this->Slaves->query("SELECT sum(sale) as amts, retailer_id, retailers.mobile FROM  `retailers_logs` ,retailers   WHERE retailers.id = retailer_id AND date >= '$from' AND date <= '$to' group by retailer_id having (sum(sale) >= 200000) order by amts desc");
        $i = 0;
        $MsgTemplate = $this->General->LoadApiBalance(); 
        $total_rets = 0;
        $total = 0;
        foreach ($data as $dt) {

            $amt = 500;
            $sms = "";
//            $sms .= "\nAapka last month ka sale hai: Rs " . $dt['0']['amts'];
//            $sms .= "\nCompany is giving you a diwali bonus of Rs $amt!!";
            $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $amt, $dt['retailers_logs']['retailer_id'], RETAILER);
            $bal = $this->Shop->shopBalanceUpdate($amt, 'add', $dt['retailers_logs']['retailer_id'], RETAILER);
            $this->Shop->addOpeningClosing($dt['retailers_logs']['retailer_id'], RETAILER, $trans_id, $bal - $amt, $bal);
//            $sms .= "\nYour current balance is: Rs. " . $bal;

            $paramdata['AMOUNT'] =  $amt;
            $paramdata['SALE_AMOUNT'] = $dt['0']['amts'];
            $paramdata['BALANCE'] = $bal;  

            $sms .=  $MsgTemplate['Retailers_MoneyBack_Special_MSG'];
            $sms = $this->General->ReplaceMultiWord($paramdata,$sms);
            
            $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$amt,1,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
            $this->General->sendMessage($dt['retailers']['mobile'], $sms, 'notify');
            $total += $amt;
            $total_rets += 1;
            echo nl2br($sms) . "<br/>";
            $i++;
        }

        $mail_body = "Diwali bonus given to retailers: $total<br/>Total retailers $total_rets";
        //echo $mail_body;
        $this->General->sendMails("Incentive Retailers", $mail_body, array('tadka@mindsarray.com', 'rm@mindsarray.com'), 'mail');

        $this->autoRender = false;
    }

    /*function moneyBackDistributors() {//1st day of month
        //SCHEME: 50000 via app ..Get 100 rs back
        $data = $this->Slaves->query("SELECT distributors.*,users.mobile FROM distributors,users WHERE users.id = user_id AND distributors.discounted_money > 0");
        foreach ($data as $dt) {
            $amt = $dt['distributors']['discounted_money'];
            $kit_discount = $dt['distributors']['discount_kit'];

            $sms = "Dear Distributor, We are refunding kit money of Rs. " . $amt . " to you";
            $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $amt, $dt['distributors']['id'], DISTRIBUTOR);
            $bal = $this->Shop->shopBalanceUpdate($amt, 'add', $dt['distributors']['id'], DISTRIBUTOR);
            $this->Shop->addOpeningClosing($dt['distributors']['id'], DISTRIBUTOR, $trans_id, $bal - $amt, $bal);

            $this->Retailer->query("UPDATE distributors SET discounted_money = 0 WHERE id = " . $dt['distributors']['id']);
            
            if($dt['users']['mobile']){
            	$message_distributor = "No. of Kits left: ". $dt['distributors']['kits'];
            	$this->General->sendMessage($dt['users']['mobile'], $message_distributor, "notify", null, DISTRIBUTOR);
            }
            $sms .= "\nYour current balance is: Rs. " . $bal;
            if ($dt['distributors']['kits'] >= 0) {
                $sms .= "\nNo. of Kits left: " . $dt['distributors']['kits'];
            }
            $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . DISTRIBUTOR . ",$trans_id,$amt,2,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");

            $this->General->sendMessage($dt['users']['mobile'], $sms, 'notify');
        }
        $this->autoRender = false;
    }*/

    /*function newRetailers() {//1st day of month
        //SCHEME: 1000 pe Rs25, 2000 pe Rs50
        $data = $this->Slaves->query("SELECT distributors.*,users.mobile FROM distributors,users WHERE users.id = user_id AND distributors.toshow=1 AND distributors.discounted_money > 0");
        foreach ($data as $dt) {
            $amt = $dt['distributors']['discounted_money'];
            $kit_discount = $dt['distributors']['discount_kit'];

            $sms = "Dear Distributor, We are refunding kit money of Rs. " . $amt . " to you";
            $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $amt, $dt['distributors']['id'], DISTRIBUTOR);
            $bal = $this->Shop->shopBalanceUpdate($amt, 'add', $dt['distributors']['id'], DISTRIBUTOR);
            $this->Shop->addOpeningClosing($dt['distributors']['id'], DISTRIBUTOR, $trans_id, $bal - $amt, $bal);

            $this->Retailer->query("UPDATE distributors SET discounted_money = 0 WHERE id = " . $dt['distributors']['id']);
            
            if($dt['users']['mobile']){
            	$message_distributor = "No. of Kits left: ". $dt['distributors']['kits'];
            	$this->General->sendMessage($dt['users']['mobile'], $message_distributor, "notify", null, DISTRIBUTOR);
            }
            $sms .= "\nYour current balance is: Rs. " . $bal;
            if ($dt['distributors']['kits'] >= 0) {
                $sms .= "\nNo. of Kits left: " . $dt['distributors']['kits'];
            }
            $this->General->sendMessage($dt['users']['mobile'], $sms, 'notify');
        }
        $this->autoRender = false;
    }*/

    function logs() {//every 1 hour log
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();
        $hour = date('H') + 1;
        //float
        $sds_float = $this->Slaves->query("SELECT SUM(balance) as bal FROM super_distributors WHERE id NOT IN (" . SDISTS . ") AND balance > 0");
        $dis_float = $this->Slaves->query("SELECT SUM(balance) as bal FROM distributors WHERE id NOT IN (" . DISTS . ") AND balance > 0");
        $ret_float = $this->Slaves->query("SELECT SUM(balance) as bal FROM retailers WHERE balance > 0");
		$ret_float_without_b2c_retailer = $this->Slaves->query("SELECT balance  FROM retailers WHERE balance > 0 and id = 13");

        $float = intval($sds_float['0']['0']['bal'] + $dis_float['0']['0']['bal'] + $ret_float['0']['0']['bal']);
		
		$float_without_b2c_retailer = intval($float-$ret_float_without_b2c_retailer[0]['retailers']['balance']);

        //amount transferred
        $sds_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='" . date('Y-m-d') . "' AND type = " . ADMIN_TRANSFER . " AND confirm_flag != 1 AND ref2_id NOT IN (" . SDISTS . ")");
        $dis_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='" . date('Y-m-d') . "' AND type = " . SDIST_DIST_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . SDISTS . ") AND ref2_id NOT IN (" . DISTS . ")");
        $ret_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='" . date('Y-m-d') . "' AND type = " . DIST_RETL_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . DISTS . ")");

        //$pullback = $this->Retailer->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='".date('Y-m-d')."' AND ((type = ".PULLBACK_RETAILER . " AND ref1_id IN (".DISTS.")) OR  (type = ".PULLBACK_DISTRIBUTOR . " AND ref1_id IN (".SDISTS.") AND ref2_id NOT IN (".DISTS.")) OR (type = ".PULLBACK_SUPERDISTRIBUTOR." AND ref2_id NOT IN (".SDISTS.")))");

        $transferred = intval($sds_trans['0']['0']['amt'] + $dis_trans['0']['0']['amt'] + $ret_trans['0']['0']['amt']);

        //inventory already in system
		

        $inventory = 0;
        $vendors = $this->Slaves->query("SELECT * FROM `vendors` where update_flag = 0 AND active_flag = 1");
        foreach ($vendors as $vend) {
            $name = $vend['vendors']['shortForm'];
            $method = $name . "Balance";
            if (method_exists($obj, $method)) {
                $balance = $obj->$method(1);
                $inventory += intval($balance['balance']);
            }
        }

        $modem_bal = 0;

        $data = $this->Shop->getVendors();
        foreach ($data as $dt) {
            if ($dt['vendors']['update_flag'] == 1) {
                $modem = $obj->modemBalance(null, $dt['vendors']['id'],false);
                foreach ($modem as $mds) {
                    $modem_bal += $mds['balance'];
                }
            }
        }

        $inventory += $modem_bal;

        //sale
        $sale_all = $this->Retailer->query("SELECT SUM(if(confirm_flag = 1 AND type = 4,amount,0)) as amt, SUM(if(type_flag = 1 AND type = 11 ,amount,0)) as reversal FROM shop_transactions WHERE date='" . date('Y-m-d') . "' AND ref1_id != 13 AND type in (4,11)");
        $sale = intval($sale_all['0']['0']['amt']);
        $reversals = intval($sale_all['0']['0']['reversal']);

        //comm
        $comm = $this->Retailer->query("SELECT SUM( IF( TYPE =6 AND distributors.parent_id = 3, amount, 0 ) ) AS comm_d, SUM( IF( TYPE =5, amount, 0 ) ) AS comm_sd, SUM( IF( confirm_flag =1 AND TYPE =4, amount * discount_comission /100, 0 ) ) AS comm_r, SUM( IF( TYPE =24, amount, 0 ) ) AS service_charge FROM shop_transactions LEFT JOIN distributors ON (distributors.id = ref1_id AND distributors.parent_id = 3) WHERE DATE =  '" . date('Y-m-d') . "' AND TYPE IN ( 4, 5, 6, 24 )");
        $comm_tot = intval($comm['0']['0']['comm_d'] + $comm['0']['0']['comm_sd'] + $comm['0']['0']['comm_r']);

		
        $this->Retailer->query("INSERT INTO float_logs VALUES (NULL,'$float','$transferred','$inventory','$sale','$comm_tot','$reversals','" . date('Y-m-d') . "','" . date('H:i:s') . "',$hour,'".$comm['0']['0']['service_charge']."','".$float_without_b2c_retailer."')");

        $this->autoRender = false;
    }

    function cutRetailerRental($dist_id = null) {//every month once on 1st
        $from = '2013-12-01';
        $to = '2013-12-31';
        $MsgTemplate = $this->General->LoadApiBalance();

        if (empty($dist_id)) {
            echo "Please put distributor id";
            exit;
        }
        if (!empty($dist_id))
            $distributors = $this->Slaves->query("SELECT * FROM distributors WHERE id in ($dist_id) AND id NOT IN (115,169,197) AND parent_id NOT IN (6) AND rental_amount > 0");
        else
            $distributors = $this->Slaves->query("SELECT * FROM distributors WHERE id NOT IN (115,169,197) AND parent_id NOT IN (6) AND rental_amount > 0");

        foreach ($distributors as $dist) {
            $dist_id = $dist['distributors']['id'];

            $retailers = $this->Slaves->query("SELECT retailers.* FROM retailers WHERE parent_id = $dist_id AND balance >= 0 AND rental_flag = 1 AND date(created) < '$from'");

            $rental_amount = $dist['distributors']['rental_amount'];
            $target_amount = $dist['distributors']['target_amount'];

            foreach ($retailers as $ret) {
                $sms = "";

                $from_ret1 = date('Y-m-d', strtotime($ret['retailers']['created']));
                $ret_id = $ret['retailers']['id'];

                if ($from > $from_ret1)
                    $from_ret = $from;
                else {
                    //$from_ret = $from_ret1;
                    continue;
                }


                $date1 = new DateTime($to);
                $date2 = new DateTime($from_ret);
                $date3 = new DateTime($from);
                $interval = $date1->diff($date2);
                $days = $interval->d + 30 * $interval->m;

                $interval_tot = $date1->diff($date3);
                $days_tot = $interval_tot->d + 30 * $interval_tot->m;
                $frac = $days / $days_tot;

                $rental = intval($frac * $rental_amount);
                if ($target_amount > 0) {
                    $target = intval($target_amount * $frac);
                    $sale = $this->Slaves->query("SELECT sum(sale) as amt FROM retailers_logs WHERE retailer_id = $ret_id AND date >= '$from' AND date <= '$to'");
                    $sale = $sale['0']['0']['amt'];

                    if ($sale >= $target) {
                        /* if($from > $from_ret1){
                          $sms = "RENTAL MESSAGE\nTotal sale of last month: Rs$sale\nTotal Monthly rental: Rs$rental\nTotal Waiver: Rs$rental\nAmount Charged: Rs0";
                          }
                          else {
                          $sms = "RENTAL MESSAGE\nTotal sale from $from_ret to $to: Rs$sale\nTotal Monthly rental: Rs$rental\nTotal Waiver: Rs$rental\nAmount Charged: Rs0";
                          }
                         */
                        $rental = 0;
                    } else {
                        if ($from > $from_ret1) {
//                            $sms = "RENTAL MESSAGE\nTotal sale of last month: Rs $sale\nTotal Monthly rental: Rs $rental\nTotal Waiver: Rs0\nAmount Charged: Rs$rental";
//                            $sms .= "\nPlease do sale of Rs $target_amount every month to avoid monthly rental of Rs$rental_amount";
                            
                            $paramdata['FROM_MONTH'] = 'of last month';
                        } else {
//                            $sms = "RENTAL MESSAGE\nTotal sale from $from_ret to $to: Rs$sale\nTotal Monthly rental: Rs$rental\nTotal Waiver: Rs0\nAmount Charged: Rs$rental";
//                            $sms .= "\nPlease do sale of Rs $target_amount every month to avoid monthly rental of Rs$rental_amount";
                       
                            $paramdata['FROM_MONTH'] = 'from'.$from_ret;
                            $paramdata['TO_MONTH'] = 'to'.$to;
                        }
//                      $sms .= "\nPlease do sale of Rs $target_amount every month to avoid monthly rental of Rs$rental_amount";
                        $paramdata['SALE'] = $sale;
                        $paramdata['RENTAL'] = $rental;
                        $paramdata['TARGET_AMOUNT'] = $target_amount;
                        $paramdata['RENTAL_AMOUNT'] = $rental_amount;  
                        $content =  $MsgTemplate['Retailer_RentalCut_Monthly_MSG'];
                        $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                    }
                } else {
//                    $sms = "RENTAL MESSAGE\nDear Retailer, Your rental from $from_ret to $to of Rs$rental is deducted from your account.";
                    
                    $paramdata['FROM_RET'] = $from_ret;
                    $paramdata['TO'] = $to;
                    $paramdata['RENTAL'] = $rental;  
                    $content =  $MsgTemplate['Retailer_RentalCut_Deducted_MSG'];
                    $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                }

                if ($rental > 0) {
                    $this->General->logData($_SERVER['DOCUMENT_ROOT'] . "/abc.txt", "Cutting rental of " . $ret['retailers']['mobile']);
                    if ($this->Shop->getMemcache("rental" . $ret['retailers']['id']) == 1)
                        continue;

                    $trans_id = $this->Shop->shopTransactionUpdate(RENTAL, $rental, $ret['retailers']['id'], RETAILER);
                    $bal = $this->Shop->shopBalanceUpdate($rental, 'subtract', $ret['retailers']['id'], RETAILER);
                    $this->Shop->addOpeningClosing($ret['retailers']['id'], RETAILER, $trans_id, $bal + $rental, $bal);
                    $this->Retailer->query("INSERT INTO rentals VALUES (NULL," . $ret['retailers']['id'] . ",$trans_id,$rental_amount,$target_amount,$rental,'$from_ret','$to','" . date('Y-m-d') . "')");
                    $this->Shop->setMemcache("rental" . $ret['retailers']['id'], 1, 5 * 24 * 60 * 60);
                    $this->General->logData($_SERVER['DOCUMENT_ROOT'] . "/abc.txt", "Cut rental of " . $ret['retailers']['mobile'] . ": $sms");
                }
                else {
                    //$this->Retailer->query("INSERT INTO rentals VALUES (NULL,".$ret['retailers']['id'].",NULL,$rental_amount,$target_amount,$rental,'$from_ret','$to','".date('Y-m-d')."')");
                }

                if (!empty($sms)) {
                    echo $ret['retailers']['mobile'] . ":$sms<br/>";
                    $this->General->sendMessage($ret['retailers']['mobile'], $sms, 'notify');
                    usleep(500000);
                }
            }
            $this->Retailer->query("UPDATE retailers SET block_flag = 2, modified = '".date('Y-m-d H:i:s')."' WHERE parent_id = $dist_id AND balance < 0");
        }


        $this->autoRender = false;
    }

    function cleanLogs() {
        $this->Retailer->query("DELETE FROM app_req_log WHERE date <= DATE_SUB(curdate(),INTERVAL 90 DAY)");
        $this->Retailer->query("DELETE FROM ussds WHERE date <= DATE_SUB(curdate(),INTERVAL 30 DAY)");
        $this->Retailer->query("DELETE FROM vendors_messages WHERE Date(timestamp) <= DATE_SUB(curdate(),INTERVAL 60 DAY)");
        $this->Retailer->query("DELETE FROM virtual_number WHERE date <= DATE_SUB(curdate(),INTERVAL 60 DAY)");

        $this->Retailer->query("DELETE FROM vendors_transactions WHERE date <= DATE_SUB(curdate(),INTERVAL 30 DAY)");
        
        
        $this->Retailer->query("DELETE FROM repeated_transactions WHERE Date(timestamp) <= DATE_SUB(curdate(),INTERVAL 15 DAY)");
        $this->Retailer->query("DELETE FROM requests_dropped WHERE Date(timestamp) <= DATE_SUB(curdate(),INTERVAL 30 DAY)");

        $this->Retailer->query("DELETE FROM cc_call_logging WHERE date <= DATE_SUB(curdate(),INTERVAL 10 DAY)");

        $this->Retailer->query("DELETE FROM opening_closing WHERE Date(timestamp) <= DATE_SUB(curdate(),INTERVAL 90 DAY)");
        $this->Retailer->query("DELETE FROM complaints WHERE in_date <= DATE_SUB(curdate(),INTERVAL 60 DAY)");

        $this->Retailer->query("DELETE FROM temp_reversed WHERE date <= DATE_SUB(curdate(),INTERVAL 30 DAY)");
        $this->Retailer->query("DELETE FROM temp_transaction WHERE date <= DATE_SUB(curdate(),INTERVAL 1 DAY)");

        if ($this->Retailer->query("INSERT INTO shop_transactions_logs (SELECT * FROM shop_transactions WHERE date <= DATE_SUB(curdate(),INTERVAL 90 DAY))")) {
            $this->Retailer->query("DELETE FROM shop_transactions WHERE date <= DATE_SUB(curdate(),INTERVAL 90 DAY)");
        }
        
        /*if ($this->Retailer->query("INSERT INTO vendors_activations_logs (SELECT * FROM vendors_activations WHERE date <= DATE_SUB(curdate(),INTERVAL 60 DAY))")) {
            $this->Retailer->query("DELETE FROM vendors_activations WHERE date <= DATE_SUB(curdate(),INTERVAL 60 DAY)");
        }*/
        
        $this->Shop->Memcache->flush();
        $this->autoRender = false;
    }

    /*function offer() {
        $query = "SELECT retailer_id,sum(sale),sum(app_sale),retailers.mobile,retailers.id FROM `retailers_logs`,retailers WHERE retailers.id = retailer_id AND date >= '2013-02-01' group by retailer_id having (sum(app_sale) = 0)";

        $data = $this->Slaves->query($query);
        $sms = "Rs100 Bonus - Last 2 days
Activate Pay1 Mobile App on your phone today & do total business worth Rs5000 via Pay1 App till 16th mar
To know more contact Pay1 CC or your distributor
Download App MissCall 02267242289
Offer Valid till 16/3/13 for this demo no only";
        $mobiles = array();
        $values = array();
        foreach ($data as $dt) {
            $mobile = $dt['retailers']['mobile'];
            $code = $dt['retailers']['id'] . "P" . substr($mobile, -4);

            $mobiles[] = $mobile;
            $values[] = "(NULL,'$mobile','100 - 5000 - APP','$code'," . date('Y-m-d') . "','2013-03-15','" . date('Y-m-d') . "')";
            sleep(1);
        }
        $this->Retailer->query("INSERT INTO offers VALUES " . implode(",", $values));
        $this->General->sendMessage($mobiles, $sms, 'notify');
        $this->autoRender = false;
    }*/

    function proc_cron() {
        $this->Retailer->query("call wall()");
        $this->autoRender = false;
    }

    function proc_cron_ussd() {
        $this->Retailer->query("call removeUSSDLogs()");
        $this->autoRender = false;
    }

    function test() {
        echo "1232";
    	/*$data = $this->Slaves->query("SELECT * FROM vendors");
    	print_r($data);
        $at = array('asd' => 'asasas');
        $key = key($at);
        echo $at[$key];*/
        //echo "1"; exit;

        $this->autoRender = false;
    }

    function incentiveReminderForRetailer() {// send a reminder about incentive to retailer
        $send == false;
        $sms = "";

        if (date('d') == 1) {//On 1st day of month
            $send = true;
        } else if (date('d') == 15) {
            $send = true;
        } else if (date('d') == 22) {
            $send = true;
        } else if (date('d') == 28 && date('m') == 2) {//if month is feb
            $send = true;
        } else if (date('d') == 29) {
            $send = true;
        }
        $MsgTemplate = $this->General->LoadApiBalance(); 
        if ($send) {
            if (date('d') == 1) {// to all retailers ( via app ) only
                $retailers = $this->Slaves->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date > '" . date('Y-m-d', strtotime('-7 days')) . "' group by retailer_id having (sum(retailers_logs.sale) > 0)");
            } else {// to active retailers ( via app ) only
                $retailers = $this->Slaves->query("SELECT retailers.mobile , sum(retailers_logs.app_sale) as total_app_sale FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date > '" . date('Y-m-d', strtotime('- ' . date('d') . ' days')) . "' group by retailer_id having (sum(retailers_logs.app_sale) > 0)");
            }
            foreach ($retailers as $dt) {

                $mobile = $dt['retailers']['mobile'];
                $sale = isset($dt['0']['total_app_sale']) ? $dt['0']['total_app_sale'] : 0;

                if (date('d') == 1) {  // for day 1
//                    $sms = "Dear Retailer,
//Application/Web se 25000 sale par kamaiye 50Rs. bonus aur 50000 par 100Rs. bonus !!
//Aaj hi application download kare, misscall on 02267242289";
                   
                  $content =  $MsgTemplate['Retailer_IncentiveReminder_forDay1_MSG'];     
                
                } else if ($sale < 25000) { // if retailer's SALE < 25000
//                    $sms = "Dear Retailer,
//Is month ka application sale hai Rs $sale. 25000 poore karne par milega 50Rs. aur 50000 par 100Rs. bonus !!";
                  
                  $paramdata['SALE'] = $sale;
                  $content =  $MsgTemplate['Retailer_IncentiveReminder_forSale25K_MSG'];
                    
                } else if (25000 <= $sale && $sale < 50000) { // if retailer's sale  25000 < SALE < 50000
//                    $sms = "Dear Retailer,
//Is month ka application sale hai Rs $sale. Aap already 25000 ka target complete kar chuke ho, 50000 complete karne par 100Rs. bonus paiye !!";
                	    
		  $paramdata['SALE'] = $sale;
                  $content =  $MsgTemplate['Retailer_IncentiveReminder_forSale50K_MSG'];
                  
                }
                $sms = $this->General->ReplaceMultiWord($paramdata,$content);  
                if (!empty($sms)) {

                    $this->General->sendMessage($mobile, $sms, 'notify');
                }
            }
        }
        $this->autoRender = false;
    }

    function rentalReminderForRetailer() {

        $retailers = $this->Slaves->query("SELECT retailers.mobile , sum(retailers_logs.app_sale) as total_app_sale FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '" . date('Y-m-d', strtotime('-4 days')) . "' group by retailer_id having (sum(retailers_logs.app_sale) > 0)");
        $MsgTemplate = $this->General->LoadApiBalance(); 
        foreach ($retailers as $dt) {

            $mobile = $dt['retailers']['mobile'];
            $sale = $dt['0']['total_app_sale'];

            if ($sale < 25000) { // if retailer's SALE < 25000
                $diff = 25000 - $sale;
//                $sms = "Dear Retailer,
//Apki monthly sale $sale Rs. ho chuki hai. !!
//Agar aap 25000 tak sale puri krte hai to aap apka 50 Rs. rental 0 kar skate hai !!";
            
                $paramdata['SALE'] = $sale;
                $content =  $MsgTemplate['Retailer_RentalReminder_MSG'];
                $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                
                $send == true;
            } else {
                $send == false;
            }

            // send a reminder about avoid rental to retailer
            if (intval(date('d')) > ( intval(date('t')) - 3 )) {//On Last 3 days of the month
                if ($send == true) {
                    $this->General->sendMessage($mobile, $sms, 'notify');
                }
            }
        }
        $this->autoRender = false;
    }

    function killIdleConnections($slave=0) {
    	if($slave==1){
    		$result = $this->Slaves->query("SHOW FULL PROCESSLIST");
    		$sub = "(Slave)";
    		$time = 200;
    	}
    	else {
    		$result = $this->Retailer->query("SHOW FULL PROCESSLIST");
    		$sub = "";
    		$time = 50;
    	}
        
        foreach ($result as $res) {
            $process_id = $res['0']['Id'];
            if ($res['0']['Time'] > $time && date('H') > 8 && date('H') < 23  && $res['0']['Command'] == 'Query') {
            	$mail_body = $process_id . " " . json_encode($res);
                
                $this->General->sendMails("Killing mysql connection$sub", $mail_body, array('ashish@mindsarray.com','pravin@mindsarray.com','nandan@mindsarray.com'), 'mail');
                if($slave == 1){
                	$this->Slaves->query("KILL $process_id");
                }
                else $this->Retailer->query("KILL $process_id");
            }
        }
        $this->autoRender = false;
    }

    /*function misReport($vendor = null, $date = null) {
        $result = $this->Slaves->query("SELECT id FROM vendors WHERE shortForm='$vendor'");
        $vendor_id = $result['0']['vendors']['id'];

        $file = $_SERVER['DOCUMENT_ROOT'] . "/$vendor" . "_" . "$date.csv";

        $query = 'SELECT mobile,param,amount,ref_code,vendor_refid,status,timestamp from vendors_activations where vendor_id=' . $vendor_id . ' AND date="' . $date . '"';
        shell_exec("echo '$query' | mysql --host=" . DB_HOST . " --user=" . DB_USER . " --password=" . DB_PASS . " " . DB_DB . " > $file");

        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        unlink($file);
        $this->autoRender = false;
    }

    function backtrack1($id) {//$type = 0 => sms
        $time_1 = "2013-11-29 18:59:46";
        $time_2 = "2013-12-02 23:00:00";
        $query = "SELECT opening,closing,opening_closing.id FROM `shop_transactions`,opening_closing WHERE date in ('2013-11-29','2013-11-30','2013-12-01','2013-12-02') AND shop_transactions.id = shop_transaction_id AND group_id = 5 AND shop_id = ref1_id AND type =2 AND shop_id = $id AND shop_transactions.timestamp >= '$time_1' AND  shop_transactions.timestamp <= '$time_2'";
        $result = $this->Slaves->query($query);

        foreach ($result as $res) {
            $oc_id = $res['opening_closing']['id'];
            $opening = $res['opening_closing']['opening'];
            $closing = $res['opening_closing']['closing'];

            $amount = $opening - $closing;
            $opening = $closing;
            $closing = $closing - $amount;
            $this->Retailer->query("UPDATE opening_closing SET opening='$opening',closing='$closing' WHERE id = '$oc_id' ");
            usleep(1000);
        }
        $this->autoRender = false;
    }

    function correctOldEntries() {
        $i = 0;
        $max = 50;
        while ($i < $max) {
            $data = $this->Slaves->query("SELECT GROUP_CONCAT(st1.id) as ids FROM `shop_transactions` as st1 inner join shop_transactions as st2 ON (st2.id = st1.ref2_id AND st2.date != st1.date) WHERE st1.`type` = 11 AND st1.`date` = '" . date('Y-m-d', strtotime('- ' . $i . ' days')) . "'");

            $ids = $data['0']['0']['ids'];
            if (!empty($ids)) {
                $this->Retailer->query("UPDATE shop_transactions SET type_flag = 1 WHERE id in ($ids)");
            }
            $i++;
        }
        $this->autoRender = false;
    }

    function correctOldEntries1() {
        $i = 0;
        $max = 50;
        while ($i < $max) {
            $data = $this->Slaves->query("SELECT SUM(amount) as reversal FROM shop_transactions WHERE type_flag = 1 AND date='" . date('Y-m-d', strtotime('- ' . $i . ' days')) . "' AND type =11");
            $comm = $this->Slaves->query("SELECT SUM( IF( TYPE =6 AND distributors.parent_id = 3, amount, 0 ) ) AS comm_d, SUM( IF( TYPE =5, amount, 0 ) ) AS comm_sd, SUM( IF( (confirm_flag =1 OR type_flag = 1) AND TYPE =4, amount * discount_comission /100, 0 ) ) AS comm_r FROM shop_transactions LEFT JOIN distributors ON (distributors.id = ref1_id AND distributors.parent_id = 3) WHERE DATE =  '" . date('Y-m-d', strtotime('- ' . $i . ' days')) . "' AND TYPE IN ( 4, 5, 6 )");

            $reversals = intval($data['0']['0']['reversal']);

            $comm_tot = intval($comm['0']['0']['comm_d'] + $comm['0']['0']['comm_sd'] + $comm['0']['0']['comm_r']);


            $this->Retailer->query("UPDATE float_logs SET commissions='$comm_tot',old_reversals='$reversals' WHERE date='" . date('Y-m-d', strtotime('- ' . $i . ' days')) . "' AND hour = 24");
            $i++;
        }
        $this->autoRender = false;
    }

    function plansDetails($vendorsStr = null) {
        //$filePath = "d:/plans/";//for windows

        $filePath = ROOT . "/plans/"; //for linux @TODO :- create a folde plans  and change folder permissions
        $filePath = addslashes($filePath);
         $url = 'http://static9.mobikwik.com/views/js/ui/plans.js';
          $file = file_get_contents($url);
          try{
          //echo $file;
          $fp = fopen("d:/palns.js", 'w');
          fwrite($fp, $file);
          }catch(Exception $e){
          print_r($e);
          }

          $arr = explode("var ", $file);

          $varArr = array();$a = 1;
          foreach($arr as $key => $val){
          $val = trim($val);
          if(!empty($val)){
          $arrEql = explode("=", trim($val) , 2);
          $arrEql[1] = trim($arrEql[1]);
          $arrEql[1] = str_replace("'", "\"", $arrEql[1]);
          $varArr[trim($arrEql[0])] = $arrEql[1];
          }

          } 

        $fpCronLog = fopen($filePath . "cronLog" . date("d-m-Y") . ".txt", 'w');

        fwrite($fpCronLog, "\n**********  Getting Plan Details ( " . $vendorsStr . " ) date - " . date("d-m-Y H:i:s") . " ********** ");

        $oprArr = array(
                "Aircel" => array("code" => "Aircel", "freecharge_code" => 1, "product_code" => 1, "mobiq_code" => 6, "type" => "mobile", "source" => "freecharge"),
                "Airtel" => array("code" => "Airtel", "freecharge_code" => 2, "product_code" => 2, "mobiq_code" => 1, "type" => "mobile", "source" => "freecharge"),
                "BSNL" => array("code" => "BSNL", "freecharge_code" => 3, "product_code" => 3, "mobiq_code" => 3, "type" => "mobile", "source" => "freecharge"),
                //--"BSNL NET" => array("code" => "", "product_code" => 46,"type" => "mobile","source"=>"paytm"), //new
                "Idea" => array("code" => "Idea", "freecharge_code" => 6, "product_code" => 4, "mobiq_code" => 8, "type" => "mobile", "source" => "freecharge"),
                //--"Idea NET" => array("code" => "", "product_code" => 47,"type" => "mobile","source"=>"paytm"), //new
                //"Loop Mobile" => array("code" => "Loop Mobile", "freecharge_code" => 8, "product_code" => 5, "mobiq_code" => 10, "type" => "mobile", "source" => "freecharge"),
                "MTNL DELHI" => array("code" => "MTNL", "freecharge_code" => '9', "product_code" => 30, "type" => "mobile", "source" => "freecharge"),
                "MTNL MUMBAI" => array("code" => "MTNL", "freecharge_code" => '54', "product_code" => 30, "type" => "mobile", "source" => "freecharge"),
                "MTS" => array("code" => "MTS", "freecharge_code" => 22, "product_code" => 6, "mobiq_code" => 13, "type" => "mobile", "source" => "freecharge"),
                //--"MTS NET" => array("code" => "", "product_code" => 49,"type" => "mobile","source"=>"paytm"), //new
                "Reliance CDMA" => array("code" => "Reliance CDMA", "freecharge_code" => 10, "product_code" => 7, "mobiq_code" => 4, "type" => "mobile", "source" => "freecharge"),
                "Reliance GSM" => array("code" => "Reliance GSM", "freecharge_code" => 11, "product_code" => 8, "mobiq_code" => 5, "type" => "mobile", "source" => "freecharge"),
                //--"Reliance NET" => array("code" => "", "product_code" => 50,"type" => "mobile","source"=>"paytm"), //new
                "Tata Docomo" => array("code" => "Tata Docomo GSM", "freecharge_code" => 5, "product_code" => 9, "mobiq_code" => 11, "type" => "mobile", "source" => "freecharge"),
                "Tata Indicom" => array("code" => "Tata Docomo CDMA", "freecharge_code" => 12, "product_code" => 10, "mobiq_code" => 9, "type" => "mobile", "source" => "freecharge"),
                //--"TATA PHOTON" => array("code" => "", "product_code" => 51,"type" => "mobile","source"=>"paytm"), //new
                "Uninor" => array("code" => "Uninor", "freecharge_code" => 13, "product_code" => 11, "mobiq_code" => 16, "type" => "mobile", "source" => "freecharge"),
                "Videocon" => array("code" => "Videocon", "freecharge_code" => 24, "product_code" => 12, "mobiq_code" => 17, "type" => "mobile", "source" => "freecharge"),
                //"Virgin CDMA" => array("code" => "Virgin CDMA", "freecharge_code" => 12, "product_code" => 13, "type" => "mobile", "source" => "freecharge"),
                //"Virgin GSM" => array("code" => "Virgin GSM", "freecharge_code" => 30, "product_code" => 14, "type" => "mobile", "source" => "freecharge"),
                "Vodafone" => array("code" => "Vodafone", "freecharge_code" => 14, "product_code" => 15, "mobiq_code" => 2, "type" => "mobile", "source" => "freecharge"),
                //paytm dth product code mapping
                "Airtel DTH" => array("code" => "Airtel Digital TV", "product_code" => 16, "mobiq_code" => 23, "type" => "dth", "source" => "mobikwik"),
                "Dish TV" => array("code" => "Dish TV", "product_code" => 18, "mobiq_code" => 18, "type" => "dth", "source" => "mobikwik"),
                "Reliance DTH" => array("code" => "Reliance Digital TV", "product_code" => 17, "mobiq_code" => 20, "type" => "dth", "source" => "mobikwik"),
                //"Sun DTH" => array("code" => "Sun Direct", "product_code" => 19, "type" => "dth","source"=>"paytm"),
                "Tata Sky DTH" => array("code" => "Tata Sky", "product_code" => 20, "mobiq_code" => 19, "type" => "dth", "source" => "mobikwik"),
                "Videocon Dth" => array("code" => "Videocon d2h", "product_code" => 21, "mobiq_code" => 21, "type" => "dth", "source" => "mobikwik"),
        );


        $finalArr = array();
        //$circleArr = json_decode($varArr['CIRCLES'], true);

        $circleData = Array(
                4 => Array(
                        "type" => "Metros",
                        "id" => 4,
                        "frid" => 5,
                        "name" => "Chennai",
                        "code" => "CH",
                        "paytm_code" => "Chennai",
                        "freecharge_circle" => 5
                ),
                5 => Array
                        (
                        "type" => "Metros",
                        "id" => 5,
                        "frid" => 6,
                        "name" => "Delhi NCR",
                        "code" => "DL",
                        "paytm_code" => "Delhi NCR",
                        "freecharge_circle" => 6
                ),
                12 => Array
                        (
                        "type" => "Metros",
                        "id" => 12,
                        "frid" => 12,
                        "name" => "Kolkata",
                        "code" => "KO",
                        "paytm_code" => "Kolkata",
                        "freecharge_circle" => 12
                ),
                15 => Array
                        (
                        "type" => "Metros",
                        "id" => 15,
                        "frid" => 14,
                        "name" => "Mumbai",
                        "code" => "MU",
                        "paytm_code" => "Mumbai",
                        "freecharge_circle" => 15
               ),
                1 => Array
                        (
                        "type" => "States",
                        "id" => 1,
                        "frid" => 1,
                        "name" => "Andhra Pradesh",
                        "paytm_code" => "Andhra Pradesh",
                        "code" => "AP",
                        "freecharge_circle" => 1
                ),
                16 => Array
                        (
                        "type" => "States",
                        "id" => 16,
                        "frid" => 16,
                        "name" => "Tripura", //NA
                        "paytm_code" => "North East",
                        "code" => "NE",
                        "freecharge_circle" => 16
                ),
                2 => Array
                        (
                        "type" => "States",
                        "id" => 2,
                        "frid" => 2,
                        "name" => "Assam",
                        "paytm_code" => "Assam",
                        "code" => "AS",
                        "freecharge_circle" => 2
                ),
                3 => Array
                        (
                        "type" => "States",
                        "id" => 3,
                        "frid" => 7,
                        "name" => "Jharkhand",
                        "paytm_code" => "Bihar Jharkhand",
                        "code" => "BR",
                        "freecharge_circle" => 3
                ),
                14 => Array
                        (
                        "type" => "States",
                        "id" => 14,
                        "frid" => 13,
                        "name" => "Madhya Pradesh",
                        "paytm_code" => "Madhya Pradesh Chhattisgarh",
                        "code" => "MP",
                        "freecharge_circle" => 13
                ),
                13 => Array
                        (
                        "type" => "States",
                        "id" => 13,
                        "frid" => 14,
                        "name" => "Maharashtra",
                        "paytm_code" => "Maharashtra",
                        "code" => "MH",
                        "freecharge_circle" => 14
                ),
                6 => Array
                        (
                        "type" => "States",
                        "id" => 6,
                        "frid" => 7,
                        "name" => "Gujarat",
                        "paytm_code" => "Gujarat",
                        "code" => "GJ",
                        "freecharge_circle" => 6
                ),
                7 => Array
                        (
                        "type" => "States",
                        "id" => 7,
                        "frid" => 16,
                        "name" => "Haryana",
                        "paytm_code" => "Haryana",
                        "code" => "NE",
                        "freecharge_circle" => 7
                ),
                8 => Array
                        (
                        "type" => "States",
                        "id" => 8,
                        "frid" => 9,
                        "name" => "Himachal Pradesh",
                        "paytm_code" => "Himachal Pradesh",
                        "code" => "HP",
                        "freecharge_circle" => 8
                ),
                9 => Array
                        (
                        "type" => "States",
                        "id" => 9,
                        "frid" => 34,
                        "name" => "Jammu & Kashmir",
                        "paytm_code" => "Jammu Kashmir",
                        "code" => "JK",
                        "freecharge_circle" => 34
                ),
                10 => Array
                        (
                        "type" => "States",
                        "id" => 10,
                        "frid" => 10,
                        "name" => "Karnataka",
                        "paytm_code" => "Karnataka",
                        "code" => "KA",
                        "freecharge_circle" => 10
                ),
                11 => Array
                        (
                        "type" => "States",
                        "id" => 11,
                        "frid" => 11,
                        "name" => "Kerala",
                        "paytm_code" => "Kerala",
                        "code" => "KL",
                        "freecharge_circle" => 11
                ),
                17 => Array
                        (
                        "type" => "States",
                        "id" => 17,
                        "frid" => 7,
                        "name" => "Orissa",
                        "paytm_code" => "Orissa",
                        "code" => "OR",
                        "freecharge_circle" => 17
                ),
                18 => Array
                        (
                        "type" => "States",
                        "id" => 18,
                        "frid" => 18,
                        "name" => "Punjab",
                        "paytm_code" => "Punjab",
                        "code" => "PB",
                        "freecharge_circle" => 18
                ),
                19 => Array
                        (
                        "type" => "States",
                        "id" => 19,
                        "frid" => 19,
                        "name" => "Rajasthan",
                        "paytm_code" => "Rajasthan",
                        "code" => "RJ",
                        "freecharge_circle" => 19
                ),
                23 => Array
                        (
                        "type" => "States",
                        "id" => 23,
                        "frid" => 23,
                        "name" => "West Bengal",
                        "paytm_code" => "West Bengal",
                        "code" => "WB",
                        "freecharge_circle" => 23
                ),
                20 => Array
                        (
                        "type" => "States",
                        "id" => 20,
                        "frid" => 20,
                        "name" => "Tamil Nadu",
                        "paytm_code" => "Tamil Nadu",
                        "code" => "TN",
                        "freecharge_circle" => 20
                ),
                21 => Array
                        (
                        "type" => "States",
                        "id" => 21,
                        "frid" => 21,
                        "name" => "Uttar Pradesh (East)",
                        "paytm_code" => "UP East",
                        "code" => "UE",
                        "freecharge_circle" => 21
                ),
                22 => Array
                        (
                        "type" => "States",
                        "id" => 22,
                        "frid" => 22,
                        "name" => "Uttarakhand",
                        "paytm_code" => "-", //NA
                        "code" => "UW",
                        "freecharge_circle" => 22
                ),
        );

 

        $finalArr['CIRCLES'] = $circleData;
        $finalArr['OPERATORS'] = $oprArr;
        try {
            //$fp = fopen('d:/dataInsertFile.txt','w');
            $cnt = 0;
            $x = 0;
            $fpnp = fopen($filePath . "plansNotFound.txt", 'w');
            $fpInsert = fopen($filePath . 'dataInsertFile.txt', 'w');
            $fpInsertvalue = fopen($filePath . 'newValuesInsertFile.txt', 'w');

           //$out = $this->getFreeChargeplans(89, '15');
            foreach ($finalArr['OPERATORS'] as $opr => $oprArr) { // iteration for operators
                //echo $oprArr['code']."-";
                $dthContinue = false;
                foreach ($finalArr['CIRCLES'] as $cid => $cArr) { // iteration for circles
                    //echo $cid.",";
                    
                	if($oprArr['freecharge_code'] == '9' && $cid != '5')continue;
                	if($oprArr['freecharge_code'] == '54' && $cid != '15')continue;
                	
                    if ($dthContinue) {
                        continue;
                    }

                    if ($oprArr['type'] == "dth") {
                        $dthContinue = true;
                        $cArr['name'] = "";
                        $cid = 0;
                    }

                    echo $pstr = "\n Processing  Operator = $opr(" . $oprArr['code'] . ") ,  Circle = " . $cArr['name'] . " ( " . $cid . " )  ..... ";
                    fwrite($fpCronLog, $pstr);

                    if ($vendorsStr == "getFromApi") {
                        if ($oprArr['source'] == "freecharge") {
                            if ($oprArr['type'] == 'mobile') {
                                $out = $this->getFreeChargeplans($oprArr['freecharge_code'], $cArr['freecharge_circle']);
                                $source = "freecharge";
                            }
                        } else {
                            echo "\nGetting data from mobikwik2" . $oprArr['code'];
                            $out = CronsController::getPlans($oprArr['mobiq_code'], $cid);
                            $source = "mobikwik";
                        }
                    } else {

                        $filename = $filePath . "plans_" . $oprArr['code'] . "-$cid.txt";
                        $fp = fopen($filename, 'r');
                        $out = fread($fp, filesize($filename));
                        $outJson = json_decode($out, true);
                        $source = key($outJson);
                        $outJson = $outJson[$source];
                        fclose($fp);
                    }

                    if (!empty($out) && $out != "{\"failure\":false,\"data\":\"Plans not found\",\"description\":\"Plan List.\"}" && $out != "[]") {
                        if ($vendorsStr == "getFromApi") {

                            $fp = fopen($filePath . "plans_" . $oprArr['code'] . "-$cid.txt", 'w');
                            $outJson = json_decode($out, true);
                            if ($oprArr['type'] == 'mobile' && $source == "freecharge") {
                                if(!empty($outJson['ALL_RECHARGE_PLANS']))
                                fwrite($fp, json_encode(array($source => $outJson['ALL_RECHARGE_PLANS'])));
                            } else {
                                if(!empty($outJson))
                                fwrite($fp, json_encode(array($source => $outJson)));
                            }
                            fclose($fp);
                        }

                        $cnt++;
                        $dataArray = array();
                        $datafromtable = array();
                        if ($source == "mobikwik") {
                            foreach ($outJson['data'] as $type => $typeData) { // iteration for plan type
                                if (count($typeData) > 0 && is_array($typeData)) {
                                    foreach ($typeData as $key => $data) {// iteration for plans
                                        if ($oprArr['type'] == "dth") {
                                            $str = "(" . "\"null\",\"" . "0" . "\",\"" . "all" . "\",\"" . "all" . "\",\"" . "all" . "\",\"" . $oprArr['code'] . "\",\"" . $oprArr['product_code'] . "\",\"" . $opr . "\",\"" . $type . "\",\"" . $data['amount'] . "\",\"" . $data['validity'] . " days" . "\",\"" . addslashes($data['description']) . "\",'0','" . date('Y-m-d H:i:s') . "'),\n";
                                        }
                                        $dataArray[$type . "_" . $data['amount']] = array("prod_code_pay1" => $oprArr['product_code'], "plan_type" => $type, "plan_amt" => $data['amount'], "plan_validity" => $data['validity'], "plan_desc" => $data['description']);
                                        //if(!empty($str))
                                        //fwrite($fpInsert, $str);
                                    }
                                }
                            }
                        } else if ($source == "freecharge") {
                            foreach ($outJson as $key => $data) {// iteration for plans
                                if (count($data) > 0 && is_array($data)) {
                                    foreach ($data as $datakey => $dataval) {
                                        $str = "(" . "\"null\",\"" . $cid . "\",\"" . $cArr['code'] . "\",\"" . $cArr['type'] . "\",\"" . $cArr['name'] . "\",\"" . $oprArr['code'] . "\",\"" . $oprArr['product_code'] . "\",\"" . $opr . "\",\"" . $dataval['name'] . "\",\"" . $dataval['amount'] . "\",\"" . $dataval['validity'] . "\",\"" . addslashes($dataval['description']) . "\",'0','" . date('Y-m-d H:i:s') . "'),\n";
                                        $dataArray[$dataval['name'] . "_" . $dataval['amount']] = array("prod_code_pay1" => $oprArr['product_code'], "plan_type" => $dataval['name'], "plan_amt" => $dataval['amount'], "plan_validity" => $dataval['validity'], "plan_desc" => $dataval['description']);
                                       // if(!empty($str))
                                       // fwrite($fpInsert, $str);
                                    }
                                }
                            }
                        }
						
						

                        if ($cid == '' && $oprArr['type'] == 'dth') {
                            $circle_id = 0;
                        } else {
                            $circle_id = $cid;
                        }
                        $getData = $this->Retailer->query("SELECT id, prod_code_pay1,plan_type,plan_amt,plan_validity,plan_desc FROM `circle_plans` WHERE prod_code_pay1 = '" . $oprArr['product_code'] . "' and c_id='" . $circle_id . "'");
                        if (count($getData) > 0) {
                            foreach ($getData as $key => $value) {
                                foreach ($value as $k => $v) {
                                    $datafromtable[$v['plan_type'] . "_" . $v['plan_amt']] = $v;
                                }
                            }
                        }
                        $datadiff = array();
                        $diffValuefromtable = array();
                        $dataId = array();
                        if (empty($dataArray) && !empty($datafromtable)) {
                            $this->General->sendMails("Plans Data not found",$oprArr['code'].":".$cid, array("pravin@mindsarray.com"), 'mail');
                        } else {
                            $datadiff = $this->arrayRecursiveDiff($dataArray, $datafromtable);
                            $diffValuefromtable = $this->arrayRecursiveDiff($datafromtable, $dataArray);  //

                            if (count($diffValuefromtable) > 0) {
                                foreach ($diffValuefromtable as $keyvalue => $diffvalue) {
                                    if (count($diffvalue) > 1) {
                                        foreach ($diffvalue as $key => $value) {
                                            if ($key == "id") {
                                                $datadiff[$keyvalue]['id'] = $value;
                                            }
                                        }
                                    }
                                }
                            }
							
							
                            
                            unset($datafromtable);
                            if (!count($datadiff)) {
                                //$updateFlag = $this->Retailer->query("update  `circle_plans` set show_flag ='1' WHERE prod_code_pay1 = '" . $oprArr['product_code'] . "' and c_id='" . $cid . "'");
                            } elseif (count($datadiff) > 0) {
                                foreach ($datadiff as $datadiffkey => $datadiffval) {
                                    if (isset($datadiffval['id'])) {
                                        $getqueryData = $this->Retailer->query("SELECT `id` FROM `circle_plans` WHERE id = '" . $datadiffval['id'] . "'");
                                        if (count($getqueryData) > 0) {
                                            foreach ($datadiffval as $dkey => $dvalue) {
                                                if (count($datadiffval) > 1) {
                                                    if ($dkey != 'id') {
                                                        $sql = $this->Retailer->query("update `circle_plans` set $dkey = '" . $dvalue . "' ,show_flag ='1',updated='" . date('Y-m-d H:i:s') . "' WHERE id = '" . $getqueryData[0]['circle_plans']['id'] . "'");
                                                    }
                                                } else {
                                                    $sql = $this->Retailer->query("update `circle_plans` set show_flag ='0',updated='" . date('Y-m-d H:i:s') . "' WHERE id = '" . $dvalue . "'");
                                                }
                                            }
                                        }
                                    } else {
                                        if ($oprArr['type'] == "dth") {
                                            $cArr['code'] = "all";
                                            $cArr['type'] = "all";
                                            $cArr['name'] = "all";
                                        }
                                        unset($datadiff[$datadiffkey]['id']);
                                        $newValues = "(" . "\"null\",\"" . $cid . "\",\"" . $cArr['code'] . "\",\"" . $cArr['type'] . "\",\"" . $cArr['name'] . "\",\"" . $oprArr['code'] . "\",\"" . $oprArr['product_code'] . "\",\"" . $opr . "\",\"" . $datadiff[$datadiffkey]['plan_type'] . "\",\"" . $datadiff[$datadiffkey]['plan_amt'] . "\",\"" . $datadiff[$datadiffkey]['plan_validity'] . "\",\"" . addslashes($datadiff[$datadiffkey]['plan_desc']) . "\",'1','" . date('Y-m-d H:i:s') . "'),\n";
                                        fwrite($fpInsertvalue, $newValues);
                                        unset($datadiffkey);
                                        unset($datadiffval);
                                    }
                                }
                            }
                            unset($datadiff);
                            unset($diffValuefromtable);
                            unset($dataId);
                            unset($dataArray);
                        }
                    } else {
                        fwrite($fpnp, $oprArr['code'] . "," . $cid . "\n");
                        fwrite($fpCronLog, "...Data Not Found...");
                    }

                    $x++;
                    echo $pstr = "Complete \n";
                    fwrite($fpCronLog, $pstr);
                    if ($vendorsStr == "getFromApi") {
                   
                    $filegetNewValue = file_get_contents($filePath . "/newValuesInsertFile.txt");
			        if (!empty($filegetNewValue)) {
			            $filegetNewValue = "VALUES" . trim($filegetNewValue);
			            $filegetNewValue = substr_replace($filegetNewValue, "", -1, 1);
			            $insert = $this->Retailer->query("INSERT INTO circle_plans $filegetNewValue ");
			            file_put_contents($filePath . "/newValuesInsertFile.txt", '');
			        }
                    //sleep(2);
                    }
                     
                
                   
                    
                }
                if ($x >= 10) {//$cnt >= 2 ||
                    //break;
                }
            }
            //fclose($fpInsert);
            //fclose($fpnp);
            echo $pstr = "\n------------------ Getting Plans Complete --------------\n";
            fwrite($fpCronLog, $pstr);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        
         
        
       $filegetcontent = file_get_contents($filePath . "/dataInsertFile.txt");
        $filegetcontent = "VALUES" . trim($filegetcontent);
        $filegetcontent = substr_replace($filegetcontent, "", -1, 1);
        $insert = $this->Retailer->query("INSERT INTO circle_plans $filegetcontent ");

        $this->autoRender = false;
    }

    public function getPaytmPlans($operator, $circle, $type) {
        //https://paytm.com/plans?operator=Airtel%2520Digital%2520TV&type=dth
        //return "Inside ";
        $operator = urlencode($operator);
        $circle = urlencode($circle);
        $postString = "operator=$operator&type=$type" . ( $type == "dth" ? "" : "&circle=$circle" );
        $url = "https://paytm.com/plans?" . $postString;

        $str = $this->General->curl_post($url, array(), 'GET');

         $filePath = ROOT . "/plans/";//for linux @TODO :- create a folde plans  and change folder permissions
          $filePath = addslashes($filePath);
          $fpInsert = fopen($filePath . 'urlCalledPaytm.txt', 'a+');
          fwrite($fpInsert, "\n".$url);
          fclose($fpInsert);

          $str = CronsController::paytmCurlGet($url, $postString); 
        //$this->printArray($str);
        if ($str['success'])
            return $str['output'];
        else
            return '';
    }

    public static function getPlans($operator, $circle) {
        //return "Inside ";
        $postString = "action=showplansnew&circle=$circle&operator=$operator&plantype=0";
        $url = "https://www.mobikwik.com/nm/mobileDataVerify.jsp";
        $out = CronsController::mobikwikCurlPost($url, $postString);
        if (strlen($out) != 64) {
            // Email to admin  : "Failed to collect correct checksum from mobikwik";
        }
        $postString = "action=showplansnew&checksum=" . $out . "&circle=$circle&operator=$operator&plantype=0&mid=MBK4838";
        $url = "https://www.mobikwik.com/wallet.do";
        $str = CronsController::mobikwikCurlPost($url, $postString);

        return $str;
    }

    public static function mobikwikCurlPost($url, $postString) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "X-Requested-With: XMLHttpRequest",
                "Referer: https://www.mobikwik.com/"));
        $out = trim(curl_exec($ch));
        curl_close($ch);
        return $out;
    }

    public static function paytmCurlGet($url, $postString) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9;q=0.8",
                "Accept-Language: en-US,en;q=0.5",
                "Host: paytm.com",
                )
        ); //"X-Requested-With: XMLHttpRequest",
        $out = trim(curl_exec($ch));
        curl_close($ch);
        return $out;
    }

    public function getFreeChargeplans($operator, $circle) {

        $url = "https://www.freecharge.in/rest/plans/" . $operator . "/" . $circle . "/Mobile";
        $str = $this->General->curl_post($url, array(), 'GET');
        
        if ($str['output']) {
           return $str['output'];
            } else {
                return '';
            }
        
        }*/
    

    /* function sendGcmNotification($jsonStr = null) {// only for testing purpose
      $this->autoRender = FALSE;
      $message = "Hello";
      $response = $this->General->sendMessage("9769597418",$message,"notify",$extra=null);//("9892471156", "APA91bHhNAGO9s5NzMEsIw06TWD3G5CuaxePA4gSz3OuuAUP8c5pDzHpF2WNrdg4tfoxG8AskW0yyADMDXajtf2v6tfBPadr9aW8Zfl0LudKyTD7sViJ9ERLDTbqG_XCKyHamQRzCFTRAt8bR0vePI7SbCekI8kM9g", $message, "notification");
      echo $response;
      } */

    function issueShowOnBanner() {
        $this->autoRender = false;
        $data = $this->Slaves->query("
                   SELECT 
                        count(vendors_activations.id) as ids, 
                        vendors_activations.vendor_id,
                        vendors_activations.product_id,
                        products.name,
                        vendors.shortForm,
                        sum(if( vendors_activations.status != 3,amount,0)) as sale,
                        sum(if(vendors_activations.status =0,1,0)) as process,
                        sum(if(vendors_activations.status =2 OR vendors_activations.status =3,1,0)) as failure,
                        vendors.update_flag 
                   FROM 
                        `vendors_activations`,
                        products,
                        vendors 
                   WHERE 
                        products.id = vendors_activations.product_id 
                   AND 
                        vendors.id = vendors_activations.vendor_id 
                   AND 
                        vendors_activations.date = '" . date('Y-m-d') . "' 
                   AND 
                        vendors_activations.timestamp <= '" . date('Y-m-d H:i:s', strtotime('-1 minutes')) . "' 
                   AND  
                        vendors_activations.timestamp >= '" . date('Y-m-d H:i:s', strtotime('-5 minutes')) . "'                   
                   GROUP BY 
                        vendors_activations.product_id,vendors_activations.vendor_id 
                   ORDER BY 
                        vendors_activations.product_id"
        );


        $prods = array();
        $vendorCurConsmption = array();
        foreach ($data as $dt) {
            if (empty($prods[$dt['vendors_activations']['product_id']])) {
                $prods[$dt['vendors_activations']['product_id']]['total'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['process'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['failure'] = 0;
                //$prods[$dt['vendors_activations']['product_id']]['active'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['success'] = 0;
                $prods[$dt['vendors_activations']['product_id']]['sale'] = 0;
            }

            $prods[$dt['vendors_activations']['product_id']]['total'] += $dt['0']['ids'];
            $prods[$dt['vendors_activations']['product_id']]['process'] += $dt['0']['process'];
            $prods[$dt['vendors_activations']['product_id']]['failure'] += $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['success'] += $dt['0']['ids'] - $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['sale'] += $dt['0']['sale'];

            $prods[$dt['vendors_activations']['product_id']]['name'] = $dt['products']['name']; //$dt['vendors_activations']['name']; //@TODO1


            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['total'] = $dt['0']['ids'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['process'] = $dt['0']['process'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['failure'] = $dt['0']['failure'];

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['sale'] = $dt['0']['sale'];

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['success'] = $dt['0']['ids'] - $dt['0']['failure'];
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['vendor'] = $dt['vendors']['shortForm']; //$dt['vendors_activations']['shortForm'];//@TODO1
            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['vendor_id'] = $dt['vendors_activations']['vendor_id']; //$dt['vendors_activations']['vendor_id'];//@TODO1

            $prods[$dt['vendors_activations']['product_id']]['vendors'][$dt['vendors_activations']['vendor_id']]['modem_flag'] = $dt['vendors']['update_flag']; //$dt['vendors_activations']['update_flag'];//@TODO1
        }

        $allDet = $prods;
        $array = array();
        $data = $this->Slaves->query("
                    SELECT 
                        products.id,
                        products.name,
                        vendors_commissions.vendor_id,
                        vendors.update_flag 
                    FROM 
                        products,
                        vendors_commissions,
                        vendors 
                    WHERE 
                        product_id = products.id 
                    AND 
                        vendors_commissions.vendor_id = vendors.id 
                    AND vendors_commissions.active = 1 
                    AND monitor = 1"
        );

        foreach ($data as $prod) {
            if (!isset($prods[$prod['products']['id']])) {
                $array['no'][] = $prod['products']['name'];
            } else if (($prods[$prod['products']['id']]['total'] >= 5 && $prods[$prod['products']['id']]['failure'] * 100 / $prods[$prod['products']['id']]['total'] >= 10) || $prods[$prod['products']['id']]['failure'] * 100 / $prods[$prod['products']['id']]['total'] >= 50) {
                $array['failure'][$prod['products']['id']] = $prods[$prod['products']['id']];
            } else if ($prods[$prod['products']['id']]['total'] >= 20 && $prods[$prod['products']['id']]['process'] * 100 / $prods[$prod['products']['id']]['total'] >= 25) {
                $array['process'][$prod['products']['id']] = $prods[$prod['products']['id']];
            }

            $active_vendor = $prod['vendors_commissions']['vendor_id'];
            $prods[$prod['products']['id']]['active_total'] = isset($prods[$prod['products']['id']]['vendors'][$active_vendor]['total']) ? $prods[$prod['products']['id']]['vendors'][$active_vendor]['total'] : 0;
        }

        $mail_body = "";
        if (!empty($array['no'])) {
            $mail_body .= "<b style='font-size:15px;font-weight:bold;color:blue;' >No transactions :</b>";
            foreach ($array['no'] as $val) {
                $mail_body .= $val . ", ";
            }
            $mail_body .= "</br>";
        }
        //$mail_body ="<b style='font-size:15px;font-weight:bold;color:blue;'> No transactions happening in:</b>Aircel, Idea, Loop, MTS, Reliance CDMA, Reliance GSM, Videocon, Sun TV DTH, Airtel, BSNL,</br>";//

        if (!empty($array['failure'])) {
            $mail_body .= "<b style='font-size:15px;font-weight:bold;color:blue;'>Failures in:</b>";
            foreach ($array['failure'] as $prod => $val) {
                $mail_body .= $val['name'] . "(" . intval($val['failure'] * 100 / $val['total']) . "%), ";
            }
        }
        //echo  $mail_body .="<b style='font-size:15px;font-weight:bold;color:blue;'>Failures :</b>MTS(5%), BSNL(9%), Airtel DTH(6%), Uninor(24%), Aircel(6%), BSNL SV(13%), Airtel(10%), ";//
        $this->Shop->setMemcache('failures', $mail_body);

        $mail_body = addslashes($mail_body);
        $faildata = $this->Retailer->query("update vars set `value` = '$mail_body' WHERE `name` = 'failures'");
        //echo $mail_body;
    }

    function autoShiftPay1Topup() {

    	$url = "https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/Daily_transaction/?url=1&date=".date('Y-m-d');

    	$data = $this->General->curl_post($url,null,'GET',30,30);
    	$info = json_decode($data['output'],true);
    	 
    	//$sale_all = $this->Retailer->query("SELECT SUM(if(confirm_flag = 1 AND type = 4,amount,0)) as amt, SUM(if(type_flag = 1 AND type = 11 ,amount,0)) as reversal FROM shop_transactions WHERE date='" . date('Y-m-d') . "' AND ref2_id = 44 AND type in (4,11)");
    	$balance = $this->Shop->getShopDataById(13,RETAILER);
		$mail_subject = "Balance tfrd to Pay1 B2C Retailer";
    	if(empty($info)){
    		$this->General->sendMails($mail_subject, "Not found any data from b2c", array("tadka@mindsarray.com","sunilr@pay1.in"), 'mail');
    		return;
    	}
    	
    	$money_to_tfr = $info['total_wallet_balnce'] - $balance['balance'];
        if ($money_to_tfr > 0) {
    		$type = 'add';
    	}
    	else {
    		$type = 'subtract';
    	}
    	 
    	 
            $recId = $this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER, $money_to_tfr, 0, 13, 1);
    	$bal1 = $this->Shop->shopBalanceUpdate($money_to_tfr, $type, 13, RETAILER);
            $this->Shop->addOpeningClosing(13, RETAILER, $recId, $bal1 - $money_to_tfr, $bal1);
    	 

           
    	$mail_body = "Amount transferred: $money_to_tfr<br/>Total balance now is $bal1<br/>Blocked balance is ".$info['total_blocked_amt'];

            $this->General->sendMails($mail_subject, $mail_body, array("tadka@mindsarray.com","sunilr@pay1.in"), 'mail');

        $this->autoRender = false;
    }

    function tfredAmount($date = null) {
        if (empty($date))
            $date = date('Y-m-d');
        //amount transferred
        $sds_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='$date' AND type = " . ADMIN_TRANSFER . " AND confirm_flag != 1 AND ref2_id NOT IN (" . SDISTS . ")");
        $dis_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='$date' AND type = " . SDIST_DIST_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . SDISTS . ") AND ref2_id NOT IN (" . DISTS . ")");
        $ret_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='$date' AND type = " . DIST_RETL_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . DISTS . ")");
        if(empty($sds_trans[0][0]['amt'])){
			$sds_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions_logs WHERE date='$date' AND type = " . ADMIN_TRANSFER . " AND confirm_flag != 1 AND ref2_id NOT IN (" . SDISTS . ")");
		}
		
		if(empty($dis_trans[0][0]['amt'])){
			$dis_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions_logs WHERE date='$date' AND type = " . SDIST_DIST_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . SDISTS . ") AND ref2_id NOT IN (" . DISTS . ")");
		}
		
		if(empty($ret_trans[0][0]['amt'])){
			
			$ret_trans = $this->Slaves->query("SELECT SUM(amount) as amt FROM shop_transactions_logs WHERE date='$date' AND type = " . DIST_RETL_BALANCE_TRANSFER . " AND confirm_flag != 1 AND ref1_id IN (" . DISTS . ")");
		}
		
        //$pullback = $this->Retailer->query("SELECT SUM(amount) as amt FROM shop_transactions WHERE date='".date('Y-m-d')."' AND ((type = ".PULLBACK_RETAILER . " AND ref1_id IN (".DISTS.")) OR  (type = ".PULLBACK_DISTRIBUTOR . " AND ref1_id IN (".SDISTS.") AND ref2_id NOT IN (".DISTS.")) OR (type = ".PULLBACK_SUPERDISTRIBUTOR." AND ref2_id NOT IN (".SDISTS.")))");

        $transferred = intval($sds_trans['0']['0']['amt'] + $dis_trans['0']['0']['amt'] + $ret_trans['0']['0']['amt']);
		
        echo "Amount transferred by Admin: " . intval($sds_trans['0']['0']['amt']);
        echo "<br/>Amount transferred by Mindsarray (SD): " . intval($dis_trans['0']['0']['amt']);
        echo "<br/>Amount transferred by Distributors: " . intval($ret_trans['0']['0']['amt']);
        echo "<br/>Total amount transferred: $transferred";
        $this->Retailer->query("UPDATE float_logs SET transferred = '$transferred' WHERE date='$date' AND hour = 24");
        $this->autoRender = false;
    }

    function checkVendors($id = 0) {//exit;
        //$this->layout = null;
        $this->autoRender = false;

        $vendors = $this->Slaves->query("SELECT `id` , `company` , `shortForm` ,last30bal FROM `vendors` WHERE `update_flag` =0");

        // print_r($vendors);
        //$this->autoRender = false;
        //exit ;
        $vendorsData = array(); //print_r($vendors);
        foreach ($vendors as $key => $value) {
            $vendorsData[trim($value["vendors"]["shortForm"])]["shortForm"] = $value ["vendors"]["shortForm"];
            $vendorsData[trim($value["vendors"]["shortForm"])]["company"] = $value["vendors"]["company"];
            $vendorsData[trim($value["vendors"]["shortForm"])]["last30bal"] = $value["vendors"]["last30bal"];
            $vendorsData[trim($value["vendors"]["shortForm"])]["id"] = $value["vendors"]["id"];
        }


        $dataSale = $this->Slaves->query("
                                               SELECT 
                                                    count(vendors_activations.id) as ids, 
                                                    vendors_activations.vendor_id,
                                                    vendors.shortForm,
                                                    sum(if( vendors_activations.status != 3,amount,0)) as sale,
                                                    sum(if( vendors_activations.status != 3,discount_commission*amount/100,0)) as comm
                                               FROM 
                                                    `vendors_activations`,
                                                    vendors
                                               WHERE 
                                                    vendors.id = vendors_activations.vendor_id 
                                               AND  
                                                    vendors.update_flag = 0
                                               AND 
                                                    vendors_activations.date = '" . date('Y-m-d') . "' 
                                               AND  
                                                    vendors_activations.timestamp >= '" . date('Y-m-d H:i:s', strtotime('-30 minutes')) . "'                   
                                               GROUP BY 
                                                    vendors_activations.vendor_id 
                                              "
        );




        foreach ($dataSale as $key => $value) {

            $bal = 0;
            $method = $value["vendors"]["shortForm"] . "Balance";
            App::import('Controller', 'Recharges');
            $obj = new RechargesController;
            $obj->constructClasses();
            if (method_exists($obj, $method)) {
                $bal = $obj->$method(1);
                $bal = $bal['balance'];
            }

            if (!empty($bal)) {
                $this->Retailer->query("Update vendors set last30bal = '$bal',update_time='" . date('Y-m-d H:i:s') . "' where shortForm = '" . $value["vendors"]["shortForm"] . "'");

                $diff = ($vendorsData[$value["vendors"]["shortForm"]]["last30bal"] - $bal) - ($value["0"]["sale"] - $value["0"]["comm"]);
                if ($diff > 1000) {
                    //stop vendor
                    $pid = $vendorsData[$value["vendors"]["shortForm"]]["id"];
                    /* $data = $this->Shop->getVendorInfo($pid);
                      $data["active_flag"] = 2;
                      $this->Shop->setVendorInfo($pid,$data);
                      $this->Shop->setInactiveVendors(); */
                    //send email
                    $subject = "(SOS)System disabled $vendor";
                    $this->General->sendMails($subject, "Problem in Vendors Balance in last 30 minutes!!! </br>Vendor : " . $vendorsData[$value["vendors"]["shortForm"]]["company"] . "</br>
                                                    Balance (Before 30 min) : " . $vendorsData[$value["vendors"]["shortForm"]]["last30bal"] . "</br>
                                                    Balance (Current) : " . $bal . "</br> 
                                                    Sale in last 30 mins : " . ($value["0"]["sale"] - $value["0"]["comm"]) . "</br> 
                                                    Diff : " . $diff . "</br> 
                                                    ", array('ashish@mindsarray.com'), 'mail'
                    );
                    /* echo "</br>Vendor's OpeningClosing & Sale Diff Is Greater Than 1000 !!! </br> ",
                      "Vendor : ".$vendorsData[$value["shortForm"]]["company"]."</br>
                      Balance ( before 30 min ) : ".$vendorsData[$value["shortForm"]]["last30bal"]."</br>
                      Balance (    Current    ) : ".$bal."</br>
                      Sale : ".$value["sale"]."</br>
                      Diff : ".$diff."</br>
                      "; */
                }
            }
        }
    }

    function getSMSTemplates() {
    	$timestamp = isset($_REQUEST['timestamp']) ? urldecode($_REQUEST['timestamp']) : '0000-00-00 00:00:00';
    	
        $data = $this->Slaves->query("SELECT * FROM sms_templates Where datetime >= '$timestamp'");

        $prods = $this->Slaves->query("SELECT auto_check,id FROM products WHERE to_show=1");
		$data['prods'] = $prods;
        echo json_encode($data);
        $this->autoRender = false;
    }

    function autoDeclineComplaints() {//5,8 exception scenario
		
		$vendor = array('58','8','87');
		
		$product = array('3','34','30','31','7','8');
		
        $data = $this->Slaves->query("SELECT vendors_activations.ref_code, complaints.resolve_flag,vendors_messages.status,vendors_messages.response
										FROM complaints 
										inner join vendors_activations 
										ON (vendors_activations.id = vendor_activation_id) 
										INNER JOIN vendors ON 
										(vendors.id = vendors_activations.vendor_id) 
										LEFT JOIN vendors_messages 
										ON ( vendors_messages.shop_tran_id = vendors_activations.ref_code AND vendors_activations.vendor_id = vendors_messages.service_vendor_id )
                                        WHERE 
										in_date >= '" . date('Y-m-d', strtotime('-1 days')) . "' AND vendors.update_flag= 1 AND vendors_messages.response = 'Successful' AND complaints.takenby = 0   group by vendor_activation_id having (count(complaints.id) = 1 AND complaints.resolve_flag = 0)");
		
		$apiQuery = $this->Slaves->query("SELECT vendors_activations.ref_code,vendors_activations.vendor_id,vendors_activations.product_id,complaints.resolve_flag
										FROM complaints 
										inner join vendors_activations 
										ON (vendors_activations.id = vendor_activation_id) 
										inner JOIN vendors ON 
										(vendors.id = vendors_activations.vendor_id) 
										WHERE 
										in_date >= '" . date('Y-m-d', strtotime('-1 days')) . "'  and vendors.update_flag= 0 AND  complaints.takenby = 0 ANd operator_id != '' and  operator_id is not null group by vendor_activation_id having (count(complaints.id) = 1 AND complaints.resolve_flag = 0)");


        foreach ($data as $dt) {
            $ret = $this->Shop->reversalDeclined($dt['vendors_activations']['ref_code'], 1);
        }
		
		 foreach ($apiQuery as $dt) {
			 if(in_array($dt['vendors_activations']['vendor_id'],$vendor) && in_array($dt['vendors_activations']['product_id'],$product)){
				 $ret = $this->Shop->reversalDeclined($dt['vendors_activations']['ref_code'], 1);
			 }
				else {
					
					if(!in_array($dt['vendors_activations']['product_id'],$product)){
						$ret = $this->Shop->reversalDeclined($dt['vendors_activations']['ref_code'], 1);
					}
				}
            
        }
        $this->autoRender = false;
    }

    function payu_check_cron() {
        $txnqry = "SELECT shop_transaction_id,productinfo FROM `pg_payuIndia` WHERE status ='pending' AND shop_transaction_id is not null AND addedon <= '" . date('Y-m-d H:i:s', strtotime('- 10 minutes')) . "' order by id limit 10";
        $txn_result = $this->Retailer->query($txnqry);

        if (empty($txn_result)) {
            exit();
        }

        $txnids = array();
        foreach ($txn_result as $txn) {
            $txnids[] = $txn['pg_payuIndia']['shop_transaction_id'];
        }

        //$logger = $this->General->dumpLog('Payu', 'receivePayuStatus');

        $var1 = implode('|', $txnids);
        $service_api_data = array('var1' => $var1, 'command' => 'verify_payment', 'key' => PAYU_KEY);
        $hash = hash("sha512", PAYU_KEY . "|" . $service_api_data['command'] . "|" . $service_api_data['var1'] . "|" . PAYU_SALT);
        $service_api_data['hash'] = $hash;

        $out = $this->General->curl_post(PAYU_SERVICE_URL, $service_api_data);

        if ($out['success']) {
            $result_data = unserialize($out['output']);
            if (count($result_data['transaction_details']) > 0) {
                foreach ($result_data['transaction_details'] as $txn_num => $txndetail) {
                    if ($txndetail['status'] === "Not Found") {
                        $txndetail['txnid'] = $txn_num;
                        $txndetail['status'] = 'failure';
                        unset($txndetail['mihpayid']);
                        $this->Shop->update_pg_payu($txndetail, false);
                    } else {
                        $txndetail['amount'] = isset($txndetail['amount']) ? $txndetail['amount'] : "";
                        $txndetail['amount'] = ($txndetail['amount'] == "" && isset($txndetail['amt'])) ? $txndetail['amt'] : "";
                        $this->Shop->update_pg_payu($txndetail, false);
                    }
                }
            }
            //return array('txn_id'=>$var1,'result'=>$result_data);
        }
        $this->autoRender = false;
    }

    function solrRecharges() {
        $this->autoRender = false;

        $fields = array('product_id', 'id', 'mobile', 'param', 'amount', 'retailer_id', 'status', 'api_flag', 'code', 'timestamp', 'date');
        $table = "vendors_activations";

        $comma_separated = implode(",va.", $fields);
        $logTabDate = "SELECT value FROM vars WHERE name = 'solr_date'";
        $val = $this->Slaves->query($logTabDate);
        $newdate = $val['0']['vars']['value'];

        $lastdate = date('Y-m-d', strtotime($newdate . '- 2 days'));

        $query = "select $comma_separated,retailers.parent_id as distributor_id from vendors_activations as va left join retailers ON (retailers.id = retailer_id) where date >='" . $lastdate . "' AND date <= '" . $newdate . "'";
        $data = $this->Slaves->query($query);

        $fp = fopen('/opt/solr/example/exampledocs/vendors_activations.csv', 'w') or die('error ...');
        $fields[] = 'distributor_id';
        fputcsv($fp, $fields, ',', '"'); //insert column name at top in csv

        $insertQuery = "UPDATE vars SET value = '" . date('Y-m-d') . "' WHERE name = 'solr_date'";


        foreach ($data as $dt) {
            $vals = array();
            foreach ($dt as $key => $val) {
                foreach ($val as $key => $v) {
                    $vals[] = $v;
                }
            }
            fputcsv($fp, $vals, ',', '"');
        }
        fclose($fp);

        try {
            shell_exec("sh /var/www/html/shops/solr.sh");
            //shell_exec("curl http://localhost:8983/solr/pay1/update/csv?overwrite=true --data-binary @/opt/solr/example/exampledocs/vendors_activations.csv -H 'Content-type:application/csv; charset=utf-8' ");
            //shell_exec("curl 'http://localhost:8983/solr/pay1/update/csv?commit=true'");
            $this->Retailer->query($insertQuery);
        } catch (Exception $ex) {
            $ex->getTraceAsString();
        }
    }

    /*function diwaliOffer() {
        //$data = $this->Retailer->query("SELECT sum(sale) as amt,retailers.mobile,retailers.id FROM retailers_logs left join retailers ON (retailers.id = retailer_id) WHERE date >= '2014-09-01' AND date <= '2014-09-30' group by retailer_id order by retailer_id");
        $data = $this->Slaves->query("select retailer_id, mobile, sale_till_now, floor(avg_sale), floor((avg_sale * 18)+((avg_sale * 13) * 1.15)) as total_sale ,floor(((avg_sale * 13) * 1.15) * 0.1) as incentive  FROM (SELECT rl.retailer_id, r.mobile, avg(rl.sale) as avg_sale, sum(rl.sale) as sale_till_now FROM `retailers_logs` as rl LEFT JOIN  `retailers` as r on rl.retailer_id=r.id  where date between '2015-03-01' and '2015-03-18' group by 1) as t1");
        $i = 0;
        foreach ($data as $dt) {
            $retailer_id = $dt['t1']['retailer_id'];
            $retailer_mobile = $dt['t1']['mobile'];
            $retailer_sale_till_18 = $dt['t1']['sale_till_now'];
            $avg_sale = $dt['0']['average_sale'];
            $total_sale = $dt['0']['total_sale'];
            $incentive = $dt['0']['incentive'];
            
            $sms = "Bonus of the Month
Hurry Up Now!!!
Your Monthly Sales till 18 March is Rs $retailer_sale_till_18, Achieve Total Rs $total_sale by 31 March to get additional Rs $incentive  
-Pay1";
                $this->General->sendMessage($mob, $sms, 'notify');            
            }

        $this->autoRender = false;
    }

    function dist_april_Offer() {
        $dist_qry = "SELECT * FROM (SELECT distributor_id, mobile, sale_till_now, round(avg_sale) as avg_sale, round((avg_sale * 20)+((avg_sale * 10) * 1.20)) as total_sale ,floor(((avg_sale * 10) * 1.20) * 0.2/100) as incentive FROM 
                (SELECT dl.distributor_id, u.mobile, avg(dl.topup_sold) as avg_sale, sum(dl.topup_sold) as sale_till_now   
                        FROM `distributors_logs` as dl 
                        INNER JOIN  `distributors` as d ON dl.distributor_id=d.id 
                        INNER JOIN  `users` as u ON d.user_id = u.id 
                            WHERE dl.date >= '2015-04-01' and dl.date <= '2015-04-20' 
                            GROUP BY 1
                ) as t1 where sale_till_now > 0 ) as t2 where incentive > 0 and sale_till_now > 50000";
        $data = $this->Slaves->query($dist_qry);
        $i = 0;
        foreach ($data as $dt) {
            $distributor_id = $dt['t2']['retailer_id'];
            $distributor_mobile = $dt['t2']['mobile'];
            $distributor_sale_till_20 = $dt['t2']['sale_till_now'];
            $avg_sale = $dt['t2']['avg_sale'];
            $total_sale = $dt['t2']['total_sale'];
            $incentive = $dt['t2']['incentive'];
            
            $sms = "Bonus of the Month
Hurry Up Now!!!
Your Monthly Sales till 20th April is Rs $distributor_sale_till_20, Achieve Total Rs $total_sale by 30 April to get additional Rs $incentive  
-Pay1";
                $this->General->sendMessage($distributor_mobile, $sms, 'notify');            
            }

        $this->autoRender = false;
    }*/


   /*function dist_april_incentive_SMS() {
        
        $data_target = $this->Slaves->query("SELECT * FROM (SELECT distributor_id, mobile, sale_till_now, round(avg_sale) as avg_sale, round((avg_sale * 20)+((avg_sale * 10) * 1.20)) as total_sale ,floor(((avg_sale * 10) * 1.20) * 0.2/100) as incentive FROM 
                (SELECT dl.distributor_id, u.mobile, avg(dl.topup_sold) as avg_sale, sum(dl.topup_sold) as sale_till_now   
                        FROM `distributors_logs` as dl 
                        INNER JOIN  `distributors` as d ON dl.distributor_id=d.id 
                        INNER JOIN  `users` as u ON d.user_id = u.id 
                            WHERE dl.date >= '2015-04-01' and dl.date <= '2015-04-20' 
                            GROUP BY 1
                ) as t where sale_till_now > 0 ) as t1 where incentive > 0 and sale_till_now > 50000");
        
        $data_achieved = $this->Slaves->query("SELECT * FROM (SELECT distributor_id, mobile, sale_till_now, round(avg_sale) as avg_sale, round((avg_sale * 20)+((avg_sale * 10) * 1.20)) as total_sale ,floor(((avg_sale * 10) * 1.20) * 0.2/100) as incentive FROM 
                (SELECT dl.distributor_id, u.mobile, avg(dl.topup_sold) as avg_sale, sum(dl.topup_sold) as sale_till_now   
                        FROM `distributors_logs` as dl 
                        INNER JOIN  `distributors` as d ON dl.distributor_id=d.id 
                        INNER JOIN  `users` as u ON d.user_id = u.id 
                            WHERE dl.date >= '2015-04-01' and dl.date <= '2015-04-30' 
                            GROUP BY 1
                ) as t where sale_till_now > 0 ) as t2 where incentive > 0 and sale_till_now > 50000");

        $data_achiever = array();
        foreach($data_achieved as $ret_k=>$ret_val){
            $data_achiever[$ret_val['t2']['distributor_id']] = $ret_val['t2'];
        }
        
        $i = 0;
        $total = 0;
        $total_rets = 0;
        
        $data_achieverlist = array();
        foreach($data_target as $k_ret=>$val_ret){
            $mob = $val_ret['t1']['mobile'];
            $target_sale = $val_ret['t1']['total_sale'];
            $ret_id = $val_ret['t1']['distributor_id'];          
           
            $achieved_sale = $data_achiever[$ret_id]['sale_till_now'];
            
            if (!in_array(substr($mob, 0, 1), array('9', '8', '7')))
                continue;
            
            $incentive = $val_ret['t1']['incentive'];          
            
            if($achieved_sale >= $target_sale){
                $sms = "Congrats !!";
                $sms .= "\nAapka last month ka sale hai: Rs $achieved_sale";
                $sms .= "\nPay1 is giving you a Bonus for the Month (march)  Rs $incentive!!";
                $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $incentive, $ret_id, RETAILER);
                $bal = $this->Shop->shopBalanceUpdate($incentive, 'add', $ret_id, RETAILER);
                $this->Shop->addOpeningClosing($ret_id, RETAILER, $trans_id, $bal - $incentive, $bal);
                $sms .= "\nYour current balance is: Rs. " . $bal;

                $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$incentive,3,'" . addslashes($sms) . "','" . date('Y-m-d') . "','" . date('Y-m-d H:i:s') . "')");
                echo nl2br($sms) . "<br/>";
                $this->General->sendMessage($mob, $sms, 'notify');
             
                usleep(100000);

                $total += $incentive;
                $total_rets += 1;
            }            
        }

        $mail_body = "Total incentive given to pay1 distributors: $total <br> Total number of distributors received bonus : $total_rets ";
        $this->General->sendMails("April Bonus to Pay1 distributors", $mail_body, array('tadka@mindsarray.com', 'sales@mindsarray.com', 'dharmesh@mindsarray.com'), 'mail');
        $this->autoRender = false;
    }*/

    
/****************/
    
    
    
    /*function mar_incentive_SMS() {
        $data_target = $this->Retailer->query("SELECT * 
                        FROM ( SELECT retailer_id, mobile, sale_till_now, floor(avg_sale), floor((avg_sale * 18)+((avg_sale * 13) * 1.15)) as total_sale ,floor(((avg_sale * 13) * 1.15) * 0.001) as incentive  
                              FROM (SELECT rl.retailer_id, r.mobile, avg(rl.sale) as avg_sale, sum(rl.sale) as sale_till_now 
                                    FROM `retailers_logs` as rl 
                                        LEFT JOIN  `retailers` as r ON rl.retailer_id=r.id  
                                        WHERE date between '2015-03-01' and '2015-03-18' 
                                        GROUP BY 1
                                        ) as t
                                ) as t1");
        
        $data_achieved = $this->Retailer->query("SELECT * 
                                        FROM ( SELECT rl.retailer_id, r.mobile, avg(rl.sale) as avg_sale, sum(rl.sale) as sale_till_now 
                                                FROM `retailers_logs` as rl 
                                                    LEFT JOIN  `retailers` as r ON rl.retailer_id=r.id  
                                                    WHERE date between '2015-03-01' and '2015-03-31' 
                                                    GROUP BY 1
                                                ) as t2");

        $data_achiever = array();
        foreach($data_achieved as $ret_k=>$ret_val){
            $data_achiever[$ret_val['t2']['retailer_id']] = $ret_val['t2'];
        }
        
        $i = 0;
        $total = 0;
        $total_rets = 0;
        $MsgTemplate = $this->General->LoadApiBalance(); 
        foreach($data_target as $k_ret=>$val_ret){
            $mob = $val_ret['t1']['mobile'];
            $target_sale = $val_ret['t1']['total_sale'];
            $ret_id = $val_ret['t1']['retailer_id'];          
           
            $achieved_sale = $data_achiever[$ret_id]['sale_till_now'];
            
            if (!in_array(substr($mob, 0, 1), array('9', '8', '7')))
                continue;
            
            $incentive = $val_ret['t1']['incentive'];          
            
            if($achieved_sale >= $target_sale){
//                $sms = "Congrats !!";
//                $sms .= "\nAapka last month ka sale hai: Rs $achieved";
//                $sms .= "\nPay1 is giving you a Bonus for the Month (march)  Rs $incentive!!";
                $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $incentive, $ret_id, RETAILER);
                $bal = $this->Shop->shopBalanceUpdate($incentive, 'add', $ret_id, RETAILER);
                $this->Shop->addOpeningClosing($ret_id, RETAILER, $trans_id, $bal - $incentive, $bal);
                
//                $sms = "Congrats !!";
//                $sms .= "\nAapka last month ka sale hai: Rs $achieved";
//                $sms .= "\nPay1 is giving you a Bonus for the Month (march)  Rs $incentive!!";
//                $sms .= "\nYour current balance is: Rs. " . $bal;

                $paramdata['ACHIEVED'] = $achieved_sale;
                $paramdata['INCENTIVE'] = $incentive;  
                $paramdata['BALANCE'] = $bal;  

                $content =  $MsgTemplate['Retailer_MarIncentive_MSG'];
                $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                
                $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$incentive,3,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
                echo nl2br($sms) . "<br/>";
                $this->General->sendMessage($mob, $sms, 'notify');
             
                usleep(100000);

                $total += $incentive;
                $total_rets += 1;
            }            
        }

        $mail_body = "Total incentive given to pay1 retailers: $total <br> Total number of retailer received bonus : $total_rets ";
        $this->General->sendMails("March Bonus to Pay1 Retailers", $mail_body, array('tadka@mindsarray.com', 'sales@mindsarray.com', 'dharmesh@mindsarray.com'), 'mail');
        $this->autoRender = false;
    }*/
    
/****************/    
    
    /*function diwaliOfferSMS() {
        $data_sept = $this->Retailer->query("SELECT sum(sale) as amt,retailers.mobile,retailers.id FROM retailers_logs left join retailers ON (retailers.id = retailer_id) WHERE date >= '2014-09-01' AND date <= '2014-09-30' group by retailer_id order by retailer_id");
        $data_oct = $this->Retailer->query("SELECT sum(sale) as amt,retailers.mobile,retailers.id FROM retailers_logs left join retailers ON (retailers.id = retailer_id) WHERE date >= '2014-10-01' AND date <= '2014-10-31' group by retailer_id order by retailer_id");

        $sale_arr = array();
        foreach ($data_sept as $dt) {
            $amt = $dt['0']['amt'];
            $mob = $dt['retailers']['mobile'];
            $sale_arr[$mob] = $amt;
        }

        $i = 0;
        $total = 0;
        $total_rets = 0;
        $ret_50 = 0;
        $ret_100 = 0;
        $ret_200 = 0;
        $ret_300 = 0;
        $ret_400 = 0;

        //$sale_arr['9769597418'] = 20000;
        //$data_oct = array('0'=>array('0'=>array('amt'=>50000),'retailers'=>array('id'=>19,'mobile'=>'9769597418')));

        foreach ($data_oct as $dt) {
            $achieved = $dt['0']['amt'];
            $mob = $dt['retailers']['mobile'];
            $ret_id = $dt['retailers']['id'];

            if (!in_array(substr($mob, 0, 1), array('9', '8', '7')))
                continue;

            if ($achieved >= 50000 && isset($sale_arr[$mob])) {
                $last_sale = $sale_arr[$mob];
                $toachieve = intval($last_sale * 1.2);
                if ($achieved >= $toachieve) {

                    if ($achieved >= 50000 && $achieved < 100000) {
                        $incentive = 111;
                        $ret_50 ++;
                    } else if ($achieved >= 100000 && $achieved < 200000) {
                        $incentive = 221;
                        $ret_100 ++;
                    } else if ($achieved >= 200000 && $achieved < 300000) {
                        $incentive = 351;
                        $ret_200 ++;
                    } else if ($achieved >= 300000 && $achieved < 400000) {
                        $incentive = 551;
                        $ret_300 ++;
                    } else {
                        $incentive = 1111;
                        $ret_400 ++;
                    }

                    $sms = "Congrats !!";
                    $sms .= "\nAapka last month ka sale hai: Rs $achieved";
                    $sms .= "\nPay1 is giving you a diwali bonus of Rs $incentive!!";
                    $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $incentive, $ret_id, RETAILER);
                    $bal = $this->Shop->shopBalanceUpdate($incentive, 'add', $ret_id, RETAILER);
                    $this->Shop->addOpeningClosing($ret_id, RETAILER, $trans_id, $bal - $incentive, $bal);
                    $sms .= "\nYour current balance is: Rs. " . $bal;

                    $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$incentive,3,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
                    echo nl2br($sms) . "<br/>";
                    $this->General->sendMessage($mob, $sms, 'notify');

                    if (isset($incents[$incentive]))
                        $incents[$incentive] += 1;
                    else
                        $incents[$incentive] = 1;
                    usleep(100000);

                    $total += $incentive;
                    $total_rets += 1;
                }
            }
        }


        $mail_body = "Total incentive given to app retailers: $total<br/>Total retailers $total_rets<br/>No. of retailers (50000 - 100000): $ret_50<br/>No. of retailers (100000 - 200000): $ret_100<br/>No. of retailers (200000 - 300000): $ret_200<br/>No. of retailers (300000 - 400000): $ret_300<br/>No. of retailers (400000+): $ret_400";
        $this->General->sendMails("Diwali Bonus to Pay1 Retailers", $mail_body, array('tadka@mindsarray.com', 'sales@mindsarray.com', 'dharmesh@mindsarray.com'), 'mail');
        //$this->General->sendMails("Diwali Bonus to Pay1 Retailers",$mail_body,array('ashish@mindsarray.com'),'mail');

        $this->autoRender = false;
    }*/

    /*function update_txntime() {
        $data = $this->Retailer->query("SELECT va.id,max(vm.timestamp) as last FROM `vendors_activations` as va left join vendors_messages as vm ON (vm.shop_tran_id = va.ref_code) where date = '" . date('Y-m-d', strtotime('-1 days')) . "' group by vm.shop_tran_id");

        $qdata = array();
        foreach ($data as $dt) {
            $qdata[] = "(" . $dt['va']['id'] . ",'" . $dt['0']['last'] . "')";
        }

        $datas = array_chunk($qdata, 100);
        foreach ($datas as $dts) {
            $query = "INSERT INTO vendors_activations_ext (vendor_act_id,updated_time) VALUES " . implode(",", $dts);

            $this->Retailer->query($query);
        }
        $this->autoRender = false;
    }*/

    function checkForCodeUpdate() {
    	$vendor = $_REQUEST['vendor_id'];
    	$data = $this->Slaves->query("SELECT svn_flag FROM vendors WHERE id = $vendor");

    	if (!empty($data) && $data[0]['vendors']['svn_flag'] > 0) {

    		$this->Retailer->query("UPDATE vendors SET svn_flag=svn_flag-1 WHERE id = $vendor");

    		echo json_encode(array('status' => 1));
    	} else
    	echo json_encode(array('status' => 0));

    	$this->autoRender = false;
    }

    function arrayRecursiveDiff($aArray1, $aArray2) {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);

                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        // $this->autoRender = false;
        return $aReturn;
    }
    
    function getAPIBalance(){
   		$vendors = $this->Slaves->query("SELECT * FROM `vendors` where update_flag = 0 AND show_flag = 1");
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();
   		
        foreach ($vendors as $vend) {
            $name = $vend['vendors']['shortForm'];
            $method = $name . "Balance";
            if (method_exists($obj, $method)) {
                $obj->$method(1,null,1);
            }
        }
        $this->autoRender =false;
    }
    
    /*function updateRetailerInfo(){
       
      $path = $_SERVER["DOCUMENT_ROOT"] . "/uploads/";
      $scandir = scandir($path);
      if(count($scandir)>0){
          foreach($scandir as $key => $val){
              if($val!='.' && $val!='..'){
                  $explodedata = explode("_",$val);
                  if(count($explodedata)>0){
                      if($explodedata[0]=='shop'){
                          $explodedata[0] = 'image';
                      }
                       $this->Retailer->query("INSERT INTO retailers_details(id,retailer_id,type,image_name) VALUES('','" . $explodedata[1] . "','" . $explodedata[0] . "','".$val."')");
                  }
              }
          }
      }
      
      $this->autoRender =false;
    }*/
    
    //CRON FOR UPDATING RECORD ONCE
    function updateRetailersSale() {

        $prevDate = date('Y-m-d', strtotime("-6 Months"));
        $currDate = date('Y-m-01', strtotime("-1 Months"));
		
		//$currDate = '2015-01-01';
		//$prevDate = '2015-07-01';
		
		 $data = $this->Slaves->query("SELECT sum(sale) AS benchmark_value,count(distinct retailers_logs.retailer_id) as transacting,trim(retailers.parent_id) AS disId,retailers_logs.date FROM retailers_logs use index (uniq_ret_date) INNER JOIN retailers ON retailers.id = retailers_logs.retailer_id WHERE retailers_logs.DATE<='$currDate' AND retailers_logs.DATE>='$prevDate' GROUP BY retailers.parent_id,month(retailers_logs.date)");
         $transacting = array();
	
        foreach($data as $dt){
        	$dist = $dt['0']['disId'];
			$monthdays = date('t',  strtotime($dt['retailers_logs']['date']));
        	if(!isset($transacting[$dist]['benchmark'])){
        		$transacting[$dist]['benchmark'] = intval($dt['0']['benchmark_value']/$monthdays);
        		$transacting[$dist]['transacting'] = intval($dt['0']['transacting']);
        	}else {
        		if($dt['0']['benchmark_value']/$monthdays > $transacting[$dist]['benchmark']){
        			$transacting[$dist]['benchmark'] = intval($dt['0']['benchmark_value']/$monthdays);
        		}
        		
        		if($dt['0']['transacting'] > $transacting[$dist]['transacting']){
        			$transacting[$dist]['transacting'] = intval($dt['0']['transacting']);
        		}
        		
        	}
        }
      
        foreach ($transacting as $key=>$val) {
        	$this->Retailer->query("UPDATE distributors SET benchmark_value = '" . $val['benchmark'] . "',transacting_retailer = '" . $val['transacting']  . "' where id ='" . $key . "'");
        }
        
        foreach ($primarytxn as $dt) {
            //$this->Retailer->query("UPDATE distributors_logs SET primary_txn = '" . $dt[0]['primarytxn'] . "' where distributor_id ='" . $dt[0]['rid'] . "' AND date = '" . $dt['shop_transactions']['date'] . "'");
        }

        $this->autoRender = false;
    }
	
	function sendSlowLogData() {

		$data = $this->Retailer->query("SELECT * 
										FROM mysql.slow_log
										WHERE DATE_FORMAT(start_time,  '%Y-%m-%d' ) =  '" . date('Y-m-d') . "'
										AND time_to_sec(query_time) >10 ORDER BY query_time DESC LIMIT 20");
		
		$data1 = $this->Slaves->query("SELECT * 
										FROM mysql.slow_log
										WHERE DATE_FORMAT(start_time,  '%Y-%m-%d' ) =  '" . date('Y-m-d') . "'
										AND time_to_sec(query_time) >10 ORDER BY query_time DESC LIMIT 20");
		
		$subject = "Slow query";
		$body = "";
		if (count($data) > 0) {
			foreach ($data as $val) {
				$body .="Query:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['sql_text'] . "<br/></br/>Host Machine:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['user_host'] . "<br/><br/>Start time:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['start_time'] . "<br/><br/>Execution time:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['query_time'] . "<br/><br/>";
			}
			$this->General->sendMails($subject, $body, array('pravin@mindsarray.com', 'ashish@mindsarray.com','nandan@mindsarray.com'), 'mail');
		}
		
		if (count($data1) > 0) {
			foreach ($data1 as $val) {
				$body .="Query:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['sql_text'] . "<br/></br/>Host Machine:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['user_host'] . "<br/><br/>Start time:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['start_time'] . "<br/><br/>Execution time:&nbsp;&nbsp;&nbsp;" . $val['slow_log']['query_time'] . "<br/><br/>";
			}
			$this->General->sendMails($subject . "(Slave Server)", $body, array('pravin@mindsarray.com', 'ashish@mindsarray.com'), 'mail');
		}

		//echo $body;
		$this->autoRender = false;
	}
	
	function updateopeningClosing() {
		$distOpening = $this->Retailer->query("SELECT * FROM (SELECT amount,opening,closing,shop_id,date FROM `shop_transactions` left join opening_closing ON (shop_transaction_id = shop_transactions.id AND group_id = 5) where date between '".date('Y-m-d',strtotime('-12 days'))."' AND '".date('Y-m-d',strtotime('-2 days'))."' AND ((ref1_id = shop_id AND type = 2) OR (ref2_id = shop_id AND type = 1))  order by opening_closing.timestamp desc)as t group by t.shop_id,t.date");
		if(count($distOpening)>0){
			foreach ($distOpening as $dt){
			$this->Retailer->query("UPDATE distributors_logs set closing_balance = '".$dt['t']['closing']."' WHERE date ='".$dt['t']['date']."' AND distributor_id = '".$dt['t']['shop_id']."'");
			}
		}
		
		
		/*$startTime = strtotime(date('Y-m-d', strtotime('-10 days')));
		$endTime = strtotime(date('Y-m-d'));
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$date = date('Y-m-d', $i);
			$retopening = $this->Retailer->Query("SELECT amount,opening,closing,shop_id,date FROM `shop_transactions` left join opening_closing ON (shop_transaction_id = shop_transactions.id AND group_id = 6) where date = '$date' AND ((ref1_id = shop_id AND type in (4,7,11)) OR (ref2_id = shop_id AND type = 2)) group by shop_id");
			if(count($retopening)>0){
				foreach ($retopening as $dt1){
			     $this->Retailer->query("UPDATE retailers_logs set closing_balance = '".$dt1['opening_closing']['opening']."' WHERE date ='".$dt1['shop_transactions']['date']."' AND retailer_id = '".$dt1['opening_closing']['shop_id']."'");
				}
			}
		}*/
		
		$this->autoRender = false;
	}
	
	function setAPILimit(){
		$data = $this->Slaves->query("SELECT * FROM vendors_commissions WHERE cap_per_min > 0");
		$sale = $this->Slaves->query("SELECT sum(amount) as totalsale,product_id,vendor_id FROM vendors_activations WHERE status NOT IN (2,3) AND date = '".date('Y-m-d')."' GROUP BY vendor_id, IF(product_id = 9 OR product_id = 10 OR product_id = 27, 9, IF(product_id = 11 OR product_id = 29, 11, IF(product_id = 3 OR product_id = 34, 3 , IF (product_id = 7 OR product_id = 8, 8, IF (product_id = 12 OR product_id = 28, 12, IF(product_id = 30 OR product_id = 31,30,product_id)) ))))");
		
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
		
		foreach($data as $dt){
			$prod = $dt['vendors_commissions']['product_id'];
			$vendor_id = $dt['vendors_commissions']['vendor_id'];
			$capacity = $dt['vendors_commissions']['cap_per_min'];
			$prod = (isset($arr_map[$prod]))? $arr_map[$prod] :  $prod;
			
			$this->Shop->setMemcache("cap_api_$prod"."_$vendor_id",$capacity,120);
			$this->Shop->setMemcache("status_$prod"."_$vendor_id",$capacity,120);
            $this->Shop->setMemcache("cap_inprocess_$prod"."_$vendor_id",$capacity,120);
		}
		
		foreach($sale as $sl){
			$prod = $sl['vendors_activations']['product_id'];
			$prods = explode(",",$this->Shop->getOtherProds($prod));
			$vendor_id = $sl['vendors_activations']['vendor_id'];
			$totalsale = $sl['0']['totalsale'];
				
			foreach($prods as $prod){
				$this->Shop->setMemcache("sale_$prod"."_$vendor_id",$totalsale,120);
			}
		}
		
		$this->autoRender = false;
	}
	
	
	/*function tranferFileS3Aws() {

		App::import('vendor', 'S3', array('file' => 'S3.php'));
		$bucket = 'pay1bucket';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/uploads/";
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
		$scandir = scandir($path);

		foreach ($scandir as $val) {
			if ($val != '.' && $val != '..') {
				$actual_image_name = $val;
				if ($s3->putObjectFile($path . $actual_image_name, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
					$imgUrl = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
					
					$this->Retailer->query("UPDATE retailers_details SET image_name = '" . addslashes($imgUrl) . "' WHERE image_name = '".addslashes($actual_image_name)."'");
					//echo "<img src = " . $imgUrl . " height='300px;' width='300px;'>";
				}
			}
		}

		$this->autoRender = false;
	}*/
	


	/*function updateCCReport(){
		$this->autoRender = false;
		$date = "2015-05-16";
		$comments = $this->Retailer->query("select ref_code, date, retailers_id from comments where date >= '$date'");
		foreach($comments as $c){
			if($c['comments']['ref_code']){
				$comments_count = $this->Retailer->query("select ref_code, count from comments_count where ref_code = '".$c['comments']['ref_code']."' and date = '".$c['comments']['date']."'");
				$va = $this->Retailer->query("select vendor_id, product_id, api_flag from vendors_activations where ref_code = '".$c['comments']['ref_code']."'");
	
				if($comments_count)
					$this->Retailer->query("update comments_count set count = count + 1 where ref_code = '".$c['comments']['ref_code']."' and date = '".$c['comments']['date']."'");
				else
					$this->Retailer->query("insert into comments_count(ref_code, count, vendor_id, product_id, retailer_id, medium, date) values('".$c['comments']['ref_code']."', 1, ".$va[0]['vendors_activations']['vendor_id'].", ".$va[0]['vendors_activations']['product_id'].", '".$c['comments']['retailers_id']."', ".$va[0]['vendors_activations']['api_flag'].", '".$c['comments']['date']."')");
			}
		}
		echo "Done";
	}

	function getMemcacheKeys() {
		$memcache = new Memcache;
		$memcache->connect('127.0.0.1', 11211) or die("Could not connect to memcache server");

		$list = array();
		$allSlabs = $memcache->getExtendedStats('slabs');

		$items = $memcache->getExtendedStats('items');

		foreach ($allSlabs as $server => $slabs) {
			foreach ($slabs AS $slabId => $slabMeta) {
				$cdump = $memcache->getExtendedStats('cachedump', (int) $slabId);

				foreach ($cdump AS $keys => $arrVal) {
					if (!is_array($arrVal))
						continue;
					foreach ($arrVal AS $k => $v) {
						echo $k . '<br>';
					}
				}
			}
		}
		$this->autoRender = false;
	}*/
	
	
		
    function unlock_txn_before_hour(){
        $curTime = date('Y-m-d H:i:s');
        $qry = "DELETE FROM `temp_txn` WHERE time_to_sec(timediff('$curTime',timestamp)) >  (60*15)";
        $this->Retailer->query($qry);
        $this->autoRender = false;
    }
    
    function ipupdate(){
    	$vendor = $_REQUEST['vendor'];
    	$ip = $_SERVER['REMOTE_ADDR'];
    	$this->Shop->setVendorInfo($vendor,array('ip'=>$ip));
        $this->Shop->setMemcache("vendorip_$vendor",$ip,120);
    	$this->autoRender = false;
    }
    
	
	function CheckOperatorFlag(){
		
		
		$checkAutoUpdateOperator = $this->Slaves->query("Select id,auto_check,name from products where modified  >= '".$_REQUEST['prevtimestamp']."'");
		if(count($checkAutoUpdateOperator)>0){
			foreach ($checkAutoUpdateOperator as $val){
				$data[$val['products']['id']]['check'] = $val['products']['auto_check'];
				$data[$val['products']['id']]['name'] = $val['products']['name'];
			}
			 echo json_encode($data);
		} 
	
		$this->autoRender = false;
	}
	
	/*function UpdateRefund(){
		
		$refundData = $this->Retailer->query("Select * from shop_transactions where date = '".date('Y-m-d')."' and type ='".REFUND."'");
		
		if(!empty($refundData)){
			
			foreach ($refundData as $val){
				
				if($val['shop_transactions']['ref2_id'] == DISTRIBUTOR){
					$groupId = DISTRIBUTOR;
					$type = '3';
				} else {
					$groupId = RETAILER;
					$type = '2';
				}
				
				$this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,date,timestamp) VALUES ('".$groupId."','".$val['shop_transactions']['id']."','".$val['shop_transactions']['amount']."', '".$type."','" . $val['shop_transactions']['date'] . "','".$val['shop_transactions']['timestamp']."')");
			}
			
		}
		
		$this->autoRender = false;
		
	}*/
	
	/**
	 * Function for updating retailers benchmark sale
	 */
	function updateRetailersBenchmark	(){

		$this->autoRender = false;
		
		$prevDate = date('Y-m-d', strtotime("-30 days "));
		$currDate = date('Y-m-d', strtotime("-1 days"));
		
		//last 30 days sale
		$dataCurrentAverageSale = $this->Slaves->query("
							SELECT 
								retailers.id, sum(sale) AS base_value 
							FROM 
								retailers_logs use index (uniq_ret_date) 
							INNER JOIN 
								retailers 
							ON 
								retailers.id = retailers_logs.retailer_id 
							WHERE 
								retailers_logs.DATE between '$prevDate' AND '$currDate'
							GROUP BY 
								retailers.id"
						);
// 		echo "<pre>";
// 		print_r($dataBaseSale);
		$currentAverageSale = array();
		
		foreach ($dataCurrentAverageSale as  $index => $arr){
			$currentAverageSale[$arr['retailers']['id']] = $arr[0]['base_value']/30;
		}
		
		//GETTING PREVIOUS BENCHMARK SET
		$dataBenchmark = $this->Slaves->query("
							SELECT 
								id, ret_benchmark_value
							FROM 
								retailers"
						);
		$benchmark = array();
		
		foreach ($dataBenchmark as $index => $arr){
			$benchmark[$arr['retailers']['id']] = $arr['retailers']['ret_benchmark_value'];
		}
		
// 		print_r($currentAverageSale);
// 		print_r($benchmark);
		
		foreach ($currentAverageSale as $id => $amt){
			if(!isset($benchmark[$id]))
				$benchmark[$id] = 0;
			if($benchmark[$id] < $amt)
				$benchmark[$id] = $amt;			
		}
		
		foreach ($benchmark as $id=>$amt) {
			if($amt != 0){
				$query = "UPDATE retailers 	SET ret_benchmark_value = '$amt' WHERE id = $id ";
// 				echo "$query<br>";
				$this->Retailer->query($query);
			}
		}

	}

	function blockNonworkingRetailers(){
		$query = "SELECT retailers.id, max( retailers_logs.date ) AS maxdate
FROM `retailers`
LEFT JOIN retailers_logs ON ( retailer_id = retailers.id )
WHERE parent_id =1
AND toshow =1
AND date( created ) < '".date('Y-m-d',strtotime('- 30 days'))."'
GROUP BY retailers.id
ORDER BY maxdate limit 5000";
		
		$data = $this->Slaves->query($query);
		foreach($data as $dt){
			if(empty($dt['0']['maxdate']) || $dt['0']['maxdate'] < date('Y-m-d',strtotime('- 30 days')) ){
				$ids[] = $dt['retailers']['id'];
			}
			echo count($ids) . "<br/>";
		}
		
		$this->Retailer->query("UPDATE retailers SET toshow = 0,modified='".date('Y-m-d H:i:s')."' WHERE id in (".implode(",",$ids).")");
		$this->autoRender = false;
	}
	
	function checkVendorHealth(){
		$time = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
		$query = "select max(sync_timestamp),vendor_id,TIME_TO_SEC(TIMEDIFF('$time',sync_timestamp)) FROM devices_data where sync_date='$date' AND TIME_TO_SEC(TIMEDIFF('$time',sync_timestamp)) > 300 group by vendor_id";
	
		$data = $this->Slaves->query($query);
		foreach($data as $dt){
			$vendor = $dt['devices_data']['vendor_id'];
			$info = $this->getVendorInfo($vendor);
			if($info['active_flag'] == 1){
				$this->Shop->unHealthyVendor($vendor,20);
			}
		}
		
		$this->autoRender = false;
	}
        
    /**
     * API vendor generate insert data in api_transactions
     * @param type $autoflag : true to dump record of all 24 hrs
     * @param type $prev_day_num : 1= yesterday, 2=day before yesterday ...
     * @param type $vendorId : vendor ID of api vendor in case to process individually
     * @param type $hour : hour in 24 hr format in case to process for single hour
     */
	function sync_api_txn_data($autoflag = false, $prev_day_num = 1, $vendorId = null, $hour_num = null) {

        if (empty($vendorId)):
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where update_flag=0 and id not in (22) order by 1 desc ";
        else:
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where update_flag=0 and id='$vendorId' order by 1 desc ";
        endif;

        $txn_hour = is_null($hour_num) ? date("H") : $hour_num;

        $API_vendorList = $this->Slaves->query($API_vendor_qry);
        
        $statusList = array('0' => 'pending', '1' => 'success', '2' => 'failure', '3' => 'failure', '4' => 'success', '5' => 'success');//---status list
        
        $search_date = date('Y-m-d', strtotime('-' . $prev_day_num . ' days'));//----- generation date from 'no_of_days_before_yesterday'

        $hoursList = ($autoflag == true) ? array(range(0, 23)) : array(array($txn_hour)) ; // if autoflage is true generator for all hours in a day
        
        foreach ($hoursList[0] as $hour):
            
            foreach ($API_vendorList as $key => $val):

                $vendorId = $val['vendors']['id'];
                $chk_exist_qry = "SELECT count(1) as total FROM api_rec_log where date='$search_date' and vendor_id='$vendorId' and hour='$hour' and status=1";

                $chk_qry_data = $this->Retailer->query($chk_exist_qry);

                if ($chk_qry_data[0][0]['total'] > 0):
                    echo "$vendorId :: $hour continue<hr>";
                    continue;
                endif;

                // Query to get combine data from vendor_activataion and vendor_messages
                $txn_qry = "SELECT * FROM (SELECT distinct va.id,va.date,(CASE WHEN va.vendor_id = vm.service_vendor_id THEN va.status ELSE '2' END) as status,"
                        . " vm.vendor_refid,va.ref_code,va.amount "
                        . "FROM `vendors_activations` as va "
                        . "LEFT JOIN vendors_messages as vm ON (vm.shop_tran_id=va.ref_code) "
                        . "WHERE date = '$search_date' AND vm.service_vendor_id=$vendorId and HOUR(va.timestamp)='$hour') as api_txn_data ";

                $apiTXNs = $this->Slaves->query($txn_qry);
                
                $i = 1;// Setting initial counter
                
                $batch_qry = array();// Initializing batch query array

                foreach ($apiTXNs as $txndata):
                    
                    $transId = $txndata['api_txn_data']['ref_code'];
                    $transdate = $txndata['api_txn_data']['date'];
                    $transrefId = $txndata['api_txn_data']['vendor_refid'];
                    $transamount = $txndata['api_txn_data']['amount'];
                    $transStatus = $statusList[$txndata['api_txn_data']['status']];

                    $current_datetime = date('Y-m-d H:i:s');
                    $current_date = date('Y-m-d');

                    $batch_qry['type'] = "INSERT";
                    $batch_qry['predata'] = "INSERT INTO `api_transactions` (`vendor_id`,`txn_id`,`ref_code`,`amount`,`vendor_status`,`server_status`,`updated_time`,`date`,`hour`) VALUES";
                    $batch_qry['data'][] = "('$vendorId','$transId','$transrefId','$transamount','null','$transStatus','$current_datetime','$search_date','$hour')";

                    if ( ($i % 100) == 0 ): //process batch of 100 query together 
                        file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s') . " :: INSERT :: $i :: vendor :: $vendorId :: calling batch query \n", FILE_APPEND | LOCK_EX);
                        $this->run_batch($batch_qry);
                        $batch_qry = array();
                    endif;

                    $i++;

                endforeach;

                if ( !empty($batch_qry) ): // handled cases if last batch consist less then 100 queries                    
                    file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s') . " :: INSERT :: $i :: vendor :: $vendorId :: calling batch query \n", FILE_APPEND | LOCK_EX);
                    $this->run_batch($batch_qry);
                    $batch_qry = array();                    
                endif;
                                
                $api_rec_log_in_qry = "INSERT INTO api_rec_log (`vendor_id`,`date`,`hour`,`status`) values('$vendorId','$search_date','$hour','1')";
                $this->Retailer->query($api_rec_log_in_qry);//--- log process complete of vendor for particular date and hour

            endforeach;
            sleep(2);
        endforeach;

        $this->autoRender = false;
    }

    /**
     * This function update the vendor response of txn on hourly basis
     * @param type $prev_day_num : 1= yesterday, 2=day before yesterday ...
     * @param type $vendorId : vendor ID of api vendor in case to process individually
     * @param type $hour : hour in 24 hr format in case to process for single hour
     * @param type $forkflag : default false, change true to run vendor wise process
     */
    function update_api_recon($forkflag = "false", $prev_day_num = 1, $vendorId = null, $hour_num = null) {
        file_put_contents('/mnt/logs/api_auto_recon.txt', date("Y-m-d H:i:s")." called function :: vendor :: ".$vendorId." and param: |$forkflag|$prev_day_num|$vendorId|$hour_num \n" , FILE_APPEND | LOCK_EX);
        if (empty($vendorId)):
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where update_flag=0 and id not in (22)  order by 1 desc ";
        else:
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where update_flag=0  and id='$vendorId' order by 1 desc ";
        endif;
        
        $hour = (is_null($hour_num) or ($hour_num == "null")) ? date("H") : $hour_num;

        $API_vendorList = $this->Slaves->query($API_vendor_qry);

        $statusList = array('0' => 'pending', '1' => 'success', '2' => 'failure', '3' => 'failure', '4' => 'pending', '5' => 'success'); //---status list

        $search_date = date('Y-m-d', strtotime('-' . $prev_day_num . ' days')); //----- generation date from 'no_of_days_before_yesterday'

        $_SERVER['DOCUMENT_ROOT'] = "/var/www/html/shops/app/webroot";
        
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();
        $obj_funct_list = get_class_methods($obj);
        
        foreach ($API_vendorList as $key => $val):
            $vendorId = $val['vendors']['id'];
            
            //fork individual process for each vendor if forkflag is true
            if( $forkflag == "true"):
                file_put_contents('/mnt/logs/api_auto_recon.txt', date("Y-m-d H:i:s")." forking process for update :: vendor :: ".$val['vendors']['shortForm']." \n" , FILE_APPEND | LOCK_EX);
                $forkflag_frwd = "false";    
                shell_exec("nohup sh  /var/www/html/shops/app/webroot/scripts/api_auto_recon.sh $forkflag_frwd $prev_day_num $vendorId $hour  2>&1 > /dev/null & ");
                continue;
            endif;
        
            $function_name = $val['vendors']['shortForm'] . "TranStatus";

            if (in_array($function_name, $obj_funct_list)):

                $flag = $prev_day_num - 1;
                $qry_api_txn = "SELECT * FROM `api_transactions` where date='$search_date' and hour='$hour' and vendor_id='$vendorId'";
                $vendor_txn_list = $this->Retailer->query($qry_api_txn);
                $vendor_txn_ids = array();
                foreach ($vendor_txn_list as $txn_data):
                    $vendor_txn_ids[] = $txn_data['api_transactions']['txn_id'];
                endforeach;

                if(empty($vendor_txn_ids)):
                    continue;
                endif;
                
                $txn_list_string = implode(",", $vendor_txn_ids);
               
                // Query to get combine data from vendor_activataion and vendor_messages
                $txn_qry = "SELECT * FROM (SELECT distinct va.id,va.date,(CASE WHEN va.vendor_id = vm.service_vendor_id THEN va.status ELSE '2' END) as status,"
                        . " vm.vendor_refid,va.ref_code,va.amount,vm.service_id, va.vendor_id, vm.service_vendor_id "
                        . "FROM `vendors_activations` as va "
                        . "LEFT JOIN vendors_messages as vm ON (vm.shop_tran_id=va.ref_code) "
                        . "WHERE date = '$search_date' AND vm.service_vendor_id=$vendorId and HOUR(va.timestamp)='$hour' and "
                        . "vm.shop_tran_id in ($txn_list_string) ) as api_txn_data ";
                
                $apiTXNs = $this->Slaves->query($txn_qry);
                
                $i = 1;// Setting initial counter
                $healthcount = 0;
                $batch_qry = array();// Initializing batch query array
                $missmatchedData_serversuccess = $missmatchedData_serverfailure = array();
                foreach ($apiTXNs as $txndata):

                    $transId = $txndata['api_txn_data']['ref_code'];
                    $transdate = $txndata['api_txn_data']['date'];
                    $transrefId = $txndata['api_txn_data']['vendor_refid'];
                    $transamount = $txndata['api_txn_data']['amount'];
                    $transStatuscode = $txndata['api_txn_data']['status'];
                    $transStatus = $statusList[$txndata['api_txn_data']['status']];
                    $transServiceId = $txndata['api_txn_data']['service_id'];
                    $vendor_actId = $txndata['api_txn_data']['id'];
                    $transVendorId = $txndata['api_txn_data']['vendor_id'];
                    
                    ob_start(); //to supress the printable output of below function call
                    $vendordata = $obj->$function_name($transId, $transdate, $transrefId);
                    ob_end_clean();
                    
                                       
                    if(empty($vendordata)){                                                
                        $healthcount++;
                    }else{
                        $healthcount = 0;
                    }
                    
                    if($healthcount > 5){
                        break;
                    }
                    $current_datetime = date('Y-m-d H:i:s');
                    $current_date = date('Y-m-d');
                    
                    $batch_qry['type'] = "UPDATE";
                    $batch_qry['predata'] = "UPDATE `api_transactions` SET ";
                    $batch_qry['vendor'] = true;
                    $batch_qry['data'][$transId] = array('vendor_status'=>$vendordata['status'],'server_status'=>$transStatus,'vendor_id'=>$vendorId,'flag'=>0);
                    
                    file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::transstaus: $transStatus, transvendorid $transVendorId, vendorid $vendorId\n" , FILE_APPEND | LOCK_EX);
                
                    if(strtolower($transStatus) != strtolower($vendordata['status'])){ // action in case of difference
                        if(strtolower($transStatus) == "success" && $vendordata['status'] == "failure" && $transVendorId == $vendorId){
                            //$this->Shop->addStatus($transId,$vendorId);
                            $message = "";
                            file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::failing txn here\n" , FILE_APPEND | LOCK_EX);
                			$batch_qry['data'][$transId]['flag'] = 1;
                            //$this->Retailer->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$transrefId."','".$transServiceId."','$vendorId','14','failure','".addslashes($message)."','".date("Y-m-d H:i:s")."')");
                            //$this->Shop->reverseTransaction($transId);                    
                            $missmatchedData_serversuccess['data'][] = array('transId'=>$transId,'refCode'=>$transrefId,'serverStatus'=>$transStatus,'venderStatus'=>$vendordata['status'],'date'=>$search_date);
                            //$transStatus = "failure";
                        }elseif(strtolower($transStatus) == "failure" && $vendordata['status'] == "success"  && $transVendorId == $vendorId){                            
                            //$this->Retailer->query("INSERT into `trans_pullback` (`vendors_activations_id`,`vendor_id`,`status`,`timestamp`,`pullback_by`,`pullback_time`,`reported_by`,`date`) values('$vendor_actId','$vendorId','$transStatuscode','$current_datetime','0','0000-00-00 00:00:00','System','$current_date')");
                            $trans_pullbackdata = array('vendors_activations_id'=>$vendor_actId,
                                                        'vendor_id'=>$vendorId,
                                                        'status'=>$transStatuscode,
                                                        'timestamp'=>$current_datetime,
                                                        'pullback_by'=>'0',
                                                        'pullback_time'=>'0000-00-00 00:00:00',
                                                        'reported_by'=>'System',
                                                        'date'=>$current_date);
                            $this->General->manage_transPullback($trans_pullbackdata);
                            file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::wrong failure by system\n" , FILE_APPEND | LOCK_EX);
                
                            $missmatchedData_serverfailure['data'][] = array('transId'=>$transId,'refCode'=>$transrefId,'serverStatus'=>$transStatus,'venderStatus'=>$vendordata['status'],'date'=>$search_date);
                        }
                        
                    }
                    
                    //$batch_qry['data'][$transId] = array('vendor_status'=>$vendordata['status'],'server_status'=>$transStatus,'vendor_id'=>$vendorId);
                    if ( ($i % 5) == 0 ): //process queries in batch
                        file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s') . " :: UPDATE :: $i :: vendor :: $vendorId :: calling batch query ".  json_encode($batch_qry)." \n", FILE_APPEND | LOCK_EX);
                        $this->run_batch($batch_qry);
                        $batch_qry = array();
                    endif;
                    
                    usleep(1000000/2);//sleep to set gap between to request
                    $i++;
                    
                endforeach;
                
                if ( !empty($batch_qry) ): // handled cases if last batch consist less then 100 queries                    
                    file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s') . " :: UPDATE :: $i :: vendor :: $vendorId :: calling batch query ".  json_encode($batch_qry)." \n", FILE_APPEND | LOCK_EX);
                    $this->run_batch($batch_qry);
                    $batch_qry = array();                    
                endif;                                
                
                //send mail of txn success at server and fail at vendor
                if(isset($missmatchedData_serversuccess['data']) && !empty($missmatchedData_serversuccess['data'])):
                     $missmatchedData_serversuccess['colhead'] = array_keys($missmatchedData_serversuccess['data'][0]);
                     $missmatchedData_serversuccess['colval'] = $missmatchedData_serversuccess['data'];
                     $missmatchedData_serversuccess['subject'] = "Auto reconcilation report for server success : ".$val['vendors']['shortForm'];
                     $this->sendFormatedmail($missmatchedData_serversuccess);
                 endif;
                
                 //send mail of txn fail at server and success at vendor
                 if(isset($missmatchedData_serverfailure['data']) && !empty($missmatchedData_serverfailure['data'])):
                     $missmatchedData_serverfailure['colhead'] = array_keys($missmatchedData_serverfailure['data'][0]);
                     $missmatchedData_serverfailure['colval'] = $missmatchedData_serverfailure['data'];
                     $missmatchedData_serverfailure['subject'] = "Auto reconcilation report for server failure : ".$val['vendors']['shortForm'];
                     $this->sendFormatedmail($missmatchedData_serverfailure);
                 endif;
                 
            endif;
            
        endforeach;

        $this->autoRender = false;
    }

    /**
     * This will clear all txn those are having vendor status as error
     * @param type $prev_day_num : default 1 i.e yesterday
     * @param type $batch_size : number of records to check on each execuution
     */
    function update_api_recon_clear_pending_in_batch($prev_day_num=1,$batch_size=100){
        $search_date = date('Y-m-d', strtotime('-'.$prev_day_num.' days'));        
        
        $date_condition = " date='$search_date'";
        $limit_qry = ($batch_size < 0) ? "" : " limit $batch_size";
        
        $date_range = explode(",",$prev_day_num);
        
        $statusList = array('0' => 'pending', '1' => 'success', '2' => 'failure', '3' => 'failure', '4' => 'success', '5' => 'success');//---status list
        if(count($date_range)>1){
            $start_date = date('Y-m-d', strtotime('-'.$date_range[0].' days')); 
            $end_date = date('Y-m-d', strtotime('-'.$date_range[1].' days')); 
            $date_condition = "date >= '$start_date' and date <= '$end_date'";
        }
        
        $api_txn_qry = "SELECT * FROM `api_transactions` LEFT JOIN `vendors` ON vendor_id = vendors.id WHERE $date_condition and (vendor_status not in ('success','failure','incomplete') or server_status not in ('success','failure','incomplete')) order by rand() $limit_qry";
        //$vendor_txn_list = $this->Retailer->query($api_txn_qry);

        //$qry_api_txn = "SELECT * FROM `api_transactions` where date='$search_date' and status='error'";
        $vendor_txn_list = $this->Slaves->query($api_txn_qry);
        $vendor_txn_ids = array();
        $vendor_list_arr = array();
        $vendor_txn_combination = array();
        foreach ($vendor_txn_list as $txn_data):
            $vendor_txn_ids[$txn_data['api_transactions']['txn_id']] = $txn_data['vendors']['id'];
            $vendor_list_arr[$txn_data['vendors']['id']] = $txn_data['vendors']['shortForm']; 
            $vendor_txn_combination[] = $txn_data['api_transactions']['txn_id']."_".$txn_data['vendors']['id'];
        endforeach;
                
        if(empty($vendor_txn_ids)):
            echo "no pending data";
            exit();
        endif;
        App::import('Controller', 'Recharges');
        $txn_list_string = implode("','", array_keys($vendor_txn_ids));
        $vendor_txn_combination_string = implode("','",$vendor_txn_combination);
        // Query to get combine data from vendor_activataion and vendor_messages
        $txn_qry = "SELECT * FROM (SELECT distinct va.id,va.date,(CASE WHEN va.vendor_id = vm.service_vendor_id THEN va.status ELSE '2' END) as status,"
                . " vm.vendor_refid,va.ref_code,va.amount,vm.service_id,va.vendor_id, vm.service_vendor_id, CONCAT(vm.shop_tran_id,'_',vm.service_vendor_id) as combo_id "
                . "FROM `vendors_activations` as va "
                . "LEFT JOIN vendors_messages as vm ON (vm.shop_tran_id=va.ref_code) "
                . "WHERE $date_condition AND "
                . "vm.shop_tran_id in ('$txn_list_string') ) as api_txn_data where combo_id in ('$vendor_txn_combination_string')";

        $apiTXNs = $this->Slaves->query($txn_qry);

        $healthcount = 0;
        $batch_qry = array();// Initializing batch query array
        $missmatchedData_serversuccess = $missmatchedData_serverfailure = array();
        $unhealthy_vendor_array = array();
        
        //echo "<pre>";print_r($vendor_txn_list);
        $obj = "";
        $obj = new RechargesController;
        $obj->constructClasses();
        $obj_funct_list = get_class_methods($obj);
        
        $_SERVER['DOCUMENT_ROOT'] = "/var/www/html/shops/app/webroot";
        
        foreach ($apiTXNs as $txndata){
            $transId = $txndata['api_txn_data']['ref_code'];
            $vendorId = $vendor_txn_ids[$transId];
            $shortForm = $vendor_list_arr[$vendorId];        
            
            $function_name = $shortForm . "TranStatus";
            

            if (in_array($function_name, $obj_funct_list)):        
                $transServiceId = $txndata['api_txn_data']['service_id'];    
                $transdate = $txndata['api_txn_data']['date'];
                $transrefId = $txndata['api_txn_data']['vendor_refid'];
                $transamount = $txndata['api_txn_data']['amount'];
                $transStatus = $statusList[$txndata['api_txn_data']['status']];
                $transVendorId = $txndata['api_txn_data']['vendor_id'];
                $transId = $txndata['api_txn_data']['ref_code'];
                $transStatuscode = $txndata['api_txn_data']['status'];
                $vendor_actId = $txndata['api_txn_data']['id'];                
                
                ob_start(); //to supress the printable output of below function call
                $vendordata = $obj->$function_name($transId,$transdate,$transrefId);
                ob_end_clean();
                
                file_put_contents('/mnt/logs/clear_api_auto_recon.txt', date('Y-m-d H:i:s') . " txnId $transId :: ".  json_encode($vendordata)." \n", FILE_APPEND | LOCK_EX);
                $current_datetime = date('Y-m-d H:i:s');
                $current_date = date('Y-m-d');

                $batch_qry['type'] = "UPDATE";
                $batch_qry['predata'] = "UPDATE `api_transactions` SET ";
                $batch_qry['vendor'] = true;
                
                $batch_qry['data'][$transId] = array('vendor_status'=>$vendordata['status'],'server_status'=>$transStatus,'vendor_id'=>$vendorId,'flag'=>0);

                file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::transstaus: $transStatus, transvendorid $transVendorId, vendorid $vendorId\n" , FILE_APPEND | LOCK_EX);
                
                if(strtolower($transStatus) != strtolower($vendordata['status'])){ // action in case of difference
                    
                    if(strtolower($transStatus) == "success" && $vendordata['status'] == "failure" && $transVendorId == $vendorId){
                        file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::failing txn here\n" , FILE_APPEND | LOCK_EX);
                
                    	//$this->Shop->addStatus($transId,$vendorId);
                        $message = "";
                        $batch_qry['data'][$transId]['flag'] = 1;
                        //$this->Retailer->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$transrefId."','".$transServiceId."','$vendorId','14','failure','".addslashes($message)."','".date("Y-m-d H:i:s")."')");
            			//$this->Shop->reverseTransaction($transId);                    
                        $missmatchedData_serversuccess['data'][] = array('transId'=>$transId,'refCode'=>$transrefId,'serverStatus'=>'failure','venderStatus'=>$vendordata['status'],'date'=>$search_date);
                        //$transStatus = "failure";
                    }elseif(strtolower($transStatus) == "failure" && $vendordata['status'] == "success"){
                        $trans_pullbackdata = array('vendors_activations_id'=>$vendor_actId,
                                                        'vendor_id'=>$vendorId,
                                                        'status'=>$transStatuscode,
                                                        'timestamp'=>$current_datetime,
                                                        'pullback_by'=>'0',
                                                        'pullback_time'=>'0000-00-00 00:00:00',
                                                        'reported_by'=>'System',
                                                        'date'=>$current_date);
                        $this->General->manage_transPullback($trans_pullbackdata);
                        file_put_contents('/mnt/logs/api_auto_recon_data.txt', date("Y-m-d H:i:s")."::$transId:: vendordata: ".json_encode($vendordata) ."::wrong failure by system\n" , FILE_APPEND | LOCK_EX);
                
                        $missmatchedData_serverfailure['data'][] = array('transId'=>$transId,'refCode'=>$transrefId,'serverStatus'=>$transStatus,'venderStatus'=>$vendordata['status'],'date'=>$search_date);
                    }

                }

                //$batch_qry['data'][$transId] = array('vendor_status'=>$vendordata['status'],'server_status'=>$transStatus,'vendor_id'=>$vendorId);
                
                if ( ($i % 2) == 0 ): //process queries in batch
                    file_put_contents('/mnt/logs/clear_api_auto_recon.txt', date('Y-m-d H:i:s') . " :: 5 min :: UPDATE :: $i :: vendor :: $vendorId :: calling batch query ".  json_encode($batch_qry)." \n", FILE_APPEND | LOCK_EX);
                    $this->run_batch($batch_qry);
                    $batch_qry = array();
                endif;
                
                usleep(1500000);//sleep to set gap between to request
                $i++;
            endif;
        }
        
        if ( !empty($batch_qry) ): // handled cases if last batch consist less then 100 queries                    
            file_put_contents('/mnt/logs/clear_api_auto_recon.txt', date('Y-m-d H:i:s') . "  :: 5 min :: UPDATE :: $i :: vendor :: $vendorId :: calling batch query ".  json_encode($batch_qry)." \n", FILE_APPEND | LOCK_EX);
            $this->run_batch($batch_qry);
            $batch_qry = array();                    
        endif;                                

        //send mail of txn success at server and fail at vendor
        if(isset($missmatchedData_serversuccess['data']) && !empty($missmatchedData_serversuccess['data'])):
             $missmatchedData_serversuccess['colhead'] = array_keys($missmatchedData_serversuccess['data'][0]);
             $missmatchedData_serversuccess['colval'] = $missmatchedData_serversuccess['data'];
             $missmatchedData_serversuccess['subject'] = "Auto reconcilation report for server success : ".$shortForm;
             $this->sendFormatedmail($missmatchedData_serversuccess);
         endif;

         //send mail of txn fail at server and success at vendor
         if(isset($missmatchedData_serverfailure['data']) && !empty($missmatchedData_serverfailure['data'])):
             $missmatchedData_serverfailure['colhead'] = array_keys($missmatchedData_serverfailure['data'][0]);
             $missmatchedData_serverfailure['colval'] = $missmatchedData_serverfailure['data'];
             $missmatchedData_serverfailure['subject'] = "Auto reconcilation report for server failure : ".$shortForm;
             $this->sendFormatedmail($missmatchedData_serverfailure);
         endif;
        
        $this->autoRender = false;
    }
    
    /*
     * API vendor Auto reconcilation 
     * should run for no_of_days_before_yesterday t+1, t+2
     */
    /*function auto_reconcilation($vendorId=null,$no_of_days_before_yesterday=2,$limitstart=null,$limitend=NULL){
        
        //get list of api vendor if not provided as parameter
        if(empty($vendorId)){            
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where (machine_id is null or machine_id < 1) and id in (5,8,19,34,35,36,48,56,58,62)";            
            //$API_vendor_qry = "SELECT id,shortForm FROM vendors where (machine_id is null or machine_id < 1) and id in (62)";            
            //exit();
        }else{            
            $API_vendor_qry = "SELECT id,shortForm FROM vendors where (machine_id is null or machine_id < 1) and id='$vendorId'";            
        }
        
        $API_vendorList = $this->Slaves->query($API_vendor_qry);
        
        // getting object of controller
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();
        $obj_funct_list = get_class_methods($obj);
        
        //---status list
        $statusList = array('0'=>'pending','1'=>'success','2'=>'failure','3'=>'failure','4'=>'success','5'=>'success');
        
        //----- generation date from 'no_of_days_before_yesterday'
        $search_date = date('Y-m-d', strtotime('-'.$no_of_days_before_yesterday.' days'));
        
        foreach($API_vendorList as $key=>$val):
            
            $vendorId = $val['vendors']['id'];
            $function_name = $val['vendors']['shortForm']."TranStatus";        
            
            if(in_array($function_name,$obj_funct_list)){
                 if(is_null($limitstart) || is_null($limitend)){
                     file_put_contents('/mnt/logs/api_auto_recon.txt', " forking process for :: vendor :: ".$val['vendors']['shortForm']." \n" , FILE_APPEND | LOCK_EX);
                     $this->auto_recon_process_fork($vendorId, $search_date, $no_of_days_before_yesterday, $limitstart,$limitend);
                     file_put_contents('/mnt/logs/api_auto_recon.txt', " after forking process for :: vendor :: ".$val['vendors']['shortForm']."  :: $vendorId : $search_date : $no_of_days_before_yesterday : $limitstart : $limitend \n" , FILE_APPEND | LOCK_EX);
                     continue;
                 }
                 
                 file_put_contents('/mnt/logs/api_auto_recon.txt', " Starting Recon chk for :: vendor :: ".$val['vendors']['shortForm']." \n" , FILE_APPEND | LOCK_EX);
                 //$txn_qry = "SELECT * FROM `vendors_activations` where vendor_id='".$val['vendors']['id']."' and date='$search_date'";
                 
                 $txn_qry = "SELECT * FROM (SELECT distinct va.id,va.date,(CASE WHEN va.vendor_id = vm.service_vendor_id THEN va.status ELSE '2' END) as status,"
                        . " vm.vendor_refid,va.ref_code,va.amount "
                        . "FROM `vendors_activations` as va "
                        . "LEFT JOIN vendors_messages as vm ON (vm.shop_tran_id=va.ref_code) "
                        . "WHERE date = '$search_date' AND vm.service_vendor_id=$vendorId) as api_txn_data "
                        . "LIMIT ".$limitstart." , ".$limitend;
                
                 $apiTXNs = $this->Slaves->query($txn_qry);
                 $i=1;
                 
                 $batch_qry = array();
                 $missmatchedData = array();
                 
                 foreach($apiTXNs as $txndata):
                    
                    $transId = $txndata['api_txn_data']['ref_code'];
                    $transdate = $txndata['api_txn_data']['date'];
                    $transrefId = $txndata['api_txn_data']['vendor_refid'];
                    $transamount = $txndata['api_txn_data']['amount'];
                    $transStatus = $statusList[$txndata['api_txn_data']['status']];
                    
                    ob_start();//to supress the printable output of below function call
                    $vendordata = $obj->$function_name($transId,$transdate,$transrefId);
                    ob_end_clean();
                    
                    $current_datetime = date('Y-m-d H:i:s');
                    $current_date = date('Y-m-d');
                    
                    if($no_of_days_before_yesterday == 2){
                        $batch_qry['type'] = "INSERT";
                        $batch_qry['predata'] = "INSERT INTO `api_transactions` (`vendor_id`,`txn_id`,`ref_code`,`amount`,`vendor_status`,`server_status`,`updated_time`,`date`) VALUES";
                        $batch_qry['data'][] = "('$vendorId','$transId','$transrefId','$transamount','".$vendordata['status']."','$transStatus','$current_datetime','$search_date')";
                    }elseif($no_of_days_before_yesterday > 2){
                        $batch_qry['type'] = "UPDATE";
                        $batch_qry['predata'] = "UPDATE `api_transactions` SET ";
                        $batch_qry['data'][$transId] = array('vendor_status'=>$vendordata['status'],'server_status'=>$transStatus);                        
                    }
                    
                    //if($statusList[$transStatus] != strtolower($vendordata['status'])){ // action in case of difference
                    if(strtolower($transStatus) != strtolower($vendordata['status'])){ // action in case of difference
                        $missmatchedData['data'][] = array('transId'=>$transId,'refCode'=>$transrefId,'serverStatus'=>$transStatus,'venderStatus'=>$vendordata['status'],'date'=>$search_date);                         
                    }
                    
                    if(($i % 25) == 0) {//process batch of 100 query together
                        file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s')." :: $i :: vendor :: $vendorId :: calling batch query \n" , FILE_APPEND | LOCK_EX);
                        $this->run_batch($batch_qry);
                        $batch_qry = array();
                    }
                    $i++;
                     
                 endforeach;
                 
                 if(!empty($batch_qry)){// handled cases if last batch consist less then 100 queries
                     file_put_contents('/mnt/logs/api_auto_recon.txt', date('Y-m-d H:i:s')." :: $i :: vendor :: $vendorId :: calling batch query \n" , FILE_APPEND | LOCK_EX);
                     $this->run_batch($batch_qry);
                     $batch_qry = array();
                 }
                 
                 if(isset($missmatchedData['data']) && !empty($missmatchedData['data'])){
                     $missmatchedData['colhead'] = array_keys($missmatchedData['data'][0]);
                     $missmatchedData['colval'] = $missmatchedData['data'];
                     $missmatchedData['subject'] = "Auto reconcilation report for ".$val['vendors']['shortForm'];
                     $this->sendFormatedmail($missmatchedData);
                 }                 
            }
        endforeach;
        $this->autoRender = false;
    } */   

    /*
     * send html formated mail
     */
    function sendFormatedmail($data=array()){
        if(!empty($data)){            
            $colhead = $datarow = $reportdata = "";
            
            foreach($data['colhead'] as $colh):
                $colhead .="<th>".$colh."</th>";
            endforeach;
            
            $mail_report = "<table border='1' >"; 
            $mail_report .= "<tr>$colhead</tr>";
            
            foreach($data['colval'] as $colv):
                $reportdata .= "<tr>";
                foreach ($colv as $row):
                    $reportdata .= "<td>".$row."</td>";
                endforeach;
                $reportdata .= "</tr>";
            endforeach;
            
            $mail_report .= $reportdata;
            $mail_report .= "</table>";
            
            $this->General->sendMails($data['subject'],$mail_report,array('nandan@mindsarray.com','cc.support@mindsarray.com','naziya@mindsarray.com'),'mail');
        }
    }
    
    /*
     * fork multiple process for auto_reconcilation limit
     */
    function auto_recon_process_fork($vendorId=null,$search_date,$no_of_days_before_yesterday=2,$limitstart=null,$limitend=NULL){
        //$total_cnt_qry = "SELECT count(1) as total FROM `vendors_activations` where vendor_id='".$vendorId."' and date='$search_date'";
        file_put_contents('/mnt/logs/api_auto_recon.txt', " inside auto_recon_process_fork  \n" , FILE_APPEND | LOCK_EX);
        $total_cnt_qry = "SELECT count(1) as total FROM (SELECT distinct va.id,va.date,(CASE WHEN va.vendor_id = vm.service_vendor_id THEN 1 ELSE 2 END) as status, vm.vendor_refid "
                         . "FROM `vendors_activations` as va "
                         . "LEFT JOIN vendors_messages as vm ON (vm.shop_tran_id=va.ref_code) "
                         . "WHERE date = '$search_date' AND vm.service_vendor_id=$vendorId) as api_txn_data";
        
        $total_cnts = $this->Slaves->query($total_cnt_qry);
        $totalrecord = isset($total_cnts[0][0]['total']) ? $total_cnts[0][0]['total'] : 0;
        $limitgap = 1000;
        $attempt = 0;
        while($totalrecord > 0):
            $limitstart = $limitgap * $attempt;
            $limitend = $limitgap;
            $attempt ++;
            $totalrecord -= 1000;
            //echo "<br>IN fork method : ".$vendorId."/".$no_of_days_before_yesterday."/".$limitstart."/".$limitend;
            //shell_exec("nohup lynx http://localhost/crons/auto_reconcilation/".$vendorId."/".$no_of_days_before_yesterday."/".$limitstart."/".$limitend);
            file_put_contents('/mnt/logs/api_auto_recon.txt', " forking  api_auto_recon.sh with :: $vendorId :: $no_of_days_before_yesterday :: $limitstart :: $limitend \n" , FILE_APPEND | LOCK_EX);
            shell_exec("nohup sh  /var/www/html/shops/app/webroot/scripts/api_auto_recon.sh $vendorId $no_of_days_before_yesterday $limitstart $limitend");
        endwhile;
    }
    
    /*
     * Execute query in batch for auto recon
     */
    function run_batch($querydata = array()){
        $mainQry = "";
        $curTimestamp = date("Y-m-d H:i:s");
        if($querydata['type']){
            switch (strtoupper($querydata['type'])):
                case 'INSERT':
                    $mainQry .= $querydata['predata'];
                    $mainQry .= implode(",", $querydata['data']);
                    break;
                case 'UPDATE':
                    $mainQry .= $querydata['predata'];
                    $mainQry .= (isset($querydata['vendor']) && $querydata['vendor'])? " vendor_status=(CASE CONCAT(txn_id,'_',vendor_id) ":" vendor_status=(CASE txn_id ";
                    
                    foreach ($querydata['data'] as $txnId => $txndata):
                        $txnId = isset($txndata['vendor_id'])? $txnId."_".$txndata['vendor_id']:$txnId;
                        $mainQry .= "WHEN '$txnId' THEN '".$txndata['vendor_status']."' ";
                    endforeach;
                    
                    $mainQry .= " END) ,";
                    $mainQry .= (isset($querydata['vendor']) && $querydata['vendor'])? " server_status=(CASE CONCAT(txn_id,'_',vendor_id) ":" server_status=(CASE txn_id ";
                    
                    foreach ($querydata['data'] as $txnId => $txndata):
                        $txnId = isset($txndata['vendor_id'])? $txnId."_".$txndata['vendor_id']:$txnId;
                        $mainQry .= "WHEN '$txnId' THEN '".$txndata['server_status']."' ";
                    endforeach;
                    
                    $mainQry .= " END) ,";
                    $mainQry .= (isset($querydata['vendor']) && $querydata['vendor'])? " flag=(CASE CONCAT(txn_id,'_',vendor_id) ":" flag=(CASE txn_id ";
                    
                    foreach ($querydata['data'] as $txnId => $txndata):
                        $txnId = isset($txndata['vendor_id'])? $txnId."_".$txndata['vendor_id']:$txnId;
                        $mainQry .= "WHEN '$txnId' THEN '".$txndata['flag']."' ";
                    endforeach;
                    
                    $mainQry .= " END) , updated_time='$curTimestamp' ";
                    $mainQry .= "WHERE txn_id in (".implode(",",array_keys($querydata['data'])).")";
                    
                    break;
            endswitch;
            if(!empty($mainQry)):
                    file_put_contents('/mnt/logs/api_auto_recon.txt', "\n Query :: $mainQry  \n" , FILE_APPEND | LOCK_EX);
                    $this->Retailer->query($mainQry);
            endif;            
        }
    }
	
	/*function updateRetailersPin(){
		
		$this->autoRender = false;
		
		
		$retailersRecord = $this->Slaves->query("
                                            SELECT retailer_id,r.mobile,sum(rl.sale) as total_sale, sum(rl.ussd_sale + rl.sms_sale) as ussd_sms_sale, (sum(rl.sale) - (sum(rl.ussd_sale + rl.sms_sale))) as sale_diff FROM `retailers_logs` as rl inner join retailers r on rl.retailer_id = r.id where date >= '2015-12-01' and date <= '2015-12-22' group by 1 having ussd_sms_sale > 0 and (((total_sale - ussd_sms_sale)/total_sale) < 0.1)");
		
		$date = date('Y-m-d');
		
		if($date == '2015-12-23'){
                
		foreach ($retailersRecord as $val){
			
			$password = $this->General->generatePassword(4);
			
			echo $password;
			
			echo "<br/>";
			
			
			
			$crypt = $this->Auth->password($password);
			
			echo $crypt;
			die;
			
			$this->Retailer->query("update users set password = '".$crypt."' where mobile = '".$val['r']['mobile']."'");
			
			if($val[0]['sale_diff']>0){
			$message = 	"Aapke App Ki Security ke liye appka Password reset kiya gaya hai.Agar aapko PAY1 App mei login problem ho raha hain to app 'GENERATE NEW PIN' se naya password chun sakte hain.\n PAY1";
			$this->General->sendMessage($val['r']['mobile'], $message, 'shops');
			}
			$this->General->logData("/mnt/logs/updatepin.txt",date("Y-m-d H:i:s")."Mob Number => ".$val['r']['mobile']);
			
			//$message = 	"You can login to Pay1 Channel Partner Android App with pin: $password. Kindly, change your pin from the app.";
			
			//$this->General->sendMessage($val['r']['mobile'], $message, 'shops');
			
		///}
		
		
	}
	}
	
	
	echo "Password Updated Successfully!!!";
	}*/
	
	function updateEarningLogs($date){
		if(empty($date))return;
		$last_date = $date;

		$comm = $this->Slaves->query("SELECT sum(vendors_activations.amount) as sale,sum(vendors_activations.amount*vendors_activations.discount_commission/100) as expected, vendors_activations.vendor_id, vendors_activations.date, earnings_logs.opening,earnings_logs.closing FROM vendors_activations left join earnings_logs ON (earnings_logs.vendor_id = vendors_activations.vendor_id AND earnings_logs.date = vendors_activations.date) WHERE vendors_activations.product_id != 44 AND vendors_activations.status != 2 AND vendors_activations.status != 3 AND vendors_activations.date = '$last_date' group by vendors_activations.vendor_id,vendors_activations.date");

		/*$reversals = $this->Retailer->query("SELECT SUM( shop_transactions.amount ) AS reversal, vendors_activations.vendor_id
FROM vendors_activations
INNER JOIN shop_transactions ON ( vendors_activations.shop_transaction_id = shop_transactions.ref2_id
AND shop_transactions.type =11
AND shop_transactions.type_flag =1 )
WHERE shop_transactions.date = '$last_date' AND vendors_activations.product_id != 44
AND vendors_activations.date >= '".date('Y-m-d',strtotime($last_date."- 3 days"))."'
GROUP BY vendors_activations.vendor_id");*/

		$data = array();
		foreach($comm as $com){
			$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['sale'] = $com['0']['sale'];
			$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['expected'] = $com['0']['expected'];
		}
			
		/*foreach($reversals as $revs){
			$data[$revs['vendors_activations']['vendor_id']][$last_date]['reversal'] = $revs['0']['reversal'];
		}*/
			
			
		foreach($data as $key=>$dt){
			$this->General->printArray($dt);
			if($this->Retailer->query("UPDATE earnings_logs SET sale='".$dt[$date]['sale']."',expected_earning='".$dt[$date]['expected']."' WHERE vendor_id = $key AND date = '$last_date'")){
					
			}
		}
			
		
			
	}
	
	/*function retailerBonus(){
		
		$date = '2015-11-03';

        $data = $this->Slaves->query("SELECT sale as amts, retailer_id, retailers.mobile"
										. " FROM  "
										. "`retailers_logs` ,retailers   "
								     	. "WHERE retailers.id = retailer_id AND date = '$date' AND retailer_id IN (15,25,30,34,42,46,47,61,63,64,101,104,117,133,195,199,241,268,278,279,301,308,309,318,350,360,374,444,449,456,467,471,473,480,486,487,488,490,493,507,514,515,541,585,598,608,615,617,636,639,642,660,677,695,710,762,766,769,773,796,820,829,831,843,844,869,892,893,900,917,926,944,949,950,953,955,959,965,982,1001,1011,1015,1029,1047,1065,1083,1091,1107,1113,1126,1196,1239,1260,1293,1333,1343,1418,1507,1518,1530,1538,1610,1671,1682,1714,1903,2181,2186,2223,2246,2289,2294,2316,2362,2366,2402,2427,2437,2440,2442,2489,2502,2507,2509,2512,2523,2537,2670,2693,2751,2805,2822,2876,2881,2934,2999,3009,3040,3047,3188,3189,3198,3216,3232,3236,3266,3363,3403,3425,3514,3530,3593,3606,3613,3676,3718,3721,3759,3783,3792,3821,3831,3843,3852,3859,3933,3957,3998,4012,4018,4051,4070,4140,4181,4198,4220,4334,4404,4442,4473,4500,4551,4590,4629,4642,4690,4735,4761,4817,4896,4928,4974,4999,5013,5022,5035,5057,5136,5181,5211,5240,5249,5274,5283,5287,5292,5294,5324,5333,5344,5360,5363,5364,5436,5445,5492,5497,5531,5567,5591,5643,5663,5665,5676,5686,5726,5730,5756,5763,5826,5847,5854,5859,5862,5914,5946,5962,5967,6016,6027,6061,6072,6091,6129,6134,6161,6163,6170,6205,6251,6270,6326,6332,6421,6425,6440,6452,6483,6506,6538,6559,6560,6616,6624,6635,6668,6731,6763,6829,6860,6864,6868,6871,6876,6971,7046,7057,7089,7269,7281,7290,7341,7487,7511,7557,7561,7565,7606,7627,7664,7671,7693,7704,7757,7763,7770,7778,7816,7820,7847,7862,7866,7940,7951,7953,7965,7968,7973,7995,8047,8069,8071,8073,8078,8090,8105,8137,8163,8221,8230,8233,8235,8238,8241,8250,8282,8294,8295,8305,8320,8376,8414,8420,8424,8449,8514,8542,8548,8572,8583,8584,8587,8611,8623,8624,8634,8640,8642,8655,8687,8740,8795,8806,8846,8873,8946,8968,9026,9085,9137,9190,9197,9220,9238,9285,9295,9296,9405,9418,9446,9517,9523,9533,9549,9634,9645,9667,9669,9673,9710,9711,9798,9896,9910,9948,9952,10016,10068,10069,10147,10214,10254,10345,10397,10399,10446,10462,10466,10475,10478,10479,10483,10484,10496,10500,10501,10504,10529,10530,10557,10573,10607,10711,10725,10761,10765,10774,10786,10787,10815,10870,10904,10910,10928,10929,10946,10952,10961,10989,10992,11004,11052,11084,11089,11125,11136,11209,11218,11221,11244,11245,11317,11338,11342,11398,11411,11413,11477,11487,11489,11495,11542,11610,11628,11648,11649,11652,11653,11655,11657,11676,11726,11768,11785,11791,11799,11824,11852,11918,12015,12046,12086,12091,12096,12107,12128,12132,12163,12179,12254,12265,12304,12327,12340,12369,12375,12382,12419,12421,12429,12446,12447,12449,12451,12454,12514,12583,12589,12593,12606,12615,12640,12642,12671,12672,12695,12753,12759,12818,12842,12914,12925,12952,12980,12995,13017,13049,13067,13077,13167,13171,13172,13183,13208,13210,13217,13219,13262,13357,13415,13431,13513,13521,13537,13595,13625,13649,13669,13677,13718,13724,13727,13741,13748,13754,13766,13775,13820,13841,13902,13920,13947,13992,14021,14030,14034,14050,14056,14071,14093,14100,14113,14117,14124,14136,14150,14154,14180,14199,14237,14247,14249,14263,14275,14287,14288,14302,14309,14325,14333,14403,14417,14493,14501,14534,14551,14615,14695,14696,14734,14761,14763,14781,14862,14980,14988,14997,15030,15072,15118,15140,15196,15209,15258,15366,15374,15378,15406,15441,15445,15460,15465,15501,15533,15599,15619,15643,15675,15708,15714,15716,15721,15728,15757,15762,15764,15773,15799,15826,15836,15863,15869,15883,15931,15940,15952,15967,15979,15986,15996,16036,16062,16082,16085,16086,16111,16165,16168,16170,16183,16194,16199,16238,16246,16270,16278,16282,16291,16307,16334,16356,16384,16405,16437,16455,16468,16479,16516,16536,16556,16559,16590,16601,16642,16645,16652,16659,16693,16703,16732,16754,16764,16767,16795,16800,16801,16803,16811,16813,16822,16828,16833,16836,16839,16841,16846,16850,16857,16922,16924,16936,16941,16951,16968,16976,16981,16984,16987,16988,16997,17011,17035,17071,17072,17169,17175,17192,17210,17217,17235,17244,17279,17289,17310,17334,17346,17394,17400,17415,17454,17488,17498,17524,17525,17527,17529,17532,17537,17579,17589,17628,17632,17639,17644,17732,17734,17744,17820,17836,17855,17865,17883,17896,17936,17937,17950,17980,17996,17997,18001,18090,18113,18119,18154,18194,18235,18246,18251,18256,18270,18277,18328,18345,18360,18365,18377,18387,18395,18403,18410,18414,18443,18463,18467,18480,18484,18495,18499,18511,18521,18534,18541,18596,18608,18627,18639,18656,18677,18681,18688,18722,18724,18733,18769,18778,18785,18794,18827,18838,18904,18905,18911,18914,18935,18943,18944,18950,18976,18988,18990,18997,19002,19004,19006,19016,19017,19027,19046,19066,19068,19085,19147,19151,19163,19187,19189,19194,19199,19224,19228,19284,19330,19357,19370,19372,19377,19378,19407,19411,19429,19433,19464,19469,19549,19552,19557,19565,19583,19616,19621,19646,19647,19652,19661,19676,19754,19793,19866,19873,19884,19901,19918,19924,19980,19999,20016,20030,20033,20079,20095,20136,20223,20250,20290,20317,20328,20342,20348,20351,20353,20367,20370,20393,20411,20431,20436,20447,20448,20449,20456,20464,20465,20473,20483,20489,20509,20525,20536,20546,20592,20601,20640,20646,20662,20663,20748,20761,20771,20773,20776,20798,20799,20825,20837,20890,20900,20905,20949,20978,21014,21030,21057,21064,21073,21087,21172,21187,21196,21207,21214,21219,21239,21255,21280,21307,21330,21340,21345,21389,21422,21425,21463,21473,21479,21517,21550,21562,21572,21635,21684,21686,21714,21787,21795,21800,21807,21874,21938,21988,21998,22002,22025,22037,22048,22071,22086,22096,22098,22178,22191,22222,22227,22241,22284,22301,22358,22371,22372,22379,22425,22461,22507,22518,22542,22565,22580,22581,22595,22598,22601,22640,22649,22652,22657,22664,22681,22696,22705,22718,22753,22757,22783,22796,22829,22837,22839,22841,22845,22851,22866,22909,22927,22948,22950,22985,23001,23027,23040,23072,23073,23127,23157,23182,23226,23236,23242,23246,23261,23346,23366,23367,23373,23392,23396,23486,23489,23491,23499,23509,23537,23558,23569,23570,23575,23587,23598,23600,23632,23639,23662,23667,23668,23707,23709,23715,23719,23723,23746,23751,23759,23766,23774,23802,23803,23804,23806,23823,23834,23847,23854,23860,23869,23891,23897,23901,23906,23920,23931,23938,23939,23952,23963,23965,23972,23991,23993,24027,24034,24038,24081,24084,24102,24111,24114,24123,24126,24183,24202,24244,24287,24322,24348,24349,24352,24357,24370,24381,24393,24396,24432,24446,24464,24475,24478,24516,24537,24565,24611,24615,24616,24658,24665,24668,24676,24700,24739,24744,24781,24797,24819,24820,24829,24893,24900,24906,24935,24987,24988,24999,25109,25188,25197,25198,25216,25228,25255,25274,25281,25284,25330,25374,25378,25394,25416,25430,25436,25505,25508,25517,25566,25567,25570,25608,25617,25647,25669,25670,25676,25680,25716,25726,25735,25812,25816,25872,25925,25983,26032,26058,26094,26161,26173,26195,26206,26212,26225,26229,26247,26249,26250,26255,26270,26277,26282,26287,26296,26304,26324,26335,26369,26377,26396,26414,26429,26441,26448,26479,26494,26495,26503,26523,26562,26563,26593,26600,26617,26621,26627,26646,26666,26677,26705,26711,26741,26744,26789,26791,26849,26851,26868,26871,26880,26884,26899,26921,26922,26933,26949,26957,26963,26966,26976,27003,27008,27038,27050,27058,27062,27084,27093,27096,27115,27118,27150,27153,27154,27161,27167,27180,27189,27195,27211,27223,27252,27277,27312,27327,27356,27381,27386,27391,27410,27455,27464,27509,27551,27556,27563,27565,27576,27584,27594,27603,27618,27629,27635,27701,27730,27747,27774,27775,27800,27814,27865,27880,27929,27960,27969,27993,28020,28038,28044,28049,28054,28057,28058,28060,28065,28075,28119,28129,28139,28149,28153,28159,28173,28185,28197,28198,28237,28242,28268,28270,28280,28297,28298,28321,28329,28363,28385,28400,28425,28431,28455,28462,28490,28495,28497,28544,28549,28561,28594,28597,28598,28653,28674,28699,28733,28769,28783,28789,28793,28804,28860,28892,28928,28944,28962,28982,29004,29079,29081,29089,29107,29119,29168,29214,29222,29223,29231,29261,29274,29308,29314,29331,29338,29399,29400,29425,29445,29457,29473,29495,29535,29542,29546,29551,29560,29592,29608,29619,29626,29630,29665,29666,29676,29706,29723,29729,29753,29758,29760,29768,29773,29818,29830,29897,29907,29938,29943,29944,29954,29983,29986,30011,30020,30023,30026,30052,30055,30060,30095,30130,30141,30155,30159,30163,30176,30193,30231,30233,30283,30316,30328,30339,30342,30348,30352,30354,30358,30396,30432,30435,30450,30455,30458,30460,30477,30487,30508,30529,30543,30545,30560,30598,30600,30641,30677,30696,30766,30772,30788,30808,30809,30822,30831,30849,30851,30862,30873,30891,30898,30935,30941,30950,30964,30970,30990,30994,31021,31036,31055,31086,31127,31149,31160,31175,31180,31186,31192,31232,31248,31263,31286,31289,31304,31345,31356,31374,31377,31382,31397,31417,31442,31481,31491,31508,31525,31565,31572,31576,31595,31625,31654,31728,31760,31782,31797,31798,31806,31837,31873,31877,31879,31880,31897,31910,31941,31943,31982,31985,31986,31998,32014,32037,32040,32061,32071,32090,32121,32144,32151,32171,32188,32189,32226,32240,32253,32269,32274,32292,32351,32370,32383,32393,32398,32399,32407,32408,32439,32445,32458,32490,32517,32528,32535,32552,32558,32561,32608,32610,32636,32667,32737,32748,32755,32763,32764,32821,32855,32879,32893,32898,32899,32986,32999,33018,33022,33024,33043,33046,33051,33057,33060,33070,33072,33080,33084,33090,33110,33131,33133,33146,33183,33213,33246,33278,33289,33301,33319,33348,33372,33378,33443,33463,33542,33566,33605,33612,33622,33643,33657,33697,33739,33782,33803,33808,33826,33882,33883,33896,33899,33907,33961,33996,34003,34045,34072,34086,34113,34120,34148,34152,34159,34190,34191,34207,34224,34264,34275,34316,34344,34355,34361,34401,34426,34441,34445,34449,34469,34475,34479,34480,34485,34487,34489,34494,34500,34513,34525,34592,34613,34637,34644,34665,34698,34710,34729,34743,34777,34785,34829,34841,34864,34866,34877,34921,34928,34934,34945,34965,34996,35018,35024,35068,35090,35099,35108,35117,35147,35186,35216,35228,35232,35238,35255,35268,35295,35313,35360,35395,35401,35411,35426,35431,35443,35463,35548,35571,35587,35610,35622,35623,35627,35637,35647,35650,35662,35664,35670,35683,35701,35703,35712,35735,35742,35753,35770,35792,35799,35804,35867,35921,35934,35935,35939,35952,35953,35964,35977,35989,35991,36002,36014,36016,36021,36046,36069,36076,36099,36103,36109,36125,36144,36205,36206,36208,36212,36229,36239,36278,36291,36296,36300,36305,36315,36324,36359,36361,36392,36442,36471,36477,36522,36543,36547,36558,36559,36584,36592,36623,36626,36633,36681,36684,36734,36738,36745,36746,36754,36769,36775,36784,36792,36807,36809,36815,36816,36829,36835,36855,36860,36867,36878,36885,36888,36895,36908,36911,36912,36943,36975,36980,36987,37019,37023,37027,37029,37037,37054,37063,37073,37075,37080,37096,37104,37117,37160,37166,37172,37199,37203,37218,37219,37222,37233,37235,37277,37282,37291,37298,37299,37307,37313,37342,37343,37351,37359,37365,37372,37383,37400,37418,37424,37429,37441,37451,37452,37467,37477,37490,37500,37502,37508,37509,37519,37525,37526,37534,37536,37540,37545,37547,37565,37567,37612,37615,37629,37645,37647,37652,37653,37655,37665,37667,37701,37712,37715,37740,37741,37755,37758,37761,37762,37763,37789,37791,37803,37809,37812,37814,37870,37886,37897,37898,37903,37907,37953,37954,37971,37981,37992,38004,38012,38021,38048,38050,38058,38061,38062,38097,38116,38137,38159,38161,38169,38172,38184,38195,38202,38208,38215,38233,38247,38248,38259,38273,38288,38304,38313,38340,38343,38372,38374,38380,38391,38422,38440,38444,38453,38513,38522,38524,38525,38565,38578,38584,38585,38598,38599,38620,38641,38654,38667,38700,38705,38719,38735,38744,38762,38765,38771,38783,38795,38811,38815,38820,38822,38831,38853,38855,38889,38893,38922,38932,38933,38943,38999,39002,39024,39031,39033,39046,39074,39076,39102,39126,39127,39147,39149,39150,39162,39180,39207,39222,39226,39231,39234,39248,39271,39287,39313,39315,39325,39341,39346,39357,39358,39364,39374,39383,39388,39401,39403,39404,39432,39453,39454,39469,39480,39487,39492,39523,39525,39526,39536,39537,39555,39566,39581,39586,39614,39620,39643,39644,39650,39670,39676,39677,39679,39686,39705,39730,39758,39783,39788,39798,39800,39805,39813,39827,39854,39889,39894,39897,39921,39923,39927,39942,39946,39948,39972,39992,39997,40000,40013,40018,40025,40026,40035,40038,40043,40046,40057,40061,40067,40082,40085,40094,40129,40132,40133,40164,40172,40173,40185,40190,40192,40202,40206,40212,40258,40273,40275,40277,40280,40289,40293,40294,40301,40321,40322,40338,40354,40363,40371,40456,40478,40482,40500,40505,40521,40530,40543,40564,40577,40606,40612,40617,40623,40656,40701,40735,40760,40762,40767,40776,40783,40790,40805,40807,40833,40855,40857,40858,40906,40935,40938,40977,40990,41042,41052,41055,41070,41106,41120,41123,41126,41145,41147,41148,41161,41164,41204,41234,41255,41274,41322,41340,41349,41352,41374,41377,41386,41394,41397,41413,41420,41426,41430,41461,41497,41523,41526,41546,41589,41595,41622,41627,41654,41667,41689,41694,41712,41722,41724,41774,41777,41780,41783,41813,41821,41841,41858,41879,41897,41903,41931,41990,42013,42017,42034,42058,42070,42071,42086,42098,42114,42122,42133,42150,42153,42158,42166,42173,42232,42291,42321,42365,42374,42384,42391,42400,42436,42438,42476,42477,42495,42508,42515,42517,42595,42605,42630,42649,42652,42663,42665,42675,42676,42679,42686,42698,42714,42734,42789,42804,42807,42812,42868,42870,42889,42893,42895,42897,42913,42932,42948,42972,42973,42978,42994,43005,43006,43018,43021,43028,43043,43050,43052,43056,43067,43088,43099,43100,43109,43148,43154,43161,43173,43176,43180,43182,43193,43256,43260,43285,43293,43301,43302,43306,43320,43326,43327,43332,43348,43369,43370,43373,43385,43406,43409,43414,43420,43422,43427,43463,43480,43501,43521,43563,43580,43593,43630,43644,43648,43677,43701,43706,43715,43723,43725,43726,43730,43750,43789,43835,43862,43880,43907,43928,43988,44061,44068,44084,44096,44135,44137,44143,44165,44191,44215,44287,44292,44295,44300,44307,44308,44348,44371,44433,44437,44439,44454,44484,44490,44522,44553,44558,44595,44601,44627,44649,44673,44732,44773,44791,44834,44837,44849,44850,44875,44896,44899,44911,44931,44981,45008,45009,45019,45049,45053,45084,45094,45117,45172,45182,45206,45211,45213,45216,45217,45246,45257,45262,45284,45297,45303,45318,45321,45325,45331,45334,45349,45368,45377,45382,45410,45418,45429,45448,45549,45567,45574,45594,45608,45646,45665,45669,45675,45684,45707,45728,45746,45815,45825,45840,45848,45853,45868,45872,45896,45909,45927,45957,45965,45974,45985,46004,46017,46020,46060,46068,46069,46093,46098,46105,46121,46128,46145,46165,46182,46193,46218,46238,46276,46282,46316,46344,46350,46364,46384,46419,46441,46449,46478,46522,46538,46586,46607,46610,46646,46692,46696,46699,46715,46717,46723,46727,46736,46740,46741,46767,46826,46836,46872,46875,46881,46892,46895,46901,46917,46922,46926,46945,46953,46962,46964,46996,47000,47025,47037,47040,47042,47055,47057,47148,47191,47222,47239,47277,47298,47328,47334,47338,47362,47363,47396,47404,47418,47439,47440,47469,47488,47554,47560,47575,47597,47600,47619,47620,47640,47650,47685,47686,47689,47746,47747,47764,47769,47788,47801,47812,47813,47836,47842,47875,47886,47897,47902,47907,47932,47945,47946,47953,47966,48002,48037,48053,48062,48085,48096,48101,48114,48132,48139,48148,48164,48178,48185,48187,48192,48193,48196,48211,48233,48295,48320,48330,48336,48337,48338,48368,48382,48383,48405,48416,48441,48505,48511,48539,48593,48613,48614,48617,48624,48638,48647,48650,48652,48654,48658,48692,48707,48780,48789,48801,48808,48810,48813,48814,48821,48833,48840,48853,48856,48877,48929,48939,48954,48961,48963,48967,48975,48992,48998,49028,49032,49034,49036,49040,49054,49061,49062,49063,49065,49069,49087,49089,49113,49117,49122,49139,49140,49158,49192,49203,49237,49243,49251,49292,49328,49334,49335,49338,49366,49371,49382,49409,49415,49458,49501,49514,49519,49531,49549,49555,49561,49569,49583,49586,49591,49592,49629,49630,49641,49642,49657,49684,49685,49703,49740,49749,49758,49760,49765,49780,49792,49796,49799,49826,49827,49828,49830,49859,49916,49920,49931,50038,50049,50065,50094,50123,50131,50137,50169,50199,50206,50213,50226,50249,50255,50263,50271,50273,50294,50359)  ");
		
		$total = 0;
        $MsgTemplate = $this->General->LoadApiBalance();        
        foreach ($data as $dt) {

            $amt = $dt['retailers_logs']['amts']*0.01;
//            $sms = "";
//			$sms .="Thanks A Lot!";
//            $sms .= "It was a pleasure doing Business with you on BIG DAY SALE. Your Pay1 a/c has been credited with CASH BONUS $amt.";
//			$sms .= "\n - Pay1";
		
            $paramdata['AMOUNT'] = $amt;
            $content =  $MsgTemplate['Retailer_Bonus_MSG'];
            $sms = $this->General->ReplaceMultiWord($paramdata,$content);            
                        
            $trans_id = $this->Shop->shopTransactionUpdate(REFUND, $amt, $dt['retailers_logs']['retailer_id'], RETAILER);
            $bal = $this->Shop->shopBalanceUpdate($amt, 'add', $dt['retailers_logs']['retailer_id'], RETAILER);
            $this->Shop->addOpeningClosing($dt['retailers_logs']['retailer_id'], RETAILER, $trans_id, $bal - $amt, $bal);
            $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . RETAILER . ",$trans_id,$amt,1,'" . addslashes($sms) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
            $this->General->sendMessage($dt['retailers']['mobile'], $sms, 'shops');
            $total+=$amt;
			
        }
		
        $mail_body = "Incentive given to retailers: $total<br/>Total retailers".count($data);
        $this->General->sendMails("Incentive Retailers", $mail_body, array('tadka@mindsarray.com', 'pravin@mindsarray.com','nandan@mindsarray.com'), 'mail');
        $this->autoRender = false;
		
	}*/
	
	
	function sendSupplierTargets(){
		$query="SELECT devices_data.vendor_id, devices_data.opr_id, devices_data.inv_supplier_id,inv_planning_sheet.max_sale_capacity,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims, inv_modem_planning_sheet.target
FROM inv_planning_sheet 
INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id)
INNER JOIN inv_modem_planning_sheet ON (inv_modem_planning_sheet.supplier_operator_id = devices_data.supplier_operator_id AND devices_data.vendor_id =inv_modem_planning_sheet.vendor_id)
WHERE devices_data.sync_date = '".date('Y-m-d')."' group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0) order by vendor_id";
		$data = $this->Retailer->query($query);
		
		$vendorData = array();
		foreach($data as $dt){
			$vendor_id = $dt['devices_data']['vendor_id'];
			$opr_id = $dt['devices_data']['opr_id'];
			$weight = ($dt['inv_modem_planning_sheet']['target'] - $dt['0']['totalsale'])/$dt['0']['sims'];
			$vendorData[$vendor_id][$opr_id][$weight] =  $dt['devices_data']['inv_supplier_id'];
		}
		
		foreach($vendorData as $vendor_id => $vdata){
			
			$data_to_send = array();
			foreach($vdata as $opr_id => $opdata){
				krsort($opdata);
				$data_to_send[$opr_id] = array_values($opdata);
			}
			$query = "query=settarget&data=".urlencode(json_encode($data_to_send));
			$this->Shop->modemRequest($query,$vendor_id);
		}
		
		$this->autoRender = false;
		
	}
        
        function clean_olddata($date){
            $date = date('Y-m-d', strtotime($date . '-90 days'));
            $rowcount_log = 1;
            $rowcount = 1;
            
            try{
                $datasource = $this->Retailer->getDataSource();
                $datasource->begin();

                $vendors_activations_cnt_data = $this->Slaves->query("SELECT count(1) as cnt FROM vendors_activations WHERE date = DATE_SUB(curdate(),INTERVAL 90 DAY)");
                $vendors_activations_logs_cnt_data = $this->Slaves->query("SELECT count(1) as cnt FROM vendors_activations_logs WHERE date = DATE_SUB(curdate(),INTERVAL 90 DAY)");
                if($vendors_activations_cnt_data[0][0]['cnt'] > $vendors_activations_logs_cnt_data[0][0]['cnt']){
                    while($rowcount_log > 0){
                        $qry = "delete from vendors_activations_logs where date = '$date' order by id asc limit 2000";
                        $result = $this->Retailer->query($qry);
                        $rowcount_log = $this->Retailer->getAffectedRows();
                    }
                
                    $qry2 = "INSERT INTO vendors_activations_logs (SELECT * FROM vendors_activations WHERE date = '$date' order by id asc)";
                    if(!$this->Retailer->query($qry2)){
                        throw new Exception();
                    }
                }                

                $vendor_a_count = $vendors_activations_cnt_data;
                $vendor_a_l_count = $vendors_activations_logs_cnt_data;
                
                if($vendor_a_count[0][0]['cnt'] > 0) {
                    if($vendor_a_count[0][0]['cnt'] == $vendor_a_l_count[0][0]['cnt']) {
                        while($rowcount > 0){
                            $query = "delete from vendors_activations where date = '$date' order by id asc limit 2000";
                            $res = $this->Retailer->query($query);
                            $rowcount = $this->Retailer->getAffectedRows();
                        }
                    }                    
                }                
                if(!$datasource->commit()){
                    $this->General->sendMails("cleaning vendors_activation status : ".$date, "Some issue occured while cleaning in datasource val : ".  json_encode($datasource), array('nandan@mindsarray.com'), 'mail');
                }
                $this->General->sendMails("cleaning vendors_activation status : ".$date, " cleaned table successfully. datasource val : ".  json_encode($datasource), array('nandan@mindsarray.com'), 'mail');
            } catch (Exception $ex) {
                
                if(!$datasource->rollback()){
                    $this->General->sendMails("cleaning vendors_activation status : ".$date, "(rollback) Some issue occured while cleaning in datasource val : ".  json_encode($datasource), array('nandan@mindsarray.com'), 'mail');
                }
                $this->General->sendMails("cleaning vendors_activation status : ".$date, "(rollback) cannot able to clean table. exception val : ".  json_encode($ex), array('nandan@mindsarray.com'), 'mail');
            }
            $this->autoRender = false;
        }
	
	/*function endRetailerTrial(){
		$trial_period = $this->General->findVar("trial_period");
		
		$retailers = $this->User->query("select id, datediff(NOW(), created) as days from retailers 
				where trial_flag = 1");
		
		foreach($retailers as $retailer){
			if($retailer['0']['days'] > $trial_period){
				$this->User->query("update retailers
						set trial_flag = 2,
						modified = '".date('Y-m-d H:i:s')."' 
						where id = ".$retailer['retailers']['id']);
			}
		}
		echo "Done";
		$this->autoRender = false;
	}*/
	
        
        
/*function updateAreaIDUserProfile1(){
		App::Import('Model', 'Retailer');  
        $ret = new Retailer();
        $data = $ret->query("SELECT * FROM (SELECT id,user_id,latitude,longitude from user_profile where latitude > 0 AND user_id in (
        SELECT retailers.user_id  FROM `unverified_retailers` as ret inner join retailers ON (retailers.id = ret.retailer_id) inner join locator_area as la ON (la.id = ret.area_id) inner join locator_city as lc ON (lc.id = la.city_id) inner join locator_state as ls ON (ls.id = lc.state_id) WHERE  (BINARY substr(la.name,1,1) = BINARY lower(substr(la.name,1,1)) OR BINARY substr(lc.name,1,1) = BINARY lower(substr(lc.name,1,1)) OR BINARY substr(ls.name,1,1) = BINARY lower(substr(ls.name,1,1)) OR ls.name like '%,%' OR la.name like '%,%' OR lc.name like '%,%') 
        ) order by updated desc) as t group by t.user_id");
        //$data = $ret->query("select t.id,t.user_id,t.latitude,t.longitude from (select * from  `user_profile` WHERE area_id > 0 AND latitude >0 AND updated < '2016-04-13 00:00:00' order by updated desc) as t group by t.user_id");
		
	foreach($data as $dt){
			$loc_data['state_id'] = 0;
			$loc_data['city_id'] = 0;
			$loc_data['area_id'] = 0;
			$loc_data = false;
			//$loc_data = $this->Shop->getMemcache("area_".round($dt['t']['longitude'],3)."_".round($dt['t']['latitude'],3));
			if($loc_data === false){
				sleep(1);
				
				$loc_data = $this->General->getAreaByLatLong($dt['t']['longitude'],$dt['t']['latitude']);
				if(!empty($loc_data['state_name']))$loc_data['state_id'] = $this->General->stateInsert($loc_data['state_name']);
				if(!empty($loc_data['city_name']))$loc_data['city_id'] = $this->General->cityInsert($loc_data['city_name'],$loc_data['state_id']);
				if(!empty($loc_data['area_name']))$loc_data['area_id'] = $this->General->areaInsert($loc_data['area_name'],$loc_data['city_id']);		
				$this->Shop->setMemcache("area_".round($dt['t']['longitude'],3)."_".round($dt['t']['latitude'],3),$loc_data,7*24*60*60);
			}
			
			if(!empty($loc_data['area_id'])){
				if(!$ret->query("UPDATE `user_profile` SET area_id = ".$loc_data['area_id']." WHERE id = ". $dt['t']['id'])){
					$ret = new Retailer();
				}
				$this->General->logData("user_profile.txt","updated area_id ".$loc_data['area_id']." of ". $dt['t']['id']);
				//echo "\nupdated area_id ".$loc_data['area_id']." of ". $dt['t']['id'];
			}
		}
        echo "Done";
		$this->autoRender = false;
	}*/
        
    function updateAreaIDUserProfile($search_date=null){
		if(empty($search_date))$search_date = date('Y-m-d', strtotime('-1 days'));
		App::Import('Model', 'Retailer');  
        $ret = new Retailer();
		$data = $ret->query("SELECT * FROM  `user_profile` WHERE DATE( updated ) =  '$search_date' AND longitude != 0 AND latitude != 0 AND area_id = 0");
		foreach($data as $dt){
			$loc_data['state_id'] = 0;
			$loc_data['city_id'] = 0;
			$loc_data['area_id'] = 0;
			$loc_data = $this->Shop->getMemcache("area_".round($dt['user_profile']['longitude'],3)."_".round($dt['user_profile']['latitude'],3));
			if($loc_data === false){
				sleep(1);
				$loc_data = $this->General->getAreaByLatLong($dt['user_profile']['longitude'],$dt['user_profile']['latitude']);
				if(!empty($loc_data['state_name']))$loc_data['state_id'] = $this->General->stateInsert($loc_data['state_name']);
				if(!empty($loc_data['city_name']))$loc_data['city_id'] = $this->General->cityInsert($loc_data['city_name'],$loc_data['state_id']);
				if(!empty($loc_data['area_name']))$loc_data['area_id'] = $this->General->areaInsert($loc_data['area_name'],$loc_data['city_id']);		
				$this->Shop->setMemcache("area_".round($dt['user_profile']['longitude'],3)."_".round($dt['user_profile']['latitude'],3),$loc_data,7*24*60*60);
			}
			
			if(!empty($loc_data['area_id'])){
				if(!$ret->query("UPDATE `user_profile` SET area_id = ".$loc_data['area_id']." WHERE id = ". $dt['user_profile']['id'])){
					$ret = new Retailer();
				}
				echo "\nupdated area_id ".$loc_data['area_id']." of ". $dt['user_profile']['id'];
			}
		}
		echo "Done";
		
		$this->autoRender = false;
	}
	
	function memcacheKey($key, $action = 'get'){
		$this->autoRender = false;
		
		$value = $this->Shop->getMemcache($key);
		echo "key: ".$key."<br/>value: ".json_encode($value)."<br/>";
		
		if($action == 'delete'){
			$this->Shop->delMemcache($key);
			echo $key." deleted from memcache";
		}			
	}
        
        function unblock_user_via_ip($ip=null){
            if(!empty($ip)){
            $redis = $this->Shop->redis_connect();
                if($redis->del("blockipset_".$ip)){
                    $sub = "UNBLOCKED USER IP";
                    $msg = "UNBLOCKED IP : $ip";
                    echo "user IP unblocked successfully";
                    $this->General->sendMails($sub,$msg,array('nandan@mindsarray.com','ashish@mindsarray.com'),'mail');
                }   
            }
            $this->autoRender = false;
        }
        
        function unblock_user_via_call(){
            //echo "<pre>";
            $ip = (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "") ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
            $user = trim($_REQUEST['mobile']);            
            try{
                $whitelistedIP = $this->General->findVar('whitelistedIP');
                $whitelistedIP_Arr = explode(",",$whitelistedIP);
                //print_r($whitelistedIP_Arr);
                $retailer_query = "select * from retailers where mobile='$user'";
                $retailer_data = $this->Slaves->query($retailer_query);
                if(empty($retailer_data)){ return; }
                if(!in_array($ip,$whitelistedIP_Arr)){ return; }

                $redis = $this->Shop->redis_connect();
                $user_search_cmd = "blockuseripset_".$user."*";                
                $blockuserdatamap = $redis->keys($user_search_cmd);
                //print_r($blockuserdatamap);
                $user_ip_cnt = array();
                foreach($blockuserdatamap as $blockuserdata){
                    $blockuserdata_arr = explode('_',$blockuserdata);
                    $user_ip = $blockuserdata_arr['2'];
                    $user_ip_search_cmd = "blockuseripset_*_".$user_ip;
                    $blockuser_ip_datamap = $redis->keys($user_ip_search_cmd);
                    $user_ip_cnt[$user_ip] = sizeof($blockuser_ip_datamap);
                    //print_r($blockuser_ip_datamap);
                }
                $validIP = array_search(min($user_ip_cnt),$user_ip_cnt);
                //echo "<hr>".$validIP."<hr>";
                if($redis->del("blockipset_".$validIP)){
                    $redis->del("blockuseripset_".$user."_".$validIP);
                    $sub = "UNBLOCKED USER IP VIA MISSED CALL";
                    $msg = "UNBLOCKED USER : $user | IP : $validIP";
                    $this->General->sendMails($sub,$msg,array('nandan@mindsarray.com','ashish@mindsarray.com','ketan@mindsarray.com','siddhi@mindsarray.com','kalpana@mindsarray.com'),'mail');
                }
            }catch(Exception $e){
                $sub = "UNABLE TO UNBLOCKED USER IP VIA MISSED CALL";
                $msg = "UNBLOCKED IP : $ip <br> ERROR MSG : ".$e->getMessage();
                $this->General->sendMails($sub,$msg,array('nandan@mindsarray.com'),'mail');
            }
            $this->autoRender = false;
        }

        /**
         * set and reset airtel operator of specific distributor
         * @param type $stop_flag
         */
        function stop_airtel_for_distributor($stop_flag = null){
            if(in_array($stop_flag,array(0,1))){
                if($this->General->setVar("stop_airtel_by_distributor",$stop_flag)){
                    echo "done";
                }
            }
        }
        
        /**
         * Block ddos suspected ip
         * @return type
         * @throws Exception
         */
        function block_ddos_suspect(){
            $ip = $_REQUEST['ip'];
            try{
                $whitelistedIP = $this->General->findVar('whitelistedIP');
                $whitelistedIP_Arr = explode(",",$whitelistedIP);
                if(empty($ip)){ echo "empty ip "; return;}
                if(in_array($ip,$whitelistedIP_Arr)){ echo "whitelisted ip "; return; }                
                $redis = $this->Shop->redis_connect();                
                if($redis->hget('DDOS_SUSPECT_IP_SET',$ip)){
                    throw new Exception("IP already blocked");
                }                
                if($redis->hset('DDOS_SUSPECT_IP_SET',$ip,'1')){
                    $sub = "DDOS SUSPECTED IP BLOCK";
                    $msg = "BLOCKED IP : $ip";
                    $this->General->sendMails($sub,$msg,array('nandan@mindsarray.com','milind@mindsarray.com'),'mail');
                }else{
                    throw new Exception("unable to BLOCK DDOS SUSPECT");
                }
            }catch(Exception $e){
                $sub = "DDOS SUSPECTED IP BLOCK issue";
                echo $msg = "BLOCKED IP : $ip <br> ERROR MSG : ".$e->getMessage();
                $this->General->sendMails($sub,$msg,array('nandan@mindsarray.com'),'mail');
            }
            $this->autoRender = false;
        }
        
        function fork_vm_table_process(){
            $this->autoRender = false;
            $diff = ASYNC_BATCH_INTERVAL;
            $is_start = 1;
            while(true){
                $_SERVER['DOCUMENT_ROOT'] = '/var/www/html/shops/app/webroot';
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/async_db_process.sh $is_start vm");
                $is_start = 0;
                sleep($diff);
            }
        }
        
        function fork_vt_table_process(){
            $this->autoRender = false;
            $diff = 5;
            $is_start = 1;
            while(true){
                $_SERVER['DOCUMENT_ROOT'] = '/var/www/html/shops/app/webroot';
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/async_db_process.sh $is_start vt");
                $is_start = 0;
                sleep($diff);
            }
        }
        

        /**
         * 
         * @param type $diff (in seconds)
         */
        function db_table_async_process($is_start=0,$tb = "vm"){
            $this->autoRender = false;
            $pid = microtime(true)*10000;
            $start_time = time();
            if($tb == "vm"){                
                $this->General->execute_async_query(1,0,$is_start);
                $this->General->logData('/mnt/logs/async_db_query_perfomance.txt',"vm PID: $pid | total time in (seconds) : ".(time() - $start_time));
            }elseif($tb == "vt"){
                $this->General->execute_async_update_query($is_start);
                $this->General->logData('/mnt/logs/async_db_query_perfomance.txt',"vt PID: $pid | total time in (seconds): ".(time() - $start_time));
            }
        }

        /**
         * It will clear pending queries from redis  every min
         */
        function clear_async_db_backlog(){
            $this->autoRender = false;
            $this->General->execute_async_query(0,1);
        }
	
        /**
         * It will run every minute from cron and get the count of time TPS was greater then threshold
         */
        function monitor_tps_alert(){
            $redisObj = $this->Shop->redis_connector();
            $tps_threshold_crossed_count = $redisObj->get("TPS_MARKER");            
            $redisObj->set("TPS_MARKER",0);
            if(!empty($tps_threshold_crossed_count)){
                $MOBILETO="9819032643,9221770571,7738832731";
                $sms = "Recharges queue length is greater than threshold .".$tps_threshold_crossed_count." times in last min";
                $this->General->sendMessage($MOBILETO,$sms,'shops');
            }     
            $redisObj->quit();
        }
        
	function updateMobileCodes(){
		$this->autoRender = false;
	
		$mobile_numbering = $this->Slaves->query("select *
					from mobile_numbering");
		foreach($mobile_numbering as $mn){
			$code = $mn['mobile_numbering']['number'];
			for($i = $code."0"; $i <= $code."9"; $i++){
				$existing_codes = $this->Slaves->query("select *
						from mobile_operator_area_map moam
						where moam.number = '$i'");
				if(!empty($existing_codes)){
					if(empty($existing_codes[0]['moam']['area'])){
						$this->User->query("update mobile_operator_area_map
								set area = '".$mn['mobile_numbering']['area']."'
								where number = '$i'");
					}
				}
				else {
					$this->User->query("insert into mobile_operator_area_map
							(operator, area, number)
							values ('".$mn['mobile_numbering']['operator']."', '".$mn['mobile_numbering']['area']."',
							'$i')");
				}
			}
		}
	
		echo "Done";
	}
        
        /**
         * This function will execute and get data from modem via url
         * @param type $vendorId : modemId
         * @param type $param: query string
         * @return type json string
         */
        function modem_via_api($vendorId,$param){
            $this->autoRender = false;
            $logger = $this->General->dumpLog('modem_api_route', 'modem_api_route');
            $logger->info("Received data : $vendorId param : ".  json_encode($param));
            $response = json_encode($this->Shop->modemRequest($param, $vendorId));
            $logger->info("Response data : ".$response);
            return $response;            
        }
        
        function getData($vendor_id,$date){
        	//$date = date('Y-m-d');        	
        	$sql = "SELECT va.id,  va.discount_commission,va.amount,va.ref_code,va.shop_transaction_id,va.ref_code,"
                         ."va.retailer_id,va.vendor_id,va.product_id,va.timestamp,va.date,va.retailer_id,vm.id as vmid, vm.vendor_refid,"
                        ."vm.service_id,vm.shop_tran_id, vm.service_vendor_id,vm.internal_error_code, "
                        ."vm.response, vm.status,vm.timestamp as endtimestamp,ret.parent_id "
                        ."FROM vendors_activations AS va "
                        ." JOIN vendors_messages AS vm ON ( vm.shop_tran_id = va.ref_code) "
                        ." LEFT JOIN retailers as ret ON (va.retailer_id = ret.id) "
                        ."WHERE date = '$date'  AND va.vendor_id=$vendor_id ";
			$data = $this->Slaves->query($sql);
			//$this->General->logData("mongo_db.txt",json_encode($data));
			$ret = gzcompress(json_encode($data),9);
			//$this->General->logData("mongo_db_2.txt",$ret);
			
			
			echo $ret;
			$this->autoRender = false;
        }
        
        function getVendors(){
        	$data = $this->Slaves->query("SELECT id,update_flag FROM vendors where show_flag = 1");
        	echo json_encode($data);
        	$this->autoRender = false;
        }
        
//        send birthday wishes to distributors
          function sendBirthdayWishEmail()
        {
            $this->autoRender = false;
            $MsgTemplate = $this->General->LoadApiBalance();
            $date = date("Y-m-d");
            $query="SELECT d.name,d.email,u.mobile "
                    . "FROM distributors d "
                    . "JOIN users u "
                    . "ON (u.id=d.user_id) "
                    . "WHERE DAYOFMONTH(d.dob)=DAYOFMONTH('$date') and month(d.dob)=month('$date')";
            $users=$this->Slaves->query($query);
            $this->General->logData('/mnt/logs/sendBirthdayWishEmail'.date('Y-m-d').'.log',json_encode($users),FILE_APPEND | LOCK_EX);
            
            foreach($users as $user):
                $content =  $MsgTemplate['Birthday Wishes'];
                $params['NAME'] = $user['d']['name'];
                 $mail_body = $this->General->ReplaceMultiWord($params,$content);
                $mail_subject='Birthday Wishes';
                $this->General->sendMails ( $mail_subject, $mail_body, array($user['d']['email']), 'mail' );
                $sms =  $MsgTemplate['Birthday_Wish_sms'];
                $this->General->sendMessage ( $user['u']['mobile'], $sms, 'shops' );
            endforeach;
        }
        
//        send anniversary wishes to distributors
        function sendAnniversaryEmail()
        {
            $this->autoRender = false;
            $MsgTemplate = $this->General->LoadApiBalance();
            $date = date("Y-m-d");            
            $query="SELECT d.name,d.email,u.mobile "
                    . "FROM distributors d "
                    . "JOIN users u "
                    . "ON (u.id=d.user_id) "
                    . "WHERE DAYOFMONTH(d.created)=DAYOFMONTH('$date') and month(d.created)=month('$date')";
            $users=$this->Slaves->query($query);
            $this->General->logData('/mnt/logs/sendAnniversaryEmail'.date('Y-m-d').'.log',json_encode($users),FILE_APPEND | LOCK_EX);
            
            foreach($users as $user):
                $content =  $MsgTemplate['Anniversary Wishes'];
                $params['NAME'] = $user['d']['name'];
                 $mail_body = $this->General->ReplaceMultiWord($params,$content);
                $mail_subject = 'Happy Anniversary from Pay1';
                $this->General->sendMails ( $mail_subject, $mail_body,array($user['d']['email']), 'mail' );                
                $sms =  $MsgTemplate['Anniversary_Wish_sms'];
                $this->General->sendMessage ( $user['u']['mobile'], $sms, 'shops' );
            endforeach;
        }
        
        /*
         * Send msg & change password for those retailers who have weak passwords
         */
         function cp()
        {

        	$this->autoRender=false;
        	
        	$i=1;
            

        	$samplePassword=array('1010','1111','9999','1234','0000','4321','2222','4444','5555','6666','7777','8888','3333');
        	//$samplePassword=array('4321');

             foreach($samplePassword as $key=>$pass): 
             		$samplePassword[$key]=$this->Auth->password($pass);
             	endforeach;

             	
            $sql="SELECT r.user_id, u.mobile AS umobile, r.mobile AS rmobile, u.password
            FROM `retailers` r
            JOIN users u ON r.user_id = u.id
            JOIN user_profile up ON up.user_id = u.id
            WHERE u.mobile
            REGEXP '[0-9]{10}'
            AND DATE( up.updated ) >= '2016-06-02'  
            GROUP BY r.user_id";
           
            $result=$this->Slaves->query($sql);
            
            foreach ($result as $row):
          
          	   $samplePassword[71]=  $this->Auth->password(substr($row['u']['umobile'],-4));
                $samplePassword[81]=  $this->Auth->password(substr($row['u']['umobile'],1,4));

              if (in_array($row['u']['password'],$samplePassword)) {

              // 	 $p=rand(1001,9998);

            		// $this->General->sendMessage($row['u']['umobile'], 'You password has been changed for security reasons.Your new password is  '.$p, 'payone');  	

            		// $hash=$this->Auth->password($p);

            		// echo $q="Update users set password='{$hash}'  where id='{$row['r']['user_id']}' "; 
              //               echo "<br/>";

            		// $this->Retailer->query($q);
                $i++;

              }
          
            endforeach;

            echo $i;
        }
        
        
        function offerRetailer(){
        	$this->autoRender =false;
        	$query = "select retailers.mobile, users.mobile, distributors.active_flag from retailers left join retailers_logs as rl ON (rl.retailer_id = retailers.id) left join distributors ON (retailers.parent_id = distributors.id) left join users on (users.id = distributors.user_id) where rl.date  >= '2016-05-01' AND date(retailers.created) >= '2016-05-01' and date(retailers.created) <= '2016-07-31' and retailers.parent_id != 1 group by retailers.id having max(rl.date) < '2016-08-10'";
        	
        	$fetchdata=$this->Slaves->query($query);
        	foreach($fetchdata as $data){
        		$dist_no = "";
        	
        		if($data['distributors']['active_flag'] == 1){
        			$dist_no .= ": (". $data['users']['mobile'] . ")";
        		}
        		$sms = "Pay1 Festival Offer - 1% EXTRA MARGIN!!
Pay1 par jyada ka vada,18 Aug se 25 Aug tak sabhi recharge txn par paaye 1% margin EXTRA. Scheme ka laabh uthane ke liye apne distributor se contact kare$dist_no
Extra margin 26 Aug ko credit hoga. Jyada jaankari ke liye missed call de (022-4293-2279)";
        		
        		//echo $sms;
        		$this->General->sendMessage($data['retailers']['mobile'],$sms,'shops');
        		usleep(10000);
        	}
        }
        
        
        
        function saleRetention(){
        	$this->autoRender =false;
        	$retailers = "'9718253010','9966858436','9824782217','9097627921','9891936786','8698737074','7053588319','9637469306','9833292952','9866697850','9887380463','8097041590','9275066666','9782206995','9167113113','9174286842','9210860068','9538794863','9925825807','9828819176','7503819594','9322812278','9887436540','9136286614','8588003779','9765246397','9612051609','8861253504','9888899906','7396734673','8424956032','9849084048','8375049192','9871323738','9768872974','9769796499','9873888998','8239905628','8107422031','9224114962','7738935695','8285192962','9724948888','7057333033','7719805345','7219292793','9915838886','7795493353','9718841842','9015401529','9822854770','9885027421','9582781716','9923635960','7302444510','7666829965','8767737692','9977955037','8527021522','9920888297','8562831803','9999226811','9702150543','9907223208','9561369240','8269554979','9501729163','9030901198','9769686999','9898241561','8000032444','7276381712','7899858807','8588980336','9555060935','9975531777','8000135572','9879360113','9821947980','9768232504','9133597307','8014465475','9880538569','9503335538','8285845856','7506869121','9823267589','9552180234','8975298031','9035251450','8905479670','9873659195','9891172145','9599515399','9898959594','9665339099','9966386736','9845074961','9975873636','9594673323','9702453185','9582252401','9823542922','9139445828','9595631088','9996444900','9652940251','7610001116','7042112096','8010650002','9890640639','9987866866','8806823131','8886213116','9850190502','7208793778','9768783999','9311854607','9637373760','9911115074','7588575200','9867547624','9300186060','9890097910','7045173226','9650689640','9620652000','9892987858','9595438585','9611647366','7411598517','9372031818','9407800171','9930374446','9764034248','8976303390','9960810528','8223886648','9723678040','7718001141','9022411988','8501841534','9666685577','9022121217','9414644064','8767377451','9879390822','9870122129','9503186809','8600222261','8237513583','8446576789','8976769380','9503121627','8007267876','7875968621','8408005150','7775059201','9545099810','9702684487','9849504453','9987931036','9313006583','8600312931','7887569697','9963787175','7411139099','9848009738','9855566777','9765683335','7303232798','9814393477','9313417716','9901842796','8422012368','9555607017','9711610939','7097524786','7737571408','7580986810','9888051512','7355058377','8283000294','9030101272','7355013208','9517260113','7754013947','9115263357','9417426390','7710833532','9891272680','9560528137','9781722620','9699494929','7207210131','9872290313','7415311504','8087905172','7083001956','8826973212','7709892260','7416324041','9823809807','9866672382','9527458018','8446710136','9960023305','9573742991','9508669183','9945752504','9210317790','9440103998','8802156539','9912249382','9821458521','7210303824','8801672212','8983597893','9669154890','9923101971','9145485773','9066214364','9036875508','7382026078','9703579624','7022221647','9404879266','9260592456','7207242352','9815554435','9970111123','9711509954','9899533300','8743929418','7503269500','8285745436','8010665111','9650604686','8793879313','9213612612','8008666615','8952931757','9030571251','7509859153','9885215103','9036991183','9716394696','9718295025','7396091919','7503090231','9069018208','9540518781','9650401256','9867627483','7530900950','8802541105','9899212947','9871682245','9990044316','9704078886','9860665400','8802538703','9999217309','9160092018','8802538972','9250603337','9209067686','9637239702','7530826822','8983509051','8454858499','7709820940','9945778432','7719865425','9036791900','9637633447','7530938025','8008543543','9985000777','8892126472','9582535312','7532089996','8130337251','9819877265','9069028077','7083212141','7877949445','9213831536','9908035635','9099210459','8010791521','9643639476','8520056928','9156456452','9850143829','9542187736','9503633893','7355634064','9248788371','7798372627','7875358888','7600073450','8223044409','7738611878','9741178705','8121341368','7841010629','9540414157','8080660470','9509643408','8290651316','8879305369','8088722832','9891062606','9911786898','9560247228','7503853530','9890066618','8149735100','9502747413','9213740000','9373413171','9811262648','9890028813','9574397000','8375951919','9049826776','9989622538','9704408539','7042543757','9918559355','9822998399','9022633720','8390949474','9441958486','9971626263','8955048412','9642441822','9624371836','7755915516','9555230757','8527867022','9515599752','9029325615','7791004758','9811979211','9902256863','9911362918','7292049183','8898162926','7727834467','7588331163','8928449090','9595245021','9702411411','9711941128','8121112998','9989496614','9766186269','9767046818','9494929977','9371202607','9891352090','9601654314','9205702116','8828351675','7799597793','9998281380','9968072537','7666566395','9029822280','9833460926','7058363912','9604737364','9970280524','9769164264','9999454068','9920445140','7879510055','8551851555','9867217565','9324747247','9826570018','9716540858','9096892071','9595949309','8425907040','9582531628','7697945436','9820622653','8154822784','9420594414','9422652385','7276249557','7710961619','7566848252','8652178664','9763423679','9960135143','8087603619','9584420497','8484846994','7530993786','8285494807','9594225792','9441457943','7692935614','9716476458','7049025014','8506069732','9860377681','7755972781','8744889917','9032346667','9409407721','9924877977','9930288425','9819161463','9818231079','9158332273','8446248615','8308679905','7045448002','8120039553','9999077683','9312576853','8390838774','9763749649','9815721213','9535650686','9023081344','7097828771','7303633330','9210167901','9930130133','9424700451','9716110311','9886998419','8510098997','8826730796','8097253128','8805175543','8237188964','8390304038','8379807250','8587813646','9617607997','9424253878','7053848255','8000624228','7698739377','9441560267','9069235381','9718516184','9922555690','8879439659','8087507547','9818401521','9911582445','7697502537','9970345612','7715082244','9909370444','8968523758','9595708050','9899979338','7405108443','9702621289','8483936883','9535653159','9977386955','9573864351','9617925915','9405290287','7053463408','8856875053','7620115251','8120875192','9977375150','7066285725','7276211765','9833323326','9911549414','9819792457','8802271565','9595991723','9990074955','9582842217','9028716562','7386159077','9085476618','7415145594','9990877985','9033831050','7383888164','9010498143','9810828218','8691925455','7069597360','9145550650','8522066678','9821144567','9891416744','9891702946','9764627903','9782946478','9770824212','9561461369','9665796146','7622900505','9650171721','9768700214','9755987124','7798812673','8767573033','9722712992','7210475924','8898187065','9828338495','9630321709','9270966777','8087815581','7057667267','9920340940','9716398239','7053321528','7875635516','9643940759','9211779810','9099575552','7715056834','9960727411','9404494834','7053877297','9420991002','8375892230','7875807574','9270543797','9824477782','8467070758','9975887969','9890171735','8551016056','8802157950','7210180003','8826578983','9989773458','7276238500','9881264964','9391387073','8099123444','8461029006','7045364506','9004979744','9910421738','9911493714','9959563331','9975959995','9819075940','9015161200','8120443334','8527936256','7101000521','7101000522','7101000523','9643416187','9029042391','7798270143','8879317982','8374843095','9867264198','8802029901','8898374426','8750599168','9560186213','9069058110','9323427056','9910746894','9899564583','9617056139','9601111010','9016087771','9665927649','9619954002','9300656175','9860284939','7040070068','8108260271','9468668044','9811410431','9552422667','9958358137','9421679666','9819112416','7030263264','8655389997','9867013463','9822092196','9743740713','9987156980','8898456979','8976961661','9594940294','7805993923','9136486578','7095651027','7709299865','9211872862','9766889333','9226900625','9211630849','9650698677','9914204541','9990822987','9958204676','9990444094','9561160844','7774806750','9821404298','9666565381','9212972744','9592390580','7838040978','8198883006','9726090126','8796292856','9467111868','9029447309','9653418738','7276311805','9911919697','8802529647','9494851289','9322082967','8143143050','7503307752','9885118106','8516070888','8691912806','9711579602','7503836227','9769804303','8287166966','9581903133','7666877998','7698800080','8898456200','8527616101','7083496296','9711129029','9173240188','9914496800','7406947143','9990767805','9643227949','7709067248','9427254047','9075641298','9015489099','7666310634','9811684179','9527298647','9914817653','7798192405','9716194038','7859991116','9687462448','9871883225','9871245071','9663391841','9723511708','9540540898','8050029169','7203053810','7355632424','7053834240','9347702999','9949944849','9892563263','9449834999','9028945790','8467081235','9904511723','9949930414','9999288511','9739173986','8287213214','9085969547','8962985744','9702514711','7101000524','9004025855','8007375400','7042989920','9271433042','9819220957','9833793446','9717138786','9926342546','9595729977','9620796156','9599243534','8796465153','9691714408','9890553511','9910495912','8422008477','8108534563','8447705058','9540891615','7678039231','7359839447','7669657049','9082623000','9542425512','7210233804','8237769619','9069322414','9822779592','8750170721','9028313436','9716394313','7770809653','8898272126','9891342876','8624835535','9211373931','9989315261','9718643489','8983737478','9575559525','9111444870','8802919191','8412049627','8082966786','9069669744','9958509755','9766024111','8866121426','9999732182','9096336458','8108462576','9599956014','7276324904','9004882792','9591941518','7718816466','8285921058','9320229960','9872722117','9908209911','8401116063','9889755515','9351721767','7024160773','9657120036','8080744400','9555997719','9731976326','9028482314','8010223233','7306219070','9637444696','9890657875','9172771380','9076999998','8452999902','9441936523','9998174646','8285861507','8989779254','7875995160','8008888853','8806215015','7840057397','9899476817','9274711455','9322008531','9069570573','9069724745','9417851201','8149711081','9702769476','9711774311','8285093741','9000686185','9866709811','8374478903','9849076855','9049388174','9723891770','9533249999','7387192332','7529942657','9158074700','9441521298','8374269361','8802011712','9990482782','8130373327','9867761599','8802549994','8287393047','9212053241','9818740017','8185010829','9849468882','9998842759','8801425265','8010758082','9673584751','8858872217','9820119810','7875353702','9049096353','9676987624','9205808499','9441094479','9849866992','9753000694','8689871219','8885848848','9704048491','9010175241','9205152874','9154301809','9561092192','9978104455','8600724207','7786824357','9968563869','7698977878','7508004843','9550024849','9177886405','8767464643','8686058495','7691907016','9920164275','9818970457','9494837575','9540837740','9540228035','9924865683','7075268700','9871783646','9926004781','9133118494','9958747438','9560474700','9811961450','9963936352','9753798160','7387507136','7042547275','8801623786','8008717503','9560272971','9959637387','9479337541','9662726121','8130078722','9990384024','9951942143','9372959596','8225847208','7530974510','9718904060','9702872474','8802512334','7053469754','9723616846','9820026908','9916344658','8923165165','9049886279','7304444494','7218796689','8275541697','8712772508','8000446004','9819074963','9972627751','9540485438','9640990103','9987593888','9990439612','9963162774','9505307556','9819052342','8793180553','9892958518','9849449185','9769629702','9822932680','9019096252','9970211327','7675912292','9650350122','8186008009','9725540970','8605209361','9820973592','9770222224','8983100710','7758043311','9594938356','9015303427','9716491281','8143438585','9130431827','9228904895','9920431022','7042535775','9810894714','9582520263','9888103458','9999749944','9998843489','8010484794','8793287028','9654222330','7743965181','9099860893','9989105738','8962985724','9717832324','8488888424','9898788599','9922030894','9717374707','8686217504','9737776773','8285612239','9599679767','9999552772','9911887076','8460051781','9664652424','9848488541','9902477222','9881802026','7038659350','9179414443','9766525252','8743023480','9855498306','9036302469','9990088609','9703311055','9213243843','7065881695','8586968879','9999740291','9871859240','8225901590','9860271094','9949968119','9028866689','9604089804','9032297588','7048222958','7566522566','9848716006','9015304061','9999326175','8143775599','9990491188','7503934364','9911756517','9819139106','9975877315','9665975171','9669229917','7785924244','9899855008','7803026265','8793517951','9175431994','8435076309','8655412061','8802784206','8506992815','9666448779','9653405310','8305832564','8989795105','9872326020','9139379020','7088584476','9423383184','9023750502','9729442038','9888840230','7069131024','9646819525','9892914880','7405206902','9762294143','9260606794','7503413106','9420242006','9953677769','8551959551','7715033005','9958524104','8196990486','9844557789','7875115707','8554077375','7532052522','7503333381','9990400390','9666515858','8419988403','9718151584','7503884221','9891018350','9205149566','8010280426','9959594332','9371711669','9637540585','7709168968','9860101786','9772071955','8796816448','8743047839','9820842786','9765227833','9560092235','9999908893','9893011127','9591851999','9689660257','9503981222','8600371215','9716968544','8505962871','9714884005','9624945709','9810425388','8380921789','9850631068','9898082391','9974327750','9960008037','9953181516','7530953991','9533926788','9953540053','9711708721','7355670024','8983390203','9825806629','9172838087','9833963909','8308878851','9527103319','9323281008','9555010008','8130165400','9705053674','8655760441','9885347899','8527168497','8801772299','9966667998','9822788866','9510026060','9892393532','7387866309','9814154447','9823266910','7089158196','8285092771','9971738130','9599233397','7075257588','9716447186','8801482715','7386561000','7350406789','9890079751','9891303202','8080601477','9638329018','9716405060','7680069870','7813822332','9909153452','9898353987','7406407934','8285148699','9640119169','9573848182','9891303239','9921142158','9898704516','7874937418','9925940465','9212381060','9022221004','9672536416','9724643220','7101000525','9890576104','9730347982','9949812362','8285158723','9860964131','8750976035','9985011959','9833085989','9810622821','9158997711','9705053868','7416676806','8275171160','9849360418','9743624407','9971140269','9966002022','9819994254','8600837088','9662793984','9987404033','9887415615','9738939409','8123537224','9533939533','9136282309','9916131001','7503242014','9032660340','9860161023','8747012342','9552932146','9923845482','8762725563','8587863288','9623237774','9872656127','8285061530','9949034182','8866600049','7794029921','9780602200','9738578143','9049424974','9898144173','7042948008','9054922200','9911325479','7411426623','8099966633','9540044014','9990930828','8427396216','8983122253','9873231234','9998648103','9872733375','7531968078','9691760007','8977512253','9975675382','9960497310','8379815407','9545918744','8308129950','9925902699','8559044257','7730925501','9926938382','9010577899','7702508065','9052412121','8720000822','7691904356','9076160786','8185945555','8142562143','7503347277','8008441550','9096119620','9700000637','9096109191','9766666578','9555788350','7068982370','9033047400','8948435273','9700007886','8080775776','9921474845','9604060481','8876238662','8872630122','7219605852','9971053659','7069213821','7053067181','9601835504','9175330969','9811085343','8875749945','8485825249','8686683880','9810312068','9974178305','8750451858','9146782929','8097358054','8888898755','8626083500','7263989717','7875162193','9810071062','8483988662','9096070077','9423491371','7709881241','9004421222','9425650064','9221326785','7406795576','8485939875','8333072789','9848243717','7776042562','9926943909','8108968143','9423302631','9156909052','9391633933','9923568818','9136445159','9247739475','8143555502','9102815169','8600708600','9966472620','8878916160','8879833003','9885427038','8686111833','9687617599','9666113144','7065542661','9173273642','9987145911','9015513313','9665747644','7304568697','9781787528','9891561743','9923057327','9839173381','9409372533','9637898387','9010707143','7040688701','9999666067','9516098643','8080864385','9173311888','7532999286','9139393924','7838423248','9638744677','9780344640','9998274848','7666039004','9540818687','7802991336','9066334402','9948770404','9966656422','9824395688','8802709808','9923316363','9886646668','9923099007','9911881415','9891911148','7795667795','7276383235','8287190052','7795425104','8800376601','8866428707','9601351845','9818875152','7053856223','8285124078','7729056993','9891253600','9754404245','9738887245','9601141991','7355942000','9008339090','7835954801','8885463339','9871046137','8885670567','9716093242','9666656696','8285832130','8285883423','9730341380','9716492934','9491078134','9676402029','8587034561','9881664471','9998783322','8512852015','8977299789','9867700609','9617020381','9346514143','8107623181','9977044628','9899046874','8467888149','9850581901','9618318351','9415868423','9716093201','7083783587','9820495385','9592929258','8146982014','8506031954','9999990000','9920489121','9130199299','9713112586','9999027399','8527431229','9464176922','8976603684','9505692553','8454077590','9891497635','7306331404','9867147753','8375817185','9873986682','9987176078','7066602229','9892597018','9819900069','9852297053','9074444461','9873388900','7276065583','7996092936','9930772478','9599692245','9716292865','9930323032','9818391471','9115284601','9823339954','9867123124','9818660652','9029889428','8447520866','9722124332','8885566156','9039557678','9039645245','9154547056','8285494793','9909087654','9879278238','9604035798','9725792449','9860107520','9820707944','9960311181','9302994161','7416607557','9390064617','9039224274','9893222245','7799995501','7359720489','7600872251','7350146544','9033386997','9845148799','9290104020','8143276590','9737551307','8652897267','7330860379','9844917590','9699466695','9953128317','8096444204','9666054345','9553322454','8982620857','9967159494','8686482098','9723825655','9676869395','9136661531','7036691033','9599508682','8587045375','9967822399','9908116777','8520057748','9302333995','9993393323','7065124105','8605304631','9172387378','8879110146','9052575442','9221616168','8985555365','7758884999','9904432803','9516779102','9665742577','8743900462','9122140223','8686200183','8080082088','8080828143','8697811687','7738412013','9988442281','8652706300','9990472199','9987556510','9845600758','8725066342','9918804452','8380908360','9503228494','9999512046','9737417958','8553337471','9818585142','9911179686','9049594139','9158144449','9211534952','9685931629','7503115418','9664223323','8860308906','8605214715','9766646813','9740890488','9871153840','9096899905','8587079156','9653920001','9425835635','9860626340','7276728117','9000744474','8802104668','9327200931','9060076102','8750021441','9665520222','9075826011','9911110428','9892832153','8879060485','9666063059','9948046456','9921629909','9849227625','9885764234','7304888871','9849228213','9885568855','9573537829','9769617082','9949005713','9654561040','9665424222','9540632075','9010303039','9172360330','9849184977','9700500711','9949005709','9820248764','8670779895','8686703410','8108808999','9718524612','9718776604','9920618424','7276794218','9584231728','7383491103','9242739848','9911620722','9049909077','9619130192','9848734530','8686465523','9866435991','9973890552','8096195485','9004685632','9611226364','9848614894','8287520329','9832310505','8796092069','8652780671','9638412019','8758448448','9820938603','9867600825','7028249488','9022559331','9979770088','9619106841','9922885303','9959351403','9033034891','8803836021','9022215598','9811450441','9038102390','9993066696','8149736765','9971012772','8800894094','7724902755','9755827204','9959594318','8440000206','8285026849','7276660700','9978549898','7771084444','9726155594','7794845551','9977881420','9892922316','8743891161','9769878887','8567929619','9948104854','9611148143','9990934462','8019275415','9066686444','9510618065','9989165251','9811479716','9990777290','9866983143','9666770545','9221809942','7053316332','9686463254','8527738288','8050010375','8586006301','9595389521','9643293144','8431112363','9062525419','8885891567','9867413127','9686633378','7738786143','8177866225','9545751343','8650107467','9324773652','9000071425','9312127220','9985399673','7330292591','9392395959','9560678731','7065630390','9922416480','7531039240','9574864886','9958465334','9819808619','7799119727','9910295898','9545535977','8000007578','9815931888','9999759723','9871780443','8123062280','7500570518','9987882957','9738090694','9999791860','9158954888','9339430385','8447003131','8108877794','8179770469','9169891600','9925650808','9158829101','9553588552','9985754050','9293102670','9493061406','8888645282','9666446701','8553173242','8800509704','7415603288','7059491631','9390307973','9417061295','9056226879','9824514620','9640047717','9769669972','9662069143','9433994129','9426023931','9914712081','8699522017','8087640363','9825165094','8505872052','8886106869','9898189468','9550082891','9741801300','9039312510','8655259530','9892653208','9892605913','9624675786','8796829049','7405510697','8959300879','9535513307','9210086834','8793660070','9540823545','9718566636','9699407622','8967945962','9966992347','8872631802','8828180227','7263828202','9831035397','8750915722','9820648781','8600501696','7778833000','9811529522','7670978098','9669444525','9211111019','9592964006','7276005652','7666777102','8374524808','8483871127','8512858515','9911850153','9860237506','9716937559','7039743245','9650347156','8600000103','9811857903','9716848817','8143294800','9702453772','9066273175','9560674638','9849842010','9702158313','7798467218','9619921067','7354062783','9899599845','9867869828','7208100070','8421839898','8802573513','9718443122','7058597111','9620502008','9066242069','9871783654','9718083413','8801425473','8879167802','9866604039','9948916747','9494212299','9702144337','7721819055','8802812750','9702840826','7743821265','8375908138','8806890751','8447902903','9849405108','9669235039','9764000600','9755455544','8861090118','7204230085','7697042369','7666450714','9824694318','9971209651','9890993377','7697333918','9657777460','8888913991','8548881500','9971868698','9792617546','9765999947','8689882720','8866488332','8750932732','9718527118','9420429581','8691821602','9273178900','9885777885','9716094574','9987892923','8237781088','7039715643','9463794700','9011328441','7210600793','8879375238','7738300746','7798488485','8080907399','9666803235','8275009371','9004188356','9540630765','9721511262','8802582363','7378658773','8655555356','9425409835','9448423578','9714891395','9891528237','9669534643','7416417857','9133003005','9967171128','9637373728','9322186410','7290827769','9300616112','9871781914','9987321432','7411325343','9074487171','9902984460','8686918786','9970002801','9971728208','7038258852','9773747847','7741066221','9522023432','8745066893','9765145995','9666979701','9891424849','8600484948','9289430130','7503406107','7053431674','9891415040','9953231314','7746999880','9922424662','9769246655','9038031222','9899005859','9895694328','8341682999','8626090505','9702729950','9503610547','9209546162','9624067858','9028587642','9963688886','8962682821','9604855487','9502991130','9987357842','9987342275','9881320939','9824935600','9811483604','9873642904','9066669396','7503588835','9708444030','9063879796','9967608143','7623947132','9711951982','9623934115','7887531097','9004585960','9396908353','9781085959','9066997070','9358221996','8750527029','7875543112','9867199919','9898141927','9967366377','7387996386','9848767604','9654448202','9773214587','8866565509','9167590720','9058078723','9818982393','9594398180','9828198381','7666111517','9154796166','9966054843','7744080811','7276101017','9725265977','7738261241','8872621792','7531999696','8723014844','7666097609','9987852411','9990136830','9763701156','9702683695','8553383080','7218049787','9700192775','9540883838','8550959899','9594431343','8490909451','8487997420','9781549007','8800969525','7875504783','9722222337','8287803649','7692909430','9900104787','8010609900','9766401810','9879471486','8855958383','9665769787','9850080152','9708813743','9999899395','7387025589','9321201435','9702786323','9154323452','9958046944','9702783164','9654549797','9702653641','8595424523','9971558732','9555042928','9979053934','7210946159','9158532658','7794094959','9844673015','9014649460','8698840571','9873992022','8888100223','9700666040','7738735915','8482842994','7210651866','9867068685','9741280676','8510906823','9899593248','8225091919','9594740403','9824421368','7738815629','8796155956','8285093622','7038632989','7498668319','9449443016','9815014697','9823311731','9970073725','7509001705','9075799948','9681344012','8286192204','9867525128','9717226772','8698529030','9035063821','7038023636','8962186479','9967679946','7776029260','9824367467','8149744208','8461084093','7053611739','8108713671','7276063365','9762791715','9799991494','9738446142','9811061328','8470025007','7066732292','7697016543','9172523400','9666325151','9822680008','9714381002','9819937028','9855294300','8433865292','7532880177','9924723298','9772668857','9945378277','9246996969','9424058863','9686102105','9375744144','9466690189','8796792047','9763112323','9782879113','9650735528','8308403214','9494622965','9999598172','9212121051','9595337176','9702377115','8197842222','9833229026','8233455102','9636598163','9718224525','9725479943','9890606671','9044156896','9660867812','7709238693','9011536333','9028812776','9845148392','7621048295','9910474733','9892151480','9740717114','9810036224','7204924475','8989525152','9923185565','8469580484','9999490427','9987471081','9424937722','9767997808','7385656566','8802785184','9545240754','9871630063','9892244404','8826536211','9881228072','9555232967','9700176563','9175380213','9289234114','8982371889','9572971780','9163512240','9833194714','9290183212','9873000252','9705740277','9425558326','7768822010','9714358948','8109437301','9870516221','9322379620','9535005805','9891345754','8855037050','9163795406','8712129942','8983204875','8605379797','8096565757','9598010136','8793666474','7498358767','9799229846','7697442022','9691072376','9899156055','8264786893','9819799183','8080850840','8527818477','8268849045','9730227503','9867267408','8286901088','8105569705','9766066433','9650362215','9175258793','7533035000','9810041513','9975390104','9987788584','9810514784','7053868695','8802123842','9988941211','9595313162','9740158109','8087471236','9921485285','9867081322','9717874137','9987160552','7416608396','7411134530','9867546325','7838979194','7709063436','9555911544','7500610147','9004731247','9819656515','9810007478','9022662911','8625899797','9885670722','9175483482','9704006364','8792397934','8792729287','9074773026','9718697858','9822221297','7057955959','7875773184','9891353480','8978890457','9711485086','8688556363','9550887709','8010681600','9032915255','7400606323','8975445251','9890219123','9637216883','8285000659','9409392539','9008924941','9819528711','7030314497','7046619552','8097345620','9768534741','8430811314','7378979876','7350135757','8898682889','9911340676','7696106812','9921285766','9923081224','9781346410','9420736946','9891873137','9928631405','7053764196','8898701542','9246907390','8285090993','9098049899','8142283998','9000627156','9350072947','7665026456','7276781188','9911078644','8471041234','8624031249','9666980422','7013603026','9582398881','7042236413','9803862262','9494103458','8287551820','9950929872','8500348142','9953600326','9211678211','9010465555','7030787813','9502204957','8904740039','9494091677','7208214364','8185816741','9613261003','8285091069','8007440079','9702809009','7276357880','9830068955','9762192189','7506668080','7588515117','7290000729','9971584007','9910347777','9015989517','9205201633','8179999962','8854000707','9811836976','8087141112','8464863735','9880008384','7389252222','9819075729','9069644984','9212952871','9981338520','8050521259','9824788151','9764442333','8561011786','9492636397','9911057635','7288071104','8743971033','8691876368','8285865019','7738448002','8556839194','7587524958','9921002809','9810577717','9716394194','9491704183','9560216770','9781932917','9891494922','7040437700','9716815475','9689052333','9742444101','8806834250','8557839626','9270553337','9821042859','9663197192','9049359929','9015776577','9000560064','9850182340','9999917220','8379920383','9731333017','9873015610','8374587334','7838303168','9987344511','9766640278','8982103625','8237153860','9654606042','9986910706','8271522233','9021955151','8886885855','8435485947','8008459009','9922384858','7065358363','9553599139','9350685541','8750153277','9873505733','7673916361','9912053381','9646663268','9654113202','8866886106','9004702896','8097074443','9928627898','9491607786','7420986285','9038305003','9313636355','9666617770','8285042155','8826498984','8454013676','9461600737','7208704231','9209988879','9888225431','7702636669','9038388278','9491568349','9881236758','9716586387','8879254641','9711881049','8805710745','7503091402','9423859272','9099029687','9908829143','8285592708','9036241294','9990111361','9910905800','9139944073','9205701592','9822377838','8801633501','8019194407','8087601654','7416447857','8743891492','9782406139','9782368668','8806756043','9676133331','7097040575','8652735224','9893549448','9818671528','9993809707','7415566336','8600964473','9617540772','9909116143','8600559009','9246592757','9850580918','8871374949','9921779453','9250006800','8125602135','8087444810','7661838143','9716620201','9948449527','9290849489','7226822440','9850542011','9650116490','7503683777','9036229478','7803993508','9963251980','9666394444','9960724977','9702991945','9754767688','7053852434','9987711981','8808101209','9819209710','9324169936','9762253415','9867844644','9860208666','9717183617','8879571230','9642903007','9821992020','9028237742','8879837008','9672073084','9700401731','9892924307','8885499950','9570081966','9948158090','8802948801','9573735849','7803037264','7207135987','8885733734','8605500680','9822476621','9822034281','8983648294','9848675177','8087595900','9247180887','8898234109','9779410008','9455572005','9973813545','9711547526','9300001707','7531839457','9000875244','8983114007','7218565946','9888623567','8010779671','9718282315','7259225799','8080284436','9967461008','9666716196','8108549484','9705060906','9999941184','8527052444','9999649647','9270576943','9177260680','9611762369','8750723972','9971272760','9527449004','7249753879','9323142654','9921700622','8054023098','7089312479','9491910656','9762532747','9347398109','9700630219','9545417060','9999985519','7058551663','9803490457','8411873173','9601409668','8897199087','9542994198','8750260927','9966222364','9270456900','9226762452','9166939494','8882177469','9640943160','9782373132','9967227835','8484977945','8057287417','9873397366','9624458284','9981861664','9930749986','9210449555','9323455777','7350858503','9623078875','9052229393','8650454912','8567800772','8888450653','9990998843','9584875697','9685464400','9824370859','9819811047','9930566311','7413091301','9716093582','9770130144','9999229998','7738253351','9716093299','7259846936','9969657858','9972222678','9818894284','9716993428','9833391962','9891993170','9990547714','7531879798','7261918335','9951574190','9910428020','9015648678','9757476848','9011544704','9873800110','7042469829','8184939990','9160098369','9015412687','7738984835','9899470410','8390654242','9440075458','9920308574','9999877949','9824797177','8802923746','8238855151','7818885857','8275054109','9716591367','8686138116','7287862086','8750451168','9558292901','9925257862','9137345400','9490855820','9666564512','7065991969','9848703417','9711711743','9912361613','9673388333','9000371435','9030540456','7405116123','9059246243','9892959818','9210421042','8802537918','9730324105','9546810738','8601864391','8264550195','9706855966','9899674748','9589671176','9250588342','8888478415','9654689993','8285392805','8882223683','9966881922','8871711918','7405173731','8655567663','9818538920','9041265124','9899378666','9899860074','7828181133','8019203476','9923890051','9662121495','9049064864','8185984151','9571775725','9879974999','8652811606','8691044421','9924949820','7208267374','9766699867','7875537548','9555188249','9638334445','7840020403','9979078444','9714108699','8567812954','9819909039','9624288045','8080092091','8522019008','9967791497','9902924493','7210976190','9324951687','9737515188','8130979032','9160607862','9951696787','9665501554','7036794725','7428088777','9987435364','9711314722','9076666662','7776805045','8510918961','9989405339','8946990877','9924223161','9978095675','9848837678','9871995997','9885448448','9136511834','9899207317','9898248224','9779682590','7503093960','8151986645','9644660077','9599899086','7567869214','9987012552','8719830090','9891093305','9818281768','9022243580','9990989921','9393199143','9717378220','9785228204','8099112837','7692953737','7748903376','7075672288','9555942628','9015440233','8446662656','8872055108','9974486854','9881664488','8010719482','9004702551','9661981465','8285452249','9096742680','8866021586','8411032143','9175919090','9911413385','9924536438','9948005482','8585072466','9922449397','9966133337','9448895732','9160796987','7615974387','9213169191','8308786825','7066880057','9960780837','8180841001','9987434620','9433427710','7738874740','9642004143','8802315476','9849026878','8802523971','8225060688','9581264445','9922378589','9000209922','9738227770','9990600932','9350903141','8855048819','9505522368','8007589377','9604319851','7666969543','9849485815','9780258225','7859803136','8879046056','9210421822','8600560761','9015260278','9390443034','9028206200','9768200275','9978110565','7097212127','8888505435','9953254312','9211550752','8983124560','8686471105','9959986684','8826936556','9620856219','9845053239','9699091144','8341239889','9867129719','9582408240','9210326490','9440224328','9911771133','8888019089','9764590440','8085376647','9922566581','8097276665','9890886989','8484845151','7411912616','7219521920','7053846653','9650678035','9491395901','9898414147','9397838555','9908238852','9899180556','9990412197','9175646441','9970326937','8855969001','9584203526','8470992396','9210333630','9713930124','7503417819','9988851418','9717399394','7028299524','9689707465','8130108992','9770489000','9716939212','9000736900','8484833101','9704355808','9673277786','7387196834','9820108799','7388110080','8374754133','9650117647','9716317377','8802388766','9924625257','8897474886','9926181959','9975299734','9643163066','8341251627','8109994552','8149415036','9873137393','9913976551','9555355636','8801334988','9824428022','9540408376','9222029665','7075553609','8802877091','9555869998','9136658919','9220050092','9717099373','9001182856','9136119137','8518063523','8374767341','8130605188','7250332649','9951786083','9100183046','8888739012','8855927474','9021070707','8341527861','8297053928','8179957544','9902929238','9174817350','9553845080','9059335442','9850320004','7600813845','9553095082','9819126201','9898799455','8898919262','9689808954','9000395759','9015878593','9716616461','7531960982','9028212811','9860190447','9028906466','8130256509','9871981492','9927618055','8553317773','9958335527','8743909706','7847845555','9990587136','9972278506','7024485212','8149020308','8419945905','9460703239','9902090922','9987940387','8800975814','8892061537','9177777648','7773930220','9708065673','9619708299','8412818247','9740858590','8149879575','9592251348','9885222919','9958389329','9420214587','8237208293','9505523956','9560901973','9911033393','8511681014','7738795229','9964642786','7383070166','9970143775','9927844561','9329132999','9665729972','7798198592','7303991330','9603827104','9825266593','9421051465','9341313750','9594234749','9063118205','9813200749','9914511353','9391750658','8238261137','9978364383','9300445244','8238977854','9139833753','9689536628','8971317770','8409573075','9970073888','8285094246','9685022991','7569116450','9915810110','8297025174','8983615196','7666623441','8802788404','8888280999','9867108208','9041501107','8826626264','7385451010','8750517254','9870001432','9311451470','9765666795','9892927704','9424321536','7898405999','8186080964','9673341685','9973884089','9663121456','9676812633','9958776043','9860601212','9810465337','9987855760','9990088111','9323428008','9392519979','9213392199','9963301303','8889086985','9250700801','9990909214','7385375555','7784093891','9910393963','9619875376','8553194732','9099237170','9323309573','8553086753','9880763521','7030616326','9739488786','8793125189','8690035229','9408563921','9420001117','9930750780','9966778646','9763870553','9866766613','9726915436','8802739002','9630545349','9067941982','8446834645','9428602638','9866409079','9620923737','9062868509','9891762422','9825166136','9665031112','7567178079','9522112295','9912986638','9978135303','9879181148','8889788786','9553506930','9394764445','9725590999','9725637773','8097688647','9016923169','9022772202','9958321693','9913435749','7060517017','8878878543','7040238260','9987172088','8796969684','9594564547','9873833087','8605575777','9799967374','8796241350','8082544124','7744985270','9550419031','9844959520','9586255127','9930675090','9066100786','9669569786','9822392891','9724981997','8553388293','9467672340','8898341947','8689995567','9951924664','9560817352','9050036857','9211455367','9811076027','8802866481','9246595406','8197532619','8275206812','9998893238','7053299461','9953309150','9420274930','9740001444','9428260546','9253553535','9886388404','9810105207','9700007050','9814000996','9811772295','9812282704','9913676723','9895128218','8461018185','9716805371','9329999416','9754096351','9781722227','9855315989','9722168786','9960371813','7387618353','9923967292','8566800031','8888083888','9762878143','9888707861','9030889908','8437735114','8793443584','8897892018','9900967641','7058129894','9502278089','9560134680','9639171829','9820274705','9246715715','9579208490','9768322118','8433696422','9860585153','7738178093','8268664446','9781094000','7743857061','8796838501','9033322110','9704970755','7048184817','9967555803','9727108888','9033240111','9898098095','8446496169','9052969529','8484042675','9718501251','7799191857','9892778282','9818957471','7053637079','9913897058','8010492007','7588609446','8010178337','9503020319','7028154829','9930748969','7709631438','7757886690','9010205032','8879587404','9743143163','9555892786','7387543421','9650086757','9426081252','8510995348','8446535247','9699991004','8800880046','9066686665','9175220586','9762590772','9425022055','8258040826','8050751917','9911868225','9136794673','8828458899','8103346602','8553321977','8860010006','8285297575','9022655144','9873541022','9038535271','9540311511','9981621059','9717514787','8791291925','9015558999','9000664885','9100337999','9762100306','8251043715','7838945495','8898544978','7503447334','8484836300','7208837048','9731111161','9716681284','9702152006','9867631432','9756330909','8971095499','9028655313','8882798423','9833972450','9403092799','9604307143','9972666888','8390610090','8826132034','7755986699','9999909266','9654478295','9727014544','9160333394','9818011787','9716493224','7836003996','9999939988','8087729029','7053728357','7019104856','9960931764','8374540140','7746078722','9540699724','9665875174','7065713084','7075750786','9211303000','9873024092','8470959191','8882996372','9945776184','8688610069','9015387625','8750937402','9946234432','9767525323','8285094766','9872729004','7276214744','9911705160','9893664376','8128346998','8879866808','9656955552','9666259711','9030907021','9033173980','8446753285','8087017836','9823657096','9776283791','9766558808','9892297675','8802853260','9920258772','9990654667','9998876804','8905808282','8758200004','9457974743','9247381222','9999931954','8827198284','9970888840','8805972749','9555208886','9350773293','7208124279','9716992634','9545612387','9727030452','7053309700','9545780764','7415543182','9890428852','8802516911','9650519594','8980745608','9160042222','8055559199','9687600911','9765715289','9963670689','8485844084','9826803512','9899103046','8520966676','8739963836','8080250200','9975786189','9890708977','8087231578','9716093525','9881373639','8871073801','9822020588','9156585062','8860339898','8287404413','9642239728','9033757111','9510202025','9967672286','8184859298','9210067913','9594408284','9016644585','9989739417','9949573030','7349859292','9618999919','9845338788','7079666655','9949592301','9015767278','8286622688','9948763836','8080216350','8275200256','9818772422','9811138200','9440246562','8095700763','9703776244','9971038384','7382890259','9967774577','9891230087','9820164682','9849022502','9908007600','9015401701','8390469352','9172476818','8285668268','9511941528','7053592935','9030134601','9726476719','9555661515','9910106567','9971324936','7531946055','9871436419','9623745147','9422848975','8530378786','9555555438','9008896658','9850938639','9881988356','9958015838','9818474336','9873656984','8800956971','9990672794','7838576981','9891103293','7208787642','8886860233','8686115496','8860097374','9990748636','8686522992','8802262750','8451073732','9654932824','8871659827','8439457990','8882322287','9034797174','8950921602','9911833933','9069578363','8888719188','7506071086','8530676486','7043372622','9958316645','7588603229','9773902678','9028907540','9727520151','9711364641','9898388806','9848378920','7208123567','9030602452','9850410497','9535168629','9766131756','8719975925','9090555555','9890533884','9764645060','8080753838','8588911317','7758848210','8464048287','8510957865','9867932790','8747068301','9768304971','9158507869','9901811122','9945444464','9533223223','9967302883','9657789953','8966088202','9837759608','8446613467','9910732187','9821626519','9885447388','9668329314','9029379777','8982076602','8826615593','7053718691','8055284921','7702700700','9712123555','9901951959','9900412359','9422232124','8750160430','7875997388','7046969360','9825642491','7210868790','8698540096','9685862747','9716991870','9960780880','9270348386','9012245124','9899503783','8802905701','9971781487','8010027085','7207117493','9818040567','9125926242','9111111467','9885162525','8017007689','7659062137','9811991722','9685086047','7489685873','9893364281','9428419323','7509020040','9910531501','8285768604','7053501686','8767975219','8179952995','9289610688','9555505031','9740368200','7349465561','9716991147','9165213671','8977770801','8767313200','7292036553','9650962363','9868066440','9743479871','8109902838','9654953660','9850295060','9920361589','9953721428','9312525800','7744994496','8099358751','9167330241','9492878500','9210835170','9716494216','9660156310','8652521930','7028364346','7774950656','9716884423','9764778534','8888397985','9969122234','7350099002','9773760007','9949815772','7798654255','9145154377','9960833956','9999646363','9885086477','8359080800','8655063041','9076947200','9923015798','9911995513','9773667880','8286291616','9440696048','9336661113','9209677880','9780442184','8177834854','9850176217','9891987348','8087421886','9479466440','9699086633','8446332411','8341517074','9022266000','9978006018','9893487916','7530862415','8087936124','9716708182','9409661227','8080080340','9083357303','9519444467','9765569364','7745806959','9763727221','9855798300','8587821812','9891203019','9899939580','9891916748','9769097942','8882940452','9823592176','9972244430','8974013388','9156860129','9850013134','8985876040','8308308132','9627787749','7208168723','9049750007','9844074621','9990650301','9716979737','8882323796','8693070809','9373915439','9762152215','9811817136','9822338892','8879073792','8510861982','8341150244','8499997744','9890121650','8454051997','9029200902','7804955603','8130259431','9063219711','9035248464','9618287737','8409031503','7204343525','9555564415','9850676933','9912432465','9595563107','9028864634','9028779991','9741233416','9845182481','8501906906','8801447999','9494231412','9848777470','8185938889','7097930363','9985248397','9968207564','9930476460','9989256303','7065566359','7049260854','7049261037','7702758190','9867176514','9640954130','9370936337','9718439369','7087131500','9870057570','9730919494','9716747319','8308789000','9586977832','9030906890','9505550323','9989403801','9765742140','9920303441','9927256330','8380898800','9036752878','9421311951','9958786687','8882816363','9990473265','9910811878','9831194345','8928148179','9250325450','9229666674','9663751511','9623861882','8446915049','9920172016','8080147009','9823601022','7030161743','7389536799','7503859897','8826487147','7078428367','8626094500','9866832536','9920607078','7741090206','7860728260','9491368375','7042510855','8971965888','8827268299','7206573000','9971853879','9225662972','9849964696','8510034354','7219193245','9422874120','8886859510','9766252801','9850452951','9987778700','9971037654','9599839595','9623923523','9300425871','9849037829','9540202091','8237690638','8019330447','8285116070','9029868339','9930474108','9010690846','9454047177','8050433323','9619857838','9013301847','9849367320','8019769445','9910628900','9014720515','8185080806','9503247206','9823802255','8879313020','8885665757','7354647286','9665588666','8527526812','8796660546','7702523143','7416147934','9657304060','8806769606','9540444777','8454938909','9175517504','9765355545','8898946708','9713109191','8468012789','8237373537','7012517698','8889324124','8379071931','9910327664','9975852867','9545883474','9819540002','8855014692','9711691401','9156866242','7720839191','8801858716','8390607094','8588069755','9641637081','9866157735','9818894275','9899549973','8882688856','7666888598','9492732331','9584175404','9979226012','9890721110','9810734754','9891476069','9584424140','9860899839','9700978585','9036386553','9768618483','8888368667','7045050401','9885393115','7744065353','9665977861','9850413446','7878469462','9718895430','9724582622','8881880118','8120080883','8221930940','8898370067','8898775173','9650363798','8686225108','9700049461','9822028203','9254918182','9822681907','8977262625','8291044729','9224290517','9891122117','9441325522','9555222889','7840066833','9893259140','9813112923','7017323201','9716358144','9819685336','9922151298','8019508419','8898014071','9253130361','8802552336','9911003966','8586037140','9972050544','7503574309','9909075176','9739951863','9028513470','7041142988','7738170391','9177743337','9850278484','9699169912','8291016663','7378929184','7416300618','8796346669','9768469119','9867801066','8511885533','9821776634','7053963893','9892526233','8806778004','9742008849','9765479899','9898292998','9716093541','9579464791','9960782108','8796693231','7448288691','8802549114','9818285308','9986115467','9555639799','7045246033','8688665978','9666954540','8687398409','9036303420','9014430133','9066336116','9246282829','7206165317','8862007775','8433864699','8423542541','8553735123','7208211836','9891993258','8290810093','9404239423','9555205005','8285076177','9454999250','7204688490','9703383848','9422807095','8604981721','7045148346','8128821274','8120243062','9910500643','9898620594','9716093446','8380856736','7013431677','9985660900','7032081237','9158423529','9558907192','9029856802','7070486467','8976685274','8460471659','9161038085','7697209820','8008182435','9553775175','7385077007','9768391108','9542233200','8652787924','8008996083','9892740425','9010608072','9848093833','9951731304','9527132014','9920465543','9652225272','9910724858','7899773637','9700668496','9100356033','9029745816','9533071164','8085618289','7075827972','9505750611','9770561620','9642236665','9990999601','9866356851','9963559393','8686308208','8983440019','8802222685','8099468310','8605050707','9849810195','9989753095','7793930898','9396427500','7210586033','9405941497','9632533376','9891077704','7219005630','8268227777','8729076528','9811725220','9902523296','9871819713','9998850979','9765220120','9999816625','9818789635','9898684750','9958871475','9494851288','9990356802','9555122298','9810830774','8802203649','9255252777','8108911094','9096579397','8425025042','8177883317','9767027777','9855766056','9821804658','8080906484','9021947242','7353972515','7379047362','8686908585','9713471929','7249580742','8007793332','8447620580','7291066186','7489008008','8802807506','8951036669','9019998888','9716190422','9028929950','9880282720','7509095771','7533034398','9892453412','7898487036','7693866720','9004955944','7838377915','8828054123','9820762128','9632135460','9623499947','9871345885','7303833335','9892790992','9930500037','9902248981','9594242425','9876980440','9819131473','8959309143','9911899882','9914299000','9222211922','7846053179','9975037786','9953820220','9989944787','9028149901','9981726015','7798610631','7888217925','9818209866','9063139999','9881546434','8424948486','9729813930','7737402466','7353003000','9175123542','7835981985','9493443905','8860013039','9902843027','7760538358','8800668831','7026255700','9806546508','8859281494','9820214641','9814433041','9356086541','9821376915','9948127698','9899095936','9074802911','9888153376','8699763701','9136592748','9911088005','9028240214','7709774087','7666641665','8793424973','9739562658','9764197797','9700604050','9964044845','9873704489','7503601892','9888763286','9540971774','9172729912','7340781113','7697134905','9599866694','9893333459','9873774318','9880380346','9770073430','7276122621','7799093577','9404995396','9957020488','9785079145','9811500255','9347240758','9989671971','9921603660','8587847106','9999192710','9899780863','9210851025','9538100820','9716237812','8050367491','9001536640','8147568045','8955508143','7718001016','7748013379','9702180777','9819741940','9920479309','9272539153','9136344110','9822716762','9711449004','8652541872','9403486033','9752161691','8890071003','9011759736','9637411853','7767980697','9654043347','9866514563','8802644402','9595959647','9844803392','9493233039','9226917315','9156115697','8802957246','8750298388','8898109076','9205715068','9742110965','7387597338','9006762387','9850207514','9999042564','7838255347','9704226215','8744040706','9899123986','9700055664','7738787000','9980516061','8379063601','9925700141','7666262627','8947028510','9542942311','9870729134','9769561358','9310909978','9479959188','9705496063','7532854026','9764232506','9727117998','7416410889','9039070001','9584445730','9175157007','9833087923','9914443247','9540626257','9920457936','8130457893','9819162795','8793788879','9624673041','9773861186','9892154999','8879428042','9654325454','9850917119','9892556151','8108971258','9981303213','9226806676','9716391182','9987884029','7208252550','8437654097','9773666676','9617636483','9930071676','9922277336','7053547984','9050654557','7021175859','7400820105','9844365806','8652018977','9490366775','9619450209','9393761000','9013684152','9892883477','9004630782','8698304278','9594926843','7738714502','9076218412','9144759723','7666115152','8806476796','9036343408','7045855405','9421475610','7869961345','7389726320','9820218180','8750114408','9782879801','9872806303','9650595690','8692008555','7588709542','9665716374','9773677747','9738772770','7899822798','7278131118','7039148407','8796300392','9611003567','7013802247','9989673231','7066157772','9638174220','9000228802','9910600431','9241444491','8108905355','7666676960','9014881122','8510088612','8882883777','9820378765','8897294003','9075427474','9967476415','9902414359','7738813736','8237064603','9022165138','8871842768','7387282716','9008999548','8968978023','9871051465','9716355435','9271718478','9827913661','9108770055','9033900089','9619030004','8421441933','9819335369','9569818339','9619790022','8826163999','9167050659','9753254414','7697762547','9174450818','8867484923','7827620098','8487855706','8446439681','8130545335','9891541044','8974172602','9619080299','9561345007','9569950722','9866366456','9970045856','9948123492','8460668200','7329071726','9666416669','7666320143','8010437659','9164364749','7288854520','9898393108','7760519192','9885692400','9293786787','9768122329','9059269626','9999916174','9702467577','9768590756','8097381020','7021082662','9848809882','9705823716','9004088902','9999096934','9329553953','8080780894','9004726957','9904470363','9923410931','9819160115','9953959697','9958716475','8683809009','7307786666','9702440472','7307107067','8459127478','9811199194','9573173727','9702660238','8517891018','9630698793','9810796461','9560763293','7666678700','9886889474','9565710841','9921731491','7303637195','9765819459','9716734354','9177671797','7755989983','8855815566','9273848482','8796292846','8237678712','7730018592','8268546091','9818356935','9848760638','9729785771','9848327821','9111460415','8888715085','8100492235','9019227981','7718929991','9958328310','7710962566','7307076916','7470318844','8879616110','9030089143','8980565093','9910127902','9766978663','9999397730','9892731362','9987298261','8879850067','9642999880','9422204253','8554839441','9953152224','9899556454','9769890299','7836917952','8341993992','9893327765','7675033005','9652232129','8424859360','9250607791','9819859614','9993680895','8143234887','9915427876','7869700102','9826456989','9702440686','9096649067','8149513173','8686849394','8978996669','9075642400','9310294500','9368735104','9987705243','8885188512','9096875747','7891434747','8510874072','8108289801','9925449496','9060419143','8889317625','7741091041','9848004457','8882157948','9310105590','8750166500','7503931069','9910117271','9890954403','7843033058','9891181070','9827682758','9717440125','9763236140','9015223522','9990109929','8806713434','9718688687','9741490005','7353525351','9615234122','7798111360','8726160457','9819009458','8424803450','7021142147','9811796914','9769337470','8801590427','9027323820','9029614921','9673136031','9848108028','7428324088','9716999176','9860800854','8054181198','9666358933','8096680400','9988440103','9250572828','9019421729','9822225065','9811940322','9028866151','9211644191','8727960751','8008499179','9555516669','7696600107','9989497315','8000004130','9059522337','9216755551','9034469803','8197981573','9250868686','9752325463','7533078597','8412953954','9615401695','9963228871','7042008467','9890682434','9743189302','9022535310','9569985197','9901804013','8983152391','9901764672','9322029800','8285591791','9967655687','9535418222','7568022533','9810801368','7755939495','8010770316','9945261346','8286592050','9878449993','7038680167','8080999889','8465964126','9015882951','9711889816','9637873869','9910308669','7532064133','9999666579','7838814306','8140091181','9541664666','9878375003','9177738744','9819260858','9701888653','8285094579','9700728711','7210869945','9985610283','8971814037','9646920690','9767699851','8380926638','7387474750','9819018439','8000674700','8983773638','8087666636','9879142205','9449357568','9813660921','7503529707','9440472106','9765932074','9850389457','9268970907','9762157595','9503003803','9999863535','9811188131','9822913916','9740378893','7724901377','9482711123','9898809705','9888862660','9139313466','9449494491','8527318140','8390132621','9738452632','9822036365','9793744893','9579609295','9425325102','8108910678','7042847174','8882331223','9763700774','8121332202','7022677944','9175215637','9833008028','9822723056','8268104969','9891406013','8285551613','9999447878','9096799977','7666322565','7045643503','7057201617','9985028591','9098009909','7835984469','9717907416','9845408125','9206357662','9425153301','8130500455','8471020252','9210003100','9407551785','7208247917','8459384233','9096587490','8085399499','9972499282','9533951954','9998573439','7503482042','9404489917','9945554616','9870662588','7659929402','8446332498','8793350820','8686536375','9741653195','7875777397','8983681442','9825195492','7738694117','9823333314','9885247800','8882830820','9769721598','9623496018','9250239293','9769694558','9023147471','9826591475','7532843588','8109910118','9039800090','9871785869'";
        	
        	$ret_arr = explode(",",$retailers);
        	$arrays = array_chunk($ret_arr, 500);
        	
        	$date_array = array();
        	foreach($arrays as $arr){
        		$rets = implode(",",$arr);
        		$query = "SELECT sum(sale) as total,count(retailers.id) as cts,date FROM retailers_logs LEFT JOIN retailers ON (retailers.id = retailer_id) WHERE retailers.mobile in ($rets) AND date >= '2016-08-18' group by date";
        		$datas = $this->Slaves->query($query);
        		foreach($datas as $data){
	        		$date = $data['retailers_logs']['date'];
	        		if(empty($date_array[$date])){
	        			$date_array[$date]['total'] = $data['0']['total'];
	        			$date_array[$date]['count'] = $data['0']['cts'];
	        		}
	        		else {
	        			$date_array[$date]['total'] = $date_array[$date]['total'] + $data['0']['total'];
	        			$date_array[$date]['count'] = $date_array[$date]['count'] + $data['0']['cts'];
	        		}
        		}
        		
        		
        	}
        	$this->General->printArray($date_array);
        }
        
        
        function moneyBackRetention(){
        	$this->autoRender =false;
        	$retailers = "'9718253010','9966858436','9824782217','9097627921','9891936786','8698737074','7053588319','9637469306','9833292952','9866697850','9887380463','8097041590','9275066666','9782206995','9167113113','9174286842','9210860068','9538794863','9925825807','9828819176','7503819594','9322812278','9887436540','9136286614','8588003779','9765246397','9612051609','8861253504','9888899906','7396734673','8424956032','9849084048','8375049192','9871323738','9768872974','9769796499','9873888998','8239905628','8107422031','9224114962','7738935695','8285192962','9724948888','7057333033','7719805345','7219292793','9915838886','7795493353','9718841842','9015401529','9822854770','9885027421','9582781716','9923635960','7302444510','7666829965','8767737692','9977955037','8527021522','9920888297','8562831803','9999226811','9702150543','9907223208','9561369240','8269554979','9501729163','9030901198','9769686999','9898241561','8000032444','7276381712','7899858807','8588980336','9555060935','9975531777','8000135572','9879360113','9821947980','9768232504','9133597307','8014465475','9880538569','9503335538','8285845856','7506869121','9823267589','9552180234','8975298031','9035251450','8905479670','9873659195','9891172145','9599515399','9898959594','9665339099','9966386736','9845074961','9975873636','9594673323','9702453185','9582252401','9823542922','9139445828','9595631088','9996444900','9652940251','7610001116','7042112096','8010650002','9890640639','9987866866','8806823131','8886213116','9850190502','7208793778','9768783999','9311854607','9637373760','9911115074','7588575200','9867547624','9300186060','9890097910','7045173226','9650689640','9620652000','9892987858','9595438585','9611647366','7411598517','9372031818','9407800171','9930374446','9764034248','8976303390','9960810528','8223886648','9723678040','7718001141','9022411988','8501841534','9666685577','9022121217','9414644064','8767377451','9879390822','9870122129','9503186809','8600222261','8237513583','8446576789','8976769380','9503121627','8007267876','7875968621','8408005150','7775059201','9545099810','9702684487','9849504453','9987931036','9313006583','8600312931','7887569697','9963787175','7411139099','9848009738','9855566777','9765683335','7303232798','9814393477','9313417716','9901842796','8422012368','9555607017','9711610939','7097524786','7737571408','7580986810','9888051512','7355058377','8283000294','9030101272','7355013208','9517260113','7754013947','9115263357','9417426390','7710833532','9891272680','9560528137','9781722620','9699494929','7207210131','9872290313','7415311504','8087905172','7083001956','8826973212','7709892260','7416324041','9823809807','9866672382','9527458018','8446710136','9960023305','9573742991','9508669183','9945752504','9210317790','9440103998','8802156539','9912249382','9821458521','7210303824','8801672212','8983597893','9669154890','9923101971','9145485773','9066214364','9036875508','7382026078','9703579624','7022221647','9404879266','9260592456','7207242352','9815554435','9970111123','9711509954','9899533300','8743929418','7503269500','8285745436','8010665111','9650604686','8793879313','9213612612','8008666615','8952931757','9030571251','7509859153','9885215103','9036991183','9716394696','9718295025','7396091919','7503090231','9069018208','9540518781','9650401256','9867627483','7530900950','8802541105','9899212947','9871682245','9990044316','9704078886','9860665400','8802538703','9999217309','9160092018','8802538972','9250603337','9209067686','9637239702','7530826822','8983509051','8454858499','7709820940','9945778432','7719865425','9036791900','9637633447','7530938025','8008543543','9985000777','8892126472','9582535312','7532089996','8130337251','9819877265','9069028077','7083212141','7877949445','9213831536','9908035635','9099210459','8010791521','9643639476','8520056928','9156456452','9850143829','9542187736','9503633893','7355634064','9248788371','7798372627','7875358888','7600073450','8223044409','7738611878','9741178705','8121341368','7841010629','9540414157','8080660470','9509643408','8290651316','8879305369','8088722832','9891062606','9911786898','9560247228','7503853530','9890066618','8149735100','9502747413','9213740000','9373413171','9811262648','9890028813','9574397000','8375951919','9049826776','9989622538','9704408539','7042543757','9918559355','9822998399','9022633720','8390949474','9441958486','9971626263','8955048412','9642441822','9624371836','7755915516','9555230757','8527867022','9515599752','9029325615','7791004758','9811979211','9902256863','9911362918','7292049183','8898162926','7727834467','7588331163','8928449090','9595245021','9702411411','9711941128','8121112998','9989496614','9766186269','9767046818','9494929977','9371202607','9891352090','9601654314','9205702116','8828351675','7799597793','9998281380','9968072537','7666566395','9029822280','9833460926','7058363912','9604737364','9970280524','9769164264','9999454068','9920445140','7879510055','8551851555','9867217565','9324747247','9826570018','9716540858','9096892071','9595949309','8425907040','9582531628','7697945436','9820622653','8154822784','9420594414','9422652385','7276249557','7710961619','7566848252','8652178664','9763423679','9960135143','8087603619','9584420497','8484846994','7530993786','8285494807','9594225792','9441457943','7692935614','9716476458','7049025014','8506069732','9860377681','7755972781','8744889917','9032346667','9409407721','9924877977','9930288425','9819161463','9818231079','9158332273','8446248615','8308679905','7045448002','8120039553','9999077683','9312576853','8390838774','9763749649','9815721213','9535650686','9023081344','7097828771','7303633330','9210167901','9930130133','9424700451','9716110311','9886998419','8510098997','8826730796','8097253128','8805175543','8237188964','8390304038','8379807250','8587813646','9617607997','9424253878','7053848255','8000624228','7698739377','9441560267','9069235381','9718516184','9922555690','8879439659','8087507547','9818401521','9911582445','7697502537','9970345612','7715082244','9909370444','8968523758','9595708050','9899979338','7405108443','9702621289','8483936883','9535653159','9977386955','9573864351','9617925915','9405290287','7053463408','8856875053','7620115251','8120875192','9977375150','7066285725','7276211765','9833323326','9911549414','9819792457','8802271565','9595991723','9990074955','9582842217','9028716562','7386159077','9085476618','7415145594','9990877985','9033831050','7383888164','9010498143','9810828218','8691925455','7069597360','9145550650','8522066678','9821144567','9891416744','9891702946','9764627903','9782946478','9770824212','9561461369','9665796146','7622900505','9650171721','9768700214','9755987124','7798812673','8767573033','9722712992','7210475924','8898187065','9828338495','9630321709','9270966777','8087815581','7057667267','9920340940','9716398239','7053321528','7875635516','9643940759','9211779810','9099575552','7715056834','9960727411','9404494834','7053877297','9420991002','8375892230','7875807574','9270543797','9824477782','8467070758','9975887969','9890171735','8551016056','8802157950','7210180003','8826578983','9989773458','7276238500','9881264964','9391387073','8099123444','8461029006','7045364506','9004979744','9910421738','9911493714','9959563331','9975959995','9819075940','9015161200','8120443334','8527936256','7101000521','7101000522','7101000523','9643416187','9029042391','7798270143','8879317982','8374843095','9867264198','8802029901','8898374426','8750599168','9560186213','9069058110','9323427056','9910746894','9899564583','9617056139','9601111010','9016087771','9665927649','9619954002','9300656175','9860284939','7040070068','8108260271','9468668044','9811410431','9552422667','9958358137','9421679666','9819112416','7030263264','8655389997','9867013463','9822092196','9743740713','9987156980','8898456979','8976961661','9594940294','7805993923','9136486578','7095651027','7709299865','9211872862','9766889333','9226900625','9211630849','9650698677','9914204541','9990822987','9958204676','9990444094','9561160844','7774806750','9821404298','9666565381','9212972744','9592390580','7838040978','8198883006','9726090126','8796292856','9467111868','9029447309','9653418738','7276311805','9911919697','8802529647','9494851289','9322082967','8143143050','7503307752','9885118106','8516070888','8691912806','9711579602','7503836227','9769804303','8287166966','9581903133','7666877998','7698800080','8898456200','8527616101','7083496296','9711129029','9173240188','9914496800','7406947143','9990767805','9643227949','7709067248','9427254047','9075641298','9015489099','7666310634','9811684179','9527298647','9914817653','7798192405','9716194038','7859991116','9687462448','9871883225','9871245071','9663391841','9723511708','9540540898','8050029169','7203053810','7355632424','7053834240','9347702999','9949944849','9892563263','9449834999','9028945790','8467081235','9904511723','9949930414','9999288511','9739173986','8287213214','9085969547','8962985744','9702514711','7101000524','9004025855','8007375400','7042989920','9271433042','9819220957','9833793446','9717138786','9926342546','9595729977','9620796156','9599243534','8796465153','9691714408','9890553511','9910495912','8422008477','8108534563','8447705058','9540891615','7678039231','7359839447','7669657049','9082623000','9542425512','7210233804','8237769619','9069322414','9822779592','8750170721','9028313436','9716394313','7770809653','8898272126','9891342876','8624835535','9211373931','9989315261','9718643489','8983737478','9575559525','9111444870','8802919191','8412049627','8082966786','9069669744','9958509755','9766024111','8866121426','9999732182','9096336458','8108462576','9599956014','7276324904','9004882792','9591941518','7718816466','8285921058','9320229960','9872722117','9908209911','8401116063','9889755515','9351721767','7024160773','9657120036','8080744400','9555997719','9731976326','9028482314','8010223233','7306219070','9637444696','9890657875','9172771380','9076999998','8452999902','9441936523','9998174646','8285861507','8989779254','7875995160','8008888853','8806215015','7840057397','9899476817','9274711455','9322008531','9069570573','9069724745','9417851201','8149711081','9702769476','9711774311','8285093741','9000686185','9866709811','8374478903','9849076855','9049388174','9723891770','9533249999','7387192332','7529942657','9158074700','9441521298','8374269361','8802011712','9990482782','8130373327','9867761599','8802549994','8287393047','9212053241','9818740017','8185010829','9849468882','9998842759','8801425265','8010758082','9673584751','8858872217','9820119810','7875353702','9049096353','9676987624','9205808499','9441094479','9849866992','9753000694','8689871219','8885848848','9704048491','9010175241','9205152874','9154301809','9561092192','9978104455','8600724207','7786824357','9968563869','7698977878','7508004843','9550024849','9177886405','8767464643','8686058495','7691907016','9920164275','9818970457','9494837575','9540837740','9540228035','9924865683','7075268700','9871783646','9926004781','9133118494','9958747438','9560474700','9811961450','9963936352','9753798160','7387507136','7042547275','8801623786','8008717503','9560272971','9959637387','9479337541','9662726121','8130078722','9990384024','9951942143','9372959596','8225847208','7530974510','9718904060','9702872474','8802512334','7053469754','9723616846','9820026908','9916344658','8923165165','9049886279','7304444494','7218796689','8275541697','8712772508','8000446004','9819074963','9972627751','9540485438','9640990103','9987593888','9990439612','9963162774','9505307556','9819052342','8793180553','9892958518','9849449185','9769629702','9822932680','9019096252','9970211327','7675912292','9650350122','8186008009','9725540970','8605209361','9820973592','9770222224','8983100710','7758043311','9594938356','9015303427','9716491281','8143438585','9130431827','9228904895','9920431022','7042535775','9810894714','9582520263','9888103458','9999749944','9998843489','8010484794','8793287028','9654222330','7743965181','9099860893','9989105738','8962985724','9717832324','8488888424','9898788599','9922030894','9717374707','8686217504','9737776773','8285612239','9599679767','9999552772','9911887076','8460051781','9664652424','9848488541','9902477222','9881802026','7038659350','9179414443','9766525252','8743023480','9855498306','9036302469','9990088609','9703311055','9213243843','7065881695','8586968879','9999740291','9871859240','8225901590','9860271094','9949968119','9028866689','9604089804','9032297588','7048222958','7566522566','9848716006','9015304061','9999326175','8143775599','9990491188','7503934364','9911756517','9819139106','9975877315','9665975171','9669229917','7785924244','9899855008','7803026265','8793517951','9175431994','8435076309','8655412061','8802784206','8506992815','9666448779','9653405310','8305832564','8989795105','9872326020','9139379020','7088584476','9423383184','9023750502','9729442038','9888840230','7069131024','9646819525','9892914880','7405206902','9762294143','9260606794','7503413106','9420242006','9953677769','8551959551','7715033005','9958524104','8196990486','9844557789','7875115707','8554077375','7532052522','7503333381','9990400390','9666515858','8419988403','9718151584','7503884221','9891018350','9205149566','8010280426','9959594332','9371711669','9637540585','7709168968','9860101786','9772071955','8796816448','8743047839','9820842786','9765227833','9560092235','9999908893','9893011127','9591851999','9689660257','9503981222','8600371215','9716968544','8505962871','9714884005','9624945709','9810425388','8380921789','9850631068','9898082391','9974327750','9960008037','9953181516','7530953991','9533926788','9953540053','9711708721','7355670024','8983390203','9825806629','9172838087','9833963909','8308878851','9527103319','9323281008','9555010008','8130165400','9705053674','8655760441','9885347899','8527168497','8801772299','9966667998','9822788866','9510026060','9892393532','7387866309','9814154447','9823266910','7089158196','8285092771','9971738130','9599233397','7075257588','9716447186','8801482715','7386561000','7350406789','9890079751','9891303202','8080601477','9638329018','9716405060','7680069870','7813822332','9909153452','9898353987','7406407934','8285148699','9640119169','9573848182','9891303239','9921142158','9898704516','7874937418','9925940465','9212381060','9022221004','9672536416','9724643220','7101000525','9890576104','9730347982','9949812362','8285158723','9860964131','8750976035','9985011959','9833085989','9810622821','9158997711','9705053868','7416676806','8275171160','9849360418','9743624407','9971140269','9966002022','9819994254','8600837088','9662793984','9987404033','9887415615','9738939409','8123537224','9533939533','9136282309','9916131001','7503242014','9032660340','9860161023','8747012342','9552932146','9923845482','8762725563','8587863288','9623237774','9872656127','8285061530','9949034182','8866600049','7794029921','9780602200','9738578143','9049424974','9898144173','7042948008','9054922200','9911325479','7411426623','8099966633','9540044014','9990930828','8427396216','8983122253','9873231234','9998648103','9872733375','7531968078','9691760007','8977512253','9975675382','9960497310','8379815407','9545918744','8308129950','9925902699','8559044257','7730925501','9926938382','9010577899','7702508065','9052412121','8720000822','7691904356','9076160786','8185945555','8142562143','7503347277','8008441550','9096119620','9700000637','9096109191','9766666578','9555788350','7068982370','9033047400','8948435273','9700007886','8080775776','9921474845','9604060481','8876238662','8872630122','7219605852','9971053659','7069213821','7053067181','9601835504','9175330969','9811085343','8875749945','8485825249','8686683880','9810312068','9974178305','8750451858','9146782929','8097358054','8888898755','8626083500','7263989717','7875162193','9810071062','8483988662','9096070077','9423491371','7709881241','9004421222','9425650064','9221326785','7406795576','8485939875','8333072789','9848243717','7776042562','9926943909','8108968143','9423302631','9156909052','9391633933','9923568818','9136445159','9247739475','8143555502','9102815169','8600708600','9966472620','8878916160','8879833003','9885427038','8686111833','9687617599','9666113144','7065542661','9173273642','9987145911','9015513313','9665747644','7304568697','9781787528','9891561743','9923057327','9839173381','9409372533','9637898387','9010707143','7040688701','9999666067','9516098643','8080864385','9173311888','7532999286','9139393924','7838423248','9638744677','9780344640','9998274848','7666039004','9540818687','7802991336','9066334402','9948770404','9966656422','9824395688','8802709808','9923316363','9886646668','9923099007','9911881415','9891911148','7795667795','7276383235','8287190052','7795425104','8800376601','8866428707','9601351845','9818875152','7053856223','8285124078','7729056993','9891253600','9754404245','9738887245','9601141991','7355942000','9008339090','7835954801','8885463339','9871046137','8885670567','9716093242','9666656696','8285832130','8285883423','9730341380','9716492934','9491078134','9676402029','8587034561','9881664471','9998783322','8512852015','8977299789','9867700609','9617020381','9346514143','8107623181','9977044628','9899046874','8467888149','9850581901','9618318351','9415868423','9716093201','7083783587','9820495385','9592929258','8146982014','8506031954','9999990000','9920489121','9130199299','9713112586','9999027399','8527431229','9464176922','8976603684','9505692553','8454077590','9891497635','7306331404','9867147753','8375817185','9873986682','9987176078','7066602229','9892597018','9819900069','9852297053','9074444461','9873388900','7276065583','7996092936','9930772478','9599692245','9716292865','9930323032','9818391471','9115284601','9823339954','9867123124','9818660652','9029889428','8447520866','9722124332','8885566156','9039557678','9039645245','9154547056','8285494793','9909087654','9879278238','9604035798','9725792449','9860107520','9820707944','9960311181','9302994161','7416607557','9390064617','9039224274','9893222245','7799995501','7359720489','7600872251','7350146544','9033386997','9845148799','9290104020','8143276590','9737551307','8652897267','7330860379','9844917590','9699466695','9953128317','8096444204','9666054345','9553322454','8982620857','9967159494','8686482098','9723825655','9676869395','9136661531','7036691033','9599508682','8587045375','9967822399','9908116777','8520057748','9302333995','9993393323','7065124105','8605304631','9172387378','8879110146','9052575442','9221616168','8985555365','7758884999','9904432803','9516779102','9665742577','8743900462','9122140223','8686200183','8080082088','8080828143','8697811687','7738412013','9988442281','8652706300','9990472199','9987556510','9845600758','8725066342','9918804452','8380908360','9503228494','9999512046','9737417958','8553337471','9818585142','9911179686','9049594139','9158144449','9211534952','9685931629','7503115418','9664223323','8860308906','8605214715','9766646813','9740890488','9871153840','9096899905','8587079156','9653920001','9425835635','9860626340','7276728117','9000744474','8802104668','9327200931','9060076102','8750021441','9665520222','9075826011','9911110428','9892832153','8879060485','9666063059','9948046456','9921629909','9849227625','9885764234','7304888871','9849228213','9885568855','9573537829','9769617082','9949005713','9654561040','9665424222','9540632075','9010303039','9172360330','9849184977','9700500711','9949005709','9820248764','8670779895','8686703410','8108808999','9718524612','9718776604','9920618424','7276794218','9584231728','7383491103','9242739848','9911620722','9049909077','9619130192','9848734530','8686465523','9866435991','9973890552','8096195485','9004685632','9611226364','9848614894','8287520329','9832310505','8796092069','8652780671','9638412019','8758448448','9820938603','9867600825','7028249488','9022559331','9979770088','9619106841','9922885303','9959351403','9033034891','8803836021','9022215598','9811450441','9038102390','9993066696','8149736765','9971012772','8800894094','7724902755','9755827204','9959594318','8440000206','8285026849','7276660700','9978549898','7771084444','9726155594','7794845551','9977881420','9892922316','8743891161','9769878887','8567929619','9948104854','9611148143','9990934462','8019275415','9066686444','9510618065','9989165251','9811479716','9990777290','9866983143','9666770545','9221809942','7053316332','9686463254','8527738288','8050010375','8586006301','9595389521','9643293144','8431112363','9062525419','8885891567','9867413127','9686633378','7738786143','8177866225','9545751343','8650107467','9324773652','9000071425','9312127220','9985399673','7330292591','9392395959','9560678731','7065630390','9922416480','7531039240','9574864886','9958465334','9819808619','7799119727','9910295898','9545535977','8000007578','9815931888','9999759723','9871780443','8123062280','7500570518','9987882957','9738090694','9999791860','9158954888','9339430385','8447003131','8108877794','8179770469','9169891600','9925650808','9158829101','9553588552','9985754050','9293102670','9493061406','8888645282','9666446701','8553173242','8800509704','7415603288','7059491631','9390307973','9417061295','9056226879','9824514620','9640047717','9769669972','9662069143','9433994129','9426023931','9914712081','8699522017','8087640363','9825165094','8505872052','8886106869','9898189468','9550082891','9741801300','9039312510','8655259530','9892653208','9892605913','9624675786','8796829049','7405510697','8959300879','9535513307','9210086834','8793660070','9540823545','9718566636','9699407622','8967945962','9966992347','8872631802','8828180227','7263828202','9831035397','8750915722','9820648781','8600501696','7778833000','9811529522','7670978098','9669444525','9211111019','9592964006','7276005652','7666777102','8374524808','8483871127','8512858515','9911850153','9860237506','9716937559','7039743245','9650347156','8600000103','9811857903','9716848817','8143294800','9702453772','9066273175','9560674638','9849842010','9702158313','7798467218','9619921067','7354062783','9899599845','9867869828','7208100070','8421839898','8802573513','9718443122','7058597111','9620502008','9066242069','9871783654','9718083413','8801425473','8879167802','9866604039','9948916747','9494212299','9702144337','7721819055','8802812750','9702840826','7743821265','8375908138','8806890751','8447902903','9849405108','9669235039','9764000600','9755455544','8861090118','7204230085','7697042369','7666450714','9824694318','9971209651','9890993377','7697333918','9657777460','8888913991','8548881500','9971868698','9792617546','9765999947','8689882720','8866488332','8750932732','9718527118','9420429581','8691821602','9273178900','9885777885','9716094574','9987892923','8237781088','7039715643','9463794700','9011328441','7210600793','8879375238','7738300746','7798488485','8080907399','9666803235','8275009371','9004188356','9540630765','9721511262','8802582363','7378658773','8655555356','9425409835','9448423578','9714891395','9891528237','9669534643','7416417857','9133003005','9967171128','9637373728','9322186410','7290827769','9300616112','9871781914','9987321432','7411325343','9074487171','9902984460','8686918786','9970002801','9971728208','7038258852','9773747847','7741066221','9522023432','8745066893','9765145995','9666979701','9891424849','8600484948','9289430130','7503406107','7053431674','9891415040','9953231314','7746999880','9922424662','9769246655','9038031222','9899005859','9895694328','8341682999','8626090505','9702729950','9503610547','9209546162','9624067858','9028587642','9963688886','8962682821','9604855487','9502991130','9987357842','9987342275','9881320939','9824935600','9811483604','9873642904','9066669396','7503588835','9708444030','9063879796','9967608143','7623947132','9711951982','9623934115','7887531097','9004585960','9396908353','9781085959','9066997070','9358221996','8750527029','7875543112','9867199919','9898141927','9967366377','7387996386','9848767604','9654448202','9773214587','8866565509','9167590720','9058078723','9818982393','9594398180','9828198381','7666111517','9154796166','9966054843','7744080811','7276101017','9725265977','7738261241','8872621792','7531999696','8723014844','7666097609','9987852411','9990136830','9763701156','9702683695','8553383080','7218049787','9700192775','9540883838','8550959899','9594431343','8490909451','8487997420','9781549007','8800969525','7875504783','9722222337','8287803649','7692909430','9900104787','8010609900','9766401810','9879471486','8855958383','9665769787','9850080152','9708813743','9999899395','7387025589','9321201435','9702786323','9154323452','9958046944','9702783164','9654549797','9702653641','8595424523','9971558732','9555042928','9979053934','7210946159','9158532658','7794094959','9844673015','9014649460','8698840571','9873992022','8888100223','9700666040','7738735915','8482842994','7210651866','9867068685','9741280676','8510906823','9899593248','8225091919','9594740403','9824421368','7738815629','8796155956','8285093622','7038632989','7498668319','9449443016','9815014697','9823311731','9970073725','7509001705','9075799948','9681344012','8286192204','9867525128','9717226772','8698529030','9035063821','7038023636','8962186479','9967679946','7776029260','9824367467','8149744208','8461084093','7053611739','8108713671','7276063365','9762791715','9799991494','9738446142','9811061328','8470025007','7066732292','7697016543','9172523400','9666325151','9822680008','9714381002','9819937028','9855294300','8433865292','7532880177','9924723298','9772668857','9945378277','9246996969','9424058863','9686102105','9375744144','9466690189','8796792047','9763112323','9782879113','9650735528','8308403214','9494622965','9999598172','9212121051','9595337176','9702377115','8197842222','9833229026','8233455102','9636598163','9718224525','9725479943','9890606671','9044156896','9660867812','7709238693','9011536333','9028812776','9845148392','7621048295','9910474733','9892151480','9740717114','9810036224','7204924475','8989525152','9923185565','8469580484','9999490427','9987471081','9424937722','9767997808','7385656566','8802785184','9545240754','9871630063','9892244404','8826536211','9881228072','9555232967','9700176563','9175380213','9289234114','8982371889','9572971780','9163512240','9833194714','9290183212','9873000252','9705740277','9425558326','7768822010','9714358948','8109437301','9870516221','9322379620','9535005805','9891345754','8855037050','9163795406','8712129942','8983204875','8605379797','8096565757','9598010136','8793666474','7498358767','9799229846','7697442022','9691072376','9899156055','8264786893','9819799183','8080850840','8527818477','8268849045','9730227503','9867267408','8286901088','8105569705','9766066433','9650362215','9175258793','7533035000','9810041513','9975390104','9987788584','9810514784','7053868695','8802123842','9988941211','9595313162','9740158109','8087471236','9921485285','9867081322','9717874137','9987160552','7416608396','7411134530','9867546325','7838979194','7709063436','9555911544','7500610147','9004731247','9819656515','9810007478','9022662911','8625899797','9885670722','9175483482','9704006364','8792397934','8792729287','9074773026','9718697858','9822221297','7057955959','7875773184','9891353480','8978890457','9711485086','8688556363','9550887709','8010681600','9032915255','7400606323','8975445251','9890219123','9637216883','8285000659','9409392539','9008924941','9819528711','7030314497','7046619552','8097345620','9768534741','8430811314','7378979876','7350135757','8898682889','9911340676','7696106812','9921285766','9923081224','9781346410','9420736946','9891873137','9928631405','7053764196','8898701542','9246907390','8285090993','9098049899','8142283998','9000627156','9350072947','7665026456','7276781188','9911078644','8471041234','8624031249','9666980422','7013603026','9582398881','7042236413','9803862262','9494103458','8287551820','9950929872','8500348142','9953600326','9211678211','9010465555','7030787813','9502204957','8904740039','9494091677','7208214364','8185816741','9613261003','8285091069','8007440079','9702809009','7276357880','9830068955','9762192189','7506668080','7588515117','7290000729','9971584007','9910347777','9015989517','9205201633','8179999962','8854000707','9811836976','8087141112','8464863735','9880008384','7389252222','9819075729','9069644984','9212952871','9981338520','8050521259','9824788151','9764442333','8561011786','9492636397','9911057635','7288071104','8743971033','8691876368','8285865019','7738448002','8556839194','7587524958','9921002809','9810577717','9716394194','9491704183','9560216770','9781932917','9891494922','7040437700','9716815475','9689052333','9742444101','8806834250','8557839626','9270553337','9821042859','9663197192','9049359929','9015776577','9000560064','9850182340','9999917220','8379920383','9731333017','9873015610','8374587334','7838303168','9987344511','9766640278','8982103625','8237153860','9654606042','9986910706','8271522233','9021955151','8886885855','8435485947','8008459009','9922384858','7065358363','9553599139','9350685541','8750153277','9873505733','7673916361','9912053381','9646663268','9654113202','8866886106','9004702896','8097074443','9928627898','9491607786','7420986285','9038305003','9313636355','9666617770','8285042155','8826498984','8454013676','9461600737','7208704231','9209988879','9888225431','7702636669','9038388278','9491568349','9881236758','9716586387','8879254641','9711881049','8805710745','7503091402','9423859272','9099029687','9908829143','8285592708','9036241294','9990111361','9910905800','9139944073','9205701592','9822377838','8801633501','8019194407','8087601654','7416447857','8743891492','9782406139','9782368668','8806756043','9676133331','7097040575','8652735224','9893549448','9818671528','9993809707','7415566336','8600964473','9617540772','9909116143','8600559009','9246592757','9850580918','8871374949','9921779453','9250006800','8125602135','8087444810','7661838143','9716620201','9948449527','9290849489','7226822440','9850542011','9650116490','7503683777','9036229478','7803993508','9963251980','9666394444','9960724977','9702991945','9754767688','7053852434','9987711981','8808101209','9819209710','9324169936','9762253415','9867844644','9860208666','9717183617','8879571230','9642903007','9821992020','9028237742','8879837008','9672073084','9700401731','9892924307','8885499950','9570081966','9948158090','8802948801','9573735849','7803037264','7207135987','8885733734','8605500680','9822476621','9822034281','8983648294','9848675177','8087595900','9247180887','8898234109','9779410008','9455572005','9973813545','9711547526','9300001707','7531839457','9000875244','8983114007','7218565946','9888623567','8010779671','9718282315','7259225799','8080284436','9967461008','9666716196','8108549484','9705060906','9999941184','8527052444','9999649647','9270576943','9177260680','9611762369','8750723972','9971272760','9527449004','7249753879','9323142654','9921700622','8054023098','7089312479','9491910656','9762532747','9347398109','9700630219','9545417060','9999985519','7058551663','9803490457','8411873173','9601409668','8897199087','9542994198','8750260927','9966222364','9270456900','9226762452','9166939494','8882177469','9640943160','9782373132','9967227835','8484977945','8057287417','9873397366','9624458284','9981861664','9930749986','9210449555','9323455777','7350858503','9623078875','9052229393','8650454912','8567800772','8888450653','9990998843','9584875697','9685464400','9824370859','9819811047','9930566311','7413091301','9716093582','9770130144','9999229998','7738253351','9716093299','7259846936','9969657858','9972222678','9818894284','9716993428','9833391962','9891993170','9990547714','7531879798','7261918335','9951574190','9910428020','9015648678','9757476848','9011544704','9873800110','7042469829','8184939990','9160098369','9015412687','7738984835','9899470410','8390654242','9440075458','9920308574','9999877949','9824797177','8802923746','8238855151','7818885857','8275054109','9716591367','8686138116','7287862086','8750451168','9558292901','9925257862','9137345400','9490855820','9666564512','7065991969','9848703417','9711711743','9912361613','9673388333','9000371435','9030540456','7405116123','9059246243','9892959818','9210421042','8802537918','9730324105','9546810738','8601864391','8264550195','9706855966','9899674748','9589671176','9250588342','8888478415','9654689993','8285392805','8882223683','9966881922','8871711918','7405173731','8655567663','9818538920','9041265124','9899378666','9899860074','7828181133','8019203476','9923890051','9662121495','9049064864','8185984151','9571775725','9879974999','8652811606','8691044421','9924949820','7208267374','9766699867','7875537548','9555188249','9638334445','7840020403','9979078444','9714108699','8567812954','9819909039','9624288045','8080092091','8522019008','9967791497','9902924493','7210976190','9324951687','9737515188','8130979032','9160607862','9951696787','9665501554','7036794725','7428088777','9987435364','9711314722','9076666662','7776805045','8510918961','9989405339','8946990877','9924223161','9978095675','9848837678','9871995997','9885448448','9136511834','9899207317','9898248224','9779682590','7503093960','8151986645','9644660077','9599899086','7567869214','9987012552','8719830090','9891093305','9818281768','9022243580','9990989921','9393199143','9717378220','9785228204','8099112837','7692953737','7748903376','7075672288','9555942628','9015440233','8446662656','8872055108','9974486854','9881664488','8010719482','9004702551','9661981465','8285452249','9096742680','8866021586','8411032143','9175919090','9911413385','9924536438','9948005482','8585072466','9922449397','9966133337','9448895732','9160796987','7615974387','9213169191','8308786825','7066880057','9960780837','8180841001','9987434620','9433427710','7738874740','9642004143','8802315476','9849026878','8802523971','8225060688','9581264445','9922378589','9000209922','9738227770','9990600932','9350903141','8855048819','9505522368','8007589377','9604319851','7666969543','9849485815','9780258225','7859803136','8879046056','9210421822','8600560761','9015260278','9390443034','9028206200','9768200275','9978110565','7097212127','8888505435','9953254312','9211550752','8983124560','8686471105','9959986684','8826936556','9620856219','9845053239','9699091144','8341239889','9867129719','9582408240','9210326490','9440224328','9911771133','8888019089','9764590440','8085376647','9922566581','8097276665','9890886989','8484845151','7411912616','7219521920','7053846653','9650678035','9491395901','9898414147','9397838555','9908238852','9899180556','9990412197','9175646441','9970326937','8855969001','9584203526','8470992396','9210333630','9713930124','7503417819','9988851418','9717399394','7028299524','9689707465','8130108992','9770489000','9716939212','9000736900','8484833101','9704355808','9673277786','7387196834','9820108799','7388110080','8374754133','9650117647','9716317377','8802388766','9924625257','8897474886','9926181959','9975299734','9643163066','8341251627','8109994552','8149415036','9873137393','9913976551','9555355636','8801334988','9824428022','9540408376','9222029665','7075553609','8802877091','9555869998','9136658919','9220050092','9717099373','9001182856','9136119137','8518063523','8374767341','8130605188','7250332649','9951786083','9100183046','8888739012','8855927474','9021070707','8341527861','8297053928','8179957544','9902929238','9174817350','9553845080','9059335442','9850320004','7600813845','9553095082','9819126201','9898799455','8898919262','9689808954','9000395759','9015878593','9716616461','7531960982','9028212811','9860190447','9028906466','8130256509','9871981492','9927618055','8553317773','9958335527','8743909706','7847845555','9990587136','9972278506','7024485212','8149020308','8419945905','9460703239','9902090922','9987940387','8800975814','8892061537','9177777648','7773930220','9708065673','9619708299','8412818247','9740858590','8149879575','9592251348','9885222919','9958389329','9420214587','8237208293','9505523956','9560901973','9911033393','8511681014','7738795229','9964642786','7383070166','9970143775','9927844561','9329132999','9665729972','7798198592','7303991330','9603827104','9825266593','9421051465','9341313750','9594234749','9063118205','9813200749','9914511353','9391750658','8238261137','9978364383','9300445244','8238977854','9139833753','9689536628','8971317770','8409573075','9970073888','8285094246','9685022991','7569116450','9915810110','8297025174','8983615196','7666623441','8802788404','8888280999','9867108208','9041501107','8826626264','7385451010','8750517254','9870001432','9311451470','9765666795','9892927704','9424321536','7898405999','8186080964','9673341685','9973884089','9663121456','9676812633','9958776043','9860601212','9810465337','9987855760','9990088111','9323428008','9392519979','9213392199','9963301303','8889086985','9250700801','9990909214','7385375555','7784093891','9910393963','9619875376','8553194732','9099237170','9323309573','8553086753','9880763521','7030616326','9739488786','8793125189','8690035229','9408563921','9420001117','9930750780','9966778646','9763870553','9866766613','9726915436','8802739002','9630545349','9067941982','8446834645','9428602638','9866409079','9620923737','9062868509','9891762422','9825166136','9665031112','7567178079','9522112295','9912986638','9978135303','9879181148','8889788786','9553506930','9394764445','9725590999','9725637773','8097688647','9016923169','9022772202','9958321693','9913435749','7060517017','8878878543','7040238260','9987172088','8796969684','9594564547','9873833087','8605575777','9799967374','8796241350','8082544124','7744985270','9550419031','9844959520','9586255127','9930675090','9066100786','9669569786','9822392891','9724981997','8553388293','9467672340','8898341947','8689995567','9951924664','9560817352','9050036857','9211455367','9811076027','8802866481','9246595406','8197532619','8275206812','9998893238','7053299461','9953309150','9420274930','9740001444','9428260546','9253553535','9886388404','9810105207','9700007050','9814000996','9811772295','9812282704','9913676723','9895128218','8461018185','9716805371','9329999416','9754096351','9781722227','9855315989','9722168786','9960371813','7387618353','9923967292','8566800031','8888083888','9762878143','9888707861','9030889908','8437735114','8793443584','8897892018','9900967641','7058129894','9502278089','9560134680','9639171829','9820274705','9246715715','9579208490','9768322118','8433696422','9860585153','7738178093','8268664446','9781094000','7743857061','8796838501','9033322110','9704970755','7048184817','9967555803','9727108888','9033240111','9898098095','8446496169','9052969529','8484042675','9718501251','7799191857','9892778282','9818957471','7053637079','9913897058','8010492007','7588609446','8010178337','9503020319','7028154829','9930748969','7709631438','7757886690','9010205032','8879587404','9743143163','9555892786','7387543421','9650086757','9426081252','8510995348','8446535247','9699991004','8800880046','9066686665','9175220586','9762590772','9425022055','8258040826','8050751917','9911868225','9136794673','8828458899','8103346602','8553321977','8860010006','8285297575','9022655144','9873541022','9038535271','9540311511','9981621059','9717514787','8791291925','9015558999','9000664885','9100337999','9762100306','8251043715','7838945495','8898544978','7503447334','8484836300','7208837048','9731111161','9716681284','9702152006','9867631432','9756330909','8971095499','9028655313','8882798423','9833972450','9403092799','9604307143','9972666888','8390610090','8826132034','7755986699','9999909266','9654478295','9727014544','9160333394','9818011787','9716493224','7836003996','9999939988','8087729029','7053728357','7019104856','9960931764','8374540140','7746078722','9540699724','9665875174','7065713084','7075750786','9211303000','9873024092','8470959191','8882996372','9945776184','8688610069','9015387625','8750937402','9946234432','9767525323','8285094766','9872729004','7276214744','9911705160','9893664376','8128346998','8879866808','9656955552','9666259711','9030907021','9033173980','8446753285','8087017836','9823657096','9776283791','9766558808','9892297675','8802853260','9920258772','9990654667','9998876804','8905808282','8758200004','9457974743','9247381222','9999931954','8827198284','9970888840','8805972749','9555208886','9350773293','7208124279','9716992634','9545612387','9727030452','7053309700','9545780764','7415543182','9890428852','8802516911','9650519594','8980745608','9160042222','8055559199','9687600911','9765715289','9963670689','8485844084','9826803512','9899103046','8520966676','8739963836','8080250200','9975786189','9890708977','8087231578','9716093525','9881373639','8871073801','9822020588','9156585062','8860339898','8287404413','9642239728','9033757111','9510202025','9967672286','8184859298','9210067913','9594408284','9016644585','9989739417','9949573030','7349859292','9618999919','9845338788','7079666655','9949592301','9015767278','8286622688','9948763836','8080216350','8275200256','9818772422','9811138200','9440246562','8095700763','9703776244','9971038384','7382890259','9967774577','9891230087','9820164682','9849022502','9908007600','9015401701','8390469352','9172476818','8285668268','9511941528','7053592935','9030134601','9726476719','9555661515','9910106567','9971324936','7531946055','9871436419','9623745147','9422848975','8530378786','9555555438','9008896658','9850938639','9881988356','9958015838','9818474336','9873656984','8800956971','9990672794','7838576981','9891103293','7208787642','8886860233','8686115496','8860097374','9990748636','8686522992','8802262750','8451073732','9654932824','8871659827','8439457990','8882322287','9034797174','8950921602','9911833933','9069578363','8888719188','7506071086','8530676486','7043372622','9958316645','7588603229','9773902678','9028907540','9727520151','9711364641','9898388806','9848378920','7208123567','9030602452','9850410497','9535168629','9766131756','8719975925','9090555555','9890533884','9764645060','8080753838','8588911317','7758848210','8464048287','8510957865','9867932790','8747068301','9768304971','9158507869','9901811122','9945444464','9533223223','9967302883','9657789953','8966088202','9837759608','8446613467','9910732187','9821626519','9885447388','9668329314','9029379777','8982076602','8826615593','7053718691','8055284921','7702700700','9712123555','9901951959','9900412359','9422232124','8750160430','7875997388','7046969360','9825642491','7210868790','8698540096','9685862747','9716991870','9960780880','9270348386','9012245124','9899503783','8802905701','9971781487','8010027085','7207117493','9818040567','9125926242','9111111467','9885162525','8017007689','7659062137','9811991722','9685086047','7489685873','9893364281','9428419323','7509020040','9910531501','8285768604','7053501686','8767975219','8179952995','9289610688','9555505031','9740368200','7349465561','9716991147','9165213671','8977770801','8767313200','7292036553','9650962363','9868066440','9743479871','8109902838','9654953660','9850295060','9920361589','9953721428','9312525800','7744994496','8099358751','9167330241','9492878500','9210835170','9716494216','9660156310','8652521930','7028364346','7774950656','9716884423','9764778534','8888397985','9969122234','7350099002','9773760007','9949815772','7798654255','9145154377','9960833956','9999646363','9885086477','8359080800','8655063041','9076947200','9923015798','9911995513','9773667880','8286291616','9440696048','9336661113','9209677880','9780442184','8177834854','9850176217','9891987348','8087421886','9479466440','9699086633','8446332411','8341517074','9022266000','9978006018','9893487916','7530862415','8087936124','9716708182','9409661227','8080080340','9083357303','9519444467','9765569364','7745806959','9763727221','9855798300','8587821812','9891203019','9899939580','9891916748','9769097942','8882940452','9823592176','9972244430','8974013388','9156860129','9850013134','8985876040','8308308132','9627787749','7208168723','9049750007','9844074621','9990650301','9716979737','8882323796','8693070809','9373915439','9762152215','9811817136','9822338892','8879073792','8510861982','8341150244','8499997744','9890121650','8454051997','9029200902','7804955603','8130259431','9063219711','9035248464','9618287737','8409031503','7204343525','9555564415','9850676933','9912432465','9595563107','9028864634','9028779991','9741233416','9845182481','8501906906','8801447999','9494231412','9848777470','8185938889','7097930363','9985248397','9968207564','9930476460','9989256303','7065566359','7049260854','7049261037','7702758190','9867176514','9640954130','9370936337','9718439369','7087131500','9870057570','9730919494','9716747319','8308789000','9586977832','9030906890','9505550323','9989403801','9765742140','9920303441','9927256330','8380898800','9036752878','9421311951','9958786687','8882816363','9990473265','9910811878','9831194345','8928148179','9250325450','9229666674','9663751511','9623861882','8446915049','9920172016','8080147009','9823601022','7030161743','7389536799','7503859897','8826487147','7078428367','8626094500','9866832536','9920607078','7741090206','7860728260','9491368375','7042510855','8971965888','8827268299','7206573000','9971853879','9225662972','9849964696','8510034354','7219193245','9422874120','8886859510','9766252801','9850452951','9987778700','9971037654','9599839595','9623923523','9300425871','9849037829','9540202091','8237690638','8019330447','8285116070','9029868339','9930474108','9010690846','9454047177','8050433323','9619857838','9013301847','9849367320','8019769445','9910628900','9014720515','8185080806','9503247206','9823802255','8879313020','8885665757','7354647286','9665588666','8527526812','8796660546','7702523143','7416147934','9657304060','8806769606','9540444777','8454938909','9175517504','9765355545','8898946708','9713109191','8468012789','8237373537','7012517698','8889324124','8379071931','9910327664','9975852867','9545883474','9819540002','8855014692','9711691401','9156866242','7720839191','8801858716','8390607094','8588069755','9641637081','9866157735','9818894275','9899549973','8882688856','7666888598','9492732331','9584175404','9979226012','9890721110','9810734754','9891476069','9584424140','9860899839','9700978585','9036386553','9768618483','8888368667','7045050401','9885393115','7744065353','9665977861','9850413446','7878469462','9718895430','9724582622','8881880118','8120080883','8221930940','8898370067','8898775173','9650363798','8686225108','9700049461','9822028203','9254918182','9822681907','8977262625','8291044729','9224290517','9891122117','9441325522','9555222889','7840066833','9893259140','9813112923','7017323201','9716358144','9819685336','9922151298','8019508419','8898014071','9253130361','8802552336','9911003966','8586037140','9972050544','7503574309','9909075176','9739951863','9028513470','7041142988','7738170391','9177743337','9850278484','9699169912','8291016663','7378929184','7416300618','8796346669','9768469119','9867801066','8511885533','9821776634','7053963893','9892526233','8806778004','9742008849','9765479899','9898292998','9716093541','9579464791','9960782108','8796693231','7448288691','8802549114','9818285308','9986115467','9555639799','7045246033','8688665978','9666954540','8687398409','9036303420','9014430133','9066336116','9246282829','7206165317','8862007775','8433864699','8423542541','8553735123','7208211836','9891993258','8290810093','9404239423','9555205005','8285076177','9454999250','7204688490','9703383848','9422807095','8604981721','7045148346','8128821274','8120243062','9910500643','9898620594','9716093446','8380856736','7013431677','9985660900','7032081237','9158423529','9558907192','9029856802','7070486467','8976685274','8460471659','9161038085','7697209820','8008182435','9553775175','7385077007','9768391108','9542233200','8652787924','8008996083','9892740425','9010608072','9848093833','9951731304','9527132014','9920465543','9652225272','9910724858','7899773637','9700668496','9100356033','9029745816','9533071164','8085618289','7075827972','9505750611','9770561620','9642236665','9990999601','9866356851','9963559393','8686308208','8983440019','8802222685','8099468310','8605050707','9849810195','9989753095','7793930898','9396427500','7210586033','9405941497','9632533376','9891077704','7219005630','8268227777','8729076528','9811725220','9902523296','9871819713','9998850979','9765220120','9999816625','9818789635','9898684750','9958871475','9494851288','9990356802','9555122298','9810830774','8802203649','9255252777','8108911094','9096579397','8425025042','8177883317','9767027777','9855766056','9821804658','8080906484','9021947242','7353972515','7379047362','8686908585','9713471929','7249580742','8007793332','8447620580','7291066186','7489008008','8802807506','8951036669','9019998888','9716190422','9028929950','9880282720','7509095771','7533034398','9892453412','7898487036','7693866720','9004955944','7838377915','8828054123','9820762128','9632135460','9623499947','9871345885','7303833335','9892790992','9930500037','9902248981','9594242425','9876980440','9819131473','8959309143','9911899882','9914299000','9222211922','7846053179','9975037786','9953820220','9989944787','9028149901','9981726015','7798610631','7888217925','9818209866','9063139999','9881546434','8424948486','9729813930','7737402466','7353003000','9175123542','7835981985','9493443905','8860013039','9902843027','7760538358','8800668831','7026255700','9806546508','8859281494','9820214641','9814433041','9356086541','9821376915','9948127698','9899095936','9074802911','9888153376','8699763701','9136592748','9911088005','9028240214','7709774087','7666641665','8793424973','9739562658','9764197797','9700604050','9964044845','9873704489','7503601892','9888763286','9540971774','9172729912','7340781113','7697134905','9599866694','9893333459','9873774318','9880380346','9770073430','7276122621','7799093577','9404995396','9957020488','9785079145','9811500255','9347240758','9989671971','9921603660','8587847106','9999192710','9899780863','9210851025','9538100820','9716237812','8050367491','9001536640','8147568045','8955508143','7718001016','7748013379','9702180777','9819741940','9920479309','9272539153','9136344110','9822716762','9711449004','8652541872','9403486033','9752161691','8890071003','9011759736','9637411853','7767980697','9654043347','9866514563','8802644402','9595959647','9844803392','9493233039','9226917315','9156115697','8802957246','8750298388','8898109076','9205715068','9742110965','7387597338','9006762387','9850207514','9999042564','7838255347','9704226215','8744040706','9899123986','9700055664','7738787000','9980516061','8379063601','9925700141','7666262627','8947028510','9542942311','9870729134','9769561358','9310909978','9479959188','9705496063','7532854026','9764232506','9727117998','7416410889','9039070001','9584445730','9175157007','9833087923','9914443247','9540626257','9920457936','8130457893','9819162795','8793788879','9624673041','9773861186','9892154999','8879428042','9654325454','9850917119','9892556151','8108971258','9981303213','9226806676','9716391182','9987884029','7208252550','8437654097','9773666676','9617636483','9930071676','9922277336','7053547984','9050654557','7021175859','7400820105','9844365806','8652018977','9490366775','9619450209','9393761000','9013684152','9892883477','9004630782','8698304278','9594926843','7738714502','9076218412','9144759723','7666115152','8806476796','9036343408','7045855405','9421475610','7869961345','7389726320','9820218180','8750114408','9782879801','9872806303','9650595690','8692008555','7588709542','9665716374','9773677747','9738772770','7899822798','7278131118','7039148407','8796300392','9611003567','7013802247','9989673231','7066157772','9638174220','9000228802','9910600431','9241444491','8108905355','7666676960','9014881122','8510088612','8882883777','9820378765','8897294003','9075427474','9967476415','9902414359','7738813736','8237064603','9022165138','8871842768','7387282716','9008999548','8968978023','9871051465','9716355435','9271718478','9827913661','9108770055','9033900089','9619030004','8421441933','9819335369','9569818339','9619790022','8826163999','9167050659','9753254414','7697762547','9174450818','8867484923','7827620098','8487855706','8446439681','8130545335','9891541044','8974172602','9619080299','9561345007','9569950722','9866366456','9970045856','9948123492','8460668200','7329071726','9666416669','7666320143','8010437659','9164364749','7288854520','9898393108','7760519192','9885692400','9293786787','9768122329','9059269626','9999916174','9702467577','9768590756','8097381020','7021082662','9848809882','9705823716','9004088902','9999096934','9329553953','8080780894','9004726957','9904470363','9923410931','9819160115','9953959697','9958716475','8683809009','7307786666','9702440472','7307107067','8459127478','9811199194','9573173727','9702660238','8517891018','9630698793','9810796461','9560763293','7666678700','9886889474','9565710841','9921731491','7303637195','9765819459','9716734354','9177671797','7755989983','8855815566','9273848482','8796292846','8237678712','7730018592','8268546091','9818356935','9848760638','9729785771','9848327821','9111460415','8888715085','8100492235','9019227981','7718929991','9958328310','7710962566','7307076916','7470318844','8879616110','9030089143','8980565093','9910127902','9766978663','9999397730','9892731362','9987298261','8879850067','9642999880','9422204253','8554839441','9953152224','9899556454','9769890299','7836917952','8341993992','9893327765','7675033005','9652232129','8424859360','9250607791','9819859614','9993680895','8143234887','9915427876','7869700102','9826456989','9702440686','9096649067','8149513173','8686849394','8978996669','9075642400','9310294500','9368735104','9987705243','8885188512','9096875747','7891434747','8510874072','8108289801','9925449496','9060419143','8889317625','7741091041','9848004457','8882157948','9310105590','8750166500','7503931069','9910117271','9890954403','7843033058','9891181070','9827682758','9717440125','9763236140','9015223522','9990109929','8806713434','9718688687','9741490005','7353525351','9615234122','7798111360','8726160457','9819009458','8424803450','7021142147','9811796914','9769337470','8801590427','9027323820','9029614921','9673136031','9848108028','7428324088','9716999176','9860800854','8054181198','9666358933','8096680400','9988440103','9250572828','9019421729','9822225065','9811940322','9028866151','9211644191','8727960751','8008499179','9555516669','7696600107','9989497315','8000004130','9059522337','9216755551','9034469803','8197981573','9250868686','9752325463','7533078597','8412953954','9615401695','9963228871','7042008467','9890682434','9743189302','9022535310','9569985197','9901804013','8983152391','9901764672','9322029800','8285591791','9967655687','9535418222','7568022533','9810801368','7755939495','8010770316','9945261346','8286592050','9878449993','7038680167','8080999889','8465964126','9015882951','9711889816','9637873869','9910308669','7532064133','9999666579','7838814306','8140091181','9541664666','9878375003','9177738744','9819260858','9701888653','8285094579','9700728711','7210869945','9985610283','8971814037','9646920690','9767699851','8380926638','7387474750','9819018439','8000674700','8983773638','8087666636','9879142205','9449357568','9813660921','7503529707','9440472106','9765932074','9850389457','9268970907','9762157595','9503003803','9999863535','9811188131','9822913916','9740378893','7724901377','9482711123','9898809705','9888862660','9139313466','9449494491','8527318140','8390132621','9738452632','9822036365','9793744893','9579609295','9425325102','8108910678','7042847174','8882331223','9763700774','8121332202','7022677944','9175215637','9833008028','9822723056','8268104969','9891406013','8285551613','9999447878','9096799977','7666322565','7045643503','7057201617','9985028591','9098009909','7835984469','9717907416','9845408125','9206357662','9425153301','8130500455','8471020252','9210003100','9407551785','7208247917','8459384233','9096587490','8085399499','9972499282','9533951954','9998573439','7503482042','9404489917','9945554616','9870662588','7659929402','8446332498','8793350820','8686536375','9741653195','7875777397','8983681442','9825195492','7738694117','9823333314','9885247800','8882830820','9769721598','9623496018','9250239293','9769694558','9023147471','9826591475','7532843588','8109910118','9039800090','9871785869'";
        	 
        	$ret_arr = explode(",",$retailers);
        	$arrays = array_chunk($ret_arr, 500);
        	 
        	$date_array = array();
        	foreach($arrays as $arr){
        		$rets = implode(",",$arr);
        		$query = "SELECT sum(sale) as total,retailers.mobile,retailer_id FROM retailers_logs LEFT JOIN retailers ON (retailers.id = retailer_id) WHERE retailers.mobile in ($rets) AND date >= '2016-08-18' and date <= '2016-08-25' group by retailer_id order by total asc";
        		$datas = $this->Slaves->query($query);
        		foreach($datas as $data){
        			$retailer_mob = $data['retailers']['mobile'];
        			$retailer_id = $data['retailers_logs']['retailer_id'];
        			$sale = $data['0']['total'];
        			$money_to_be_given = intval($sale/100);
        			echo "Mobile: $retailer_mob, Sale: $sale, Money: $money_to_be_given<br/>";
        			if($money_to_be_given > 0) {
        				$trans_id = $this->Shop->shopTransactionUpdate(REFUND, $money_to_be_given, $retailer_id, RETAILER);
        				$bal = $this->Shop->shopBalanceUpdate($money_to_be_given, 'add', $retailer_id, RETAILER);
        				$this->Shop->addOpeningClosing($retailer_id, RETAILER, $trans_id, $bal - $money_to_be_given, $bal);
        				$sms = "Congratulations!!\nYou have earned an incentive of Rs$money_to_be_given in Pay1 par zyada ka vada scheme\n";
        				$sms .= "Aapki 18th Aug se 25th Aug ki total sale hai: $sale\n";
        				$sms .= "Pay1 se jude rahe aur exciting offers ka laabh uthate rahe.\n";
        				$sms .= "\nHappy Recharging with Pay1";
        				
        				$this->General->sendMessage($retailer_mob,$sms,'shop');
        				$this->General->sendMessage("9819032643",$sms,'shop');
        				exit;
        			}
        			//
        		}
        
        
        	}
        	
        }
		
		function floatAlert(){
			
			$this->autoRender =false;
			
			$query = "SELECT * FROM `float_logs`  order by id desc limit 0,2";
			
			$fetchdata=$this->Slaves->query($query);
			
			if(!empty($fetchdata) && count($fetchdata)>1){
			
			$closing = $fetchdata[0]['float_logs']['float_without_b2c']-$fetchdata[1]['float_logs']['float_without_b2c'];
			
			$sale = $fetchdata[0]['float_logs']['sale']-$fetchdata[1]['float_logs']['sale'];
			
			$transfer = $fetchdata[0]['float_logs']['transferred']-$fetchdata[1]['float_logs']['transferred'];
			
			$commision = $fetchdata[0]['float_logs']['commissions']-$fetchdata[1]['float_logs']['commissions'];
			
			$reversal = $fetchdata[0]['float_logs']['old_reversals']-$fetchdata[1]['float_logs']['old_reversals'];
			
			$service_charge = $fetchdata[0]['float_logs']['service_charge']-$fetchdata[1]['float_logs']['service_charge'];
			
			$diff = $closing -($commision+$reversal+$transfer-$sale-$service_charge);
			
			if(!empty($diff) && $diff<0){
				
				$this->General->sendMessage('9967331335,9819032643', 'Float balance is gone negative of  '.$diff.' between '.($fetchdata[1]['float_logs']['time']).' and ' . ($fetchdata[0]['float_logs']['time']), 'payone');  	
			}
			
			}
			
		}
    }
