<?php
class ShopsController extends AppController {
	var $name = 'Shops';
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator','GChart','Csv');
	var $components = array('RequestHandler','Shop');
	var $uses = array('SalesmanTransaction','Salesman','Retailer','Distributor','SuperDistributor','ModemRequestLog','User','ShopTransaction','Rm','Slaves');

	function beforeFilter() {
		set_time_limit(0);
		ini_set("memory_limit", "-1");
		parent::beforeFilter();
        error_reporting(0);
		$this->Auth->allow('*');
		
        //$this->Auth->allowedActions = array('retFilter','allDistributor','addInvestedAmount','investmentReport','earningReport','kitsTransfer','transferKits','backKitsTransfer','graphRetailer','pullback','addSalesmanCollection','salesmanTran','backSetup','addSetUpFee','formSetUpFee','backSalesman','createSalesman','formSalesman','saleReport','approve','lastTransactions','createCommissionTemplate','createRetailerApp','cronUpdateBalanceLFRS','printCreditDebitNote','backCreditDebit','createNote','getCreditDebitNotes','createCreditDebitNotes','retailerProdActivation','products','index','initializeOpeningBalance','generateInvoice','logout','test','setSignature','saveSignature','topupReceipts','printRequest','script','issue','backReceipt','issueReceipt','printReceipt','printInvoice','retailerListing','PNRListing');
        
        if($this->Session->read('Auth.User.passflag') == 0 && $this->Session->check('Auth.User') &&  $this->here != "/shops/changePassword" && $this->here != "/" && $this->here != "/shops/logout"){
           $this->redirect(array('controller' => 'shops', 'action' => 'changePassword'));
        }
		$states = $this->Slaves->query("SELECT id,name FROM locator_state WHERE toshow = 1 ORDER BY name asc");
		$cities = $this->Slaves->query("SELECT id,name FROM locator_city WHERE state_id = ". $states['0']['locator_state']['id']." AND toshow = 1 ORDER BY name asc");
        $bankDetails = $this->Shop->getBanks();
        $this->set('bankDetails',$bankDetails);
        
		$this->set('objShop',$this->Shop);
		if($this->Session->check('Auth.User')){
			$info = $this->Shop->getShopDataById($this->Session->read('Auth.id'),$this->Session->read('Auth.User.group_id'));
            
			//$info = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
			$this->info = $info;
			$this->set('info',$info);
		}
		if($this->Session->read('Auth.User.group_id') == ADMIN){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->SuperDistributor->recursive = -1;
			$super_distributors = $this->SuperDistributor->find('all', array(
			'fields' => array('SuperDistributor.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('SuperDistributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('SuperDistributor.user_id = users.id')
			),
			array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('SuperDistributor.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 0', 'shop_transactions.confirm_flag != 1')
			)

			),
			'order' => 'SuperDistributor.company asc',
			'group'	=> 'SuperDistributor.id'
			)
			);

			$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE active_flag = 1 AND group_id = " . RETAILER);

			$this->set('slabs',$slabs);
			$this->set('super_distributors',$super_distributors);
			$this->set('records',$super_distributors);
			$this->set('modelName','SuperDistributor');
			$this->sds = $super_distributors;
		}
		else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
					'fields' => array('Distributor.*', 'slabs.name', 'users.mobile', 'rm.name'),
					'conditions' => array('Distributor.parent_id' => $this->info['id'], 'Distributor.toshow' => 1),
					'joins' => array(
							array(
									'table' => 'slabs',
									'type' => 'inner',
									'conditions' => array('Distributor.slab_id = slabs.id')
							),
							array(
									'table' => 'users',
									'type' => 'inner',
									'conditions' => array('Distributor.user_id = users.id')
							),
							array(
									'table' => 'rm',
									'type' => 'left',
									'conditions' => array('Distributor.rm_id = rm.id')
							)
					),
					'order' => 'Distributor.active_flag desc,Distributor.company asc',
					'group' => 'Distributor.id'
					)
			);
			$distarray = array();
			$query = $this->Slaves->query("SELECT  sum(`shop_transactions`.`amount`) as xfer,ref2_id as distId FROM `shop_transactions` where  `shop_transactions`.`date` = '".date('Y-m-d')."' AND `shop_transactions`.`type` = 1 AND `shop_transactions`.`confirm_flag` != 1 and shop_transactions.ref1_id = ".$this->info['id']."  group by distId");
			foreach ($query as $key){
				$distarray[$key['shop_transactions']['distId']] = $key[0]['xfer'];				
			}
			$record =  array();
			foreach ($distributors as $dis) {
				$record[$dis['Distributor']['id']] = $dis;
				if(!empty($distarray[$dis['Distributor']['id']])){
				$record[$dis['Distributor']['id']][]['xfer'] = $distarray[$dis['Distributor']['id']];
				} else {
					$record[$dis['Distributor']['id']][]['xfer'] = 0;
				}
			}
			$distRecords = $record;
			/*$record  = $this->General->array_sort_by_column($record,0);
			$distRecords =  array();
			foreach($record as $val){
				$distRecords[$val[0]['Distributor']['id']] = $val[0]; 
			}*/
			
			if($this->info['id'] == 3){
				$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE active_flag = 1 AND group_id = " . RETAILER);
			}
			else {
				$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
			}
			$this->set('slabs',$slabs);
			$this->set('distributors',$distRecords);
			$this->set('records',$distRecords);
			$this->set('modelName','Distributor');
			$this->ds = $distRecords;
		}
		else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$retailers = $this->General->getRetailerList($this->info['id']);
                        if($this->info['id'] == 1){
				$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE active_flag = 1 AND group_id = " . RETAILER);
			}
			else {
                $slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
			}
			
			
			$this->set('slabs',$slabs);
			$this->set('retailers',$retailers);
			$this->set('records',$retailers);
			$this->set('modelName','Retailer');

			$areas = $this->Slaves->query("SELECT id,name FROM locator_area WHERE city_id = ". $cities['0']['locator_city']['id']." AND toshow = 1 ORDER BY name asc");
			$this->set('areas',$areas);
			$this->retailers = $retailers;
		}else if($this->Session->read('Auth.User.group_id') == RELATIONSHIP_MANAGER){
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile','rm.name', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Distributor.rm_id' => $this->info['id'],'Distributor.toshow' => 1,'Distributor.active_flag'=>1),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
			),
			array(
								'table' => 'rm',
								'type' => 'left',
								'conditions'=> array('Distributor.rm_id = rm.id')
			),
			array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Distributor.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 1')
			)

			),
			'order' => 'Distributor.company asc',
			'group'	=> 'Distributor.id'
			)
			);
            
			/*if($this->info['id'] == 3){
				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);
				}
				else {

				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
				}*/
			//$this->set('slabs',$slabs);
			$this->set('distributors',$distributors);// this var is used in retailers_list view
			$this->set('records',$distributors);
			$this->set('modelName','Distributor');
			$this->ds = $distributors;;// this var is used in sale reports view
		} else {
			
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
					'fields' => array('Distributor.*', 'slabs.name', 'users.mobile', 'rm.name'),
					'conditions' => array('Distributor.parent_id' => '3', 'Distributor.toshow' => 1),
					'joins' => array(
							array(
									'table' => 'slabs',
									'type' => 'inner',
									'conditions' => array('Distributor.slab_id = slabs.id')
							),
							array(
									'table' => 'users',
									'type' => 'inner',
									'conditions' => array('Distributor.user_id = users.id')
							),
							array(
									'table' => 'rm',
									'type' => 'left',
									'conditions' => array('Distributor.rm_id = rm.id')
							)
					),
					'order' => 'Distributor.active_flag desc,Distributor.company asc',
					'group' => 'Distributor.id'
					)
			);
			$distarray = array();
			$query = $this->Slaves->query("SELECT  sum(`shop_transactions`.`amount`) as xfer,ref2_id as distId FROM `shop_transactions` where  `shop_transactions`.`date` = '".date('Y-m-d')."' AND `shop_transactions`.`type` = 1 AND `shop_transactions`.`confirm_flag` != 1 and shop_transactions.ref1_id = '3'  group by distId");
			foreach ($query as $key){
				$distarray[$key['shop_transactions']['distId']] = $key[0]['xfer'];				
			}
			$record =  array();
			foreach ($distributors as $dis) {
				$record[$dis['Distributor']['id']] = $dis;
				if(!empty($distarray[$dis['Distributor']['id']])){
				$record[$dis['Distributor']['id']][]['xfer'] = $distarray[$dis['Distributor']['id']];
				} else {
					$record[$dis['Distributor']['id']][]['xfer'] = 0;
				}
			}
			$distRecords = $record;
			/*$record  = $this->General->array_sort_by_column($record,0);
			$distRecords =  array();
			foreach($record as $val){
				$distRecords[$val[0]['Distributor']['id']] = $val[0]; 
			}*/
			
			//if($this->info['id'] == 3){
				$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE active_flag = 1 AND group_id = " . RETAILER);
			//}
			//else {
				$slabs = $this->Slaves->query("SELECT * FROM slabs as Slab WHERE Slab.id = '3'");
			//}
			$this->set('slabs',$slabs);
			$this->set('distributors',$distRecords);
			$this->set('records',$distRecords);
			$this->set('modelName','Distributor');
			$this->ds = $distRecords;
		}
				
		$this->set('states',$states);
		$this->set('cities',$cities);
		$this->Auth->loginAction = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->logoutRedirect = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->loginRedirect = array('controller' => 'shops', 'action' => 'view');
	}
	
	function index(){
		if(SITE_NAME == "http://panel.activestores.in/" || SITE_NAME == "http://54.235.195.140/" || SITE_NAME == "http://54.235.193.96/")$this->redirect('https://panel.pay1.in');
		
		if($this->Session->check('Auth.User')){
			$this->redirect(array('action' => 'view'));
			/*if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER || $_SESSION['Auth']['User']['group_id'] == ACCOUNTS || $_SESSION['Auth']['User']['group_id'] == '20'){
				$this->redirect(array('action' => 'view'));
			}
			else if($_SESSION['Auth']['User']['group_id'] == CUSTCARE){
				$this->redirect(array('controller' => 'panels','action' => 'index'));
			}*/
		}
		else if($_SERVER['SERVER_NAME'] == 'apis.signal7.in'){
			echo "Work going on";
			$this->autoRender = false;
		}
		else {
			$this->render('index');
		}
	}
	
	function view(){
		 ///echo "hello1"; die;
		if(!isset($_SESSION['Auth']['User'])){
		
		  $this->redirect('/');
		}
		else if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR)
		$this->render('transfer');
		else if($_SESSION['Auth']['User']['group_id'] == RETAILER){
			$this->redirect('http://shop.pay1.in');
		}
		else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
			$this->redirect(array('controller' => 'shops','action' => 'allDistributor'));
		}
		else if($_SESSION['Auth']['User']['group_id'] == 9){
			$data = $this->Slaves->query("SELECT id FROM vendors WHERE user_id = ".$_SESSION['Auth']['User']['id']);
			if(!empty($data)){
				$this->redirect('/sims/index');	
			}
			else {
				$this->redirect('/sims/index');	
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == '18'){
			$this->redirect(array('controller' => 'shops','action' => 'limitTransfer'));
		}
		else{
			
			$this->redirect(array('controller' => 'panels','action' => 'view'));
		}
	}

	function autoCompleteSubarea(){
		$name = $this->data['Salesmen']['subarea'];

		$this->Shop->recursive = -1;
		$data = $this->Slaves->query("SELECT * from subarea where name like '".$name."%'");

		$this->set('subarea',$data);
		$this->render('auto_complete_subarea');
	}




	function formDistributor(){
		$this->render('form_distributor');
	}
        
	function backDistributor(){          
		$this->set('data',$this->data);
		$this->render('/elements/shop_form_distributor');
	}

	function backDistEdit($type){
		/*echo "<pre>";
		 print_r($this->data);
		 echo "</pre>";**/
		if($type == 'r'){
			if($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR)$this->redirect('/');

			$modName = 'Retailer';
			$cities = $this->Slaves->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." AND toshow = 1 ORDER BY name asc");
			$this->set('cities',$cities);
			$areas = $this->Slaves->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." AND toshow = 1 ORDER BY name asc");
			$this->set('areas',$areas);

			$stateName = $this->Slaves->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Slaves->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);
			$areaName = $this->Slaves->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);

			$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
			$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
			$this->data['Retailer']['area'] = $areaName['0']['locator_area']['name'];

			$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
			$this->set('sMen',$sMen);

			//$fees = $this->Retailer->query("SELECT sum(shop_transactions.amount) as fee FROM shop_transactions join salesman_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$this->data['Retailer']['id']." group by salesman_transactions.payment_type");
			//$this->set('fees',$fees);
		}
		if($type == 'd'){
			if($_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)$this->redirect('/');

			$modName = 'Distributor';
//			$cities = $this->Slaves->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." AND toshow = 1 ORDER BY name asc");
//			$this->set('cities',$cities);
//			$stateName = $this->Slaves->query("SELECT name FROM locator_state WHERE id = ". $this->data['Distributor']['state']);
//			$cityName = $this->Slaves->query("SELECT name FROM locator_city WHERE id = ". $this->data['Distributor']['city']);
//
//			$this->data['Distributor']['state'] = $stateName['0']['locator_state']['name'];
//			$this->data['Distributor']['city'] = $cityName['0']['locator_city']['name'];;
		}
		$tmparr[0] = $this->data;
		$this->set('editData',$tmparr);
		$this->set('type',$type);
		$this->render('/elements/edit_form_ele_retailer');
	}


	function createDistributor(){

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
                $MsgTemplate = $this->General->LoadApiBalance();
		if(isset($this->data['Distributor']['tds_flag']) && $this->data['Distributor']['tds_flag'] == 'on')
		$this->data['Distributor']['tds_flag'] = 1;
		else
		$this->data['Distributor']['tds_flag'] = 0;
			
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
                if(empty($this->data['Distributor']['map_lat'])){
			$empty[] = 'Latitude';
			$empty_flag = true;
			$to_save = false;
		}
                if(empty($this->data['Distributor']['map_long'])){
			$empty[] = 'Longitute';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Distributor']['mobile'] = trim($this->data['Distributor']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Distributor']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
                if(empty($this->data['Distributor']['dob'])){
			$empty[] = 'DOB';
			$empty_flag = true;
			$to_save = false;
		}else{
                    $date = explode("-", $this->data['Distributor']['dob']);
                    $this->data['Distributor']['dob'] = $date[2] . "-" . $date[1] . "-" . $date[0];
                }
		if(empty($this->data['Distributor']['state'])){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['city'])){
                    	$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['area_range'])){
                    	$empty[] = 'Area Range'; 
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
                    	$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}
                
		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Distributor']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Distributor']['mobile']);
				if($user['group_id'] != MEMBER){
					$to_save = false;
					$msg = "You cannot make this mobile as your distributor";
				}
			}
		}
                
                App::import('Controller', 'Apis');
                $obj = new ApisController;
                $obj->constructClasses();
                    
                if(!isset($this->data['Distributor']['otp']) && !$empty_flag){
                
                    $sendOTPdata['mobile'] = $this->Session->read('Auth.User.mobile'); 
                    $sendOTPdata['interest'] = 'Distributor';
                    $sendOTPdata['create_dist_otp_flag'] = 1;

                    $otpData = $obj->sendOTPToRetDistLeads($sendOTPdata);
                    
                }    

		if(!$to_save){
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_distributor','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.' </div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
                    $stateId = $this->General->stateInsert($this->data['Distributor']['state']);
                    $cityId = $this->General->cityInsert($this->data['Distributor']['city'] , $stateId);
			$slab = $this->Slaves->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);

			$this->set('slab',$slab);
                        $this->set('dob',$this->data['Distributor']['dob']);
			$this->set('city',$this->data['Distributor']['city']);
			$this->set('state',$this->data['Distributor']['state']);
			$this->render('confirm_distributor','ajax');
		}
		else {
                    
                    //To verify OTP sent to Super Distributor Mobile Number
                    $verify_param['mobile'] = $this->Session->read('Auth.User.mobile');
                    $verify_param['otp'] =   $this->data['Distributor']['otp']; 
                    $verify_param['interest'] =   'Distributor';
                        
                    $verifyData =  $obj->verifyOTP($verify_param);

                    if($verifyData['status'] =='failure'){

                        $err_msg = '<div class="error_class">Please enter correct OTP. </div>';
                        $this->Session->setFlash(__($err_msg, true));
                        $this->render('confirm_distributor','ajax'); 
                        return;
                    }

                    
                    
			$this->data['Distributor']['created'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['modified'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['balance'] = 0;
                        
			if(!$exists){
				$user = $this->General->registerUser($this->data['Distributor']['mobile'],RETAILER_REG,DISTRIBUTOR);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = DISTRIBUTOR;
				$this->User->save($userData);//make already user to a distributor
			}
			$this->data['Distributor']['user_id'] = $user['id'];
			$this->data['Distributor']['parent_id'] = $this->info['id'];
			$this->data['Distributor']['target_amount'] = 25000;
                        
			$this->Distributor->create();
			if ($this->Distributor->save($this->data)) {
				$mail_subject = "New Distributor Created";
				$this->General->makeOptIn247SMS($user['mobile']);

				$mail_body = "SuperDistributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Distributor: " . $this->data['Distributor']['company'];
				$mail_body .= "Address: " . $this->data['Distributor']['address'];
				$this->General->sendMails($mail_subject, $mail_body,array('dharmesh@mindsarray.com','sales@mindsarray.com',
						'tadka@mindsarray.com','nivedita@mindsarray.com','irfan@pay1.in', 'accounts@mindsarray.com', 'limits@mindsarray.com'),'mail');
				$this->Shop->updateSlab($this->data['Distributor']['slab_id'],$this->Distributor->id,DISTRIBUTOR);

				$distributor = $this->data;
                                
				/*$ret_user = $this->General->registerUser('1'.$distributor['Distributor']['mobile'],RETAILER_REG,RETAILER);
				 $ret_user = $ret_user['User'];

				 $this->data = null;
				 $this->data['Retailer']['user_id'] = $ret_user['id'];
				 $this->data['Retailer']['mobile'] = $distributor['Distributor']['mobile'];
				 $this->data['Retailer']['parent_id'] = $this->Distributor->id;
				 $this->data['Retailer']['toshow'] = 1;
				 $this->data['Retailer']['email'] = $distributor['Distributor']['email'];
				 $this->data['Retailer']['name'] = $distributor['Distributor']['name'];
				 $this->data['Retailer']['slab_id'] = RET_SLAB;
				 $this->data['Retailer']['shopname'] = $distributor['Distributor']['company'];
				 $this->data['Retailer']['address'] = $distributor['Distributor']['address'];
				 $this->data['Retailer']['created'] = date('Y-m-d H:i:s');
				 $this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
				 $this->Retailer->create();
				 $this->Retailer->save($this->data);
				 $this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
				 */
				//if($this->data['login'] == 'on'){
//				$sms = "Congrats!!\n
//						You have become Distributor of Pay1. Your login details are below\n
//						Online Url: http://panel.pay1.in\n
//						UserName: ".$distributor['Distributor']['mobile']."\n
//						Password: ".$user['syspass']."\n
//						Check out the Pay1 app for our Channel Partners at https://goo.gl/yuTaeB";
                                
                                $paramdata['DISTRIBUTOR_MOBILE_NUMBER'] = $distributor['Distributor']['mobile'];
                                $paramdata['USER_SYSPASS'] = $user['syspass']; 
                                $content =  $MsgTemplate['CreateDistributor_MSG'];
                                $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                                
                                
				//$sms .= "\nFind below url to download distributor app: " . $this->General->createAppDownloadUrl(DISTRIBUTOR,1);
				//$sms .= "\n\nDefault Retailer credentials for you are below: \nUserName: 1".$distributor['Distributor']['mobile']."\nPassword: ".$ret_user['syspass'];
				//$sms .= "\nFind below url to download retailer app: " . $this->General->createAppDownloadUrl(RETAILER,1);
					
				$this->General->sendMessage($user['mobile'],$sms,'shops');
				//}

				//create default salesman with distributors phone number
				if(!empty($this->data['Distributor']['name'])){
					$sales = $distributor['Distributor']['name'];
				}
				else {
					$sales = "Default";
				}
				
				$salesmanData = $this->Slaves->query("SELECT * FROM salesmen WHERE mobile = '".$distributor['Distributor']['mobile']."'");
				if(empty($salesmanData)){
					$salesman = array(
							'dist_id' 	=> 	$this->Distributor->id,
							'name'		=>	addslashes($sales),
							'mobile'	=>	$distributor['Distributor']['mobile'],
							'tran_limit'=>	'1000000',
							'balance'	=>	'1000000'
					);
					$this->insertSalesman($salesman);
				}
				else {
					$this->Retailer->query("UPDATE salesmen SET dist_id = ".$this->Distributor->id .",tran_limit=1000000,balance=1000000 WHERE mobile = '".$distributor['Distributor']['mobile']."'");	
				}
				$this->set('data',null);
				$this->render('/elements/shop_form_distributor','ajax');
			} else {
				$err_msg = '<div class="error_class">The Distributor could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}
		}
	}

	/*function allotCards(){
		$this->render('allotcards');
	}

	function backAllotment(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_allot_cards');
	}*/


	function allDistributor(){
		
		if($this->Session->read('Auth.User.group_id') == ADMIN){
			$data=$this->Slaves->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_SUPERDISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
			//}else if($this->Session->read('Auth.User.group_id') == RELATIONSHIP_MANAGER ){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));

			//      $data=$this->Retailer->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_DISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
		}else {
			$data=$this->Slaves->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_DISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
		}
		$datas = array();
		foreach($data as $dt){
			$datas[$dt['st']['ref1_id']] = $dt['0']['amts'];
		}
		$this->set('datas',$datas);
		$this->render('alldistributor');
	}

	function retFilter(){

		$filter = $_REQUEST['filter'];
		$id = $_REQUEST['id'];
		$query = "";
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			$dist = $this->info['id'];
		}
		else {
			$dist = $id;
		}
			
		if($filter == 1){//top transacting last 7 days
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) > 1000)");
		}
		else if($filter == 2){//avg transacting last 7 days
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) <= 1000 AND avg(sale) > 500)");
		}
		else if($filter == 3){//low transacting last 7 days
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) <= 500)");
		}
		else if($filter == 4){//dropped in last 2 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(vendors_activations.date) = '".date('Y-m-d',strtotime('-2 days'))."')");
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(retailers_logs.date) = '".date('Y-m-d',strtotime('-2 days'))."')");
		}
		else if($filter == 5){//dropped in last 7 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-7 days'))."' AND max(vendors_activations.date) <= '".date('Y-m-d',strtotime('-2 days'))."')");
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-7 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-7 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-2 days'))."')");
		}
		else if($filter == 6){//dropped between last 7-14 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-14 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-14 days'))."' AND max(vendors_activations.date) < '".date('Y-m-d',strtotime('-7 days'))."')");
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-14 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-14 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-7 days'))."')");
		}
		else if($filter == 7){//dropped between last 14-30 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-30 days'))."' AND max(vendors_activations.date) < '".date('Y-m-d',strtotime('-14 days'))."')");
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-30 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-14 days'))."')");
		}
		else if($filter == 8){//dropped before 30 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(vendors_activations.date) < '".date('Y-m-d',strtotime('-30 days'))."')");
			$ids = $this->Slaves->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-120 days'))."' group by retailer_id having (max(retailers_logs.date) < '".date('Y-m-d',strtotime('-30 days'))."')");
		}

		$idrs = array();
		foreach($ids as $idr){
			$idrs[] = $idr['retailers']['id'];
		}

		echo implode(",",$idrs);
		$this->autoRender = false;
	}

	function allRetailer($id = null,$retId = null){
		ini_set("memory_limit","-1");
		$query = "";
		$this->set('retailer_type','non-deleted');
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			if($retId == null) $retId = 0;
			else if($retId!=null) $query.= " AND retailers.id = $retId";
			$dist = $this->info['id'];
			$salesmen=$this->Slaves->query("select name,id,mobile from salesmen WHERE dist_id = $dist AND active_flag = 1 order by id");
			$this->set('salesmen',$salesmen);
			$this->set('sid',$id);
			$this->set('retId', $retId);
			$sid = $id;
		}else {
			$this->set('dist',$id);
			if($id != null){
				//$retailers = $this->General->getRetailerList($id,null,true);
				$distributors = $this->viewVars['distributors'];// already fetched in before controller so used used thos record
				//$this->printArray($distributors);
				$this->set('modelName','Retailer');
				//$this->set('records',$retailers);
				$distributor=array();
				foreach ($distributors as $d){//check given id is in rm's distribtors list
					if($d['Distributor']['id'] == $id ){
						$distributor = $d;
					}
				}
				if(empty($distributor)){// if rm is not belongs to the distribtors( given id )
					$this->redirect('/shops/view');
				}
				$dist = $id;
			}
		}
		
		$amountCollected = array();
		$averageResult=$this->Slaves->query("SELECT avg(sale) as avg_ret, retailer_id
                                                                           from 
                                                                                retailers_logs,retailers 
                                                                           WHERE 
                                                                                retailers.id = retailers_logs.retailer_id AND 
                                                                                retailers.parent_id = ".( empty($dist)? 0 : $dist )." AND 
                                                                                retailers_logs.date > '".date('Y-m-d',strtotime('-30 days'))."' 
                                                                                group by retailer_id");

		foreach($averageResult as $avg){
			$amountCollected[$avg['retailers_logs']['retailer_id']]['average'] = $avg['0']['avg_ret'];
		}
		$successToday=$this->Slaves->query("SELECT sum(va.amount) as amts,retailers.id from vendors_activations as va join retailers on(va.retailer_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  va.status != 2 and va.status != 3 AND va.date= '".date('Y-m-d')."' $query group by retailers.id");
		//$successToday=$this->Retailer->query("SELECT sum(st.amount) as amts,retailers.id from shop_transactions as st join retailers on(st.ref1_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  st.type='".RETAILER_ACTIVATION."' and st.confirm_flag = 1 AND st.date= '".date('Y-m-d')."' $query group by retailers.id");
		foreach($successToday as $ac){
			$amountCollected[$ac['retailers']['id']]['sale'] = $ac['0']['amts'];
		}
		
		$retailers = $this->General->getRetailerList(isset($dist)? $dist : 0,isset($sid)? $sid : 0,true,isset($retId) ? $retId : 0);
		$transactions = array();
		$lastTrans=$this->Slaves->query("SELECT retailer_id FROM vendors_activations,retailers WHERE vendors_activations.date = '".date('Y-m-d')."' AND retailers.id = vendors_activations.retailer_id AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by vendors_activations.retailer_id");
		foreach($lastTrans as $ac){
			$transactions[$ac['vendors_activations']['retailer_id']] = date('Y-m-d');
		}
		$lastTrans=$this->Slaves->query("SELECT max(date) as maxDate,retailer_id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.sale > 0 AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by retailers_logs.retailer_id");
		foreach($lastTrans as $ac){
			if(!isset($transactions[$ac['retailers_logs']['retailer_id']]))
			$transactions[$ac['retailers_logs']['retailer_id']] = $ac['0']['maxDate'];
		}
		$this->set('lastTrans',$transactions);
		$this->set('records',$retailers);
		$this->set('amounts',$amountCollected);
		
//		echo "<pre>";
//		print_r($retailers);
//		die;
		//print_r($amountCollected);
		$this->render('allretailer');
	}

	function deletedRetailer($id = null){
			
		$query = "";
		$this->set('retailer_type','deleted');
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			$dist = $this->info['id'];
			$salesmen=$this->Slaves->query("select name,id,mobile from salesmen WHERE dist_id = $dist AND mobile != '".$_SESSION['Auth']['User']['mobile']."' AND active_flag = 1 order by id");
			$this->set('salesmen',$salesmen);
			$this->set('sid',$id);
				
		}else {
			$this->set('dist',$id);
			if($id != null){
				$retailers = $this->General->getRetailerList($id);
				$distributors = $this->viewVars['distributors'];// already fetched in before controller so used used thos record
				$this->set('modelName','Retailer');
				$this->set('records',$retailers);

				$distributor=array();
				foreach ($distributors as $d){//check given id is in rm's distribtors list
					if($d['Distributor']['id'] == $id ){
						$distributor = $d;
					}
				}
				if(empty($distributor)){// if rm is not belongs to the distribtors( given id )
					$this->redirect('/shops/view');
				}
				$dist = $id;
			}
		}

		$amountCollected = array();
		$averageResult=$this->Slaves->query("SELECT avg(sale) as avg_ret, retailer_id
                                                                           from 
                                                                                retailers_logs,retailers 
                                                                           WHERE 
                                                                                retailers.id = retailers_logs.retailer_id AND 
                                                                                retailers.parent_id = ".( empty($dist)? 0 : $dist )." AND 
                                                                                 
                                                                                retailers.toShow = 0
                                                                                group by retailer_id");//retailers_logs.date > '".date('Y-m-d',strtotime('-30 days'))."'

		foreach($averageResult as $avg){
			$amountCollected[$avg['retailers_logs']['retailer_id']]['average'] = $avg['0']['avg_ret'];
		}

		$successToday=$this->Slaves->query("SELECT sum(va.amount) as amts,retailers.id from vendors_activations as va join retailers on(va.retailer_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  va.status != 2 and va.status != 3 AND va.date= '".date('Y-m-d')."' $query group by retailers.id");
		foreach($successToday as $ac){
			$amountCollected[$ac['retailers']['id']]['sale'] = $ac['0']['amts'];
		}

		$qry = 1;
		if($id != null){
			$qry = "Retailer.maint_salesman = $id";
		}
		$retailObj = ClassRegistry::init('Retailer');
		$retailers =  $retailObj->find('all', array(
                                                'fields' => array('Retailer.*','users.mobile', 'sum(shop_transactions.amount) as xfer'),
                                                'conditions' => array('Retailer.parent_id' => $dist,'Retailer.toshow' => 0,$qry),
                                                'joins' => array(
		array(
                                                                                'table' => 'users',
                                                                                'type' => 'left',
                                                                                'conditions'=> array('Retailer.user_id = users.id')
		),
		array(
                                                                                'table' => 'shop_transactions',
                                                                                'type' => 'left',
                                                                                'conditions'=> array('Retailer.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
		)
		),
                                                'order' => 'xfer desc, Retailer.shopname asc',
                                                'group'	=> 'Retailer.id'
                                                )
                                                );
                                                ////$this->General->getRetailerList(isset($dist)? $dist : 0 ,isset($sid)? $sid : 0);
                                                $transactions = array();
                                                $lastTrans=$this->Slaves->query("SELECT retailer_id FROM vendors_activations,retailers WHERE vendors_activations.date = '".date('Y-m-d')."' AND retailers.id = vendors_activations.retailer_id AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by vendors_activations.retailer_id");
                                                foreach($lastTrans as $ac){
                                                	$transactions[$ac['vendors_activations']['retailer_id']] = date('Y-m-d');
                                                }
                                                $lastTrans=$this->Slaves->query("SELECT max(date) as maxDate,retailer_id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.sale > 0 AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by retailers_logs.retailer_id");
                                                foreach($lastTrans as $ac){
                                                	if(!isset($transactions[$ac['retailers_logs']['retailer_id']]))
                                                	$transactions[$ac['retailers_logs']['retailer_id']] = $ac['0']['maxDate'];
                                                }
                                                $this->set('lastTrans',$transactions);
                                                $this->set('records',$retailers);
                                                $this->set('amounts',$amountCollected);
                                                $this->render('allretailer');
	}

	function salesmanListing($id = null)
	{
		
		$salesmanListResult=$this->Slaves->query("select sm.*,group_concat(sub.name) as subs from salesmen sm left join salesmen_subarea ssa on(ssa.salesmen_id=sm.id) left join subarea sub on(sub.id=ssa.subarea_id) WHERE sm.dist_id = ".$this->info['id']." AND sm.mobile != '".$_SESSION['Auth']['User']['mobile']."' AND active_flag = 1 group by sm.id order by sm.id asc  ");
		$topups = $this->Slaves->query("SELECT salesman_transactions.salesman,sum(shop_transactions.amount) as amt FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id = salesman_transactions.shop_tran_id) WHERE shop_transactions.ref1_id = ".$this->info['id']." AND collection_date = '".date('Y-m-d')."' AND salesman_transactions.payment_type = 2 AND shop_transactions.confirm_flag != 1  group by salesman");
		$colls = $this->Slaves->query("SELECT collection_amount,salesman FROM salesman_collections WHERE date = '".date('Y-m-d')."' AND payment_type = 2 AND distributor_id = ". $this->info['id']);

		$tops = array();
		foreach($topups as $top){
			$tops[$top['salesman_transactions']['salesman']] = $top['0']['amt'];
		}

		foreach ($colls as $col) {
			if (isset($tops[$col['salesman_collections']['salesman']])) {
				$tops[$col['salesman_collections']['salesman']] = $tops[$col['salesman_collections']['salesman']] - $col['salesman_collections']['collection_amount'];
			}
		}
		$this->set('salesman',$salesmanListResult);
		$this->set('topups',$tops);
	}


	function deleteSubarea($subareaId)
	{
		$success=0;
		$smId=$_REQUEST['smId'];

		//exit;
		$this->User->query("delete from salesmen_subarea where salesmen_id=$smId and subarea_id=$subareaId");
		$success=1;
		echo $success;
		$this->autoRender=false;
	}


	function saveEditSm()
	{
		
		$smId=$_REQUEST['smId'];
		$limit = $_REQUEST['smLimit'];
		$newSmMobile=$_REQUEST['smMobile'];

		$smMobileResult=$this->User->query("select sm.mobile,sm.tran_limit,balance from salesmen sm where sm.id=$smId");
		$smMobile=$smMobileResult['0']['sm']['mobile'];
		$subareas=$_REQUEST['subAreaList'];
		$balance = $smMobileResult['0']['sm']['balance'];

		$chkDuplicateSmMobile=$this->User->query("select sm.mobile from salesmen sm where sm.mobile=$newSmMobile and sm.id!=$smId");
		if(empty($chkDuplicateSmMobile))
		{
			$success=1;
		}
		else
		{
			$success=0;
		}

		$diff = $limit - $smMobileResult['0']['sm']['tran_limit'];
		$balance = $balance + $diff;
		/*if($diff > 0)
			$balance = $balance + $diff;
			else
			$balance = $balance - abs($diff);*/
			
		$update=$this->User->query("update salesmen set name='".$_REQUEST['smName']."', balance=".$balance.",tran_limit=$limit,mobile='$newSmMobile' where id=$smId");
		$count=$this->mapSalesmanToSubarea($smId,$subareas);


		echo $success;

		$this->autoRender=false;
	}


	function editSalesman($mobile)
	{
		$salesmanResult=$this->Slaves->query("select sm.* from salesmen sm where sm.mobile='$mobile' AND sm.dist_id = " . $this->info['id']);
		if(empty($salesmanResult)){
			$this->redirect(array('action' => 'salesmanListing'));
		}
		$this->set('smR',$salesmanResult);

		$salesmanExistingSubareaResult=$this->Slaves->query("select sm.name,sm.id,sm.mobile,sm.balance,sub.name,sub.id from salesmen sm left join salesmen_subarea ssa on(ssa.salesmen_id=sm.id) left join subarea sub on(sub.id=ssa.subarea_id) where sm.mobile='$mobile'");
		$this->set('existingSA',$salesmanExistingSubareaResult);
		$this->set('smEditDeatils',$smDetails);
	}

	/*function retailerListing($id = null){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();

		$retailers = array();
		if($id == null)$this->set('empty',1);
		else {
		$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
		$this->set('distributor',$shop['company']);
		$this->set('distributor_id',$id);
		if($shop['parent_id'] == $this->info['id']){
		$this->Retailer->recursive = -1;
		$retailers = $this->Retailer->find('all', array(
		'fields' => array('Retailer.*', 'users.mobile', 'sum(shop_transactions.amount) as xfer'),
		'conditions' => array('Retailer.parent_id' => $id),
		'joins' => array(
		array(
		'table' => 'users',
		'type' => 'left',
		'conditions'=> array('Retailer.user_id = users.id')
		),
		array(
		'table' => 'shop_transactions',
		'type' => 'left',
		'conditions'=> array('Retailer.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
		)
		),
		'order' => 'Retailer.shopname asc',
		'group'	=> 'Retailer.id'
		)
		);
		}
		else {
		$retailers = array();
		}
		}
		$this->set('retailers',$retailers);
		$this->render('retailer_listing');
		}*/

	function editRetailer($type,$id){
		if(!in_array($type, array('d','r')))
		$this->redirect(array('action' => 'allRetailer'));

		$cityName = "";
		$stateName = "";

		if($type == 'r'){
			$tableName = 'retailers';
			$modName = 'Retailer';
			$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'ur.*'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
			),
			array(
					'table' => 'unverified_retailers as ur',
					'type' => 'left',
					'conditions'=> array('ur.retailer_id = Retailer.id')
			)
			))
			);

			foreach($editData[0]['ur'] as $key => $row){
				if($key != 'id')
					$editData[0]['Retailer'][$key] = $editData[0]['ur'][$key];
			}
			$subareaList=$this->Slaves->query("select id,name from subarea where area_id= " .  $editData['0']['Retailer']['area_id']);


			$this->set('subareaList',$subareaList);




			$city = $this->Slaves->query("SELECT locator_city.name,locator_city.id FROM locator_city,locator_area WHERE locator_area.id = " . $editData['0']['Retailer']['area_id'] . " AND locator_area.city_id = locator_city.id");
			$cityName = "";
			if(!empty($city)){
				$cityName = trim($city['0']['locator_city']['name']);
				$this->set('retCity',$city['0']['locator_city']['id']);
				$state = $this->Slaves->query("SELECT locator_state.name,locator_state.id FROM locator_state,locator_city WHERE locator_city.id = " . $city['0']['locator_city']['id'] . " AND locator_city.state_id = locator_state.id");
			}
			$stateName = "";
			if(!empty($state)){
				$stateName = trim($state['0']['locator_state']['name']);
				$this->set('retState',$state['0']['locator_state']['id']);
			}
		}

		if($type == 'd'){
			$tableName = 'distributors';
			$modName = 'Distributor';
			$editData = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
			'conditions' => array('Distributor.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
			)

			))
			);
			$stateName = trim($editData['0']['Distributor']['state']);
		}

               
		if(!$this->Slaves->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id))
		$this->redirect(array('action' => 'allRetailer'));

//                $city_data = $this->User->query("SELECT state_id,name FROM locator_city WHERE id = (SELECT city_id  FROM `locator_area` WHERE `name` LIKE '".$editData['0']['Distributor']['area_range']."') AND toshow = 1 ORDER BY name asc");
//                
//                $editData['0']['Distributor']['state_id'] = $city_data['0']['locator_city']['state_id'];
//                $editData['0']['Distributor']['city'] = $city_data['0']['locator_city']['name'];
//                
//                $state_data = $this->User->query("SELECT name FROM locator_state WHERE id = '".$editData['0']['Distributor']['state_id']."' AND toshow = 1 ORDER BY name asc");
//		$editData['0']['Distributor']['state'] = $state_data['0']['locator_state']['name'];
              
//                $this->set('areas',$areas);
//
//		$cities = $this->Slaves->query("SELECT id,name FROM locator_city WHERE state_id = (select id from locator_state where name='".$stateName."') AND toshow = 1 ORDER BY name asc");
//		$this->set('cities',$cities);
//
//		$areas = $this->Slaves->query("SELECT id,name FROM locator_area WHERE city_id = (select id from locator_city where name='".$cityName."') AND toshow = 1 ORDER BY name asc");
//		$this->set('areas',$areas);

		$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);

		$rm_list = $this->Slaves->query("SELECT id,name FROM rm WHERE super_dist_id = ".$this->info['id']." ORDER BY name asc");
		$this->set('rm_list',$rm_list);
		/*$fees = $this->Retailer->query("SELECT sum(salesman_transactions.collection_amount) as fee FROM salesman_transactions inner join shop_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$id);
		 $this->set('fees',$fees);*/


		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);

		$this->render('edit_form_retailer');
		//$this->autoRender = false;
	}

	function deleteRetailer(){

		$type = $_REQUEST['type'] ;
		$id = $_REQUEST['id'] ;
		$toShow = $_REQUEST['toShow'] ;
		$block = $_REQUEST['block'] ;

		if(!in_array($type, array('d','r')))
		$this->redirect(array('action' => 'allRetailer'));

		 

		if($type == 'r'){
			$subareaList=$this->Retailer->query("UPDATE retailers SET toshow = $toShow ,  block_flag = $block, modified = '".date('Y-m-d H:i:s')."' where id= " . $id);
		}

		if($type == 'd'){
			$subareaList=$this->Retailer->query("UPDATE retailers SET toshow = $toShow ,  block_flag = $block, modified = '".date('Y-m-d H:i:s')."' where id= " . $id);
		}

		$this->autoRender = false;
		$res = array('status'=>'success','msg'=>'record deleted .');
		return(json_encode($res));
	}
	/*
	 ({request:{options:{method:"post", asynchronous:true, contentType:"application/x-www-form-urlencoded", encoding:"UTF-8", parameters:{id:43, type:"r"}, evalJSON:true, evalJS:true, onSuccess:(function (transport)
	 {
	 alert(transport.toSource());
	 $("ret_"+rid).hide();
	 })}, transport:{}, url:"/shops/deleteRetailer", method:"post", parameters:{id:43, type:"r"}, body:"id=43&type=r", _complete:true}, transport:{}, readyState:4, status:200, statusText:"OK", responseText:"{\"status\":\"success\",\"msg\":\"record deleted .\"}", headerJSON:null, responseXML:null, responseJSON:null})
	 */
	function showDetails($type,$id,$dist=null){
		
		if(!in_array($type, array('d','r')) || empty($id)){
			if($dist == null)
			$this->redirect(array('action' => 'allRetailer'));
			else
			$this->redirect(array('action' => 'retailerListing'));
		}

		if($type == 'r'){
			$tableName = 'retailers';
			$modName = 'Retailer';
			$this->Retailer->recursive = -1;
			$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'ur.*'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
			),
			array(
					'table' => 'unverified_retailers as ur',
					'type' => 'left',
					'conditions'=> array('ur.retailer_id = Retailer.id')
			)
			))
			);
			
			if($editData[0]['Retailer']['verify_flag'] != 1){
				foreach($editData[0]['ur'] as $key => $row){
					if($key != 'id')
						$editData[0]['Retailer'][$key] = $editData[0]['ur'][$key];
				}
			
				$editData[0]['Retailer']['shopname'] = $editData[0]['Retailer']['shop_name'];
				$editData[0]['Retailer']['pin'] = $editData[0]['Retailer']['pin_code'];
			}
			
			if($dist != null){
				$parent = $editData['0']['Retailer']['parent_id'];
				$shop = $this->Shop->getShopDataById($parent,DISTRIBUTOR);
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR && ($this->$parent != $dist || $shop['parent_id'] != $this->info['id']))
				$this->redirect(array('action' => 'retailerListing'));
				else
				$this->redirect(array('action' => 'allRetailer'));
				$this->set('dist',$dist);
			}

			$state = isset ($editData['0']['Retailer']['state'])?$editData['0']['Retailer']['state']:"";
			$city = isset($editData['0']['Retailer']['city'])?$editData['0']['Retailer']['city']:"";
			$slab = $this->Slaves->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Retailer']['slab_id']);
			$area = $this->Slaves->query("SELECT name FROM locator_area WHERE id = " . $editData['0']['Retailer']['area_id']);

			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);
			$this->set('area',$area['0']['locator_area']['name']);
		}

		if($type == 'd'){
			$tableName = 'distributors';
			$modName = 'Distributor';
			$this->Distributor->recursive = -1;
			$editData = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
				'conditions' => array('Distributor.id' => $id),
				'joins' => array(
			array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
			)

			))
			);
			$state = $editData['0']['Distributor']['state'];
			$city = $editData['0']['Distributor']['city'];
			$slab = $this->Slaves->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Distributor']['slab_id']);

			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);
		}

		if(!$this->Slaves->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id) && $dist == null)
		$this->redirect(array('action' => 'allRetailer'));

		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);
		//$this->render('showDetails');
	}

	function editRetValidation(){
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
		if(empty($this->data['Retailer']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['pan_number'])){
			$empty[] = 'Pan Number';
			$empty_flag = true;
			$to_save = false;
		}

		if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['shopname'])){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['salesman'] == 0){
			$empty[] = 'Salesman';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['mobile_info'])){
			$empty[] = 'Mobile Info';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['app_type'])){
			$empty[] = 'App Type';
			$empty_flag = true;
			$to_save = false;
		}

		$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);

		/*$fees = $this->Retailer->query("SELECT sum(shop_transactions.amount) as fee FROM shop_transactions join salesman_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$this->data['Retailer']['id']." group by salesman_transactions.payment_type");
		 $this->set('fees',$fees);*/

		$this->set('appType',implode(",",$this->data['Retailer']['app_type']));
		
		if(!$to_save){
			$cities = $this->Slaves->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." AND toshow = 1 ORDER BY name asc");
			$this->set('cities',$cities);

			$areas = $this->Slaves->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." AND toshow = 1 ORDER BY name asc");
			$this->set('areas',$areas);
			$stateName = $this->Slaves->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Slaves->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);
			$areaName = $this->Slaves->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);
			if($empty_flag){
				$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
				$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
				$this->data['Retailer']['area_id'] = $this->data['Retailer']['area_id'];
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');

			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Slaves->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			$city = $this->Slaves->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			$slab = $this->Slaves->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			$area = $this->Slaves->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);
			$subarea=$this->Slaves->query("SELECT name FROM subarea WHERE id = " . $this->data['Retailer']['subarea']);
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->set('area',$area['0']['locator_area']['name']);
			$this->set('subarea',$subarea['0']['subarea']['name']);

			$sman = $this->Slaves->query("SELECT name FROM salesmen WHERE id = " . $this->data['Retailer']['salesman']);
			$this->set('salesman',$sman['0']['salesmen']['name']);

			$this->render('confirm_ret_edit','ajax');
		}
		else {
			$id = $this->data['Retailer']['id'];
			$name = $this->data['Retailer']['name'];
			$panNumber = $this->data['Retailer']['pan_number'];

			$email = $this->data['Retailer']['email'];
			$subareaId=$this->data['Retailer']['subarea'];

			$areaId = $this->data['Retailer']['area_id'];
			$pin = $this->data['Retailer']['pin'];
			$shopName = $this->data['Retailer']['shopname'];
			$Address = $this->data['Retailer']['address'];
			$slab = $this->data['Retailer']['slab_id'];
			$salesman = $this->data['Retailer']['salesman'];
			$kyc = addslashes($this->data['Retailer']['kyc']);
			$mInfo = addslashes($this->data['Retailer']['mobile_info']);
			$appType = addslashes($this->data['Retailer']['app_type']);

			//old slab id
			$oldSlabQ = $this->Slaves->query("select slab_id from retailers where id=".$id);
			$oldSlabId = $oldSlabQ['0']['retailers']['slab_id'];
			//

			$modified = date('Y-m-d H:i:s');

			$go = 0;

			if($this->Slaves->query("SELECT id FROM retailers WHERE parent_id = ".$this->info['id']." and id=".$id)){
				if ($this->Distributor->query("update retailers 
						set app_type= '".$appType."',
						mobile_info= '".$mInfo."', 
						kyc= '".$kyc."',
						salesman= '".$salesman."',
						pan_number='".$panNumber."', 
						email='".$email."', 
						slab_id='".$slab."', 
						modified='".$modified."',
						subarea_id='".$subareaId."',
						verify_flag='0' 
						where id=".$id)) {
					$this->Distributor->query("update unverified_retailers
							set name='".$name."',
							area_id='".$areaId."', 
							pin='".$pin."', 
							shopname='".$shopName."', 
							address='".$Address."', 
							modified='".$modified."'
							where retailer_id=".$id);
					if($oldSlabId != $slab)
					$this->Shop->updateSlab($slab,$id,RETAILER);
					$go = 1;
				}else{
					$err = 'The Retailer could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this retailer.";
			}

			if ($go == 1) {
				$this->set('data',null);
				$retailer = $this->General->getRetailerList($this->info['id'],null,true);

				$this->set('records',$retailer);
				$this->render('/elements/allRetailers','ajax');
			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
	}

	function editDistValidation(){
		
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
                
                if($this->data['Distributor']['active_flag']== 1 && empty($this->data['Distributor']['map_lat'])){
			$empty[] = 'Latitude';
			$empty_flag = true;
			$to_save = false;
		}
                
		if($this->data['Distributor']['active_flag']== 1 && empty($this->data['Distributor']['map_long'])){
			$empty[] = 'Longitute';
			$empty_flag = true;
			$to_save = false;
		}
                
                if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}
                
                if(empty($this->data['Distributor']['dob'])){
			$empty[] = 'DOB';
			$empty_flag = true;
			$to_save = false;
		}else{
                    $date = explode("-", $this->data['Distributor']['dob']);
                    $this->data['Distributor']['dob'] = $date[2] . "-" . $date[1] . "-" . $date[0];
                }


		if(empty($this->data['Distributor']['state'])){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['city'])){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}



		if(empty($this->data['Distributor']['area_range'])){
			$empty[] = 'Area Range';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}                
                if(! is_numeric($this->data['Distributor']['margin']) ){
			$incorrect[] = 'Margin';
			$incorrect_flag = true;
			$to_save = false;
		}
                if(! is_numeric($this->data['Distributor']['retailer_limit']) ){
			$incorrect[] = 'Retailer Limit';
			$incorrect_flag = true;
			$to_save = false;
		}   
		if(!$to_save){
                    $this->set('cities',$this->data['Distributor']['city']); 
                    
			if($empty_flag){
				
                                $tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
                        if($incorrect_flag){
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class"> Please provide a correct value of '.implode(", ",$incorrect).' .</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
                    
			$slab = $this->Slaves->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);
			$rm = $this->Slaves->query("SELECT name FROM rm WHERE id = " . $this->data['Distributor']['rm_id']);
			$this->set('slab',$slab['0']['slabs']['name']);
                        $this->set('state',$this->data['Distributor']['state']);
                        $this->set('city',$this->data['Distributor']['city']);
                    	
			if(empty($rm)){
				$this->set('rm_name',"");
			}else{
				$this->set('rm_name',$rm['0']['rm']['name']);
			}
                        $this->set('dob', $this->data['Distributor']['dob']);
			$this->set('rm_id',$this->data['Distributor']['rm_id']);
			$this->set('type','d');
			$this->render('confirm_dist_edit','ajax');
		}
		else {
                    
			$id = $this->data['Distributor']['id'];
			$name = addslashes($this->data['Distributor']['name']);
			$panNumber = addslashes($this->data['Distributor']['pan_number']);
			$companyName = addslashes($this->data['Distributor']['company']);
			$email = addslashes($this->data['Distributor']['email']);
                        $state = addslashes($this->data['Distributor']['state']);
                        $city = addslashes($this->data['Distributor']['city']);
                        $map_lat = addslashes($this->data['Distributor']['map_lat']);
                        $map_long = addslashes($this->data['Distributor']['map_long']);
                        $dob = addslashes($this->data['Distributor']['dob']);

			$rmQ = $this->Slaves->query("SELECT name FROM rm WHERE id = " . $this->data['Distributor']['rm_id']);
			if(empty($rmQ)){
				$rm_name = "";
			}else{
				$rm_name = $rmQ['0']['rm']['name'];
			}



			$areaRange = $this->data['Distributor']['area_range'];
			$compAddress = addslashes($this->data['Distributor']['address']);
           
			$slab = $this->data['Distributor']['slab_id'];


			//old slab id
			$oldSlabQ = $this->Slaves->query("SELECT distributors.slab_id,distributors.rm_id,users.mobile,distributors.created_rm_id FROM distributors,users WHERE users.id = distributors.user_id AND distributors.id=".$id);
			$oldSlabId = $oldSlabQ['0']['distributors']['slab_id'];
			$oldRmId = $oldSlabQ['0']['distributors']['rm_id'];
			$oldcreatedRmId = $oldSlabQ['0']['distributors']['created_rm_id'];
			$distMobile = $oldSlabQ['0']['users']['mobile'];
                        
                        $targetAmount = $this->data['Distributor']['target_amount'];
                        $rentalAmount = $this->data['Distributor']['rental_amount'];
                        $margin = $this->data['Distributor']['margin'];
                        $activeFlag = $this->data['Distributor']['active_flag'];
                        $retailerLimit = $this->data['Distributor']['retailer_limit'];
                        $sdAmt = $this->data['Distributor']['sd_amt'];
						$alternate_mob = $this->data['Distributor']['alternate_mob'];
                        
                        $sdArr = explode("-",$this->data['Distributor']['sd_date']);
                        $sdDate = $sdArr[2]."-".$sdArr[1]."-".$sdArr[0];
                        
                        $sdWdArr = explode("-",$this->data['Distributor']['sd_withdraw_date']);
                        $sdWdDate = $sdWdArr[2]."-".$sdWdArr[1]."-".$sdWdArr[0];
                        
			//
			$modified = date('Y-m-d H:i:s');
			$go = 0;
			if($this->Slaves->query("SELECT distributors.id,distributors.company,users.mobile FROM distributors,users WHERE users.id = distributors.user_id AND distributors.parent_id = ".$this->info['id']." and distributors.id=".$id)){
                            
                            $targetAmount = empty($targetAmount) ? 0 : $targetAmount;
                            $rentalAmount = empty($rentalAmount) ? 0 : $rentalAmount;
                            $margin = empty($margin) ? 0 : $margin;
                            $activeFlag = empty($activeFlag) ? 0 : $activeFlag;
                            $sdAmt = empty($sdAmt) ? 0 : $sdAmt;
                            $retailerLimit = empty($retailerLimit) ? 0 : $retailerLimit;
                            
                            if(empty($oldcreatedRmId))$oldcreatedRmId = $this->data['Distributor']['rm_id'];
							
                            
                                if ($this->Distributor->query("update distributors set name='".$name."',pan_number='".$panNumber."', company='".$companyName."', email='".$email."',  dob='".$dob."',map_lat='".$map_lat."', map_long='".$map_long."', state='".$state."', city='".$city."', area_range='".$areaRange."', address='".$compAddress."', slab_id='".$slab."', rm_id='".$this->data['Distributor']['rm_id']."',created_rm_id='$oldcreatedRmId', modified='".$modified."'
                                             , target_amount=$targetAmount ,rental_amount= $rentalAmount ,margin = $margin , active_flag = $activeFlag , retailer_limit = $retailerLimit , sd_amt = $sdAmt , sd_date = '$sdDate' ,sd_withdraw_date = '$sdWdDate',alternate_number = '$alternate_mob'
                                            where id=".$id)) {
					if($oldSlabId != $slab){
						$this->Shop->updateSlab($slab,$id,DISTRIBUTOR);
						$retailers = $this->Retailer->query("SELECT id FROM retailers WHERE parent_id = $id");
						foreach($retailers as $ret){
							$this->Shop->updateSlab($slab,$ret['retailers']['id'],RETAILER);
						}
						$this->Retailer->query("UPDATE retailers SET slab_id = $slab, modified = '".date('Y-m-d H:i:s')."' WHERE parent_id = $id");
					}
					$go = 1;
					if(!empty($this->data['Distributor']['rm_id']) && $oldRmId != $this->data['Distributor']['rm_id'] ){// execute this block only if rm_id updated
						$rm_details = $this->Slaves->query("SELECT * FROM rm WHERE id=".$this->data['Distributor']['rm_id']);
						if(!empty($rm_details)){
							//-------------- Send email to admin when rm added with distributor ----------
							// variable array contains the values which is to be parsed in email_body
							$varParseArr = array (
                                                     'rm_name'             =>  $rm_details[0]['rm']['name'],
                                                     'super_distributor_company'  =>  $this->info['name'],
													 'distributor_company'  => $companyName,
							);
							$this->General->sendTemplateEmailToAdmin("emailToAdminOnRmAddWithDistributor",$varParseArr);
							//----------------------------------------------------------------------------

							//---- Send SMS ( welcome msg ) to distributor(who became RM) on RM Add with distributor ------
							// variable array contains the values which is to be parsed in sms_body
							$varParseArr = array (
                                                     'rm_mobile'           =>  $rm_details[0]['rm']['mobile'],
                                                     'rm_name'             =>  $rm_details[0]['rm']['name'],                                                
							);
							$this->General->sendTemplateSMSToMobile($distMobile,"smsToDistributorOnRmAddWithDistributor",$varParseArr);
							//----------------------------------------------------------------------------
						}
					}

				}else{
					$err = 'The Distributor could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this distributor.";
			}

			if ($go == 1) {
				$this->set('data',null);
				$distributors = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile','rm.name', 'sum(shop_transactions.amount) as xfer'),
				'conditions' => array('Distributor.parent_id' => $this->info['id']),
				'joins' => array(
				array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
				),
				array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
				),
				array(
								'table' => 'rm',
								'type' => 'left',
								'conditions'=> array('Distributor.rm_id = rm.id')
				),
				array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Distributor.id = shop_transactions.ref2_id','datediff(now(),shop_transactions.timestamp) < 1 ', 'shop_transactions.type = 1')
				)
					
				),
				'order' => 'Distributor.name asc',
				'group'	=> 'Distributor.id'
				)
				);

				$this->set('records',$distributors);
				if($this->data['trans_type'] == 'd'){
					$this->render('/elements/allDistributors','ajax');
				}else{
					$this->render('/elements/allRetailers','ajax');
				}
				//$this->render('/elements/allRetailers','ajax');

			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
	}

	function formRetailer(){
		
		$this->render('form_retailer');
	}

	function formSalesman(){
		
		$states = $this->Slaves->query("SELECT id,name FROM locator_state WHERE toshow = 1 ORDER BY name asc");
		$this->render('form_salesman');
		$this->set('states',$states);
	}
	function formRm(){
		$states = $this->Slaves->query("SELECT id,name FROM locator_state WHERE toshow = 1 ORDER BY name asc");
		$this->render('form_rm');
		$this->set('states',$states);
	}

	function formSetUpFee(){
		$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");

		$fee = $this->Slaves->query("SELECT sum(amount) as fee,ref2_id FROM shop_transactions where type = ".SETUP_FEE." group by ref2_id");
		$fees = array();
		foreach($fee as $f){
			$fees[$f['shop_transactions']['ref2_id']] = $f['0']['fee'];
		}

		$this->set('fees',$fees);
		$this->set('sMen',$sMen);
		$this->render('form_setupfee');
	}

	function backRetailer(){
		$this->set('data',$this->data);
		/*$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
		 $this->set('cities',$cities);
		 $areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
		 $this->set('areas',$areas);*/
		$this->render('/elements/shop_form_retailer');
	}

	function backSalesman(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_form_salesman');
	}

	function backSetup(){
		$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);
		$this->set('data',$this->data);
		$this->render('/elements/shop_form_setup');
	}

	function addSetUpFee(){
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$this->set('data',$this->data);
		if($this->data['SalesmanTransaction']['retailer'] == 0){
			$empty[] = 'Retailer';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['SalesmanTransaction']['salesman'] == 0){
			$empty[] = 'Salesman';
			$empty_flag = true;
			$to_save = false;
		}


		if(empty($this->data['SalesmanTransaction']['amount'])){
			$empty[] = 'Amount';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['SalesmanTransaction']['amount'] = trim($this->data['SalesmanTransaction']['amount']);
			preg_match('/^[0-9]{1,}$/',$this->data['SalesmanTransaction']['amount'],$matches,0);
			if(empty($matches)){
				$msg = "Enter valid Amount";
				$to_save = false;
			}
		}

		if($this->data['SalesmanTransaction']['payment_mode'] == 0){
			$empty[] = 'Payment Mode';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['SalesmanTransaction']['collection_date'])){
			$empty[] = 'Collection Date';
			$empty_flag = true;
			$to_save = false;
		}

		if(!$to_save){
			$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
			$this->set('sMen',$sMen);
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_setup','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_setup','ajax');
			}
		}else if($confirm == 0 && $to_save){
			$ret = $this->Slaves->query("SELECT mobile,name FROM retailers WHERE id = " . $this->data['SalesmanTransaction']['retailer']);
			$sman = $this->Slaves->query("SELECT name FROM salesmen WHERE id = " . $this->data['SalesmanTransaction']['salesman']);

			$this->set('retailer',$ret['0']['retailers']['name']." (".$ret['0']['retailers']['mobile'].")");
			$this->set('salesman',$sman['0']['salesmen']['name']);
			$this->render('/shops/confirm_setup','ajax');
		}else {
			$shpTranId = $this->Shop->shopTransactionUpdate(SETUP_FEE,$this->data['SalesmanTransaction']['amount'],$this->info['id'],$this->data['SalesmanTransaction']['retailer']);
			$this->data['SalesmanTransaction']['shop_tran_id'] = $shpTranId;

			$colldate = explode("-",$this->data['SalesmanTransaction']['collection_date']);
			$this->data['SalesmanTransaction']['collection_date'] = $colldate['2']."-".$colldate['1']."-".$colldate['0'];
			$this->data['SalesmanTransaction']['created'] = date('Y-m-d H:i:s');
			$this->data['SalesmanTransaction']['payment_type'] = TYPE_SETUP;
			$this->data['SalesmanTransaction']['confirm_flag'] = 1;
			$this->SalesmanTransaction->create();
			if ($this->SalesmanTransaction->save($this->data)) {
				$this->set('data',null);
				$sMen = $this->Slaves->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
				$this->set('sMen',$sMen);
				$this->render('/elements/shop_form_setup','ajax');
			} else {
				$err_msg = '<div class="error_class">The transaction could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_setup','ajax');
			}
		}
	}


	function createSalesman(){
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$subareas=$this->data['subArea1'];
		$this->set('data',$this->data);
		if(empty($this->data['Salesman']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Salesman']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Salesman']['mobile'] = trim($this->data['Salesman']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Salesman']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}

		if(empty($this->data['Salesman']['tran_limit'])){
			$empty[] = 'Transaction Limit';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Salesman']['tran_limit'] = trim($this->data['Salesman']['tran_limit']);
			preg_match('/^[0-9]{1,}$/',$this->data['Salesman']['tran_limit'],$matches,0);
			if(empty($matches)){
				$msg = "Enter valid Transaction Limit";
				$to_save = false;
			}
		}
                
                /*
                 * Importing Apis Controller functions for OTP send and verify/
                 */
                
                App::import('Controller', 'Apis');
                $obj = new ApisController;
                $obj->constructClasses();
                
                
                //To send OTP to Salesman Mobile Number
                if(($confirm == 0) && !isset($this->data['Salesman']['otp']) && (!$empty_flag)){
                    
                    $sendOTPdata['mobile'] = $this->Session->read('Auth.User.mobile'); 
                    $sendOTPdata['interest'] = 'Retailer';
                    $sendOTPdata['create_saleman_otp_flag'] = 1;
                
                    $otpData = $obj->sendOTPToRetDistLeads($sendOTPdata);
                
                } 
                
		if($to_save){// salesmen creation limit check
			$salesmanLimitCheck = $this->Slaves->query("SELECT count(*) as cnt FROM salesmen WHERE dist_id =".$this->info['id']." and active_flag = 1 ");
			$distributor = $this->Slaves->query("SELECT company , salesman_limit  FROM distributors WHERE id =".$this->info['id']);
			if(!empty($salesmanLimitCheck) && $salesmanLimitCheck[0]['cnt'] >= $distributor['0']['distributors']['salesman_limit'] ){
				$msg = "Overlimit salemen creation .";
				$to_save = false;

				//-------------- email to admin about overlimit salesmen creation ----------------
				$varParseArr = array (
                                 'salesman_name'       =>  $this->data['Salesman']['name'],
                                 'salesman_mobile'     =>  $this->data['Salesman']['mobile'],
                                 'distributor_id'      =>  $this->info['id'],
                                 'distributor_company' =>  $this->info['company']
				);
				$this->General->sendTemplateEmailToAdmin("emailToAdminOnOverLimitSalesmenCreation",$varParseArr);
				//----------------------------------------------------------------------------
			}
		}
		if($to_save){
			$exists = $this->General->checkIfSalesmanExists($this->data['Salesman']['mobile']);

			$retCheck = $this->Slaves->query("SELECT * FROM retailers WHERE mobile='".$this->data['Salesman']['mobile']."'");

			if($exists){
				$msg = "Mobile number already exists.";
				$to_save = false;
			}
			else if(!empty($retCheck)){
				$msg = "You cannot make this mobile as your salesman";
				$to_save = false;
			}
		}

		if(!$to_save){
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_salesman','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_salesman','ajax');
			}
		}else if($confirm == 0 && $to_save){

			$this->render('/shops/confirm_salesman','ajax');
		}else {
                    
                    //To verify OTP sent to Salesman Mobile Number
                        $verify_param['mobile'] = $this->Session->read('Auth.User.mobile');
                        $verify_param['otp'] =   $this->data['Salesman']['otp']; 
                        $verify_param['interest'] =   'Retailer';
                        
                        $verifyData =  $obj->verifyOTP($verify_param);
                        
                        if($verifyData['status'] =='failure'){
                        
                            $err_msg = '<div class="error_class">Please enter correct OTP. </div>';
                            $this->Session->setFlash(__($err_msg, true));
                            $this->render('/shops/confirm_salesman','ajax'); 
                            return;
                        }
                    

			$this->data['Salesman']['created'] = date('Y-m-d H:i:s');
			$this->data['Salesman']['dist_id'] = $this->info['id'];
			$this->data['Salesman']['balance'] = $this->data['Salesman']['tran_limit'];
                        
			if($this->insertSalesman($this->data['Salesman'],$isCreateSalesman=true)){
				$count=$this->mapSalesmanToSubarea($this->data['Salesman']['id'],$subareas);
				$this->set('data',null);
				$this->render('/elements/shop_form_salesman','ajax');
			} else {
				$err_msg = '<div class="error_class">The Salesman could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_salesman','ajax');
			}
		}
	}
	
	function insertSalesman($salesman,$isCreateSalesman=false){
		if(isset($salesman['dist_id']) && isset($salesman['name']) && isset($salesman['mobile']) && isset($salesman['tran_limit'])){
			if(!isset($salesman['balance']))
				$salesman['balance'] = $salesman['tran_limit'];
			if(!isset($salesman['created']))
				$salesman['created'] = date('Y-m-d H:i:s');
			
			$password = $this->General->generatePassword(4);
			$salesman['password'] = $this->Auth->password($password);
			
			$this->Salesman->create();
			
			if($this->Salesman->save($salesman)){
                            //Send message only if CreateSalesman 
                            if($isCreateSalesman){
                                $paramdata['PASSWORD'] = $password;
                                $MsgTemplate = $this->General->LoadApiBalance();
                                $content =  $MsgTemplate['CreateSalesman_MSG'];
                                $message = $this->General->ReplaceMultiWord($paramdata,$content);
                            	$this->General->sendMessage($salesman['mobile'], $message, 'shops');
				return true;
                            }    
			}
			else 
				return false;
		}
		else 
			return false;
	}
	
	function createRm(){
		
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$this->set('data',$this->data);
		if(empty($this->data['Rm']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Rm']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}else {
			$this->data['Rm']['mobile'] = trim($this->data['Rm']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Rm']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}


		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Rm']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Rm']['mobile']);
				if(($user['group_id'] != MEMBER )){//|| $user['group_id'] == RELATIONSHIP_MANAGER
					$to_save = false;
					$msg = "You cannot make this mobile as your relationship manager ( RM ).";
				}
			}
		}

		if(!$to_save){
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_rm','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_rm','ajax');
			}
		}else if($confirm == 0 && $to_save){
			$this->render('/shops/confirm_rm','ajax');
		}else {

			// check if user already exist exist
			// $exists = $this->General->checkIfUserExists($this->data['Rm']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Rm']['mobile']);
				if(($user['group_id'] != MEMBER )){//|| $user['group_id'] == RELATIONSHIP_MANAGER
					$to_save = false;
					$msg = "You cannot make this mobile as your relationship manager ( RM ).";
				} else {
					$userData['User']['id'] = $user['id'];
					$userData['User']['group_id'] = RELATIONSHIP_MANAGER;
					$this->User->save($userData);//make existing user to a RelationShip Manager

				}
			}else{
				$user = $this->General->registerUser($this->data['Rm']['mobile'],ONLINE_REG,RELATIONSHIP_MANAGER);

				$user = $user['User'];
			}

			//-------------- Send email to admin on new rm registration ----------------
			// variable array contains the values which is to be parsed in email_body
			$varParseArr = array (
                             'rm_mobile'           =>  $this->data['Rm']['mobile'],
                             'rm_name'             =>  $this->data['Rm']['name'],
                             'distributor_company' =>  "None"
                             );
                             $this->General->sendTemplateEmailToAdmin("emailToAdminOnRmRegistration",$varParseArr);
                             //----------------------------------------------------------------------------

                             $this->data['Rm']['created'] = date('Y-m-d H:i:s');
                             $this->data['Rm']['super_dist_id'] = $this->info['id'];
                             $this->data['Rm']['user_id'] = $user['id'];
                             $this->Rm->create();

                             if ($this->Rm->save($this->data)) {

                             	//--------- Send SMS ( welcome msg ) to rm on rm registration ----------------
                             	// variable array contains the values which is to be parsed in sms_body
                             	$varParseArr = array (
                                     'rm_mobile'           =>  $this->data['Rm']['mobile'],
                                     'rm_name'             =>  $this->data['Rm']['name'],
                                     'distributor_company' =>  "None"
                                     );
                                     $this->General->sendTemplateSMSToMobile($this->data['Rm']['mobile'],"smsToRMOnRmRegistration",$varParseArr);
                                     //----------------------------------------------------------------------------

                                     //----- Send SMS ( rm registered ) to super distributors on rm registration --
                                     // variable array contains the values which is to be parsed in sms_body
                                     $varParseArr = array (
                                     'rm_mobile'           =>  $this->data['Rm']['mobile'],
                                     'rm_name'             =>  $this->data['Rm']['name'],
                                     'distributor_company' =>  "None"
                                     );
                                     $this->General->sendTemplateSMSToMobile($_SESSION['Auth']['User']['mobile'],"smsToSuperDistOnRmRegistration",$varParseArr);
                                     //----------------------------------------------------------------------------

                                     //$count=$this->mapSalesmanToSubarea($this->data['Salesman']['id'],$subareas);
                                     $this->set('data',null);
                                     $this->render('/elements/shop_form_rm','ajax');
                             } else {
                             	$err_msg = '<div class="error_class">The rm could not be saved. Please, try again.</div>';
                             	$this->Session->setFlash(__($err_msg, true));
                             	$this->render('/elements/shop_form_rm','ajax');
                             }
		}
	}
	function mapSalesmanToSubarea($sId,$subArea)
	{
		/*$sIdResult=$this->User->query("select id from salesmen where mobile='$sMobile'");
		 $sId=$sIdResult['0']['salesmen']['id'];*/

		$subareaList =split(" ",$subArea);

		$count=0;
		foreach($subareaList as $s)
		{

			$subareaIdResult=$this->Slaves->query("select id from subarea where name='$subareaList[$count]'");
			$subAreaId=$subareaIdResult['0']['subarea']['id'];

			$chkIfExistResult=$this->Slaves->query("select id from salesmen_subarea where  salesmen_id=$sId and subarea_id=$subAreaId ");
			if(empty($chkIfExistResult))
			{
				$this->User->query("insert into salesmen_subarea (salesmen_id,subarea_id) values($sId,$subAreaId)");
				$count++;
			}
		}

		return $count;
	}



	function salesmanTran($frm=null,$to=null,$id=null){
		if(is_null($frm)){
			$frm = date('d-m-Y');
		}
		if(is_null($to)){
			$to = date('d-m-Y');
		}
		$query = "";
		if($id != null && $id != 0){
			$query = " AND salesman = $id";
		}

		$frmArr = date('Y-m-d',strtotime($frm));
		$toArr = date('Y-m-d',strtotime($to));

		if($id != 0){
			$salesman = $this->Slaves->query("SELECT * FROM salesmen where dist_id = ".$this->info['id']." AND id = $id");
			$this->set('sinfo',$salesman);
		}
		$salesmans = $this->Slaves->query("SELECT * FROM salesmen where dist_id = ".$this->info['id']." AND active_flag = 1 order by id");

		$data = array();
		$collections = $this->Slaves->query("SELECT salesman_collections.* FROM salesman_collections WHERE salesman_collections.distributor_id = ".$this->info['id']." AND salesman_collections.date >= '$frmArr' AND salesman_collections.date <= '$toArr' $query order by salesman_collections.date");
		$topups = $this->Slaves->query("SELECT salesman_transactions.salesman,salesman_transactions.collection_date,salesman_transactions.payment_type,if(salesman_transactions.payment_type = 1,sum(salesman_transactions.collection_amount),sum(shop_transactions.amount)) as amt FROM salesman_transactions inner join salesmen ON (salesman_transactions.salesman = salesmen.id) inner join shop_transactions ON (shop_transactions.id = salesman_transactions.shop_tran_id) WHERE shop_transactions.ref1_id = ".$this->info['id']." AND collection_date >= '$frmArr' AND collection_date <= '$toArr' $query AND shop_transactions.confirm_flag != 1 group by salesman,collection_date,payment_type");
		foreach($collections as $coll){
			if(empty($coll['salesman_collections']['collection_amount'])){
				$coll['salesman_collections']['collection_amount'] = 0;
			}
			$data[$coll['salesman_collections']['date']][$coll['salesman_collections']['salesman']]['collection'][$coll['salesman_collections']['payment_type']] = $coll['salesman_collections']['collection_amount'];
		}
		foreach($topups as $topup){
			if(empty($topup['0']['amt'])){
				$topup['0']['amt'] = 0;
			}
			$data[$topup['salesman_transactions']['collection_date']][$topup['salesman_transactions']['salesman']]['topup'][$topup['salesman_transactions']['payment_type']] = $topup['0']['amt'];
		}
		$this->set('data',$data);
		$this->set('salesmans',$salesmans);
		$this->set('from',$frm);
		$this->set('to',$to);
		$this->set('id',$id);
		$this->render('form_salesman_tran');
	}

	function addSalesmanCollection(){
		$id = trim($_REQUEST['id']);
		$date = trim($_REQUEST['date']);

		$topup = trim($_REQUEST['topup']);
		$setup = trim($_REQUEST['setup']);
		$cash = trim($_REQUEST['cash']);
		$cheque = trim($_REQUEST['cheque']);

		if(empty($topup))$topup = 0;
		if(empty($setup))$setup = 0;
		if(empty($cash))$cash = 0;
		if(empty($cheque))$cheque = 0;
		
		$data = $this->Slaves->query("SELECT * FROM salesman_collections WHERE date = '$date' AND salesman = $id order by payment_type");
		$salesman = $this->Slaves->query("SELECT salesmen.dist_id,salesmen.name,salesmen.balance FROM salesmen WHERE id = $id");
		$distMobile = $this->Slaves->query("Select mobile from users where id = '".$this->info['user_id']."'");
		$distName = $this->info['name'];
		$salesmanName = $salesman[0]['salesmen']['name'];
		$MsgTemplate = $this->General->LoadApiBalance();
		if($cash + $cheque == $topup + $setup){
			if(empty($data) && $salesman['0']['salesmen']['dist_id'] == $this->info['id']){
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',1,$setup,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',2,$topup,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',3,$cash,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',4,$cheque,'".date('Y-m-d H:i:s')."')");

				$diff_top = $topup;
				$diff_set = $setup;
				$remainingbal = $salesman[0]['salesmen']['balance']+$diff_top;
				$this->Retailer->query("UPDATE salesmen SET balance = balance + $diff_top, setup_pending = setup_pending - $diff_set WHERE id = $id");
                        
                                $paramdata['DIFF_TOP'] = $diff_top;
                                $paramdata['SALESMAN_NAME'] = $salesmanName;
                                $paramdata['REMAINING_BALANCE'] = $remainingbal;
                                $content =  $MsgTemplate['Salesman_Collection_MSG'];
                                $msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                $this->General->sendMessage($distMobile[0]['users']['mobile'],$msg,'notify');
                                
//			        $this->General->sendMessage($distMobile[0]['users']['mobile'],"Dear Sir, Amount of Rs. $diff_top collected from $salesmanName and now salesman topup limit is Rs. $remainingbal",'notify');
				echo "done";
			}
			else if(!empty($data) && $data[0]['salesman_collections']['distributor_id'] == $this->info['id']){
				$diff_top = $topup - $data[1]['salesman_collections']['collection_amount'];
				$diff_set = $setup - $data[0]['salesman_collections']['collection_amount'];
				$remainingbal = $salesman[0]['salesmen']['balance']+$diff_top;
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$topup WHERE id = " . $data[1]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$setup WHERE id = " . $data[0]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$cash WHERE id = " . $data[2]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$cheque WHERE id = " . $data[3]['salesman_collections']['id']);

				$this->Retailer->query("UPDATE salesmen SET balance = balance + $diff_top, setup_pending = setup_pending - $diff_set WHERE id = $id");
				
                                $paramdata['DIFF_TOP'] = $diff_top;
                                $paramdata['SALESMAN_NAME'] = $salesmanName;
                                $paramdata['REMAINING_BALANCE'] = $remainingbal;
                                $content =  $MsgTemplate['Salesman_Collection_MSG'];
                                $msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                $this->General->sendMessage($distMobile[0]['users']['mobile'],$msg,'notify');
                                
//                                $this->General->sendMessage($distMobile[0]['users']['mobile'],"Dear Sir, Amount of Rs. $diff_top collected from $salesmanName and now salesman topup limit is Rs. $remainingbal",'notify');
				echo "done";
			}
			else {
				echo "Sorry, you cannot edit this entry";
			}
		}
		else {
			echo "Sum of cash & cheque should match with your total topup & setup collections";
		}

		$this->autoRender = false;
	}
	
	function create_unverified_retailer($retailer_id, $retailer){
		if(isset($retailer) && isset($retailer_id)){
			$this->User->query("insert into unverified_retailers
					(retailer_id, name, address, shopname, shop_type, shop_type_value, location_type, 
					created, modified)
					values ('".$retailer_id."', '".mysql_real_escape_string($retailer['name'])."', 
					'".mysql_real_escape_string($retailer['address'])."', '".mysql_real_escape_string($retailer['shopname'])."', 
					'".$retailer['shop_type']."', '".mysql_real_escape_string($retailer['shop_type_value'])."',
					'".$retailer['location_type']."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')");
		}
	}
	
	function createRetailer($retailer = array()){ 
		$is_trial = false;
		if(!empty($retailer)){
			$this->data['confirm'] = 1;
			if($retailer['api_flow'] == "verify_lead"){
				//Auto sign up of retailers from Pay1 Merchant App and pay1.in/partners
				$this->data['dist'] = $retailer['distributor_user_id'];
				$this->data['Retailer']['name'] = $retailer['name'];
				$this->data['Retailer']['shopname'] = $retailer['shop_name'];
				$this->data['Retailer']['mobile'] = $retailer['phone'];
				$this->data['Retailer']['email'] = $retailer['email'];
				$this->data['Retailer']['rental_flag'] = 0;
				$this->data['Retailer']['retailer_type'] = 3;
				$this->data['Retailer']['trial_flag'] = 1;
				
				$this->data['address']['address'] = "";
				$this->data['address']['area'] = $retailer['area'];
				$this->data['address']['city'] = $retailer['city'];
				$this->data['address']['state'] = $retailer['state'];
				$this->data['address']['pincode'] = $retailer['pin_code'];
				
				$this->data['r_u_d'] = $retailer['r_u_d'];
				
				$this->data['password'] = $retailer['pin'];
			}
			else if($retailer['api_flow'] == "trial"){
				//Create retailer from Distributor App by Distributors and Salesmen
				$is_trial = true;
				$this->data['dist'] = $retailer['distributor_user_id'];
				$this->data['Retailer']['name'] = $retailer['name'];
				$this->data['Retailer']['shopname'] = $retailer['shop_name'];
				$this->data['Retailer']['mobile'] = $retailer['mobile'];
				
				$this->data['Retailer']['rental_flag'] = 0;
				$this->data['Retailer']['retailer_type'] = 2;
				$this->data['Retailer']['trial_flag'] = 1;
                            
                            $this->data['Retailer']['otp'] = empty($retailer['otp']) ? 0 : $retailer['otp'];
                            $app_verify_otp = empty($retailer['otp_verify_flag']) ?  0 : $retailer['otp_verify_flag'];
                            
                            /*
                             * to stop reatiler creation on older app version 
                            */
                            $this->data['Retailer']['stop_creation'] = empty($retailer['app_version_code']) ? 0 : $retailer['app_version_code'];
			}
			else {
				//Create retailer from Distributor App (Not used now)
				$this->data['dist'] = $retailer['d_uid'];
				$this->data['Retailer']['name'] = $retailer['r_n'];
				$this->data['Retailer']['mobile'] = $retailer['r_m'];
				$this->data['Retailer']['shopname'] = $retailer['s_n'];
				$this->data['Retailer']['address'] = $retailer['r_add'];
				if(isset($retailer['s_t'])){
					$shop_type_index = array_search($retailer['s_t'], $this->Shop->retailerTypes());
					if(!$shop_type_index){
						$this->data['Retailer']['shop_type'] = 8;
						$this->data['Retailer']['shop_type_value'] = $retailer['s_t'];
					}
					else {
						$this->data['Retailer']['shop_type'] = $shop_type_index;
					}
				}
				if(isset($retailer['l_t'])){
					$location_type_index = array_search($retailer['l_t'], $this->Shop->locationTypes());
					if($location_type_index){
						$this->data['Retailer']['location_type'] = $location_type_index;
					}
				}
				$this->data['Retailer']['trial_flag'] = 1;
// 				$this->data['Retailer']['shop_structure'] = $retailer['s_s'];
				$this->data['Retailer']['rental_flag'] = 0;
				$this->data['Retailer']['retailer_type'] = 2;
				
				$this->data['address']['address'] = $retailer['r_add'];
				$this->data['address']['area'] = $retailer['r_a'];
				$this->data['address']['city'] = $retailer['r_c'];
				$this->data['address']['state'] = $retailer['r_s'];
				$this->data['address']['pincode'] = $retailer['r_pc'];
				if(is_float($retailer['r_la']) && is_float($retailer['r_lo'])){
					$this->data['address']['latitude'] = $retailer['r_la'];
					$this->data['address']['longitude'] = $retailer['r_lo'];
				}	
			}	
		}
        else{
        	//Create retailer through SMS and Distributor panel
            $is_trial = true;
            
                if((isset($this->data['Retailer']['shopname']))){
        
                    App::import('Controller', 'Apis');
                    $obj = new ApisController;
                    $obj->constructClasses();

                    if(!isset($this->data['Retailer']['otp'])){
                        $sendOTPdata['mobile'] = $this->Session->read('Auth.User.mobile'); 
                        $sendOTPdata['interest'] = 'Retailer';
                        $sendOTPdata['create_ret_otp_flag'] = 1;
                        
                        $otpData = $obj->sendOTPToRetDistLeads($sendOTPdata);
                    }
                    $otp_flag = true;
                }
        }
        
        $authData = $this->Session->read('Auth');
        
         if(isset($authData) && $authData['User']['group_id'] == SALESMAN){
            $salesman = $authData;
            $authData = $this->Shop->getShopDataById($salesman['dist_id'], DISTRIBUTOR);
            $authData['User']['group_id'] = SALESMAN;
            $authData['User']['mobile'] = $salesman['mobile'];                
         } elseif(empty($this->data['dist'])){
            $authData = $this->Session->read('Auth');
         } else { 
        		$authData = $this->Shop->getShopData($this->data['dist'], DISTRIBUTOR);
			$getUserdata = $this->General->getUserDataFromId($this->data['dist']);
			$authData['User']['group_id'] = $getUserdata['group_id'];
			$authData['User']['id'] = $getUserdata['id'];
			$authData['User']['mobile'] = $getUserdata['mobile'];
			$authData['User']['name'] = $getUserdata['name'];
		}
		
		if(!in_array($authData['User']['group_id'], array(DISTRIBUTOR, SALESMAN))){
			$this->redirect('/');
		}
		
	    $this->info = $authData; 
            
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
		
		$this->set('data',$this->data);
		if(empty($this->data['Retailer']['name']) && !$is_trial){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Retailer']['mobile'] = trim($this->data['Retailer']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Retailer']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
		/*if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
			}
			if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
			}
			if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
			}

			if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
			}*/
		if(empty($this->data['Retailer']['shopname']) && empty($retailer)){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address']) && empty($retailer) && !$is_trial){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
                
                
                if($to_save){
			//if(!isset($this->data['Retailer']['rental_flag'])){
				$this->data['Retailer']['rental_flag'] = 0;
			///}
                        
			$exists = $this->General->checkIfUserExists($this->data['Retailer']['mobile']);
                        if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Retailer']['mobile']);
				if($user['group_id'] == SUPER_DISTRIBUTOR || $user['group_id'] == DISTRIBUTOR || $user['group_id'] == RETAILER || $user['group_id'] == ADMIN){
					$to_save = false;
					$msg = "You cannot make this mobile as your retailer";
				}
			}
                        
                        /*
                         * To Stop Creation of Retailers only by using Distributor App
                         */
                        if(isset($this->data['Retailer']['stop_creation']) && (!$this->data['Retailer']['stop_creation'])){
                            $this->info['retailer_creation'] = 0; 
                        }
                        /*
                         * End of Stop Reatailer Creation
                        */

			
                        /*
                         * To Stop Creation of Retailers only by using Distributor App
                         */
                        if(isset($this->data['Retailer']['stop_creation']) && (!$this->data['Retailer']['stop_creation'])){
                            $this->info['retailer_creation'] = 0; 
                        }
                        /*
                         * End of Stop Reatailer Creation
                        */

			
			if($this->info['retailer_creation'] == 0){
				$msg = "You cannot create a retailer, contact pay1";
                                
                            /*
                                * To Stop Creation of Retailers only by using Distributor App
                                */
                                if(isset($this->data['Retailer']['stop_creation']) && (!$this->data['Retailer']['stop_creation'])){
                                $msg = "Due to security reason, please create Retailer using SMS or panel.pay1.in \n"
                                        . "This service will resume before the 11th of July 2016.";    
                                }
                            /*
                              * End of Stop Reatailer Creation
                           */    
                                
				$to_save = false;
			}

			$count = count($this->retailers);
			if($this->info['retailer_limit'] > 0 && $count >= $this->info['retailer_limit']){
				$msg = "You have reached your retailer-creation limit. You cannot create retailer now";
				$to_save = false;
			}

//			if($this->data['Retailer']['rental_flag'] == 0 && $this->info['kits'] == 0){
//				$msg = "You have 0 kits left. Buy more retailer kits to enjoy this benefit";
//				$to_save = false;
//			}
		}
		
		if(!$to_save){
			/*$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			 $this->set('cities',$cities);
			 	
			 $areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			 $this->set('areas',$areas);
			 	
			 $subareas=$this->Retailer->query("select id,name from subarea where area_id=".$this->data['Retailer']['area_id']);
			 $this->set('subareas',$subareas);*/

			if($empty_flag){
				if(empty($retailer)){
					$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
					$this->Session->setFlash($err_msg, true);
					$this->render('/elements/shop_form_retailer','ajax');
				}
				else{
					return array("status" => "failure", "description" => $msg);
				}	
			}
			else {
				if(empty($retailer)){
					$err_msg = '<div class="error_class">'.$msg.'</div>';
					$this->Session->setFlash(__($err_msg, true));
					$this->render('/elements/shop_form_retailer','ajax');
				}
				else{
					return array("status" => "failure", "description" => $msg);
				}
			}
		}
		else if($confirm == 0 && $to_save){
			/*$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			 $city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			 $slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			 $area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);
			 	
			 	
			 $this->set('slab',$slab['0']['slabs']['name']);
			 $this->set('city',$city['0']['locator_city']['name']);
			 $this->set('state',$state['0']['locator_state']['name']);
			 $this->set('area',$area['0']['locator_area']['name']);*/
			if(empty($retailer)){
				$this->render('/shops/confirm_retailer','ajax');
			}
			else{
				return array("status" => "failure", "description" => $msg);
			}	
		}
		else {
                    
                    App::import('Controller', 'Apis');
                    $obj = new ApisController;
                    $obj->constructClasses();

                    if($app_verify_otp == 0 && $this->data['Retailer']['stop_creation']){

                            $sendOTPdata['mobile'] = $this->Session->read('Auth.User.mobile'); 
                            $sendOTPdata['interest'] = 'Distributor';
                            $sendOTPdata['create_ret_otp_flag'] = 1;
                            $otpData = $obj->sendOTPToRetDistLeads($sendOTPdata);

                            return $otpData;

                    }
                    
                    //only if otp sent i.e. otp flag set
                    if($otp_flag || $app_verify_otp){
                        
                        $verify_param['mobile'] = $this->Session->read('Auth.User.mobile');
                        $verify_param['otp'] =   $this->data['Retailer']['otp']; 
                        $verify_param['interest'] =   'Distributor'; 
                           
                        $verifyData =  $obj->verifyOTP($verify_param);
                        
                        if($verifyData['status'] =='failure'){
                            
                            
                            //Return Failure for Distributor & Salesman App
                            if($app_verify_otp){
                              
                                return $verifyData;
                            }
                        
                            $err_msg = '<div class="error_class">Please enter correct OTP. </div>';
                            $this->Session->setFlash(__($err_msg, true));
                            $this->render('/shops/confirm_retailer','ajax'); 
                            return;
                        }
                    }
                    
			$this->data['Retailer']['created'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['balance'] = 0;
			/*$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			 $this->data['Retailer']['state'] = $state['0']['locator_state']['name'];
			 	
			 $city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			 $this->data['Retailer']['city'] = $city['0']['locator_city']['name'];*/

			$default = $this->Slaves->query("SELECT id FROM salesmen WHERE mobile = '" . $authData['User']['mobile']."'");

			if(!empty($default)){
				$this->data['Retailer']['salesman'] = $default['0']['salesmen']['id'];
				$this->data['Retailer']['maint_salesman'] = $default['0']['salesmen']['id'];
			}

			if(!$exists){
				$user = $this->General->registerUser($this->data['Retailer']['mobile'],RETAILER_REG,RETAILER, $this->data['password']);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = RETAILER;
				$this->User->save($userData);//make already user to a retailer
			}
			$this->data['Retailer']['user_id'] = $user['id'];
			$this->data['Retailer']['parent_id'] = $this->info['id'];
			$this->data['Retailer']['slab_id'] = $this->info['slab_id'];
                        
			$this->Retailer->create();
			if ($this->Retailer->save($this->data)) {
				//$this->General->updateLocation($this->data['Retailer']['area_id']);
					
				$this->create_unverified_retailer($this->Retailer->id, $this->data['Retailer']);
				
				if(isset($this->data['address']))
					$this->General->updateRetailerAddress($this->Retailer->id, $this->data['Retailer']['user_id'], $this->data['address']);
				
				$this->General->makeOptIn247SMS($this->data['Retailer']['mobile']);
				
				//if($this->data['login'] == 'on'){
				//$sms = "Welcome to Pay1!\nUserName: ".$this->data['Retailer']['mobile']."\nPassword: ".$user['syspass'];
//                                $sms = "Welcome to Pay1!\nUserName: ".$this->data['Retailer']['mobile'];
//                                $sms .= "Click on GENERATE NEW PIN to set password ";
//				$sms .= "\nDownload Apps: http://panel.pay1.in/users/app";
//				$sms .= "\nWebsite: http://shop.pay1.in";
//				$sms .= "\nMisscall recharges: Dial 02267242234";
                                
                                //$paramdata['RETAILER_MOBILE_NUMBER'] = $this->data['Retailer']['mobile'];
                                $MsgTemplate = $this->General->LoadApiBalance(); 
		                $sms = $MsgTemplate['CreateRetailer_App_MSG'];
                                
				//$sms = $this->getRentalSMS($this->data['Retailer']['rental_flag'],$this->info['rental_amount'],$this->info['target_amount']);
				
				$this->General->sendMessage($user['mobile'],$sms,'payone');
				if($this->data['Retailer']['rental_flag'] == 1){
					$mail_subject = "New Retailer Created On Rental";
 				}else {
					$mail_subject = "New Retailer Created Via Kit";
				}
				$mail_body = "Distributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Retailer: " . $this->data['Retailer']['shopname'] . "<br/>";
				$mail_body .= "Address: " . $this->data['Retailer']['address'];
// 				$this->General->sendMails($mail_subject, $mail_body,array('tl@mindsarray.com','rm@mindsarray.com','tadka@mindsarray.com'));
				
				/*if(!empty($retailer)){
					$this->User->query("update users set passFlag = 1 where mobile = '".$this->data['Retailer']['mobile']."'");
				}*/	
				if(!empty($retailer) && $this->data['r_u_d']){
					$mail_subject_rud = "New Retailer Registered: Assign Distributor";
					$mail_body_rud = "A new retailer was created just now. A distributor needs to be assigned.<br/>";
					$mail_body_rud .= "Name: ". $this->data['Retailer']['name'] . "<br/>";
					$mail_body_rud .= "Mobile: " . $this->data['Retailer']['mobile'] . "<br/>";
					$mail_body_rud .= "Area: " . $this->data['address']['area'] . "<br/>";
					$mail_body_rud .= "City: " . $this->data['address']['city'] . "<br/>";
					$mail_body_rud .= "State: " . $this->data['address']['state'] . "<br/>";
					$mail_body_rud .= "Pin code: " . $this->data['address']['pincode'] . "<br/>";
					$emails_rud = array('info@pay1.in');
					$this->General->sendMails($mail_subject_rud, $mail_body_rud, $emails_rud, "mail");
				}
				
				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);

				if($this->data['Retailer']['rental_flag'] == 0){
					if($this->info['commission_kits_flag'] == 1){
						if($this->info['kits'] == -1){
							$this->Retailer->query("UPDATE distributors SET discounted_money=discounted_money+".$this->info['discount_kit']." WHERE id = ".$this->info['id']);
						}
						else {
							$this->Retailer->query("UPDATE distributors SET kits=kits-1,discounted_money=discounted_money+".$this->info['discount_kit']." WHERE id = ".$this->info['id']);
						}
					}
					else if($this->info['kits'] > 0){
						$this->Retailer->query("UPDATE distributors SET kits=kits-1 WHERE id = ".$this->info['id']);
					}
				}

				/*if(!empty($default)){
					$date=Date("Y-m-d");
					$date_created=Date("Y-m-d H:i:s");
					$st_id = $this->Shop->shopTransactionUpdate(SETUP_FEE,SETUP_FEE_AMT,$this->info['id'],$this->Retailer->id);
					//for trial put insert amount=0 in sst but in st put 1500
					$this->Retailer->query("INSERT INTO salesman_transactions(shop_tran_id,salesman,payment_mode,payment_type,collection_amount,collection_date,created) VALUES ($st_id,".$default['0']['salesmen']['id'].",1,1,0,'$date','$date_created')");
					}*/
				if(empty($retailer)){
					$this->set('data',null);
					$this->render('/elements/shop_form_retailer','ajax');
				}
				else{
					App::import('Controller', 'Distributors');
					$ini = new DistributorsController;
					$ini->constructClasses();
					$retailer_detail = $ini->getRetailer(array('r_id' => $this->Retailer->id));
					return array("status" => "success", "description" => array(
								'User' => $this->Retailer->read(),
								'retailer' => $retailer_detail['description']
							));
				}	
			} else {
				if(empty($retailer)){
					$err_msg = '<div class="error_class">The Retailer could not be saved. Please, try again.</div>';
					$this->Session->setFlash(__($err_msg, true));
					$this->render('/elements/shop_form_retailer','ajax');
				}	
				else {
					return array("status" => "failure", "description" => $msg);
				}	
			}
		}
	}

	function getRentalSMS($rental_flag,$rental_amount,$target_amount){
		$msg = "";
		if($rental_flag == 1){
			if($rental_amount > 0 && $target_amount > 0){
				//$msg = "\nDo minimum sale of Rs $target_amount per month to avoid rental of Rs $rental_amount";
			}
			else if($rental_amount > 0 && $target_amount == -1){
				//$msg = "\nRs $rental_amount will be charged as monthly rental";
			}
		}
		else {
			//$msg = "\nKindly pay setup fee of Rs 500 to your distributor";
		}
		return $msg;
	}

	function createRetailerApp($params,$format){
		$to_save = true;

		$this->data['Retailer']['mobile'] = $params['mobile'];
		if(empty($_SESSION['Auth']))$this->redirect('/');
		preg_match('/^[7-9][0-9]{9}$/',$this->data['Retailer']['mobile'],$matches,0);
		if(empty($matches)){
			$msg = "Invalid demo mobile number";
			return array('status' => 'failure','description' => $msg);
		}
		$exists = $this->General->checkIfUserExists($this->data['Retailer']['mobile']);
		if($exists){
			$user = $this->General->getUserDataFromMobile($this->data['Retailer']['mobile']);
			if($user['group_id'] != MEMBER){
				$to_save = false;
				$msg = "You cannot make this mobile as your retailer.";
			}
			else {
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = RETAILER;
				$this->User->save($userData);//make already user to a retailer
			}
		}
		else{
			$user = $this->General->registerUser($this->data['Retailer']['mobile'],RETAILER_REG,RETAILER);
			$user = $user['User'];
		}

		if($to_save){  
                
			$this->data['Retailer']['user_id'] = $user['id'];
			$this->data['Retailer']['parent_id'] = $_SESSION['Auth']['id'];
			$this->data['Retailer']['slab_id'] = $_SESSION['Auth']['slab_id'];
			$this->data['Retailer']['rental_flag'] = 0;
			if(isset($params['name'])){
				$this->data['Retailer']['name'] = $params['name'];
				$this->data['Retailer']['pin'] = $params['pincode'];
			}

			if(isset($params['shopname']))
			{
				$this->data['Retailer']['shopname'] = $params['shopname'];
			}

			if(isset($params['subArea']))
			{
				$this->data['Retailer']['subarea_id'] = $params['subArea'];
			}

			if(isset($params['type']))
			{
				$this->data['Retailer']['retailer_type'] = $params['type'];
			}


			if(isset($params['salesmanId'])){
				$this->data['Retailer']['salesman'] = $params['salesmanId'];
				$this->data['Retailer']['maint_salesman'] = $params['salesmanId'];
			}

			$this->Retailer->create();
			if ($this->Retailer->save($this->data)) {
				
				$this->create_unverified_retailer($this->Retailer->id, $this->data['Retailer']);
				
				$this->General->makeOptIn247SMS($this->data['Retailer']['mobile']);
				//$sms = "Welcome to Pay1!\nUserName: ".$this->data['Retailer']['mobile']."\nPassword: ".$user['syspass'];
//                $sms = "Welcome to Pay1!\n";
//				$sms .= "\nDownload Apps: http://panel.pay1.in/users/app";
//				$sms .= "\nWebsite: http://shop.pay1.in";
//				$sms .= "\nMisscall recharges: Dial 02267242234";
                                
                                $MsgTemplate = $this->General->LoadApiBalance(); 
		                $sms = $MsgTemplate['CreateRetailer_App_MSG'];

				//$sms .= $this->getRentalSMS($params['rental_flag'],$_SESSION['Auth']['rental_amount'],$_SESSION['Auth']['target_amount']);

				$this->General->sendMessage($this->data['Retailer']['mobile'],$sms,'payone');

				$sName = '';
				$msg = 'Retailer created successfully.';

				if($params['salesmanId']){
					$sQ = $this->Slaves->query("SELECT name,mobile FROM salesmen where id=".$params['salesmanId']);
					$sName = $sQ['0']['salesmen']['name'];
				}
				if($params['rental_flag'] == 1){
					$mail_subject = "New Retailer Created On Rental";
				}
				else {
					$mail_subject = "New Retailer Created Via Kit";
				}
				$mail_body = "Salesman: ".$sName."<br/>";
				$mail_body .= "Distributor: " .$_SESSION['Auth']['company'] . "<br/>";
				if(isset($this->data['Retailer']['name'])){
					$mail_body .= "Retailer: " . $this->data['Retailer']['name'] . "<br/>";
				}
				$mail_body .= "Retailer Mobile: " . $this->data['Retailer']['mobile'];
				//$this->General->sendMails($mail_subject, $mail_body);

				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
					
				if($params['rental_flag'] == 0){
					if($_SESSION['Auth']['commission_kits_flag'] == 1){
						if($_SESSION['Auth']['kits'] == -1){
							$this->User->query("UPDATE distributors SET discounted_money=discounted_money+".$_SESSION['Auth']['discount_kit']." WHERE id = ".$_SESSION['Auth']['id']);
						}
						else {
							$this->User->query("UPDATE distributors SET kits=kits-1,discounted_money=discounted_money+".$_SESSION['Auth']['discount_kit']." WHERE id = ".$_SESSION['Auth']['id']);
						}
					}
					else if($_SESSION['Auth']['kits'] > 0){
						$this->Retailer->query("UPDATE distributors SET kits=kits-1 WHERE id = ".$_SESSION['Auth']['id']);
						if($params['salesmanId'] && $sQ['0']['salesmen']['mobile'] == $_SESSION['Auth']['User']['mobile']){
							$msg .= "\nKits left: " . $_SESSION['Auth']['kits'] - 1;
						}
					}
				}

				/*if(isset($params['salesmanId'])){
					$date=Date("Y-m-d");
					$date_created=Date("Y-m-d H:i:s");
					$st_id = $this->Shop->shopTransactionUpdate(SETUP_FEE,SETUP_FEE_AMT,$_SESSION['Auth']['id'],$this->Retailer->id);
					//for trial put insert amount=0 in sst but in st put 1500
					$this->User->query("insert into salesman_transactions(shop_tran_id,salesman,payment_mode,payment_type,collection_amount,collection_date,created) values($st_id,".$params['salesmanId'].",1,1,0,'$date','$date_created')");
					}*/
			}
			return array('status' => 'success','description' => $msg);
		}
		else {
			return array('status' => 'failure','description' => $msg);
		}
	}

	function changePassword(){
		if($this->Session->read('Auth.User.group_id') ==''){
			$this->redirect('/');
	       }

		$this->render('setting');
	}

	/*function commissions(){
		$data = $this->Shop->getAllCommissions($this->info['id'],$this->Session->read('Auth.User.group_id'),$this->info['slab_id']);
		//$this->printArray($data);
		$this->set('data',$data);
		$this->render('discount_table');
	}*/

	/*function setSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		if($this->info['signature_flag'] == 1){
			$data['signature'] = 'on';
		}
		$data['text'] = $this->info['signature'];
		$this->set('data',$data);
		$this->render('signature');
	}


	function saveSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		$signature = 0;
		if(isset($this->data['signature']) && $this->data['signature'] == 'on'){
			$signature = 1;
		}
		$this->set('data',$this->data);
		if($signature == 1 && empty($this->data['text'])){
			$err_msg = '<div class="error_class">Please enter your 60 character signature.</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
		else {
			$this->Retailer->updateAll(array('Retailer.signature_flag' => $signature, 'Retailer.signature' => '"'.$this->data['text'].'"'),array('Retailer.id' => $this->info['id']));
			if($signature == 0) $msg = 'Signature Removed Successfully';
			else $msg = 'Signature Saved Successfully';
			$err_msg = '<div class="success">'.$msg.'</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
	}*/

	function accountHistory($date=null,$page=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RETAILER){
			$this->redirect('/');
		}
		$api = false;
		$page_limit = 30;
		$pageWise = 1;
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
			$page_limit = $params['limit'];
            $pageWise = isset($params['is_page_wise'])? $params['is_page_wise'] : 1;            
		}        
		$grp_id = $_SESSION['Auth']['User']['group_id'];
		if($page == null)$page = 1;
		if($date != null){
			//$limit = " limit " . ($page-1)*$page_limit.",".$page_limit;

			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];

			
				
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				
				$nodays=(strtotime($date_to) - strtotime($date_from))/ (60 * 60 * 24);
				$nodays += 1;
				if($grp_id == ADMIN){
					$query = "(
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Topup Transferred' as name,trim(super_distributors.company) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN super_distributors ON (super_distributors.id = shop_transactions.ref2_id) WHERE type = " . ADMIN_TRANSFER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Commission Transferred' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE type = " . COMMISSION_SUPERDISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Pullback' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE user_id = ".$_SESSION['Auth']['User']['id'] . " AND type = " . PULLBACK_SUPERDISTRIBUTOR . ")
						)";
				}
				else if($grp_id == SUPER_DISTRIBUTOR){
					$query = "(
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Topup Received' as name,trim(super_distributors.company) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN super_distributors ON (super_distributors.id = shop_transactions.ref2_id) WHERE ref2_id = ".$this->info['id']." AND type = " . ADMIN_TRANSFER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Commision Received' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND type = " . COMMISSION_SUPERDISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Topup Transferred' as name,trim(distributors.company) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = shop_transactions.ref2_id) WHERE ref1_id = ".$this->info['id']." AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Commission Transferred' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = shop_transactions.ref1_id) WHERE distributors.parent_id = ".$this->info['id']." AND type = " . COMMISSION_DISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Pullback by Company' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']. " AND type = " . PULLBACK_SUPERDISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Pullback by me' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE user_id = ".$_SESSION['Auth']['User']['id']. " AND type = " . PULLBACK_DISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Incentive/Refund' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']. " AND ref2_id = ".SUPER_DISTRIBUTOR." AND type = " . REFUND . ")
					)";	
				}
				else if($grp_id == DISTRIBUTOR){
					$query = "(
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Topup Received' as name,trim(distributors.company) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = shop_transactions.ref2_id) WHERE ref2_id = ".$this->info['id']." AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Commision Received' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND type = " . COMMISSION_DISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Topup Transferred' as name,if(retailers.shopname is NULL or retailers.shopname = '',retailers.mobile,retailers.shopname) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN retailers ON (retailers.id = shop_transactions.ref2_id) WHERE ref1_id = ".$this->info['id']." AND type = " . DIST_RETL_BALANCE_TRANSFER . " AND (confirm_flag != 1 OR type_flag != 5))
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Pullback by SD' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']. " AND type = " . PULLBACK_DISTRIBUTOR . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Pullback by Me' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE user_id = ".$_SESSION['Auth']['User']['id']. " AND type = " . PULLBACK_RETAILER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Incentive/Refund' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']. " AND ref2_id = ".DISTRIBUTOR." AND type = " . REFUND . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Service Charge' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$this->info['id']. " AND user_id = ".DISTRIBUTOR." AND type = " . SERVICE_CHARGE . ")
					)";
				}
				else if($grp_id == RETAILER){
					$retailers = $this->User->query("select device_serial_no from retailers where id = ".$_SESSION['Auth']['id']);
					$mpos_history = "";
					if(!empty($retailers[0]['retailers']['device_serial_no'])){
						$mpos_history = " UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'mPOS Cash Out' as name,trim(shop_transactions.ref1_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']. " AND ref2_id = '57' AND type = " . MPOS_TRANSFER . " )
						 UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'mPOS Cash Out Incentive' as name,trim(shop_transactions.ref1_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']. " AND type = " . MPOS_INCENTIVE . " )
						 ";
					}
					$query = "(
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Topup Received' as name,trim(retailers.shopname) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN retailers ON (retailers.id = shop_transactions.ref2_id) WHERE ref2_id = ".$_SESSION['Auth']['id']." AND type = " . DIST_RETL_BALANCE_TRANSFER . " AND (confirm_flag != 1 OR type_flag != 5))
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Recharge' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']." AND type = " . RETAILER_ACTIVATION . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Recharge Commission' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']." AND type = " . COMMISSION_RETAILER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Pullback by Distributor' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']." AND type = " . PULLBACK_RETAILER . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, shop_transactions.amount as credit, 0 as debit, 'Incentive/Refund' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']. " AND ref2_id = ".RETAILER." AND type = " . REFUND . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Service Charge' as name,trim(shop_transactions.ref2_id) as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']. " AND user_id = ".RETAILER." AND type = " . SERVICE_CHARGE . ")
						UNION
						(SELECT shop_transactions.date, shop_transactions.id, shop_transactions.confirm_flag, 0 as credit, shop_transactions.amount as debit, 'Monthly Rental' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref1_id = ".$_SESSION['Auth']['id']. " AND ref2_id = ".RETAILER." AND type = " . RENTAL . ")
						 $mpos_history 
						)";
					/*$query = "(
						(SELECT shop_transactions.id,shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN distributors ON (distributors.id = ref1_id) WHERE ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,products.name as name,services.name as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN products ON (ref2_id = products.id) INNER JOIN services ON (products.service_id = services.id) WHERE ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . RETAILER_ACTIVATION . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type in (" . TDS_RETAILER . "," . COMMISSION_RETAILER . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,'Reversal' as name,products.name as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN products ON (st1.ref2_id = products.id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type = ".REVERSAL_RETAILER.")
					)";*/
				}
				if($nodays <= 7){
					$transactions = $this->Shop->getMemcache("txns_".$date."_".$_SESSION['Auth']['id']."_".$grp_id);
					if(empty($transactions)){
						$transactions = $this->Slaves->query("SELECT transactions.*,opening_closing.opening,opening_closing.closing,if(opening_closing.timestamp is null,transactions.timestamp,opening_closing.timestamp) as stamp FROM $query as transactions left join opening_closing ON (opening_closing.shop_transaction_id = transactions.id AND opening_closing.shop_id = ".((empty($_SESSION['Auth']['id']))? 0 : $_SESSION['Auth']['id'])." AND opening_closing.group_id = $grp_id) where transactions.date >= '$date_from' AND  transactions.date <= '$date_to'  AND  (transactions.credit > 0 OR transactions.debit > 0)  order by stamp desc,transactions.id desc");
						
						if($date_to == date('Y-m-d')) $time = 10*60;
						else $time = 7*24*60*60;
						$this->Shop->setMemcache("txns_".$date."_".$_SESSION['Auth']['id']."_".$grp_id,$transactions,$time);
					}
					
					$trans_count = count($transactions);
                    if($pageWise == 1){
                        $transactions = array_slice($transactions,($page-1)*$page_limit,$page_limit);
                    }					
				}else {
					$transactions = array();
					$trans_count = 0;
					$this->set('date_limit',0);
				}
				$this->set(compact('trans_count','date_from','date_to'));
				if($api){
					$result['trans_count'] = $trans_count;
					$result['date_from'] = $date_from;
					$result['date_to'] = $date_to;
				}
			}
			else {
				$transactions = array();
			}

		}
		else {
			$transactions = array();
		}
		$this->set('page',$page);
		$this->set('transactions',$transactions);
		if($api){
			if($date == null) $result['empty'] = 0;
			$result['page'] = $page+1;
			$result['transactions'] = $transactions;
			return $result;
		}
		else {
			if($date == null) $this->set('empty',0);
			$this->render('account_history');
		}
	}

	function topup($date = null,$page=null){
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		$grp_id = $_SESSION['Auth']['User']['group_id'];
		if($page == null)$page = 1;
		if(empty($date)){
			$date = date('dmY')."-".date('dmY');
		}
		$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

		$dates = explode("-",$date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);

			if($grp_id == DISTRIBUTOR){
				$query = "SELECT shop_transactions.id,shop_transactions.ref1_id,shop_transactions.ref2_id,shop_transactions.amount,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions USE INDEX (type_date) LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = ".$_SESSION['Auth']['id']." AND group_id = ".DISTRIBUTOR.") WHERE shop_transactions.confirm_flag != 1 AND ((ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . COMMISSION_DISTRIBUTOR . ")) AND date >= '$date_from' AND date <= '$date_to' order by id desc";
			}
			else if($grp_id == SUPER_DISTRIBUTOR){
				$query = "SELECT shop_transactions.id,shop_transactions.ref1_id,shop_transactions.ref2_id,shop_transactions.amount,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions USE INDEX (type_date) LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = ".$_SESSION['Auth']['id']." AND group_id = ".SUPER_DISTRIBUTOR.") WHERE shop_transactions.confirm_flag != 1 AND ((ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . ADMIN_TRANSFER . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . COMMISSION_SUPERDISTRIBUTOR . ")) AND date >= '$date_from' AND date <= '$date_to' order by id desc";
			}
			$transactions = $this->Slaves->query($query . " $limit");
			$transArr = array();
			if(!empty($transactions))foreach ($transactions as $transaction){
				if($transaction['shop_transactions']['type'] == COMMISSION_DISTRIBUTOR || $transaction['shop_transactions']['type'] == COMMISSION_SUPERDISTRIBUTOR){
					$transArr[$transaction['shop_transactions']['ref2_id']]['amount'] =  ( isset($transArr[$transaction['shop_transactions']['ref2_id']]['amount']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['amount'] : 0 ) + $transaction['shop_transactions']['amount'];
					$transArr[$transaction['shop_transactions']['ref2_id']]['opening'] =  isset($transArr[$transaction['shop_transactions']['ref2_id']]['opening']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['opening'] : 0  ;
					$transArr[$transaction['shop_transactions']['ref2_id']]['closing'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['closing']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['closing'] : 0  ;
					$transArr[$transaction['shop_transactions']['ref2_id']]['timestamp'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['timestamp']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['timestamp'] : 0  ;
					$transArr[$transaction['shop_transactions']['ref2_id']]['type'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['type']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['type'] : $transaction['shop_transactions']['type']  ;
				}else{
					$transArr[$transaction['shop_transactions']['id']]['amount'] = ( isset($transArr[$transaction['shop_transactions']['id']]['amount']) ? $transArr[$transaction['shop_transactions']['id']]['amount'] : 0 ) +  $transaction['shop_transactions']['amount'];
					$transArr[$transaction['shop_transactions']['id']]['opening'] = $transaction['opening_closing']['opening'];
					$transArr[$transaction['shop_transactions']['id']]['closing'] = $transaction['opening_closing']['closing'];
					$transArr[$transaction['shop_transactions']['id']]['timestamp'] = $transaction['shop_transactions']['timestamp'];
					$transArr[$transaction['shop_transactions']['id']]['type'] = $transaction['shop_transactions']['type'];
					 
				}
			}
			$transactions = $transArr ;
			$trans_count = $this->Slaves->query($query);
			$trans_count = count($trans_count);
			$this->set(compact('trans_count','date_from','date_to'));
			if(!empty ($api)){
				$result['trans_count'] = $trans_count;
				$result['date_from'] = $date_from;
				$result['date_to'] = $date_to;
			}
		}
		else {
			$transactions = array();
		}

		/*}
		 else {
			$transactions = array();
			}*/
		$this->set('page',$page);
		$this->set('transactions',$transactions);

		if($date == null) $this->set('empty',0);
		$this->render('buy_report');
	}

	function topupDist($date = null,$dist=null){
		$pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
        $page_old = empty($_GET['old_data']) ? "" : $_GET['old_data'];
                
        $grp_id = $_SESSION['Auth']['User']['group_id'];
        $query_old = '';

		
		if(is_array($date)){
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		if($dist == null)$dist = 0;

		if($date == null){
			$date = date('dmY') . '-' . date('dmY');
		}

		$this->set('dist',$dist);
                
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];

			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
                                
				if($grp_id == ADMIN){
					if($dist == 0){
                                            
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.note,shop_transactions.type_flag,st.amount as commission,shop_transactions.timestamp,trim(super_distributors.company) as company ,opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join super_distributors  ON (super_distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id ) WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.type =" . ADMIN_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' order by shop_transactions.id desc";
                                            if($page_old == "old_csv"){
                                                $query_old = "SELECT shop_transactions_logs.id,shop_transactions_logs.amount,shop_transactions_logs.type,shop_transactions_logs.note,shop_transactions_logs.type_flag,st.amount as commission,shop_transactions_logs.timestamp,trim(super_distributors.company) as company ,opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions_logs USE INDEX ( type_date ) inner join super_distributors  ON (super_distributors.id = shop_transactions_logs.ref2_id) left join shop_transactions_logs as st ON (st.ref2_id = shop_transactions_logs.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions_logs.id ) WHERE shop_transactions_logs.confirm_flag != 1 AND shop_transactions_logs.type =" . ADMIN_TRANSFER . " AND shop_transactions_logs.date >= '$date_from' AND shop_transactions_logs.date <= '$date_to' order by shop_transactions_logs.id desc";
                                                                                          
                                            }
                                                
                                        }
					else {
                                             
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.note,shop_transactions.type_flag,st.amount as commission,shop_transactions.timestamp,trim(super_distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join super_distributors  ON (super_distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id ) WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.ref2_id = $dist AND shop_transactions.type =" . ADMIN_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' order by shop_transactions.id desc";
                                             if($page_old == "old_csv"){
                                                $query_old = "SELECT shop_transactions_logs.id,shop_transactions_logs.amount,shop_transactions_logs.type,shop_transactions_logs.note,shop_transactions_logs.type_flag,st.amount as commission,shop_transactions_logs.timestamp,trim(super_distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions_logs USE INDEX ( type_date ) inner join super_distributors  ON (super_distributors.id = shop_transactions_logs.ref2_id) left join shop_transactions_logs as st ON (st.ref2_id = shop_transactions_logs.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions_logs.id ) WHERE shop_transactions_logs.confirm_flag != 1 AND shop_transactions_logs.ref2_id = $dist AND shop_transactions_logs.type =" . ADMIN_TRANSFER . " AND shop_transactions_logs.date >= '$date_from' AND shop_transactions_logs.date <= '$date_to' order by shop_transactions_logs.id desc";
                                             }
					}
                                        
				}else {
                                    
					$r1 = 0;
					if($grp_id == RELATIONSHIP_MANAGER){
						$r1 = $this->info['super_dist_id'];
						$extra = " AND distributors.rm_id = " . $this->info['id'];
					}else{
						$r1 = $this->info['id'];
						$extra = "";
					}
					if($dist == 0){
                                                
                                                    $query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.type_flag,shop_transactions.note,st.amount as commission,shop_transactions.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join distributors  ON (distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND group_id = ".SUPER_DISTRIBUTOR.") WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.ref1_id = $r1 AND shop_transactions.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to'  $extra order by shop_transactions.id desc";
                                                if($page_old == "old_csv"){
                                                    $query_old = "SELECT shop_transactions_logs.id,shop_transactions_logs.amount,shop_transactions_logs.type,shop_transactions_logs.type_flag,shop_transactions_logs.note,st.amount as commission,shop_transactions_logs.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions_logs USE INDEX ( type_date ) inner join distributors  ON (distributors.id = shop_transactions_logs.ref2_id) left join shop_transactions_logs as st ON (st.ref2_id = shop_transactions_logs.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions_logs.id AND group_id = ".SUPER_DISTRIBUTOR.") WHERE shop_transactions_logs.confirm_flag != 1 AND shop_transactions_logs.ref1_id = $r1 AND shop_transactions_logs.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions_logs.date >= '$date_from' AND shop_transactions_logs.date <= '$date_to'  $extra order by shop_transactions_logs.id desc";
                                                }
					}else{
                                                
                                                    $query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.type_flag,shop_transactions.note,st.amount as commission,shop_transactions.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join distributors  ON (distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND group_id = ".SUPER_DISTRIBUTOR.") WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.ref1_id = $r1 AND shop_transactions.ref2_id = $dist AND shop_transactions.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' $extra order by shop_transactions.id desc";
                                                if($page_old == "old_csv"){
                                                    $query_old = "SELECT shop_transactions_logs.id,shop_transactions_logs.amount,shop_transactions_logs.type,shop_transactions_logs.type_flag,shop_transactions_logs.note,st.amount as commission,shop_transactions_logs.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions_logs inner join distributors  ON (distributors.id = shop_transactions_logs.ref2_id) left join shop_transactions_logs as st ON (st.ref2_id = shop_transactions_logs.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions_logs.id AND group_id = ".SUPER_DISTRIBUTOR.") WHERE shop_transactions_logs.confirm_flag != 1 AND shop_transactions_logs.ref1_id = $r1 AND shop_transactions_logs.ref2_id = $dist AND shop_transactions_logs.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions_logs.date >= '$date_from' AND shop_transactions_logs.date <= '$date_to' $extra order by shop_transactions_logs.id desc";    
                                                }
                                        }
                                        //exit;
				}

				$transactions = $this->Slaves->query($query);
                if(!empty($query_old))$transactions_old = $this->Slaves->query($query_old);
				$this->set(compact('date_from','date_to'));
			}
			else {
				$transactions = array();
                $transactions_old = array();
			}
		}
		else {
			$transactions = array();
                        $transactions_old = array();
		}
		
                
        if($pageType != "csv"){
			$this->set('transactions',$transactions);
                        if($date == null) $this->set('empty',0);
		}else{
			
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			//"v.company,v.shortForm, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile, va.ref_code, va.amount, va.status, va.timestamp"
				
			//---------------------------
			//$line = array('Row','TransId','VendorTransId','Retailer Mobile', 'Shop', 'Vendor',  'Cust Mob','Operator','Circle','Amt','Comm','Status','Date','TypeStatus','Cause');
			$line = array("Transaction ID","Date",      "Party" , "Particulars","Transfer Type","Credit","Commission","Opening","Closing"); 
                        $csv->addRow($line);
			 
			$i=1;
				
			foreach($transactions as $transaction){
                                $id = $transaction['shop_transactions']['id'];
                                $timestamp = date('d-m-Y H:i:s', strtotime($transaction['shop_transactions']['timestamp']));
                                $company = $transaction['0']['company'];
                                $trans_type = $this->General->getTransferTypeName($transaction['shop_transactions']['type']);

                                $note = "";
                                if($transaction['shop_transactions']['type_flag'] == 1) 
                                    $note = 'Cash'; 
                                else if($transaction['shop_transactions']['type_flag'] == 2) 
                                    $note = 'NEFT'; 
                                else if($transaction['shop_transactions']['type_flag'] == 3) 
                                    $note = 'ATM Transfer'; 
                                else if($transaction['shop_transactions']['type_flag'] == 4) 
                                    $note = 'Cheque';
                                else if($transaction['shop_transactions']['type_flag'] == 5) 
                                    $note = 'Payment Gateway';

                                $note = $note . " - " . $transaction['shop_transactions']['note']; 

                                $amount = empty($transaction['shop_transactions']['amount']) ? 0 : $transaction['shop_transactions']['amount'];
                                $commission = empty($transaction['st']['commission']) ? 0 : $transaction['st']['commission'];
                                $opening = empty($transaction['opening_closing']['opening'])? 0 : $transaction['opening_closing']['opening'] ;
                                $closing = empty($transaction['opening_closing']['closing']) ? 0 : $transaction['opening_closing']['closing'];                               
                                      
                                
				$line = array($id ,$timestamp , $company , $note ,$trans_type ,$amount ,  $commission , $opening , $closing);
                        
				$csv->addRow($line);
				$i++;
			}
                        
                        
                        foreach($transactions_old as $transaction){
                                $id = $transaction['shop_transactions_logs']['id'];
                                $timestamp = date('d-m-Y H:i:s', strtotime($transaction['shop_transactions_logs']['timestamp']));
                                $company = $transaction['0']['company'];
                                $trans_type = $this->General->getTransferTypeName($transaction['shop_transactions_logs']['type']);

                                $note = "";
                                if($transaction['shop_transactions_logs']['type_flag'] == 1) 
                                    $note = 'Cash'; 
                                else if($transaction['shop_transactions_logs']['type_flag'] == 2) 
                                    $note = 'NEFT'; 
                                else if($transaction['shop_transactions_logs']['type_flag'] == 3) 
                                    $note = 'ATM Transfer'; 
                                else if($transaction['shop_transactions_logs']['type_flag'] == 4) 
                                    $note = 'Cheque';
                                else if($transaction['shop_transactions_logs']['type_flag'] == 5) 
                                    $note = 'Payment Gateway';     

                                $note = $note . " - " . $transaction['shop_transactions_logs']['note']; 

                                $amount = empty($transaction['shop_transactions_logs']['amount']) ? 0 : $transaction['shop_transactions_logs']['amount'];
                                $commission = empty($transaction['st']['commission']) ? 0 : $transaction['st']['commission'];
                                $opening = empty($transaction['opening_closing']['opening'])? 0 : $transaction['opening_closing']['opening'] ;
                                $closing = empty($transaction['opening_closing']['closing']) ? 0 : $transaction['opening_closing']['closing'];                               
                                      
                                
				$line = array($id ,$timestamp , $company , $note ,$trans_type ,$amount ,  $commission , $opening , $closing);
                        
				$csv->addRow($line);
				$i++;
			}
                        
                        $fileNamePre = "Transactions_";
                        /*if($page_old != "old_csv"){
                            $fileNamePre = "Archieve_Transactions_";
                        }else{
                            $fileNamePre = "Transactions_";
                        }*/
			echo $csv->render($fileNamePre.$date_from."_".$date_to.".csv");

		}
                
                $this->set('pageType',$pageType);
		$this->render('buy_dist_report');
	}

	function saleReport($date = null,$id=null){
		$api = false;
		$service_id = null;
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			if(isset($params['id']))$id = $params['id'];
			if(isset($params['service']))$service_id = $params['service'];
		}
		if(!isset($_SESSION['Auth']['User']))$this->redirect(array('action' => 'index'));
		$show = true;

		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
			}
		}
		else {
			$date_from = date('Y-m-d');
			$date_to = date('Y-m-d');
		}
		$this->set('date_from',$date_from);
		$this->set('date_to',$date_to);

		$cond_id = '';
		if($id != null){
			$cond_id = " AND st3.ref1_id = $id";
			$this->set('id',$id);
		}
		if($show){
			$cond = 'AND st1.date >= "'.$date_from.'" AND st1.date <= "' .$date_to . '"';

			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
				$query = "SELECT products.name,products.id,count(st1.id) as counts,sum(st1.amount) as amount, sum(st2.amount-st3.amount) as income FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_SUPERDISTRIBUTOR." AND st2.ref1_id = ".$_SESSION['Auth']['id'].") INNER JOIN shop_transactions as st3 ON (st3.ref2_id = st1.id $cond_id AND st3.type = ".COMMISSION_DISTRIBUTOR.") INNER JOIN products ON (products.id = st1.ref2_id) WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION;
			}
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
				$query = "SELECT products.name,products.id,count(st1.id) as counts,sum(st1.amount) as amount FROM shop_transactions as st1 INNER JOIN products ON (products.id = st1.ref2_id) INNER JOIN retailers ON (retailers.id = st1.ref1_id AND retailers.parent_id = ".$this->info['id'].") WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION;
			}
			else if($this->Session->read('Auth.User.group_id') == RETAILER){
				//$query = "SELECT products.name,products.id,count(vd.id) as counts,sum(vd.amount) as amount, sum(st1.amount) as income FROM vendors_activations as vd INNER JOIN shop_transactions as st1 ON (st1.ref2_id = vd.shop_transaction_id AND st1.type = ".COMMISSION_RETAILER.") INNER JOIN products ON (products.id = vd.product_id) WHERE  vd.retailer_id = ".$_SESSION['Auth']['id']." AND vd.status != 2 AND vd.status != 3";
				$query = "SELECT products.name,products.id,count(va.id) as counts,sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income FROM vendors_activations as va INNER JOIN products ON (products.id = va.product_id) INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id']." AND va.status != 2 AND va.status != 3";
				$cond = 'AND va.date >= "'.$date_from.'" AND va.date <= "' .$date_to . '"';
			}
			$products = array();
			if($service_id != null || !empty($service_id)){
				$services = $this->Slaves->query("SELECT services.id,services.name FROM services WHERE services.id = $service_id");
			}
			else {
				$services = $this->Slaves->query("SELECT services.id,services.name FROM services order by id");
			}

			foreach($services as $service){
				$service_id = $service['services']['id'];
				$query1 = $query . " AND service_id = $service_id $cond group by products.id";
				$data = $this->Slaves->query($query1);
				if(!empty($data)){
					$products[$service_id]['data'] = $data;
					$products[$service_id]['name'] = $service['services']['name'];
				}
			}
		}
		else {
			$products = array();
		}
		if(!$api)
		$this->set('products',$products);
		else return $products;
	}

	function investmentReport($from=null,$to=null,$vendorId=null){
		if(!isset($from))
		$from=date('d-m-Y',strtotime('-7 days'));
		if(!isset($to))
		$to=date('d-m-Y');
			
		if(empty($vendorId))$vendorId = 0;
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
			
		//for vendors dropdown
		$vendorResult=$this->Shop->getVendors();
		$this->set('vendors',$vendorResult);
		$this->set('id',$vendorId);

		if($vendorId==0){
			$result=$this->Slaves->query("SELECT earnings_logs.*,vendors.company,vendors.update_flag,sum(devices_data.inc) as inc FROM earnings_logs inner join  vendors ON (vendors.id = earnings_logs.vendor_id) left join devices_data ON (earnings_logs.date = devices_data.sync_date  and devices_data.vendor_id = earnings_logs.vendor_id ) where  date >= '".$fd."' and date <= '".$ft."'  group by vendors.id,date order by date desc,vendor_id");
			
		}else{
			$result=$this->Slaves->query("SELECT earnings_logs.*,vendors.company,vendors.update_flag,sum(devices_data.inc) as inc FROM earnings_logs inner join  vendors ON (vendors.id = earnings_logs.vendor_id) left join devices_data ON (earnings_logs.date = devices_data.sync_date  and devices_data.vendor_id = earnings_logs.vendor_id ) where  date >= '".$fd."' and date <= '".$ft."' and   earnings_logs.vendor_id = $vendorId  group by vendors.id,date order by date desc,vendor_id");
		}
		
		$data = array();
		foreach($result as $res){
			$data[$res['earnings_logs']['date']][] = $res;
		}
		
		$this->set('data',$data);
	}
	
	function addInvestmentEntry(){
		$vendor_id = $_POST['vendor_id'];
		$date = $_POST['date'];
		if($vendor_id && $date){
			$fdarr = explode("-", $date);
			$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
			$earnings_logs = $this->User->query("select * from earnings_logs
					where vendor_id = '$vendor_id' and date = '$fd'");
			if(empty($earnings_logs)){
				if($this->User->query("insert into earnings_logs (vendor_id, date)
						values ('$vendor_id', '$fd')"))
					echo "done";	
					exit;	
			}
		}
		$this->autoRender = false;
	}
	
	function floatReport($from=null,$to=null){
		
		if(!isset($from))
		$from=date('d-m-Y',strtotime('-30 days'));
		if(!isset($to))
		$to=date('d-m-Y');
			
		if(empty($vendorId))$vendorId = 0;
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$td = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];

		$fd_old = date('Y-m-d',strtotime($fd . ' -1 days'));
		
		$this->set('from',$from);
		$this->set('to',$to);

		$result1=$this->Slaves->query("SELECT float_logs.* FROM float_logs WHERE date >= '".$fd_old."' AND date <= '".$td."' AND hour =24 order by date desc");
		$result2=$this->Slaves->query("SELECT sum(amount) as amt,date FROM refunds WHERE date >= '".$fd."' AND date <= '".$td."' group by date order by date desc");
		$result3=$this->Slaves->query("SELECT sum(rental) as amt,date FROM rentals,retailers WHERE retailers.id = rentals.retailer_id AND date >= '".$fd."' AND date <= '".$td."' AND balance >= 0 group by date order by date desc");
		
		$result4=$this->Slaves->query("SELECT sum(amount) as amt,date FROM `shop_transactions` where type = 2 AND ref1_id = 0 AND date >= '".$fd."' AND date <= '".$td."' group by date");
		$result5=$this->Slaves->query("SELECT SUM(if(confirm_flag = 1 AND type = 4,amount,0)) as amt, SUM(if(type_flag = 1 AND type = 11 ,amount,0)) as reversal,date FROM shop_transactions USE INDEX ( ref1_type ) WHERE date >= '".$fd."' AND date <= '".$td."' AND ref1_id = 13 AND type in (4,11) group by date");
		
		$data = array();
		foreach($result1 as $res){
			$data[$res['float_logs']['date']]['closing'] = $res['float_logs']['float'];
			$data[date('Y-m-d',strtotime($res['float_logs']['date'].' + 1 days'))]['opening'] = $res['float_logs']['float'];
			$data[$res['float_logs']['date']]['sale'] = $res['float_logs']['sale'];
			$data[$res['float_logs']['date']]['transferred'] = $res['float_logs']['transferred'];
			$data[$res['float_logs']['date']]['commission'] = $res['float_logs']['commissions'];
			$data[$res['float_logs']['date']]['reversals'] = $res['float_logs']['old_reversals'];
		}
		
		//unset($data[$fd_old]);
		
		foreach($result2 as $res){
			$data[$res['refunds']['date']]['refund'] = $res['0']['amt'];
		}
		
		foreach($result3 as $res){
			$data[$res['rentals']['date']]['rental'] = $res['0']['amt'];
		}

		foreach($result4 as $res){
			$data[$res['shop_transactions']['date']]['adjusted'] = $res['0']['amt'];
		}
		
		foreach($result5 as $res){
			$data[$res['shop_transactions']['date']]['b2c_topup'] = $res['0']['amt'] - $res['0']['reversal'];
		}
		
		$this->set('data',$data);
	}
	
	function addInvestedAmount(){
		$id = trim($_REQUEST['id']);
		$amount = empty($_REQUEST['amount'])? 0 : $_REQUEST['amount'];
		$opening = empty($_REQUEST['opening'])? 0 : $_REQUEST['opening'];
		$closing = empty($_REQUEST['closing'])? 0 : $_REQUEST['closing'];
		$comment = empty($_REQUEST['comment'])? "" : $_REQUEST['comment'];
		$max_date = date('Y-m-d',strtotime('- 180 days'));
		
		$this->Retailer->query("UPDATE earnings_logs 
				SET invested='$amount',opening='$opening',closing='$closing', comment='$comment' 
				WHERE id = $id AND date >= '$max_date'");

		echo 'done';
		$this->autoRender = false;
	}

	function earningReport($from=null,$to=null){
		if(!isset($from))
		$from=date('d-m-Y',strtotime('-7 days'));
		if(!isset($to))
		$to=date('d-m-Y');
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
			
		$result=$this->Slaves->query("SELECT sum(sale) as sale,sum(invested) as invested,sum(expected_earning) as expected_earning,sum(if(vendors.id != 56,sale+closing-opening-invested,0)) as earning,sum(if(vendors.update_flag=0,old_reversal,0)) as reversal,date FROM earnings_logs left join vendors ON (vendors.id = vendor_id) WHERE closing is not null AND date >= '".$fd."' AND date <= '".$ft."' group by date order by date desc");
			
		$data = array();
		foreach($result as $res){
			$data[$res['earnings_logs']['date']]['sale'] = $res['0']['sale'];
			$data[$res['earnings_logs']['date']]['invested'] = $res['0']['invested'];
			$data[$res['earnings_logs']['date']]['earning'] = $res['0']['earning'];
			$data[$res['earnings_logs']['date']]['expected_earning'] = $res['0']['expected_earning'];
			$data[$res['earnings_logs']['date']]['reversal'] = $res['0']['reversal'];
		}
			
		$extra_ret=$this->Slaves->query("SELECT sum(earning) as earning,date FROM retailers_logs WHERE date >= '".$fd."' AND date <= '".$ft."' group by date");
		$extra_dist=$this->Slaves->query("SELECT sum(distributors_logs.earning) as earning,distributors_logs.date FROM distributors_logs,distributors WHERE distributors.id = distributors_logs.distributor_id AND distributors.parent_id = 3 AND distributors_logs.date >= '".$fd."' AND distributors_logs.date <= '".$ft."' group by distributors_logs.date");
		$extra_superdist=$this->Slaves->query("SELECT sum(shop_transactions.amount) as earning,shop_transactions.date FROM shop_transactions WHERE shop_transactions.type = ".COMMISSION_SUPERDISTRIBUTOR." AND confirm_flag != 1 AND shop_transactions.date >= '".$fd."' AND shop_transactions.date <= '".$ft."' group by shop_transactions.date");
			
		$result2=$this->Slaves->query("SELECT sum(amount) as amt,date FROM refunds WHERE date >= '".$fd."' AND date <= '".$ft."' group by date");
		
		foreach($extra_ret as $ext){
			if(isset($data[$ext['retailers_logs']['date']]))
			$data[$ext['retailers_logs']['date']]['retailer_earning'] = $ext['0']['earning'];
		}
		foreach($extra_dist as $ext){
			if(isset($data[$ext['distributors_logs']['date']]))
			$data[$ext['distributors_logs']['date']]['distributor_earning'] = $ext['0']['earning'];
		}
		foreach($extra_superdist as $ext){
			if(isset($data[$ext['shop_transactions']['date']]))
			$data[$ext['shop_transactions']['date']]['sdistributor_earning'] = $ext['0']['earning'];
		}
		foreach($result2 as $ext){
			if(isset($data[$ext['refunds']['date']]))
			$data[$ext['refunds']['date']]['refunds'] = $ext['0']['amt'];
		}

		$this->set('data',$data);
	}

	function salesmanReport($from=null,$to=null,$salesmanId=null,$retailerId=null)
	{	
        $pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
        $page_old = empty($_GET['old_data']) ? "" : $_GET['old_data'];
        $this->set('pageType',$pageType);
        if(!in_array($this->Session->read('Auth.User.group_id'), array(DISTRIBUTOR, SALESMAN))){
			$this->redirect(array('action' => 'index'));
		}
		//echo "Start SSM".$salesmanMobile;
		if(!isset($from))
		$from=date('d-m-Y');
		if(!isset($to))
		$to=date('d-m-Y');
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
        
        //for salesman dropdown
		$salesmanResult=$this->Slaves->query("select * from salesmen where dist_id = ".$this->info['id']." AND active_flag = 1");
		$this->set('salesmans',$salesmanResult);

		//for retailers dropDown
		//$retailerResult=$this->User->query("select * from retailers where parent_id = ".$this->info['id']);
		//echo "select * from retailers where parent_id = ".$this->info['id'];
		$this->set('retailers',$this->retailers);
			
		//for table in salesman Reports
		//	echo "Sales Mobile= ".$salesmanMobile;
		$this->set('id',$salesmanId);
		$this->set('rid',$retailerId);
		$salesMenCond = "";
		if(!empty($salesmanId)){
			$salesMenCond = "salesmen.id = $salesmanId";
		}else{
			$salesMenCond = 1;
		}

		if(!empty($retailerId)){
			$retailerCond = "r.id = $retailerId";
		}else{
			$retailerCond = 1;
		}
		
		if($_POST['request_from'] == "distributorApp"){
			if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
				$distributor_id = $this->Session->read('Auth.id');
			}
			else {
				$distributor_id = $this->Session->read('Auth.dist_id');
			}
			$salesResult = $this->Slaves->query("
                select r.id,r.name,r.mobile,ur.shopname,st.amount,sst.id,st.id,st.note,st.type_flag,sst.created,salesmen.name,
				oc.opening,oc.closing,salesmen.id,sst.closing,(sst.closing + st.amount) as s_opening
                from shop_transactions st
                left join opening_closing oc on(oc.shop_transaction_id=st.id AND oc.shop_id = st.ref1_id
                AND oc.group_id = ".DISTRIBUTOR.")
                left join salesman_transactions sst on (st.id=sst.shop_tran_id) 
                left join salesmen ON (salesmen.id = sst.salesman) 
                left join   retailers r on(r.id=st.ref2_id)  
				left join unverified_retailers ur on ur.retailer_id = r.id	
                where 
                        $salesMenCond AND
                        $retailerCond AND
                st.ref1_id = ".$distributor_id." AND 
                st.confirm_flag != 1 AND 
                st.type = ".DIST_RETL_BALANCE_TRANSFER." AND 
                st.date between '".$fd."' and '".$ft."' 
                order by st.timestamp desc");

			$report = array();
			foreach($salesResult as $sr){
				$note = "";
                if($transaction['st']['type_flag'] == 1) 
                	$note = 'Cash'; 
                else if($transaction['st']['type_flag'] == 2) 
                    $note = 'NEFT'; 
                else if($transaction['st']['type_flag'] == 3) 
                    $note = 'ATM Transfer'; 
                else if($transaction['st']['type_flag'] == 4) 
                    $note = 'Cheque';
                else if($transaction['st']['type_flag'] == 5) 
                    $note = 'Payment Gateway';
                $note .= " - " . $sr['st']['note'];
                if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
                	$opening = $sr['oc']['opening'];
                	$closing = $sr['oc']['closing'];
                }
                else {
                	$opening = $sr['0']['s_opening'];
                	$closing = $sr['sst']['closing'];
                }
                
				$report[] = array(
					"s_t_id" 	=> $sr['st']['id'],
					"a"			=> $sr['st']['amount'],
					"o"			=> $opening,
					"c"			=> $closing,
					"t"			=> $sr['sst']['created'],
					"n"			=> $note,
					"r_id"		=> $sr['r']['id'],
					"r_m"		=> $sr['r']['mobile'],
					"r_sn"		=> $sr['ur']['shopname'],	
					"s_id"		=> $sr['salemen']['id'],
					"sm_t_id"	=> $sr['sst']['id']						
				);
			}
			return array("status" => "success", "description" => $report);
		}
		else {
			$salesResult = $this->Slaves->query("
                select r.name,r.mobile,r.shopname,st.amount,sst.id,st.id,st.note,st.type_flag,sst.created,salesmen.name,oc.opening,oc.closing
                from shop_transactions st
                left join opening_closing oc on(oc.shop_transaction_id=st.id AND oc.shop_id = st.ref1_id
                AND oc.group_id = ".DISTRIBUTOR.")
                left join salesman_transactions sst on (st.id=sst.shop_tran_id) 
                left join salesmen ON (salesmen.id = sst.salesman) 
                left join   retailers r on(r.id=st.ref2_id)  
                where 
                        $salesMenCond AND
                        $retailerCond AND
                st.ref1_id = ".$this->info['id']." AND 
                st.confirm_flag != 1 AND 
                st.type = ".DIST_RETL_BALANCE_TRANSFER." AND 
                st.date between '".$fd."' and '".$ft."' 
                order by st.timestamp desc");
		
		}
        if($page_old == "old_csv"){
            $query_old = "
                            select r.name,r.mobile,r.shopname,st.amount,sst.id,st.id,st.note,st.type_flag,sst.created,salesmen.name,oc.opening,oc.closing
                            from shop_transactions_logs st
                            left join opening_closing oc on(oc.shop_transaction_id=st.id AND oc.shop_id = st.ref1_id
                            AND oc.group_id = ".DISTRIBUTOR.")
                            left join salesman_transactions sst on (st.id=sst.shop_tran_id) 
                            left join salesmen ON (salesmen.id = sst.salesman) 
                            left join retailers r on(r.id=st.ref2_id)  
                            where 
                                    $salesMenCond AND
                                    $retailerCond AND
                            st.ref1_id = ".$this->info['id']." AND 
                            st.confirm_flag != 1 AND 
                            st.type = ".DIST_RETL_BALANCE_TRANSFER." AND 
                            st.date between '".$fd."' and '".$ft."' 
                            order by st.timestamp desc
                        ";
        }
        
                           
		
        if($pageType != "csv"){
            $this->set('salesResult',$salesResult);
        }else{
            App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			
            $line = array("Transaction ID","Salesman","Retailer" , "Retailer Mobile","Note","Amount","Opening","Closing","Time"); 
            $csv->addRow($line);
			$i=1;
            foreach($salesResult as $transaction){
                 $TransactionID = $transaction['st']['id'];
                 $Salesman = $transaction['salesmen']['name'];
                 $Retailer = $transaction['r']['shopname'];
                 $RetailerMobile = $transaction['r']['mobile'];
                 $Note = "";
                 if($transaction['st']['type_flag'] == 1) 
                     $Note = 'Cash'; 
                 else if($transaction['st']['type_flag'] == 2) 
                     $Note = 'NEFT'; 
                 else if($transaction['st']['type_flag'] == 3) 
                     $Note = 'ATM Transfer'; 
                 else if($transaction['st']['type_flag'] == 4) 
                     $Note = 'Cheque';
                 else if($transaction['st']['type_flag'] == 5) 
                     $Note = 'Payment Gateway';

                 $Note .= " - " . $transaction['st']['note'];     
                 
                 $Amount = $transaction['st']['amount'];
                 $Opening = $transaction['oc']['opening'];
                 $Closing = $transaction['oc']['closing'];
                 $Time = $transaction['sst']['created'];
                
                $line = array($TransactionID,$Salesman,$Retailer,$RetailerMobile,$Note,$Amount,$Opening,$Closing,$Time);
                $csv->addRow($line);
                $i++;                
            }
            if(!empty($query_old)){
                $salesResult_old = $this->Slaves->query($query_old);
                foreach($salesResult_old as $transaction){
                     $TransactionID = $transaction['st']['id'];
                     $Salesman = $transaction['salesmen']['name'];
                     $Retailer = $transaction['r']['shopname'];
                     $RetailerMobile = $transaction['r']['mobile'];
                     $Note = "";
                     if($transaction['st']['type_flag'] == 1) 
                         $Note = 'Cash'; 
                     else if($transaction['st']['type_flag'] == 2) 
                         $Note = 'NEFT'; 
                     else if($transaction['st']['type_flag'] == 3) 
                         $Note = 'ATM Transfer'; 
                     else if($transaction['st']['type_flag'] == 4) 
                         $Note = 'Cheque';
                     else if($transaction['st']['type_flag'] == 5) 
                     	 $Note = 'Payment Gateway';

                 	 $Note .= " - " . $transaction['st']['note'];     

                     $Amount = $transaction['st']['amount'];
                     $Opening = $transaction['oc']['opening'];
                     $Closing = $transaction['oc']['closing'];
                     $Time = $transaction['sst']['created'];

                    $line = array($TransactionID,$Salesman,$Retailer,$RetailerMobile,$Note,$Amount,$Opening,$Closing,$Time);
                    $csv->addRow($line);
                    $i++;                
                }
            }
            $fileNamePre = "Transactions_";
            echo $csv->render($fileNamePre.$fd."_".$ft.".csv");
        }
		
	}

	function pullback($params){
		if(!in_array($this->Session->read('Auth.User.group_id'), array(ADMIN, SUPER_DISTRIBUTOR, DISTRIBUTOR, SALESMAN))){
			$this->redirect(array('action' => 'index'));
		}
		$MsgTemplate = $this->General->LoadApiBalance();
		if(in_array($this->Session->read('Auth.User.group_id'), array(DISTRIBUTOR, SALESMAN))){
			if(isset($params) && $params['request_from'] == "distributorApp"){
				$authData = $this->Session->read('Auth');
				if($authData['User']['group_id'] == SALESMAN){
					$salesman = $authData;
					$authData = $this->Shop->getShopDataById($salesman['dist_id'], DISTRIBUTOR);
					$authData['User']['group_id'] = SALESMAN;
					$authData['User']['mobile'] = $salesman['mobile'];
					
					$this->info = $authData;
				}
				else
					$this->info = $this->Session->read('Auth');
			}
			$salesman_trans_id = isset($params['salesman_transid']) ? $params['salesman_transid'] : $_REQUEST['salesman_transid'];
			$shop_transid = isset($params['shop_transid']) ? $params['shop_transid'] : $_REQUEST['shop_transid'];
			
			$data = $this->Shop->getMemcache("pullback$salesman_trans_id");
			if($data == null)$this->Shop->setMemcache("pullback$salesman_trans_id",1,2*60);
			else {
				if(isset($params) && $params['request_from'] == "distributorApp"){
					return array("status" => "failure", "description" => "Cannot be pulled back right now. Try again after some time");
				}
				else
					echo "What??Cannot be pulled back right now";exit;
			}
			
			$salesmanResult=$this->User->query("SELECT shop_transactions.date,shop_transactions.timestamp,shop_transactions.ref2_id,
					shop_transactions.confirm_flag,shop_transactions.amount,salesman_transactions.id,
					salesman_transactions.shop_tran_id,salesman_transactions.created,salesman_transactions.salesman 
					FROM salesman_transactions,shop_transactions,salesmen 
					WHERE salesmen.dist_id = ".$this->info['id']." 
					AND salesman_transactions.salesman = salesmen.id 
					AND payment_type = 2 
					AND salesman_transactions.id = $salesman_trans_id 
					AND salesman_transactions.shop_tran_id = shop_transactions.id 
					AND shop_transactions.id = " . $shop_transid);
			
			$success = false;
			$msg = "";
			if(!empty($salesmanResult)){
				$retid = $salesmanResult['0']['shop_transactions']['ref2_id'];
				$salesmanid = $salesmanResult['0']['salesman_transactions']['salesman'];
				$shopid = $salesmanResult['0']['salesman_transactions']['shop_tran_id'];
				$trans_date = $salesmanResult['0']['shop_transactions']['date'];
				
				$confirm_flag = $salesmanResult['0']['shop_transactions']['confirm_flag'];
				$amt = $salesmanResult['0']['shop_transactions']['amount'];
				
				if($confirm_flag == 1){
					if(isset($params) && $params['request_from'] == "distributorApp"){
						return array("status" => "failure", "description" => "Already pulled back your amount");
					}
					else
						echo "Already pulled back your amount";exit;
				}
				$shopResult=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND ref2_id = $retid AND type = ".DIST_RETL_BALANCE_TRANSFER." ORDER BY id desc limit 1");

				if ($shopResult['0']['shop_transactions']['id'] == $shopid || $confirm_flag > 1){
					//$amt = $shopResult['0']['shop_transactions']['amount'];

					$retResult=$this->User->query("SELECT retailers.balance, retailers.mobile, retailers.shopname, ur.shopname 
							FROM retailers 
							left join unverified_retailers ur on ur.retailer_id = retailers.id
							WHERE retailers.id = $retid 
							AND retailers.parent_id = " . $this->info['id']);

					if(!empty($retResult)){
						if($retResult['0']['retailers']['balance'] >= $amt || $confirm_flag > 2){
							if($confirm_flag == 4 && $retResult['0']['retailers']['balance'] < $amt){
								$amt = $retResult['0']['retailers']['balance'];
							}
							$success = true;
							
							$bal_ret = $this->Shop->shopBalanceUpdate($amt,'subtract',$retid,RETAILER);
							$bal_dis = $this->Shop->shopBalanceUpdate($amt,'add',$this->info['id'],DISTRIBUTOR);
				
							$this->User->query("UPDATE salesmen SET balance = balance + $amt WHERE id = $salesmanid");

							$trans_id = $this->Shop->shopTransactionUpdate(PULLBACK_RETAILER,$amt,$retid,$shopid,$this->info['user_id']);
							$this->Shop->addOpeningClosing($retid,RETAILER,$trans_id,$bal_ret+$amt,$bal_ret);
							$this->Shop->addOpeningClosing($this->info['id'],DISTRIBUTOR,$trans_id,$bal_dis-$amt,$bal_dis);
							
							$this->User->query("UPDATE shop_transactions SET confirm_flag=1 WHERE id = $shopid");
							$this->User->query("UPDATE distributors_logs SET topup_sold = topup_sold - $amt WHERE distributor_id = " . $this->info['id']. " AND date = '$trans_date'");
							$this->User->query("UPDATE retailers_logs SET topup = topup - $amt WHERE retailer_id = $retid AND date = '$trans_date'");
							
							//$this->User->query("DELETE FROM salesman_transactions WHERE id = $salesman_trans_id");

							//$this->User->query("INSERT INTO pullbacks (salesman_id,retailer_id,distributor_id,amount,topup_time,pullback_time) VALUES ($salesmanid,$retid,".$this->info['id'].",$amt,'".$salesmanResult['0']['shop_transactions']['timestamp']."','".date('Y-m-d H:i:s')."')");
							$salesData = $this->Slaves->query("SELECT mobile, balance FROM salesmen WHERE id = $salesmanid");
                                                        
                                                        $paramdata['AMOUNT'] = $amt;
                                                        $paramdata['BALANCE'] = $retResult['0']['retailers']['balance'] - $amt;

                                                        
                                                        $content =  $MsgTemplate['Pullback_Retailer_MSG'];
                                                        $ret_msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        $this->General->sendMessage($retResult['0']['retailers']['mobile'],$ret_msg,'notify');
                                                        
//							$this->General->sendMessage($retResult['0']['retailers']['mobile'],"Dear Retailer, Rs $amt is pulled back from your account by your distributor. Your balance is now Rs " . ($retResult['0']['retailers']['balance'] - $amt),'notify');
							
                                                        $paramdata['AMOUNT'] = $amt;
                                                        $paramdata['SHOP_NAME'] = $retResult['0']['retailers']['shopname'];
                                                        $paramdata['MOBILE_NUMBER'] = $retResult['0']['retailers']['mobile'];

                                                        $content =  $MsgTemplate['Pullback_Salesmen_MSG'];
                                                        $saleman_msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        $this->General->sendMessage($salesData['0']['salesmen']['mobile'],$saleman_msg,'notify', ($salesData[0]['salesmen']['balance']),SALESMAN);
                                                        
//                                                      $this->General->sendMessage($salesData['0']['salesmen']['mobile'],"Dear Salesman, Rs $amt is pulled back from retailer ".$retResult['0']['retailers']['shopname']." (".$retResult['0']['retailers']['mobile'].")",'notify', ($salesData[0]['salesmen']['balance']),SALESMAN);
							
                                                        
                                                        if($bal_ret < 0){
								$this->General->sendMails("Retailer balance is negative after pullback", "Retailer: ".$retResult['0']['retailers']['mobile']."<br/>Balance: $bal_ret",array('tadka@mindsarray.com','limits@mindsarray.com'),'mail');
								
							}
							if(isset($params) && $params['request_from'] == "distributorApp"){
								if($authData['User']['group_id'] == SALESMAN){
									$balance = $salesData['0']['salesmen']['balance'];
								}
								else
									$balance = $bal_dis;
								return array("status" => "success", "description" => "Done", "balance" => $balance);
							}
							else
								echo "success";
						}
						else{
							if(isset($params) && $params['request_from'] == "distributorApp"){
								return array("status" => "failure", "description" => "Retailer balance is less than $amt");
							}
							else{
								$msg = "Retailer balance is less than $amt";
								echo $msg;
							}	
						}
					}
					else {
						if(isset($params) && $params['request_from'] == "distributorApp"){
							return array("status" => "failure", "description" => "Retailer does not exist");
						}
						else{
							$msg = "Retailer does not exists";
							echo $msg;
						}	
					}
				}
				else {
					if(isset($params) && $params['request_from'] == "distributorApp"){
						return array("status" => "failure", "description" => "Only last topup can be pulled back");
					}
					else{
						$msg = "Only last topup can be pulled back";
						echo $msg;
					}	
				}
			}
			else {
				if(isset($params) && $params['request_from'] == "distributorApp"){
					return array("status" => "failure", "description" => "Only last topup can be pulled back");
				}
				else{
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}	
			}
		}
		else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			$shop_trans_id = $_REQUEST['shop_transid'];
			$data = $this->Shop->getMemcache("pullback$shop_trans_id");
			if($data == null)$this->Shop->setMemcache("pullback$shop_trans_id",1,2*60);
			else {
				echo "Cannot be pulled back right now";exit;
			}
			
			$shopResult=$this->User->query("SELECT shop_transactions.* FROM shop_transactions WHERE id = $shop_trans_id AND type = " .SDIST_DIST_BALANCE_TRANSFER);

			if(!empty($shopResult)){
				$amt = $shopResult['0']['shop_transactions']['amount'];
				$distid = $shopResult['0']['shop_transactions']['ref2_id'];
				$confirm_flag = $shopResult['0']['shop_transactions']['confirm_flag'];
				$trans_date = $shopResult['0']['shop_transactions']['date'];
				
				if($confirm_flag == 1){
					echo "Already pulled back your amount";exit;
				}
				
				$shopResult1=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND ref2_id = $distid AND type = ".SDIST_DIST_BALANCE_TRANSFER." ORDER BY id desc limit 1");
				if ($shopResult1['0']['shop_transactions']['id'] == $shop_trans_id || $confirm_flag > 1){
					$comm=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_DISTRIBUTOR);
					if(!empty($comm)){
						$amt += $comm['0']['shop_transactions']['amount'];
					}
					$distResult=$this->User->query("SELECT distributors.balance,users.mobile,distributors.company FROM distributors,users WHERE distributors.user_id = users.id AND distributors.id = $distid AND parent_id = " . $this->info['id']);

					if(!empty($distResult)){
						if($distResult['0']['distributors']['balance'] >= $amt || $confirm_flag > 2){
							if($confirm_flag == 4 && $distResult['0']['distributors']['balance'] < $amt){
								$amt = $distResult['0']['distributors']['balance'];
							}
							$success = true;
							$bal_dis = $this->Shop->shopBalanceUpdate($amt,'subtract',$distid,DISTRIBUTOR);
							$bal_sdis = $this->Shop->shopBalanceUpdate($amt,'add',$this->info['id'],SUPER_DISTRIBUTOR);
				
							$trans_id = $this->Shop->shopTransactionUpdate(PULLBACK_DISTRIBUTOR,$amt,$distid,$shop_trans_id,$this->Session->read('Auth.User.id'));
							$this->Shop->addOpeningClosing($distid,DISTRIBUTOR,$trans_id,$bal_dis+$amt,$bal_dis);
							$this->Shop->addOpeningClosing($this->info['id'],SUPER_DISTRIBUTOR,$trans_id,$bal_sdis-$amt,$bal_sdis);
							
							$this->User->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE id = $shop_trans_id");
							$this->User->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_DISTRIBUTOR);

							$this->User->query("UPDATE distributors_logs SET topup_buy = topup_buy - $amt,earning=earning-".$comm['0']['shop_transactions']['amount']." WHERE distributor_id = $distid AND date = '$trans_date'");
							
							//$this->User->query("INSERT INTO pullbacks (salesman_id,retailer_id,distributor_id,amount,topup_time,pullback_time) VALUES ($salesmanid,$retid,".$this->info['id'].",$amt,'".$shopResult['0']['shop_transactions']['timestamp']."','".date('Y-m-d H:i:s')."')");
							
                                                        $paramdata['USER'] = 'Distributor';
                                                        $paramdata['AMOUNT'] = $amt;
                                                        $paramdata['BALANCE'] = $distResult['0']['distributors']['balance'] - $amt;
                                                        $content =  $MsgTemplate['Pullback_Distributor_MSG'];
                                                        $dist_msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        $this->General->sendMessage($distResult['0']['users']['mobile'],$dist_msg,'notify',null,DISTRIBUTOR);
                                                        
//                                                    	$this->General->sendMessage($distResult['0']['users']['mobile'],"Dear Distributor, Rs $amt is pulled back from your account. Your balance is now Rs " . ($distResult['0']['distributors']['balance'] - $amt),'notify',null,DISTRIBUTOR);

							if($bal_dis < 0){
								$this->General->sendMails("Distributor balance is negative after pullback", "Distributor: ".$distResult['0']['distributors']['company']."<br/>Balance: $bal_dis",array('tadka@mindsarray.com','limits@mindsarray.com'),'mail');
							}
							echo "success";
						}
						else{
							$msg = "Distributor balance is less than $amt";
							echo $msg;
						}
					}
				}
				else {
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}
			}
		}
		else if($this->Session->read('Auth.User.group_id') == ADMIN){
			
			$shop_trans_id = $_REQUEST['shop_transid'];
			$data = $this->Shop->getMemcache("pullback$shop_trans_id");
			if($data == null)$this->Shop->setMemcache("pullback$shop_trans_id",1,2*60);
			else {
				echo "Cannot be pulled back right now";exit;
			}
			
			
			$shopResult=$this->User->query("SELECT shop_transactions.* FROM shop_transactions WHERE id = $shop_trans_id AND type = " .ADMIN_TRANSFER);

			if(!empty($shopResult)){
				$amt = $shopResult['0']['shop_transactions']['amount'];
				$sdistid = $shopResult['0']['shop_transactions']['ref2_id'];
				$confirm_flag = $shopResult['0']['shop_transactions']['confirm_flag'];
				
				if($confirm_flag == 1){
					echo "Already pulled back your amount";exit;
				}
					
				$shopResult1=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $sdistid AND type = ".ADMIN_TRANSFER." ORDER BY id desc limit 1");
				if ($shopResult1['0']['shop_transactions']['id'] == $shop_trans_id || $confirm_flag > 1){
					$comm=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_SUPERDISTRIBUTOR);
					if(!empty($comm)){
						$amt += $comm['0']['shop_transactions']['amount'];
					}
					$distResult=$this->User->query("SELECT super_distributors.balance,users.mobile,super_distributors.company FROM super_distributors,users WHERE super_distributors.user_id = users.id AND super_distributors.id = $sdistid");

					if(!empty($distResult)){
						if($distResult['0']['super_distributors']['balance'] >= $amt || $confirm_flag > 2){
							if($confirm_flag == 4 && $distResult['0']['super_distributors']['balance'] < $amt){
								$amt = $distResult['0']['super_distributors']['balance'];
							}
							$success = true;
							$bal_sdis = $this->Shop->shopBalanceUpdate($amt,'subtract',$sdistid,SUPER_DISTRIBUTOR);
				
							$trans_id = $this->Shop->shopTransactionUpdate(PULLBACK_SUPERDISTRIBUTOR,$amt,$sdistid,$shop_trans_id,$this->Session->read('Auth.User.id'));
							$this->Shop->addOpeningClosing($sdistid,SUPER_DISTRIBUTOR,$trans_id,$bal_sdis+$amt,$bal_sdis);
							
							$this->User->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE id = $shop_trans_id");
							$this->User->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_SUPERDISTRIBUTOR);
							
                                                        $paramdata['USER'] = 'Sir';
                                                        $paramdata['AMOUNT'] = $amt;
                                                        $paramdata['BALANCE'] = $distResult['0']['super_distributors']['balance'] - $amt;
                                                        $content =  $MsgTemplate['Pullback_Distributor_MSG'];
                                                        $sup_dist_msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        $this->General->sendMessage($distResult['0']['users']['mobile'],$sup_dist_msg,'shops');
                                                    	
//							$this->General->sendMessage($distResult['0']['users']['mobile'],"Dear Sir, Rs $amt is pulled back from your account. Your balance is now Rs " . ($distResult['0']['super_distributors']['balance'] - $amt),'shops');
							if($bal_sdis < 0){
								$this->General->sendMails("Super Distributor balance is negative after pullback", "Super Distributor: ".$distResult['0']['super_distributors']['company']."<br/>Balance: $bal_sdis",array('tadka@mindsarray.com','limits@mindsarray.com'),'mail');
							}
							echo "success";
						}
						else{
							$msg = "Super Distributor balance is less than $amt";
							echo $msg;
						}
					}
				}
				else {
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}
			}
		}

		if(!$success && empty($msg)){
			if(isset($params) && $params['request_from'] == "distributorApp"){
				return array("status" => "success", "description" => "Cannot be pulled back");
			}
			else
				echo "Cannot be pulled back";
		}
		$this->autoRender = false;
	}

	function topupRequests(){//for distributor
		$data = $this->Slaves->query("SELECT topup_request.*,retailers.name,retailers.id FROM topup_request,retailers where retailers.user_id =  topup_request.user_id AND retailers.parent_id = " . $_SESSION['Auth']['id']);
		$this->set('data',$data);
	}

	/*function approve($id){
		$data = $this->Slaves->query("SELECT topup_request.*,retailers.name,retailers.mobile,retailers.id,retailers.shopname,retailers.kyc_flag FROM topup_request,retailers where retailers.user_id =  topup_request.user_id AND retailers.parent_id = " . $_SESSION['Auth']['id'] . " AND topup_request.id=$id");
		$bal = $this->Shop->getBalance($_SESSION['Auth']['id'],DISTRIBUTOR);
		$to_save = true;
		if($data['0']['topup_request']['amount'] > $bal){
			$message = "Your amount cannot be greater than your account balance";
			$to_save = false;
		}
		else {
			if($data['0']['retailers']['kyc_flag'] == 0 && ($data['0']['topup_request']['amount'] + $bal) > KYC_AMOUNT){
				$message = "Please collect KYC of the retailer. Retailer balance cannot be greater than Rs." . KYC_AMOUNT;
				$to_save = false;
			}
		}
		if($to_save){
			$this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$data['0']['topup_request']['amount'],$_SESSION['Auth']['id'],$data['0']['retailers']['id']);
			$bal = $this->Shop->shopBalanceUpdate($data['0']['topup_request']['amount'],'subtract',$_SESSION['Auth']['id'],DISTRIBUTOR);
			$bal1 = $this->Shop->shopBalanceUpdate($data['0']['topup_request']['amount'],'add',$data['0']['retailers']['id'],RETAILER);
			$mail_subject = "Retailer Top-Up request approved";
			$mail_body = "Distributor: " . $_SESSION['Auth']['company']. " approved top-up of Rs. " . $data['0']['topup_request']['amount'] . " of retailer: " . $data['0']['retailers']['shopname'];

//			$msg = "Dear Retailer,\nYour account is successfully credited with Rs." . $data['0']['topup_request']['amount']. "\nYour current balance is Rs.$bal1";
			
                        $paramdata['TOPUP_AMOUNT'] = $data['0']['topup_request']['amount'];
                        $paramdata['BALANCE'] = $bal1;
                        $MsgTemplate = $this->General->LoadApiBalance();
                        $content =  $MsgTemplate['Reatiler_Approve_MSG'];
                        $msg = $this->General->ReplaceMultiWord($paramdata,$content);
                        
                        $this->General->sendMessage($data['0']['retailers']['mobile'],$msg,'notify');
			//$this->General->sendMails($mail_subject, $mail_body);
			
			$this->Retailer->query("UPDATE topup_request SET approveStatus = 1 WHERE id = $id");
			echo "<script> reloadShopBalance(".$bal.");  </script>";
			echo "Approved";
		}
		else {
			echo $message;
		}
		$this->autoRender = false;
	}*/

	function transfer(){
        
		$this->render('transfer');
	}

	function amountTransfer($params=null,$authData=null){
            
		if(empty($authData)){
                    $authData = $this->Session->read('Auth');
                }
                $MsgTemplate = $this->General->LoadApiBalance(); 

		if(!in_array($authData['User']['group_id'],array(DISTRIBUTOR,SUPER_DISTRIBUTOR,ADMIN, SALESMAN))){
			$this->redirect(array('action' => 'index'));
		}
		
                $app_flag = 0;      // 0 for panel, 1 for sms transfer & 2 for auto limit transfer, 3 for app api
		$name = "";
		$confirm = 0;
		if(isset($this->data['confirm']))
                        $confirm = $this->data['confirm'];
		
		if($confirm ==1){
			
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","request when confirm flag is 1*: data=> ".json_encode($this->data)."<br/>".date('Y-m-d H:i:s'));
		}
		
		if($params != null){
			$this->data['confirm'] = 1;
			$confirm = 1;
			$this->data['amount'] = $params['amount'];
			$this->data['shop'] = $params['retailer'];
			$this->info = $authData;
			if(isset($params['description'])){
				$this->data['description'] = $params['description'];
				$this->data['typeRadio'] = 1;
			}	
                        if(isset($params['app_flag']) && ($params['app_flag'] == 2)){
                                $app_flag = 2;
                                $this->data['bank_name'] = $params['bankName'];
                                $this->data['typeRadio'] = 1;
                                $this->data['description'] = $params['txnId'];
                                if(isset($params['margin'])){
                                        $this->data['commission'] = $params['margin'];
                                }
                        } else if(isset($params['app_flag']) && ($params['app_flag'] == 3)){
                                $app_flag = 3;
                                if($authData['User']['group_id'] == SALESMAN){
                                        $salesman = $authData;
                                        $authData = $this->Shop->getShopDataById($salesman['dist_id'], DISTRIBUTOR);
                                        $authData['User']['group_id'] = SALESMAN;
                                        $authData['User']['mobile'] = $salesman['mobile'];
                                        $this->info = $authData;
                                }            		
                        } else {
                                $app_flag = 1;
                        }
		} else {
			if($confirm == 0 && $authData['User']['group_id'] != trim($this->data['group'])) {
				echo 'Invalid transfer. Kindly login again';
                                exit;
			}
			
			if($app_flag == 0) {
				if($confirm == 1 && $authData['User']['id'] == 1) {
                                        $getlimitPassword = $this->General->findVar('limit_password');
                                        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : "" ;
                                        if($getlimitPassword!=$password) {
                                                echo "Please enter valid password or contact your system admin"; exit;
                                        }
		            
					if(isset($this->data['bank_name']) && empty($this->data['bank_name'])){
                                                echo "Please Select Bank Name"; exit;
                                        }
					if(isset($this->data['description']) && empty($this->data['description'])){
                                                echo "Please Enter Bank Txn Id"; exit;
                                        }
                                } else if($confirm == 1 && $authData['User']['group_id'] == ADMIN) {
                                        $getlimitPassword = $this->General->findVar('limit_adminpassword');
                                        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : "" ;
                                        if($getlimitPassword != $password) {
                                                echo "Please enter valid password or contact your system admin"; exit;
                                        }
		            
                                        if(isset($this->data['bank_name']) && empty($this->data['bank_name'])){
                                                echo "Please Select Bank Name"; exit;
                                        }
                                        if(isset($this->data['description']) && empty($this->data['description'])){
                                                echo "Please Enter Bank Txn Id"; exit;
                                        }
                                } else if($confirm == 1 && $authData['User']['group_id'] == DISTRIBUTOR && in_array($authData['id'],explode(",",DISTS))) {
                                        if(isset($this->data['bank_name']) && empty($this->data['bank_name'])){
                                                echo "Please Select Bank Name"; exit;
                                        }
                                        if(isset($this->data['description']) && empty($this->data['description'])){
                                                echo "Please Enter Bank Txn Id"; exit;
                                        }
                                }
			}
		}
        
		$to_save = true;
		$this->data['amount'] = trim($this->data['amount']);
        
		if($this->data['shop'] == 0) {
			if($authData['User']['group_id'] == ADMIN)
                                $msg = "Please select super distributor";
			else if($authData['User']['group_id'] == SUPER_DISTRIBUTOR)
                                $msg = "Please select distributor";
			else if($authData['User']['group_id'] == DISTRIBUTOR) {
				if($app_flag == 1) {
					$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
				} else if($app_flag == 0) {
					$msg = "Please select retailer";
				}
			}

			$to_save = false;
		} else if(empty($this->data['amount'])) {
			if($app_flag == 1) {
				$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
			} else if($app_flag == 0)
                                $msg = "Please enter some amount";
			$to_save = false;
		} else if($this->data['amount'] > 500000 && !($authData['User']['group_id'] == ADMIN && $this->data['shop'] == 3) && $app_flag!=2) // removed restriction of 500000 limit for admin and auto limit transfer
		{
			$msg = "Amount cannot be greater than 500000";
			$to_save = false;
		}
		else if($this->data['amount'] > 1000000 && !($authData['User']['group_id'] == ADMIN && $this->data['shop'] == 3) && $app_flag==2) // removed restriction of 1000000 limit for admin and increased the transfer limit for autop transfer
		{
			$msg = "Amount cannot be greater than 1000000";
			$to_save = false;
		} else if($this->data['amount'] <= 0 || !preg_match('/^\d+$/',$this->data['amount'])){
			if($app_flag == 1){
				$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
			}
			else if($app_flag == 0)
                                $msg = "Amount entered is not valid";
			$to_save = false;
		} else if(isset($this->data['commission']) && !empty($this->data['commission']) && !is_numeric($this->data['commission'])) {
			$msg = "Commission entered is not valid";
			$to_save = false;
		} else {

                        
			if($authData['User']['group_id'] != ADMIN){
				$bal = $this->Shop->getBalance($authData['id'], ($authData['User']['group_id'] == SALESMAN) ? DISTRIBUTOR : $authData['User']['group_id']);
			}
			$amt_bal = $this->data['amount'];
            
			if($authData['User']['group_id'] == ADMIN || $authData['User']['group_id'] == SUPER_DISTRIBUTOR)
                                $amt_bal = $amt_bal + $this->data['commission'];
           
			if($authData['User']['group_id'] != ADMIN  && $amt_bal > $bal) {
				if($app_flag == 1) {
					$msg = "Contact your distributor, he doesn't have enough balance";
				} else if($app_flag == 0)
                                        $msg = "Your amount cannot be greater than your account balance";
				else if($app_flag == 3)
                                        return array("status" => "failure", "description" => "Cannot transfer due to insufficient balance");
				else
                                        $msg = "Cannot transfer due to insufficient balance";
                                
				$to_save = false;
			} else {
				if($authData['User']['group_id'] != ADMIN) {
					if($app_flag == 3 && $authData['User']['group_id'] == SALESMAN) {
						$shop = $this->Shop->getShopDataById($this->data['shop'], 6);
						if($shop['parent_id'] != $this->info['id']) {
							return array("status" => "failure", "description" => "Invalid transfer");
						}
					} else {
						$shop = $this->Shop->getShopDataById($this->data['shop'],$authData['User']['group_id'] + 1);
						if($shop['parent_id'] != $this->info['id']) {
							echo "Invalid transfer";
                                                        exit;
                                                }
					}    
				} else {
					$shop = $this->Shop->getShopDataById($this->data['shop'],SUPER_DISTRIBUTOR);
					if(isset($shop['parent_id'])) {
						echo "Invalid transfer";
                                                exit;
					}
				}
				
                                $getUserdata = $this->General->getUserDataFromId($shop['user_id']);
				if($authData['User']['group_id'] == DISTRIBUTOR && $shop['kyc_flag'] == 0 && ($this->data['amount'] + $shop['balance']) > KYC_AMOUNT){
					$msg = "Please collect KYC of the retailer. Retailer balance cannot be greater than Rs." . KYC_AMOUNT;
					$to_save = false;
                    
				} else if($authData['User']['group_id'] == SALESMAN && $shop['kyc_flag'] == 0 && ($this->data['amount'] + $shop['balance']) > KYC_AMOUNT){
					$msg = "Please collect KYC of the retailer. Retailer balance cannot be greater than Rs." . KYC_AMOUNT;
					$to_save = false;
				} else if(isset($shop['active_flag']) && $shop['active_flag'] == 0) {
					$msg = "You can not transfer to a closed distributor.";
					$to_save = false;
				} else {
					$this->data['shopData'] = $shop;
					if($confirm == 0) {
						if($authData['User']['group_id'] != ADMIN)
                                                        $this->set('balance',$bal - $amt_bal);
						$this->set('data',$this->data);
						$this->render('confirm_transfer','ajax');
					} else {
						
						if($app_flag == 0 || $app_flag ==3){
						
						$check_wait_time = $this->Shop->addMemcache("requested_distributor_".$authData['id'],$authData['id'] , 6);
                
                        if(!$check_wait_time){
                            return array('status'=>'failure','code'=>'37','description'=>$this->errors(37));	
                        }
						
						}
						$mail_subject = "Retail Panel: Amount Transferred";
						$bal1 = 0;
						if(isset($this->data['bank_name']) && !empty($this->data['bank_name'])) {
                                                        $ret = $this->Shop->lockBankTransaction($this->data['bank_name'],$this->data['description']);
                                                        if($ret == false) {
                                                                $msg = "Transfer for ".$this->data['description']." is already done.";
                                                                $to_save = false;
                                                        }
						}
						$det = $this->Shop->getMemcache("txn_" . $getUserdata['mobile']);
						if ($det === false) {
							$this->Shop->setMemcache("txn_" . $getUserdata['mobile'],1, 2*60);
						} else {
							$msg = "You cannot transfer limit to same person within 2 minutes!!!";
							$to_save = false;
						}
						if($to_save && $authData['User']['group_id'] == ADMIN){
                             $trans_id = $this->Shop->shopTransactionUpdate(ADMIN_TRANSFER,$this->data['amount'],null,$shop['id'],$authData['User']['id']);
							if(!empty($this->data['commission'])){
								$this->Shop->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$this->data['commission'],$shop['id'],$trans_id, $authData['User']['id']);
							}

							if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['bank_name'].":".$this->data['description'])."' where id = $trans_id");
								}

							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'add',$shop['id'],SUPER_DISTRIBUTOR);
							$this->Shop->addOpeningClosing($shop['id'],SUPER_DISTRIBUTOR,$trans_id,$bal1-($this->data['amount']+$this->data['commission']),$bal1);

							$mail_body = "Admin: " . $this->info['company'] . " transferred Rs. " . ($this->data['amount'] + $this->data['commission']) . " to SuperDistributor: " . $shop['company'];
							$name = "SuperDistributor";
							$dist_data = $this->General->getUserDataFromId($shop['user_id']);
							$shop['mobile'] = $dist_data['mobile'];
							$sms = "Dear $name,\nYour account is successfully credited with Rs." . ($this->data['amount'] + $this->data['commission']) . "\nYour current balance is Rs.$bal1";

							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'];
							}

							if(!in_array($shop['id'],explode(",",SDISTS))){
								$this->General->sendMails($mail_subject, $mail_body,array('limits@mindsarray.com'));
								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "SD";
								$data1['name'] = $shop['company'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['commission'] = $this->data['commission'];
								$data1['commission_per'] = $this->data['commission_per'];
								$data1['transid'] = $trans_id;
								$this->General->curl_post($this->General->findVar('limit_url'),$data1);
							}
							$msg = "Transaction is Completed Successfully And Transaction Id is $trans_id";
							$shopId = $trans_id;
						} else if($to_save && $authData['User']['group_id'] == SUPER_DISTRIBUTOR) {
							$trans_id = $this->Shop->shopTransactionUpdate(SDIST_DIST_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id'],$authData['User']['id']);
							$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 1*: transId=> $trans_id<br/>".date('Y-m-d H:i:s'));
							if(!empty($this->data['commission'])) {
								
//								if($this->data['bank_name'] == 'ICICI6714' &&  ($this->data['typeRadio'] == 1 || $this->data['typeRadio'] == 3)){
//									$this->data['commission'] = round($this->data['commission']-$this->data['amount']*0.0005,2);
//								}
								
							
								
                                                                $this->Shop->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$this->data['commission'],$shop['id'],$trans_id,$authData['User']['id']);
																$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 2 update commision*: transId=> $trans_id<br/>".date('Y-m-d H:i:s'));
                                                        }

                                                        if($app_flag == 2){
                                                                $this->data['typeRadio'] = 1;
                                                        }
                                                        if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['bank_name'].":".$this->data['description'])."' where id = $trans_id");
								$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 3 update shop_transactions*: transId=> $trans_id<br/>".date('Y-m-d H:i:s'));
							}
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'subtract',$this->info['id'],SUPER_DISTRIBUTOR);
							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'add',$shop['id'],DISTRIBUTOR);
							$this->Shop->addOpeningClosing($this->info['id'],SUPER_DISTRIBUTOR,$trans_id,$bal+$this->data['amount']+$this->data['commission'],$bal);
							$this->Shop->addOpeningClosing($shop['id'],DISTRIBUTOR,$trans_id,$bal1-($this->data['amount']+$this->data['commission']),$bal1);
							$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 4 update and insert*: transId=> $trans_id<br/>".date('Y-m-d H:i:s'));
                            
							$mail_body = "SuperDistributor: " . $this->info['company'] . " transferred Rs. " . ($this->data['amount'] + $this->data['commission']) . " to Distributor: " . $shop['company'];
							$name = "Distributor";
							$dist_data = $this->General->getUserDataFromId($shop['user_id']);
							$shop['mobile'] = $dist_data['mobile'];
							$sms = "Dear $name,\nYour account is successfully credited with Rs." . ($this->data['amount'] + $this->data['commission']) . "\nYour current balance is Rs.$bal1";

							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'].$this->data['bank_name'];
							}
							 
							if(in_array($this->info['id'],explode(",",SDISTS)) && !in_array($shop['id'],explode(",",DISTS))){

								$slab_det = $this->Slaves->query("select commission_dist , name from slabs where id = ".$shop['slab_id']."\n");
								if ( $this->data['commission'] * 100 / $this->data['amount']  > $slab_det[0]['slabs']['commission_dist'] ){

									if($shop['margin'] == round($this->data['commission'] * 100/$this->data['amount'],2) && $shop['margin_approved'] == 1){
										
									} else {
										$this->General->sendMails("Wrong Commission Transfered to distributor","
										Company : ".$shop['company']."</br>
                                                                                Mobile : ".$shop['mobile']."</br>
                                                                                Amount : ".$this->data['amount']."</br>
                                                                                Commission : ".$this->data['commission']."(".round($this->data['commission'] * 100 / $this->data['amount'],2)."%)"."</br> 
                                                                                Slab Name : ".$slab_det[0]['slabs']['name']."</br> 
                                                                                " ,array('sunilr@pay1.in','limits@mindsarray.com'), 'mail');
									}
                                                                                                                                     
								}
								if($bal1 >= 500000){// if current bal after transfer then raise a alarm

									$mail_subject = "Current balance of Distributor is greater than 500000";
									$mail_body = "Current balance of ".$shop['company']." is $bal1 </br>
                                                                                Mobile : ".$shop['mobile']."</br>
                                                                                Amount Transferred : ".$this->data['amount']."</br>
                                                                                ";
									$this->General->sendMails($mail_subject, $mail_body , array("tadka@mindsarray.com",'limits@mindsarray.com'),'mail');
								}

                               
								$this->General->sendMails($mail_subject, $mail_body,array('limits@mindsarray.com'));

								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "D";
								$data1['name'] = $shop['company'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['commission'] = $this->data['commission'];
								$data1['commission_per'] = $this->data['commission_per'];
								$data1['transid'] = $trans_id;
								$this->General->curl_post($this->General->findVar('limit_url'),$data1);
								$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 5 after curl operation: transId=> $trans_id<br/>".date('Y-m-d H:i:s'));

							}
//							$msg = "Transaction is Completed Successfully And Transaction Id is $trans_id";
//							$succmsg = "Transfer to ".$shop['company']."Completed Successfully!!!";
                                                        
                                                        $paramdata['RECID'] = $trans_id;
                                                        $content=  $MsgTemplate['AmountTransfer_TransactionComplete_MSG'];
                                                        $msg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        
						        $paramdata['SHOP_NAME'] = $shop['company'];
                                                        $content=  $MsgTemplate['AmountTransfer_TransferComplete_MSG'];
                                                        $succmsg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        
                                                        
							$shopId = $trans_id;
                                                } else if($to_save && in_array($authData['User']['group_id'], array(DISTRIBUTOR, SALESMAN))){
													
							$recId = $this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id'],$authData['id']);
							$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 1*: transId=> $recId<br/>".date('Y-m-d H:i:s'));
							if(!isset($params['salesmanId']) || $params['salesmanId'] == ''){
								$dstMob = $this->Slaves->query("SELECT mobile from users where id = '".$authData['User']['id']."'");
								$sm = $this->Slaves->query("SELECT id,name from salesmen where mobile = '".$dstMob['0']['users']['mobile']."'");
                                                                $params['salesmanId'] = $sm['0']['salesmen']['id'];
                                                                $params['salesmanName'] = $sm['0']['salesmen']['name'];
                                                                $params['distId'] = 0;
                                                        }
							if(!isset($salesman)){
								$salesmen = $this->Slaves->query("select * from salesmen where id = '".$params['salesmanId']."'");
								$salesman = $salesmen['0']['salesmen'];
							}
                                                        if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['bank_name'].":".$this->data['description'])."' where id = $recId");
							}
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],DISTRIBUTOR);
							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],RETAILER);
							
							$this->Shop->addOpeningClosing($shop['id'],RETAILER,$recId,$bal1-$this->data['amount'],$bal1);
							$this->Shop->addOpeningClosing($this->info['id'],DISTRIBUTOR,$recId,$bal+$this->data['amount'],$bal);
							
							$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step2 => update and insert transaction  : transId=> $recId<br/>".date('Y-m-d H:i:s'));
							
							$this->Retailer->query("INSERT INTO salesman_transactions
									(shop_tran_id,salesman,payment_mode,payment_type,details,collection_date,created, closing)
									VALUES ('".$recId."','".$params['salesmanId']."','".MODE_CASH."','".TYPE_TOPUP."','','".date('Y-m-d')."','".date('Y-m-d H:i:s')."', '".($salesman['balance'] - $this->data['amount'])."')");
							
							if(!empty($shop['shopname'])){
								$shop_name = substr($shop['shopname'],0,15);
								if($shop_name != $shop['shopname'])$shop_name = $shop_name . "..";
							} else
                                                                $shop_name = $shop['mobile'];
						
							if($authData['User']['group_id'] == SALESMAN){
                                                                $distributors = $this->Slaves->query("select u.mobile
									from users u
									left join distributors d on d.user_id = u.id
									where d.id = ".$this->info['id']);
                                                                if($distributors){
        //								$message_distributor = "Salesman: ".$params['salesmanName']." ) transferred Rs. " . $this->data['amount'] . " to Retailer: " . $shop_name;
                                                                        $paramdata['SALESMAN_NAME'] = "(".$params['salesmanName'].")";
                                                                        $paramdata['AMOUNT'] = $this->data['amount'];
                                                                        $paramdata['SHOP_NAME'] = $shop_name;
                                                                        $content =  $MsgTemplate['AmountTransfer_SalesmanToRetailer_MSG'];
                                                                        $message_distributor = $this->General->ReplaceMultiWord($paramdata,$content);
                                                                
                                                              
                                                                        $this->General->sendMessage($distributors['0']['u']['mobile'], $message_distributor, "notify", $bal, DISTRIBUTOR);
                                                                }
							}
							
							$mail_body = "Distributor: " . $this->info['company'] . " (Salesman: ".$params['salesmanName']." ) transferred Rs. " . $this->data['amount'] . " to Retailer: " . $shop_name;
							$name = "Retailer";
//							$sms = "Dear $name,\nYour account is successfully credited with Rs." . $this->data['amount']. "\nYour current balance is Rs.$bal1";

                                                        $paramdata['NAME'] = $name;
                                                        $paramdata['AMOUNT'] = $this->data['amount'];
                                                        $paramdata['BALANCE'] = $bal1;
                                                        $content=  $MsgTemplate['AmountTransfer_AccountCreated_MSG'];
                                                        $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        
                                                        
							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'].$this->data['bank_name'];
							}

							if(in_array($this->info['id'],explode(",",DISTS))){
								$this->General->sendMails($mail_subject, $mail_body,array('limits@mindsarray.com'));

								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "R";
								$data1['name'] = $shop['shopname'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['transid'] = $recId;
								$this->General->curl_post($this->General->findVar('limit_url'),$data1);
								$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step3 => after curl operation  : transId=> $recId<br/>".date('Y-m-d H:i:s'));
							}
							if($bal1 >= 500000){// if current bal after transfer then raise a alarm

								$mail_subject = "Current balance of Retailer is greater than 500000";
								$mail_body = "Current balance of ".$shop['shopname']." is $bal1 </br>
                                                                            Mobile : ".$shop['mobile']."</br>
                                                                            Amount Transferred : ".$this->data['amount']."</br>
                                                                            ";
								$this->General->sendMails($mail_subject, $mail_body , array("tadka@mindsarray.com","limits@mindsarray.com"),'mail');
							}
							
								
							$msg = "Transaction is Completed Successfully And Transaction Id is $recId";
                                                       /* $paramdata['RECID'] = $recId;
                                                        $content=  $MsgTemplate['AmountTransfer_TransactionComplete_MSG'];
                                                        $msg = $this->General->ReplaceMultiWord($paramdata,$content);*/
                                         
							$succmsg = "Transfer to ".$shop['shopname']."Completed Successfully!!!";
                                                        
														/*$paramdata['SHOP_NAME'] = $shop['shopname'];
                                                        $this->General->logData('/mnt/logs/salesman_limit_transfer.txt', "after sms21");
                                                        $content=  $MsgTemplate['AmountTransfer_TransferComplete_MSG'];
                                                        $this->General->logData('/mnt/logs/salesman_limit_transfer.txt', "after sms22");   
                                                        $succmsg = $this->General->ReplaceMultiWord($paramdata,$content);
                                                        */
                                                        
							$shopId = $recId;
						}
						
						//$this->General->sendMessage($shop['mobile'],$sms,$name=="Retailer"?'notify':'shops');
						$this->General->sendMessage($shop['mobile'],$sms,'shops');
                        
						if($to_save && isset($params['distId'])) {
							
							$sm = $this->Slaves->query("SELECT mobile,balance,tran_limit from salesmen where id = ".$params['salesmanId']);

							$data = $this->Slaves->query("SELECT sum(shop_transactions.amount) as topups FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id=salesman_transactions.shop_tran_id) WHERE salesman_transactions.salesman=".$params['salesmanId']." AND shop_transactions.ref2_id is not null AND salesman_transactions.payment_type=2 AND collection_date='".date('Y-m-d')."'");

//							$message = "Amount Rs ".$this->data['amount']." transferred to retailer $shop_name successfully.";
//							$message .= "\nYour balance now: $bal";
//							$message .= "\nYour today's topups: " . $data['0']['0']['topups'];
                                                         
                                                        $paramdata['AMOUNT'] = $this->data['amount'];
                                                        $paramdata['BALANCE'] = $bal;
                                                        $paramdata['SHOP_NAME'] = $shop_name;
                                                        $paramdata['TOPUPS'] = $data['0']['0']['topups'];
                                                        $content =  $MsgTemplate['AmountTransfer_ToRetailer_MSG'];
                                                        $message = $this->General->ReplaceMultiWord($paramdata,$content);    
                                                        
							if($app_flag != 1){
// 								$this->General->sendMessage($sm['0']['salesmen']['mobile'],$message,'ussd');
							} else {
								$sms_send = $message;
							}
						} else if($to_save && isset($params['salesmanId'])){
							
							$sm = $this->Slaves->query("SELECT mobile,balance,tran_limit from salesmen where id = ".$params['salesmanId']);
								
							$this->Retailer->query("UPDATE salesmen SET balance = ".($sm['0']['salesmen']['balance'] - $this->data['amount'])." WHERE id=".$params['salesmanId']);
							$data = $this->Slaves->query("SELECT sum(shop_transactions.amount) as topups FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id=salesman_transactions.shop_tran_id) WHERE salesman_transactions.salesman=".$params['salesmanId']." AND salesman_transactions.payment_type=2 AND collection_date='".date('Y-m-d')."'");

//							$message = "Amount Rs ".$this->data['amount']." transferred to retailer $shop_name successfully.";
//							$message .= "\nYour balance now: " . ($sm['0']['salesmen']['balance'] - $this->data['amount']) . " (" . $sm['0']['salesmen']['tran_limit'] . ")";
//							$message .= "\nYour today's topups: " . $data['0']['0']['topups'];
							
                                                        $paramdata['AMOUNT'] = $this->data['amount'];
                                                        $paramdata['BALANCE'] = $sm['0']['salesmen']['balance'] - $this->data['amount'] . " (" . $sm['0']['salesmen']['tran_limit'] . ")";
                                                        $paramdata['SHOP_NAME'] = $shop_name;
                                                        $paramdata['TOPUPS'] = $data['0']['0']['topups'];
                                                        $content =  $MsgTemplate['AmountTransfer_ToRetailer_MSG'];
                                                        $message = $this->General->ReplaceMultiWord($paramdata,$content);    
                                                        
                                                        if($app_flag != 1 && $app_flag !=3) {
								$this->General->sendMessage($sm['0']['salesmen']['mobile'],$message,'notify',null,DISTRIBUTOR);
							} else {
								$sms_send = $message;
							}	
						}
						if($to_save){
                                                        if($app_flag == 0){
                                                                //echo "<script> reloadShopBalance(".$bal.");  </script>";
                                                                //$this->render('/elements/shop_transfer','ajax');
                                                        } else {
                                                                return array('status' => 'success','balance' => $bal1,'description' => $sms_send,'shopId' => $shopId ,'msg' => $succmsg);
                                                        }  
                                                }
					}
				}
			}
		}

		if(!$to_save){
			if($app_flag == 0){
				$this->set('data',$this->data);
				$msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($msg, true));
				$this->render('/elements/shop_transfer','ajax');
			} else {
				return array('status' => 'failure','description' => $msg);
			}
		} else if($app_flag == 0 && $confirm == 1) {
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 6 before rendering : transId=> $shopId<br/>".date('Y-m-d H:i:s'));
			echo "<script> reloadShopBalance(".$bal.");  </script>";
			$msg = '<div class="error_class">'.$msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('/elements/shop_transfer','ajax');
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/amount_transfer.txt","inside balance transfer step 7 after rendering : transId=> $shopId<br/>".date('Y-m-d H:i:s'));
		}
        }

	function calculateCommission(){
		$id = trim($_REQUEST['id']);
		$amount = trim($_REQUEST['amount']);
		$comm = 0;
		$margin = 0;

		if($amount > 0){
			$margin = $this->Shop->getMemcache("margin_".$this->Session->read('Auth.User.group_id')."_".$id);	
			
			if($margin === false){
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
					$data = $this->Slaves->query("SELECT margin FROM distributors as shop WHERE id = $id");
				}
				else {
					$data = $this->Slaves->query("SELECT margin FROM super_distributors as shop WHERE id = $id");
				}
					
				if(!empty($data)){
					$margin = $data['0']['shop']['margin'];
					$this->Shop->setMemcache("margin_".$this->Session->read('Auth.User.group_id')."_".$id,$data['0']['shop']['margin'],6*60*60);
				}
				
			}
			$comm = round($margin * $amount /100,0);
		}
		
		echo $comm;
		$this->autoRender = false;
	}


	function backTransfer(){
		
		$this->set('data',$this->data);
		$this->render('/elements/shop_transfer');
	}

	function kitsTransfer(){

	}

	function transferKits(){
		//echo "======".isset($this->data['commission_flag'])?"Y":"N"."======";
		$this->data['commission_flag'] = isset($this->data['commission_flag'])?$this->data['commission_flag']:"";
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
		preg_match('/^[0-9]/',$this->data['amount'],$matches,0);
		preg_match('/^[0-9]/',$this->data['kit_commission'],$matches1,0);
		if($this->data['shop'] == 0)
		{
			$msg = "Please select distributor";
			$to_save = false;
		}
		else if(empty($this->data['amount']))
		{
			$msg = "Please enter amount";
			$to_save = false;
		}
		else if($this->data['amount'] <= 0 || empty($matches)){
			$msg = "Amount entered is not valid";
			$to_save = false;
		}
		else if($this->data['kit'] > 30){
			$msg = "Cannot transfer more than 30 kits at a time";
			$to_save = false;
		}
		else if($this->data['commission_flag'] == 'on' && (empty($matches1) || empty($this->data['kit_commission']))){
			$msg = "Commission amount entered is not valid";
			$to_save = false;
		}
		else {
			$shop = $this->Shop->getShopDataById($this->data['shop'],DISTRIBUTOR);
			$this->data['shopData'] = $shop;

			if($confirm == 0){
				$this->set('data',$this->data);
				$this->render('confirm_kits_transfer','ajax');
			} else {

				$mail_subject = "Retail Panel: Kits Transferred";
				$kits = $shop['kits'];
				if($kits == -1)$kits = 0;
				if(empty($this->data['kit']))
				$kits = -1;
				else
				$kits = $kits + $this->data['kit'];

				if($this->data['commission_flag'] == 'on'){
					$this->Retailer->query("UPDATE distributors SET kits=$kits,commission_kits_flag=1,retailer_creation=1,discount_kit=".$this->data['kit_commission']." WHERE id = " . $shop['id']);
				}
				else {
					$this->Retailer->query("UPDATE distributors SET kits=$kits,commission_kits_flag=0,retailer_creation=1,discount_kit=null WHERE id = " . $shop['id']);
				}

				if(!empty($this->data['kit'])){
					$this->Retailer->query("INSERT INTO distributors_kits (distributor_id,kits,amount,note,timestamp) VALUES (".$shop['id'].",".$this->data['kit'].",".$this->data['amount'].",'".addslashes($this->data['note'])."','".date('Y-m-d H:i:s')."')");
				}
				else {
					$this->Retailer->query("INSERT INTO distributors_kits (distributor_id,amount,note,timestamp) VALUES (".$shop['id'].",".$this->data['amount'].",'".addslashes($this->data['note'])."','".date('Y-m-d H:i:s')."')");
				}

				$mail_body = "SuperDistributor: " . $this->info['company'] . " transferred kits " . $this->data['kit'] . " to Distributor: " . $shop['company'] . " in Rs. " . $this->data['amount'];
				if(!empty($this->data['note'])){
					$mail_body .= "<br/>Note: ".$this->data['note'];
				}

				$dist_data = $this->General->getUserDataFromId($shop['user_id']);
				$shop['mobile'] = $dist_data['mobile'];
//				$msg = "Dear Distributor,\nYour account is successfully credited with " . $this->data['kit']. "kits";
                                
                                $paramdata['KIT_DATA'] = $this->data['kit'];
                                $MsgTemplate = $this->General->LoadApiBalance();
                                $content=  $MsgTemplate['TransferKits_MSG'];
                                
				if($kits != -1){
//					$msg .= "\nYou have total $kits now";
                                        $paramdata['TOTAL_KITS'] = $kits;
                                        $content =  $MsgTemplate['Transfer_TotalKits_MSG'];
				}
                                $msg = $this->General->ReplaceMultiWord($paramdata,$content);
				$this->General->sendMessage($shop['mobile'],$msg,'shops');
				$this->General->sendMails($mail_subject, $mail_body,array('distributor.care@mindsarray.com','limits@mindsarray.com'),'mail');

				$this->render('/elements/shop_kits_transfer','ajax');
			}
		}

		if(!$to_save){
			$this->set('data',$this->data);
			$msg = '<div class="error_class">'.$msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('/elements/shop_kits_transfer','ajax');
		}
	}


	function backKitTransfer(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_kits_transfer');
	}


	function logout(){
            
		$this->Auth->logout();
		session_destroy();
		$this->redirect('/shops');
	}

	/*function updateTransactionPP(){
		$TransactionId = $_REQUEST['TransactionId'];
		$TransactionType = $_REQUEST['TransactionType'];
		$MobileNo = $_REQUEST['MobileNo'];
		$CustomerNo = $_REQUEST['CustomerNo'];
		$Amount = $_REQUEST['Amount'];
	}*/

	/*function lastTransactions($page = null){
		if($page == null)$page = 1;
		$ret = $this->Shop->getLastTransactions(null,$page);
		//$this->printArray($ret);
	}*/

	function initializeOpeningBalance(){//initialize balance once everyday at 11:30PM
		$this->SuperDistributor->updateAll(array('SuperDistributor.opening_balance' => 'SuperDistributor.balance'));
		$this->Distributor->updateAll(array('Distributor.opening_balance' => 'Distributor.balance'));
		$this->Retailer->updateAll(array('Retailer.opening_balance' => 'Retailer.balance'));
		$this->autoRender = false;
	}


	/*function products(){
		$this->render('products','xml/default');
	}

	function createCommissionTemplate(){
		$slab_id = 3;
		$percents = array(3,1.9,3.5,2,3.5,4.5,3,3,2.5,2.5,3.5,4,3.5,3.5,2,2,3.3,2.7,3.5,3.3,4.2);
		$products = $this->Retailer->query("SELECT id from products where active = 1");
		$i = 0;
		foreach($products as $prod){

			$this->Retailer->query("INSERT INTO slabs_products (slab_id,product_id,percent) VALUES (".$slab_id.",".$prod['products']['id'].",".$percents[$i].")");
			$i++;
		}
		$this->autoRender = false;
	}*/


	/*function genaric_match($template,$string,$varStart="{{",$varEnd="}}"){


		$template = str_replace($varStart,"|~|`",$template);
		$template = str_replace($varEnd,"`|~|",$template);

		$t=explode("|~|",$template);

		$temp="";
		$i=0;
		foreach ($t as $n=>$v){
			$i++;
			if (($i==count($t)||($i==(count($t)-1)&&$t[$n+1]==""))&&substr($v,0,1)=="`"&&substr($v,-1)=="`"){
				//Last Item
				$temp.="(?P<".substr($v,1,-1).">.++)";

			}elseif(substr($v,0,1)=="`"&&substr($v,-1)=="`"){
				//Search Item
				$temp.="(?P<".substr($v,1,-1).">[^".$t[$n+1]."]++)";

			}else{
				$temp.=$v;
			}

		}
		$temp="~^".$temp."$~";
		echo $temp . "<br/>";
		echo $string . "<br/>";

		preg_match($temp, $string, $matches);

		return $matches;

	}
	 
	function test(){
		$array = array(1,2,3,4,5,6,7,8,9,10);
		$array = array_slice($array,3*3,3);
		print_r($array);
		$text = '<?xml version="1.0" encoding="UTF-8"?>
<Response>
<Status>0</Status>
<StatusText>SUCCESS</StatusText>
<Ticket>
<Status>0</Status>
<StatusText>SUCCESS</StatusText>
<DrawID>7</DrawID>
<TSN>6844-3DA1-E679-9CBF</TSN>
<DrawDate>22/07/2013 21:15:00</DrawDate>
<SLSN>200107556</SLSN>
<BetDate>07/16/2013 12:19</BetDate>
<GameName>Keno,Monday,.,,</GameName>
<Cost>20.00</Cost>
<Mrp>10.00</Mrp>
<Panel>
<LP>0</LP>
<BetLine>A: 01 02 03 02</BetLine>
</Panel>
<Promotion/>
</Ticket>
<CardBalance>14730.00</CardBalance>
</Response>';

		$arr = $this->General->xml2array($text);
		$this->printArray($arr);exit;
		echo "1";
		$this->autoRender = false;
		exit;

		echo "1";exit;
		$this->General->sendMessageViaInfobip('',array('9819032643','9833032643'),'hello');exit;
		$this->General->mailToUsers('test','test',array('ashish@mindsarray.com'));
		exit;
		$this->General->sendMessage('','9892609560,9819852204,9004387418','hello');
		exit;
	}

	function script(){
		$this->Invoice->recursive = -1;
		$invoices = $this->Invoice->find('all');
		foreach($invoices as $invoice){
			if($invoice['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($invoice['Invoice']['group_id'] == DISTRIBUTOR || $invoice['Invoice']['group_id'] == RETAILER){
				$shop = $this->Shop->getShopDataById($invoice['Invoice']['ref_id'],$invoice['Invoice']['group_id']);
				$parent = $shop['parent_id'];
			}
			$this->Invoice->updateAll(array('Invoice.from_id' => $parent),array('Invoice.id' => $invoice['Invoice']['id']));
		}
		$this->autoRender = false;
	}

	function issue(){
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];
		$child = $_REQUEST['child'];

		if($type == RECEIPT_TOPUP){
			$number = $this->Shop->getTopUpReceiptNumber($id);
		}
		else if($type == RECEIPT_INVOICE){
			$invoice = $this->Invoice->find('first',array('fields' => array('Invoice.timestamp','Invoice.invoice_number'), 'conditions' => array('Invoice.id' => $id)));
			$number = $invoice['Invoice']['invoice_number'];
		}
		$this->set('number',$number);
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('child',$child);
		$this->render('receipt_form','ajax');
	}*/

	function graphRetailer(){
		$type = $_REQUEST['type'];
		$id = $_REQUEST['id'];
		if(empty($_REQUEST['from'])){
			$from = date('Y-m-d',strtotime('-30 days'));
			$to = date('Y-m-d');
		}
		else {
			$from = $_REQUEST['from'];
			$to = $_REQUEST['to'];
			if(checkdate(substr($from,2,2), substr($from,0,2), substr($from,4)) && checkdate(substr($to,2,2), substr($to,0,2), substr($to,4))){
				$from =  substr($from,4) . "-" . substr($from,2,2) . "-" . substr($from,0,2);
				$to =  substr($to,4) . "-" . substr($to,2,2) . "-" . substr($to,0,2);
			}
			else {
				$from = date('Y-m-d',strtotime('-30 days'));
				$to = date('Y-m-d');
			}
		}
		
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER && $this->Session->read('Auth.User.group_id') != CUSTCARE)$this->redirect('/shops/view');
		
		if($type == 'r'){
			if(!empty($id)){
				if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					if($data['parent_id'] != $this->info['id'])exit;
				}
				else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					$data1 = $this->Shop->getShopDataById($data['parent_id'],DISTRIBUTOR);
					if($data1['parent_id'] != $this->info['id']){
						exit;
					}
				}
			}
			else exit;

			$sale = $this->Slaves->query("SELECT sale,date,topup FROM retailers_logs WHERE retailer_id  = $id AND date >= '$from' AND date <= '$to'");
			$begin = new DateTime($from);
			$end = new DateTime($to);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$data = array();
			$totSale = 0;
			$i = 0;

			$saleData = array();

			foreach($sale as $sl){
				$saleData[$sl['retailers_logs']['date']]['sale'] = $sl['retailers_logs']['sale'];
				$saleData[$sl['retailers_logs']['date']]['topup'] = $sl['retailers_logs']['topup'];
			}

			foreach ($period as $dt){
				$data1 = array();
				$date = $dt->format("Y-m-d");
				if(isset($saleData[$date])){
					if(isset($saleData[$date]['sale'])){
						$totSale += $saleData[$date]['sale'];
						$sale = intval($saleData[$date]['sale']);
					}
					else {
						$sale = 0;
					}

					if(isset($saleData[$date]['topup'])){
						$topup = intval($saleData[$date]['topup']);
					}
					else {
						$topup = 0;
					}
					$data[] = array($date,$sale,$topup);
				}
				else {
					$data[] = array($date,0,0);
				}

				$i++;
			}

			$avg = intval($totSale/$i);
			$i=0;
			foreach($data as $dt){
				$data[$i][3]=$avg;
				$i++;
			}
			$graphData = array(
			   'labels' => array(
			array('string' => 'Sample'),
			array('number' => 'Daily Sale'),
			array('number' => 'Daily Topup'),
			array('number' => 'Average Sale')
			),
			   'data' => $data,
			   'title' => 'Retailer Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
		}
		else if($type == 'd'){
			if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
				$id = $this->info['id'];
			}

			if(!empty($id)){
				if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,DISTRIBUTOR);
					if($data['parent_id'] != $this->info['id']){
						exit;
					}
				}

				$sale = $this->Slaves->query("SELECT sum(sale) as amts,date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id  = $id AND date >= '$from' AND date <= '$to' GROUP by date order by date");
				$dist_data = $this->Slaves->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE distributor_id = $id AND date >= '$from' AND date <= '$to' group by date order by date");
				$averageResult=$this->Slaves->query("SELECT retailers_logs.sale,retailers_logs.date,retailers_logs.retailer_id from retailers_logs,retailers WHERE retailers.parent_id = $id AND retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '$from' AND retailers_logs.date <= '$to'");
			}
			else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
				$sale = $this->Slaves->query("SELECT sum(sale) as amts,date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND date >= '$from' AND date <= '$to' GROUP by date order by date");
				$dist_data = $this->Slaves->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE date >= '$from' AND date <= '$to' group by date order by date");
				$averageResult=$this->Slaves->query("SELECT retailers_logs.sale,retailers_logs.date,retailers_logs.retailer_id from retailers_logs WHERE retailers_logs.date >= '$from' AND retailers_logs.date <= '$to'");
			}

			$begin = new DateTime($from);
			$end = new DateTime($to);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$data = array();
			$data0 = array();
			$data1 = array();
			$data2 = array();

			$totSale = 0;
			$totSale_sec = 0;
			$i = 0;
			$avgSale = 0;
			$avgSale_sec = 0;

			$saleData = array();

			foreach($sale as $sl){
				$saleData[$sl['retailers_logs']['date']]['sale'] = $sl['0']['amts'];
				$totSale += $sl['0']['amts'];
				$i++;
			}

			if($i>0)$avgSale = intval($totSale/$i);
			$i = 0;
			$retData = array();

			foreach($dist_data as $tp){
				$saleData[$tp['distributors_logs']['date']]['topup'] = $tp['0']['topup_buy'];
				$saleData[$tp['distributors_logs']['date']]['topup_sec'] = $tp['0']['topup_sold'];
				$totSale_sec += $tp['0']['topup_sold'];
				$i++;

				$retData[$tp['distributors_logs']['date']]['retailers'] = $tp['0']['retailers'];
				$retData[$tp['distributors_logs']['date']]['transacting'] = $tp['0']['transacting'];
				$retData[$tp['distributors_logs']['date']]['topups'] = $tp['0']['topup_unique'];
			}

			if($i>0)$avgSale_sec = intval($totSale_sec/$i);


			foreach($averageResult as $sl){
				if($sl['retailers_logs']['sale'] >= 1000){
					if(isset($retData[$sl['retailers_logs']['date']]['plus1000'])){
						$retData[$sl['retailers_logs']['date']]['plus1000'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['plus1000'] = 1;
					}
				}
				else if($sl['retailers_logs']['sale'] >= 500 && $sl['retailers_logs']['sale'] < 1000){
					if(isset($retData[$sl['retailers_logs']['date']]['plus500'])){
						$retData[$sl['retailers_logs']['date']]['plus500'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['plus500'] = 1;
					}
				}
				else {
					if(isset($retData[$sl['retailers_logs']['date']]['less500'])){
						$retData[$sl['retailers_logs']['date']]['less500'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['less500'] = 1;
					}
				}
			}

			foreach ($period as $dt){
				$date = $dt->format("Y-m-d");
				if(isset($saleData[$date])){
					if(isset($saleData[$date]['sale'])){
						$sale = intval($saleData[$date]['sale']);
					}
					else {
						$sale = 0;
					}

					if(isset($saleData[$date]['topup'])){
						$topup = intval($saleData[$date]['topup']);
					}
					else {
						$topup = 0;
					}

					if(isset($saleData[$date]['topup_sec'])){
						$topup_s = intval($saleData[$date]['topup_sec']);
					}
					else {
						$topup_s = 0;
					}
					$data[] = array($date,$topup,$topup_s,$avgSale_sec);
					$data0[] = array($date,$sale,$avgSale);
				}
				else {
					$data[] = array($date,0,0,0);
					$data0[] = array($date,0,0);
				}

				$retBefore = 0;
				if(isset($retData[$date])){
					if(isset($retData[$date]['retailers'])){
						$retBefore = $retData[$date]['retailers'];
					}
					$ret = $retBefore;

					if(isset($retData[$date]['transacting'])){
						$trans = $retData[$date]['transacting'];
					}
					else {
						$trans = 0;
					}

					if(isset($retData[$date]['topups'])){
						$topups = $retData[$date]['topups'];
					}
					else {
						$topups = 0;
					}

					if(isset($retData[$date]['plus1000'])){
						$plus1000 = $retData[$date]['plus1000'];
					}
					else {
						$plus1000 = 0;
					}

					if(isset($retData[$date]['plus500'])){
						$plus500 = $retData[$date]['plus500'];
					}
					else {
						$plus500 = 0;
					}

					if(isset($retData[$date]['less500'])){
						$less500 = $retData[$date]['less500'];
					}
					else {
						$less500 = 0;
					}

					$data1[] = array($date,$ret,$trans,$topups);
					$data2[] = array($date,$plus1000,$plus500,$less500);
				}
				else {
					$data1[] = array($date,$retBefore,0,0);
					$data2[] = array($date,0,0,0);
				}
				$i++;
			}
			
			$graphData = array(
			   'labels' => array(
			array('string' => 'Sample'),
			array('number' => 'Daily Topup (Primary)'),
			array('number' => 'Daily Sale (Secondary)'),
			array('number' => 'Average Sale (Secondary)')
			),
			   'data' => $data,
			   'title' => 'Distributor Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData0 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Daily Sale (Tertiary)'),
			   array('number' => 'Average Sale (Tertiary)')
			   ),
			   'data' => $data0,
			   'title' => 'Retailers Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData1 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Total Retailers'),
			   array('number' => 'Transacting Retailers'),
			   array('number' => 'Daily Unique Retailer topups')
			   ),
			   'data' => $data1,
			   'title' => 'Distributor Retailers Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData2 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Retailers (Sale >= 1000)'),
			   array('number' => 'Retailers (500 <= Sale < 1000)'),
			   array('number' => 'Retailers (Sale < 500)')
			   ),
			   'data' => $data2,
			   'title' => 'Retailer Performance Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $this->set('data0',$graphData0);
			   $this->set('data1',$graphData1);
			   $this->set('data2',$graphData2);
		}
		//echo json_encode($graphData);
		$this->set('data',$graphData);
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('from',$from);
		$this->set('to',$to);
	}

	function mainReport($id = null)
	{
		$show = false;
		if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
			$distid = $this->info['id'];
			$show = true;
			$this->set('name',$this->info['company']);
		}
		else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
			$sdistid = $this->info['id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['parent_id'] == $this->info['id'] || $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
				$this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
			$rmid = $this->info['id'];
			$sdistid = $this->info['super_dist_id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['rm_id'] == $this->info['id'] && $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
                $this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == ADMIN){
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,SUPER_DISTRIBUTOR);
				if(!empty($shop)){
					$sdistid = $id;
					$this->set('name',$shop['company']);
					$show = true;
					$this->set('dist',$sdistid);
				}
			}
			else {
				$this->set('name','All MasterDistributors');
				$show = true;
			}
		}

		if($show){
			$today = date('Y-m-d');
			$datas = array();

			if(!isset($sdistid) && !isset($distid)){//Admin with all SD's
				$data_buy = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions WHERE shop_transactions.confirm_flag != 1 AND ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER.") OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR.")) AND shop_transactions.date = '$today'");
				$data_sold = $this->Slaves->query("SELECT sum(amount) as amts,count(distinct ref2_id) as cts FROM shop_transactions WHERE confirm_flag != 1 AND type = ".DIST_RETL_BALANCE_TRANSFER." AND date = '$today'");
				$data_ret = $this->Slaves->query("SELECT count(retailers.id) as cts FROM retailers");
				$data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va WHERE va.retailer_id != 13 AND va.status != 2 AND va.status !=3 AND va.date = '$today'");

				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '2013-06-10' AND retailers.parent_id=64
				$data_bef = $this->Slaves->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE date >= '".date('Y-m-d',strtotime('-30 days'))."' group by date order by date");
				$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date) WHERE retailers_logs.retailer_id not in (13) AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(isset($sdistid) && !isset($distid)){//SD with all distributors or RM with all distributors or Admin with a SD
				if(isset($rmid))$extra = " AND distributors.rm_id = $rmid AND distributors.active_flag = '1'";
				else $extra = " AND distributors.parent_id = $sdistid AND distributors.active_flag = '1'";

				$data_buy = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions WHERE shop_transactions.confirm_flag != 1 AND (shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER.")  AND ref1_id = '".$this->info['id']."' and  shop_transactions.date = '$today'");
				$data_sold = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions FORCE INDEX(type_date),distributors WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = distributors.id $extra");
				$data_ret = $this->Slaves->query("SELECT count(retailers.id) as cts FROM retailers,distributors WHERE retailers.parent_id = distributors.id $extra");
				$data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va USE INDEX (idx_date),retailers,distributors WHERE va.status != 2 AND va.retailer_id != 13 AND va.status !=3 AND retailers.id = va.retailer_id AND retailers.parent_id = distributors.id AND va.date = '$today' $extra");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers,distributors WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND retailers.parent_id = distributors.id AND va.date = '$today' $extra
				$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs,distributors WHERE distributor_id  = distributors.id $extra AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers,distributors WHERE retailers.id not in (13) AND retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id $extra AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(isset($sdistid) && isset($distid)){//SD with a distributor OR RM with a distributor
				$extra = "";
				if(isset($rmid))$extra = " AND distributors.rm_id = $rmid";
				$data_buy = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions inner join distributors ON (distributors.id = shop_transactions.ref2_id)  WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER." AND shop_transactions.ref1_id = $sdistid AND shop_transactions.ref2_id = $distid AND distributors.parent_id = shop_transactions.ref1_id $extra  AND shop_transactions.date = '$today'");
				$data_sold = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions  WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = $distid");
				$data_ret = $this->Slaves->query("SELECT count(retailers.id) as cts FROM retailers WHERE retailers.parent_id = $distid");
				$data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.retailer_id != 13 AND va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '2013-06-10' AND retailers.parent_id=$distid

				$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id not in (13) AND retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(!isset($sdistid) && isset($distid)){//Distributor
				$data_buy = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions WHERE shop_transactions.confirm_flag != 1 AND ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER." AND shop_transactions.ref2_id = $distid) OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR." AND shop_transactions.ref1_id = $distid)) AND shop_transactions.date = '$today'");
				$data_sold = $this->Slaves->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions  WHERE shop_transactions.confirm_flag != 1 AND shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = $distid");
				$data_ret = $this->Slaves->query("SELECT count(retailers.id) as cts FROM retailers WHERE retailers.parent_id = $distid");
				$data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.retailer_id != 13 AND va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid
					
				$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id not in (13) AND retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}

			foreach($data_buy as $dt){
				$datas['buy'] = $dt['0']['amts'];
			}

			foreach($data_sold as $dt){
				$datas['sold'] = $dt['0']['amts'];
				$datas['unique'] = $dt['0']['cts'];
			}

			foreach($data_ret as $dt){
				$datas['retailers'] = $dt['0']['cts'];
			}

			foreach($data_trans as $dt){
				$datas['transacting'] = $dt['0']['cts'];
				$datas['sale'] = $dt['0']['sale'];
				$datas['percent_trans'] = $datas['retailers']==0 ? 0 : intval($datas['transacting']*100/$datas['retailers']);
				$datas['sale_avg_ret'] = empty($datas['transacting'])? 0 : intval($datas['sale']/$datas['transacting']);
				$datas['sale_avg'] = intval($datas['sale']);
			}
			
			
			
			foreach($data_bef_ret as $dt){
				$tertiarysale[$dt['retailers_logs']['date']] = array("sale" => $dt[0]['sale'],"transacting"=>$dt[0]['transacting']);
			}
			
			
			$yest_date = date('Y-m-d',strtotime('-1 days'));
			$week_date = date('Y-m-d',strtotime('-7 days'));
			$month_date = date('Y-m-d',strtotime('-30 days'));

			$datas_before = array();
			$ret_before=0;
			$ret_tot = 0;
			$datas_before['week']['new'] = 0;
			$datas_before['month']['new'] = 0;
			$i=0;
			$j=0;
			
			$datatopupsold = array();
			$datatransRetailer = array();
			$datanewOutlets = array();
			
		   foreach($data_bef as $dt){
				$datatopupsold[] = array($dt['distributors_logs']['date'],$dt[0]['topup_sold'],$dt[0]['topup_buy'],$tertiarysale[$dt['distributors_logs']['date']]['sale']);
				$datatransRetailer[] = array($dt['distributors_logs']['date'],intval($dt['0']['transacting']));
				if($dt['distributors_logs']['date']!=$month_date){
				$datanewOutlets[] = array($dt['distributors_logs']['date'],$dt['0']['retailers'] - $ret_before);
				}
				$dataUniqueTopups[] = array($dt['distributors_logs']['date'],$dt['0']['topup_unique']);
				
				
				
				if($dt['distributors_logs']['date'] == $yest_date){
					$datas_before['yesterday']['buy'] = $dt['0']['topup_buy'];
					$datas_before['yesterday']['sold'] = $dt['0']['topup_sold'];
					$datas_before['yesterday']['unique'] = $dt['0']['topup_unique'];
					$datas_before['yesterday']['retailers'] = $dt['0']['retailers'];
					$datas_before['yesterday']['transacting'] = $dt['0']['transacting'];
					$datas_before['yesterday']['new'] = $dt['0']['retailers'] - $ret_before;
					$datas_before['yesterday']['percent_trans'] = intval($dt['0']['transacting']*100/$dt['0']['retailers']);
				}

				if($dt['distributors_logs']['date'] >= $week_date){
					$datas_before['week']['percent_trans'] += $dt['0']['transacting']/$dt['0']['retailers'];
					$datas_before['week']['new'] = $datas_before['week']['new'] + $dt['0']['retailers'] - $ret_before;
					$datas_before['week']['buy'] +=  $dt['0']['topup_buy'];
					$datas_before['week']['sold'] +=  $dt['0']['topup_sold'];
					$datas_before['week']['unique'] +=  $dt['0']['topup_unique'];
					$i++;
				}
				$datas_before['month']['percent_trans'] += $dt['0']['transacting']/$dt['0']['retailers'];
				if($ret_before > 0)$datas_before['month']['new'] = $datas_before['month']['new'] + $dt['0']['retailers'] - $ret_before;
				$datas_before['month']['buy'] +=  $dt['0']['topup_buy'];
				$datas_before['month']['sold'] +=  $dt['0']['topup_sold'];
				$datas_before['month']['unique'] +=  $dt['0']['topup_unique'];
				$ret_before = $dt['0']['retailers'];
				$j++;
			}
			
			
			
			$datatransRetailer[] = array(date('Y-m-d'),intval($datas['transacting']));
			//$datanewOutlets[] = array(date('Y-m-d'),$datas['retailers'] - $ret_before);
			$dataUniqueTopups[] = array(date('Y-m-d'),$datas['unique']);
			
			$datas_before['week']['percent_trans'] = empty($datas_before['week']['percent_trans'])? 0 : intval($datas_before['week']['percent_trans']*100/$i);
			$datas_before['week']['buy'] = empty($datas_before['week']['buy']) ? 0 : intval($datas_before['week']['buy']/$i);
			$datas_before['week']['sold']= empty($datas_before['week']['sold'])? 0 : intval($datas_before['week']['sold']/$i);
			$datas_before['week']['unique'] = empty($datas_before['week']['unique']) ? 0 : intval($datas_before['week']['unique']/$i);

			$datas_before['month']['percent_trans'] = empty($datas_before['month']['percent_trans'])? 0 : intval($datas_before['month']['percent_trans']*100/$j);
			$datas_before['month']['buy'] = empty($datas_before['month']['buy']) ? 0 : intval($datas_before['month']['buy']/$j);
			$datas_before['month']['sold'] = empty($datas_before['month']['sold']) ? 0 : intval($datas_before['month']['sold']/$j);
			$datas_before['month']['unique'] = empty($datas_before['month']['unique']) ? 0 :intval($datas_before['month']['unique']/$j);

			if(isset($datas_before['yesterday']['retailers'])){
				$datas['new'] = $datas['retailers'] - $datas_before['yesterday']['retailers'];
			}
			$i = 0;
			$j = 0;
			$retialerSale = array();
			foreach($data_bef_ret as $dt){
				$retialerSale[] = array($dt['retailers_logs']['date'],$dt[0]['sale']);
				$retailerAvgSale[] = array($dt['retailers_logs']['date'],intval($dt[0]['sale']/$dt[0]['transacting']));
				if($dt['retailers_logs']['date'] == $yest_date){
					$datas_before['yesterday']['sale'] = $dt['0']['sale'];
					$datas_before['yesterday']['sale_avg_ret'] = intval($datas_before['yesterday']['sale']/$datas_before['yesterday']['transacting']);
				}
				if($dt['retailers_logs']['date'] >= $week_date){
					$datas_before['week']['sale'] += isset($dt['0']['sale']) ? $dt['0']['sale'] : 0;
					$datas_before['week']['sale_avg_ret'] += isset($dt['0']['sale']) ? ($dt['0']['sale']/$dt['0']['transacting']) : 0;
					$i++;
				}
				$datas_before['month']['sale'] += $dt['0']['sale'];
				$datas_before['month']['sale_avg_ret'] += $dt['0']['sale']/$dt['0']['transacting'];
				$j++;
			}

			$retialerSale[] =array(date('Y-m-d'),$datas['sale']);
			$retailerAvgSale[] = array(date('Y-m-d'),intval($datas['sale']/$datas['transacting']));

			$datas_before['yesterday']['sale_avg'] = isset($datas_before['yesterday']['sale']) ? $datas_before['yesterday']['sale'] : 0;
			$datas_before['week']['sale_avg'] = isset($datas_before['week']['sale']) ? intval($datas_before['week']['sale']/$i) : 0;
			$datas_before['month']['sale_avg'] = isset($datas_before['month']['sale'])? intval($datas_before['month']['sale']/$j) : 0;
			$datas_before['week']['sale_avg_ret'] = (isset($datas_before['week']['sale_avg_ret']) && $i != 0) ? intval($datas_before['week']['sale_avg_ret']/$i) : 0;
			$datas_before['month']['sale_avg_ret'] = (isset($datas_before['month']['sale_avg_ret']) && $j != 0) ? intval($datas_before['month']['sale_avg_ret']/$j) : 0;



			$graphData = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Topup sold today'),
							array('number' => 'Topup buy today'),
							array('number' => 'Tertiary Sale')
					),
					'data' => $datatopupsold,
					'title' => 'Topup sold/day',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData1 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'transacting Retailers')
					),
					'data' => $datatransRetailer,
					'title' => 'transacting Retailers',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData2 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'New outlets opened')
					),
					'data' => $datanewOutlets,
					'title' => 'New outlets opened',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData3 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Unique topups/day')
					),
					'data' => $dataUniqueTopups,
					'title' => 'Unique topups/day',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);

			$graphData5 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Avg Sale/Retailer')
					),
					'data' => $retailerAvgSale,
					'title' => 'Avg Sale/Retailer',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			
			$this->set('data_today',$datas);
			$this->set('data_before',$datas_before);
			$this->set('data1',$graphData);
			$this->set('data2',$graphData1);
			$this->set('data3',$graphData2);
			$this->set('data4',$graphData3);
			//$this->set('data5',$graphData4);
			$this->set('data6',$graphData5);
		}

		if(!empty($id))$this->set('id',$id);
	}


	function overallReport($date=null){
		
		if($date == null){
			$date = date('dmY',strtotime('-30 days')) . '-' . date('dmY',strtotime('-1 days'));
		}
		 
		$dates = explode("-",$date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);

			if($date_to == date('Y-m-d')){
				$date_to = date('Y-m-d',strtotransactingtime('-1 days'));
			}

			if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
                $data = $this->Slaves->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp,users.mobile,distributors.name as distname,distributors.city,distributors.created as created_date,distributors.margin as margin,sum(topup_unique) as secondrytxn,sum(primary_txn) as primarytxn,benchmark_value as benchmark_tervalue,transacting_retailer as trans_retailer,avg(transacting)as transacting,rm.name as rmname FROM `distributors_logs`,distributors,users,rm WHERE distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND distributors.user_id=users.id AND rm.id = distributors.rm_id  AND date >= '$date_from' AND date <= '$date_to' AND distributors.active_flag= '1' group by distributor_id");
				$data_ter = $this->Slaves->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp,sum(retailers_logs.transactions)as transactions,count(distinct retailer_id) as uniqueret FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' AND distributors.active_flag= '1' group by distributors.id");
                $prevdata = $this->Slaves->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp,users.mobile,distributors.name as distname,distributors.city,sum(topup_unique) as secondrytxn,sum(primary_txn) as primarytxn,benchmark_value as benchmark_tervalue,transacting_retailer as trans_retailer,rm.name as rmname FROM `distributors_logs`,distributors,users,rm WHERE distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND distributors.user_id=users.id AND rm.id = distributors.rm_id  AND date >= '".date('Y-m-d',strtotime($date_from.'-1 months'))."' AND date <= '".date('Y-m-d',strtotime($date_to.'-1 months'))."' AND distributors.active_flag= '1' group by distributor_id");         
				$prev_data_ter = $this->Slaves->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp,sum(retailers_logs.transactions)as transactions FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '".date('Y-m-d',strtotime($date_from.'-1 months'))."' AND retailers_logs.date <= '".date('Y-m-d',strtotime($date_to.'-1 months'))."' AND distributors.active_flag= '1' group by distributors.id");
				
				$incentiveRefund = $this->Slaves->query("SELECT ref1_id,sum(shop_transactions.amount) as incentive,'discount' as name  FROM `shop_transactions` inner join distributors on distributors.id =  shop_transactions.ref1_id  WHERE  shop_transactions.type = '".COMMISSION_DISTRIBUTOR."'   and   date>='$date_from' and date<='$date_to' and distributors.rm_id = ".$this->info['id']." group by ref1_id 
                                                           UNION 
                                                          SELECT ref1_id,sum(shop_transactions.amount)  as refund,'refund' as name FROM `shop_transactions` inner join distributors on shop_transactions.ref1_id =  distributors.id   WHERE  shop_transactions.type = '".REFUND."' and  ref2_id  = '".DISTRIBUTOR."'  and shop_transactions.date>='$date_from' and  shop_transactions.date<='$date_to' and distributors.rm_id  = '".$this->info['id']."'group by ref1_id");
				$rmdetails = $this->Slaves->query("SELECT rm.name,distributors.id from rm left join distributors on rm.id = distributors.created_rm_id where rm.id = '".$this->info['id']."'");
			    $getNewretailer = $this->Slaves->query("select retailers.parent_id,count(retailers.id) as new_retailers from retailers inner join distributors on distributors.id = retailers.parent_id left join rm on distributors.rm_id = rm.id where date(retailers.created)>='$date_from' and date(retailers.created)<='$date_to' and distributors.rm_id = '".$this->info['id']."' group by distributors.id");
				$totalBaseRet = $this->Slaves->query("select distributors.id as distId,count(*) as base_ret from retailers inner join distributors on retailers.parent_id = distributors.id left join rm on rm.id = distributors.rm_id  where date(retailers.created)<='$date_to' and distributors.rm_id ='".$this->info['id']."' group by retailers.parent_id");
				
			}
			else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
				$data = $this->Slaves->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp,distributors.state,users.mobile,distributors.name as distname,distributors.city,distributors.created as created_date,distributors.margin as margin,sum(topup_unique) as secondrytxn,sum(primary_txn) as primarytxn,benchmark_value as benchmark_tervalue,transacting_retailer as trans_retailer,avg(transacting)as transacting,rm.name as rmname FROM `distributors_logs` INNER JOIN distributors ON (distributors.id = distributor_id) INNER JOIN users ON (distributors.user_id=users.id)  left join rm ON (distributors.rm_id = rm.id)  WHERE distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND date >= '$date_from' AND date <= '$date_to' AND distributors.active_flag= '1' group by distributor_id");
				$data_ter = $this->Slaves->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp,sum(retailers_logs.transactions) as transactions,count(distinct retailer_id) as uniqueret FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' and distributors.active_flag= '1' group by distributors.id");
				$prevdata = $this->Slaves->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp,distributors.state,users.mobile,distributors.name as distname,distributors.city,distributors.created as discreated,distributors.margin as distmargin,sum(topup_unique) as secondrytxn,sum(primary_txn) as primarytxn,benchmark_value as benchmark_tervalue,transacting_retailer as trans_retailer,rm.name as rmname FROM `distributors_logs` INNER JOIN distributors ON (distributors.id = distributor_id) INNER JOIN users ON (distributors.user_id=users.id)  left join rm ON (distributors.rm_id = rm.id)  WHERE distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND date >= '".date('Y-m-d',strtotime($date_from.'-1 months'))."' AND date <= '".date('Y-m-d',strtotime($date_to.'-1 months'))."' AND distributors.active_flag= '1' group by distributor_id");
				$prev_data_ter = $this->Slaves->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp,sum(retailers_logs.transactions) as transactions FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '".date('Y-m-d',strtotime($date_from.'-1 months'))."' AND retailers_logs.date <= '".date('Y-m-d',strtotime($date_to.'-1 months'))."' AND distributors.active_flag= '1' group by distributors.id");
				$incentiveRefund = $this->Slaves->query("SELECT ref1_id,sum(shop_transactions.amount) as incentive,'discount' as name  FROM `shop_transactions` WHERE  shop_transactions.type = '".COMMISSION_DISTRIBUTOR."'   and   date>='$date_from' and date<='$date_to' group by ref1_id 
                                                           UNION 
                                                           SELECT ref1_id,sum(shop_transactions.amount)  as refund,'refund' as name FROM `shop_transactions` inner join distributors on shop_transactions.ref1_id =  distributors.id   WHERE  shop_transactions.type = '".REFUND."' and  ref2_id  = '".DISTRIBUTOR."'  and shop_transactions.date>='$date_from' and  shop_transactions.date<='$date_to' and distributors.parent_id  = '".$this->info['id']."'group by ref1_id");
				$rmdetails = $this->Slaves->query("SELECT rm.name,distributors.id from rm left join distributors on rm.id = distributors.created_rm_id where distributors.parent_id = '".$this->info['id']."' group by distributors.id");
			    $getNewretailer = $this->Slaves->query("SELECT retailers.parent_id,count(retailers.id) as new_retailers from retailers inner join distributors on distributors.id = retailers.parent_id where date(retailers.created)>='$date_from' and date(retailers.created)<='$date_to' and distributors.parent_id = '".$this->info['id']."' group by distributors.id");
				$totalBaseRet = $this->Slaves->query("select distributors.id as distId,count(*) as base_ret from retailers inner join distributors on retailers.parent_id = distributors.id where date(retailers.created)<='$date_to' and distributors.parent_id = '".$this->info['id']."' group by retailers.parent_id");
				
			}
			else if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
				$data = array();//$this->Retailer->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `distributors_logs`,distributors WHERE distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by distributor_id");
				$data_ter = $this->Slaves->query("SELECT retailers.id as rid , retailers.shopname as rname , retailers.mobile as rmobile , sum(sale) as sum_sale , sum(sale-ussd_sale-sms_sale) as sum_app_sale , sum(ussd_sale) as sum_ussd_sale , sum(sms_sale) as sum_sms_sale ,  sum(topup) as sum_topup  FROM `retailers_logs`,retailers WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = ".$this->info['id']." AND date >= '$date_from' AND date <= '$date_to'  group by retailers.id order by sum_sale desc");
				$this->set('datas',$data_ter);
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
				$this->render('retailer_overall_report');
			}
			else {
				$data = $this->Slaves->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(super_distributors.id) as rid,trim(super_distributors.company) as comp FROM `distributors_logs`,distributors,super_distributors WHERE distributors.parent_id = super_distributors.id AND super_distributors.id NOT IN (".SDISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by super_distributors.id");
				$data_ter = $this->Slaves->query("SELECT sum(sale) as sale,trim(super_distributors.id) as rid,trim(super_distributors.company) as comp FROM `retailers_logs`,retailers,distributors,super_distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.parent_id = super_distributors.id AND super_distributors.id NOT IN (".SDISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' group by super_distributors.id");
			}
			
			$list = array();
			foreach($data as $dt){
				$list[$dt['0']['rid']]['primary'] = $dt['0']['prim'];
				$list[$dt['0']['rid']]['secondary'] = $dt['0']['second'];
				$list[$dt['0']['rid']]['newr'] = $dt['0']['newr'];
				$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
				$list[$dt['0']['rid']]['tertiary'] = 0;
				$list[$dt['0']['rid']]['id'] = $dt['0']['rid'];
                $list[$dt['0']['rid']]['mobile'] = $dt['users']['mobile'];
                $list[$dt['0']['rid']]['city'] = $dt['distributors']['city'];
				$list[$dt['0']['rid']]['state'] = $dt['distributors']['state'];
                $list[$dt['0']['rid']]['distributor_name'] = $dt['distributors']['distname'];
                $list[$dt['0']['rid']]['primary_txn'] = $dt[0]['primarytxn'];
                $list[$dt['0']['rid']]['secondry_txn'] = $dt[0]['secondrytxn'];
                $list[$dt['0']['rid']]['benchmark_tertiary'] = $dt['distributors']['benchmark_tervalue'];
                $list[$dt['0']['rid']]['transacting_retailer'] = $dt['distributors']['trans_retailer'];
				$list[$dt['0']['rid']]['transacting'] = $dt[0]['transacting'];
                $list[$dt['0']['rid']]['rmname'] = $dt['rm']['rmname'];
				$list[$dt['0']['rid']]['created_date'] = $dt['distributors']['created_date'];
				$list[$dt['0']['rid']]['margin'] = $dt['distributors']['margin'];
			}
			foreach($prevdata as $dt){
				$list[$dt['0']['rid']]['prev_primary'] = $dt['0']['prim'];
				$list[$dt['0']['rid']]['prev_secondary'] = $dt['0']['second'];
				$list[$dt['0']['rid']]['newr'] = $dt['0']['newr'];
				$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
				$list[$dt['0']['rid']]['prev_tertiary'] = 0;
                $list[$dt['0']['rid']]['prev_primary_txn'] = $dt[0]['primarytxn'];
                $list[$dt['0']['rid']]['prev_secondry_txn'] = $dt[0]['secondrytxn'];
                $list[$dt['0']['rid']]['prev_benchmark_tertiary'] = $dt['distributors']['benchmark_tervalue'];
                $list[$dt['0']['rid']]['prev_ransacting_retailer'] = $dt['distributors']['trans_retailer'];
                $list[$dt['0']['rid']]['rmname'] = $dt['rm']['rmname'];
				
			}
			foreach ($data_ter as $dt) {
				if ($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR) {
					$list[$dt['0']['rid']]['tertiary'] = $dt['0']['sale'];
					$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
					$list[$dt['0']['rid']]['tertiary_txn'] = $dt['0']['transactions'];
					$list[$dt['0']['rid']]['unique_ret'] = $dt['0']['uniqueret'];
				}
			}

			foreach ($prev_data_ter as $dt) {
				if ($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR) {
					$list[$dt['0']['rid']]['prev_tertiary'] = $dt['0']['sale'];
					$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
					$list[$dt['0']['rid']]['prev_tertiary_txn'] = $dt['0']['transactions'];
				}
			}
			
			foreach ($incentiveRefund as $val) {
				if (isset($list[$val[0]['ref1_id']])):
					$list[$val[0]['ref1_id']][$val[0]['name']] = $val[0]['incentive'];
				endif;
			}
			
			foreach ($rmdetails as $val) {
				if (isset($list[$val['distributors']['id']])) {
					$list[$val['distributors']['id']]['created_rm'] = $val['rm']['name'];
				}
			}

			foreach ($getNewretailer as $val) {
				if (isset($list[$val['retailers']['parent_id']])) {
					$list[$val['retailers']['parent_id']]['new_retailers'] = $val[0]['new_retailers'];
				}
			}
			
			foreach($totalBaseRet as $val){
				if (isset($list[$val['distributors']['distId']])) {
					
					$list[$val['distributors']['distId']]['base_retailers'] = $val[0]['base_ret'];
				}
				
			}
			
			$date1 = new DateTime($date_from);
			
            $date2 = date("Y-m-d", strtotime("+1 day", strtotime($date_to)));
			
			$date2 = new DateTime($date2);
			
			$diffdays = $date2->diff($date1)->format("%a");
		
			$pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
            $list = Set::sort($list, '{[0-9]}.tertiary', 'desc');
			$this->set('datas',$list);
			$this->set('date_from',$date_from);
			$this->set('date_to',$date_to);
			$this->set('diffdays',$diffdays);
            $this->set('pageType',$pageType);
            if($pageType == 'csv'){
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			//$line = array("Txn Date","Txn Id","Signal7 T_ID","Number","Operator","Amount","Opening","Closing","Earning","Reversal Date","Description","Status");
			$line = array("S.No.","City","State","Distributor","id","Reg Date","Margin Slab","Mob No.","LEAD RM","CURRENT RM","Primary Value","Primary Txn","Primary Avg","Previous Primary Avg","SecondaryValue","Secondary Unique Retailer","Secondry Avg","Previous Secondry-Avg","Tertiary Value","Tertiary Txn","Tertiary Avg","Previous Tertiary-Avg","Half Yearly Benchmark-Avg Tertiary Value INR","New Retailer","Total Retailer Base","Total Transacting-Retailer","Half Yearly Benchmark - Avg Transacting Retailer","Discount Recevied","Incentive Recevied");
			$csv->addRow($line);
			$i=1;
			foreach ($list as $data) {
               
				$primaryavg  = intval($data['primary']/$diffdays);
                $secondryavg  = intval($data['secondary']/$diffdays);
				$tertiaryavg  = intval($data['tertiary']/$diffdays);
				$prevprimaryavg = intval($data['prev_primary']/$diffdays);
				$prevsecondryavg = intval($data['prev_secondary']/$diffdays);
				$prevtertiaryavg = intval($data['prev_tertiary']/$diffdays);
				$temp = array($i, $data['city'],$data['state'], $data['company'],$data['id'],date('Y-m-d',  strtotime($data['created_date'])),$data['margin'],$data['mobile'],$data['created_rm'],$data['rmname'],$data['primary'],$data['primary_txn'],$primaryavg,$prevprimaryavg,$data['secondary'],$data['secondry_txn'],$secondryavg,$prevsecondryavg,$data['tertiary'],$data['tertiary_txn'],$tertiaryavg,$prevtertiaryavg,$data['benchmark_tertiary'],$data['new_retailers'],$data['base_retailers'],$data['unique_ret'],$data['transacting_retailer'],$data['discount'],$data['refund']);
				$csv->addRow($temp);
				$i++;
			}
            
            echo $csv->render('overall_report'.date('YmdHis').'.csv');

		} else {
            
            $this->render("overall_report");
        }
			
		}
	}


	function sReport($date=null,$state=0,$rm_id=0)
	{
		
		//--------------
		$qryPart = "";
                $dates = explode("-",$date);
                $date_from = $dates[0];
                $date_to = $dates[1];
		if( !(empty($date_from) || empty($date_to)) ){
			

			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				if($date_to == date('Y-m-d')){
					$date_to = date('Y-m-d');//,strtotime('-1 days')
				}
			}
			$this->set('date_from',$date_from);
			$this->set('date_to',$date_to);
			$qryPart = " AND Date(distributors.created) >= '$date_from' AND Date(distributors.created) <= '$date_to'";
                        
		}              
                if(!empty($state)){
                    $qryPart = $qryPart." AND distributors.state like '$state' AND distributors.active_flag='1'"; 
                }
                if(!empty($rm_id)){
                    $qryPart = $qryPart." AND distributors.rm_id = '$rm_id' AND distributors.active_flag='1'"; 
                }
		//---------------		
                $today = date('Y-m-d');
		$yest_date = date('Y-m-d',strtotime('-1 days'));
		$week_date = date('Y-m-d',strtotime('-7 days'));
		$month_date = date('Y-m-d',strtotime('-30 days'));
		
                $rmList = $this->Slaves->query("SELECT id , name FROM `rm` Where  active_flag = 1 ORDER BY name"); //WHERE toShow = 0
               
                $this->set('rmList',$rmList);
                $this->set('state',$state);
                $this->set('rm_id',$rm_id);
                
                $datas = array();
			
		if($_SESSION['Auth']['User']['group_id'] == ADMIN){

			foreach($this->sds as $sdist){
				/*if($date != null){
					if(!($sdist['SuperDistributor']['created'] >= $date_from && $sdist['SuperDistributor']['created'] <= $date_to))  continue;
					}*/
				$sdistid = $sdist['SuperDistributor']['id'];
                $data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va USE INDEX(idx_date),retailers , distributors  WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = distributors.id AND distributors.parent_id=$sdistid AND distributors.active_flag='1'");
				//echo "SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers , distributors  WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = distributors.id AND distributors.parent_id=$sdistid  $qryPart";
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = distributors.id AND distributors.parent_id=$sdistid
				$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id AND distributors.parent_id = $sdistid AND distributors.active_flag='1' AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
				$i = 0;
				$j = 0;
				$datas_before = array();
				$datas_before['yesterday'] = 0;
				$datas_before['name'] = $sdist['SuperDistributor']['company'];
				$datas_before['today'] = $data_trans['0']['0']['sale'];
				foreach($data_bef_ret as $dt){
					if($dt['retailers_logs']['date'] == $yest_date){
						$datas_before['yesterday'] = $dt['0']['sale'];
					}
					if($dt['retailers_logs']['date'] >= $week_date){
						$datas_before['week'] += $dt['0']['sale'];
						$i++;
					}
					$datas_before['month'] += $dt['0']['sale'];
					$j++;
				}

				$datas_before['week'] = isset($datas_before['week'])&& !empty($i) ? intval($datas_before['week']/$i):0;
				$datas_before['month'] = isset($datas_before['month']) && !empty($j) ? intval($datas_before['month']/$j) : 0;

				$datas[] = $datas_before;
			}
		}
		else {
                        $data_trans = $this->Slaves->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale,distributors.id,distributors.company FROM vendors_activations as va USE INDEX (idx_date),retailers , distributors WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND retailers.parent_id = distributors.id AND va.date = '$today' AND distributors.parent_id = ".$_SESSION['Auth']['id']." $qryPart GROUP BY distributors.id");
                        $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date,distributors.id,distributors.company FROM retailers_logs,retailers ,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id = distributors.id AND distributors.parent_id= ".$_SESSION['Auth']['id']." AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' $qryPart  group by distributors.id,retailers_logs.date order by distributors.id,retailers_logs.date");

			foreach($data_bef_ret as $dist){
				$distid = $dist['distributors']['id'];
				$datas[$distid]['name'] = $dist['distributors']['company'];

				if(!isset($datas[$distid]['yesterday']))$datas[$distid]['yesterday'] = 0;
				if(!isset($datas[$distid]['week'])){
					$datas[$distid]['week'] = 0;
					$datas[$distid]['week_count'] = 0;
				}
				if(!isset($datas[$distid]['month'])){
					$datas[$distid]['month'] = 0;
					$datas[$distid]['month_count'] = 0;
				}

				if($dist['retailers_logs']['date'] == $yest_date){
					$datas[$distid]['yesterday'] = $dist['0']['sale'];
				}
				if($dist['retailers_logs']['date'] >= $week_date){
					$datas[$distid]['week'] += $dist['0']['sale'];
					$datas[$distid]['week_count'] ++;
				}
				$datas[$distid]['month'] += $dist['0']['sale'];
				$datas[$distid]['month_count'] ++;
			}
				
			foreach($data_trans as $dist){
				$distid = $dist['distributors']['id'];
				$datas[$distid]['name'] = $dist['distributors']['company'];
				$datas[$distid]['today'] = $dist['0']['sale'];
			}
				
			$datas1 = array();
			foreach($this->ds as $dist){
				$distid = $dist['Distributor']['id'];
				if(isset($datas[$distid])){
					if(isset($datas[$distid]['week_count']) && $datas[$distid]['week_count'] > 0){
						$datas[$distid]['week'] = intval($datas[$distid]['week']/$datas[$distid]['week_count']);
					}
					else $datas[$distid]['week'] = 0;
					if(isset($datas[$distid]['month_count']) && $datas[$distid]['month_count'] > 0){
						$datas[$distid]['month'] = intval($datas[$distid]['month']/$datas[$distid]['month_count']);
					}
					else $datas[$distid]['month'] = 0;
					if(!isset($datas[$distid]['yesterday']))$datas[$distid]['yesterday'] = 0;
					if(!isset($datas[$distid]['today']))$datas[$distid]['today'] = 0;
					$datas1[$distid] = $datas[$distid];
				}
				else {
					if($date != null){
						if(!(date('Y-m-d',strtotime($dist['Distributor']['created'])) >= $date_from && date('Y-m-d',strtotime($dist['Distributor']['created'])) <= $date_to))  continue;
					}
						
					$datas1[$distid]['name'] = $dist['Distributor']['company'];
					$datas1[$distid]['week'] = 0;
					$datas1[$distid]['month'] = 0;
					$datas1[$distid]['yesterday'] = 0;
					$datas1[$distid]['today'] = 0;
				}

			}
				
			$datas = $datas1;
		}

		$this->set('datas',$datas);
	}

	function refundRetailer($ret_id,$amt){
//		if($_SESSION['Auth']['User']['group_id'] != ADMIN && $_SESSION['Auth']['User']['group_id'] != ACCOUNTS){
//			$this->redirect('/');
//		}

		if(empty($ret_id) || empty($amt)){
			echo "Please hit with id & amount as well";
			exit;
		}
		else if($amt > 1000){
			echo "You are not allowed to transfer more than Rs1000 at one time";
			exit;
		}
		$shop = $this->Shop->getShopDataById($ret_id,RETAILER);

		if(!empty($shop)){
			$trans_id = $this->Shop->shopTransactionUpdate(REFUND,$amt,$ret_id,RETAILER);
			$bal = $this->Shop->shopBalanceUpdate($amt,'add',$ret_id,RETAILER);
			$this->Shop->addOpeningClosing($ret_id,RETAILER,$trans_id,$bal-$amt,$bal);

//			$sms = "Dear Retailer,\nYou have got refund of Rs $amt from Pay1 company";
//			$sms .= "\nYour current balance is now: Rs. " .$bal;
                        
                        $paramdata['AMOUNT'] = $amt;
                        $paramdata['BALANCE'] = $bal;
                        $MsgTemplate = $this->General->LoadApiBalance();
                        $content =  $MsgTemplate['Retailer_Refund_MSG'];
                        $sms = $this->General->ReplaceMultiWord($paramdata,$content);

			$this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (".RETAILER.",$trans_id,$amt,2,'$sms','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')");
			$this->General->sendMessage($shop['mobile'],$sms,'notify');
			$mail_body = "Incentive of Rs $amt given to retailer ".$shop['mobile'] . " (" . $shop['shopname']. ")";
			$mail_body .= "<br/>Retailer's balance after this transaction is $bal";
			$this->General->sendMails("Incentive to Retailer", $mail_body,array('dharmesh@mindsarray.com','sunilr@pay1.in','naziya@mindsarray.com'),'mail');
			echo $mail_body;
		}
		else {
			
			echo "Retailer id is not right";
		}

		$this->autoRender = false;
	}

	function incentiveDistributor($id,$amt){
//		if($_SESSION['Auth']['User']['group_id'] != ADMIN){
//			$this->redirect('/');
//		}

		if(empty($id) || empty($amt)){
			echo "Please hit with id & amount as well";
			exit;
		}
		else if($amt > 20000){
			echo "You are not allowed to transfer more than Rs20000 at one time";
			exit;
		}

		$shop = $this->Slaves->query("SELECT mobile,distributors.company FROM users,distributors WHERE distributors.user_id = users.id AND distributors.id = $id");
		if(!empty($shop)){
			$trans_id = $this->Shop->shopTransactionUpdate(REFUND,$amt,$id,DISTRIBUTOR);
			$bal = $this->Shop->shopBalanceUpdate($amt,'add',$id,DISTRIBUTOR);
			$this->Shop->addOpeningClosing($id,DISTRIBUTOR,$trans_id,$bal-$amt,$bal);

//			$sms = "Dear Distributor,\nYou have got incentive of Rs $amt from Pay1 company";
//			$sms .= "\nYour current balance is now: Rs. " .$bal;

                        $paramdata['AMOUNT'] = $amt;
                        $paramdata['BALANCE'] = $bal;
                        $MsgTemplate = $this->General->LoadApiBalance();
                        $content =  $MsgTemplate['Distributor_Incentive_MSG'];
                        $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                        
                        $this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (".DISTRIBUTOR.",$trans_id,$amt,1,'$sms','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')");
			$this->General->sendMessage($shop['0']['users']['mobile'],$sms,'notify',$bal,DISTRIBUTOR);
				
			$mail_body = "Incentive of Rs $amt given to distributor ".$shop['0']['distributors']['company'];
			$mail_body .= "<br/>Distributor's balance after this transaction is $bal";
			$this->General->sendMails("Incentive to Distributor", $mail_body,array('jimmit@mindsarray.com','nivedita@mindsarray.com','rm@mindsarray.com','tadka@mindsarray.com','sunilr@pay1.in'),'mail');
			echo $mail_body;
		}
		else {
			echo "Distributor id is not right";
		}
		$this->autoRender = false;
	}
        
	function changeDistributorMobileNo($oldNo,$newNo){
            
            App::import('Controller', 'Apis');
            $obj = new ApisController;
            $obj->constructClasses();
            
            $verify_otp = 0;
            $verify_otp_fail = 0;
            

            if($this->RequestHandler->isPost()) {
                
                $data = $this->params['form'];
                
                
                if($data['otp']){
                    
                    $verify_param['mobile'] = $data['super_dist_mob'];
                    $verify_param['otp'] =    $data['otp']; 
                    $verify_param['interest'] =   'Distributor'; 
                    
                    $verifyData =  $obj->verifyOTP($verify_param);

                    //Return Failure for if number not changes

                    if($verifyData['status'] =='failure'){
                        
                        $verify_otp_fail = 1;

                        $verify_response = array(
                            'status' => FALSE ,
                            'errors' => array('code'=>'E000','msg'=>'Please enter correct OTP.')
                        ) ;   

                    }else{
                    
                    $verify_otp = 1;   
                    
                    }
                }
                
                $oldNo = isset($data['old_mob_num']) ? $data['old_mob_num'] : 0 ;
                $newNo = isset($data['new_mob_num']) ? $data['new_mob_num'] : 0 ;
 
                
//            	$response = array();
//		$this->autoRender = FALSE;
//		exit;
		
            
		//if(empty($_SESSION['Auth']) || ($_SESSION['Auth']['User']['group_id'] != ADMIN)){
			//$response = array(
              ////                      'status' => FALSE ,
               //                     'errors' => array('code'=>'E000','msg'=>'UnAuthorized Access.')
			//) ;
		//}
                
                if((empty ($oldNo) || empty ($newNo)) && !$verify_otp){
			$response = array(
                                    'status' => FALSE ,
                                   'errors' => array('code'=>'E001','msg'=>'Please enter old and new Distributor Mobile Number.')
			) ;
                

                }else{

                    $oldData = $this->User->query("SELECT users.id,users.group_id,users.mobile,distributors.id,distributors.user_id,distributors.company, distributors.parent_id FROM users inner join distributors ON (distributors.user_id = users.id) WHERE mobile = '".$oldNo."' and group_id = ".DISTRIBUTOR);

                    $superDistData = $this->User->query("SELECT users.mobile FROM users inner join super_distributors ON (super_distributors.user_id = users.id) WHERE super_distributors.id = '".$oldData[0]['distributors']['parent_id']."' ");
                    
                    //Super Distributor Mobile Number of Old Distributor Mobile Number
                    $data['super_dist_mob']  =  $superDistData[0]['users']['mobile'];  
                    
                    if(empty ($oldData)  && !$verify_otp){
				//error user-distributor does'nt exist
				$response = array(
                                            'status' => FALSE ,
                                        'errors' => array('code'=>'E002','msg'=>'Distributor (User) not exist for Given mobile number .')
				) ;

                    }else if(empty ($superDistData)  && !$verify_otp){
                    
                        //error user-distributor does'nt exist
                            $response = array(
                                        'status' => FALSE ,
                                        'errors' => array('code'=>'E002','msg'=>'Super Distributor (User) not exist for Given Distributor mobile number .')
                            ) ;

                    }else{
                        
				//$oldDistData = $this->User->query("SELECT id,user_id,company FROM distributors WHERE user_id = '".$oldData[0]['users']['id']."'");
				$newData = $this->User->query("SELECT * FROM users WHERE mobile = '".$newNo."'");
                    
				if(empty ($newData)){//check if new_no not already exist )
					//update the previous number
                                
                                if($verify_otp){
                                    
					$this->User->query("update users set mobile = '".$newNo."' where id = ".$oldData[0]['users']['id']);
                    
                            $response = array(
                                            'status' => true ,
                                        'success' => array('code'=>'S001','msg'=>'Distributor Number changed successfully .')
					) ;
					$data['OLD_NUMBER'] = $oldNo;
					$data['NEW_NUMBER'] = $newNo;
					$MsgTemplate = $this->General->LoadApiBalance();
					$content =  $MsgTemplate['Change_Distributor_Number'];
                    $sms = $this->General->ReplaceMultiWord($data,$content);
					
					$this->General->sendMessage($oldNo.",".$newNo,$sms,'shops');
					
                                }else if(!$verify_otp_fail){
					
                                    $sendOTPdata['mobile'] = $data['super_dist_mob'];
                                    $sendOTPdata['interest'] = 'Distributor';
                                    $sendOTPdata['change_dist_mob_otp_flag'] = 1;
					
                                    $obj->sendOTPToRetDistLeads($sendOTPdata);
                                }
					//sms code

				}else{//( if  existing number )
					if($newData[0]['users']['group_id'] != MEMBER){// if existing user is not a member then error

						$response = array(
                                            'status' => false ,
                                            'errors' => array('code'=>'E004','msg'=>'Given New Mobile No is already exist ')
						) ;
					}else{// existing user is a member

						//swap numbers in user table
						$dumyNo = '1111111111';// because of unique constraint
						$rn = $this->User->query("update users set mobile = '$dumyNo' where id = ".$oldData[0]['users']['id']);
						$ro = $this->User->query("update users set mobile = '$oldNo' where id = ".$newData[0]['users']['id']);//" , group_id = ".MEMBER.
						$rn = $this->User->query("update users set mobile = '$newNo' where id = ".$oldData[0]['users']['id']);//" , group_id = ".DISTRIBUTOR.

						$response = array(
                                            'status' => true ,
                                            'success' => array('code'=>'S001','msg'=>'Number changed successfully .')
						) ;
					}
				}
					
				if($response['status']){
					$oldSalesMen = $this->User->query("SELECT id,dist_id FROM salesmen WHERE mobile = '".$oldNo."'");
					if(empty($oldSalesMen)){// if salesmen entry not exist for given user
						$salesman = array(
							'dist_id' 	=> 	$oldData[0]['distributors']['id'],
							'name'		=>	"Default",
							'mobile'	=>	'$oldNo',
							'tran_limit'=>	'100000',
							'balance'	=>	'100000'
						);
						$this->insertSalesman($salesman);
					}
					else if($oldSalesMen['0']['salesmen']['dist_id'] != $oldData[0]['distributors']['id']){
						$this->User->query("UPDATE salesmen SET dist_id = '".$oldData[0]['distributors']['id']."',tran_limit='100000',balance='100000' WHERE id = ".$oldSalesMen['0']['salesmen']['id']);
					}

					$newSalesMen = $this->User->query("SELECT id,dist_id FROM salesmen WHERE mobile = '".$newNo."'");
					if(empty($newSalesMen)){//ONLY UPDATE SALESMEN NUMBER
						$newData = $this->User->query("update salesmen set mobile = '".$newNo."' where mobile = '".$oldNo."'");
					}
					else if($newSalesMen['0']['salesmen']['dist_id'] != $oldData[0]['distributors']['id']){
						$this->User->query("UPDATE salesmen SET dist_id = '".$oldData[0]['distributors']['id']."',tran_limit='100000',balance='100000' WHERE id = ".$newSalesMen['0']['salesmen']['id']);
					}
				}
					
			}
		}
//		echo "<pre>";
//		print_r($response);
//            	echo "</pre>";

                if($verify_otp)
                   $this->redirect('/panels');

                $this->set("data",$data);
                $this->set("response",$response);
                $this->set("verify_response",$verify_response);
                $this->set("verify_otp",$verify_otp);

            }
            
	}

	function floatGraph(){
		
		if(empty($_SESSION['Auth']))$this->redirect('/');
		$dt_to = isset($_REQUEST['from']) ? $_REQUEST['to'] : "";
		$dt_from = isset($_REQUEST['to']) ? $_REQUEST['from'] : "";
		$type = empty($_REQUEST['type']) ? "hourly" : $_REQUEST['type'];
		$qp = "";
		$to = "";
		$from = "";

		$qp = " where 1";
		if (!empty($dt_to) && !empty($dt_from)) {
			$qp = $qp . " and ( date >= '" . substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2) . "' and date <= '" . substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2) . "' ) or `date` = '".date("Y-m-d")."'";

			$to = substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2);
			$from = substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2);
			$interval = date_diff(date_create($from), date_create($to));
			if (intval($interval->format('%a')) > 15) {// if request is for more than 15 days then it will show only 15 days before data of  to_date
				//$to = date("Y-m-d");
				$date = new DateTime($to);
				$date->modify('-15 day');
				$from = $date->format('Y-m-d');
			}
		} else {
			//date ( $format, strtotime ( '-7 day' . $date ) )
			$to = date("Y-m-d");
			$date = new DateTime($to);
			$date->modify('-7 day');
			$from = $date->format('Y-m-d');
			$qp = $qp . " and date >= '" . $from . "' and date <= '" . $to . "'";
		}


		$optionSales = array(
                'title' => 'Hourly Sale Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Hours"),
                'seriesType' => "bars",
                'series' => array(
		1 => array("type" => "line", 'curveType'=> "function",'pointSize' => 2 , 'color'=>'red'),
		2 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3,'color'=>'green'),
		3 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3,'color'=>'#5EFB6E'))//1 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3),
		);



		$datas = $this->Slaves->query("SELECT id , sale , float_logs.float , `date` , hour FROM float_logs $qp order by `date` , `hour` "); //and hour = 24

		$dateWiseFloatData = array();

		$datewiseSaleGraphData = array();
		$hourlyData = array();
		$hourlyTodayData = array(); // , 1 =>634.5 ,2 =>172 ,  1 =>634.5 , 2 =>172
		$hourlyYesterdayData = array(); //
		$hourlyTillTodayData = array();
		$hourlyTillYesterdayData = array();
		$prevHrSale = 0;
		$prevDate = "";
		$tillHourData = array();
		$totalTillHourData = array();
		$avgTillHourData = 0;
		$countDayEndHourData = 0;
		$sumDayEndHourData = 0;
		foreach ($datas as $data) {
			$d = $data['float_logs'];
			$hrSale = 0;

			if ($d['hour'] == 1) {
				$hrSale = $d['sale'];
			} else {
				$hrSale = $d['sale'] - $prevHrSale;
			}
			$prevHrSale = $d['sale'];
			$prevDate = $d['date'];
			if (!isset($hourlyData[$d['hour']])) {
				$hourlyData[$d['hour']] = array();
			}
			if ($d['date'] != date('Y-m-d')) {
				array_push($hourlyData[$d['hour']], $hrSale);
			}

			if (!isset($tillHourData[$d['hour']])) {
				$tillHourData[$d['hour']] = array();
			}
			if ($d['date'] != date('Y-m-d')) {
				array_push($tillHourData[$d['hour']], $d['sale']);
			}
			if ($d['date'] == date('Y-m-d')) {
				$hourlyTodayData[$d['hour']] = $hrSale;
				$hourlyTillTodayData[$d['hour']] = $d['sale'];
			}
			if ($d['date'] == date("Y-m-d", strtotime( '-1 days' ) )) {
				$hourlyYesterdayData[$d['hour']] = $hrSale;
				$hourlyTillYesterdayData[$d['hour']] = $d['sale'];
				//$hourlyTillTodayData[$d['hour']] = $d['sale'];
			}


			$dtStr = date("d-M-Y", strtotime($data['float_logs']['date']));
			if ($data['float_logs']['hour'] == 24) {
				array_push($totalTillHourData , $data['float_logs']['sale']);

				$sumDayEndHourData = $sumDayEndHourData + $data['float_logs']['sale'];
				$countDayEndHourData++;
				array_push($datewiseSaleGraphData, array($dtStr, intval($data['float_logs']['sale'])));

				//$dateWiseFloatData[$dtStr]['day_end'] = $data['float_logs']['float'];
			}
			$dateWiseFloatData[$dtStr]['day_end'] = $data['float_logs']['float'];
			//$dateWiseFloatData[$dtStr]['min'] =
			if (isset($dateWiseFloatData[$dtStr]['min'])) {
				if ($dateWiseFloatData[$dtStr]['min'] >= $d['float']) {
					$dateWiseFloatData[$dtStr]['min'] = $d['float'];
					$dateWiseFloatData[$dtStr]['min_hour'] = $d['hour'];
				}

				if ($dateWiseFloatData[$dtStr]['max'] <= $d['float']) {
					$dateWiseFloatData[$dtStr]['max'] = $d['float'];
					$dateWiseFloatData[$dtStr]['max_hour'] = $d['hour'];
				}
			} else {
				$dateWiseFloatData[$dtStr]['min'] = $d['float'];
				$dateWiseFloatData[$dtStr]['min_hour'] = $d['hour'];

				$dateWiseFloatData[$dtStr]['max'] = $d['float'];
				$dateWiseFloatData[$dtStr]['max_hour'] = $d['hour'];
			}
			//array_push($graphData3['data'],array($dtStr,$data['float_logs']['float']));
		}

		$hourlyAvgData = array();
		foreach ($hourlyData as $key => $data) {
			$hourlyAvgData[$key] = array_sum($data) / count($data);
		}

		$datewiseSaleGraphDataWithAvg = array();
		$avgDayEndHourData = $countDayEndHourData==0 ? 0 : $sumDayEndHourData / $countDayEndHourData ;
		foreach ($datewiseSaleGraphData as $key => $data) {
			array_push( $datewiseSaleGraphDataWithAvg , array($data[0], $data[1] ,$avgDayEndHourData) );
		}

		array_unshift($datewiseSaleGraphDataWithAvg, array('Date', 'Sale' ,'Average'));
		$this->set('datewisesaledata', $datewiseSaleGraphDataWithAvg);


		$tillHourAvgData = array();
		foreach ($tillHourData as $key => $data) {
			$tillHourAvgData[$key] = array_sum($data) / count($data);
		}
		$avgTillHourData = count($totalTillHourData) == 0 ? 0 : array_sum($totalTillHourData) / count($totalTillHourData);



		$saleData = array();
		$sumAvg = 0;
		$sumCurr = 0;
		$perDiff = 0;
		$hdAvg = 0;
		$hdCurr = 0;
		$prevAvg = 0;
		$prevCurr = 0;
		$sumDip = 0;
		$countDip = 0;
		$finalSale = 0;
		$calPerNumSum = 0;
		$calPerDenSum = 0;
		$calPer = 0;
		// main logic part
		foreach ($hourlyAvgData as $h => $data) {
			$yhd = intval(empty($hourlyYesterdayData[$h]) ? 0 : $hourlyYesterdayData[$h]);
			if (isset($hourlyTodayData[$h])) {
				 
				$diffTill = $hourlyTillTodayData[$h] - $tillHourAvgData[$h];
				$perDiffTill = $tillHourAvgData[$h] * 100 / $tillHourAvgData[24];

				$calPerNum = ( $diffTill / $tillHourAvgData[$h] ) * $perDiffTill; // numerator single part
				$calPerDen = $perDiffTill;
				$calPerNumSum = $calPerNumSum*0.5 + $calPerNum;//numerator part
				$calPerDenSum = $calPerDenSum*0.5 + $calPerDen;//denominator part
				$calPer = $calPerNumSum / $calPerDenSum * 100;// final weighted mean (nuemerator part / denominator part)

				$temp = array($h . "", intval($data),intval($tillHourAvgData[$h]),intval($hourlyTillTodayData[$h]),intval($hourlyTillYesterdayData[$h]));//, intval($hourlyTodayData[$h])

				//$finalSale = $finalSale + $prevCurr;
			} else { // here we will calculate exacted data

				//echo $calPer . "<br/>";
				$expDiffTillSale = $tillHourAvgData[$h] * $calPer / 100;
				$expTillSale = $tillHourAvgData[$h] + $expDiffTillSale;

				$temp = array($h . "", intval($data),intval($tillHourAvgData[$h]),intval($expTillSale),intval($hourlyTillYesterdayData[$h]));//, intval($expSale)

				//$finalSale = $finalSale + $prevCurr;
			}
			array_push($saleData, $temp);
		}
		array_unshift($saleData, array('Hours', 'Average',"Avg Sale Trend","Today's Sale Trend","Yesterday's Sale"));//,"Today's Sale Trend" //, 'Today' . " = " . round($finalSale, 0) . " \n  [ " . round($perDiff, 2) . " % ]"
		//echo json_encode($saleData);
		$this->set('saledata', $saleData);
		$this->set('type', $type);
		$this->set('to', $to);
		$this->set('from', $from);

		$this->set('optionSales', $optionSales);

		$floatdata = array(
		// array('Date', 'Min','Max','Day End'),
		// array('01-May-2013',  165, 551.5 ,938      ),
		// array('29-May-2013', 135,  627.5 ,  1120   )
		);
		foreach ($dateWiseFloatData as $key => $data) {
			array_push($floatdata, array($key, "", intval($data['min']), intval($data['max']), intval(isset($data['day_end']) ? $data['day_end'] : 0)));
		}


		$optionFloat = array(
                'title' => 'Daily Float Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Date"),
                'seriesType' => "bars",
                'tooltip' => array('isHtml' => true),
                'series' => array(0 => array('type' => 'line', 'pointSize' => 5), 1 => array('type' => 'line', 'pointSize' => 5), 2 => array('type' => 'line', 'pointSize' => 5)),
                'focusTarget' => 'category',
		);
		$this->set('floatdata', $floatdata);
		$this->set('optionFloat', $optionFloat);


		$optionDateWiseSale = array(
                'title' => 'Daily Sale Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Date"),
                'seriesType' => "bars",
                'series' => array(0 => array('type' => 'line', 'pointSize' => 5),1 => array('type' => 'line', 'pointSize' => 5))//
		);

		$this->set('optionDateWiseSale', $optionDateWiseSale);
		$this->render('float_graph');
	}


	function distTopUpRequest(){
		$dist_user = $this->General->getUserDataFromId($_SESSION['Auth']['user_id']);
			$this->set('mobile',$dist_user['mobile']);//home
			
			$banks = $this->Shop->getBanks();
			$this->set('banks',$banks);
			
			if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
				$top_tab = 'activity';
			}else{
				$top_tab = 'home';
			}
			$this->set('top_tab',$top_tab);
			$this->set('side_tab','topup_request');
			$this->render('dist_topup_request');
	}

	function allRetailerTrans($ret_id = 0,$productId=0,$date=null,$page=1,$itemsPerPage=20){

		if(empty($date)){
			$date = date('dmY')."-".date('dmY');
		}

		//$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

		$dates = explode("-",$date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		$transactions = array();
		if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
		}else{
			$date_from =  date('Y') . "-" . date('m') . "-" . date('d');
			$date_to =  date('Y') . "-" . date('m') . "-" . date('d');
		}
		$this->set('date_from',$date_from);
		$this->set('date_to',$date_to);
		$this->set('page',$page);
		$this->set('ret_id',$ret_id);

		//($date,$page=1,$service = null,$date2=null,$itemsPerPage=0,$retailerId=0,$operatorId=0)
		$records = $this->Shop->getLastTransactions($date_from,$page,null,$date_to,$itemsPerPage,$ret_id,$productId);
			
		$products = $this->Slaves->query("SELECT id , name  FROM `products` WHERE to_show = 1 ORDER BY name asc");
		$this->set('productId',$productId);
		$this->set('products',$products);
			
			
		$this->set('transactions',$records['ret']);
		$this->set('total_count',$records['total_count']);
		$this->set('side_tab','allRetailerTrans');
		$this->render('dist_retailer_trans');//$this->autoRender = false;
	}
	function testXml(){
		/*$xml = $string = <<<XML
		 <?xml version='1.0'?>
		 <document>
		 <title>Forty What?</title>
		 <from>Joe</from>
		 <to>Jane</to>
		 <body>
		 I know that's the answer -- but what's the question?
		 </body>
		 </document>
		 XML;
		 $array = json_decode(json_encode((array)simplexml_load_string($xml)),1);
		 print_r($array);*/
		$dom = new DOMDocument;
		$dom->loadXML("<document>
 <title>Forty What?</title>
 <from>Joe</from>
 <to>Jane</to>
 <body>
  I know that's the answer -- but what's the question?
 </body>
</document>");
		if (!$dom) {
			echo 'Error while parsing the document';
			exit;
		}

		$s = simplexml_import_dom($dom);
		$array = json_decode(json_encode($s),1);
		print_r($array);//->document[0]->title;

	}

	/*function dropoutRetailerList(){
		$ids = $this->Slaves->query("
                   SELECT retailers.name , retailers.shopname ,retailers.mobile , retailers.id ,  retailers.balance ,distributors.id ,distributors.company , MAX(retailers_logs.date)as last_trans , AVG(sale) as avg_sale
                   FROM retailers_logs,retailers ,distributors 
                   WHERE 
                        retailers.id = retailers_logs.retailer_id 
                   AND  retailers_logs.date >= '".date('Y-m-d',strtotime('-14 days'))."'
                   AND retailers.parent_id = distributors.id  
                   GROUP BY retailer_id ");
		$qryToday = $this->Slaves->query("SELECT sum( va.amount ) AS amts, retailers.id
                                            FROM vendors_activations AS va
                                            JOIN retailers ON ( va.retailer_id = retailers.id )
                                            WHERE retailers.toshow = 1
                                            AND va.status !=2
                                            AND va.status !=3
                                            AND va.date = '".date('Y-m-d')."'
                                            GROUP BY retailers.id");
			
		//echo date('Y-m-d',1370476800);
		$arr = array();
		$data = array();
			
		$today = array();
		foreach($qryToday as $td){// to check current day record in transactions
			$today[$td['retailers']['id']] = $td['0']['amts'];
		}
			
		foreach($ids as $idObj){
			//$date = "2013-06-04";
			$date_to = date("Y-m-d",strtotime('-5 days', time()));
			$date_from = date("Y-m-d",strtotime('-10 days', time()));
			$flag = true;
				
			if(isset($today[$idObj['retailers']['id']]))$flag = false;
			if( $idObj[0]['avg_sale'] >= 1000 && strtotime($idObj[0]['last_trans']) <= strtotime($date_to) && strtotime($idObj[0]['last_trans']) >= strtotime($date_from) && $flag){
				array_push($arr,$idObj['retailers']['id']);
				if(empty($data[$idObj['distributors']['id']])){ // to arrange distributor wise data
					$data[$idObj['distributors']['id']] = array();
					array_push($data[$idObj['distributors']['id']],$idObj);
				}else{
					array_push($data[$idObj['distributors']['id']],$idObj);
				}
			}
		}
			
		$this->set('data',$data);
		$this->render('dropout_retailer_list');
			
			
	}*/
	
	function lastTransferred(){
		$shop_id = $_REQUEST['id'];
		$ref1_id = $this->info['id'];
        $comm_type = '';
        $type = '';
		if($this->Session->read('Auth.User.group_id') == ADMIN){
			$type = 0;
			$comm_type = 5;
			$ref1_id = 0;
		}
		else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			$type = 1;
			$comm_type = 6;
		}
		else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$type = 2;
			$comm_type = 7;
		}
		
		if($ref1_id == 0){
			$records = $this->Slaves->query("SELECT st1.id,st1.amount,st1.note,st2.amount,st1.timestamp FROM shop_transactions as st1 left join shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = $comm_type)  WHERE st1.ref2_id = $shop_id AND st1.type = $type AND st1.date >='".date('Y-m-d',strtotime('-7 days'))."' order by st1.id desc limit 5");	
		}
		else {
			$records = $this->Slaves->query("SELECT st1.id,st1.amount,st1.note,st2.amount,st1.timestamp FROM shop_transactions as st1 left join shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = $comm_type)  WHERE st1.ref1_id = $ref1_id AND st1.ref2_id = $shop_id AND st1.type = $type AND st1.date >='".date('Y-m-d',strtotime('-7 days'))."' order by st1.id desc limit 5");
		}
		
		//$this->General->logData($_SERVER['DOCUMENT_ROOT']."/log.txt",json_encode($records));
		echo json_encode($records);
		$this->autoRender = false;
	}

	/*function transactingRetailers(){

		$records = $this->Slaves->query("
                   SELECT retailers.id , retailers.name , retailers.shopname ,retailers.mobile ,   retailers.balance , distributors.id ,distributors.company ,  retailers_logs.sale
                   FROM retailers_logs,retailers ,distributors 
                   WHERE 
                        retailers.id = retailers_logs.retailer_id 
                   AND  retailers_logs.sale >= 10000
                   AND  retailers.parent_id = distributors.id    
                   AND  retailers_logs.date = '".date('Y-m-d',strtotime('-1 days'))."'");
			
		$this->set('data',$records);
		$this->render('transacting_retailers');
	}*/
	/*function distSaleUpDown(){
		$ids = $this->Retailer->query("
		SELECT distributors.name , distributors.company , distributors.id  , distributors.balance, SUM(retailers_logs.sale)as sale , AVG(sale) as avg_sale
		FROM retailers_logs,retailers ,distributors
		WHERE
		retailers.id = retailers_logs.retailer_id
		AND  retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."'
		AND retailers.parent_id = distributors.id
		GROUP BY distributors.id ");

		$idToday = $this->Retailer->query("
		SELECT distributors_logs.distributor_id , distributors_logs.topup_sold
		FROM distributors_logs
		WHERE
		distributors_logs.date = '".date('Y-m-d',time("-1 days"))."' ");
		$todayData = array();
		$data = array();
		foreach($idToday as $sub){
		$todayData[$sub['distributors_logs']['distributor_id']]=$sub['distributors_logs']['topup_sold'];
		}
		foreach($ids as $idObj){
		$temp = array();
		// diff * tot /100
		$diff_per = ( ( empty($todayData[$idObj['distributors']['id']]) ? 0 : $todayData[$idObj['distributors']['id']] ) - $idObj[0]['avg_sale']) / $idObj[0]['avg_sale'] * 100 ;

		if( abs($diff_per) > 15 ){
		$temp['name'] = $idObj['distributors']['name'];
		$temp['company'] = $idObj['distributors']['company'];
		$temp['id'] = $idObj['distributors']['id'];
		$temp['balance'] = $idObj['distributors']['balance'];
		$temp['last_trans'] = $idObj[0]['last_trans'];
		$temp['avg_sale'] = $idObj[0]['avg_sale'];
		$temp['diff_per'] = $diff_per;
		$data[$idObj['distributors']['id']] = $temp;
		}

		}
		$this->set('data',$data);
		$this->render('distributor_sale_up_down');
			
		}*/
	
         
        function pullBackApproval(){
           // echo "1"; exit;
            if ($this->RequestHandler->isPost() || $this->RequestHandler->isPut()){  
                $trnsID = $_REQUEST['trans_id'];
                if(empty($trnsID)){
                    $this->Session->setFlash("<div class='error'>Transaction ID can not be empty !</div>", true);
                    $this->render("/elements/shop_pulllback_approval");
                
                }else{
                    $statusID = $_REQUEST['status_id'];
                    $check = $this->Retailer->query("SELECT confirm_flag FROM shop_transactions WHERE id = '$trnsID' AND confirm_flag != 1 AND type_flag != 5");
                    if(!empty($check)){
                    	$this->Retailer->query("UPDATE shop_transactions SET confirm_flag = '$statusID'  WHERE id = '$trnsID' ");
                    	$this->Session->setFlash("<div class='success'>Transaction approved.</div>", true);
                    }
                    else {
                    	$this->Session->setFlash("<div class='error'>Transaction ID already pulled back or cannot be pulled back!</div>", true);
                    }
                    $this->render("/elements/shop_pulllback_approval");
                
                }
                
            }else{
                $this->render("pulllback_approval");
        }
        }
        
       /* function retailer($retailer_id = 0){
        
//            if($_SERVER['SERVER_NAME'] != 'panel.pay1.in'){
//             	$query_str = http_build_query($_GET);
//             	$this->redirect("http://panel.pay1.in/shops/retailer/?".$query_str);
//             }
            
            if ($this->RequestHandler->isAjax()) {
               $imgUrl = trim($_POST["imgurl"]);
               if($this->Retailer->query("DELETE FROM  retailers_details  where image_name = '".$imgUrl."'")){
				  App::import('vendor', 'S3', array('file' => 'S3.php'));
				  $bucket = 'pay1bucket';
				  $s3 = new S3(awsAccessKey, awsSecretKey);
				  $s3->deleteObject($bucket, $imgUrl);
                  echo json_encode(array("success" => 1,"filename" => $imgUrl, "msg" => "file deleted successfully"));
                  exit;
               }
              }
              
        if ($this->RequestHandler->isPost()) {
              
            $distMobile = trim($_POST['dist_mobile']);
            $mobile = trim($_POST['mobile']);
            $shop = trim($_POST['shop']);
            $ret_type = isset($_POST['shop_type']) ? $_POST['shop_type'] : "";
            $loc_type =  isset($_POST['loc_type']) ? $_POST['loc_type']  : "" ;
            $struct_type = isset($_POST['struct_type']) ? $_POST['struct_type'] : "";
            $retailerId = trim($_POST['retailer_id']);
            $address = trim($_POST['address']);
            $area = trim($_POST['area']);
            $city = trim($_POST['city']);
            $state = trim($_POST['state']);
            $latitude = trim($_POST['latitude']);
            $longitude = trim($_POST['longitude']);
            $pin = trim($_POST['pincode']);
            $distributor = trim(isset($_POST['distributor']) ? $_POST['distributor'] : "");
            $updateFlag = trim($_POST['update']);
            //$verifiedFlag = trim($_POST['verified_flag']);
            
            $shopData = $this->Slaves->query("SELECT retailers.id,retailers.user_id FROM retailers,distributors,users WHERE  retailers.parent_id = distributors.id AND distributors.user_id = users.id AND retailers.mobile='$mobile' AND users.mobile='$distMobile'");
            
            if (!empty($mobile)) {
                $this->set('mobile', $mobile);
                if (!empty($shopData) && $updateFlag == "true") {
                    $retId = $shopData['0']['retailers']['id'];
                    $this->General->updateRetailerAddress($retId, $shopData['0']['retailers']['user_id'], $_POST);
                    $resImage = $this->uploadImages("shop", "shop_" . $retId);
                    $resDocs = $this->uploadImages("idProof", "idProof_" . $retId);
                    $this->uploadImages("addressProof", "addressProof" . $retId);
					
                    App::import('Controller', 'Distributors');
                    $DistributorsController = new DistributorsController;
                    $DistributorsController->constructClasses();
                    
                    $DistributorsController->setToPendingVerification($retId);
                    
                    if (!empty($shop)) {
                        $this->Retailer->query("UPDATE retailers SET shopname = '" . addslashes($shop) . "',shop_type=$ret_type,location_type=$loc_type,shop_structure=$struct_type, modified = '".date('Y-m-d H:i:s')."' WHERE id = $retId");
                    }
                     //$this->Retailer->query("UPDATE retailers SET verify_flag = '0' WHERE id = $retId");
                    $mail_subject = "Retailer Info updated by distributor";
                    $mail_body = "Retailer Mobile: " . $_POST['mobile'] . "<br/>Distributor Mobile: $distMobile";

                    $this->General->sendMails($mail_subject, $mail_body, array('distributor.care@mindsarray.com'), 'mail');
                    echo "Retailer Info updated successfully";
                } else if ($retailerId == '') {
                    echo 'Retailer demo number Or Distributor mobile no is not valid';
                    exit;
                }
            } else if (empty($shopData) && $updateFlag == "true") {
                echo 'Retailer demo number Or Distributor mobile no is not valid';
                exit;
            }
        	}
            else {
             
            $mobile = trim($_GET['mobile']);
            $distMobile = trim($_GET['dist_mobile']);
            $shop = trim(urldecode($_GET['shop']));
            $retailerId = trim($_GET['retailer_id']);
            $address = trim(urldecode($_GET['address']));
            $area = trim(urldecode($_GET['area']));
            $city = trim(urldecode($_GET['city']));
            $state = trim(urldecode($_GET['state']));
            $latitude = trim(urldecode($_GET['latitude']));
            $longitude = trim(urldecode($_GET['longitude']));
            $pin = trim(urldecode($_GET['pincode']));
            $distributor = trim($_GET['distributor']);
            $ret_type = "";
            $loc_type = "";
            $struct_type = "";
        }
        $this->set('mobile', $mobile);
        $this->set('retailer_id', $retailerId);
        $this->set('shop', $shop);
        $this->set('address', $address);
        $this->set('area', $area);
        $this->set('city', $city);
        $this->set('state', $state);
        $this->set('latitude', $latitude);
        $this->set('longitude', $longitude);
        $this->set('pincode', $pin);
        $this->set('dist_mobile', $distMobile);
        $this->set('shoptype', $ret_type);
        $this->set('loctype', $loc_type);
        $this->set('structtype', $struct_type);
		
		$getfile = $this->Slaves->query("Select * from retailers_details where retailer_id = '".$retailerId."'");
		
		foreach ($getfile as $val){
			if($val['retailers_details']['type'] == 'image' || $val['retailers_details']['type'] == 'shop'){
				$imagefile[$val['retailers_details']['retailer_id']][] = $val['retailers_details']['image_name'];
			}
			else if(in_array($val['retailers_details']['type'], array('documents', "idProof"))){
				$docsIDfile[$val['retailers_details']['retailer_id']][] = $val['retailers_details']['image_name'];
			}
			else if(in_array($val['retailers_details']['type'], array("addressProof"))){
				$docsAddressfile[$val['retailers_details']['retailer_id']][] = $val['retailers_details']['image_name'];
	3		}
		}
		
        $this->set('image', $imagefile);
        $this->set('documentID', $docsIDfile);
        $this->set('documentAddress', $docsAddressfile);
        
        $this->render("image_upload");
        }*/
        
        function uploadImages($upFileName,$fileN){
        	$filename = "uploadKYCDocuments".date('Ymd').".txt";
        	$this->General->logData('/mnt/logs/'.$filename,"inside uploadImages ::files::".json_encode($_FILES));
            $response = array();
            for($i=0;$i<count($_FILES[$upFileName]["name"]);$i++){
                try {
                    
                        // Undefined | Multiple Files | $_FILES Corruption Attack
                        // If this request falls under any of them, treat it invalid.
                       
//                        Array ( 
//                            [image] => Array ( 
//                                    [name] => logo.png 
//                                    [type] => image/png 
//                                    [tmp_name] => D:\xampp\tmp\phpD03E.tmp 
//                                    [error] => 0 
//                                    [size] => 6542 
//                                ) 
//                            [documents] => Array ( 
//                                    [name] => Array ( 
//                                        [0] => pay1_logo.png 
//                                        [1] => Registration-Error.png 
//                                    ) 
//                                    [type] => Array ( 
//                                        [0] => image/png 
//                                        [1] => image/png 
//                                    ) 
//                                    [tmp_name] => Array ( 
//                                        [0] => D:\xampp\tmp\phpD03F.tmp 
//                                        [1] => D:\xampp\tmp\phpD040.tmp 
//                                    ) 
//                                    [error] => Array ( 
//                                        [0] => 0 
//                                        [1] => 0 
//                                    ) 
//                                    [size] => Array ( 
//                                        [0] => 6997 
//                                        [1] => 252827 
//                                    ) 
//                                ) 
//                            ) 
                        //print_r($_FILES);exit;
                
                        if (!isset($_FILES[$upFileName]['error'][$i]) || is_array($_FILES[$upFileName]['error'][$i])){
                            throw new RuntimeException('Invalid parameters.');
                        }

                        // Check $_FILES['upfile']['error'] value.
                        switch ($_FILES[$upFileName]['error'][$i]) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                throw new RuntimeException('No file sent.');
                            case UPLOAD_ERR_INI_SIZE:
                            case UPLOAD_ERR_FORM_SIZE:
                                throw new RuntimeException('Exceeded filesize limit.');
                            default:
                                throw new RuntimeException('Unknown errors.');
                        }
                        
                        // You should also check filesize here. 
                        if ($_FILES[$upFileName]['size'][$i] > 5000000) {//5 MB
                            throw new RuntimeException('Exceeded filesize limit.');
                        }

                        // DO NOT TRUST $_FILES[$upFileName]['mime'] VALUE !!
                        // Check MIME Type by yourself.
                        try{
                            $finfo = new finfo(FILEINFO_MIME_TYPE);
                        }catch(Exception $e){
                            echo $e ;
                        }
                        
                        $var = $finfo->file($_FILES[$upFileName]['tmp_name'][$i]);
                        
                        if (false === $ext = array_search(
                            $finfo->file(
                                $_FILES[$upFileName]['tmp_name'][$i]),
                                array(
                                    'jpg' => 'image/jpeg',
                                    'png' => 'image/png',
                                    'gif' => 'image/gif',
                                ),
                                true
                        )) {
                            
                            throw new RuntimeException('Invalid file format.');
                        }
                        // You should name it uniquely.
                        // DO NOT USE $_FILES[$upFileName]['name'] WITHOUT ANY VALIDATION !!
                        // On this example, obtain safe unique name from its binary data.
                        $rand = rand(1000,9999);
                        //echo $_FILES[$upFileName]['tmp_name'][$i];
                        //die;
                        
						App::import('vendor', 'S3', array('file' => 'S3.php'));
						$bucket = 'pay1bucket';
						$s3 = new S3(awsAccessKey, awsSecretKey);
						$s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
						$retInfo = explode('_', $fileN);
						$actual_image_name = $fileN . "_" . $rand . "." . $ext;
						if ($s3->putObjectFile($_FILES[$upFileName]['tmp_name'][$i], $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
						$imgUrl = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
						if($_POST['verify_flag'] == 1){
							$this->Retailer->query("insert into retailers_docs
									(retailer_id, type, src, uploader_user_id)
								values('" . $retInfo[2] . "', '" . $upFileName . "', '" . $imgUrl . "', '".$_SESSION['Auth']['user_id']."')");
						}
						else {
							$this->Retailer->query("insert into retailers_details
									(retailer_id, type, image_name, uploader_user_id) 
								values('" . $retInfo[2] . "', '" . $upFileName . "', '" . $imgUrl . "', '".$_SESSION['Auth']['user_id']."')");
						}
						
						//echo "<img src='$imgUrl' style='max-width:400px'/><br/>";
						array_push($response, array(
							'status' => 'success',
							'filename' => $_FILES[$upFileName]['name'][$i],
							'msg' => 'File is uploaded successfully.'
					));
				   } else {
					array_push($response, array(
							'status' => 'faliure',
							'filename' => $_FILES[$upFileName]['name'][$i],
							'msg' => 'File is uploaded successfully.'
					));
				  }
				} catch (RuntimeException $e) {
					array_push($response, array(
							'status' => 'failure',
							'filename' => $_FILES[$upFileName]['name'][$i],
							'msg' => $e->getMessage()
					));
				}
			}
            
            
            return $response;
        }
        
        /*
        function uploadImage($upFileName,$fileN){
            try {

                        // Undefined | Multiple Files | $_FILES Corruption Attack
                        // If this request falls under any of them, treat it invalid.

                        if (!isset($_FILES[$upFileName]['error']) || is_array($_FILES[$upFileName]['error'])){
                            throw new RuntimeException('Invalid parameters.');
                        }

                        // Check $_FILES['upfile']['error'] value.
                        switch ($_FILES[$upFileName]['error']) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                throw new RuntimeException('No file sent.');
                            case UPLOAD_ERR_INI_SIZE:
                            case UPLOAD_ERR_FORM_SIZE:
                                throw new RuntimeException('Exceeded filesize limit.');
                            default:
                                throw new RuntimeException('Unknown errors.');
                        }
                        // You should also check filesize here. 
                        if ($_FILES[$upFileName]['size'] > 5000000) {//5 MB
                            throw new RuntimeException('Exceeded filesize limit.');
                        }
                        
                        // DO NOT TRUST $_FILES[$upFileName]['mime'] VALUE !!
                        // Check MIME Type by yourself.
                        try{
                            $finfo = new finfo(FILEINFO_MIME_TYPE);
                        }catch(Exception $e){
                            echo $e ;
                        }
                        
                        $var = $finfo->file($_FILES[$upFileName]['tmp_name']);
                        
                        if (false === $ext = array_search(
                            $finfo->file(
                                $_FILES[$upFileName]['tmp_name']),
                                array(
                                    'jpg' => 'image/jpeg',
                                    'png' => 'image/png',
                                    'gif' => 'image/gif',
                                ),
                                true
                        )) {
                            
                            throw new RuntimeException('Invalid file format.');
                        }
                        // You should name it uniquely.
                        // DO NOT USE $_FILES[$upFileName]['name'] WITHOUT ANY VALIDATION !!
                        // On this example, obtain safe unique name from its binary data.
                        
                        if (!move_uploaded_file( $_FILES[$upFileName]['tmp_name'], 
                                                 sprintf($_SERVER['DOCUMENT_ROOT'].'/uploads/%s.%s',$fileN,$ext )
                        )){
                            
                            throw new RuntimeException('Failed to move uploaded file.');
                        }
                        
                        return array( 
                            'status'=>'success',
                            'msg'=>'File is uploaded successfully.'
                        );

                    } catch (RuntimeException $e) {
                        return array( 
                            'status'=>'failure',
                            'filename'=>$_FILES[$upFileName]['name'],
                            'msg'=>$e->getMessage()
                        );                       
                    }
            
        }*/
        
        function recheckTrans() {

        	if ($this->RequestHandler->isAjax()) {
        		$id = trim($_POST["shop_transid"]);
        		if (isset($id) && !empty($id)) {
        			$checktrans = $this->User->query("Select `status` from pg_payuIndia where shop_transaction_id  = $id");
        			if (!empty($checktrans) && $checktrans[0]['pg_payuIndia']['status'] == 'failure') {
        				$this->User->query("UPDATE shop_transactions SET confirm_flag = '0' WHERE id = $id");
        				$this->User->query("UPDATE pg_payuIndia SET status = 'Pending' WHERE shop_transaction_id = $id");
        				echo "Data updated successfully";
        				exit();
        			}
        			else {
        				echo "Transaction not found";
        				exit();
        			}
        		}
        	}
    	}
    	function limitTransfer() {
    		 	
    		if ($this->RequestHandler->isPost()) {
    			$file = $_FILES['upload_file']['name'];
		        if (!empty($file)) {
					$allowedExtension = array("xls");
					$getfileInfo = pathinfo($file, PATHINFO_EXTENSION);
					if (in_array($getfileInfo, $allowedExtension)) {
						if (!move_uploaded_file($_FILES['upload_file']['tmp_name'], "/tmp/" . $file)) {
							echo $msg = "Failed to move uploaded file.";
							die;
						}
						chmod("/tmp/". $file, 777);
					} else {
						echo $msg = "Invalid File Format!!!!!";
						die;
					}
					$arrayRecord = array();
    				$amountArray = array();
    				$filepath = "/tmp/" . $file;
    				App::import('Vendor', 'excel_reader2');
    				$excel = new Spreadsheet_Excel_Reader($filepath, true);
    				$data = $excel->sheets[0]['cells'];
    				foreach ($data as $key => $value) {
    					if (isset($value) && count($value)>5) {
                                                $txnId          = substr($file,0,3) == 'sbi' ? substr($file, 4, 10) . '_' . $value[7] : trim($value[2]);
                                                $valuedate      = substr($file,0,3) == 'sbi' ? substr($file, 4, 10) : trim($value[3]);
                                                $txnPostedDate  = substr($file,0,3) == 'sbi' ? date('d-m-Y h:i:s A', strtotime(substr($file, 4, 10))) : trim($value[4]);
                                                $description    = substr($file,0,3) == 'sbi' ? $value[3] : $value[6];
                                                $transType      = substr($file,0,3) == 'sbi' ? ($value[5] > 0 ? 'DR' : 'CR') : trim($value['7']);
                                                $transAmt       = substr($file,0,3) == 'sbi' ? ($transType == 'DR' ? intval(str_replace(",","",trim($value[5]))) : intval(str_replace(",","",trim($value[6])))) : intval(str_replace(",","",trim($value['8'])));
                                                
                                                if(substr($file,0,3) == 'sbi') {
                                                    $val[1] = $value[1];
                                                    $val[2] = $txnId;
                                                    $val[3] = $valuedate;
                                                    $val[4] = $txnPostedDate;
                                                    $val[6] = $description;
                                                    $val[7] = $transType;
                                                    $val[8] = $transAmt;
                                                    $val[9] = $value[7];
                                                    $value = $val;
                                                }
                                                
						if(!empty($txnId)){
                                                        $param['bankName'] = substr($file,0,3) == 'sbi' ? 'SBI' : 'ICICI6714';
    							$checkduplicateTrans = $this->Retailer->query("Select id FROM bank_transactions where bank_name ='" . $param['bankName'] . "' AND bank_txnid = '".$txnId."'");
    							if(count($checkduplicateTrans)){
    								$arrayRecord['alreadydone'][] = $value;
    							}
    						}
    						if($transType == 'DR'){
    							$arrayRecord['failed'][] = $value;
    							if($description == 'CASH PAID:'){
    								$amountArray[] = $transAmt;
								}
    						}
    						if ((strpos($description, 'BY CASH ') !== false || strpos($description, 'CSH DEP (CDM)-CARDLESS DEPOSITBY ') !== false) && $transType == 'CR' && !in_array($transAmt, $amountArray) && empty($checkduplicateTrans)) {
    							$description = str_replace("  ", " ", $description);
    							$getdesription = explode(" ", $description);
    							if (count($getdesription) > 2) {
    								//$mobileNo = ($param['bankName'] == 'SBI') ? $getdesription[4] : substr($getdesription[2],-10);
                                                                                                                                                     $mobileNo = ($param['bankName'] == 'SBI') ? (strlen($getdesription[4]) < 10 ? str_replace('-','',$getdesription[5]) : $getdesription[4]) : substr($getdesription[2],-10);                               
    								$checkRecords = $this->Slaves->query("SELECT `id`,`name`,`group_id`  FROM  users WHERE mobile = '" . $mobileNo . "'");
    								if (count($checkRecords) > 0) {
    									if (!empty($checkRecords[0]['users']['group_id'])) {
    										if ($checkRecords[0]['users']['group_id'] == RETAILER) {
    											$getRetailerInfo = $this->Slaves->query("SELECT retailers.id,retailers.user_id,retailers.balance,retailers.parent_id,distributors.id,distributors.user_id FROM retailers INNER JOIN  distributors ON retailers.parent_id = distributors.id where retailers.user_id='" . $checkRecords[0]['users']['id'] . "' AND distributors.id in (".DISTS.")");
    											if (count($getRetailerInfo) > 0) {
    												$disData = $this->Slaves->query("SELECT * from users where id ='" . $getRetailerInfo[0]['distributors']['user_id'] . "'");
    												if (count($disData) > 0) {
    													$info = $this->Shop->getShopData($disData[0]['users']['id'], $disData[0]['users']['group_id']);
    													$info['User']['group_id'] = $disData[0]['users']['group_id'];
    													$info['User']['id'] = $disData[0]['users']['id'];
    													$info['User']['mobile'] = $disData[0]['users']['mobile'];
    													$param['amount'] = intval($transAmt);
    													$param['retailer'] = $getRetailerInfo[0]['retailers']['id'];
    													$param['app_flag'] = 2;
    													$param['txnId'] = $txnId;
    													$result =$this->amountTransfer($param,$info);
    													if($result['status']=='success'){
															$value['txnid'] = $result['shopId'];
															$value['msg'] = $result['msg'];
    														$arrayRecord['transfer'][] = $value;
    													} else {
															$value['msg'] = $result['description'];
    														$arrayRecord['failed'][] = $value;
    													}
    												}
    												else {
														$value['msg'] = "Distributors does not Exists";
    													$arrayRecord['failed'][] = $value;
    												}
    											}
    											else {
													$value['msg'] = "Retailer does not Exists";
    												$arrayRecord['failed'][] = $value;
    											}
    										} else if ($checkRecords[0]['users']['group_id'] == DISTRIBUTOR) {
    											 
    											$getDistributorInfo = $this->Slaves->query("SELECT distributors.id,distributors.user_id,distributors.margin,distributors.parent_id,super_distributors.id,super_distributors.user_id FROM distributors INNER JOIN super_distributors ON distributors.parent_id = super_distributors.id where distributors.user_id='" . $checkRecords[0]['users']['id'] . "' AND super_distributors.id in (".SDISTS.")");
    											if(count($getDistributorInfo)>0){
    												$superDisdata = $this->Slaves->query("SELECT * from users WHERE id ='".$getDistributorInfo[0]['super_distributors']['user_id']."'");
    												if(count($superDisdata)>0){
    													$info = $this->Shop->getShopData($superDisdata[0]['users']['id'], $superDisdata[0]['users']['group_id']);
    													$info['User']['group_id'] = $superDisdata[0]['users']['group_id'];
    													$info['User']['id'] = $superDisdata[0]['users']['id'];
    													$info['User']['mobile'] = $superDisdata[0]['users']['mobile'];
    													$param['amount'] = intval($transAmt);
														//$param['margin'] = round($transAmt*($getDistributorInfo[0]['distributors']['margin']-0.05)/100,2);

    													$param['margin'] = round($transAmt*$getDistributorInfo[0]['distributors']['margin']/100,2);
    													$param['retailer'] = $getDistributorInfo[0]['distributors']['id'];
    													$param['app_flag'] = 2;
    													$param['txnId'] = $txnId;
    													
    													$result = $this->amountTransfer($param,$info);
    													if($result['status']=='success'){
    														$value['txnid'] = $result['shopId'];
															$value['msg'] = $result['msg'];
    														$arrayRecord['transfer'][] = $value;
    													} else {
															$value['msg'] = $result['description'];
    														$arrayRecord['failed'][] = $value;
															
    													}

    												}
    												else {
														$value['msg'] = "Super Distributors does not Exists";
    													$arrayRecord['failed'][] = $value;
    												}
    											}
    											else {
													$value['msg'] = "Distributors does not Exists";
    												$arrayRecord['failed'][] = $value;
    											}
    										}
    										else {
												$value['msg'] = "Invalid group Id";
    											$arrayRecord['failed'][] = $value;
    										}
    									}
    								} else {
										$value['msg'] = "Number does not exist!!!";
    									$arrayRecord['failed'][] = $value;
										
    								}
    							}
    						} else {
    							if((strpos($description, 'BY CASH ') == false || strpos($description, 'CSH DEP (CDM)-CARDLESS DEPOSITBY ') == false) && $transType=='CR' && empty($checkduplicateTrans)){
									//$value['msg'] = "";
    								$arrayRecord['failed'][] = $value;
    							}

    						}
    					}
    				}
					
    				$this->set('transferRecord',$arrayRecord);
    				unlink($filepath);
    			}
    		}
        }

function pullbackRefund() {

		if ($this->RequestHandler->isAjax()) {
			$shopId = $_POST['shop_id'];
			if(!empty($shopId)){
				$shopdata = $this->Retailer->query("SELECT * FROM shop_transactions WHERE id = '".$shopId."' AND type = '".REFUND."'");
				if(!empty($shopdata)){
					$groupId = $shopdata[0]['shop_transactions']['ref2_id'];
					$amount = $shopdata[0]['shop_transactions']['amount'];
					$id = $shopdata[0]['shop_transactions']['ref1_id']; //retid.distid,supid
					$getUserdata = $this->Shop->getShopDataById($id,$groupId);
					$userId = $getUserdata['user_id'];	
					
					$getUserdata = $this->User->query("select mobile from users where id = '".$userId."'");
					
					$bal = $this->Shop->shopBalanceUpdate($amount,'subtract',$id,$groupId);
					$deletefromOpeningClosing = $this->Retailer->query("DELETE FROM opening_closing WHERE shop_transaction_id = '".$shopId."'");
					$deletefromrefunds = $this->Retailer->query("DELETE FROM refunds WHERE shoptrans_id = '".$shopId."'");
					$deleteFromShopTransaction  = $this->Retailer->query("DELETE FROM shop_transactions WHERE id ='".$shopId."'");
					
//					$sms = "Dear User,\nYour  incentive of Rs $amount Pullback from Pay1 company";
//			        $sms .= "\nYour current balance is now: Rs. " .$bal;
                                
                                $paramdata['AMOUNT'] = $amount;
                                $paramdata['BALANCE'] = $bal;
                                $MsgTemplate = $this->General->LoadApiBalance();
                                $content =  $MsgTemplate['Pullback_Refund_MSG'];
                                $sms = $this->General->ReplaceMultiWord($paramdata,$content);
                                
					$this->General->sendMessage($getUserdata['0']['users']['mobile'],$sms,'notify',$bal,$groupId);
					//$this->General->sendMails("Pullback Refund", $sms,array('tadka@mindsarray.com'),'mail');
			        echo "success";
				} else {
					echo "faliure";
				}
				
			}
		}
		$this->autoRender = false;
	}
	
	function incentivePullback($date = null) {


		if ($date == null) {
			$date = date('dmY' . '-' . date('dmY'));
		}

		$dates = explode("-", $date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		if (checkdate(substr($date_from, 2, 2), substr($date_from, 0, 2), substr($date_from, 4)) && checkdate(substr($date_to, 2, 2), substr($date_to, 0, 2), substr($date_to, 4))) {
			$date_from = substr($date_from, 4) . "-" . substr($date_from, 2, 2) . "-" . substr($date_from, 0, 2);
			$date_to = substr($date_to, 4) . "-" . substr($date_to, 2, 2) . "-" . substr($date_to, 0, 2);
		}
		$query = "Select refunds.*, distributors.company,retailers.shopname,retailers.mobile,shop_transactions.date from refunds  inner join shop_transactions on (refunds.shoptrans_id = shop_transactions.id) left join distributors ON (shop_transactions.ref1_id = distributors.id and shop_transactions.ref2_id = 5 ) left join retailers ON (shop_transactions.ref1_id =retailers.id  and shop_transactions.ref2_id = 6) WHERE refunds.date >= '" . $date_from . "' AND refunds.date<='" . $date_to . "' order by refunds.timestamp desc";
		$transdata = $this->Slaves->query($query);
		$this->set('date_from', $date_from);
		$this->set('date_to', $date_to);
		$this->set('transaction', $transdata);
	}
	
	function graphMainReport($id = null,$date=null,$range = null){
		
		
		$show = false;
		if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
			$distid = $this->info['id'];
			$show = true;
			$this->set('name',$this->info['company']);
		}
		else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
			$sdistid = $this->info['id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['parent_id'] == $this->info['id'] || $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
				$this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
			$rmid = $this->info['id'];
			$sdistid = $this->info['super_dist_id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['rm_id'] == $this->info['id'] && $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
                $this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == ADMIN){
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,SUPER_DISTRIBUTOR);
				if(!empty($shop)){
					$sdistid = $id;
					$this->set('name',$shop['company']);
					$show = true;
					$this->set('dist',$sdistid);
				}
			}
			else {
				$this->set('name','All MasterDistributors');
				$show = true;
			}
		}
			if($show){
			if ($date == null) {
				$date = date('dmY' . '-' . date('dmY'));
			}
			$dates = explode("-", $date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if (checkdate(substr($date_from, 2, 2), substr($date_from, 0, 2), substr($date_from, 4)) && checkdate(substr($date_to, 2, 2), substr($date_to, 0, 2), substr($date_to, 4))) {
				$date_from = substr($date_from, 4) . "-" . substr($date_from, 2, 2) . "-" . substr($date_from, 0, 2);
				$date_to = substr($date_to, 4) . "-" . substr($date_to, 2, 2) . "-" . substr($date_to, 0, 2);
			}

			
			$datas = array();

			if(!isset($sdistid) && !isset($distid)){//Admin with all SD's
				if ($range == null) {
					$groupbydate = 'group by date';
					$groupbyretailerdate = 'group by retailers_logs.date';
					$data_bef = $this->Slaves->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE date >= '".$date_from."' AND date<='".$date_to."' $groupbydate order by date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date) WHERE retailers_logs.date >= '".$date_from."' AND date<='".$date_to."'  $groupbyretailerdate order by retailers_logs.date");
				}else {
					$groupbydate = 'group by week(date)';
					$groupbyretailerdate = 'group by week(retailers_logs.date)';
					$data_bef = $this->Slaves->query("SELECT sum(topup_buy/7) as topup_buy,sum(topup_sold/7) as topup_sold,sum(topup_unique/7) as topup_unique,sum(retailers/7) as retailers,sum(transacting/7) as transacting,date FROM distributors_logs WHERE date >= '".$date_from."' AND date<='".$date_to."' $groupbydate order by date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date) WHERE retailers_logs.date >= '".$date_from."' AND date<='".$date_to."'  $groupbyretailerdate order by retailers_logs.date");
					
				}
				
			}
               else if (isset($sdistid) && !isset($distid)) {//SD with all distributors or RM with all distributors or Admin with a SD
				if (isset($rmid))
					$extra = " AND distributors.rm_id = $rmid AND distributors.active_flag = '1'";
				else
					$extra = " AND distributors.parent_id = $sdistid AND distributors.active_flag = '1'";

				if ($range == null) {
					$groupbydate = 'group by distributors_logs.date';
					$groupbyretailerdate = 'group by retailers_logs.date';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs,distributors WHERE distributor_id  = distributors.id $extra AND distributors_logs.date >= '" . $date_from . "' and distributors_logs.date<='" . $date_to . "'  $groupbydate order by distributors_logs.date");
					$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id $extra AND retailers_logs.date >= '" . $date_from . "' AND retailers_logs.date<='" . $date_to . "' $groupbyretailerdate order by retailers_logs.date");
				} else {
					$groupbydate = 'group by week(distributors_logs.date)';
					$groupbyretailerdate = 'group by week(retailers_logs.date)';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy/7) as topup_buy,sum(distributors_logs.topup_sold/7) as topup_sold,sum(distributors_logs.topup_unique/7) as topup_unique,sum(distributors_logs.retailers/7) as retailers,sum(distributors_logs.transacting/7) as transacting,distributors_logs.date FROM distributors_logs,distributors WHERE distributor_id  = distributors.id $extra AND distributors_logs.date >= '" . $date_from . "' and distributors_logs.date<='" . $date_to . "'  $groupbydate order by distributors_logs.date");
					$data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id $extra AND retailers_logs.date >= '" . $date_from . "' AND retailers_logs.date<='" . $date_to . "' $groupbyretailerdate order by retailers_logs.date");
				}
			} else if(isset($sdistid) && isset($distid)){//SD with a distributor OR RM with a distributor
				$extra = "";
				if(isset($rmid))$extra = " AND distributors.rm_id = $rmid";
				if ($range == null) {
					$groupbydate = 'group by distributors_logs.date';
					$groupbyretailerdate = 'retailers_logs.date';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".$date_from."' AND distributors_logs.date<='".$date_to."' $groupbydate order by distributors_logs.date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".$date_from."' AND retailers_logs.date<='".$date_to."' $groupbyretailerdate order by retailers_logs.date");
		
				}else {
					$groupbydate = 'group by week(distributors_logs.date)';
					$groupbyretailerdate = 'group by week(retailers_logs.date)';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy/7) as topup_buy,sum(distributors_logs.topup_sold/7) as topup_sold,sum(distributors_logs.topup_unique/7) as topup_unique,sum(distributors_logs.retailers/7) as retailers,sum(distributors_logs.transacting/7) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".$date_from."' AND distributors_logs.date<='".$date_to."' $groupbydate order by distributors_logs.date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".$date_from."' AND retailers_logs.date<='".$date_to."' $groupbyretailerdate order by retailers_logs.date");
				}
				
					}
			else if(!isset($sdistid) && isset($distid)){//Distributor
				
				if ($range == null) {
					$groupbydate = 'group by distributors_logs.date';
					$groupbyretailerdate = 'group by retailers_logs.date';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".$date_from."' AND distributors_logs.date<='".$date_to."' $groupbydate order by distributors_logs.date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".$date_from."' AND retailers_logs.date<='".$date_to."' $groupbyretailerdate order by retailers_logs.date");
				}else {
					$groupbydate = 'group by week(distributors_logs.date)';
					$groupbyretailerdate = 'group by week(retailers_logs.date)';
					$data_bef = $this->Slaves->query("SELECT sum(distributors_logs.topup_buy/7) as topup_buy,sum(distributors_logs.topup_sold/7) as topup_sold,sum(distributors_logs.topup_unique/7) as topup_unique,sum(distributors_logs.retailers/7) as retailers,sum(distributors_logs.transacting/7) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".$date_from."' AND distributors_logs.date<='".$date_to."' $groupbydate order by distributors_logs.date");
				    $data_bef_ret = $this->Slaves->query("SELECT sum(retailers_logs.sale/7) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs FORCE INDEX (idx_date),retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".$date_from."' AND retailers_logs.date<='".$date_to."' $groupbyretailerdate order by retailers_logs.date");
				}
				
			}
			
			
            $datas_before = array();
			$ret_before=0;
			$ret_tot = 0;
			$datas_before['week']['new'] = 0;
			$datas_before['month']['new'] = 0;
			
			$datatopupsold = array();
			$datatransRetailer = array();
			$datanewOutlets = array();
			
			foreach($data_bef_ret as $dt){
				$tertiarysale[$dt['retailers_logs']['date']] = array("sale" => $dt[0]['sale'],"transacting"=>$dt[0]['transacting']);
				if($range != null){
				$retailerAvgSale[] = array($dt['retailers_logs']['date'],intval(($dt[0]['sale']*7)/$dt[0]['transacting']));
				} else {
					$retailerAvgSale[] = array($dt['retailers_logs']['date'],intval($dt[0]['sale']/$dt[0]['transacting']));
				}
			}
			
			foreach ($data_bef as $dt) {
				$datatopupsold[] = array($dt['distributors_logs']['date'], intval($dt[0]['topup_sold']),intval($dt[0]['topup_buy']), intval($tertiarysale[$dt['distributors_logs']['date']]['sale']));
				$datatransRetailer[] = array($dt['distributors_logs']['date'], intval($dt['0']['transacting']));
				if ($dt['distributors_logs']['date'] != $date_from) {
					$datanewOutlets[] = array($dt['distributors_logs']['date'], intval($dt['0']['retailers'] - $ret_before));
				}

				$dataUniqueTopups[] = array($dt['distributors_logs']['date'], intval($dt['0']['topup_unique']));
				$ret_before = $dt['0']['retailers'];
			}
			

		$retialerSale = array();
			foreach($data_bef_ret as $dt){
				$retailerAvgSale[] = array($dt['retailers_logs']['date'],intval($dt[0]['sale']/$dt[0]['transacting']));
			}
             $graphData = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Topup sold today'),
							array('number' => 'Topup buy today'),
							array('number' => 'Tertiary Sale')
					),
					'data' => $datatopupsold,
					'title' => 'Topup sold/day',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData1 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'transacting Retailers')
					),
					'data' => $datatransRetailer,
					'title' => 'transacting Retailers',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData2 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'New outlets opened')
					),
					'data' => $datanewOutlets,
					'title' => 'New outlets opened',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			$graphData3 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Unique topups/day')
					),
					'data' => $dataUniqueTopups,
					'title' => 'Unique topups/day',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			

			$graphData5 = array(
					'labels' => array(
							array('string' => 'Sample'),
							array('number' => 'Avg Sale/Retailer')
					),
					'data' => $retailerAvgSale,
					'title' => 'Avg Sale/Retailer',
					'type' => 'line',
					'width' => '900',
					'height' => '400'
			);
			
			
			$this->set('data1',$graphData);
			$this->set('data2',$graphData1);
			$this->set('data3',$graphData2);
			$this->set('data4',$graphData3);
			//$this->set('data5',$graphData4);
			$this->set('data6',$graphData5);
		}
		$this->set('date_from', $date_from);
		$this->set('date_to', $date_to);

		if(!empty($id))$this->set('id',$id);
		
		if(!empty($range)) $this->set('type',$range);
		//$this->autoRender = false;
		
	}
	
function distributorsMonthReport($month=null){
		
		if($month ==null){
			$month = date('m');
		} 
		$d= cal_days_in_month(CAL_GREGORIAN,$month,date('Y'));
		$fromdate = date('Y-m-d',strtotime(date("Y-$month-01")));
		$todate = date("Y-m-d", strtotime("+$d day", strtotime($fromdate)));
		
		$distRecords = array();
		$datearray = array();
		$distId = array();
		
		$distSale = $this->Slaves->query("SELECT SUM(sale) as retsale,retailers_logs.date,retailers.parent_id,distributors.company,distributors.city,distributors.state,distributors.id,date(distributors.created) as created_date,distributors.margin,users.mobile "
				                           ."FROM "
				                            ."retailers_logs  USE index(idx_date) "
				                            ."LEFT JOIN retailers on (retailers_logs.retailer_id = retailers.id) "
											."LEFT JOIN distributors on (distributors.id = retailers.parent_id) "
											."LEFT JOIN users ON (users.id = distributors.user_id) "
							
				                            ."WHERE retailers_logs.date>= '".$fromdate."' AND retailers_logs.date<='".$todate."' "
				                            ."AND distributors.parent_id = '".$this->info['id']."'"
				                            ."GROUP BY retailers.parent_id,retailers_logs.date");
		
		
		
		foreach ($distSale as $val){
			$datearray[$val['retailers_logs']['date']] = $val['retailers_logs']['date'];
			$dataRecords[$val['retailers']['parent_id']][$val['retailers_logs']['date']] = array("sale" => $val[0]['retsale']);
			$distId[$val['retailers']['parent_id']] = $val;
		}
		
		foreach ($datearray as $dateval){
			foreach($distId as $diskey => $disval){
				if(!isset($distRecords[$diskey][$dateval])){
					$distRecords[$diskey][$dateval] = array();
				}
				$distRecords[$diskey][$dateval] = isset($dataRecords[$diskey][$dateval]) ? $dataRecords[$diskey][$dateval] : array("sale"=>"");
			}
		}
		
		
			$fromdate = date("Y-$month-01");
			$this->set('fromdate',$fromdate);
			$this->set('todate',$todate);
			$this->set('distRecords',$distRecords);
			$this->set('monthval',$month);
			$this->set('distId',$distId);
		    $pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
			$this->set('pageType',$pageType);
            if($pageType == 'csv'){
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			$line = array();
			$line[0] = "Distributors Name";
			$line[1] = "City";
			$line[2] = "State";
			$line[3] = "Id";
			$line[4] = "Reg Date";
			$line[5] = "Margin Slab";
			$line[6] = "Mobile No";
			$i=7;
			foreach ($datearray as $dateval){
				$line[$i] = $dateval;
				$i++;
			}
			$csv->addRow($line);
			
			foreach($distRecords as $key => $val){
				$temp[0] = $distId[$key]['distributors']['company'];
				$temp[1] = $distId[$key]['distributors']['city'];
				$temp[2] = $distId[$key]['distributors']['state'];
				$temp[3] = $distId[$key]['distributors']['id'];
				$temp[4] = $distId[$key][0]['created_date'];
				$temp[5] = $distId[$key]['distributors']['margin'];
				$temp[6] = $distId[$key]['users']['mobile'];
				$i=7;
				foreach ($val as $k => $v){
					$temp[$i] = isset($v['sale']) ? $v['sale'] : "";
					$i++;
					
				}
				$csv->addRow($temp);
			}
			 echo $csv->render('distributors_month_report'.date('YmdHis').'.csv');
                } else {
                        $this->render('distributors_month_report');
                }
		
        }
        
        function bankDetails()
        {
            $bankdetails = $this->Shop->getMemcache("bankinfo");
            
            if(empty($bankdetails))
            {
				
                $bankdetails = $this->Slaves->query("select * from bank_details where visible_to_retailer_flag = 1");

                $this->Shop->setMemcache("bankinfo",$bankdetails,1*24*60*60);
				
            }

            $this->set('bankdetails',$bankdetails);
            $this->render('bank_detail');
        }
        
        function distributorsHelpDesk()
        {
            $this->render('distributor_helpdesk');
        }
        
        function limitDepartmentDetails()
        {
            $this->render('limit_department');
        }
        
        function customerCare()
        {
            $this->render('customer_care');
        }
        
}

?>
		



