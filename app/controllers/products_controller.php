<?php
//error_reporting(0);
//ini_set("log_errors", 0);

class ProductsController extends AppController
{
        var $name = 'Products';
        var $components = array('RequestHandler', 'Shop', 'Busvendors', 'General');
        var $helpers = array('Html', 'Ajax', 'Javascript', 'Minify', 'Paginator','Sims');
        var $uses = array('User','Slaves');
       
        
        function index()
        {
            $this->layout = 'products';

            $product_id = $this->params['form']['product_id'];
            $products = $this->User->query("select * from products");
                      
            $this->set("product_id",$product_id);
            $this->set("products", $products);
        }
        
        function edit()
        {
            $this->layout = 'products';

            $product_id = $this->params['url']['product_id'];
            $products = $this->User->query("select * from products where id='$product_id'");
            $circles = $this->User->query("select area_code as id, area_name from mobile_numbering_area");

            $this->set("product_id",$product_id);
            $this->set("products", $products);
            $this->set("circles", $circles);
        }
        
        function editFormEntry($id)
        {
           $this->autoRender = false;

         //  $id = $this->params['form']['id'];
           $to_show = $this->params['form']['to_show'];
           $invalid=$this->params['form']['invalid'];
           $circle_yes=$this->params['form']['cy'];
           $circle_no=$this->params['form']['cn'];
                      
           $query="update products "
                   . " set to_show='$to_show', "
                   . " invalid='$invalid',"
                   . "circle_yes='$circle_yes',"
                   . "circle_no='$circle_no'"
                   . " WHERE id = '$id'";
           //send sms
           
           $products = $this->User->query("select name from products where id='$id'");
           $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".json_encode($products));
           if(!empty($products) && !empty($id)){
               $sms = "";
               $product = "Product : ".$products[0]['products']['name'];
               if(!empty($invalid)){
                    $sms .=" \n amount stop $invalid Rs ";
                }
                if(!empty($circle_yes)){
                   $sms .=" \nCircle Activated : ".$circle_yes;
                }
                if(!empty($circle_no)){
                    $sms .="\n Circle In-Activated : ".$circle_no;
                }
                if(!empty($sms)){
                    $final_sms = $product.$sms;
                    $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".$final_sms);
                    $data = array('user'=>$this->Session->read('Auth.User.id'),'type'=>'operator changed', 'sms'=>$final_sms);
                    $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".json_encode($data));
                    $this->General->curl_post("http://inv.pay1.in/alertsystem/alertreport/addInAlert",$data,'POST');
                }
                
           }
            $updatequery=$this->User->query($query);
           echo json_encode(array('status'=>'done'));
         }

        function local_vendor_mapping($vendor_id = NULL) {
            
                $this->layout = 'products';
            
                $edit_whr = "";
                if(isset($vendor_id)) {
                    $data = $this->User->query("SELECT * FROM local_vendor_mapping WHERE vendor_id = $vendor_id");
                    $edit_whr = "WHERE vendor_id != $vendor_id";
                    $this->set('vendor_data', $data);
                }
                
                $vendors = $this->User->query("SELECT id, company FROM vendors WHERE active_flag = 1 AND id NOT IN"
                        . " (SELECT vendor_id FROM local_vendor_mapping $edit_whr)");
                $temp_vendors = array();
                foreach($vendors as $vendor) {
                    $temp_vendors[$vendor['vendors']['id']] = $vendor['vendors'];
                }
                $vendors = $temp_vendors;
                $this->set('vendors', $vendors);

                $operators = $this->User->query('SELECT id, name FROM products WHERE service_id IN (1,2,4,6) AND to_show = 1 AND active = 1');
                $this->set('operators', $operators);
        }
        
        function l_v_m_entry() {
            
                $this->autoRender = FALSE;
                
                $action = $this->params['form']['action'];
                $vendor = $this->params['form']['vendor'];
                $operator = $this->params['form']['operator'];
                $distributed = $this->params['form']['distributed'];
                $activation = $this->params['form']['activation'];
                
                if($action == 'a') {
                    $this->User->query("INSERT INTO local_vendor_mapping VALUES ('$vendor', '$operator', '$distributed', '$activation')");
                    $this->Session->setFlash('Mapping is successfull !!!');
                } else {
                    $this->User->query("UPDATE local_vendor_mapping SET operator_id = '$operator', distributor_id = '$distributed', is_deleted = '$activation' WHERE vendor_id = '$vendor'");
                    $this->Session->setFlash('Mapping is updated successfully !!!');
                    $this->Shop->delMemcache('local_Vendors_map');
                }
                
                $this->redirect('listing_lvm');
        }
        
        function listing_lvm() {
            
                $this->layout = 'products';
            
                $list_lvm = $this->User->query("SELECT local_vendor_mapping.vendor_id, vendors.company, products.name,
                    local_vendor_mapping.distributor_id, local_vendor_mapping.is_deleted
                    FROM
                    local_vendor_mapping
                    LEFT JOIN
                    vendors ON local_vendor_mapping.vendor_id = vendors.id
                    LEFT JOIN
                    products ON local_vendor_mapping.operator_id = products.id ORDER BY 1 DESC");
                
                $this->set('list_lvm', $list_lvm);
        }

        function a_p_mapping($id = NULL) {
            
                $this->layout = 'products';

                if(isset($id)) {
                        
                        $data = $this->User->query("SELECT * FROM amount_priority_mapping WHERE id = $id");
                        $this->set('data', $data);
                }
                $operators = $this->User->query('SELECT id, name FROM products WHERE service_id IN (1,2,4,6) AND to_show=1 AND active=1');
                $this->set('operators', $operators);

                $vendors = $this->User->query("SELECT id, company FROM vendors WHERE active_flag = 1");
                $this->set('vendors', $vendors);
        }
        
        function a_p_m_entry() {
        
                $this->autoRender = FALSE;
                
                $id             = $this->params['form']['id'];
                $operator       = $this->params['form']['operator'];
                $vendor         = $this->params['form']['vendor'];
                $min_amount     = $this->params['form']['min_amount'];
                $max_amount     = $this->params['form']['max_amount'];
                $list_amount    = $this->params['form']['list_amount'];
                $activation     = $this->params['form']['activation'];
                
                if($id > 0) {
                        $this->User->query("UPDATE amount_priority_mapping SET product_id = '$operator',"
                                . " vendor_id = '$vendor', min_amount = '$min_amount', max_amount = '$max_amount',"
                                . " list_amount = '$list_amount', is_deleted = '$activation' WHERE id = '$id'");
                        $this->Shop->delMemcache('amount_priority_map');
                } else {
                        $this->User->query("INSERT INTO amount_priority_mapping"
                                . " (product_id, vendor_id, min_amount, max_amount, list_amount, is_deleted)"
                                . " VALUES ('$operator', '$vendor', '$min_amount', '$max_amount', '$list_amount', '$activation')");
                }
                
                $this->redirect("listing_apm");
        }
        
        function listing_apm() {
            
                $this->layout = 'products';
                
                $list_apm = $this->User->query("SELECT amount_priority_mapping.*, products.name, vendors.company FROM amount_priority_mapping"
                        . " LEFT JOIN products ON products.id = amount_priority_mapping.product_id"
                        . " LEFT JOIN vendors ON vendors.id = amount_priority_mapping.vendor_id ORDER BY 1 DESC");
                $this->set('list_apm', $list_apm);
        }
        
        function uploadivr(){
            
            if(isset($this->params['form']) && !empty($this->params['form'])){
                
                if(isset($this->params['form']['ivrfile']['name']) && !empty($this->params['form']['ivrfile']['name']) && 
                        isset($this->params['form']['ivrfile2']['name']) && !empty($this->params['form']['ivrfile2']['name'])
                    && isset($this->params['form']['ivrconf']['name']) && !empty($this->params['form']['ivrconf']['name'])){
//                    error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED);
// 		ini_set("display_errors", 1);
                    $ftp_server = "click2call.ddns.net";
                    $ftp_user_name = "ftpserver";
                    $ftp_user_pass = "asdf1234";
                    $remote_file1 = "/var/lib/asterisk/sounds/en/problem/silence.mp3";
                    $remote_file2 = "/var/lib/asterisk/sounds/en/problem/silence1.mp3";
                    $remote_conf_file="/etc/asterisk/";
                    // set up basic connection
                    $conn_id = ftp_connect($ftp_server);
                    // login with username and password
                    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
                    ftp_pasv($conn_id, true);
                    //for mp3 (silence) file
                    $size = $this->params['form']['ivrfile']['size'];
                    $temp_file = $this->params['form']['ivrfile']['tmp_name'];
                    $silence = $this->params['form']['ivrfile']['name'];
                    //for mp3 (silence1) file
                    $size2 = $this->params['form']['ivrfile2']['size'];
                    $temp_file2 = $this->params['form']['ivrfile2']['tmp_name'];
                    $silence1 = $this->params['form']['ivrfile2']['name'];
                    //for config (extensions_additional.conf) file
                    $confsize = $this->params['form']['ivrconf']['size'];
                    $conftemp = $this->params['form']['ivrconf']['tmp_name'];
                    $conffile = $this->params['form']['ivrconf']['name'];

                    $ext = pathinfo($silence, PATHINFO_EXTENSION);
                    $ext2 = pathinfo($silence1, PATHINFO_EXTENSION);
                    $confext = pathinfo($conffile, PATHINFO_EXTENSION);
                    if($ext=='mp3' && $ext2=='mp3' && $confext=='conf'){
                        $file1="silence.mp3";
                        $file2="silence1.mp3";
                        $file3 = "extensions_additional.conf";

                        $target_file = $target_dir . basename($file1);
                        $target_file2 = $target_dir . basename($file2);
                        $conftarget_file = $target_dir . basename($file3);
//                        $remote_file1 = $remote_file.$file1;
//                        $remote_file2 = $remote_file.$file2;
                        $remote_file3 = $remote_conf_file.$file3;
                        
                      if (ftp_put($conn_id, $remote_file1, $temp_file, FTP_BINARY) && ftp_put($conn_id, $remote_file2, $temp_file2, FTP_BINARY) && ftp_put($conn_id, $remote_file3, $conftemp, FTP_ASCII) ) {
                        $this->Session->setFlash("<b>The file ". basename($silence). ", ".basename($silence1)." and ".$conffile." has been uploaded.</b>", 'default', array(), 'form1');
                       } else {
                        $this->Session->setFlash("<b>Problem while uploading the file.</b>", 'default', array(), 'form1');
                       }
                    }else{
                       $this->Session->setFlash("<b>File is not with proper extension.</b>", 'default', array(), 'form1');
                   }
                
                }else{
                    $this->Session->setFlash("<b>Please upload IVR mp3 and config files.</b>", 'default', array(), 'form1');
                }
                
            }
            $this->layout = 'products';
            $this->set('uploadivr',$val);
        }
        
}
?>