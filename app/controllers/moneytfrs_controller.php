<?php
class MoneytfrsController extends AppController {

	var $name = 'Moneytfrs';
	var $components = array('RequestHandler','Shop','General');
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator');
	var $uses = array('User');
	
	function beforeFilter() {
		$this->Auth->allow('*');
		
	}
	
	function test(){
		echo "1";
		$this->autoRender = false;
	}
	
	function receiveWeb($u,$p,$format='json'){
		$logger = $this->General->dumpLog('ReceiveMnyTfr Request', 'receiveMnyTfr');
		
		if(!in_array($format,$this->validFormats))$format = 'json';
		$method = $_REQUEST['method'];
		$deviceType = empty($_REQUEST['device_type']) ? "" : $_REQUEST['device_type'];
		
		$authId = empty ($_SESSION['Auth']['id']) ? 0 : $_SESSION['Auth']['id'];
		$requestid = rand() . time();
		
		$logger->info("$requestid|Request: " . json_encode($_REQUEST) . "| SERVER: " . $_SERVER['REMOTE_ADDR']);
		
		if(!method_exists($this, $method)){
			$logger->info("$requestid|Response: " . $this->Shop->errors(2));
			$this->displayWeb(array('status'=>'failure','code'=>'2','description'=>$this->Shop->errors(2),'app_log_id'=>$app_log_id), $format);
		}
	}
	
	function displayWeb($u,$p,$format='json'){
		
	}
	
	function register(){
		
	}
	
	function register_gitech($params=null){
		
		$txnid = rand(1,100000000);
		
		
		$retid = $params['retailer_code'];
		
		$retid = 10;
		
		$request = "<REGISTRATIONREQUEST>
<TERMINALID>".GI_LOGINID_MNYTFR."</TERMINALID>
<LOGINKEY>".GI_PASSWORD_MNYTFR."</LOGINKEY>
<MERCHANTID>".GI_MERCHANTID_MNYTFR."</MERCHANTID>
<AGENTID>".$params['retailer_code']."</AGENTID>
<TRANSACTIONID>$txnid</TRANSACTIONID>
<KYCFLAG>1</KYCFLAG>
<USERNAME></USERNAME>
<USERMIDDLENAME></USERMIDDLENAME>
<USERLASTNAME></USERLASTNAME>
<USERMOTHERSMAIDENNAME></USERMOTHERSMAIDENNAME>
<USERDATEOFBIRTH></USERDATEOFBIRTH>
<USEREMAILID></USEREMAILID>
<USERMOBILENO></USERMOBILENO>
<USERSTATE></USERSTATE>
<USERCITY></USERCITY>
<USERADDRESS></USERADDRESS>
<PINCODE></PINCODE>
<USERIDPROOFTYPE></USERIDPROOFTYPE>
<USERIDPROOF></USERIDPROOF>
<IDPROOFURL></IDPROOFURL>
<USERADDRESSPROOFTYPE></USERADDRESSPROOFTYPE>
<USERADDRESSPROOF></USERADDRESSPROOF>
<ADDRESSPROOFURL></ADDRESSPROOFURL>
<PARAM1></PARAM1>
<PARAM2></PARAM2>
<PARAM3></PARAM3>
<PARAM4></PARAM4>
<PARAM5></PARAM5>
</REGISTRATIONREQUEST>";
		
		$out = $this->General->gitechApi_mnytfr("REGISTRATION",$request);
		if($out['success']){
			$output = $this->General->xml2array($out['output']);
			$output = $this->General->xml2array($output['string']);
			print_r($output);
		}
		$this->autoRender = false;
		//return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId']);
	}
	
	
	
}