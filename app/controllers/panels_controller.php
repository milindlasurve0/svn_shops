<?php

class PanelsController extends AppController {

	var $name = 'Panels';
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator','GChart');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Retailer','Distributor','SuperDistributor','User','ModemRequestLog','Slaves');
	var $turnaround_time = array(0.5, 1, 2, 24, 48, 100);
	var $api_medium_map = array("SMS", "API", "USSD", "Android", "", "Java", "", "Windows 7", "Windows 8", "Web");	

	function beforeFilter() {
		
		set_time_limit(0);
		ini_set("memory_limit","-1");
                ini_set("display_errors", "off");
               // error_reporting(0);
		parent::beforeFilter();
		$this->layout = 'module';
		
		$reversalDD = array('None','PPI Reversal','OSS Reversal','Modem Reversal','System Reversal','Manual Reversal','Manual Check','Drop SMS','Manual Transaction','Paytronics Reversal');
		$this->set('reversalDD',$reversalDD);

		$taggings = $this->General->getTaggings();
		$this->set('taggings', $taggings);

		$this->set('turnaround_time', $this->turnaround_time);

		$call_types = $this->General->getCallTypes();
		$this->set('call_types', $call_types);
			
		$moduleListing = $this->Slaves->query("SELECT modules.module_name,module_group_mapping.module_id,module_full_name,module_group_mapping.group_id from modules Inner join module_group_mapping ON (modules.id=module_group_mapping.module_id) WHERE group_id = ".$this->Session->read('Auth.User.group_id')."  order by modules.id ASC");

		Configure::load('acl');
		
		$mappedModule= Configure::read('acl.modules');

			 $bypassmodule= Configure::read('acl.bypass');
			
			foreach ($moduleListing as $moduleval):
				
				if(array_key_exists($moduleval['modules']['module_name'],$mappedModule)):
					
				if(!empty($moduleval['modules']['module_full_name'])):
				
				$this->modulearray[$moduleval['modules']['module_full_name']] = array("action" => $mappedModule[$moduleval['modules']['module_name']]['url'][0]);
					
				endif;
				
				endif;
			
		    endforeach;
			
		
		$this->set('modulelist',$this->modulearray);
	}
	
	/* function findUser($userId=null)
	{
		if($userId==null)
		{
			$userId=$_REQUEST['userId'];
		}	//echo $userId;
		$usersResult=$this->User->query("select mobile from users where id=$userId ");
		$num=$usersResult['0']['users']['mobile'];
		//echo $num;
		$this->userInfo($num);
	//$this->render('/panels/userInfo');
		//$this->redirect(array('panels' => 'userInfo'));
	}*/
	
	function createTag(){
		$tagName = $_REQUEST['tagName'];
		$tagType = $_REQUEST['tagType'];
		$tag_exists = $this->Slaves->query("select name from taggings where name like '$tagName' and type like '$tagType'");
		
		if(empty($tag_exists)){
			$insertion = $this->User->query("insert into taggings (name, type) values ('$tagName', '$tagType')");
			$this->Shop->delMemcache("taggings");
			if($insertion)
				echo "success";
		}	
		
		$this->autoRender=false;
	}

	function removeTag(){
		$tagId = $_REQUEST['tagId'];
		$tag_exists = $this->Slaves->query("select name from taggings where id = ".$tagId);
		
		if(!empty($tag_exists)){
			$this->User->query("update taggings set is_active = 0 where id = ".$tagId);
			$this->Shop->delMemcache("taggings");
			echo "success";
		}
		
		$this->autoRender=false;
	}
	
	function manualRequest()
	{
		$loggedInUserId=$_SESSION['Auth']['User']['id'];
		$retailerMobile=$_REQUEST['retMobile'];
		$amount=$_REQUEST['Amount'];
		$operator=$_REQUEST['Operator'];
		$subscriberId=$_REQUEST['subId'];
		$userMobile=$_REQUEST['UserMobile'];
		$date=Date("Y-m-d");
		$power = 0;
		
		if($this->Session->read('Auth.User.group_id') !=SUPER_DISTRIBUTOR){
			$data = $this->Slaves->query("SELECT * FROM repeated_transactions WHERE type = 3 AND msg like '%$userMobile%' AND added_by = $loggedInUserId");
			if(!empty($data)){
				echo "Your repeated request cannot be submitted. Contact admin";
				$this->autoRender=false;
				exit;
			}
		}
		
		//add retailers shop name in email
		$retailerShopNameResult=$this->Slaves->query("select shopname from retailers where mobile='$retailerMobile'");
		$retailerShopName=$retailerShopNameResult['0']['retailers']['shopname'];
		
		$loggedInUserName=$this->Slaves->query("select name,mobile from users where id=$loggedInUserId");
		
		$operatorIdResult=$this->Slaves->query("select id from products where name ='$operator' ");
		$operatorId=$operatorIdResult['0']['products']['id'];
		
// 		$manualTransTagIdResult=$this->User->query("select id,name from taggings where name ='Manual Transaction'");
// 		$manualTransTagId=$manualTransTagIdResult['0']['taggings']['id'];
// 		$manualTransTagName=$manualTransTagIdResult['0']['taggings']['name'];
		
                $prodInfo = $this->Shop->smsProdCodes($operatorId);
                        
		if($prodInfo['method'] == "vasRecharge"){
                    if($prodInfo['type']=="fix")
			$msg='*'.$operatorId.'*'.$userMobile;
                    else
                        $msg='*'.$operatorId.'*'.$userMobile.'*'.$amount;
                }else{
                    if(empty($subscriberId))
			$msg='*'.$operatorId.'*'.$userMobile.'*'.$amount;
                    else
			$msg='*'.$operatorId.'*'.$subscriberId.'*'.$userMobile.'*'.$amount;
                }
                
                
		
                
		$body="Manual Prepaid Request made by ".$loggedInUserName['0']['users']['name']. " (".$loggedInUserName['0']['users']['mobile'].")";
		$body.="<br><b>Transaction Details</b>";
		$body.="<br><br>Customer Mobile Number: ".$userMobile;
		$body.="<br>Retailer Mobile Number: ".$retailerMobile;
		$body.="<br>Retailer Shop Name: ".$retailerShopName;
		$body.="<br>Operator: ".$operator;
		$body.="<br>Amount: Rs.".$amount;
		//echo "Email :".$body;
		//echo "</br>";
		$subject='Pay1-Manual Recharge Request from Panel';
		//echo $subject;
		//echo "message is: ".$msg;
		$this->User->query("insert into repeated_transactions(sender,msg,send_flag,type,added_by,processed_by,timestamp) values('$retailerMobile','$msg',1,3,$loggedInUserId,$loggedInUserId,'$date')");
		echo "Your request is successfully submitted.";
		
		$this->General->sendMails($subject,$body,array('chirutha@mindsarray.com'));
		
		//for tagging 'Manual Transaction'
		$usersChkResult=$this->Slaves->query("select id,mobile from users where mobile='$userMobile'");
		$userIdChk=$usersChkResult['0']['users']['id'];
		//$usersMobileChk=$usersChkResult['0']['users']['mobile'];
// 		$this->User->query("insert into user_taggings (tagging_id,user_id,timestamp) values($manualTransTagId,$userIdChk,'$date') ");
		
		$this->autoRender=false;
		
	}
	
	
function updateCommentsForReversalNew(){
		$current_date = date('Ymd');
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside updateCommentsForReversalNew::". json_encode($_REQUEST));
		$loggedInUser = $_SESSION['Auth']['User']['mobile'];
		$tId = $_REQUEST['tId'];
		$reason = $_REQUEST['reason'];
		$userMobile = $_REQUEST['userMobile'];
		$flag = $_REQUEST['flag'];
		$retMobile = $_REQUEST['retMobile'];
		$retId = $_REQUEST['retId'];
		$callTypeId = $_REQUEST['callTypeId'];
		$tagId = $_REQUEST['tagId'];
		
		$callTypeId == 'none' && $callTypeId = null;
		$tagId == 'none' && $tagId = null;
		
		if($flag == 3){
			$message = "Complaint for transactin id ".$tId."has been declined . ";
	
			$this->General->sendMessage($retMobile, $message, 'notify');
		}
		
		$userIdResult = $this->User->query("select id from users where mobile='$userMobile' ");
		$userId = empty($userIdResult) ? 0 : $userIdResult['0']['users']['id'];
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside updateCommentsForReversalNew ADD COMMENT::" . json_encode(array($userId, $retId, $tId, $reason, $loggedInUser, null, $tagId, $callTypeId)));
		$this->Shop->addComment($userId, $retId, $tId, $reason, $loggedInUser, null, $tagId, $callTypeId);
		$comment = $this->User->query("SELECT c.comments,c.ref_code,u.name,u.mobile,c.created
					from comments c join users u on(c.mobile=u.mobile)
					where c.ref_code = '$tId'  order by c.created desc limit 1");
		echo "<tr bgcolor='#CEF6F5' style='border: 2px solid white'>";
		echo "<td><span style='font-size:12px;'>By ".$comment[0]['u']['name']." @ ".$comment[0]['c']['created']." on ".$comment[0]['c']['ref_code']."</span></br>".$comment[0]['c']['comments']."</td>"; 
		echo "</tr>";
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "end updateCommentsForReversalNew" . json_encode($comment));
		$this->autoRender=false;
	}
	
	/*function updateCommentsForReversal()
	{
		$loggedInUser= $_SESSION['Auth']['User']['mobile'];
		$tId=$_REQUEST['tId'];
		$reason=$_REQUEST['reason'];
		$userMobile=$_REQUEST['userMobile'];
		$flag=$_REQUEST['flag'];
		$retMobile=$_REQUEST['retMobile'];
		$retId=$_REQUEST['retId'];
		echo "Retailer Mobile is ".$retMobile;
		//$retIdResult=$this->User->query("select id from retailers where mobile='$retMobile' ");
		//$retId=$retIdResult['0']['retailers']['id'];

		if($flag==3) // for tagging of transaction function, flag=3.
		{
			$message="Complaint for transactin id ".$tId."has been declined . ";
			echo "user mobile is ".$userMobile;
			echo "Message is ".$message;

			$this->General->sendMessage($retMobile,$message,'notify');

		}

		echo "retailer id is ".$retId;
		//		echo "Transactin Id is ".$tId;
		$userIdResult=$this->User->query("select id from users where mobile='$userMobile' ");
		$userId=$userIdResult['0']['users']['id'];

		$this->Shop->addComment($userId,$retId,$tId,$reason,$loggedInUser);
		//echo "userid for mobile ".$usermobile." is ".$userId;

		//comments entered by
		$commentsEnteredByResult=$this->User->query("select mobile,name from users where id=$loggedInUser");
		 $commentsEnteredBy=$commentsEnteredByResult['0']['users']['mobile'];
		 $commentsEnteredByName=$commentsEnteredByResult['0']['users']['name'];
		 //echo "Comments entered by : ".$commentsEnteredBy;
		 $newDate=date("Y-m-d H:i:s");
		 $this->User->query("insert into comments(users_id,retailers_id,mobile,ref_code,flag,comments,created) values ($userId,$retId,$commentsEnteredBy,$tId,$flag,'".addslashes($reason)."','$newDate') ");

		 if(empty($commentsEnteredByName))
			$var=$commentsEnteredBy;
			else
			$var=$commentsEnteredByNamel;
			echo $var."&nbsp;&nbsp&nbsp;&nbsp;".$newDate."&nbsp;</br>".$reason;
		//echo "Inserted in comments table";
		$this->autoRender=false;
	}*/
        
        function addNewNumber($flag = null) {            
            // if retInfo number change then flag=1
            $oldNum = $_REQUEST['oldNumber'];
            $newNum = $_REQUEST['newNumber'];
            $otp = isset($_REQUEST['otp']) ? $_REQUEST['otp'] : "" ;

            $oldIDForLogsResult = $this->User->query("select users.id,retailers.maint_salesman,salesmen.mobile,retailers.shopname,retailers.parent_id from users,retailers left join salesmen ON (salesmen.id = retailers.maint_salesman) where users.mobile='$oldNum' AND users.id = retailers.user_id");
            $oldIDForLogs = $oldIDForLogsResult['0']['users']['id'];
            $otpSystem = $this->Shop->getMemcache("otp_changeMob_$oldNum");

            if (!empty($oldIDForLogs)) {
                $result = $this->User->query("Select id,mobile,group_id from users where mobile='$newNum'");
                //for retailers table start
                //$newNumRetailerCheck=$this->User->query("Select mobile from retailers where mobile='$newNum'");
                //retailers table end

                if (count($result) > 0 && $result['0']['users']['group_id'] != MEMBER) { // checking if new number already present in system.
                    $msg = "This user cannot be shifted to a retailer number.";
                    $successs = 0;
                } 
                else if(count($result) > 0 && $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR && $otp==''){

                    $newId = $result['0']['users']['id'];
                    $this->User->query("update users set mobile='temp_str' where id=$newId"); //relacing new num by 'temp_str' in users table.
                    //if retailer number to b changed start
                    $this->General->makeOptIn247SMS($newNum);

                    $oldId = $oldIDForLogs;
                    $this->User->query("update users set mobile='" . $newNum . "',ussd_flag=0 where id=$oldId");
                    $this->User->query("update retailers set mobile='" . $newNum . "', modified = '".date('Y-m-d H:i:s')."' where user_id=$oldId");
                    $this->User->query("update users set mobile='" . $oldNum . "',ussd_flag=0 where id=$newId");
                    //ret end
                    $successs = 1;
                }else if(!count($result) &&  $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
                    $this->General->makeOptIn247SMS($newNum);
                    $result = $this->User->query("UPDATE users SET mobile='$newNum',ussd_flag=0 WHERE mobile='$oldNum'");
                    $result1 = $this->User->query("UPDATE retailers SET mobile='$newNum', modified = '".date('Y-m-d H:i:s')."' WHERE mobile='$oldNum'");
                    $successs = 1;
                }
                else if (count($result) > 0 && !empty($otpSystem) && $otp == $otpSystem) {  // old number and new number get swapped.
                    //echo "New Number already present";
                    $newId = $result['0']['users']['id'];
                    $this->User->query("update users set mobile='temp_str' where id=$newId"); //relacing new num by 'temp_str' in users table.
                    //if retailer number to b changed start
                    $this->General->makeOptIn247SMS($newNum);

                    $oldId = $oldIDForLogs;
                    $this->User->query("update users set mobile='" . $newNum . "',ussd_flag=0 where id=$oldId");
                    $this->User->query("update retailers set mobile='" . $newNum . "', modified = '".date('Y-m-d H:i:s')."' where user_id=$oldId");
                    $this->User->query("update users set mobile='" . $oldNum . "',ussd_flag=0 where id=$newId");
                    //ret end
                    $successs = 1;

                } else if (!count($result) && $otp == $otpSystem) {
                    //echo "New Number not present";
                    $this->General->makeOptIn247SMS($newNum);
                    $result = $this->User->query("UPDATE users SET mobile='$newNum',ussd_flag=0 WHERE mobile='$oldNum'");
                    $result1 = $this->User->query("UPDATE retailers SET mobile='$newNum', modified = '".date('Y-m-d H:i:s')."' WHERE mobile='$oldNum'");
                    $successs = 1;
                } else {
                    $msg = "OTP does not match. Please try again";
                    $successs = 0;
                }

                $msg1 = "Dear Retailer,\nYour number has been shifted from $oldNum to your new number " . $newNum;
                $msg2 = "Dear Distributor,\nDemo number of retailer (" . $oldIDForLogsResult['0']['retailers']['shopname'] . ") changed from $oldNum to new number " . $newNum;


                $dist_mob = $this->User->query("SELECT mobile FROM distributors left join users ON (users.id = distributors.user_id) WHERE distributors.id=" . $oldIDForLogsResult['0']['retailers']['parent_id']);

                if ($successs == 1) {
                    echo $successs . "^^^" . $msg1;
                    //if($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR){
                    $this->General->sendMessage($oldNum, $msg1, 'shops');
                    //}
                    if (!empty($dist_mob['0']['users']['mobile']))
                        $this->General->sendMessage($dist_mob['0']['users']['mobile'], $msg2, 'shops');
                }else {
                    echo $successs . "^^^" . $msg;
                }
            } else {
                echo "0^^^Retailer does not exists";
            }
            $this->autoRender = false;
        }
        
	function manualSuccess(){
		$tranId = $_REQUEST['id'];
		$ref_code = $this->User->query("SELECT vendors_activations.vendor_id, vendors_activations.vendor_refid, products.service_id FROM vendors_activations join  products on (vendors_activations.product_id = products.id) WHERE vendors_activations.ref_code= '".$tranId."' AND vendors_activations.status not in (2,3)");

		if(!empty($ref_code)){
			if(!$this->Shop->checkStatus($tranId,$ref_code['0']['vendors_activations']['vendor_id'])){
				echo "Error";
			}
			else {
				if(!$this->Shop->lockTransaction($tranId)){
					echo "Error";
					return;
				}
				$this->Shop->unlockTransaction($tranId);
				$this->User->query("UPDATE vendors_activations SET prevStatus =status,status='".TRANS_SUCCESS."' , complaintNo='".(empty($_SESSION['Auth']['User']['id'])? 0 :$_SESSION['Auth']['User']['id'])."' WHERE ref_code='$tranId'");
				$this->Shop->addStatus($tranId,$ref_code['0']['vendors_activations']['vendor_id']);
					
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$tranId."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13','Manual Success by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");

				echo "Done";

			}
		}
		else {
			echo "Not found";
		}
		$this->autoRender = false;
	}
	
	function reverseTransaction($ref_id, $session = null){
		$this->autoRender = false;
		if($_SERVER['HTTP_HOST'] != PROCESSOR_HOSTNAME){
			if(empty($_SESSION['Auth']['User']['group_id'])){
				echo "NOT allowed";
				exit;
			}
			$session = urlencode($_SESSION['Auth']['User']['group_id']. "_" .$_SESSION['Auth']['User']['id'] . "_" . $_SESSION['Auth']['User']['name']);
			//$out = $this->General->curl_post("http://54.235.195.140/recharges/reverseTransaction/$ref_id/$session",null,'GET');
                        $out = $this->General->curl_post(TRANS_REVERSAL_URL."$ref_id/$session",null,'GET');
			echo $out['output'];
		}
		else {
			if(!empty($session)){
				$session = urldecode($session);
				$session = explode("_",$session);
				$grpId = $session[0];
				$usrId = $session[1];
				$usrName = $session[2];
			}
			else {
				$grpId = $_SESSION['Auth']['User']['group_id'];
				$usrId = $_SESSION['Auth']['User']['id'];
				$usrName = $_SESSION['Auth']['User']['name'];
			}
			if($grpId ==ADMIN || $grpId ==CUSTCARE || $usrId == 1){
				
				///echo "group";
				
				$ref_code = $this->Slaves->query("SELECT vendors_activations.*,vendors.update_flag,vendors.shortForm, products.service_id,products.id,`vendors_transactions`.sim_num "
                        . "FROM vendors_activations join  products on (vendors_activations.product_id = products.id) "
                        . "LEFT JOIN vendors ON (vendors_activations.vendor_id = vendors.id) "
                        . "LEFT JOIN `vendors_transactions` ON (vendors_activations.ref_code = `vendors_transactions`.ref_id AND vendors_activations.vendor_id = `vendors_transactions`.vendor_id) "
                        . "WHERE vendors_activations.ref_code= '".$ref_id."'");
				
				if(!empty($ref_code)){
					$vendor = $ref_code[0]['vendors_activations']['vendor_id'];
					
					if(!$this->Shop->checkStatus($ref_id,$vendor)){
						return;
					}
					
					if($ref_code['0']['products']['id'] == WALLET_ID){
						$out = $this->General->b2c_pullback($ref_code['0']['vendors_activations']['vendor_refid'],$ref_id);
						if($out['status'] == 'failure'){
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, Amount is less in user account, so cannot be pulled back','success','".date("Y-m-d H:i:s")."')");
							echo "Amount is less in user account, so cannot be pulled back";
							return;
						}
					}else{
                        
                        
                        $result = $ref_code;
                        $statusList = array('0' => 'pending', '1' => 'success', '2' => 'failure', '3' => 'failure', '4' => 'success', '5' => 'success'); //---status list
                        $transId = $result['0']['vendors_activations']['ref_code'];
                        $transvendor = $result['0']['vendors_activations']['vendor_id'];
                        $transvendorname = trim($result['0']['vendors']['shortForm']);
                        $transdate = $result['0']['vendors_activations']['date'];
                        $transvrefId = $result['0']['vendors_activations']['vendor_refid'];
                        $transoprId = $result['0']['vendors_activations']['operator_id'];
                        $trans_server_Status = $statusList[$result['0']['vendors_activations']['status']];
                        $vendors_activations_id = $result['0']['vendors_activations']['id'];

                        ob_start(); //to supress the printable output of below function call
                        App::import('Controller', 'Recharges');
        				$obj = new RechargesController;
        				$obj->constructClasses();
                        $tranResponse = $obj->tranStatus($transId,$transvendorname,$transdate,$transvrefId,1);
                        ob_end_clean();                        

                                                   
                        if($ref_code[0]['vendors']['update_flag'] == 0 ){ // contraints for API
                            $MAX_ALLOWED = 2000;
                            $tranResponse_status = isset($tranResponse['status']) ? strtolower(trim($tranResponse['status'])) : "";
                            $allowed_status = array('pending','success');
                            if($tranResponse_status != 'failure'){
                                //if(in_array($tranResponse_status, $allowed_status)){
                                    $amountqry= "SELECT sum(amount) as total "
                                            . "FROM `vendors_activations` "
                                            . "INNER JOIN `trans_pullback` ON (`vendors_activations`.id = `trans_pullback`.vendors_activations_id) "
                                            . "LEFT JOIN vendors ON  (`vendors_activations`.vendor_id = vendors.id) "
                                            . "WHERE `trans_pullback`.date = '".date('Y-m-d')."' AND vendors.update_flag=0 AND `trans_pullback`.pullback_by = 0 "
                                            . "AND `trans_pullback`.reported_by = 'Non-system'";

                                    $total_used_amount_arr = $this->User->query($amountqry);
                                    $total_used_amount = empty($total_used_amount_arr[0][0]['total']) ? 0 : $total_used_amount_arr[0][0]['total'];
                                    /*if( ($MAX_ALLOWED - $total_used_amount) > 0 ||  $grpId == ADMIN){
                                        $req_data = array('id'=>'','vendors_activations_id'=>$vendors_activations_id,'vendor_id'=>$vendor,'status'=>'1','timestamp'=>date('Y-m-d H:i:s'),
                                            'pullback_by'=>0,'pullback_time'=>'0000-00-00 00:00:00','reported_by'=>'Non-system','date'=>date('Y-m-d'));
                                        $this->General->manage_transPullback($req_data);  
                                    }else{
                                        $res_msg = "reversal amount exceeding MAX Allowed Amount";
                                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, $res_msg , so cannot be reversed','success','".date("Y-m-d H:i:s")."')");
                                        echo $res_msg;
                                        return;
                                    }*/
                            }
                        }else if($ref_code[0]['vendors']['update_flag'] == 1){ // contraints for MODEM
                            $tranResponse_arr = json_decode($tranResponse,true);
                            $tranResponse['status'] = isset($tranResponse_arr['status']) ? strtolower($tranResponse_arr['status']) :"";
                            if(!in_array($tranResponse['status'],array('failure','invalid')) && empty($transoprId)){
                                $sim_num = $result['0']['vendors_transactions']['sim_num'];
                                $trans_oprId = $this->Shop->getOtherProds($result['0']['vendors_activations']['product_id']);
                                $sim_qry = "SELECT * FROM `devices_data` WHERE mobile='".substr($sim_num,-10)."' AND vendor_id='$vendor' AND opr_id in ($trans_oprId) AND sync_date='$transdate'";
                                $sim_result = $this->User->query($sim_qry);
                                if(!empty($sim_result)){
                                	$sim_result[0]['devices_data']['closing'] = ($transdate == date('Y-m-d')) ? $sim_result[0]['devices_data']['balance'] : $sim_result[0]['devices_data']['closing'];
                                	$diff =  $sim_result[0]['devices_data']['sale'] - $sim_result[0]['devices_data']['opening'] - $sim_result[0]['devices_data']['tfr'] + $sim_result[0]['devices_data']['closing'] - $sim_result[0]['devices_data']['inc'];
                                    
                                    $this->General->logData('/mnt/logs/reverse.txt',"transId: $transId ::diff: $diff:: server_diff:".$sim_result[0]['devices_data']['server_diff']);
                                    
                                    /*if(intval($diff) <= intval($sim_result[0]['devices_data']['server_diff']) && $grpId != ADMIN){
                                        $res_msg = "server_diff more than modem_diff";
                                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, $res_msg , so cannot be reversed','success','".date("Y-m-d H:i:s")."')");
                                        echo $res_msg;
                                        return;
                                    }elseif(intval($diff) <= intval($sim_result[0]['devices_data']['server_diff']) && $grpId == ADMIN){
                                        $req_data = array('id'=>'','vendors_activations_id'=>$vendors_activations_id,'vendor_id'=>$vendor,'status'=>'1','timestamp'=>date('Y-m-d H:i:s'),
                                            'pullback_by'=>0,'pullback_time'=>'0000-00-00 00:00:00','reported_by'=>'Non-system','date'=>date('Y-m-d'));
                                        $this->General->manage_transPullback($req_data);  
                                    }*/
                                }
                            }
                        }
                        
                    }
					$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','14','Manual reversal by $usrName','failure','".date("Y-m-d H:i:s")."')");
						
					$this->Shop->addStatus($ref_id,$vendor);
					$this->Shop->reverseTransaction($ref_id,null,null,$grpId,$usrId);
				}
				else {
					echo "NOT allowed";
				}
		}
			else {
				echo "NOT allowed";
			}
		}

	}
	
	function manualFailure(){
		$ref_code = $_REQUEST['id'];
		$va = $this->User->query("select va.retailer_id
				from vendors_activations va
				where va.ref_code = '".$ref_code."'");
	
	
		if(!empty($va)){
			$mobile = $_SESSION['Auth']['User']['mobile'];
			$this->reverseTransaction($ref_code);
			$this->Shop->addComment(0, $va[0]['va']['retailer_id'], $ref_code, null, $mobile, null, 29, 12);
			echo "DONE";
			die;
		}
		else {
			echo "NOT FOUND";
		}
			$this->autoRender = false;
	}
	
	function reversalDeclined($ref_id,$send){
		$this->Shop->reversalDeclined($ref_id,$send);
		$this->autoRender = false;
	}
	
	function search($from=null,$to=null,$rShop=null)
	{
		if(!isset($from)) $from=date('d-m-Y');
		if(!isset($to)) $to=date('d-m-Y');
		
		//echo "from".$from;
		//echo "to".$to;
		
		$this->set('from',$from);
		$this->set('to',$to);
		
		if(!empty($rShop))
		{
			
			$retailerShopResults=$this->Slaves->query("select id,name,mobile,shopname from retailers where shopname like '%$rShop%' ");
			$this->set('retailerShopResults',$retailerShopResults);
			$this->set('rShop',$rShop);
			
		}
	}
	
	
	function view(){}
		
	function retMsg(){
		//$ret=$this->Retailer->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by retailer_id having (sum(retailers_logs.sale) > 0)");

		$tmp=$this->Slaves->query("SELECT * from msg_templates order by name asc");
		$this->set('templates',$tmp);
		
		

		$root = 'payone';
		if(isset($_REQUEST['user'])){
			$retArr = array();
			if($_REQUEST['user'] == 'test'){
			    $_REQUEST['testMobile'] = str_replace(' ','',$_REQUEST['testMobile']);
				$retArr = explode(",",$_REQUEST['testMobile']);
				$root = 'notify';
			}else if($_REQUEST['user'] == 'multi'){
				foreach($_REQUEST['multiRet'] as $k=>$v){
					array_push($retArr,$v);
				}
			}else if($_REQUEST['user'] == 'rotation'){
                
                $retArr1 = $retArr2 = $retArr3 = $retArr4 =array();
                
				$ret=$this->Slaves->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by retailer_id having (sum(retailers_logs.sale) > 0)");
				$companyrm = $this->Slaves->query("SELECT rm.mobile from rm where super_dist_id = '3' AND active_flag='1'");
				//$distributors = $this->Slaves->query("SELECT users.mobile from users inner join distributors on (users.id = distributors.user_id) WHERE distributors.active_flag = '1'");
				//$saleman = $this->Slaves->query("SELECT salesmen.mobile from salesmen WHERE active_flag = '1'");
							
				foreach ($companyrm as $rm) {
					array_push($retArr1, $rm['rm']['mobile']);
				}
//				foreach ($distributors as $dist) {
//					array_push($retArr2, $dist['users']['mobile']);
//				}
//				foreach ($saleman as $sm) {
//					array_push($retArr3, $sm['salesmen']['mobile']);
//				}
				foreach ($ret as $r) {
					array_push($retArr4, $r['retailers']['mobile']);
				}
				
				$this->General->sendMessage($retArr1,$_REQUEST['message1'],'shops');
				//$this->General->sendMessage($retArr2,$_REQUEST['message1'],'shops');
				//$this->General->sendMessage($retArr3,$_REQUEST['message1'],'shops');
				$this->General->sendMessage($retArr4,$_REQUEST['message1'],'shops');
				
				$this->General->logData("/tmp/notifaction.txt",date("Y-m-d H:i:s")." :: ".$_REQUEST['user']." ::message=>::".$_REQUEST['message1']."mobno=>".json_encode($retArr1).json_encode($retArr4));
				
				$root = 'special';
			}
			else if($_REQUEST['user'] == 'app_rotation'){
				$ret=$this->Slaves->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by retailer_id having (sum(retailers_logs.sale - retailers_logs.sms_sale - retailers_logs.ussd_sale) > 0)");

				foreach($ret as $r){
					array_push($retArr,$r['retailers']['mobile']);
				}
				$root = 'notify';
			}
			else if($_REQUEST['user'] == 'sms_rotation'){
				$ret=$this->Slaves->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by retailer_id having (sum(retailers_logs.sms_sale) > 0)");

				foreach($ret as $r){
					array_push($retArr,$r['retailers']['mobile']);
				}
			}
			else if($_REQUEST['user'] == 'ussd_rotation'){
				$ret=$this->Slaves->query("SELECT retailers.mobile FROM `retailers_logs`,retailers where retailers.id = retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by retailer_id having (sum(retailers_logs.ussd_sale) > 0)");

				foreach($ret as $r){
					array_push($retArr,$r['retailers']['mobile']);
				}
			}
			else if($_REQUEST['user'] == 'distributors'){
				$distributors = $this->Slaves->query("SELECT users.mobile from distributors_logs ,distributors, users  WHERE distributors_logs.distributor_id = distributors.id AND distributors.user_id = users.id AND distributors_logs.date >= '".date('Y-m-d',strtotime('-4 days'))."' group by distributor_id having (sum(distributors_logs.topup_sold) > 0)");
				
				//$distributors = $this->User->query("select users.mobile from distributors , users  WHERE distributors.user_id = users.id and toshow = 1");

				foreach($distributors as $distributor){
					array_push($retArr,$distributor['users']['mobile']);
				}
			
			}
			else if($_REQUEST['user'] == 'salesman'){
				//$ret=$this->Retailer->query("SELECT mobile from retailers where toshow  = 1");
				$salesmen = $this->Slaves->query("select * from salesmen WHERE block_flag=0 AND active_flag=1");

				foreach($salesmen as $salesman){
					array_push($retArr,$salesman['salesmen']['mobile']);
				}
			}
            else if($_REQUEST['user'] == 'pay1_salesmen'){
				//$ret=$this->Retailer->query("SELECT mobile from retailers where toshow  = 1");
				$salesmenStr = $_REQUEST['salesmen_no'];
                $retArr = explode(",", $salesmenStr);				
			}
			
			
		    
			$this->General->logData("/tmp/notifaction.txt",date("Y-m-d H:i:s")." :: ".$_REQUEST['user']." ::message=> ::".$_REQUEST['message1']."mobno=>".json_encode($retArr));
			$this->General->sendMessage($retArr,$_REQUEST['message1'],$root);
			unset($retArr);
			$this->set('Error',"Sent successfully!");
		    $this->redirect('/panels/retMsg');
			
			
		}
		//$this->set('retailer',$ret);

	}
	
	/*function saveMaintenanceSM($maintenanceSMId=null,$retMobile=null)
	{
		echo "maint sm id is ".$maintenanceSMId;
		echo "retailer mobile is ".$retMobile;
		
		$this->User->query("update retailers set maint_salesman=$maintenanceSMId where mobile='$retMobile' ");
		$this->redirect('/panels/retColl');
	}*/
	
	
	
	function retColl($distId = null, $smId = null) {
			
		// flag=1 => search retailers by mainenance Salesman
			// flag=2 => search retailers by acquision Salesman
		
		$query = "";
		$query1 = "";
		if ($distId == null)
			$distId = 1;
		$this->set ( 'distId', $distId );
		
		// echo $distId;
		// echo $distId;
		
		if ($smId == null || $smId == 0) {
			
			$query = " AND parent_id = $distId";
			$query1 = " INNER JOIN retailers ON (retailers.parent_id = $distId AND retailers.id = ref1_id)";
		} else {
			
			$this->set ( 'sid', $smId );
			$query = " AND parent_id = $distId AND maint_salesman = $smId";
			$query1 = " INNER JOIN retailers ON (retailers.parent_id = $distId AND retailers.maint_salesman = $smId AND retailers.id = ref1_id)";
		}
		
		$search_query = trim($_POST['search_term']) ? " and (ret.mobile like '%".trim($_POST['search_term'])."%' or ret.shopname like '%".$_POST['search_term']."%') " : "";
		$this->set('search_term', trim($_POST['search_term']));
		// echo $query1;
		// echo $query;
		$amountTransaferredQuery = "select salesmen.name, salesmen.mobile,salesmen.id,ret.block_flag, ret.id,ret.name,
					ret.maint_salesman,ret.shopname,ret.rental_flag, ret.mobile, ret.parent_id, ret.balance, ret.toShow , 
					Date(ret.created) as created 
				from retailers ret left join salesmen on (salesmen.id = ret.salesman) 
				where 1 $query $search_query group by ret.id order by ret.id desc";
// 		$amountTransferred = $this->Slaves->query ( "select salesmen.name, salesmen.mobile,salesmen.id,ret.block_flag, ret.id,ret.name,ret.maint_salesman,ret.shopname,ret.rental_flag, ret.mobile, ret.parent_id, ret.balance, ret.toShow , Date(ret.created) as created from retailers ret left join salesmen on (salesmen.id = ret.salesman) where 1 $query group by ret.id order by ret.id desc" ); // ret.toshow=1
		$amountTransferred = $this->paginate_query($amountTransaferredQuery);
		$this->set ( 'amountTransferred', $amountTransferred );
		
		$salesmenList = $this->Slaves->query ( "select id,name,mobile from salesmen WHERE active_flag=1 AND dist_id = $distId" );
		$this->set ( 'salesmenList', $salesmenList );
		
		$distList = $this->Slaves->query ( "select distributors.id,distributors.company,users.mobile from distributors,users WHERE users.id =distributors.user_id order by company" );
		$this->set ( 'distList', $distList );
		
		// $amountCollected = array();
		// $averageResult=$this->Slaves->query("SELECT avg(sale) as avg_ret, retailer_id from retailers_logs use index(idx_date),retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id = $distId AND retailers_logs.date > '".date('Y-m-d',strtotime('-30 days'))."' group by retailer_id");
		// foreach($averageResult as $avg){
		// $amountCollected[$avg['retailers_logs']['retailer_id']]['average'] = $avg['0']['avg_ret'];
		// }
		// $averageResult1=$this->Slaves->query("SELECT avg(sale) as avg_ret, retailer_id from retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id = $distId group by retailer_id");
		// foreach($averageResult1 as $avg){
		// $amountCollected[$avg['retailers_logs']['retailer_id']]['average1'] = $avg['0']['avg_ret'];
		// }
		// $this->set('amountCollected',$amountCollected);
		
		// $res=$this->Slaves->query("select st.ref2_id, sum(slt.collection_amount) as sm from salesman_transactions slt join shop_transactions st on (slt.shop_tran_id = st.id) inner join retailers ON (retailers.id = st.ref2_id AND retailers.parent_id = $distId) where st.type = 18 group by st.ref2_id");
		// $setupCollected = array();
		// foreach($res as $ac){
		// $setupCollected[$ac['st']['ref2_id']] = $ac['0']['sm'];
		// }
		// $this->set('setupCollected',$setupCollected);
	}
	
	function changeDistributor(){
		$distid = $_REQUEST['sid'];
		$rid = $_REQUEST['rid'];
		
		$data = $this->Retailer->query("SELECT slab_id FROM distributors WHERE id = $distid");
		$slab = $data['0']['distributors']['slab_id'];
		
		if($distid != 0){
			$retailers = $this->User->query("select rental_flag from retailers where id = $rid");
			$rental_flag = $retailers[0]['retailers']['rental_flag'] ? 1 : 0;
			$this->Retailer->query("UPDATE retailers 
					SET parent_id = $distid,maint_salesman = null,slab_id=$slab, rental_flag = $rental_flag, modified = '".date('Y-m-d H:i:s')."' 
					WHERE id = $rid");
			$this->Shop->updateSlab($slab,$rid,RETAILER);
		}
		$this->autoRender = false;
	}
	
	
	function retailerSale($frm=null,$to=null,$vendor=0,$dist=null)
	{
//		if($this->Session->read('Auth.User.group_id') !=ADMIN && $this->Session->read('Auth.User.id') != 1)
//		$this->redirect('/shops/view');

		if(isset($_REQUEST['from']))
		{
			$frm=$_REQUEST['from'];
			$to=$_REQUEST['to'];
		}
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');


		$nodays=(strtotime($to) - strtotime($frm))/ (60 * 60 * 24);
		$nodays += 1;

		if($nodays <= 8){
			$fdarr = explode("-",$frm);
			$tdarr = explode("-",$to);

			$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
			$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];

			$vendorDDResult=$this->Slaves->query("select id,company from vendors");
			$distDDResult=$this->Slaves->query("select id,company from distributors where active_flag = 1 AND parent_id = 3 order by company");
				
			$this->set('vendorDDResult',$vendorDDResult);
			$this->set('distDDResult',$distDDResult);
				
			if($vendor == 0)
			$vndStr = '';
			else
			$vndStr = ' and va.vendor_id='.$vendor.' ';
				
			if(empty($dist))
			$distStr = '';
			else
			$distStr = ' and distributors.id='.$dist.' ';
				
			/*$success=$this->Retailer->query("SELECT sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",va.amount,0)) as Amount,sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",1,0)) as success,sum(if(va.api_flag=1 AND va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",1,0)) as app_success,sum(if(va.api_flag=2 AND va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",1,0)) as ussd_success,sum(if(va.status = ".TRANS_FAIL." OR va.status = ".TRANS_REVERSE.",1,0)) as failure from vendors_activations as va WHERE va.date between '".$fd."' and '".$ft."' $vndStr order by va.id desc");
			 $this->set('success',$success);*/

			//for operator wise SUCCESSFUL reversal
			//SET @@group_concat_max_len = 25000;
			
			
			$operatorSale=$this->Slaves->query("
                        SELECT 
                            p.name,
                            p.id,
                            va.retailer_id,
                            count(va.id) as count,
                            sum(if(va.retailer_id != 13 AND va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",va.amount,0)) as b2bsuccess,
                            sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",va.amount,0)) as success,
							sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE." and vendors.update_flag=0,va.amount,0)) as api_success,
							sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE." and vendors.update_flag=1,va.amount,0)) as modem_success,
                            sum(if(va.status != ".TRANS_FAIL." AND va.status != ".TRANS_REVERSE.",1,0)) as scount, 
                            sum(if(va.api_flag=1,1,0)) as app_sale, 
                            sum(if(va.api_flag=2,1,0)) as ussd_sale , 
                            sum(if(va.api_flag=3,1,0)) as android_sale , 
                            sum(if(va.api_flag=5,1,0)) as java_sale,
                            sum(if(va.api_flag=9,1,0)) as web_sale,
                            sum(va.amount*(shop_transactions.discount_comission + if(distributors.parent_id = 3,(distributors.margin*100/(100+distributors.margin)) ,0) + (super_distributors.margin*100/(100+super_distributors.margin)))) as comm,
                            sum(va.amount) as tot
                        FROM 
                            vendors_activations va JOIN products p on (va.product_id=p.id)
                            left join retailers ON (retailers.id = va.retailer_id)
							left join vendors ON (vendors.id = va.vendor_id)
left join shop_transactions ON (shop_transactions.id = va.shop_transaction_id)
left join distributors ON (distributors.id = retailers.parent_id)
left join super_distributors ON (super_distributors.id = distributors.parent_id)
                        WHERE 
                            va.date between '".$fd."' and '".$ft."' $vndStr $distStr
                        GROUP BY 
                            p.id 
                        ORDER BY success desc");
            
           
			$this->set('operatorSale',$operatorSale);

			$retCount=$this->Slaves->query("  SELECT
                                                             va.api_flag ,count( DISTINCT va.retailer_id ) as ret_count
                                                        FROM 
                                                            vendors_activations va 
                                                            left join retailers ON (retailers.id = va.retailer_id)
                                                        left join distributors ON (distributors.id = retailers.parent_id)
                                                            WHERE 
                                                            va.date between '".$fd."' and '".$ft."' $vndStr $distStr
                                                        GROUP BY 
                                                            va.api_flag
                                                        ");
			$retCountArr = array();
			foreach($retCount as $cnt){
				$retCountArr[$cnt['va']['api_flag']] = $cnt[0]['ret_count'];
			}
			$this->set('retCountArr',$retCountArr);
				
			$success = array();
			$success['sale'] = 0;
			$success['success'] = 0;
			$success['ussd'] = 0;
			$success['app'] = 0;
			$success['android'] = 0;
			$success['java'] = 0;
			$success['windows7'] = 0;
			$success['windows8'] = 0;
			$success['web'] = 0;
			$success['tot'] = 0;
			$success['comm'] = 0;
			$success['failed'] = 0;
			$success['api_success'] = 0;
			$success['modem_success'] = 0;
            
            if (!empty($operatorSale) && count($operatorSale) > 0) {
                foreach ($operatorSale as $sale) {
                    //if (($vendor == 0 && $sale['va']['retailer_id'] != 13) || $vendor != 0){
                    	//$this->General->logData("/tmp/saledata.txt",date("Y-m-d H:i:s")." :: ".$sale['p']['name']." ::sale ::".$sale['0']['success']." total :: ".$success['sale']);
            
	                    $success['sale'] += ($vendor == 0) ? $sale['0']['b2bsuccess'] : $sale['0']['success'];
	                    $success['success'] += $sale['0']['scount'];
	                    $success['failed'] += $sale['0']['count'] - $sale['0']['scount'];
	                    $success['ussd'] += $sale['0']['ussd_sale'];
	                    $success['app'] += $sale['0']['app_sale'];
	                    $success['android'] += $sale['0']['android_sale'];
	                    $success['java'] += $sale['0']['java_sale'];
	                    $success['windows7'] += isset($sale['0']['windows7_sale'])?$sale['0']['windows7_sale']:0;
	                    $success['windows8'] += isset($sale['0']['windows8_sale'])?$sale['0']['windows8_sale']:0;
	                    $success['web'] += $sale['0']['web_sale'];
	                    $success['tot'] += $sale['0']['tot'];
	                    $success['comm'] += $sale['0']['comm'];
						$success['api_success'] += $sale['0']['api_success'];
	                    $success['modem_success'] += $sale['0']['modem_success'];
                    //}
                }
            }
            $this->set('success',$success);
             
			/*//for operator wise sale of successful transaction
			 $operatorSuccessSale=$this->User->query("select p.name,sum(va.amount) as Total,count(va.id) as count from vendors_activations va join products p on (va.product_id=p.id)
			 WHERE (va.status<>'".TRANS_FAIL."' and va.status<>'".TRANS_REVERSE."')
			 and va.date between '".$fd."' and '".$ft."' $vndStr group by p.name order by Total desc");
			 	
			 $this->set('operatorSuccessSale',$operatorSuccessSale);*/
			/*$retailers = array();
			 foreach($successSaleResult as $result)
			 {
				if(!empty($result['r']['name']))
				{
				$name = $result['r']['name'];
				}
				else $name = $result['r']['mobile'];

				$retailers[$result['r']['id']] = array('name' => $name,'mobile' => $result['r']['mobile'],'shopname' => $result['r']['shopname'],'tot' => $result['0']['Amount'],'balance' => $result['r']['balance'],'min' => $result['0']['firstDay'],'sale' => 0);

				$this->set('retailers',$retailers);
				}*/
			$this->set('frm',$frm);
			$this->set('to',$to);
			$this->set('days',$nodays);
			$this->set('vendor',$vendor);
			$this->set('dist',$dist);
		}
		else {
			$this->set('frm',$frm);
			$this->set('to',$to);
			$this->set('days',$nodays);
			$this->set('vendor',$vendor);
			$this->set('dist',$dist);
		}
	}
	
	
	
	function regReversal(){
		App::import('Controller', 'Apis');
		$obj = new ApisController;
		$obj->constructClasses();
		$ret = $obj->reversal(array('id'=>$_REQUEST['id'],'mobile'=>$_REQUEST['mobile'],'user_id'=>$_SESSION['Auth']['User']['id'], 'turnaround_time' => $_REQUEST['turnaroundTime']),'json',$_SESSION['Auth']['User']['id']);
                //echo $ret['status'];
		if(isset($ret['mobile'])){
			//$this->General->sendMessage($ret['mobile'],$ret['msg'],$ret['root']);
		}
		echo json_encode($ret);
		$this->autoRender = false;
	}
	
	/*function operatorStatus($frm=null,$to=null){
		if(isset($_REQUEST['from'])){
			$frm=$_REQUEST['from'];
			$to=$_REQUEST['to'];
		}
		//echo "From retailer panel";
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
		
		//echo "FROM".$frm;
		//echo "TO".$to;
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		//for operator wise SUCCESSFUL reversal
		$operatorReversalsResult=$this->Slaves->query("select p.name,sum(va.amount) as total from vendors_activations va join products p on (va.product_id=p.id)
				where (va.status='".TRANS_REVERSE."') and va.date between '".$fd."' and '".$ft."' group by p.name");
		$this->set('operatorReversalResult',$operatorReversalsResult);
		
		//for operator wise sale of successful transaction
		$operatorSuccessSale=$this->Slaves->query("select p.name,sum(va.amount) as Total from vendors_activations va join products p on (va.product_id=p.id)
				WHERE (va.status<>'".TRANS_FAIL."' and va.status<>'".TRANS_REVERSE."')  
				and va.date between '".$fd."' and '".$ft."' group by p.name order by Total desc");
		
		$this->set('operatorSuccessSale',$operatorSuccessSale);
		$this->set('frm',$frm);
		$this->set('to',$to);
	}*/
	
	
	 function tranReversal($frm=null,$to=null,$vendor=null)
	{ 
		if(isset($_REQUEST['from'])){
			$frm=$_REQUEST['from'];
			$to=$_REQUEST['to'];
		}
        $query = "";
        
        if(isset($_REQUEST['b2c_flag'])){
            $query.= "AND va.retailer_id =".B2C_RETAILER;
            $this->set('b2c_flag',$_REQUEST['b2c_flag']);
        }
       
       
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
		
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
                $qpart = "";
                if(!empty($vendor)){
                    $qpart = "AND v.id = $vendor";
                }
				
		    $retcords = $this->Shop->getMemcache("newretailers");

			if(empty($retcords)){
				
				$getNewRetailer = $this->Slaves->query("select retailers.id from retailers where date(created)>='".date("Y-m-d", strtotime("-30 day"))."' and date(created)<='".date("Y-m-d")."'");
				
				$this->Shop->setMemcache("newretailers",$getNewRetailer,1*24*60*60);
				
			}
		
			if(!empty($retcords)){

				foreach ($retcords as $val){

					$retailerData[$val['retailers']['id']] = $val['retailers']['id'];
				}

			}
		
		
		//reversals in process transactions
		$success=$this->Slaves->query("SELECT c.comments,t.name,v.shortForm,v.company,r.name,r.id,r.mobile,r.shopname,p.name,va.id,va.vendor_refid,va.mobile, va.ref_code, va.amount, va.status, va.timestamp, complaints.in_date,complaints.in_time,complaints.id,complaints.takenby,complaints.turnaround_time
				from complaints inner join vendors_activations va ON (complaints.vendor_activation_id = va.id) join comments c on(va.ref_code=c.ref_code) join taggings t on(t.id = c.tag_id) join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id)
				where  complaints.in_date between '$fd' and '$ft' AND complaints.resolve_flag = 0  $query $qpart  group  by ref_code order by if(UNIX_TIMESTAMP(complaints.turnaround_time) > 0, complaints.turnaround_time, '3020-01-01 00:00:00') asc, complaints.id desc");
                    
		foreach ($success as $val) {

			if (in_array($val['r']['id'], $retailerData)) {
				$newRetailer[$val['va']['ref_code']] = $val;
			} else {
				$data[] = $val;
			}
		}

		foreach ($newRetailer as $newretval) {

			$successdata[] = $newretval;
		}

		foreach ($data as $dataval) {

			$successdata[] = $dataval;
		}


		$retailerNameResult=$success['0']['r']['name'];
		
//		$closed_count = $this->Slaves->query("SELECT count(*) as count
//										from complaints 
//										inner join vendors_activations va ON (complaints.vendor_activation_id = va.id) 
//										join products p on(p.id=va.product_id) 
//										left join users ON (users.id = complaints.closedby) 
//										join vendors v on(v.id=va.vendor_id) 
//										where  complaints.resolve_date between '$fd' and '$ft' 
//										AND complaints.resolve_flag = 1  $qpart $query");
                
                $closed_count = $this->Slaves->query("SELECT count(distinct vendor_activation_id) count FROM complaints 
                                            WHERE resolve_date BETWEEN '$fd' AND '$ft' AND resolve_flag = 1");
		
		$this->set("closed_count", $closed_count[0][0]['count']);
		
		$this->set('success',$successdata);
		
		$this->set('frm',$frm);
		$this->set('to',$to);
                $this->set('vendor',$vendor);
                
                $vendorDDResult=$this->Slaves->query("select id,company from vendors");
		$this->set('vendorDDResult',$vendorDDResult);
		$this->set('retailerData',$retailerData);
                
	}
	
	function closedComplaints($frm = null, $to = null, $vendor = null, $b2c_flag = null){
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
		
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		$query = "";
		if(isset($b2c_flag)){
			$query .= " AND va.retailer_id =".B2C_RETAILER;
		}
		if(!empty($vendor)){
			$query .= " AND v.id = $vendor";
		}
                
                $closed_temp = $this->Slaves->query("SELECT max(created) comment_created, a.* FROM (SELECT c.comments,c.created,t.name t_name,v.shortForm, v.company,users.name users_name,p.name,va.id va_id,va.vendor_refid,va.mobile, va.ref_code,
				va.amount, va.status, va.timestamp, complaints.*
				from complaints
				inner join vendors_activations va ON (complaints.vendor_activation_id = va.id)
                                join comments c on(va.ref_code=c.ref_code)
                                join taggings t on(t.id = c.tag_id)
				join products p on(p.id=va.product_id)
				left join users ON (users.id = complaints.closedby)
				join vendors v on(v.id=va.vendor_id)
				where complaints.resolve_date between '$fd' and '$ft' AND complaints.resolve_flag = 1 $query
                                order by complaints.id desc) a GROUP BY va_id ORDER BY a.resolve_date desc, a.resolve_time desc");
                
                $i = 0;
                foreach($closed_temp as $c_t) {
                        $closed[$i]['c']            = array('comments'=>$c_t['a']['comments'],'created'=>$c_t[0]['comment_created']);
                        $closed[$i]['t']            = array('name'=>$c_t['a']['t_name']);
                        $closed[$i]['v']            = array('shortForm'=>$c_t['a']['shortForm'],'company'=>$c_t['a']['company']);
                        $closed[$i]['users']        = array('name'=>$c_t['a']['users_name']);
                        $closed[$i]['p']            = array('name'=>$c_t['a']['name']);
                        $closed[$i]['va']           = array('id'=>$c_t['a']['va_id'],'vendor_refid'=>$c_t['a']['vendor_refid'],'mobile'=>$c_t['a']['mobile'],'ref_code'=>$c_t['a']['ref_code'],'amount'=>$c_t['a']['amount'],'status'=>$c_t['a']['status'],'timestamp'=>$c_t['a']['timestamp']);
                        $closed[$i]['complaints']   = array('id'=>$c_t['a']['id'],'vendor_activation_id'=>$c_t['a']['vendor_activation_id'],'takenby'=>$c_t['a']['takenby'],'closedby'=>$c_t['a']['closedby'],'in_date'=>$c_t['a']['in_date'],'in_time'=>$c_t['a']['in_time'],'resolve_date'=>$c_t['a']['resolve_date'],'resolve_time'=>$c_t['a']['resolve_time'],'turnaround_time'=>$c_t['a']['turnaround_time'],'resolve_flag'=>$c_t['a']['resolve_flag']);
                        $i++;
                }
                
		$this->set("closed", $closed);
                
                $temp_closed = array();
                foreach($closed as $close) {
                        $temp_closed[] = $close['va']['ref_code'];
                }
                
                $resolution_tag = $this->Slaves->query("SELECT * FROM (SELECT taggings.name, comments.ref_code FROM comments
                                            LEFT JOIN taggings ON (taggings.id = comments.tag_id)
                                            WHERE comments.ref_code IN ('".implode("','", $temp_closed)."')
                                            ORDER BY comments.created DESC) as t GROUP BY t.ref_code");
                
                $temp_rs = array();
                foreach($resolution_tag as $tag) {
                        $temp_rs[$tag['t']['ref_code']] = $tag['t']['name'];
                }
                
                $this->set('resolution_tag', $temp_rs);
	}
	
	function inProcessTransactions($fromDate=null, $toDate=null, $vendorIds=null, $productIds=null){
		$fromDate = isset($_REQUEST['fromDate']) ? $_REQUEST['fromDate'] : date('d-m-Y');
		$toDate = isset($_REQUEST['toDate']) ? $_REQUEST['toDate'] : date('d-m-Y');
		
		$query = "";

// 		if(isset($_REQUEST['b2c_flag']) && $_REQUEST['b2c_flag']){
// 			$query.= " and va.retailer_id = ".B2C_RETAILER;
// 			$this->set('b2c_flag', $_REQUEST['b2c_flag']);
// 		}
		
		$modem_flag = isset($_REQUEST['modem_flag']) ? $_REQUEST['modem_flag'] : 1;
		$api_flag = isset($_REQUEST['api_flag']) ? $_REQUEST['api_flag'] : 1;
		if($modem_flag && !$api_flag){
			$query .= " and v.update_flag=1 ";
		}
		else if(!$modem_flag && $api_flag){
			$query .= " and v.update_flag=0 ";
		}
		else {
			$modem_flag = $api_flag = 1;
		}
		$this->set('modem_flag', $modem_flag);
		$this->set('api_flag', $api_flag);
		
		$vendorIds = isset($_REQUEST['vendorIds']) ? $_REQUEST['vendorIds'] : $vendorIds;
		if(!empty($vendorIds)) {
			$query .= " and v.id IN ($vendorIds) ";
			$vendorIds = explode(',', $vendorIds);
		}
		
		$productIds = isset($_REQUEST['productIds']) ? $_REQUEST['productIds'] : $productIds;
		if(!empty($productIds)) {
			$query .= " and p.id IN ($productIds) ";
			$productIds = explode(',', $productIds);
		}
		
		$toDate = $fromDate;
		$fdarr = explode("-", $fromDate);
		$tdarr = explode("-", $toDate);
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		$time = date('Y-m-d H:i:s', strtotime('-5 minutes'));
		
		//in process transactions
		$process = $this->Slaves->query("SELECT v.*,p.id,p.name,va.mobile, va.retailer_id, va.ref_code, va.vendor_refid, va.amount,
					va.status, va.timestamp, va.date, (count(c.id) - sum(c.resolve_flag)) as complaint_flag, vt.sim_num, dd.device_id,
					v.active_flag
				from vendors_activations va
				join products p on(p.id=va.product_id)
				join vendors v on(v.id=va.vendor_id)
				left join complaints c on c.vendor_activation_id = va.id
				left join vendors_transactions vt on vt.vendor_id = v.id and vt.ref_id = va.ref_code
				left join devices_data dd on dd.vendor_id = v.id and dd.mobile = vt.sim_num and dd.sync_date = '$fd'
				where va.date between '$fd' and '$ft'
				AND (va.status = 0 OR (va.prevStatus = 0 AND va.status = 4))
				AND va.timestamp <= '$time' $query
				group by va.id
				order by va.timestamp");
		
		
			

			$retcords = $this->Shop->getMemcache("newretailers");

			if(empty($retcords)){
				
				$getNewRetailer = $this->Slaves->query("select retailers.id from retailers where date(created)>='".date("Y-m-d", strtotime("-30 day"))."' and date(created)<='".date("Y-m-d")."'");
				
				$this->Shop->setMemcache("newretailers",$getNewRetailer,1*24*60*60);
				
			}
		
			if(!empty($retcords)){

				foreach ($retcords as $val){

					$retailerData[$val['retailers']['id']] = $val['retailers']['id'];
				}

			}

		$b2c = array(); 
		$complaint = array();
		$rest = array();
		$novendor = array();
		$newRetailer = array();
		
		$vendor_wise_count = array();
		$product_wise_count = array();
		$in_process_vendors = array();
		$in_process_products = array();
		$in_process_vendors_count = array();
		$in_process_products_count = array();
		foreach($process as $p){
			$in_process_vendors_count[$p['v']['id']] = isset($in_process_vendors_count[$p['v']['id']]) ? $in_process_vendors_count[$p['v']['id']] + 1 : 0;
			$in_process_products_count[$p['p']['id']] = isset($in_process_products_count[$p['p']['id']]) ? $in_process_products_count[$p['p']['id']] + 1 : 0;
			
			if(in_array($p['va']['retailer_id'],$retailerData)){
				$newRetailer[$p['va']['ref_code']] = $p;
			}
			else if($p['va']['retailer_id'] == 13)
				$b2c[] = $p;
			else if($p[0]['complaint_flag'])
				$complaint[] = $p;
			else
				$rest[] = $p;
			
			if($p['v']['active_flag'] == 0){
				$disabled_modem[] = $p;
			}
			if(empty($p['va']['vendor_refid']))
				$novendor[] = $p;
			
			$in_process_vendors[$p['v']['id']] = $p['v']['company'];
			$in_process_products[$p['p']['id']] = $p['p']['name'];
		}
		
		$this->set('b2c_count', count($b2c));
		$this->set('complaint_count', count($complaint));
		$this->set('disabled_modem_count', count($disabled_modem));
		$this->set('novendor_count', count($novendor));
		$this->set('normal_count', count($process) - count($b2c) - count($complaint) - count($novendor));
		$this->set('new_retailer', count($newRetailer));
		
		$total_count = count($process);
		$vendor_wise_count = array_filter(
			$in_process_vendors_count,
			function ($value) use($total_count) {
				return ($value > $total_count/10);
			}
		);
		
		$top_vendors = array_keys($vendor_wise_count);
		$product_wise_count = array_filter(
			$in_process_products_count,
			function ($value) use($total_count) {
				return ($value > $total_count/10);
			}
		);
		$top_products = array_keys($product_wise_count);
		$this->set('top_vendors', $top_vendors);
		$this->set('top_products', $top_products);
		$this->set('in_process_vendors', $in_process_vendors);
		$this->set('in_process_products', $in_process_products);
		$this->set('in_process_vendors_count', $in_process_vendors_count);
		$this->set('in_process_products_count', $in_process_products_count);
		
		$sorted_process = array();
		
		foreach ($newRetailer as $k => $n){
			$sorted_process[] = $n;
		}
		foreach($b2c as $b){
			$sorted_process[] = $b;
		}
		foreach($complaint as $c){
			$sorted_process[] = $c;
		}
		foreach($rest as $r){
			$sorted_process[] = $r;
		}
		$this->set('process',$sorted_process);
		
		$vendors = $this->Retailer->query("select id, company from vendors WHERE show_flag = 1 order by company");
		$this->set('vendors', $vendors);
		$this->set('vendorIds', $vendorIds);
		
		$products = $this->Slaves->query("select id, name from products where to_show = 1");
		$this->set('products', $products);
		$this->set('productIds', $productIds);
		
		$this->set('fromDate', $fromDate);
		$this->set('toDate', $toDate);
		$this->set('retailerData',$retailerData);
		
		$this->layout = 'sims';
	}
	
	/*function tranProcess($frm=null,$to=null,$vendorId=null)
	{
		if(isset($_REQUEST['from'])){
			$frm=$_REQUEST['from'];
			$to=$_REQUEST['to'];
		}
        
        $query = "";
		//echo "From retailer panel";
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
        
       
        if(isset($_REQUEST['b2c_flag'])){
            $query.= "AND va.retailer_id =".B2C_RETAILER;
            $this->set('b2c_flag',$_REQUEST['b2c_flag']);
            
        }
		
        $vendorIdQry = "";
        if (!empty($vendorId)) {
			$vendor = array();
			$vendorIdQry = " AND v.id IN ($vendorId)";
			$vendorId = explode(',', $vendorId);
		}
		$to = $frm;
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		$time = date('Y-m-d H:i:s',strtotime('-5 minutes'));
		//in process transactions
		$process=$this->Slaves->query("SELECT v.*,p.name,va.mobile, va.retailer_id, va.ref_code, va.vendor_refid, va.amount,
					va.status, va.timestamp, va.date, (count(c.id) - sum(c.resolve_flag)) as complaint_flag 
				from vendors_activations va 
				join products p on(p.id=va.product_id) 
				join vendors v on(v.id=va.vendor_id)
				left join complaints c on c.vendor_activation_id = va.id
				where va.date between '$fd' and '$ft' $vendorIdQry 
				AND (va.status = 0 OR (va.prevStatus = 0 AND va.status = 4)) 
				AND va.timestamp <= '$time' $query 
				group by va.id
				order by va.timestamp");
		$b2c = array();
		$complaint = array();
		$rest = array();
		foreach($process as $p){
			if($p['va']['retailer_id'] == 13)
				$b2c[] = $p;
			else if($p[0]['complaint_flag'])
				$complaint[] = $p;
			else 
				$rest[] = $p;
		}
		$sorted_process = array();
		foreach($b2c as $b){
			$sorted_process[] = $b;
		}
		foreach($complaint as $c){
			$sorted_process[] = $c;
		}
		foreach($rest as $r){
			$sorted_process[] = $r;
		}
		$this->set('process',$sorted_process);
        
        $vendors=$this->Retailer->query("select id,company from vendors WHERE show_flag = 1 order by company");
		$this->set('vendors',$vendors);
        $this->set('vendorId',$vendorId);
		
		$this->set('frm',$frm);
		$this->set('to',$to);
	}*/
	
	function showComments(){
		if($refCode = $_REQUEST['refCode']){
			$commentsResult=$this->Slaves->query("SELECT c.comments,c.ref_code,u1.name,u1.mobile,c.created,t.name
					from comments c join users u on (u.id=c.users_id) join users u1 on(c.mobile=u1.mobile)
					left join taggings t on t.id = c.tag_id
					where c.ref_code = '$refCode' order by c.created desc");
			echo "<div style='height:284px;overflow:auto;'>";
			echo "<table id='past_comments_".$refCode."' cellpadding='4' style='width:100%;'>";
			if($commentsResult){
				foreach($commentsResult as $cm){
					echo "<tr bgcolor='#CEF6F5' style='border: 2px solid white'>";
					echo "<td><span style='font-size:12px;'>By ".$cm['u1']['name']." @ ".$cm['c']['created']." on ".$cm['c']['ref_code']." (".$cm['t']['name'].") </span></br>".$cm['c']['comments']."</td>"; 
					echo "</tr>";
				}	
			}
			else
				echo "<tr><td>No comments</td></tr>";
			
			echo "</table>";
			echo "</div>";
		}
		$this->autoRender = false;
	}
	
	function showTransaction(){
		if($trans = $_REQUEST['id'])
                {
                    $detailedTransaction = $this->Slaves->query("SELECT vm.*, vendors.shortForm, vendors_transactions.processing_time 
				FROM vendors_messages vm INNER JOIN vendors ON ( vendors.id = vm.service_vendor_id ) 
				left join vendors_transactions ON (vendors_transactions.ref_id = vm.shop_tran_id AND vm.service_vendor_id=vendors_transactions.vendor_id AND vm.status !=  'pending') 
				WHERE vm.shop_tran_id='".$trans."' order by vm.id desc");
                    
                    $query= $this->Slaves->query("SELECT opening_closing.opening,opening_closing.closing,opening_closing.timestamp,vendors_activations.ref_code
                            from opening_closing 
                            left join vendors_activations on ( opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id) 
                            where vendors_activations.ref_code = '".$trans."'");
                    
                    if($query)
                    {
                        echo '
				<table border="1" cellpadding="0" cellspacing="0" width="100%">
                                <caption>Retailer Opening Closing</caption>
				<tr>
				<th>Opening</th>
				<th>Closing</th>
                                <th>Timestamp</th>
                                </tr>';
                                foreach($query as $q)
                                {
                                    echo "<tr>";
                                    echo "<td style='text-align:center;'>" . $q ['opening_closing'] ['opening'] . "</td>";
                                    echo "<td style='text-align:center;'>" . $q ['opening_closing'] ['closing'] . "</td>";
                                    echo "<td style='text-align:center;'>" . $q ['opening_closing'] ['timestamp'] . "</td>";
                                    echo "</tr>";
                                }
                                echo '</table>';
                                echo '<br/>';
	
                        }
                    
                            if($detailedTransaction)
                                {
                                echo '<br/>';
				echo '
                                    
				<table border="1" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<th>Ref Code</th>
				<th>Vendor</th>
				<th>Vendor Id</th>
				<th>Internal Response</th>
				<th>Provider Response</th>
				<th>Status</th>
				<th>Timestamp</th>
				<th>Processing Time</th>
				</tr>';

				foreach ( $detailedTransaction as $d)
                                    {                               
					$vendor = strtoupper ( $d ['vendors'] ['shortForm'] );
					echo "<tr>";
					echo "<td>" . $d ['vm'] ['shop_tran_id'] . "</td>";
					echo "<td>" . $vendor . "</td>";
					echo "<td>" . $d ['vm'] ['vendor_refid'] . "</td>";
					echo "<td>" . $this->Shop->errors ( $d ['vm'] ['internal_error_code'] ) . "</td>";
					echo "<td>" . $d ['vm'] ['response'] . "</td>";
					echo "<td>" . $d ['vm'] ['status'] . "</td>";
					echo "<td>" . $d ['vm'] ['timestamp'] . "</td>";
					echo "<td>" . $d ['vendors_transactions'] ['processing_time'] . "</td>";
                                        echo "</tr>";           
                                    }
				echo '</table>';
			}
			else
				echo "No transaction history";	
		}
		else 
			echo "Invalid transaction number";
		$this->autoRender = false;					
	}
	
	function userInfo($value = null, $parameter='mobno', $fromDate = null, $toDate = null){
// 		if(isset($_REQUEST['more']) && ($_REQUEST['more'] == 1))
// 			$time = strtotime('-30 days');
// 		else
// 			$time = 0;
// 		$limitString = " limit 3";
// 		if(!$fromDate || !$toDate){
// 			$fromDate = date('Y-m-d', $time);
// 			$toDate = date('Y-m-d');
// 		}
// 		else{
// 			$limitString = "";
// 		}
		
		if(empty($value)){
			
		}
		else if($parameter == 'mobno' || $parameter == 'subid'){
// 			$fromDate = date('Y-m-d',strtotime($fromDate));
// 			$toDate = date('Y-m-d',strtotime($toDate));
			
			if($parameter == 'mobno')
				$qStr = 'va.mobile';
			else if($parameter == 'subid') 
				$qStr = 'va.param';
			$offset = 0;
			if($this->RequestHandler->isAjax()){
				$page = $_REQUEST['page'];
				$offset = $page * 3;
				$this->autoRender = false;
			}	
			
			$retcords = $this->Shop->getMemcache("newretailers");

			if(empty($retcords)){
				
				$getNewRetailer = $this->Slaves->query("select retailers.id from retailers where date(created)>='".date("Y-m-d", strtotime("-30 day"))."' and date(created)<='".date("Y-m-d")."'");
				
				$this->Shop->setMemcache("newretailers",$getNewRetailer,1*24*60*60);
				
			}
		
			if(!empty($retcords)){

				foreach ($retcords as $val){

					$retailerData[$val['retailers']['id']] = $val['retailers']['id'];
				}

			}
			
			
			$userTransResult=$this->Slaves->query("
					select va.id,va.vendor_refid,va.mobile,va.ref_code,va.date,va.vendor_refid,va.param,va.operator_id,services.name,vendors.company,vendors.shortForm,va.mobile,p.id,p.name,va.amount,
						va.timestamp,va.status,r.id,r.mobile,r.shopname,r.name,r.address,sum(c.resolve_flag) as resolve_flag,count(c.resolve_flag) as count_resolve_flag,st.confirm_flag,st.id
 					from vendors_activations va 
					join vendors on(va.vendor_id=vendors.id) 
					join products p on(p.id=va.product_id) 
					join services on (p.service_id=services.id) 
					join retailers as r  on r.id  = va.retailer_id
					left join complaints c on c.vendor_activation_id = va.id
					left join shop_transactions st on st.id = va.shop_transaction_id 
 					where ".$qStr." ='$value'
					group by va.id
					order by va.timestamp desc limit 3 offset ".$offset);
			$this->set('userTrans', $userTransResult);
                        
			if($userTransResult){
				foreach($userTransResult as $utr){
					$retailers[$utr['r']['id']] = array('shop' => $utr['r']['shopname'], 'mobile' => $utr['r']['mobile']);
				}
				$this->set('retailers', $retailers);
			}	
			
			if($parameter == 'mobno'){
				$this->set('mobno',$value);
				$usersResult=$this->Slaves->query("select users.*,groups.name,groups.id  from users join groups on (users.group_id = groups.id) where users.mobile='$value'");
			}
			else if($parameter == 'subid'){
				$this->set('subid', $value);
				$usersResult=$this->Slaves->query("select users.*,groups.name  from users join groups on (users.group_id = groups.id)  where users.mobile='".$userTransResult['0']['va']['mobile']."'");
				$this->set('mobno', $usersResult['0']['users']['mobile']);
				$value = $usersResult['0']['users']['mobile'];
			}
			$this->set('uData', $usersResult);
			$this->set('retailerData', $retailerData);

			if($this->RequestHandler->isAjax()){
				foreach($userTransResult as $key => $data){
					
					if (in_array($data['r']['id'], $retailerData)) {

						$class = "background-color: rgba(255, 0, 0, 0.2)";
					} else {
						$class = '';
					}

					echo "<tr style = '".$class."'>";
					echo ">";
					echo "</td>";
					echo "<td> <a target='_blank' href='/panels/transaction/".$data['va']['ref_code']."'>".$data['va']['ref_code']."</a></td>";
					echo "<td><a target='_blank' href='/panels/retInfo/".$data['r']['mobile']."'>".$data['r']['mobile']."</a></td>";
					echo "<td><a target='_blank' href='/recharges/tranStatus/" . $data['va']['ref_code'] . "/" . $data ['vendors'] ['shortForm'] . "/" . $data ['va'] ['date'] . "/" . $data ['va'] ['vendor_refid'] . "'>".$data['vendors']['shortForm']."</a>";	
					echo "&nbsp;/".$data['va']['vendor_refid']."&nbsp;</td>";
					echo "<td>".$data['va']['param']."</td>";
					echo "<td>".$data['va']['operator_id']."&nbsp;</td>";
				    echo "<td>".$data['p']['name']."&nbsp;</td>";	
					echo "<td>".$data['va']['amount']."&nbsp;</td>";
					//	echo "<td>".$objShop->errors($data['vm']['internal_error_code'])."&nbsp;</td>";
					//  echo "<td>".$data['vm']['response']."&nbsp;</td>";
					//	echo "<td>".$data['vm']['status']."&nbsp;</td>";
						
					$status = '';
		  		    if($data['va']['status'] == '0'){
					$status = 'In Process';
		     		}else if($data['va']['status'] == '1'){
					$status = 'Successful';
			    	}else if($data['va']['status'] == '2'){
					$status = 'Failed';
				   }else if($data['va']['status'] == '3'){
					$status = 'Reversed';
				   }else if($data['va']['status'] == '4'){
					$status = 'Reversal In Process';
		     		}else if($data['va']['status'] == '5'){
					$status = 'Reversal declined';
			     	}
					
					$resolve_factor = $data ['0'] ['count_resolve_flag'] ? floor($data ['0'] ['resolve_flag'] / $data ['0'] ['count_resolve_flag']) : $data ['0'] ['resolve_flag'];
			     	
			     	$status_icon = 'icon_caution.png';
			     	$icon_complaint = 'resend.png';
			     	if(in_array($data ['va']['status'], array(0)))
			     		$status_icon = "hourglass.png";
			     	if(in_array($data['va']['status'], array(1, 4, 5)))
						$status_icon = "green-tick.png";
			   		
			     	$complaint_status = '';
			     	if (strlen($resolve_factor) > 0 && $resolve_factor == 0){
			     		$icon_complaint = "hourglass.png";
			     		$complaint_status = 'Complaint pending';
			     	}	
			     	else if(strlen($resolve_factor) > 0 && $resolve_factor == 1){
			     		$icon_complaint = "doubletick.png";
			     		$complaint_status = 'Complaint resolved';
			     	}
		     		
					$reversalStats = "<img title='".$status."' style='max-height:15px;' src='/img/".$status_icon."' />&nbsp;&nbsp;&nbsp;";
					if($icon_complaint == 'resend.png'){
	// 					$reversalStats .= "<img id='icon_complaint' src='/img/".$icon_complaint."' style='max-height:15px;cursor:pointer' onclick='check_complaint(".$data['va']['id'].");' />";
					}
					else{
						$reversalStats .= "<img title='".$complaint_status."' id='icon_complaint' src='/img/".$icon_complaint."' style='max-height:15px;' />";
					}
	
					echo "<td>".$reversalStats."&nbsp;</td>";
					echo "<td>".$data['va']['timestamp']."&nbsp;</td>";
					//echo "<td><input type=button value=\"Request Reversal\" ></td>";
					echo "<td><a href=javascript:modal_factory('transaction','".$data['va']['ref_code']."','Transaction-History');>Transaction</a></td>"; 
					echo "<td><a name='transactionInfo' data-refCode='".$data['va']['ref_code'].
					"' data-tId='".$data['va']['id']."' data-userMobile='".$usersResult['0']['users']['mobile']."' data-retMobile='".$data['r']['mobile'].
					"' data-retId='".$data['r']['id']."' data-shopTId='".$data['st']['id']."' data-clicked='false' ";

					if ($data ['va'] ['status'] == '0' || $data ['va'] ['status'] == '1' || $data ['va'] ['status'] == '4') {
						echo " data-actionReverse=true ";
						if (strlen($resolve_factor) > 0 && $resolve_factor == 0)
							echo " data-actionDecline=true ";
					} else if ($data ['va'] ['status'] == '5' && $data ['st']['confirm_flag'] == 1)
						echo " data-actionOpenTransaction=true ";
					else
						echo " data-actionPullBack=true ";
					if(!(strlen($resolve_factor) > 0 && $resolve_factor == 0))
						echo "data-complaint=true";
					echo " href=javascript:showActionModal();selectActions('".$data['va']['ref_code']."');>Comment</a> ";
					//echo "<td>".$data ['0'] ['resolve_flag']."&".$data ['0'] ['count_resolve_flag']."&".$resolve_factor."</td>";
					echo "</tr>";
				}
				exit;
			}
			//for user mobile -operator mapping
				
                        $mobileDetails = $this->General->getMobileDetailsNew($value);
			$this->set('mobileDetails', $mobileDetails);
				
			$userId = empty($usersResult['0']['users']['id']) ? "" : $usersResult['0']['users']['id'];
				
			/*$taggingResult=$this->Retailer->query("select distinct t.name from taggings t join user_taggings tu on(t.id=tu.tagging_id) where tu.user_id=$userId order by tu.id desc");
			//$tags=$taggingResult['0']['t']['name'];
			if(empty($taggingResult)) $taggingResult = array();
			$this->set('tags',$taggingResult);*/
			if($userId){
				$commentsResult=$this->Slaves->query("SELECT c.comments,c.ref_code,u1.name,u1.mobile,c.created
					from comments c join users u on (u.id=c.users_id) join users u1 on(c.mobile=u1.mobile)
					where u.id=$userId  order by c.created desc");
				if(empty($commentsResult)) $commentsResult = array();
				$this->set('comment',$commentsResult);
			}	
			// User to retailer Link
//			$userToRetailerResult=$this->User->query("select distinct r.mobile, r.shopname,r.name,r.address from retailers r join shop_transactions st on (r.id = st.ref1_id ) where st.user_id=$userId and st.type='".RETAILER_ACTIVATION."'");
//			if(empty($userToRetailerResult)) $userToRetailerResult = array();
//			$this->set('userRetailerResult', $userToRetailerResult);
		}
// 		$fromDate = date_format(date_create($fromDate), 'd-m-Y');
// 		$toDate = date_format(date_create($toDate), 'd-m-Y');
// 		$this->set('fromDate', $fromDate);
// 		$this->set('toDate', $toDate);
	}
	       
	function retInfo($retMobile=null,$retid=null,$from=null,$to=null,$chk=null){
		 
		$pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
		$this->set('objShop',$this->Shop);
		if(!is_null($retid) && $retid != '-1' && trim($retMobile) == 'temp'){
			$retailerIdResult=$this->Slaves->query("select id,user_id,maint_salesman,mobile from retailers where id='".$retid."'");
			$retMobile = $retailerIdResult['0']['retailers']['mobile'];
		}
		else if(!empty($retMobile)){
			$retailerIdResult=$this->Slaves->query("select id,user_id,maint_salesman from retailers where mobile='$retMobile'");
		}else{
			return;
		}
		
		if(empty($retailerIdResult))return;
		if(!isset($from)) $from=date('d-m-Y', strtotime('-1 day'));
		if(!isset($to)) $to=date('d-m-Y');

		//echo "from".$from;
		//echo "to".$to;

		$this->set('from',$from);
		$this->set('to',$to);

		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);

		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		//$retailerIdResult=$this->Retailer->query("select id,user_id,maint_salesman from retailers where mobile='$retMobile'");
		$retId=isset($retailerIdResult['0']['retailers']['id']) ? $retailerIdResult['0']['retailers']['id'] : "";
			
		//$retailerUserId=$this->User->query("select id from users where mobile='".$retMobile."' ");
		$userId=isset($retailerIdResult[0]['retailers']['user_id']) ? $retailerIdResult[0]['retailers']['user_id'] : "";

		//salesman result
		//$salesmenResult=$this->User->query("select distinct sm.name,sm.mobile,r.name,sm.created from salesmen sm join salesman_transactions sst  on (sm.id=sst.salesman) join shop_transactions st on(sst.shop_tran_id=st.id) join retailers r on(r.id=st.ref2_id) where r.id=$retId order by st.id desc");
		$salesmenResult=$this->Slaves->query("select distinct sm.name,sm.mobile,r.name,sm.created from salesmen sm join retailers r on(r.salesman=sm.id OR r.maint_salesman=sm.id) where r.id=$retId");
		if(empty($salesmenResult))$salesmenResult=array();
		$this->set('salesmenResult',$salesmenResult);
		//operator DD
		/*$manualRequestMobileOperatorDD=$this->Retailer->query("select id,name from products where service_id=1");
		$this->set('operatorMDD',$manualRequestMobileOperatorDD);

		$manualRequestDthOperatorDD=$this->Retailer->query("select id,name from products where service_id=2");
		$this->set('operatorDDD',$manualRequestDthOperatorDD);*/

		$this->set('operators',$this->Shop->getProducts());

		//app based retailer request.
		$appRequestsResult=$this->Slaves->query("select params,method,description , timesatmp from app_req_log  where ret_id=$retId  AND (method = 'mobRecharge' OR method = 'dthRecharge' OR method = 'vasRecharge' OR method = 'mobBillPayment' OR method = 'utilityBillPayment' OR method = 'getBusTicket') AND date between '".$fd."' and '".$ft."' order by id desc");
		$appRequests = isset($appRequestsResult['0']['app_req_log']['params']) ? $appRequestsResult['0']['app_req_log']['params'] : "";
		$this->set('appRequests',$appRequestsResult);

		//logs for num change
		//$logs=$this->User->query("select u.name,l.description,l.modified from users as u,logs_cc as l where l.user_id=$userId and  u.id=l.parent_user_id order by modified desc");
		//$this->set('logs',$logs);
			

// 		$taggingResult=$this->Retailer->query("select distinct t.name,tu.transaction_id from taggings t join user_taggings tu on(t.id=tu.tagging_id) where tu.user_id=$userId order by tu.id desc");
		//$tags=$taggingResult['0']['t']['name'];
// 		if(empty($taggingResult))$taggingResult=array();
// 		$this->set('tags',$taggingResult);

		$mainArr = array();
        $retInfo = $this->Slaves->query("select  retailers.*, distributors.name, distributors.company, salesmen.name, ur.* 
        		from  retailers 
        		left join salesmen on (salesmen.id = retailers.maint_salesman) 
        		left join users on (retailers.user_id = users.id) 
        		left join distributors on (retailers.parent_id=distributors.id) 
        		left join unverified_retailers ur on ur.retailer_id = retailers.id
        		where retailers.id =  '" . $retId . "' ");
        if (empty($retInfo))
            $retInfo = array();
        else {
        	foreach($retInfo[0]['ur'] as $key => $row){
        		if(!in_array($key, array('id')))
        			$retInfo[0]['retailers'][$key] = $retInfo[0]['ur'][$key];
        	}
        }
        $this->set('info', $retInfo);
        if (count($retInfo) > 0) {
            $retailerAreaId = $retInfo['0']['retailers']['area_id'];
            $retailerUserId = $retInfo['0']['retailers']['user_id'];
        } else {
            $retailerAreaId = "";
            $retailerUserId = "";
        }

        $user_profile = $this->Slaves->query("select * from user_profile where user_id = '$retailerUserId' order by updated desc limit 1");
		if(!empty($user_profile)){
			if($retInfo[0]['retailers']['verify_flag'] != 1){
				$user_profile['0']['user_profile']['latitude'] = $retInfo[0]['retailers']['latitude'];
				$user_profile['0']['user_profile']['longitude'] = $retInfo[0]['retailers']['longitude'];
			}
			$this->set('user_profile', $user_profile['0']['user_profile']);
		}	
		
		
		if(!is_null($retailerAreaId))
		{
			$stateCityResult=$this->Slaves->query("Select la.name,lc.name,ls.name from locator_city lc join locator_area la on (la.city_id=lc.id) join locator_state ls on (lc.state_id=ls.id) where la.id=$retailerAreaId");
			$this->set('areaCityState',$stateCityResult);
		}

		$dataAll=$this->Slaves->query("
                    SELECT 
                            v.shortForm,
                            s.name,
                            r.name,
                            r.id,
                            r.mobile,
                            oc.opening,
                            oc.closing,
                            p.name,
                            va.mobile,
                            va.ref_code,
                            va.amount,
                            va.status,
                            va.api_flag,
                            va.cause,
                            GREATEST(va.timestamp,oc.timestamp) as timestamp,
                            oc.timestamp, 
                            va.timestamp

                    FROM    vendors_activations va 
                    JOIN    retailers r on(va.retailer_id=r.id) 
                    JOIN    products p on(p.id=va.product_id) 
                    JOIN    vendors v on(v.id=va.vendor_id) 
                    JOIN    services s on (p.service_id=s.id) 
		LEFT JOIN   opening_closing as oc ON (oc.shop_id = r.id AND oc.group_id = ".RETAILER." AND     oc.shop_transaction_id = va.shop_transaction_id) 
                    WHERE                              
                            (   (va.status='".TRANS_REVERSE_PENDING."') 
                                OR (va.status != 2 AND va.status !=3) 
                                OR (va.status=".TRANS_REVERSE." OR va.status=".TRANS_FAIL.") 
                            )
                    AND     va.retailer_id=$retId 
                    AND     va.date BETWEEN '".$fd."'  AND     '".$ft."' 
                        
                    ORDER BY   va.timestamp desc ");

		$transPanelResult = array();
		$reversalInProcessResults = array();
		$alreadyReversed = array();
		
		 $retcords = $this->Shop->getMemcache("newretailers");

			if(empty($retcords)){
				
				$getNewRetailer = $this->Slaves->query("select retailers.id from retailers where date(created)>='".date("Y-m-d", strtotime("-30 day"))."' and date(created)<='".date("Y-m-d")."'");
				
				$this->Shop->setMemcache("newretailers",$getNewRetailer,1*24*60*60);
				
			}
		
			if(!empty($retcords)){

				foreach ($retcords as $val){

					$retailerData[$val['retailers']['id']] = $val['retailers']['id'];
				}

			}
			
			$this->set('retailerData',$retailerData);
			$this->set('retId',$retId);
			
		$mpos_transactions = $this->Slaves->query("select *
				from shop_transactions st
				left join shop_transactions as sst ON (st.id = sst.ref2_id)
				left join mpos_transactions mt on mt.shop_transaction_id = st.id
				left join opening_closing oc on oc.shop_transaction_id = st.id
				where st.ref1_id = ".$retId."
				and st.type = ".MPOS_TRANSFER."
				and st.date between '$fd' and '$ft'
				order by st.timestamp desc");
		$this->set('mpos_transactions', $mpos_transactions);
		
		if(empty($dataAll))$dataAll = array();
		foreach( $dataAll as $key => $data){
			if(($data['va']['status'] != 2 AND $data['va']['status'] !=3)){
				array_push($transPanelResult,$data);
			}
			if($data['va']['status']==TRANS_REVERSE_PENDING){
				array_push($reversalInProcessResults,$data);
			}
			if(($data['va']['status']==TRANS_REVERSE ||  $data['va']['status']==TRANS_FAIL) ){
				array_push($alreadyReversed,$data);
			}
		}

		$reversalInProcessResults = Set::sort($reversalInProcessResults, '{n}.va.id', 'desc');
		// for transactoins by retailer
		$this->set('transRecords',$transPanelResult);

		$this->set('reversalInProcess',$reversalInProcessResults);
		//$reversalInProcessRefCode=$reversalInProcessResults['0']['va']['ref_code'];

		// for transactions that are reversed
		$this->set('alreadyReversed',$alreadyReversed);
		$this->set('transRecords',$transPanelResult);

		//for comments
		$commentsResult=$this->Slaves->query("SELECT c.ref_code,c.comments,u1.name,u1.mobile,c.created
		from comments c left join users u on (u.id=c.users_id) join users u1 on(c.mobile=u1.mobile)
		where c.retailers_id=$retId order by c.created  desc");
		
		if(empty($commentsResult))$commentsResult=array();
		$this->set('comment',$commentsResult);
		$this->set('mob',$retMobile);
			
		//for sms sent by retailers
		$smsResult=$this->Slaves->query("select vn.virtual_num,vn.message,vn.timestamp , vn.description from virtual_number vn where vn.mobile='$retMobile' and vn.date between '".$fd."' and '".$ft."' order by vn.timestamp desc ");
		if(empty($smsResult))$smsResult=array();
		$this->set('smsResult',$smsResult);

		//for ussd sent by retailers
		$ussdResult=$this->Slaves->query("select ussd_logs.request,ussd_logs.time,ussd_logs.date ,ussd_logs.extra,ussd_logs.sent_xml from ussds as ussd_logs where ussd_logs.request != '*6699#' AND ussd_logs.mobile='$retMobile' AND ussd_logs.request is not null AND ussd_logs.sent_xml is not null AND ussd_logs.date between '".$fd."' and '".$ft."' order by ussd_logs.date desc,ussd_logs.time desc");
		if(empty($ussdResult))$ussdResult=array();
		$this->set('ussdResult',$ussdResult);
			
		//for top-up requests by retailers
		/*$topUpResult=$this->User->query("select tr.amount,tr.type,tr.created from topup_request tr	 where tr.user_id=$retId and  Date(tr.created) between '".$fd."' and '".$ft."' order by tr.created desc");
		$this->set('topUpResult',$topUpResult);*/
			
		//for amount transferred to retailer
		$amountTransferred=$this->Slaves->query("select s.name,d.name,d.company,st.amount,st.timestamp,st.ref1_id,st.ref2_id,sum(sst.collection_amount) as colAmt from shop_transactions st join distributors d on(d.id=st.ref1_id AND st.confirm_flag != 1) join salesman_transactions sst on(st.id=sst.shop_tran_id) join salesmen s on (s.id=sst.salesman) where st.type='".DIST_RETL_BALANCE_TRANSFER."'  and st.ref2_id=$retId and st.date between '".$fd."' and '".$ft."' group by sst.shop_tran_id order by st.timestamp desc");
		if(empty($amountTransferred))$amountTransferred=array();
		$this->set('amountTransferred',$amountTransferred);
		$this->set('pageType',$pageType);
		if($pageType == 'csv'){
           
             
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			//$line = array("Txn Date","Txn Id","Signal7 T_ID","Number","Operator","Amount","Opening","Closing","Earning","Reversal Date","Description","Status");
			$line = array("S.No.","Tran Id","Recharge","Vendor","Cust Mob","Operator","Amount","Status","Reason","Via","Opening","Closing","Earning","Timestamp");
			$csv->addRow($line);
			$i=1;
			foreach ($transPanelResult as $data) {

				$tot = $tot + $data['va']['amount'];
				$earn = ($data['va']['amount'] - ($data['oc']['opening'] - $data['oc']['closing']));
				$tot_earn = $tot_earn + $earn;

				$reversalStats = '';
				if ($data['va']['status'] == '0') {
					$reversalStats = 'In Process';
				} else if ($data['va']['status'] == '1') {
					$reversalStats = 'Successful';
				} else if ($data['va']['status'] == '2') {
					$reversalStats = 'Failed';
				} else if ($data['va']['status'] == '3') {
					$reversalStats = 'Reversed';
				} else if ($data['va']['status'] == '4') {
					$reversalStats = 'Complaint In Process';
				} else if ($data['va']['status'] == '5') {
					$reversalStats = 'Complaint declined';
				}
				//echo "<td>".$reversalStats."&nbsp;</td>";
				$via = "";
				if ($data['va']['api_flag'] == 0) {
					$via = 'SMS';
				} else if ($data['va']['api_flag'] == 1) {
					$via = 'API';
				} else if ($data['va']['api_flag'] == 2) {
					$via = 'USSD';
				} else if ($data['va']['api_flag'] == 3) {
					$via = 'ANDROID';
				}  else if ($data['va']['api_flag'] == 5) {
					$via = 'JAVA';
				} else if ($data['va']['api_flag'] == 4) {
					$via = 'PARTNER';
				}
				//echo "<td>$via</td>";
				//echo "<td>".$data['oc']['opening']."&nbsp;</td>";
				//echo "<td>".$data['oc']['closing']."&nbsp;</td>";
				//echo "<td>".round($earn,2)."&nbsp;</td>";
				//echo "<td>".$data['0']['timestamp']."&nbsp;</td>";
				//echo "</tr>";
				$temp = array($i, $data['va']['ref_code'], $data['s']['name'], $data['v']['shortForm'], $data['va']['mobile'], $data['p']['name'], $data['va']['amount'], $reversalStats, $data['va']['cause'],$via, $data['oc']['opening'], $data['oc']['closing'], round($earn, 2), $data['0']['timestamp']);
				$csv->addRow($temp);
				$i++;
			}

			//Retailers "REVERSED" Transactions
			$count = 1;
			$csv->addRow(array('Retailers "REVERSED" Transactions'));
			$csv->addRow(array("S.No.","Tran Id","Recharge","Vendor","Cust Mob","Operator","Amount","Status","Reason","Via","Opening","Closing","Timestamp"));
			foreach ($alreadyReversed as $d) {
				$ps = '';
				if ($d['va']['status'] == '0')
				$ps = 'In Process';
				else if ($d['va']['status'] == '1')
				$ps = 'Successful';
				else if ($d['va']['status'] == '2')
				$ps = 'Failed';
				else if ($d['va']['status'] == '3')
				$ps = 'Reversed';
				else if ($d['va']['status'] == '4')
				$ps = 'Reversal In Process';
				else if ($d['va']['status'] == '5')
				$ps = 'Reversal declined';


				if ($d['va']['api_flag'] == 0) {
					$via = 'SMS';
				} else if ($d['va']['api_flag'] == 1) {
					$via = 'API';
				} else if ($d['va']['api_flag'] == 2) {
					$via = 'USSD';
				} else if ($data['va']['api_flag'] == 3) {
					$via = 'ANDROID';
				}  else if ($data['va']['api_flag'] == 5) {
					$via = 'JAVA';
				} else if ($data['va']['api_flag'] == 4) {
					$via = 'PARTNER';
				}


				$temp = array($count,$d['va']['ref_code'],$d['s']['name'],$d['v']['shortForm'],$d['va']['mobile'],$d['p']['name'],$d['va']['amount'],$ps,$d['va']['cause'],$via,$d['oc']['opening'],$d['oc']['closing'],$d['0']['timestamp']);
				$csv->addRow($temp);
				$count++;
			}

			echo $csv->render('RetailerTransactions_'.$fd.'_'.$ft.'.csv');


		}else{
			$this->render("ret_info");
		}

		//$this->Autorender = false;
	}
	
		
	function ussdLogs($retMobile,$from=null,$to=null){
            $this->set('objShop',$this->Shop);
            if(!isset($from) || !isset($to)){
                $from=date('d-m-Y',strtotime('-5 days'));
                $to=date('d-m-Y');
            }
              
            $fdarr = explode("-",$from);
            $tdarr = explode("-",$to);

            $fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
            $ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];

            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('mob',$retMobile);
            $logs=$this->Slaves->query("select ul.* FROM `ussds` ul where mobile='".$retMobile."' and level = 1 and `date`>=  '$fd' and `date`<=  '$ft' order by timestamp desc" );
            $this->set('logs',$logs);
            $this->render("retailer_ussd_log");
            
	}
        function appNotificationLogs($retMobile,$from=null,$to=null){
            $this->set('objShop',$this->Shop);
            if(!isset($from) || !isset($to)){
                $from=date('d-m-Y',strtotime('-5 days'));
                $to=date('d-m-Y');
            }
              
            $fdarr = explode("-",$from);
            $tdarr = explode("-",$to);

            $fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
            $ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];

            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('mob',$retMobile);
            //$logs=$this->User->query("select ul.* FROM `ussds` ul where mobile='".$retMobile."' and level = 1 and `date`>=  '$fd' and `date`<=  '$ft' order by timestamp desc" );
            $logs=$this->Slaves->query("select id , mobile , user_key , msg , notify_type , user_type , response ,created ,`date` FROM `notificationlog` where mobile='".$retMobile."' and `date`>=  '$fd' and `date`<=  '$ft' order by created desc" );
            //echo "select id , mobile , user_key , msg , notify_type , user_type , response ,created , modified FROM `notificationlog` where mobile='".$retMobile."' and `date`>=  '$fd' and `date`<=  '$ft' order by created desc";
            $output = array();
            foreach ($logs as $log){
                
                if(!empty($log["notificationlog"]["msg"])){
                    try{
                        $msgJ = json_decode($log["notificationlog"]["msg"],true);
                        $log["notificationlog"]["msg"] = empty($msgJ["msg"]) ? $log["notificationlog"]["msg"] : $msgJ["msg"] ;
                    }catch(Exception $e){                         
                        //$log["notificationlog"]["msg"] = $log["notificationlog"]["msg"];
                    }
                    
                }
                $output[] = $log;
            }
            $this->set('logs',$output);
            $this->render("retailer_app_noti_log");
            
	}
	
	function editRetailer($retMobile,$chk=null)
	{
		if(!is_null($chk)){
			$this->Retailer->query("update retailers 
					set email='".addslashes($_REQUEST['retailerEmail'])."',
					alternate_number='".$_REQUEST['alternate']."',
					block_flag=".$_REQUEST['retailerBlockFlag'].", 
					modified = '".date('Y-m-d H:i:s')."'  
					where mobile='$retMobile'");
			$this->Retailer->query("update unverified_retailers 
					set name='".addslashes($_REQUEST['retailerName'])."',
					shop_name='".addslashes($_REQUEST['retailerShopName'])."',
					area='".addslashes($_REQUEST['retailerArea'])."',
					address='".addslashes($_REQUEST['retailerAddress'])."',
					pin_code='" .$_REQUEST['retailerPin']."',
					modified = '".date('Y-m-d H:i:s')."'
					where retailer_id = ".$_REQUEST['rId']);
		}
		
		$retInfo = $this->Retailer->query("select retailers.*,distributors.company, ur.name, ur.shop_name, ur.area,
					ur.address, ur.pin_code
				from  retailers 
				left join unverified_retailers ur on ur.retailer_id = retailers.id
				left join users on (retailers.user_id = users.id) 
				left join  distributors on (retailers.parent_id=distributors.id) 
				WHERE retailers.mobile = '$retMobile'");
		foreach($retInfo[0]['ur'] as $key => $row){
			if($key != 'id')
				$retInfo[0]['retailers'][$key] = $retInfo[0]['ur'][$key];
		}
		$this->set('info',$retInfo);
		$this->set('retMobile',$retMobile);
		if(!is_null($chk))
		{
			$this->redirect('/panels/retInfo/'.$retMobile);
		}
	}
	
/*	
	function retailerTransaction($retId)
	{
		$retailerId=$retId;
		//$retailerTransactionResult=$this->Retailer->query("select st.id from shop_transactions st ,vendors_activations va where st.ref1_id=20 and st.id=va.shop_transaction_id");
		//foreach ($transId as $retailerTransactionResult)	
		//{
			$transPanelResult=$this->Retailer->query("select va.shop_transaction_id,va.ref_code,services.name,vendors.company,va.mobile,va.amount,vm.internal_error_code,vm.response,vm.status,va.timestamp
from vendors_activations va join shop_transactions st on(st.id=va.shop_transaction_id)  join vendors_messages vm on (va.ref_code=vm.shop_tran_id) join services on (vm.service_id=services.id) join vendors on(vm.service_vendor_id=vendors.id)
where st.ref1_id=$retId");
			$this->set('transRecords',$transPanelResults);	
	}
*/	
	
	function transaction($trans=null,$par=null){
		if(!empty($trans)){

			$qStr = 'va.ref_code';
			if(isset($par) && $par == 1){
				$qStr = 'va.vendor_refid';
			}else if(isset($par) && $par == 2){// for signal7 api transactions
				$partnerLog=$this->Slaves->query("SELECT  *
                                                                    FROM partners_log pl 
                                                                    LEFT JOIN partners p ON pl.partner_id = p.id 
                                                                    LEFT JOIN retailers r ON p.retailer_id = r.id 
                                                                    WHERE pl.partner_req_id = '$trans'");

				$this->set('ptran',$trans);
				$trans = $partnerLog[0]['pl']['vendor_actv_id'];
				$qStr = 'va.ref_code';

				if( $partnerLog !=null && count($partnerLog > 0) ){//$trans == 0 &&
					// render partners log here
					$this->set('partnerLog',$partnerLog);
					$this->set('renderTesting',true);
				}
			}

			if(empty($trans)) return; 
// 			$taggingResult=$this->Retailer->query("select distinct t.name from taggings t join user_taggings tu on(t.id=tu.tagging_id) where tu.transaction_id=$trans order by tu.id desc");
// 			$tags=$taggingResult['0']['t']['name'];
// 			$this->set('tags',$taggingResult);
				
			$transPanelResult_temp = $this->Slaves->query("SELECT a.*, complaints.id, complaints.turnaround_time FROM (SELECT services.id services_id, va.*, r.id r_id, r.name r_name, r.mobile r_mobile, r.shopname, services.name services_name,
                        vendors.id vendors_id, vendors.company, vendors.shortForm, p.name p_name, users.name users_name, users.id users_id,
                        sum(c.resolve_flag) as resolve_flag, count(c.resolve_flag) as count_resolve_flag, max(c.turnaround_time) as tat, c.id c_id, c.takenby		
			FROM vendors_activations va			
			JOIN products p ON ( va.product_id = p.id ) 
			JOIN services ON ( p.service_id = services.id ) 
			JOIN vendors ON ( va.vendor_id = vendors.id ) 
			JOIN retailers r ON ( va.retailer_id = r.id ) 
			LEFT JOIN users ON ( users.id = va.complaintNo ) 
			LEFT JOIN complaints c ON (c.vendor_activation_id = va.id)
			WHERE ".$qStr." =  '".$trans."' 
			group by va.id order by va.id desc) a 
                        LEFT JOIN complaints ON (complaints.vendor_activation_id = a.id) ORDER BY complaints.id DESC LIMIT 1");

                        $transPanelResult[0]['services']  = array('id'=>$transPanelResult_temp[0]['a']['services_id'],'name'=>$transPanelResult_temp[0]['a']['services_name']);
                        $transPanelResult[0]['va']        = array('id'=>$transPanelResult_temp[0]['a']['id'],'vendor_id'=>$transPanelResult_temp[0]['a']['vendor_id'],'product_id'=>$transPanelResult_temp[0]['a']['product_id'],'mobile'=>$transPanelResult_temp[0]['a']['mobile'],'param'=>$transPanelResult_temp[0]['a']['param'],'amount'=>$transPanelResult_temp[0]['a']['amount'],'discount_commission'=>$transPanelResult_temp[0]['a']['discount_commission'],'ref_code'=>$transPanelResult_temp[0]['a']['ref_code'],'vendor_refid'=>$transPanelResult_temp[0]['a']['vendor_refid'],'operator_id'=>$transPanelResult_temp[0]['a']['operator_id'],'shop_transaction_id'=>$transPanelResult_temp[0]['a']['shop_transaction_id'],'retailer_id'=>$transPanelResult_temp[0]['a']['retailer_id'],'invoice_id'=>$transPanelResult_temp[0]['a']['invoice_id'],'status'=>$transPanelResult_temp[0]['a']['status'],'prevStatus'=>$transPanelResult_temp[0]['a']['prevStatus'],'api_flag'=>$transPanelResult_temp[0]['a']['api_flag'],'cause'=>$transPanelResult_temp[0]['a']['cause'],'code'=>$transPanelResult_temp[0]['a']['code'],'timestamp'=>$transPanelResult_temp[0]['a']['timestamp'],'date'=>$transPanelResult_temp[0]['a']['date'],'extra'=>$transPanelResult_temp[0]['a']['extra'],'complaintNo'=>$transPanelResult_temp[0]['a']['complaintNo']);
                        $transPanelResult[0]['r']         = array('id'=>$transPanelResult_temp[0]['a']['r_id'],'name'=>$transPanelResult_temp[0]['a']['r_name'],'mobile'=>$transPanelResult_temp[0]['a']['r_mobile'],'shopname'=>$transPanelResult_temp[0]['a']['shopname']);
                        $transPanelResult[0]['vendors']   = array('id'=>$transPanelResult_temp[0]['a']['vendors_id'],'company'=>$transPanelResult_temp[0]['a']['company'],'shortForm'=>$transPanelResult_temp[0]['a']['shortForm']);
                        $transPanelResult[0]['p']         = array('name'=>$transPanelResult_temp[0]['a']['p_name']);
                        $transPanelResult[0]['users']     = array('name'=>$transPanelResult_temp[0]['a']['users_name'],'id'=>$transPanelResult_temp[0]['a']['users_id']);
                        $transPanelResult[0][0]           = array('resolve_flag'=>$transPanelResult_temp[0]['a']['resolve_flag'],'count_resolve_flag'=>$transPanelResult_temp[0]['a']['count_resolve_flag'],'tat'=>$transPanelResult_temp[0]['complaints']['turnaround_time']);
                        $transPanelResult[0]['c']         = array('id'=>$transPanelResult_temp[0]['a']['c_id'],'takenby'=>$transPanelResult_temp[0]['a']['takenby']);

			$retcords = $this->Shop->getMemcache("newretailers");

			if(empty($retcords)){
				
				$getNewRetailer = $this->Slaves->query("select retailers.id from retailers where date(created)>='".date("Y-m-d", strtotime("-30 day"))."' and date(created)<='".date("Y-m-d")."'");
				
				$this->Shop->setMemcache("newretailers",$getNewRetailer,1*24*60*60);
				
			}
		
			if(!empty($retcords)){

				foreach ($retcords as $val){

					$retailerData[$val['retailers']['id']] = $val['retailers']['id'];
				}

			}
			
			$this->set('retailerData',$retailerData);
			
			
			if(empty($transPanelResult)) return; 
			$confirm_flag = $this->Slaves->query("SELECT confirm_flag FROM shop_transactions WHERE id = " . $transPanelResult['0']['va']['shop_transaction_id']);
			
			$this->set('confirm_flag',$confirm_flag['0']['shop_transactions']['confirm_flag']);
				
			$this->set('individualTransaction',$transPanelResult);

			$trans = $transPanelResult['0']['va']['ref_code'];
			$this->set('tran',$trans);
			$this->set('tran1',$transPanelResult['0']['va']['vendor_refid']);
				
			//for another table for response from OSS/PPI
			$detailedTransResult=$this->Slaves->query("SELECT vm.*,vendors.shortForm, vendors_transactions.processing_time FROM vendors_messages vm INNER JOIN vendors ON ( vendors.id = vm.service_vendor_id ) left join vendors_transactions ON (vendors_transactions.ref_id = vm.shop_tran_id AND vm.service_vendor_id=vendors_transactions.vendor_id AND vm.status !=  'pending') WHERE vm.shop_tran_id='".$trans."' order by vm.timestamp desc,vm.id desc");
			$this->set('detailedTransaction',$detailedTransResult);
				
			//for comments
			$commentsResult=$this->Slaves->query("SELECT c.comments,u.name,u.mobile,c.created,c.ref_code,t.name
					from comments c 
					left join users u on(c.mobile=u.mobile)
					left join taggings t on t.id = c.tag_id
					where c.ref_code='".$trans."'  order by c.created  desc ");
			$this->set('commentsResult',$commentsResult);
		}
	}
        
        function updateCallComplain() {
            
            $this->autoRender = false;
            
            $takenby        = $this->params['form']['takenby'];
            $complaint_id   = $this->params['form']['complaint_id'];

            $this->Retailer->query("Update complaints SET takenby = $takenby WHERE id = $complaint_id");
            
            echo $takenby == 0 ? $_SESSION['Auth']['User']['id'] : 0;
        }
	
	function openTransaction(){
		$id = $_REQUEST['id'];
		$shopid = $_REQUEST['shopid'];
                $turnaround_time = $_REQUEST['turnaround_time'];
		$data1 = $this->Retailer->query("SELECT status,ref_code FROM vendors_activations WHERE id = $id AND shop_transaction_id = $shopid");
		$data2 = $this->Retailer->query("SELECT confirm_flag FROM shop_transactions WHERE id = $shopid");

		if($data1['0']['vendors_activations']['status'] == TRANS_REVERSE_DECLINE && $data2['0']['shop_transactions']['confirm_flag'] == 1){
			$this->Retailer->query("UPDATE vendors_activations SET status = ".TRANS_REVERSE_PENDING." WHERE id = $id");
			$tags = $this->Retailer->query("select t.name 
					from comments c 
					inner join taggings t on t.id = c.id
					where t.type = 'Online Complaint'
					and c.ref_code = '".$data1['0']['vendors_activations']['ref_code']."'
					order by c.id desc");
			
			App::import('Controller', 'Apis');
			$ApisController = new ApisController;
			$ApisController->constructClasses();
			$turnaround_duration = $ApisController->getTurnaroundTime($id, $tags[0]['t']['name'], $turnaround_time);
			
			if(isset($turnaround_duration)){
				$pre_adjusted_turnaround_time = time() + ($turnaround_duration * 60 * 60);
				$pre_adjusted_date = new DateTime(date("Y-m-d H:i:s", $pre_adjusted_turnaround_time));
				$date = new DateTime(date("Y-m-d H:i:s", $pre_adjusted_turnaround_time));
				if(date("H", $pre_adjusted_turnaround_time) < 8){
					$date->setTime(10, 0 ,0);
					$turnaround_time = $date->format("Y-m-d H:i:s");
				}
				else if(date("H", $pre_adjusted_turnaround_time) == 23){
					$date->add(new DateInterval('P1D'));
					$date->setTime(10, 0 ,0);
					$turnaround_time = $date->format("Y-m-d H:i:s");
				}
				else {
					$turnaround_time = date('Y-m-d H:i:s', $pre_adjusted_turnaround_time);
				}
			}
			
			$this->Retailer->query("INSERT INTO complaints 
					(vendor_activation_id,takenby,in_date,in_time,turnaround_time) 
					VALUES (".$id.",'".$_SESSION['Auth']['User']['id']."','".date('Y-m-d')."','".date('H:i:s')."', '".$turnaround_time."')");
			
			//$this->Retailer->query("UPDATE complaints SET in_date='".date('Y-m-d')."',in_time='".date('H:i:s')."',resolve_flag = 0 WHERE vendor_activation_id = $id");
			
			echo "success";
			$this->General->sendMails("Transaction reopened","Transaction id: " . $data1['0']['vendors_activations']['ref_code'],array('chirutha@mindsarray.com'),'mail');
		}
		else {
			echo "failure";
		}
		$this->autoRender = false;
	}
	
	function prodVendor($pid = null){
		if(isset($pid)){
			$vcId = $_REQUEST['vendor'.$pid];
			if(isset($vcId)){
				$this->Retailer->query("update vendors_commissions set active='0' where product_id = '".$pid."'");
				$this->Retailer->query("update vendors_commissions set active='1' where id = '".$vcId."'");
				$this->Shop->setProdInfo($pid);
			}
		}
		$this->set('prods',$this->Slaves->query("SELECT products.id, products.name, products.oprDown,products.blocked_slabs,products.auto_check,products.service_id,slabs_products.percent FROM products,slabs_products WHERE products.id = slabs_products.product_id AND slab_id = 13 AND to_show=1 "));
	        $this->set('comm',$this->Slaves->query("SELECT vendors_commissions.*,vendors.company,vendors.shortForm ,users.name FROM vendors_commissions join vendors on (vendors_commissions.vendor_id = vendors.id AND vendors.show_flag = 1) left join users on (vendors_commissions.updated_by = users.id) where vendors_commissions.is_deleted=0"));
                $this->set('vendors',$this->Slaves->query("SELECT vendors.id,vendors.shortForm,vendors.active_flag FROM vendors WHERE show_flag = 1"));
        $this->set('slabs',$this->Slaves->query("SELECT * FROM `slabs` WHERE `active_flag` = 1 ;"));
        //print_r($this->Retailer->query("SELECT * FROM `slabs` WHERE `to_show` = 1 ;"));
	}
	
	
	function disableVendor(){
		$pid = $_REQUEST['pid'];
		$flag = $_REQUEST['flag'];
		$status = 'fail';
		if(isset($pid)){
			if($flag == 0)$flag = 2;
			else if($flag >= 1)$flag = 0;
			
			$this->Retailer->query("UPDATE vendors_commissions SET oprDown = $flag , updated_by = ".$this->Session->read('Auth.User.id')." WHERE id = $pid");
			$this->Shop->setProdInfo($_REQUEST['product']);
			/*$this->Shop->delMemcache("disabled_".$_REQUEST['vendor']."_".$_REQUEST['product']);
			if($flag == 0){
				$key = "health_".$_REQUEST['vendor']."_".$_REQUEST['product'];
				$this->Shop->setMemcache($key,0,30*60);
			}*/
			$status = 'success';
		}
		echo $status;
		$this->autoRender = false;
	}
    function blockSlab(){
        $slab_id = $_REQUEST['slab_id'];
		$prod_id = $_REQUEST['prod_id'];
		$slab_status = $_REQUEST['status'];
        $status = 'fail';
        $flag = $slab_status;
		$key = "slab_".$slab_id."_".$prod_id;
        
        //$result = $this->Retailer->query("SELECT blocked_slabs from products WHERE id = $prod_id");
        //$value = $result[0]["products"]["blocked_slabs"];
        $value = $this->Shop->getProdInfo($prod_id);
        $valueArr = $value["blocked_slabs"];
        
        //$valueArr = explode(",", $value);
        if($flag == 1){
            $keyArr = array_search($slab_id,$valueArr);            
            if ($keyArr !== NULL) {
                array_push($valueArr, $slab_id);
            }
        }else{
            $keyArr = array_search($slab_id, $valueArr);
            if ($keyArr !== NULL) 
            {   
                unset($valueArr[$keyArr]);
            }
        }
        foreach($valueArr as $key=>$val){
            if(empty($val)){
                unset ($valueArr[$key]);
            }
        }
        $valueNew = join(",", $valueArr);
        $this->Retailer->query("UPDATE products SET blocked_slabs = '$valueNew'  WHERE id = $prod_id");//, updated_by = ".$this->Session->read('Auth.User.id')."
        $this->Shop->setProdInfo($prod_id);
        $status = 'success';
		echo $status;
		$this->autoRender = false;
	}
	
	function reports(){
		
	}
	
	function refreshCache(){
		
                $data = $this->Slaves->query("SELECT name FROM vars");
                foreach($data as $dt){
                        $this->Shop->delMemcache($dt['vars']['name']);
                }

                $vendors = $this->Shop->getVendors();
                foreach($vendors as $vd){
                        $this->Shop->setVendorInfo($vd['vendors']['id']);
                }
                echo 'success';
		
		
		$this->autoRender = false;
	}
	
	function deactivateVendor(){
		$pid = $_REQUEST['pid'];
		$flag = $_REQUEST['flag'];
		$status = 'fail';
		if(isset($pid)){
			$data = $this->Shop->getVendorInfo($pid);
			$user = $this->Session->read('Auth.User.name');
			$vendor = $data['shortForm'];
			
			if($flag == 0 || $flag == 2){
				$flag = 1;	
				$subject = "Manually enabled $vendor from Provider Switching";
				$body = "User: $user";
			}
			else if($flag == 1){
				$flag = 2;
				$subject = "Manually disabled $vendor from Provider Switching";
				$body = "User: $user";
			}
			$this->General->sendMails($subject,$body,array('backend@mindsarray.com'),'mail');
			
			$data1 = array();
			$data1['active_flag'] = $flag;
			$data1['health_factor'] = 0;
			$this->Shop->setVendorInfo($pid,$data1);
			$this->Shop->setInactiveVendors();
			$status = 'success';
		}
		echo $status;
		$this->autoRender = false;
	}

	function oprEnable(){
		$pid = $_REQUEST['pid'];
		$flag = $_REQUEST['flag'];
		$status = 'fail';
		if(isset($pid)){
			$msg = $_REQUEST['msg'];
			$qp = " , down_note = '".addslashes($msg)."' ";
			
			$this->Retailer->query("update products set oprDown=".$flag." $qp where id = ".$pid);
			$status = 'success';
			$opr = $this->Shop->setProdInfo($pid);
                        //sending Message
                        //$this->General->sendMessage($retMobile,$msg,'notify');
                        //$userid = $this->Session->read('Auth.User.id');
                        
			if($flag == '1'){	
                            $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".'log statrted for flag 1');
                                $data = array('user'=>$this->Session->read('Auth.User.id'),'type'=>'product disabled', 'sms'=>$opr['service_name'].": ".$opr['name']." product disabled");
                                $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".json_encode($data));
                                $this->General->curl_post("http://inv.pay1.in/alertsystem/alertreport/addInAlert",$data,'POST');
				$this->General->sendMails($opr['service_name'].": ".$opr['name']." product disabled",$opr['service_name'].": ".$opr['name']." product disabled",array('backend@mindsarray.com','rm@mindsarray.com'),'mail');
			}else if($flag == '0'){
                            $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".'log statrted for flag 1');
                                $data = array('user'=>$this->Session->read('Auth.User.id'),'type'=>'product enabled', 'sms'=>$opr['service_name'].": ".$opr['name']." product enabled");
                                 $this->General->logData("/mnt/logs/alert.txt",date('Y-m-d H:i:s')." :: ".json_encode($data));
                                $this->General->curl_post("http://inv.pay1.in/alertsystem/alertreport/addInAlert",$data,'POST');
				$this->General->sendMails($opr['service_name'].": ".$opr['name']." product enabled",$opr['service_name'].": ".$opr['name']." product enabled",array('backend@mindsarray.com','rm@mindsarray.com'),'mail');
			}
		}
		echo $status;
		$this->autoRender = false;
	}
        
        function manageOpr($flag,$operator,$comments){
            $date = date('Y-m-d');
            if($flag==1)
                $flag=0;
            elseif($flag==0)
                $flag=1;
            $this->Report->query("Insert into oprator_managment (flag,operator_id,coments,date) VALUES ($flag,$operator,'".addslashes($comments)."','$date')");
        }
	
	function tranRange($frm=null,$to=null,$vendorIds=null,$operatorIds=null,$page=null,$transType="all",$lim=1000,$f_time=null,$t_time=null){
            
            if ($frm == null && $to == null) {
			$frm = date('d-m-Y');
			$to = date('d-m-Y');
                }        
                        $fdarr = explode("-", $frm);
			$tdarr = explode("-", $to);
			$fd = $fdarr[2] . "-" . $fdarr[1] . "-" . $fdarr[0];
			$ft = $tdarr[2] . "-" . $tdarr[1] . "-" . $tdarr[0];
		
                if(($f_time== null || $t_time== null) || ($f_time== 0 && $t_time== 0)){
                    $f_time = 0;
                    $t_time = 0;
                    $VAtimeCondition = '';
                    $ShopTranstimeCondition = '';
                }else{
                    $frm_time = $fd.' '.str_replace('.', ':', $f_time).':00';
		    $to_time = $ft.' '.str_replace('.', ':', $t_time).':00'; 
                    $VAtimeCondition = "AND va.timestamp >= '{$frm_time}' and va.timestamp <= '{$to_time}'";
                    $ShopTranstimeCondition = "AND st1.timestamp >= '{$frm_time}' and st1.timestamp <= '{$to_time}'";
                }
                
		if((!empty($vendorIds)) && ($vendorIds !== null)){
                    $vndStr = " and va.vendor_id IN ($vendorIds) ";
                    $vendorIds = explode(',', $vendorIds);
                } else{
                    $vndStr = '';
                }
                
                if(!empty($operatorIds) && ($operatorIds !== null)){
                    $opStr = " and va.product_id IN ($operatorIds) ";
                    $operatorIds = explode(',', $operatorIds);
                } else{
                    $opStr = '';
                }
			
		$page = empty($_GET['res_type']) ? $page : $_GET['res_type'];
		$success1 = array();
		
		$transactionType = "Transactions";
		if(empty($page))$page = 1;
		$limit = $lim*($page-1).",$lim";
		$limit = $page == "csv" ? "": "limit ".$limit;
		
// 		$getMobileNumberingDetails = $this->Slaves->query("select number,area from mobile_numbering");
		$getMobileNumberingDetails = $this->Slaves->query("select number, area from mobile_operator_area_map");
		foreach ($getMobileNumberingDetails as $val){
			$mobArea[$val['mobile_operator_area_map']['number']] = $val['mobile_operator_area_map']['area'];
		}
		$vendorDDResult=$this->Slaves->query("select id,company from vendors order by company asc");
		$this->set('vendorDDResult',$vendorDDResult);
		$opResult=$this->Slaves->query("select products.id,products.name,products.service_id , services.name from products LEFT JOIN services ON products.service_id = services.id  where products.service_id != 0  order by  services.name , products.name ");
		$opResultTw = array();
                foreach($opResult as $tr)
                {
                        $opResultTw[$tr['products']['service_id']]["products"][]=$tr['products'];  
                        $opResultTw[$tr['products']['service_id']]["service_name"]=$tr['services']['name'];  
                }
                $this->set('opResult',$opResultTw);

		if($transType == "reverse"){
			$success=$this->Slaves->query("SELECT users.name,v.company,v.shortForm, d.id, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile,va.ref_code, va.amount, va.amount*va.discount_commission/100 as comm, va.status, va.prevStatus, va.cause ,va.timestamp ,va.vendor_refid, va.api_flag, vax.processing_time, MAX(vm.timestamp) as updated_time, group_concat(vm.response) as causes
			from vendors_activations va USE INDEX (idx_date) left join vendors_transactions as vax ON (vax.ref_id = va.ref_code AND vax.vendor_id = va.vendor_id) left join vendors_messages as vm ON (vm.shop_tran_id=va.ref_code AND va.vendor_id = vm.service_vendor_id) join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id)  left join users ON (users.id=va.complaintNo)
				left join distributors d on (r.parent_id = d.id) where va.date between '".$fd."' and '".$ft."' AND  va.status in ( 2,3) $VAtimeCondition $vndStr $opStr group by va.ref_code order by va.id desc $limit");
		}else if($transType == "success"){
			$success=$this->Slaves->query("SELECT users.name,v.company,v.shortForm, d.id, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile,va.ref_code, va.amount, va.amount*va.discount_commission/100 as comm, va.status, va.prevStatus, va.cause ,va.timestamp ,va.vendor_refid, va.api_flag, vax.processing_time, MAX(vm.timestamp) as updated_time, group_concat(vm.response) as causes
			from vendors_activations va USE INDEX (idx_date) left join vendors_transactions as vax ON (vax.ref_id = va.ref_code AND vax.vendor_id = va.vendor_id) left join vendors_messages as vm ON (vm.shop_tran_id=va.ref_code AND va.vendor_id = vm.service_vendor_id) join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id)  left join users ON (users.id=va.complaintNo)
				left join distributors d on (r.parent_id = d.id) where va.date between '".$fd."' and '".$ft."' AND  va.status not in ( 0,2,3) $VAtimeCondition $vndStr $opStr group by va.ref_code order by va.id desc $limit");
                }else{
			$success=$this->Slaves->query("SELECT users.name,v.company,v.shortForm, d.id, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile,va.ref_code, va.amount, va.amount*va.discount_commission/100 as comm, va.status, va.prevStatus, va.cause , va.timestamp ,va.vendor_refid, va.api_flag, vax.processing_time, MAX(vm.timestamp) as updated_time, group_concat(vm.response) as causes
			from vendors_activations va USE INDEX (idx_date) left join vendors_transactions as vax ON (vax.ref_id = va.ref_code AND vax.vendor_id = va.vendor_id) left join vendors_messages as vm ON (vm.shop_tran_id=va.ref_code AND va.vendor_id = vm.service_vendor_id) join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id) left join users ON (users.id=va.complaintNo)
				left join distributors d on (r.parent_id = d.id) where va.date between '".$fd."' and '".$ft."' $VAtimeCondition $vndStr $opStr group by va.ref_code order by va.id desc $limit");
                }
                
		$this->set('limitPerPage',$lim);
		if($page != "csv"){
			$this->set('vendorIds',$vendorIds);
			$this->set('operatorIds',$operatorIds);
			$this->set('success',$success);
                        $this->set('frm',$frm);
		        $this->set('to',$to);
			$this->set('f_time',$f_time);
			$this->set('t_time',$t_time);
			$this->set('page',$page);
			$this->set('transType',$transType);

		}else{
		    $this->set('page',$page);
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			//"v.company,v.shortForm, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile, va.ref_code, va.amount, va.status, va.timestamp"
		
			//---------------------------
		    $line = array('Row','TransId','VendorTransId','Distributor ID','Retailer Mobile', 'Shop', 'Vendor',  'Cust Mob','Operator','Circle','Amt','Comm','Status','Previous Status','Date','Processing Time','Updated Time','TypeStatus','Cause','Sub-Cause','CC','TxnBy');
			$csv->addRow($line);
					
		      $i=1;
			
				$dateArray = array();
				
				$vendorId = array();
				
				$oldreversalQuery = "SELECT st1.date,st2.* FROM `shop_transactions` as st1 left join `shop_transactions` as st2 ON (st1.ref2_id = st2.id) WHERE st2.type = 4 AND st2.date != st1.date AND st1.`type` = 11 AND st1.`date` >= '$fd' and st1.date<='$ft' $ShopTranstimeCondition ";
				
                                $oldreversalQuery = $this->Slaves->query($oldreversalQuery);


			   foreach ($oldreversalQuery as $val) {
					$vendorId[] = $val['st2']['id'];
					
					$dateArray[$val['st2']['date']] = $val['st2']['date'];
					
					$retailerId[$val['st2']['ref1_id']] = $val['st2']['ref1_id'];
				}
				$dateArray = implode("','", $dateArray);
				
				$vendorId = implode(',', $vendorId);
				
				$retailerId = implode(',', $retailerId);
				
				$query="SELECT
					     users.name,v.company,v.shortForm,d.id, r.name,r.shopname,r.id,r.mobile,p.name,va.mobile,va.ref_code, va.amount, va.amount*va.discount_commission/100 as comm, va.status, va.prevStatus, va.cause ,va.timestamp ,va.vendor_refid, va.api_flag, vax.processing_time, MAX(vm.timestamp) as updated_time, group_concat(vm.response) as causes
			         FROM
					 vendors_activations va  use index(idx_ret_date)
					 LEFT JOIN 
					      vendors_transactions as vax ON (vax.ref_id = va.ref_code AND vax.vendor_id = va.vendor_id)
					 LEFT jOIN 
					      vendors_messages as vm ON (vm.shop_tran_id=va.ref_code AND va.vendor_id = vm.service_vendor_id) 
					 LEFT JOIN 
					      retailers r on(va.retailer_id=r.id) 
                                         LEFT JOIN 
                                              distributors d on (r.parent_id = d.id)     
					 JOIN products p 
					      on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id) 
						  
					 LEFT JOIN
					     users ON (users.id=va.complaintNo)
					 WHERE 
					 
					 va.date IN('$dateArray') AND va.shop_transaction_id IN($vendorId) AND va.retailer_id IN ($retailerId) $vndStr $opStr group by va.ref_code";
		          
				$success1 = $this->Slaves->query($query);
				
					 
			
			$success = array_merge_recursive($success,$success1);

			foreach($success as $d){
					$type = "";
				if($d['va']['status'] == '2' || $d['va']['status'] == '3'){
						$type = "Failed";
				}else{
						$type = "Success";
					}


				$api_flag = $d['va']['api_flag'];
				$api_array = array('0'=>'sms','1'=>'old apps','2'=>'ussd','3'=>'android','4'=>'api partner','5'=>'java','7'=>'win7','8'=>'win8','9'=>'web');

				$retailerLink = strcmp($d['r']['name'],'')!=0 ? $d['r']['name'] : $d['r']['mobile'];
				$ps = '';
				if($d['va']['status'] == '0'){
					$ps = 'In Process';
				}else if($d['va']['status'] == '1'){
					$ps = 'Successful';
				}else if($d['va']['status'] == '2'){
					$ps = 'Failed';
				}else if($d['va']['status'] == '3'){
					$ps = 'Reversed';
				}else if($d['va']['status'] == '4'){
					$ps = 'Reversal In Process';
				}else if($d['va']['status'] == '5'){
					$ps = 'Reversal declined';
				}
				
				$ps_p = "";
				if($d['va']['prevStatus'] == '0'){
					$ps_p = 'In Process';
				}else if($d['va']['prevStatus'] == '1'){
					$ps_p = 'Successful';
				}else if($d['va']['prevStatus'] == '2'){
					$ps_p = 'Failed';
				}else if($d['va']['prevStatus'] == '3'){
					$ps_p = 'Reversed';
				}else if($d['va']['prevStatus'] == '4'){
					$ps_p = 'Reversal In Process';
				}else if($d['va']['prevStatus'] == '5'){
					$ps_p = 'Reversal declined';
				}
				
				if($d['vax']['processing_time']=='' || $d['vax']['processing_time']=='0000-00-00 00:00:00'){
					$processTime = $d[0]['updated_time'];
				} else {
					$processTime = $d['vax']['processing_time'];
				}

				$mobnum = substr($d['va']['mobile'],0,5);
				$sub_cause = explode(",",$d[0]['causes']);
				$sub_cause = end($sub_cause);
				$sub_cause = ($d['va']['status'] == 2 || $d['va']['status'] == 3) ? $sub_cause : "";
				
				$line = array($i,$d['va']['ref_code'],$d['va']['vendor_refid'],$d['d']['id'],$d['r']['mobile'], $d['r']['name'], $d['v']['shortForm'], $d['va']['mobile'],$d['p']['name'],$mobArea[$mobnum], $d['va']['amount'],round($d['0']['comm'],2),$ps,$ps_p,$d['va']['timestamp'],$processTime,$d[0]['updated_time'],$type,$d['va']['cause'],$sub_cause,$d['users']['name'],$api_array[$api_flag]);
				$csv->addRow($line);
				$i++;
			}
			
			echo $csv->render($transactionType."_".$frm."_".$to.".csv");
			}
		}


/*	
	function tags($tagname=null)
	{
	if(in_array($_SESSION['Auth']['User']['group_id'],array(ADMIN,CUSTCARE)) || $_SESSION['Auth']['User']['id'] == 1){
		$this->set('selectedTagName',$tagname);
		$tagNameResult=$this->User->query("select distinct name,id from taggings t order by name asc");
		$this->set('tagNames',$tagNameResult);
	 	if(!is_null($tagname))
	 		$tagsResult = $this->User->query("select va.amount,u.name,u.mobile,u.group_id,t.name,tu.transaction_id,tu.timestamp,r.shopname from taggings t join user_taggings tu on(t.id=tu.tagging_id) join users u on (u.id=tu.user_id) join retailers r on (r.user_id=u.id) left join vendors_activations va on (va.ref_code=tu.transaction_id) where t.name='".$tagname."' order by t.name asc ");
	 
		$this->set('tagsResult',$tagsResult);
	} else {
		$this->redirect('/');
	}
		
	}
*/	
    function reconsile($vendor=null,$date=null)
	{	
       $vendor_list =  $this->Slaves->query("select id, company from vendors where update_flag=1 and active_flag != 2");
       $this->set('vendor_list',$vendor_list);
       
       $operator_list = $this->Slaves->query("select id, name from products");
       $this->set('operator_list',$operator_list);
       
       if(!is_null($vendor)){           
           $reconsile_status_arr = array('va'=>array('0'=>'in-process','1'=>'success','2'=>'failure','3'=>'refund','4'=>'complain','5'=>'reversal-decline'),
                   'vt'=>array('0'=>'in-process','1'=>'success','2'=>'failure'));
           $va_qry = "SELECT concat(ref_code,'_',(CASE WHEN status in (1,4,5) THEN 1 ELSE (CASE WHEN status in (2,3) THEN 2 ELSE 0 END) END),'_',vendor_id) as cid,ref_code as id from vendors_activations where vendor_id='".$vendor."'";
           $vt_qry = "SELECT concat(ref_id,'_',status,'_',vendor_id) as cid, ref_id as id from vendors_transactions where vendor_id='".$vendor."' ";
           
           if(is_null($date) && trim($_REQUEST['rdate'])==""){
               $date = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));                              
           }else{
               $date_part = explode("-", $_REQUEST['rdate']);
               $date = date("Y-m-d",mktime(0, 0, 0, $date_part[1], $date_part[0], $date_part[2]));
           }
           $date_cond = " AND date='".$date."'";
           $va_qry .= $date_cond;
           $vt_qry .= $date_cond;
           $un_qry = "SELECT cid, id FROM (($va_qry) UNION ALL ($vt_qry))as t1 group by 1 having count(1) = 1 order by 1";
           $txn_list = array();
           $txn_list_str = "";
           $id_results= $this->Slaves->query($un_qry);
           foreach ($id_results as $k=>$v){
               if(!isset($txn_list[$v['t1']['id']])){
                    array_push($txn_list, $v['t1']['id']);
               }
           }
           $txn_list_str = implode(",", $txn_list);
           $result_qry = "SELECT * FROM ("
                                . "SELECT vt.sim_num as sim_num, va.product_id as operator,va.amount as amount,va.ref_code as txnid,"
                                    . "(CASE WHEN va.vendor_id = vt.vendor_id THEN va.status ELSE 2 END )as ser_status,"
                                    . "vt.status as vend_status,va.timestamp,vt.resolve_flag,vt.comment "
                                    . "FROM vendors_transactions as vt LEFT JOIN  vendors_activations as va  on va.ref_code = vt.ref_id  "
                                    . "WHERE vt.vendor_id='".$vendor."' and vt.ref_id in (".$txn_list_str.") and va.date='".$date."')as t "
                                . "where ser_status != vend_status";
           $result_data = $this->Slaves->query($result_qry);
           $sims_details = $this->get_reconsile_opn_clg($date, $vendor);
           $sim_wise_sale_result = $this->get_sim_wise_sale($vendor,$date);
           $result_data1 = $this->reconsile_formater($result_data);
           $sim_reports = $this->get_sorted_sim_wise_data($sim_wise_sale_result, $sims_details,$operator_list);
           $this->set('sim_reports',$sim_reports);
           $this->set('sims_details',$sims_details);
           $this->set('data_Result',$result_data);
           $this->set('data_Result1',$result_data1);
           $this->set('status_arr',$reconsile_status_arr);
           $this->set('vendor_id',$vendor);
           $this->set('opng_clg_diff',  $this->get_sim_with_diff_without_sale($sims_details, $operator_list));
           $this->set('sim_wise_sale',$sim_wise_sale_result);
       }   		
	}
    
    function get_sim_with_diff_without_sale($open_closing = array(),$operator_list = array()){
        $diff_array = array();
        $opr_list = array();    
        foreach($operator_list as $k=>$v){
             $opr_list[$v['products']['id']] = $v['products']['name'];
        }
        foreach($open_closing as $k=>$v){
            foreach ($v as $k1=>$v1){
                if(($v1['closing'] - $v1['opening']) > 0 && ( $v1['sale'] < 1 || is_null($v1['sale']) ) ){
                    array_push($diff_array, $k."_".$opr_list[$k1]);
                }
            }
        }     
        return $diff_array;
    }
            
    function get_sorted_sim_wise_data($sim_wise_sale,$sims_details,$operator_list = array()){
        $final_result = array();$opr_list = array();    
        foreach($operator_list as $k=>$v){
             $opr_list[$v['products']['id']] = $v['products']['name'];
        }
        $i = 0;
        foreach($sim_wise_sale as $k=>$v){
            if(isset($sims_details[$v['t']['sim_num']])){
                array_push($server_sim_num,$v['t']['sim_num']);
                $modem_diff = $sims_details[$v['t']['sim_num']][$v['t']['opr']]['sale'] +  $sims_details[$v['t']['sim_num']][$v['t']['opr']]['closing'] - $sims_details[$v['t']['sim_num']][$v['t']['opr']]['opening']- $sims_details[$v['t']['sim_num']][$v['t']['opr']]['diff'] - $sims_details[$v['t']['sim_num']][$v['t']['opr']]['inc'] ;
                $server_diff = floatval($sims_details[$v['t']['sim_num']][$v['t']['opr']]['sale']) - floatval($v['t']['sale']);
                $incomming = $sims_details[$v['t']['sim_num']][$v['t']['opr']]['diff'];
                $vendor = $sims_details[$v['t']['sim_num']][$v['t']['opr']]['vendor'];
                $approx_sale = $incomming + $modem_diff - $server_diff;
            }
            $final_result[$i]['operator'] = ((strlen(trim($v['t']['opr']))>0 && isset($opr_list[$v['t']['opr']]))?$opr_list[$v['t']['opr']]:'N.A');
            $final_result[$i]['operator_id'] = ((strlen(trim($v['t']['opr']))>0)?$v['t']['opr']:'N.A');
            $final_result[$i]['vendor'] = (isset($vendor)?$vendor:'N.A');
            $final_result[$i]['sim_num'] = $v['t']['sim_num'];
            $final_result[$i]['opening'] = (isset($sims_details[$v['t']['sim_num']][$v['t']['opr']]['opening'])?$sims_details[$v['t']['sim_num']][$v['t']['opr']]['opening']:'N.A');
            $final_result[$i]['closing'] = (isset($sims_details[$v['t']['sim_num']][$v['t']['opr']]['closing'])?$sims_details[$v['t']['sim_num']][$v['t']['opr']]['closing']:'N.A');
            $final_result[$i]['incomming'] = (isset($incomming)?$incomming:'N.A');
            $final_result[$i]['approx_sale'] = (isset($approx_sale)?$approx_sale:'N.A');
            $final_result[$i]['modem_sale'] = (isset($sims_details[$v['t']['sim_num']][$v['t']['opr']])?$sims_details[$v['t']['sim_num']][$v['t']['opr']]['sale']:'N.A');
            $final_result[$i]['server_sale'] = $v['t']['sale']; 
            $final_result[$i]['modem_diff'] = (isset($sims_details[$v['t']['sim_num']][$v['t']['opr']])?$modem_diff:'N.A');
            $final_result[$i]['server_diff'] = (isset($sims_details[$v['t']['sim_num']][$v['t']['opr']])?$server_diff:'N.A');            
            $i++;
        }
        asort($final_result);
        return $final_result;
    }
    
    function get_reconsile_opn_clg($date,$vendor){
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();
        $result = $this->format_sim_details($obj->modemBalance($date, $vendor));
        return $result;
    }
    
    function format_sim_details($data){
        $finalresult = array();
        foreach ($data as $k=>$v){
            if(in_array(trim($k),array('lasttime','ports'))){
                continue;
            }
            if(isset($finalresult[$v['mobile']][$v['opr_id']])){
                $finalresult[$v['mobile']][$v['opr_id']]['opening'] += $v['opening'];
                $finalresult[$v['mobile']][$v['opr_id']]['closing'] += $v['closing'];
                $finalresult[$v['mobile']][$v['opr_id']]['diff'] += $v['diff'];
                $finalresult[$v['mobile']][$v['opr_id']]['sale'] += $v['sale'];
            }else{
                $finalresult[$v['mobile']][$v['opr_id']] = array('opening'=>$v['opening'],'closing'=>$v['closing'],'diff'=>$v['tfr'],'sale'=>$v['sale'],'vendor'=>$v['vendor'],'inc'=>$v['inc']);
            }
            unset($data[$k]);
        }
        return $finalresult;
    }
            
    function get_sim_wise_sale($vendor,$date){
        $sim_wise_sale_qry = "SELECT * FROM ("
                                . "SELECT vt.sim_num, (CASE WHEN va.product_id in ('10','27') THEN 9 ELSE "
                                    . "(CASE WHEN va.product_id='29' THEN 11 ELSE "
                                    . "(CASE WHEN va.product_id='28' THEN 12 ELSE "
                                    . "(CASE WHEN va.product_id='31' THEN 30 ELSE "
                                    . "(CASE WHEN va.product_id='34' THEN 3 ELSE "
                                    . "(CASE WHEN va.product_id='7' THEN 8 ELSE va.product_id END ) END) END) END)"
                                    . " END) END ) as opr,  sum(va.amount) as sale "
                                    . "FROM `vendors_transactions` as vt "
                                        . "INNER JOIN  `vendors_activations` as va "
                                        . "ON vt.ref_id=va.ref_code AND vt.date=va.date AND vt.vendor_id=va.vendor_id "
                                        . "WHERE vt.vendor_id='".$vendor."' AND vt.sim_num is not null "
                                        . "AND vt.sim_num !='' and vt.sim_num !='0' and va.status not in (2,3) and va.date ='".$date."' group by 1 ,2 )t order by 2";
        $sim_wise_sale_result = $this->Slaves->query($sim_wise_sale_qry);
        return $sim_wise_sale_result;
    }
    
            
    function reconsile_formater($data){
        $consolidate_arr = array();
        $combine_mapper = array(10=>9,27=>9,29=>11,28=>12,31=>30,34=>3);
        foreach($data as $d){
            if(!isset($consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['total'])){
                $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['total'] = 0;
                $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['count'] = 0;
            }
            $d['t']['operator'] = in_array($d['t']['operator'],array_keys($combine_mapper))?$combine_mapper[$d['t']['operator']]:$d['t']['operator'];
            $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['total'] += $d['t']['amount'];
            $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['count'] += 1;
            $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['operator'] = $d['t']['operator'];
            $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['sim_num'] = $d['t']['sim_num'];
            if($consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['sim_num'] == $d['t']['sim_num'] && 
               $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['operator'] == $d['t']['operator']){
               $consolidate_arr[$d['t']['sim_num']][$d['t']['operator']]['data'][] = $d;                
            }
        }        
        return $consolidate_arr;
    }
            
    function update_reconsile(){
        $this->layout = null;
        $this->autoLayout = false;        
        $qry = "";
        if(isset($_REQUEST['r_flag'])){
            $update_qry = "UPDATE vendors_transactions SET resolve_flag='".$_REQUEST['r_flag']."' "
                                . "WHERE ref_id='".$_REQUEST['id']."' AND vendor_id='".$_REQUEST['r_vendor']."'";
        }
        if(isset($_REQUEST['r_comment'])){
            $update_qry = "UPDATE vendors_transactions SET comment='".$_REQUEST['r_comment']."' "
                                . "WHERE ref_id='".$_REQUEST['id']."' AND vendor_id='".$_REQUEST['r_vendor']."'";
        }
        if(strlen(trim($update_qry)) > 0){
            $result_data = $this->User->query($update_qry);
        }
        $this->autoRender = false;
    }
/*
    function tagTransactionNew($flag = null){
    	$date = date("Y-m-d");
    	if($flag == 3){
    		$tId = $_REQUEST['tagId'];
    		$tagFor = $_REQUEST['tagFor'];	
    		$retailerMobile = $_REQUEST['retMobile'];
    		$retailerUserIdResult = $this->User->query("select id from users where mobile='".$retailerMobile."' ");
    		$retailerUserId=$retailerUserIdResult['0']['users']['id'];
    		$this->User->query("insert into user_taggings (tagging_id,user_id,transaction_id,timestamp) value ($tId,$retailerUserId,'$tagFor','$date') ");
    		$this->autoRender = false;
    	}    	
    }
    
	function tagTransaction($flag=null)
	{
		$date=date("Y-m-d");
		if($flag==3) // flag=3 => call from tagReversals() from transaction.ctp
		{
			$tName=$_REQUEST['tagName'];
			$date=date("Y-m-d");
			$definedTaggingResult=$this->User->query("select id from taggings t where name like '".$tName."' ");
			$tId=$definedTaggingResult['0']['t']['id'];
			echo "DD id is ".$tId;
				
			if(empty($tId))
			{
				//$this->User->query("insert into taggings (name) value ('".$tName."') ");
				//	echo "id of last inserted record= ".mysql_insert_id();
				$tId=mysql_insert_id();

			}
				
			$tagFor=$_REQUEST['tagFor'];
			$retailerMobile=$_REQUEST['retMobile'];
			echo "retailer mobile is ".$retailerMobile;
			$retailerUserIdResult=$this->User->query("select id from users where mobile='".$retailerMobile."' ");
			$retailerUserId=$retailerUserIdResult['0']['users']['id'];
			echo "retailer User id is".$retailerUserId;
			echo "date is  ".$date;
			$this->User->query("insert into user_taggings (tagging_id,user_id,transaction_id,timestamp) value ($tId,$retailerUserId,'$tagFor','$date') ");
			$this->autoRender=false;
			exit;
				
		}

		$testname=$_REQUEST['tagName'];

		preg_match_all("/#/",$testname , $matches);
		$chk1 = trim(preg_replace("/#/", "",$testname));
		$array=explode(" ",$chk1);
		$i=0;
		//		print_r($array);

		foreach($array as $a=>$v)
		{
			$tagName=$v;
			//$tagName=array_pop($array);
			//	echo "1. ".$tagName;
			//		echo "</br>";

			$date=date("Y-m-d");
			$tagFor=$_REQUEST['tagFor'];
			//	echo "tag for ".$tagFor;
			//search if tag exist or no
			$tagSearchResult=$this->User->query("select name,id from taggings where name like '".$tagName."' ");
			$tagName1=$tagSearchResult['0']['taggings']['name'];
			//	$tagId=$tagSearchResult['0']['taggings']['id'];
			//	echo "tagName1".$tagName1;
			if(empty($tagName1))
			{
				echo "EMPTY";
				//$this->User->query("insert into taggings (name) value ('".$tagName."') ");
				//	echo "id of last inserted record= ".mysql_insert_id();
				$tagId=mysql_insert_id();
			}

			//echo "tagName isss".$tagName;
			else
			{
				$tagssResult=$this->User->query("select name,id from taggings where name like '".$tagName."' ");
				//print_r($tagssResult);
				//$tagname=$tagssResult['0']['taggings']['name'];
				$tagId=$tagssResult['0']['taggings']['id'];
				echo "New tag name is ".$tagName;
				echo "Tag id is ".$tagId;
			}
			echo "flag is ".$flag;
			if($flag==1)  // flag=1 => tagging from transaction.ctp;flag=0 => tagging from userinfo.ctp; flag=2 =>retInfo.ctp
			{
				$retailerMobile=$_REQUEST['retMobile'];
				echo "retailer mobile is ".$retailerMobile;
				$retailerUserIdResult=$this->User->query("select id from users where mobile='".$retailerMobile."' ");
				$retailerUserId=$retailerUserIdResult['0']['users']['id'];
				echo "retailer User id is".$retailerUserId;
				$this->User->query("insert into user_taggings (tagging_id,user_id,transaction_id,timestamp) value ($tagId,$retailerUserId,'$tagFor','$date') ");
			}
			else if($flag==0 || $flag==2 )
			$this->User->query("insert into user_taggings (tagging_id,user_id,timestamp) value ($tagId,'$tagFor','$date') ");
		}
		$this->autorender=false;

	}
	*/
	
	function pullback(){
		$transId=$_REQUEST['id'];
		$result=$this->User->query("SELECT vendors_activations.*,vendors.shortForm,trans_vendor.shortForm,trans_vendor.update_flag, "
                . "vendors.update_flag, trans_pullback.vendor_id, retailers.mobile,products.service_id, partners_log.id,partners_log.partner_req_id "
                . "FROM vendors_activations left join retailers ON (retailer_id = retailers.id) "
                . "LEFT JOIN products ON (products.id = product_id) "
                . "LEFT JOIN partners_log ON (vendors_activations.ref_code = partners_log.vendor_actv_id) "
                . "LEFT JOIN vendors ON (vendors_activations.vendor_id = vendors.id) "
                . "LEFT JOIN `trans_pullback` ON (vendors_activations.id = trans_pullback.vendors_activations_id) "
                . "LEFT JOIN vendors as trans_vendor ON (trans_pullback.vendor_id = trans_vendor.id) "
                . "WHERE vendors_activations.id=$transId AND (vendors_activations.status =2 OR vendors_activations.status = 3)");
	
		if(!empty($result)){
            
			if($result['0']['vendors_activations']['retailer_id'] == B2C_RETAILER && !empty($result['0']['partners_log']['id'])){//b2c partner
				$ref_code =  "2082" . sprintf('%06d', $result['0']['partners_log']['id']);
				$out = $this->General->b2c_pullback($result['0']['partners_log']['partner_req_id'],$ref_code);
				if($out['status'] == 'failure'){
					$desc = $out['description'];
					if(empty($desc)) $desc = 'Amount is less in user account';
					$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','14','Cannot be pulled back by ".$_SESSION['Auth']['User']['name'].", '.$desc,'failure','".date("Y-m-d H:i:s")."')");
					echo "Cannot be pulled back";
					return;
				}
			}elseif($this->Session->read('Auth.User.group_id') !=ADMIN){
                
                $_SERVER['DOCUMENT_ROOT'] = "/var/www/html/shops/app/webroot";        
                App::import('Controller', 'Recharges');
                $obj = new RechargesController;
                $obj->constructClasses();
                
                $trans_refcode = $result['0']['vendors_activations']['ref_code'];
                $transdate = $result['0']['vendors_activations']['date'];
                $transvrefId = $result['0']['vendors_activations']['vendor_refid'];
                $transvendorname = !empty($result['0']['trans_vendor']['shortForm']) ? trim($result['0']['trans_vendor']['shortForm']) : trim($result['0']['vendors']['shortForm']);
                
                ob_start(); //to supress the printable output of below function call
                $tranResponse = $obj->tranStatus($trans_refcode,$transvendorname,$transdate,$transvrefId,1);
                ob_end_clean();
                $vendor_type = !empty($result['0']['trans_vendor']['shortForm']) ? trim($result['0']['trans_vendor']['update_flag']) : trim($result['0']['vendors']['update_flag']);
                
                if($vendor_type == 0 ){//for API vendors and non admin user
                    $tranResponse['status'] = isset($tranResponse['status']) ? strtolower($tranResponse['status']) :"";
                    if(isset($tranResponse['status']) && strtolower(trim($tranResponse['status']) != "success")){
                        echo "Cannot be pulled back as this transaction (Only success transactions are allowed) ";
                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','14','Cannot be pulled back by ".$_SESSION['Auth']['User']['name']." as transaction with status (success) is allowed','failure','".date("Y-m-d H:i:s")."')");					
                        return;
                    }
                }else{// constrain for modem vendor and non admin user
                    $tranResponse_arr = json_decode($tranResponse,true);                    
                    if(isset($tranResponse_arr['status']) && !(in_array(strtolower(trim($tranResponse_arr['status'])),array("pending","success")))){
                        echo "Cannot be pulled back as this transaction (Only success & pending transactions are allowed) ";
                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','14','Cannot be pulled back by ".$_SESSION['Auth']['User']['name']." as transaction with status (pending, success) are allowed','failure','".date("Y-m-d H:i:s")."')");					
                        return;
                    }
                }
            }
			if(time() - strtotime($result['0']['vendors_activations']['timestamp']) > 12*60*60 && $this->Session->read('Auth.User.group_id') !=BACKEND_ADMIN){
				echo "Cannot be pulled back as this transaction is older than 12 hours";
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','14','Cannot be pulled back by ".$_SESSION['Auth']['User']['name']." as the transaction is older than 12 hours','failure','".date("Y-m-d H:i:s")."')");					
				return;
			}
			$this->User->query("START TRANSACTION");
			$shop_id = $result['0']['vendors_activations']['shop_transaction_id'];
			$mobile = $result['0']['vendors_activations']['mobile'];
			$param = $result['0']['vendors_activations']['param'];
			$amount = $result['0']['vendors_activations']['amount'];
			$retMobile = $result['0']['retailers']['mobile'];
			$vendors_activations_id = $result[0]['vendors_activations']['id'];
			$vendorId = $result[0]['vendors_activations']['vendor_id'];
			$result1 = $this->User->query("SELECT ref1_id,amount FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
                        
                        //this function is used for MsgTemplate
                        $MsgTemplate = $this->General->LoadApiBalance();
			if(!empty($result1)){
				$checktrans = $this->User->query("SELECT id,vendor_id from trans_pullback where trans_pullback.vendors_activations_id = '".$vendors_activations_id."'");
				if(!empty($checktrans)){
					$vendorId = $checktrans[0]['trans_pullback']['vendor_id'];
				}
				
				$this->User->query("UPDATE vendors_activations SET vendor_id=$vendorId,prevStatus=status,status=1, complaintNo='".(empty($_SESSION['Auth']['User']['id'])? 0 :$_SESSION['Auth']['User']['id'])."' WHERE id=$transId");
				$this->User->query("UPDATE shop_transactions SET confirm_flag=1 WHERE id=$shop_id");
			    
				$this->User->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$vendorId."','13','Pulled back by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");
				$this->Shop->addStatus($result['0']['vendors_activations']['ref_code'],$vendorId);
				//$this->General->logData("/var/www/html/shops/status.txt",$result['0']['vendors_activations']['ref_code'] . "::pullback: "."INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','13','Pulled back by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");
				
				$this->User->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
				$amt = $result1['0']['shop_transactions']['amount'];
				$ret_id = $result1['0']['shop_transactions']['ref1_id'];
				
				
				$bal = $this->Shop->shopBalanceUpdate($amt,'subtract',$ret_id,RETAILER);
				$this->Shop->addOpeningClosing($ret_id,RETAILER,$shop_id,$bal+$amt,$bal);
				if(empty($checktrans)){
					$this->User->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$vendors_activations_id."','".$vendorId."','1','".date('Y-m-d H:i:s')."','".$_SESSION['Auth']['User']['id']."','".date('Y-m-d H:i:s')."','Non-system','".date('Y-m-d')."')");
				}else {
					$this->User->query("UPDATE trans_pullback SET pullback_by='".(empty($_SESSION['Auth']['User']['id'])? 0 :$_SESSION['Auth']['User']['id'])."',pullback_time='".date('Y-m-d H:i:s')."' WHERE vendors_activations_id=$vendors_activations_id");
				}

				$this->User->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
				$this->Shop->unlockReverseTransaction($shop_id);
				
				if($this->User->query("COMMIT")){
					if($result['0']['vendors_activations']['date'] <= date('Y-m-d',strtotime('-1 days'))){
						$this->Shop->updateDeviceData($result['0']['vendors_activations']['ref_code'],$result['0']['vendors_activations']['vendor_id'],$result['0']['vendors_activations']['product_id'],$result['0']['vendors_activations']['amount'],false);
					}
//					$msg = "Pulled back amount of Rs.".$amt." from your account.\n";
			
//					$msg .="Trans Id: ".substr($transId,-5)."\n";
					if(empty($param))
					{
//						$msg .="Mobile: $mobile\n";
                                                $paramdata['PULLBACKTO'] ="Mobile: $mobile\n";
					}
					else {
//						$msg .="Subscriber Id: $param\n";
                                                $paramdata['PULLBACKTO'] ="Subscriber Id: $param\n";
					}
//                                      $msg = "Pulled back amount of Rs.".$amt." from your account.\n";
//					$msg .="Trans Id: ".substr($transId,-5)."\n";
//					$msg .="Amount: $amount\nYour current balance is Rs.".$bal;
                                        
                                        $paramdata['PULLED_AMOUNT'] = $amt;
                                        $paramdata['TRANSID'] = substr($transId,-5);
                                        $paramdata['AMOUNT'] = $amount;
                                        $paramdata['BALANCE'] = $bal;
                                        $content =  $MsgTemplate['Panels_Pullback_MSG'];
                                        $msg = $this->General->ReplaceMultiWord($paramdata,$content);

					$this->General->sendMessage($retMobile,$msg,'notify');
					echo "success";
				}
				else {
					$this->User->query("ROLLBACK");
					echo "Cannot be pulled back";
				}
			}
			else {
				$this->User->query("ROLLBACK");
				echo "Cannot be pulled back";
			}
		}
		else {
			echo "Cannot be pulled back";
		}
		$this->autoRender = false;
	}
     
	function addComment() {
	 	$loggedInUser = $_SESSION['Auth']['User']['mobile'];
		$userMobile = empty($_REQUEST['userMobile']) ? "" : $_REQUEST['userMobile'];
		$test = empty($_REQUEST['text']) ? "" : $_REQUEST['text'];
		$retId = empty($_REQUEST['retId']) ? "" : $_REQUEST['retId'];
        $transId = empty($_REQUEST['transId']) ? "" : $_REQUEST['transId'] ;
		$callTypeId = $_REQUEST['callTypeId'];
		$tagId = $_REQUEST['tagId'];
		
		$callTypeId == 'none' && $callTypeId = null;
		$tagId == 'none' && $tagId = null;
		
		if(empty($test)) exit;
		$userId = 0;
		if(!empty($userMobile)){
			$usersResult=$this->Slaves->query("select id,name,mobile from users where mobile='$userMobile'");
			$userId = empty($usersResult) ? 0 : $usersResult['0']['users']['id'];
		}
		$this->Shop->addComment($userId,$retId,$transId,$test,$loggedInUser, null, $tagId, $callTypeId);
		if($transId){
			$comment = $this->User->query("SELECT c.comments,c.ref_code,u.name,u.mobile,c.created
						from comments c join users u on c.mobile = u.mobile
						where c.ref_code = '$transId'  order by c.created desc limit 1");
			if($comment){
				echo "<tr bgcolor='#CEF6F5' style='border: 2px solid white'>";
				echo "<td><span style='font-size:12px;'>By ".$comment[0]['u']['name']." @ ".$comment[0]['c']['created']." on ".$comment[0]['c']['ref_code']."</span></br>".$comment[0]['c']['comments']."</td>"; 
				echo "</tr>";
			}	
		}	
		$this->autoRender = false;
	}
	 
	 
	 function salesmanReport($distId=null,$salesmanId=null,$from=null,$to=null)
	 {
	 	
	 	if(!isset($from))
	 		$from=date('d-m-Y');
	 	if(!isset($to))
	 	    $to=date('d-m-Y');
	 	    
	 	if($distId == null)$distId = 1;    
	 	
	 	$fdarr = explode("-",$from);
	 	$tdarr = explode("-",$to);
	 	
	 	$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
	 	$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
	 	
	 	$this->set('from',$from);
	 	$this->set('to',$to);
		$this->set('distId',$distId);
	 	
		//for salesman dropdown
	 	$distResult=$this->Slaves->query("select Distributor.*,users.mobile from distributors as Distributor inner join users ON (users.id = Distributor.user_id) where Distributor.toshow = 1");
	 	$this->set('distributors',$distResult);
	 	
	 	//for salesman dropdown
	 	$salesmanResult=$this->Slaves->query("select s.id,s.name,s.mobile from salesmen s where s.dist_id = $distId AND s.active_flag = 1");
	 	$this->set('salesman',$salesmanResult);
	 	
	 	//for table in salesman Reports
	 	//	echo "Sales Mobile= ".$salesmanMobile;
	 	$this->set('salesmanId',$salesmanId);
	 	
	 	if($salesmanId!=0){
	 		$salesResult=$this->Slaves->query("select r.name,r.mobile,r.shopname,st.amount,sst.collection_amount,sst.created,sst.payment_type,sm.name from salesman_transactions sst USE INDEX (idx_collDate) join  salesmen sm on (sm.id=sst.salesman) join shop_transactions st on(sst.shop_tran_id=st.id) join retailers r on(r.id=st.ref2_id) where sm.id=$salesmanId AND sst.collection_date between '".$fd."' and '".$ft."'");
	 		$retAcquired=$this->Slaves->query("select count(retailers.mobile) as num, salesmen.name,salesmen.id from salesmen left join retailers on (retailers.salesman=salesmen.id)  where salesman=".$salesmanId);
	 		$retAcquiredDtRng=$this->Slaves->query("select count(retailers.mobile) as num, salesmen.name,salesmen.id from salesmen left join retailers on (retailers.salesman=salesmen.id) WHERE date(retailers.created) between '".$fd."' and '".$ft."' AND salesman=".$salesmanId);
	 		$setupDtRng=$this->Slaves->query("select sum(sst.collection_amount) as sc,salesman from salesman_transactions sst where sst.salesman=$salesmanId and sst.payment_type = 1 AND Date(sst.collection_date) between '".$fd."' and '".$ft."'");
	 		$setup=$this->Slaves->query("select sum(sst.collection_amount) as sc,salesman from salesman_transactions sst where sst.salesman=$salesmanId AND sst.payment_type = 1 ");
	 	}else{
	 		$salesResult=$this->Slaves->query("select r.name,r.mobile,r.shopname,st.amount,sst.collection_amount,sst.created,sst.payment_type,sm.name from salesman_transactions sst USE INDEX (idx_collDate) join  salesmen sm on (sm.id=sst.salesman) join shop_transactions st on(sst.shop_tran_id=st.id) join retailers r on(r.id=st.ref2_id) where r.parent_id=$distId and sst.collection_date between '".$fd."' and '".$ft."'");
	 		
	 		$retAcquiredDtRng=$this->Slaves->query("select count(retailers.mobile) as num, salesmen.name,salesmen.id from salesmen left join retailers on (retailers.salesman=salesmen.id AND Date(retailers.created) between '".$fd."' and '".$ft."') WHERE retailers.parent_id = $distId AND salesmen.active_flag = 1 group by salesmen.id");
	 		$retAcquired=$this->Slaves->query("select count(retailers.mobile) as num, salesmen.name,salesmen.id from salesmen left join retailers on (retailers.salesman=salesmen.id) WHERE retailers.parent_id = $distId AND salesmen.active_flag = 1 group by salesmen.id");
	 		$setupDtRng=$this->Slaves->query("select sum(sst.collection_amount) as sc,salesman from salesman_transactions sst inner join salesmen ON (salesmen.id = sst.salesman AND salesmen.dist_id=$distId) where Date(sst.collection_date) between '".$fd."' and '".$ft."' AND sst.payment_type = 1 group by sst.salesman");
	 		$setup=$this->Slaves->query("select sum(sst.collection_amount) as sc,salesman from salesman_transactions sst inner join salesmen ON (salesmen.id = sst.salesman AND salesmen.dist_id=$distId) WHERE sst.payment_type = 1 group by sst.salesman");
	 	}
	 	$this->set('salesResult',$salesResult);
	 	$this->set('retAcquiredDtRng',$retAcquiredDtRng);
	 	$this->set('retAcquired',$retAcquired);
	 	$setup_date = array();
	 	foreach($setupDtRng as $s){
	 		$id = $s['sst']['salesman'];
	 		$setup_date[$id] = $s['0']['sc'];
	 	}
	 	$setup_all = array();
	 	foreach($setup as $s){
	 		$id = $s['sst']['salesman'];
	 		$setup_all[$id] = $s['0']['sc'];
	 	}
	 	$this->set('setupDtRng',$setup_date);
	 	$this->set('setup',$setup_all);
	 }
	 
	 /*function salesmanTransaction()
	 {
	 	$salesmanMobile=$_REQUEST['salesmanMobile'];
	 		echo "Sales Mobile= ".$salesmanMobile;
	 		
	 	$this->set('salesmanMobile',$salesmanMobile);
	 	
	 	if($salesmanMobile!=0)
	 	{
	 	//	echo "Not empty";
	 	$salesResult=$this->Slaves->query("select r.name,r.mobile,sst.payment_type,st.amount,sst.payment_mode,sst.confirm_flag,sst.created from salesman_transactions sst join  salesmen sm on (sm.id=sst.salesman) join shop_transactions st on(sst.shop_tran_id=st.id) join retailers r on(r.id=st.ref1_id) where sm.mobile='$salesmanMobile' ");
	 	}
	 	
	 	else {
	 		//echo "Empty";
	 		$salesResult=$this->Slaves->query("select sm.*,r.name,r.mobile,sst.payment_type,st.amount,sst.payment_mode,sst.confirm_flag,sst.created from salesmen sm join  salesman_transactions sst on (sm.id=sst.salesman) join shop_transactions st on(sst.shop_tran_id=st.id) join retailers r on(r.id=st.ref1_id) ");
	 		
	 	}
	 	$this->set('salesResult',$salesResult);
	 		
	 	$this->autorender=false;
	 	
	 }*/
	 
	 
	 function tranDate($retMobile,$flag=null)
	 {
	 	$retailerIdResult=$this->Slaves->query("select id from retailers where mobile='".$retMobile."'");
	 	$retId=$retailerIdResult['0']['retailers']['id'];
	 	
	 	if(isset($_REQUEST['from'])){
	 		$frm=$_REQUEST['from'];
	 		$to=$_REQUEST['to'];
	 	}
	 	if(empty($frm))$frm = date('d-m-Y');
	 	if(empty($to))$to = date('d-m-Y');
	 	 	
	 	$fdarr = explode("-",$frm);
	 	$tdarr = explode("-",$to);
	 	
	 	$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
	 	$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
	 	
	 	if($flag==0)//search retailer transactions by date , not by status
	 	{
			$reversalInProcessResults=$this->Slaves->query("SELECT v.company,s.name,r.name,r.id,r.mobile,p.name,va.mobile, va.ref_code, va.amount, va.status, va.timestamp
	 				from vendors_activations va join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id) join services s on (p.service_id=s.id)
	 				where va.retailer_id=$retId and va.date between '".$fd."' and '".$ft."' order by va.id desc");
	 	}
	 	else //search retailer transaction having status = reversal in process.
	 		$reversalInProcessResults=$this->Slaves->query("SELECT v.company,s.name,r.name,r.id,r.mobile,p.name,va.mobile, va.ref_code, va.amount, va.status, va.timestamp
	 			from vendors_activations va join retailers r on(va.retailer_id=r.id) join products p on(p.id=va.product_id) join vendors v on(v.id=va.vendor_id) join services s on (p.service_id=s.id)
	 			where r.toshow = 1 AND va.status='".TRANS_REVERSE_PENDING."' and va.retailer_id=$retId and va.date between '".$fd."' and '".$ft."' order by va.id desc");
	 	$this->set('flag',$flag);
	 	$this->set('reversalInProcess',$reversalInProcessResults);
	 } 
	 
	/*function excelToPhp(){
      	set_time_limit(0);
		ini_set("memory_limit","-1");
		error_reporting(E_ALL);
 		ini_set("display_errors", 1);
		
 		App::import('Vendor', 'excel_reader2');
		$data = new Spreadsheet_Excel_Reader('export.xls', true);
		$i = 0;
		$users = array();
		//$this->Group->query("TRUNCATE log_opt");
		
		foreach($data->sheets[0]['cells'] as $user){
			if($i>5) break;
			$this->printArray($user);
			$i++;
		}
		
		//$this->printArray($mobiles);
		
		$this->autoRender = false;
	}*/
	
	/*function optin(){
		$data = $this->Retailer->query("SELECT distinct mobile FROM retailers where toshow=1");
		foreach($data as $dt){
			$this->General->makeOptIn247SMS($dt['retailers']['mobile']);
		}
		
		$data = $this->Retailer->query("SELECT distinct mobile FROM salesmen where active_flag=1");
		foreach($data as $dt){
			$this->General->makeOptIn247SMS($dt['salesmen']['mobile']);
		}
		$this->autoRender = false;
	}*/
	
	//steps to create new API vendor
        //1) create new retailer under dist id
        //2) add new partner in partners table //P023202 + id of table wit h some random password
        //3) Change mobile in users table & retailers table with account id
        //4) change the slab in retailers table if on diff commissions
        //5) Add operators in partner_operator_status table
         
	/*function createAPIVendor($ret_id,$dist_id){
		if (empty($_SESSION['Auth']) || ($_SESSION['Auth']['User']['group_id'] != ADMIN )) {
                    $this->redirect('/');
                }            
        
	}*/
         
        function modemRequest(){
		$response = array();
		if(empty($_SESSION['Auth'])){
			$response = array('status'=>'failure','errno'=>'0','data'=>  $this->Shop->errors(0));
		}else{
			$type   =  isset($_REQUEST['type']) ? $_REQUEST['type'] : "" ;
			$vendor   =  isset($_REQUEST['vendor']) ? $_REQUEST['vendor'] : "" ;
			$device   =  isset($_REQUEST['device']) ? $_REQUEST['device'] : "" ;
			$ret = "";
			$mdmArr = array();
			if($type == 1){//to send SMS
				$mobile   =  isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : "" ;
				$msg   =  isset($_REQUEST['msg']) ? $_REQUEST['msg'] : "" ;
				$adm = "query=command&type=1&device=$device&mobile=$mobile&msg=".urlencode($msg);// create query to send SMS // @TODO
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "1";
				$mdmArr['device'] = $device;
				$mdmArr['mobile'] = $mobile;
				$mdmArr['msg'] = urlencode($msg);
				//$ret = $this->Shop->modemRequest($adm,$vendor);// @TODO

			}else if($type == 2){//to execute Command
				$cmd   =  isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : "" ;
				$command   =  isset($_REQUEST['command']) ? $_REQUEST['command'] : "" ;
				$wait   =  isset($_REQUEST['wait']) ? $_REQUEST['wait'] : "" ;
				$adm = "query=command&type=2&device=$device&command=$cmd&time=$wait";// create query to run AT Command // @TODO
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "2";
				$mdmArr['device'] = $device;
				$mdmArr['command'] = $cmd;
				$mdmArr['time'] = $wait;
				//$ret = $this->Shop->modemRequest($adm,$vendor);

			}else if($type == 3){//to USSD Command
				$cmd   =  isset($_REQUEST['ussd']) ? $_REQUEST['ussd'] : "" ;
				$command   =  isset($_REQUEST['command']) ? $_REQUEST['command'] : "" ;
				$wait   =  isset($_REQUEST['wait']) ? $_REQUEST['wait'] : "" ;
				$adm = "query=command&type=3&device=$device&command=$cmd&time=$wait";// create query to run AT Command // @TODO
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "3";
				$mdmArr['device'] = $device;
				$mdmArr['command'] = $cmd;
				$mdmArr['time'] = $wait;
				//$ret = $this->Shop->modemRequest($adm,$vendor);

			}else if($type == 4){//to Reset Bus Devide Command

				$device =  isset($_REQUEST['device']) ? $_REQUEST['device'] : "" ;
				$adm = "query=command&type=4&device=$device";// create query to run device reset command // @TODO
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "4";
				$mdmArr['device'] = $device;
				//$ret = $this->Shop->modemRequest($adm,$vendor);

			}else if($type == 5){//to Reboot System

				$adm = "query=command&type=5&device=0";// create query to run Reboot Command // @TODO
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "5";
				//$ret = $this->Shop->modemRequest($adm,$vendor);

			}else if($type == 6){//to insert new device

				$adm = "query=command&type=6&dev_act_flag=".$_REQUEST['dev_act_flag']."&dev_balance=".$_REQUEST['dev_balance']."&dev_commission=".$_REQUEST['dev_commission']."&dev_mobile=".$_REQUEST['dev_mobile']."&dev_opr_id=".$_REQUEST['dev_opr_id']."&dev_opr_name=".$_REQUEST['dev_opr_name']."&dev_par_bal=".$_REQUEST['dev_par_bal']."&dev_pin=".$_REQUEST['dev_pin']."&dev_rch_flag=".$_REQUEST['dev_rch_flag']."&dev_sim_id=".$_REQUEST['dev_sim_id']."&dev_type_id=".$_REQUEST['dev_type_id']."&dev_vendor_nm=".$_REQUEST['dev_vendor_nm'];
				$mdmArr['query'] = "command";
				$mdmArr['type'] = "6";
				$mdmArr['dev_balance']= $_REQUEST['dev_balance'];
				$mdmArr['dev_commission']= $_REQUEST['dev_commission'];
				$mdmArr['dev_mobile']= $_REQUEST['dev_mobile'];
				$mdmArr['dev_opr_id']= $_REQUEST['dev_opr_id'];
				$mdmArr['dev_opr_name']= $_REQUEST['dev_opr_name'];
				$mdmArr['dev_par_bal']= $_REQUEST['dev_par_bal'];
				$mdmArr['dev_pin']= $_REQUEST['dev_pin'];
				$mdmArr['dev_sim_id']= $_REQUEST['dev_sim_id'];
				$mdmArr['dev_type_id']= $_REQUEST['dev_type_id'];
				$mdmArr['dev_vendor_nm']= $_REQUEST['dev_vendor_nm'];


			}else if($type == 7){//to Show-Hide Sim
                                                                                                $date = date("Y-m-d H:i:s");
//                                                                                                echo $date;
                                                                                                //query to obtain last transaction timestamp
                                                                                                $query="SELECT last  FROM `devices_data` WHERE `device_id` = $device AND `vendor_id` = $vendor AND sale > 0 order by last desc limit 1";
                                                                                                $lasttxnquery=$this->Slaves->query($query);
                                                                                                $last = $lasttxnquery[0]['devices_data']['last'];
                                                                                                $lastdate=strtotime($last);
                                                                                                $curdate=strtotime($date);
                                                                                                $diff=round(($curdate - $lastdate)/(60*60*24));
                                                                                                $opr_id = $this->params['url']['opr_id'];
                                                                                                if($diff > 10):
                                                                                                        $adm = "query=simhide&device=$device&opr_id=$opr_id";// create query to run Reboot Command // @TODO
                                                                                                        $mdmArr['query'] = "simhide";
                                                                                                        $mdmArr['type'] = "7";
                                                                                                        $mdmArr['device'] = $device;
                                                                                                        $mdmArr['opr_id'] = $opr_id;
                                                                                                        //$mdmArr['flag'] = $_REQUEST['flag'];
                                                                                                        $ret = $this->Shop->modemRequest($adm,$vendor);
                                                                                                        $ret = $ret['data'];
                                                                                                        echo $ret;
                                                                                                        die;
                                                                                                else:
                                                                                                        echo "failure";
                                                                                                        die;
                                                                                                endif;
                                
			}else {//wrong type
				$response = array('status'=>'failure','errno'=>'2','data'=>  $this->Shop->errors(2));
			}


			if(!isset($response['status'])){
				//Make an entry in modem_request_log
					
				$this->data['ModemRequestLog']['input'] = json_encode($mdmArr);
				$this->data['ModemRequestLog']['output'] = "";
				$this->data['ModemRequestLog']['vendor'] = $vendor;
				$this->data['ModemRequestLog']['created'] = date('Y-m-d H:i:s');
				$this->data['ModemRequestLog']['modified'] = date('Y-m-d H:i:s');

				$this->ModemRequestLog->create();
				if ($this->ModemRequestLog->save($this->data)) {
					$id = $this->ModemRequestLog->getInsertID();
					$adm = $adm."&request=$id";
					$ret = $this->Shop->modemRequest($adm,$vendor);
				}
				if(empty($ret))$response = array('status'=>'failure','errno'=>'2','data'=>  'Device Busy');
				else $response = array('status'=>'success','data'=>  $ret);
			}
		}

		echo json_encode($response);
                                                die;
		$this->autoRender = false;
	}

	function request(){
		$data1 = $this->Slaves->query("select count(dd.id) as ct,dd.opr_id,products.name from devices_data as dd inner join vendors ON (vendors.id = dd.vendor_id) inner join products ON (products.id = dd.opr_id) inner join vendors_commissions as vc ON (vc.vendor_id = dd.vendor_id AND vc.product_id=dd.opr_id) where sync_date = '".date('Y-m-d')."' and vc.oprDown = 0 and dd.block = '0' AND dd.stop_flag = 0 and dd.device_num > 0 AND dd.active_flag = 1 AND dd.balance > 10 group by dd.opr_id");
		
		$data2 = $this->Slaves->query("SELECT count(vendors_activations.id) as ct,vendors.update_flag,vendors_activations.product_id,vendors_activations.timestamp FROM `vendors_activations` inner join vendors ON (vendors.id = vendor_id) WHERE date='".date('Y-m-d')."' AND timestamp >= '".date('Y-m-d H:i:s',strtotime('-5 minutes'))."' group by vendors.update_flag,vendors_activations.product_id,minute(vendors_activations.timestamp)");
		
		$requests = array();
		
		foreach($data1 as $dt){
			$prod = $dt['dd']['opr_id'];
			$requests[$prod]['devices'] = $dt[0]['ct'];
			$requests[$prod]['name'] = $dt['products']['name'];
		}
		
		foreach($data2 as $dt){
			$prod = $dt['vendors_activations']['product_id'];
			$prod = $this->Shop->getParentProd($prod);
			$minute = date('Y-m-d H:i',strtotime($dt['vendors_activations']['timestamp']));
			
			if(isset($requests[$prod])){
				if(!isset($requests[$prod][$minute]))$requests[$prod][$minute] = array('api_txns' => 0,'modem_txns'=>0);
				
				$requests[$prod][$minute]['api_txns'] = ($dt['vendors']['update_flag'] == 0) ? $dt['0']['ct'] + $requests[$prod][$minute]['api_txns'] : $requests[$prod][$minute]['api_txns'];
				$requests[$prod][$minute]['modem_txns'] = ($dt['vendors']['update_flag'] == 1) ? $dt['0']['ct'] + $requests[$prod][$minute]['modem_txns'] : $requests[$prod][$minute]['modem_txns'];
			}
		}
		
		//$this->set('devices',$data1);
		$this->set('requests',$requests);
		
		//$this->printArray($requests);
		//$this->autoRender = false;
	}
	
    function test(){
		echo "1"; exit;
        }   
        
        
      /*  function retList($distId=null, $retId=null){
            if($this->Session->read('Auth.User.group_id') !=ADMIN && $this->Session->read('Auth.User.id') != 1)
	    		$this->redirect('/shops/view');
            $verify_flag = isset($_GET['verify_flag']) ? $_GET['verify_flag'] : 2;
            $verify_query = "";
            if($verify_flag === "0" || in_array($verify_flag, array(1, 2))){
            	$verify_query = " and verify_flag = $verify_flag";
            	$this->set("verify_flag", $verify_flag);
            }
            
			$query = "";
			$query1 = "";
// 			if($distId == null) $distId = 1;

			$this->set('distId', $distId);
		       // $this->set('sid',$smId);
			if($distId)
		    	$query = " AND parent_id = $distId";
	           
	        if ($retId != null) {
	            $this->Retailer->query("UPDATE retailers SET verify_flag = '1', modified = '".date('Y-m-d H:i:s')."' WHERE id = $retId");
	        }
	        $retilerData=$this->Slaves->query("select * from 
	        		(select retailers.block_flag,retailers.area,retailers.address,retailers.pin,retailers.id,retailers.name,
	        			retailers.shopname,retailers.mobile,retailers.parent_id,retailers.verify_flag,retailers.balance,
	        			Date(retailers.created) as created,user_profile.latitude, user_profile.longitude, user_profile.device_type,
	        			user_profile.updated,locator_area.name AS areaname, locator_city.name AS cityname, locator_state.name AS statename,
	        		 	locator_area.city_id, locator_city.state_id 
	        		from retailers   
	        		LEFT JOIN user_profile ON (user_profile.user_id = retailers.user_id and user_profile.device_type='online') 
	        		LEFT JOIN locator_area ON locator_area.id = retailers.area_id 
	        		LEFT JOIN locator_city ON locator_city.id = locator_area.city_id  
	        		LEFT JOIN locator_state ON locator_state.id = locator_city.state_id  
	        		where 1  $query $verify_query
	        		order By 
	        			case when user_profile.device_type = 'online' then 1 else 2 end, 
	        			user_profile.updated desc) as retailers 
	        		group by retailers.id");

			$distList=$this->Slaves->query("select distributors.id,distributors.company,users.mobile from distributors,users WHERE users.id =distributors.user_id order by company");
	  
			$this->set('distList',$distList);
	                $distarray = array();
	                foreach ($distList as $key){
	                    $distarray[$key['distributors']['id']] = $key['users']['mobile'];
	                }
	                
	        $this->set('distMobileNumber', $distarray);
	        $averageSale = array();
	        $retailerarray = array();
	        
	        $rl_query = "";
	        if($distId)
	        	$rl_query = " AND retailers.parent_id = $distId ";
	        $averageResult = $this->Slaves->query("SELECT avg(sale) as avg_ret, retailer_id 
	        		from retailers_logs,retailers 
	        		WHERE retailers.id = retailers_logs.retailer_id $rl_query 
	        		group by retailer_id order by avg_ret desc");
			
	       // $averageResult = $this->Retailer->query("SELECT avg(sale) as avg_ret, retailer_id from retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id = $distId  AND retailers_logs.date between CURDATE() - INTERVAL 5 DAY  and  CURDATE()   group by retailer_id order by avg_ret desc");
	        foreach ($retilerData as $retkey) {
	            $retailerarray[$retkey['retailers']['id']] = $retkey;
	        }
	       /// var_dump($retailerarray);
	        $data = array();
	        if (!empty($averageResult) && isset($averageResult)) {
	            foreach ($averageResult as $key) {
	                if (isset($retailerarray[$key['retailers_logs']['retailer_id']])) {
	                    $data[$key['retailers_logs']['retailer_id']] = array($key[0]['avg_ret'],$retailerarray[$key['retailers_logs']['retailer_id']]);
	                }
	            }
	        }
	   		$this->set('retList',$data);
        }*/
        
        function leads() 
        {
        if ($this->RequestHandler->isPost()) 
            {
            $frm = $_POST['from'];
            $to = $_POST['to'];
            $interest = $_POST['interest'];
            $page = isset($_POST['download']) ? $_POST['download'] : "";
            $fromdate = explode("-", $frm);
            $todate = explode("-", $to);
            $fd = $fromdate[2] . "-" . $fromdate[1] . "-" . $fromdate[0];
            $ft = $todate[2] . "-" . $todate[1] . "-" . $todate[0];
            $query_where = "";
            if($_POST['search'] != '') {
                $query_where .= " and (name LIKE '%". $_POST['search'] ."%' or email LIKE '%". $_POST['search'] ."%' or phone LIKE '%". $_POST['search'] ."%') ";
            }
            if($_POST['city'] != '') {
                $query_where .= " and city = '".$_POST['city']."' ";
            }
            if(in_array($interest, array('Retailer', 'Distributor')))
            {
            	$query_where .= " and interest = '".$interest."' ";
            }
            $leadsData = $this->User->query("select id,name,email,city,messages,phone,timestamp,date,status,agentname,comment,req_by,interest,if(remark='',null,remark) as remark,followup_date 
            		from leads where date>='" . $fd . "' AND date<='" . $ft . "' $query_where group by phone order by timestamp desc");
            foreach($leadsData as $k => $ld)
                {
            	$retailer = $this->User->query("select * from retailers where mobile = '".$ld['leads']['phone']."'");
            	if(isset($retailer['0']))
            		$leadsData[$k]['leads']['is_retailer'] = "Yes";
            	else 
            		$leadsData[$k]['leads']['is_retailer'] = "No";
                }
			
                    if($page == 'download')
                    {
			$this->set('page',$page);
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			
		    $line = array('Row','Name','Email','city', 'Message', 'Phone',  'timestamp','Comment','Required by','Interest','Remark','Status','Agent name','Is_retailer','Follow up');
			$csv->addRow($line);
			$mapRemark=array(1=>'Interested',
                                         2=>'Not Contactable',
                                         3=>'Fake Lead',
                                         4=>'Not Interested');
			
		    $i=1;
			
			foreach ($leadsData as $val):
                            if($val['leads']['status']=='0')
                                {
                                 $status ="Open";
                                } 
                            else 
                                {
                                 $status ="Closed";
                                }
			
				
			$line = array($i,$val['leads']['name'],$val['leads']['email'],$val['leads']['city'],$val['leads']['messages'],$val['leads']['phone'],$val['leads']['timestamp'],$val['leads']['comment'],$val['leads']['req_by'],$val['leads']['interest'],$mapRemark[$val[0]['remark']],$status,$val['leads']['agentname'],$val['leads']['is_retailer'],$val['leads']['followup_date']);
				
			    $csv->addRow($line);
				$i++;
			endforeach;
			
			
			echo $csv->render("leads_".$frm."_".$to.".csv");
			}
                        
                        
            $this->set('leadData', $leadsData);
            $this->set('fromdate',$frm);
            $this->set('todate',$to);
            $this->set('interest', $interest);
            
            if ($this->RequestHandler->isAjax())
                {
                $comm = trim($_POST["comm"]);
                $id = trim($_POST["id"]);
                $name = trim($_POST["name"]);
                $rem = trim($_POST["rem"]);
                $followup = trim($_POST["followup"]);
                if(isset($id) && !empty($id))
                $this->Retailer->query("UPDATE leads SET comment = '" . addslashes($comm) . "',status='1',agentname='$name',remark='$rem',followup_date='$followup',updated_by = '".$this->Session->read('Auth.User.id')."' ,timestamp ='".date('Y-m-d H:i:s')."' WHERE id = $id");
                echo json_encode(array("status"=>"success","msg" => "Data updated successfully"));
                exit();
                }
        }
        
        $cities = $this->User->query("SELECT distinct city FROM leads");
        
        $city_temp = array();
        foreach($cities as $city) {
            $city_temp[] = $city['leads']['city'];
        }
        $this->set('cities', $city_temp);
    }
    
    function changeInterest() {
        
        $this->autoRender = FALSE;
        
        $id         = $this->params['form']['id'];
        $interest   = $this->params['form']['interest'];
        
        echo $res   = $this->Retailer->query("UPDATE leads SET interest = '$interest' WHERE id = $id");
    }
    
    function complainReport($frm_date=null,$to_date=null,$frm_time=null,$to_time=null) {

            if($frm_date != NULL && $to_date != NULL && $frm_time != NULL && $to_time != NULL) {
                    $fd = $frm_date;
                    $td = $to_date;
                    $ft = str_replace(".", ":", $frm_time).":00";
                    $tt = str_replace(".", ":", $to_time).":00";
            } else {
                    $fd = date('Y-m-d');
                    $td = date('Y-m-d');
                    $ft = "00:00:00";
                    $tt = "23:59:59";
            }
            $this->layout = 'sims';
		
            $getTurnaroundTime = $this->Slaves->query("Select vendors_commissions.tat_time,vendor_id,product_id from vendors_commissions");
		
            foreach ($getTurnaroundTime as $tatval) {
                    $tatArray[$tatval['vendors_commissions']['vendor_id']][$tatval['vendors_commissions']['product_id']] = $tatval['vendors_commissions']['tat_time'];
            }
		
            $transResult = $this->Slaves->query("Select  COUNT(comp.id) as complaint,
            SUM(if(comp.resolve_date='' OR comp.resolve_date IS NULL ,1,0)) as open,
            SUM(if(comp.resolve_date!='' OR comp.resolve_date IS NOT NULL,1,0)) as closed,
            SUM(if(vendors_activations.complaintNo IS NOT NULL AND  comp.takenby = 0,1,0)) as manualreverse,
            SUM(if(vendors_activations.complaintNo IS NULL,1,0)) as autoreverse,
            SUM(if(comp.takenby!=0 AND vendors_activations.complaintNo!=0 AND vendors_activations.complaintNo IS NOT NULL,1,0)) as manualComplaintreverse,
            comp.vendor_activation_id, CONCAT_WS( ' ', resolve_date,resolve_time) as resolvetime, turnaround_time,
            users.name,
            users.id as user_id,
            vendors.id,
            vendors.company,
			vendors.update_flag,
            products.name,
            vendors_activations.product_id,
			vendors_activations.vendor_id
            FROM  complaints as comp
			FORCE INDEX (idx_in_date)
            LEFT JOIN vendors_activations 
            ON comp.vendor_activation_id = vendors_activations.id 
            LEFT JOIN users  
            ON users.id = comp.closedby
            LEFT JOIN vendors on vendors.id = vendors_activations.vendor_id
            LEFT JOIN products ON vendors_activations.product_id = products.id
            WHERE comp.in_date >= '$fd' AND comp.in_date <= '$td' AND comp.in_time >= '$ft' AND comp.in_time <= '$tt'
            GROUP BY comp.id"
		);
		
        $data = array();
        $totalComplaint = 0;
        $totalClosed = 0;
        $totalOpen = 0;
        $totalManualReversed = 0;
        $totalAutoReversed = 0;
        $outOfTat = 0;
        $reopen = 0;
        $reopenData = array();

        foreach ($transResult as $transkey => $transval) {
            $totalComplaint+=$transval[0]['complaint'];
            $totalClosed+=$transval[0]['closed'];
            $totalOpen+=$transval[0]['open'];
            $totalManualReversed+=$transval[0]['manualreverse'];
            $totalAutoReversed+=$transval[0]['autoreverse'];
            $totalmanualComplaintReversed+=$transval[0]['manualComplaintreverse'];

            if (!isset($data[$transval['users']['user_id']])) {
                $data[$transval['users']['user_id']]['user']['open'] = 0;
                $data[$transval['users']['user_id']]['user']['closed'] = 0;
            }
            if (!isset($data[$transval['vendors']['vendor_id']])) {
                $data[$transval['vendors']['id']]['vendor']['open'] = 0;
                $data[$transval['vendors']['id']]['vendor']['closed'] = 0;
				$data[$transval['vendors']['id']]['vendor']['total'] = 0;
				$data[$transval['vendors']['id']]['vendor']['outoftat'] = 0;
            }
            if (!isset($data[$transval['vendors_activations']['product_id']])) {
                $data[$transval['vendors_activations']['product_id']]['product']['open'] = 0;
                $data[$transval['vendors_activations']['product_id']]['product']['closed'] = 0;
				$data[$transval['vendors_activations']['product_id']]['product']['total'] = 0;
				$data[$transval['vendors_activations']['product_id']]['product']['outoftat'] = 0;
            }
            $reopenCount[$transval['vendors']['id']][$transval['comp']['vendor_activation_id']]['vendor'][] = $transval['comp']['vendor_activation_id'];
            $reopenCount[$transval['vendors_activations']['product_id']][$transval['comp']['vendor_activation_id']]['product'][] = $transval['comp']['vendor_activation_id'];

            $data[$transval['vendors']['id']]['vendor']['open']+= isset($transval[0]['open']) ? $transval[0]['open'] : 0;
			
			$data[$transval['vendors']['id']]['vendor']['total']+= isset($transval[0]['complaint']) ? $transval[0]['complaint'] : 0;
			
            $data[$transval['vendors']['id']]['vendor']['closed']+= isset($transval[0]['closed']) ? $transval[0]['closed'] : 0;
			
            $data[$transval['vendors']['id']]['vendor']['name'] = $transval['vendors']['company'];
			
			$data[$transval['vendors']['id']]['vendor']['update_flag'] = $transval['vendors']['update_flag'];
			
            $data[$transval['users']['user_id']]['user']['open']+= isset($transval[0]['open']) ? $transval[0]['open'] : 0;
			
            $data[$transval['users']['user_id']]['user']['closed']+= isset($transval[0]['closed']) ? $transval[0]['closed'] : 0;
			
            $data[$transval['users']['user_id']]['user']['name'] = $transval['users']['name'];
			
            $data[$transval['vendors_activations']['product_id']]['product']['open']+=isset($transval[0]['open']) ? $transval[0]['open'] : 0;
			
            $data[$transval['vendors_activations']['product_id']]['product']['closed']+=isset($transval[0]['closed']) ? $transval[0]['closed'] : 0;
			
            $data[$transval['vendors_activations']['product_id']]['product']['name'] = $transval['products']['name'];
			
			$data[$transval['vendors_activations']['product_id']]['product']['total']+=$transval[0]['complaint'];
			
			if(strtotime($transval[0]['resolvetime'])>strtotime($transval['comp']['turnaround_time'])){
				$data[$transval['vendors_activations']['vendor_id']]['vendor']['outoftat']+=$transval[0]['complaint'];
				$data[$transval['vendors_activations']['product_id']]['product']['outoftat']+=$transval[0]['complaint'];
				//$data1[$transval['vendors']['id']][] = $transval;
				$outOfTat++;
			}
        }
		
        foreach ($reopenCount as $key => $val) {
            if (isset($key) && !empty($key)) {
                foreach ($val as $k => $v) {
                    if (count($v['vendor']) > 1) {
                        $reopenData[$key][] = $v;
                    }
                    if (count($v['product']) > 1) {
                        $reopenData[$key][] = $v;
                    }
                }
            }
        }

        $rcount = array();
        foreach ($reopenData as $rkey => $rval) {
            foreach ($rval as $k => $v) {
                if (isset($v['vendor'])) {
                    if (!isset($rcount[$rkey]['vendor'])) {
                        $rcount[$rkey]['vendor'] = 0;
                    }
                    $rcount[$rkey]['vendor'] ++;
                }
                if (isset($v['product'])) {
                    if (!isset($rcount[$rkey]['product'])) {
                        $rcount[$rkey]['product'] = 0;
                    }
                    $rcount[$rkey]['product'] ++;
                }
            }
        }
		
		
        $dataArray = array();
		
        foreach ($data as $key => $val):

			if (!empty($key)):

				if (isset($val['vendor'])) {

					if ($val['vendor']['update_flag'] == 1) {

						$dataArray['modem'][$key][] = $val['vendor'];
					} else {
						$dataArray['api'][$key][] = $val['vendor'];
					}
					if (isset($rcount[$key]['vendor']) && $val['vendor']['update_flag'] == 1) {

						$dataArray['modem'][$key][0]['reopen'] = $rcount[$key]['vendor'];
					} else if (isset($rcount[$key]['vendor']) && $val['vendor']['update_flag'] == 0) {

						$dataArray['api'][$key][0]['reopen'] = $rcount[$key]['vendor'];
					}
				}
				if (isset($val['product'])) {

					$dataArray['product'][$key][] = $val['product'];

					if (isset($rcount[$key]['product'])) {

						$dataArray['product'][$key][0]['reopen'] = $rcount[$key]['product'];
					}
				}
				if (isset($val['user'])) {

					$dataArray['user'][$key][] = $val['user'];
				}

			endif;

		endforeach;


		$hourWiseReport = $this->Slaves->query("SELECT count( * ) AS count, 
                                                CAST(time_to_sec( timediff(CONCAT_WS( ' ', resolve_date, resolve_time ) , CONCAT_WS( ' ', in_date, in_time ) ) ) /3600
                                                AS UNSIGNED INTEGER) 
                                                AS diff
                                                FROM complaints
                                                WHERE complaints.in_date >= '$fd' AND complaints.in_date <= '$td' AND complaints.in_time >= '$ft' AND complaints.in_time <= '$tt'
                                                GROUP BY diff");

        foreach ($hourWiseReport as $hourkey => $hourval) {
            if ($hourval[0]['diff'] <= 9) {
                $dataArray['hour'][$hourval[0]['diff']] = $hourval[0]['count'];
            }
        }
        $dayWiseReport = $this->Slaves->query("SELECT count( * ) AS closedCount,DATEDIFF(resolve_date,in_date) AS days
                                                FROM complaints where resolve_date >= '$fd' AND resolve_date <= '$td' AND resolve_time >= '$ft' AND resolve_time <= '$tt' 
                                                AND resolve_flag = '1'
                                                group by days");
		
		


        $opentransReport = $this->Slaves->query("SELECT count( * ) AS openCount,DATEDIFF(CURDATE(),in_date) AS days
                                                FROM complaints where in_date >= '$fd' AND in_date <= '$td' AND in_time >= '$ft' AND in_time <= '$tt' 
                                                AND (resolve_date IS NULL 
                                                OR resolve_date = '') and vendor_activation_id !=0 
                                                group by days");

        foreach ($opentransReport as $opentranskey => $opentransval) {
            $dataArray['opencount'][$opentransval[0]['days']] = $opentransval[0]['openCount'];
        }

        foreach ($dayWiseReport as $daykey => $dayval) {
            $dataArray['days'][$dayval[0]['days']] = $dayval[0]['closedCount'];
        }
        $this->set('dataset', $dataArray);
        $this->set('totalComplaint', $totalComplaint);
        $this->set('totalClosed', $totalClosed);
        $this->set('totalOpen', $totalOpen);
        $this->set('totalManualReversed', $totalManualReversed);
        $this->set('totalAutoReversed', $totalAutoReversed);
        $this->set('fromDate', $fd);
        $this->set('toDate', $td);
        $this->set('fromTime', $ft);
        $this->set('toTime', $tt);
        $this->set('outoftat', $outOfTat);
        $this->set('totalManualComplaintReversed', $totalmanualComplaintReversed);
    }

    /*function complainReportDetails($frmDate, $toDate, $type) {

        if (isset($frmDate) && isset($toDate)) {
            $fromdate = explode("-", $frmDate);
            $todate = explode("-", $toDate);
            $fd = $fromdate[2] . "-" . $fromdate[1] . "-" . $fromdate[0];
            $ft = $todate[2] . "-" . $todate[1] . "-" . $todate[0];
        } else {
            $fd = date('Y-m-d');
            $ft = date('Y-m-d');
        }

        if ($type == "manual") {
            $query = "AND vendors_activations.complaintNo IS NOT NULL AND vendors_activations.complaintNo!=0 AND vendors_activations.complaintNo!='' AND complaints.takenby = 0";
        } else if ($type == "comp_reversed") {
            $query = "AND complaints.takenby!=0 AND vendors_activations.complaintNo!=0 AND vendors_activations.complaintNo IS NOT NULL AND complaints.resolve_flag = 1";
        } else {
            $query = "";
        }

        $transResult = $this->Slaves->query("Select complaints.id as complaint,
                                                    complaints.takenby,
                                                    vendors_activations.complaintNo,
                                                    vendors_activations.mobile,
                                                    vendors_activations.id,
                                                    vendors_activations.ref_code,
                                                    vendors_activations.vendor_refid,
                                                    users.name,
                                                    users.id as user_id,
                                                    vendors.id,
                                                    vendors.company,
                                                    vendors_activations.product_id,
                                                    products.name,
                                                    vendors_activations.amount,
                                                    vendors_activations.shop_transaction_id,
                                                    CONCAT_WS(' ',complaints.in_date,complaints.in_time) as complainintime
                                                    FROM  complaints 
                                                    LEFT JOIN vendors_activations 
                                                    ON complaints.vendor_activation_id = vendors_activations.id 
                                                    LEFT JOIN users  
                                                    ON users.id = complaints.closedby
                                                    LEFT JOIN vendors on vendors.id = vendors_activations.vendor_id
                                                    LEFT JOIN products ON vendors_activations.product_id = products.id
                                                    WHERE vendors_activations.date BETWEEN '$fd' AND '$ft'
                                                    $query 
                                                    GROUP BY complaints.id"
        );

        $this->set('transDetails', $transResult);
    }*/

    function reOpenDetails($type, $id, $fdate, $tdate, $ftime, $ttime) {

        if (isset($fdate) && isset($tdate) && isset($ftime) && isset($ttime)) {
            $fd = $fdate;
            $td = $tdate;
            $ft = str_replace('.', ':', $ftime);
            $tt = str_replace('.', ':', $ttime);

        } else {
            $fd = date('Y-m-d');
            $td = date('Y-m-d');
            $ft = "00:00:00";
            $tt = "23:59:59";
        }

        if ($type == 'opr') {
            $query1 = "WHERE vendors_activations.product_id = '$id'
                    AND complaints.in_date between '$fd' AND '$td' AND complaints.in_time between '$ft' AND '$tt'";
        } else {
            $query1 = "WHERE vendors_activations.vendor_id ='$id'
                    AND complaints.in_date between '$fd' AND '$td' AND complaints.in_time between '$ft' AND '$tt'";
        }
	
        $query = "SELECT count(*) as reopenCount,vendors_activations.*,products.name,vendors.company "
                . " FROM "
                . " complaints INNER JOIN"
                . " vendors_activations "
                . " on complaints.vendor_activation_id = vendors_activations.id "
                . " inner join products on products.id = vendors_activations.product_id "
                . " inner join vendors on vendors.id = vendors_activations.vendor_id "
                . " $query1"
                . " group by vendors_activations.id "
                . " having reopenCount>1";
		 
        $getReopenCount = $this->Slaves->query($query);
        $this->set('transDetails', $getReopenCount);
    }
	
    function ccReport(){
    	$REPORT_INIT_DATE = "2015-05-16";
    	$REPORT_INIT_TIMESTAMP = strtotime($REPORT_INIT_DATE);//1431754200
    	
    	$fromDate = isset($_REQUEST['fromDate']) ? $_REQUEST['fromDate'] : date('Y-m-d');
    	$toDate = isset($_REQUEST['toDate']) ? $_REQUEST['toDate'] : $fromDate;
    	$days = (strtotime($toDate) - strtotime($fromDate))/(60 * 60 * 24);
    	if(strtotime($fromDate) < $REPORT_INIT_TIMESTAMP)
    		$fromDate = $REPORT_INIT_DATE;
    	if(strtotime($toDate) < strtotime($fromDate))
    		$toDate = $fromDate;
    		
    	if(strtotime($fromDate) < 1431754200 || strtotime($toDate) < 1431754200)
    	if($days > 31)
    		$fromDate = date('Y-m-d', strtotime($toDate) - (31 * 60 * 60 * 24));
    	
    	$date_range = " where cc.date between '$fromDate' and '$toDate' ";
    	$date_range_c = " where c.date between '$fromDate' and '$toDate' ";
    	 
    	$call_type_where = isset($_REQUEST['callTypeId']) && !empty($_REQUEST['callTypeId']) ? " and c.call_type_id = ".$_REQUEST['callTypeId']." " : "";
    	$via_where = isset($_REQUEST['via']) && !empty($_REQUEST['via']) ? " and cc.medium = ".$_REQUEST['via']." " : "";
    	$tag_where = isset($_REQUEST['tagId']) && !empty($_REQUEST['tagId']) ? " and c.tag_id = ".$_REQUEST['tagId']." " : "";
    	$user_where = isset($_REQUEST['mobile']) && !empty($_REQUEST['mobile']) ? " and c.mobile = '".$_REQUEST['mobile']."' " : "";
    	$vendor_where = isset($_REQUEST['vendorId']) && !empty($_REQUEST['vendorId']) ? " and cc.vendor_id = ".$_REQUEST['vendorId']." " : "";
    	$product_where = isset($_REQUEST['productId']) && !empty($_REQUEST['productId']) ? " and cc.product_id = ".$_REQUEST['productId']." " : "";
    	$vendors_activations_join = $vendor_where.$product_where ? " left join vendors_activations va on va.ref_code = c.ref_code " : "";
    	$comments_count_join = $via_where.$vendor_where.$product_where ? " left join comments_count cc on c.ref_code = cc.ref_code and c.date = cc.date " : "";
    	$comments_join = $call_type_where.$tag_where.$user_where ? " right join comments c on c.ref_code = cc.ref_code and c.date = cc.date " : "";
    	
    	$call_type_inclusion = "";//" and c.call_type_id in (0, 5, 6, 7, 8, 9) ";
    	$where = $date_range.$call_type_where.$tag_where.$user_where.$vendor_where.$product_where.$via_where;
    	$where_c = $date_range_c.$call_type_where.$tag_where.$user_where.$vendor_where.$product_where.$via_where;
    	$subject = "";
    	
    	$report_users = "(1, 7, 16, 19, 21)";
    	
    	if($fromDate == $toDate){
    		$fromTime = isset($_REQUEST['from_time']) ?  $_REQUEST['from_time'] : "";
    		$toTime = isset($_REQUEST['to_time']) ?  $_REQUEST['to_time'] : "";
    		
    		$via_where_c = isset($_REQUEST['via']) && !empty($_REQUEST['via']) ? " and c.medium = ".$_REQUEST['via']." " : "";
    		$vendor_where_va = isset($_REQUEST['vendorId']) && !empty($_REQUEST['vendorId']) ? " and va.vendor_id = ".$_REQUEST['vendorId']." " : "";
    		$product_where_va = isset($_REQUEST['productId']) && !empty($_REQUEST['productId']) ? " and va.product_id = ".$_REQUEST['productId']." " : "";
    		if($fromTime AND $toTime){
    			$date_time_where = " $date_range_c and c.created between '".$fromDate." ".$fromTime.":00:00' 
    					and '".$toDate." ".$toTime.":00:00' ";
    			$where_time = $date_time_where.$call_type_where.$tag_where.$user_where.$vendor_where_va.$product_where_va.$via_where_c;
    			$this->set('from_time', $fromTime);
    			$this->set('to_time', $toTime);
    		}	
    	}
    	switch(true){
    		case $call_type_where:
    			$call_type = $this->Slaves->query("select ct.name from cc_call_types ct where ct.id = ".$_REQUEST['callTypeId']);
    			if($call_type)
    				$subject .= " ".$call_type[0]['ct']['name'];
    			break;
    		case $tag_where:
    			$tag = $this->Slaves->query("select t.name from taggings t where t.id = ".$_REQUEST['tagId']);
    			if($tag)
    				$subject .= " ".$tag[0]['t']['name'];
    			break;
    		case $user_where:
    			$user = $this->Slaves->query("select u.name from users u where u.mobile = '".$_REQUEST['mobile']."'");
    			if($user)
    				$subject .= " ".$user[0]['u']['name'];
    			break;
    		case $vendor_where:
    			$vendor = $this->Slaves->query("select v.company from vendors v where v.id = ".$_REQUEST['vendorId']);
    			if($vendor)
    				$subject .= " ".$vendor[0]['v']['company'];
    			break;
    		case $product_where:
    			$product = $this->Slaves->query("select p.name from products p where p.id = ".$_REQUEST['productId']);
    			if($product)
    				$subject .= " ".$product[0]['p']['name'];
    			break;
    	}
    	$this->set('subject', $subject);
    	if($user_where)
    		$this->set('user_mobile', $_REQUEST['mobile']);
    	
    	$last1Month = date('Y-m-d', strtotime('-30 days'));
    	$usersList = $this->Slaves->query("select c.mobile as mobile, u.name as user
    									from comments c
										left join users u on u.mobile = c.mobile
										where u.group_id in $report_users and c.date > '".$last1Month."'
										group by c.mobile");
    	$this->set('usersList', $usersList);
    	
    	if($where_time){
    		$usersCallTypes = $this->Slaves->query("	
    				select count(c.comments) as count, u.name as user, ct.name as call_type,
						c.mobile as mobile, c.call_type_id as call_type_id
					from comments c
					left join users u on u.mobile = c.mobile
					left join cc_call_types ct on ct.id = c.call_type_id
    				$where_time
					and u.group_id in $report_users
					group by c.mobile, c.call_type_id");
    	}
    	else {
    		$usersCallTypes = $this->Slaves->query("	select count(c.comments) as count, u.name as user, ct.name as call_type,
    				c.mobile as mobile, c.call_type_id as call_type_id
    				from comments c
    				left join users u on u.mobile = c.mobile
    				left join cc_call_types ct on ct.id = c.call_type_id
    				$comments_count_join
    				$where_c
    				$call_type_inclusion
    				and u.group_id in $report_users
    				group by c.mobile, c.call_type_id");
    	}
    	$callTypes = $usersCallTypesCounts = array();
    	$users = $call_types = array();
    	foreach($usersCallTypes as $uct){
    		isset($callTypes[-1]) OR ($callTypes[-1]['name'] = 'Total' AND $callTypes[-1]['count'] = 0);
    		$callTypes[-1]['count'] +=  $uct['0']['count'];
    		$uct['ct']['call_type'] OR $uct['ct']['call_type'] = "None";
    		isset($callTypes[$uct['c']['call_type_id']]) OR ($callTypes[$uct['c']['call_type_id']]['name'] = $uct['ct']['call_type'] AND $callTypes[$uct['c']['call_type_id']]['count'] = 0);
    		$callTypes[$uct['c']['call_type_id']]['count'] += $uct['0']['count'];
    		
    		$uct['u']['name'] = $uct['u']['user'];
    		!empty($uct['u']['name']) OR $uct['u']['name'] = $uct['c']['mobile'];
    		isset($usersCallTypesCounts[$uct['c']['mobile']][-1]) OR ($usersCallTypesCounts[$uct['c']['mobile']][-1]['name'] = $uct['u']['name'] AND $usersCallTypesCounts[$uct['c']['mobile']][-1]['count'] = 0);
    		isset($usersCallTypesCounts[$uct['c']['mobile']][$uct['c']['call_type_id']]) OR $usersCallTypesCounts[$uct['c']['mobile']][$uct['c']['call_type_id']]['count'] = 0;
    		$usersCallTypesCounts[$uct['c']['mobile']][$uct['c']['call_type_id']]['count'] += $uct['0']['count'];
    		$usersCallTypesCounts[$uct['c']['mobile']][-1]['count'] += $uct['0']['count'];
    		
    		if(!array_key_exists(-1, $call_types)) $call_types[-1] = -1;
    		$users[$uct['c']['mobile']] = $uct['c']['mobile'];
    		$call_types[$uct['c']['call_type_id']] = $uct['c']['call_type_id'];
    	}
    	foreach($users as $u){
    		foreach($call_types as $c){
    			if(!isset($usersCallTypesCounts[$u][$c]))
    				$usersCallTypesCounts[$u][$c]['count'] = 0;
    		}
    	}
    	$this->set('callTypes', $callTypes);
    	$this->set('usersCallTypesCounts', $usersCallTypesCounts);
    	$this->set('users', $users);
    	$this->set('call_types', $call_types);
    	
    	if($where_time){
    		$viaCounts = $this->Slaves->query("	select count(*) as count, va.api_flag as via
    				from comments c
    				left join vendors_activations va on va.ref_code = c.ref_code
    				$where_time
    				group by va.api_flag");
    	}
    	else {
    		$viaCounts = $this->Slaves->query("	select sum(cc.count) as count, cc.medium as via 
											from comments_count cc
    										$comments_join
											$where
											group by cc.medium");
    	}	
		foreach($viaCounts as $kvc => $vc){
			$medium = isset($vc['cc']['via']) ? $vc['cc']['via'] : $vc['va']['via'];
			switch($medium){
				case 0:
					$viaCounts[$kvc]['cc']['name'] = "SMS";
					break;
				case 1:
					$viaCounts[$kvc]['cc']['name'] = "API";
					break;
				case 2:
					$viaCounts[$kvc]['cc']['name'] = "USSD";
					break;
				case 3:
					$viaCounts[$kvc]['cc']['name'] = "Android";
					break;
				case 4:
					$viaCounts[$kvc]['cc']['name'] = "Partner";
					break;
				case 5:
					$viaCounts[$kvc]['cc']['name'] = "Java";
					break;
				case 6:
					$viaCounts[$kvc]['cc']['name'] = "";
					break;
				case 7:
					$viaCounts[$kvc]['cc']['name'] = "Windows 7";
					break;
				case 8:
					$viaCounts[$kvc]['cc']['name'] = "Windows 8";
					break;
				case 9:
					$viaCounts[$kvc]['cc']['name'] = "Web";
					break;
				default:
					$viaCounts[$kvc]['cc']['name'] = "None";
					break;
			}
		}
		$this->set('viaCounts', $viaCounts);
		
		if($where_time){
			$vendorsProductsCalls = $this->Slaves->query("	
					select va.vendor_id as vendor_id, va.product_id as product_id, count(*) as count, 
						v.company as vendor, p.name as product
					from comments c
					left join vendors_activations va on va.ref_code = c.ref_code
					left join vendors v on v.id = va.vendor_id
					left join products p on p.id = va.product_id
					$where_time
					group by va.vendor_id, va.product_id");
		}
		else {
			$vendorsProductsCalls = $this->Slaves->query("	select cc.vendor_id as vendor_id, cc.product_id as product_id,
														sum(cc.count) as count, v.company as vendor, p.name as product
														from comments_count cc
														left join vendors v on v.id = cc.vendor_id
														left join products p on p.id = cc.product_id
														$comments_join
														$where
														group by cc.vendor_id, cc.product_id");
		}	
		$vendor_ids = $product_ids = array();
		isset($vendorsProducts[-1][-1]) OR ($vendorsProducts[-1][-1]['name'] = 'Total' AND $vendorsProducts[-1][-1]['count'] = 0);
		foreach($vendorsProductsCalls as $vp){			
			$vendorsProducts[-1][-1]['count'] += $vp['0']['count'];
			
			isset($vp['va']['product_id']) && $vp['cc']['product_id'] = $vp['va']['product_id'];
			isset($vp['va']['vendor_id']) && $vp['cc']['vendor_id'] = $vp['va']['vendor_id'];
			
			isset($vendorsProducts[-1][$vp['cc']['product_id']]) OR ($vendorsProducts[-1][$vp['cc']['product_id']]['name'] = $vp['p']['product'] AND $vendorsProducts[-1][$vp['cc']['product_id']]['count'] = 0);
			$vendorsProducts[-1][$vp['cc']['product_id']]['count'] += $vp['0']['count'];
			
			isset($vendorsProducts[$vp['cc']['vendor_id']][-1]) OR ($vendorsProducts[$vp['cc']['vendor_id']][-1]['name'] = $vp['v']['vendor'] AND $vendorsProducts[$vp['cc']['vendor_id']][-1]['count'] = 0);
			$vendorsProducts[$vp['cc']['vendor_id']][-1]['count'] += $vp['0']['count'];
			
			isset($vendorsProducts[$vp['cc']['vendor_id']][$vp['cc']['product_id']]) OR $vendorsProducts[$vp['cc']['vendor_id']][$vp['cc']['product_id']]['count'] = 0;
			$vendorsProducts[$vp['cc']['vendor_id']][$vp['cc']['product_id']]['count'] += $vp['0']['count'];
			
			$vendor_ids[$vp['cc']['vendor_id']] = $vp['cc']['vendor_id'];
			$product_ids[$vp['cc']['product_id']] = $vp['cc']['product_id'];
		}
		foreach($vendor_ids as $v){
			foreach($product_ids as $p){
				if(!isset($vendorsProducts[$v][$p]))
					$vendorsProducts[$v][$p]['count'] = 0; 
			}
		}
		$vendor_ids[-1] = -1;
		$product_ids[-1] = -1;
		$this->set('vendors', $vendor_ids);
		$this->set('products', $product_ids);
		$this->set('vendorsProducts', $vendorsProducts);
		
		if($where_time){
			$tags = $this->Slaves->query("	select count(c.comments) as count, t.name as tag, t.id as tag_id, t.type as type
										from comments c
										left join taggings t on t.id = c.tag_id
										$vendors_activations_join
										$where_time
										group by c.tag_id");
		}
		else {
			$tags = $this->Slaves->query("	select count(c.comments) as count, t.name as tag, t.id as tag_id, t.type as type
					from comments c
					left join taggings t on t.id = c.tag_id
					$comments_count_join
					$where_c
					$call_type_inclusion
					group by c.tag_id");
		}
		$totalCTagsCount = $totalResTagsCount = $totalRetTagsCount = 0;
		foreach($tags as $tag){
			switch($tag['t']['type']){
				case 'Customer':
					$totalCTagsCount += $tag['0']['count'];
					break;
				case 'Resolution':
					$totalResTagsCount += $tag['0']['count'];
					break;
				case 'Retailer':
					$totalRetTagsCount += $tag['0']['count'];
					break;
				case 'Online Complaint':
					$totalOCTagsCount += $tag['0']['count'];
					break;
			}
		}
		$this->set('tags', $tags);
		$this->set('totalCTagsCount', $totalCTagsCount);
		$this->set('totalResTagsCount', $totalResTagsCount);
		$this->set('totalRetTagsCount', $totalRetTagsCount);
		$this->set('totalOCTagsCount', $totalOCTagsCount);
		$this->set('callTypeId', $_REQUEST['callTypeId']);
		
    	$fromDate = date_format(date_create($fromDate), 'd-m-Y');
    	$toDate = date_format(date_create($toDate), 'd-m-Y');
    	$this->set('fromDate', $fromDate);
    	$this->set('toDate', $toDate);
    }
    		
    function newLead(){
    	if($this->RequestHandler->isPost()){
    		$name = $_REQUEST['name'];
    		$shop_name = $_REQUEST['shop_name'];
    		$email = $_REQUEST['email'];
    		$state = $_REQUEST['state'];
    		$city = $_REQUEST['city'];
    		$fax = null;
    		$messages = $_REQUEST['message'];
    		$phone = $_REQUEST['phone'];
    		//$comment = $_REQUEST['comment'];
    		$date = date('Y-m-d H:i:s');
    		$timestamp = date('Y-m-d H:i:s');
    		$req_by = 'C C Leads';
                $interest = $_REQUEST['interest'];
    		$status = 0;
    		$updated_by = $_SESSION['Auth']['User']['id'];
               
                $this->User->query("insert into leads 
    			(name, shop_name, email, state, city, fax, messages, phone, date, timestamp, req_by, interest, status, updated_by) 
    			values ('$name', '$shop_name', '$email', '$state', '$city', '$fax', '$messages', '$phone', '$date', '$timestamp', '$req_by', '$interest','$status', '$updated_by')");
    		
    		/*$subject = "Pay1 Retailer Merchant Request - from $name ($req_by)";
    		 
    		$body = "
    		</br> From          : $name
    		</br> Email-ID      : $email
    		</br> Contact       : $phone
    		</br> State         : $state
    		</br> City          : $city
    		</br> Interested In : $messages
    		</br> Source        : $req_by
    		</br> Comment       : $comment";
    		
    		$this->General->sendMails($subject,$body,array('sales@mindsarray.com','info@pay1.in'),'mail');
                 */
    		
    		$notif =  "Lead reported. The sales team has been notified.";
    		$this->set('notif', $notif);		
    	}
    }
	
        function pullbackReport($frm=null,$to=null){
		
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
		
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
                
		$result = $this->Slaves->query("SELECT vendors_activations.*, products.name, va.company,vendors.company,"
                . " trans_pullback.pullback_by, trans_pullback.pullback_time, trans_pullback.reported_by,"
                . " trans_pullback.timestamp, users.name from vendors_activations"
                . " inner join trans_pullback on vendors_activations.id = trans_pullback.vendors_activations_id"
                . " inner join products on products.id  = vendors_activations.product_id"
                . " inner join vendors on vendors.id  = vendors_activations.vendor_id"
                . " inner join vendors as va ON va.id = trans_pullback.vendor_id"
                . " left join users on users.id = trans_pullback.pullback_by"
                . " where trans_pullback.date>='$fd' AND trans_pullback.date<='$ft'");
                
                $r_c_temp = array(0);
//                $v_a_temp = array(0);
                foreach($result as $res) {
                    $r_c_temp[] = $res['vendors_activations']['ref_code'];
//                    $v_a_temp[] = $res['vendors_activations']['id'];
                }
                
                $commentsResult = $this->Slaves->query("SELECT * FROM (SELECT c.ref_code, t.name from comments c 
                                    left join taggings t on t.id = c.tag_id
                                    where c.ref_code IN (" . implode(',', $r_c_temp) . ") ORDER BY c.created DESC ) AS tbl GROUP BY tbl.ref_code");
                
                
                $comments = array();
                foreach($commentsResult as $cR) {
                    if($cR['tbl']['ref_code'] != '') {
                        $comments[$cR['tbl']['ref_code']] = $cR['tbl']['name'];
                    }
                }
                
/*************** Code to add Tag Timing ***************/
                
//                $complaints = array();
//                $complaintsResult = $this->Slaves->query("SELECT vendor_activation_id, turnaround_time FROM complaints"
//                        . " WHERE vendor_activation_id IN (" . implode(',', $v_a_temp) . ")");
//                foreach($complaintsResult as $cmR) {
//                    $complaints[$cmR['complaints']['vendor_activation_id']] = $cmR['complaints']['turnaround_time'];
//                }
                
                $this->set('comments', $comments);
//                $this->set('complaints', $complaints);
		$this->set('result', $result);
		$this->set('frm',$frm);
		$this->set('to',$to);
		
		
	}
	
	function manualReversalReport($frm=null,$to=null){	
		
		
		if(!isset($frm))$frm = date('d-m-Y');
		if(!isset($to))$to = date('d-m-Y');
		
		$fdarr = explode("-",$frm);
		$tdarr = explode("-",$to);
		
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
		
		$query = " SELECT vendors_activations.*,vendors.id,vendors.company,products.name,users.name"
				." FROM "
				."vendors_activations"
				." INNER JOIN"
				." vendors ON vendors_activations.vendor_id  = vendors.id"
				." INNER JOIN products ON products.id  = vendors_activations.product_id"
				." INNER JOIN users ON users.id = vendors_activations.complaintNo"
				." WHERE vendors_activations.date >='$fd' AND vendors_activations.date <='$ft'"
				."AND (vendors_activations.complaintNo IS NOT NULL AND  vendors_activations.complaintNo!='' AND vendors_activations.complaintNo!=0)"
				."AND vendors_activations.status IN ('2','3')"
				 ."Order BY vendors_activations.timestamp desc";
		
		$result = $this->Slaves->query($query);
		$this->set('result',$result);
		$this->set('frm',$frm);
		$this->set('to',$to);
		
	}

 	function get_server_diff_by_vendor($vendor=null,$date=null){
       $operator_list = $this->User->query("select id, name from products");
       $sims_details = $this->get_reconsile_opn_clg($date, $vendor);
       $sim_wise_sale_result = $this->get_sim_wise_sale($vendor,$date);
       $sim_reports = $this->get_sorted_sim_wise_data($sim_wise_sale_result, $sims_details,$operator_list);
       //echo "<pre>";print_r($sim_reports);echo "</pre>";
       return $sim_reports;
       //$this->autoRender=false;
   }
   
	
	function tagReport($tagId, $fromDate, $toDate, $executive_mobile, $time, $callTypeId = ''){
            
                if($time == 0) {
                        $time = '';
                }
                if($callTypeId != '' || $callTypeId != 0) {
                    $whr = "c.call_type_id = $callTypeId and";
                }
                
		$fromDate = date_format(date_create($fromDate), 'Y-m-d');
		$toDate = date_format(date_create($toDate), 'Y-m-d');
		
		if($fromDate == $toDate){
			if($time){
				$from_to = explode("_", $time);
				$from_time = $from_to[0];
				$to_time = $from_to[1];
			}
		}
		
		$query_where = $executive_mobile ? " and c.mobile = '$executive_mobile' " : "";
		$time_where = $from_time && $to_time ? " and c.created between '$fromDate $from_time:00:00' and '$toDate $to_time:00:00' " : "";
		
		if($time_where){
			$calls = $this->Slaves->query("	select r.user_id, u2.name, u.mobile, c.ref_code, v.company, p.name, va.amount, c.created,
					va.status, va.timestamp, r.mobile, r.shopname, va.api_flag
					from comments c
					left join vendors_activations va on va.ref_code = c.ref_code
					left join vendors v on v.id = va.vendor_id
					left join products p on p.id = va.product_id
					left join users u on u.id = c.users_id
					left join users u2 on u2.mobile = c.mobile
					left join retailers r on r.id = c.retailers_id
					where c.tag_id = $tagId and $whr c.date between '$fromDate' and '$toDate'
					$query_where
					$time_where
					order by c.created desc");
		}
		else {
			$calls = $this->Slaves->query("	select r.user_id, u2.name, u.mobile, c.ref_code, v.company, p.name, va.amount, c.created,
					va.status, va.timestamp, r.mobile, r.shopname, cc.medium
					from comments c
					left join comments_count cc on c.ref_code = cc.ref_code and c.date = cc.date
					left join vendors v on v.id = cc.vendor_id
					left join products p on p.id = cc.product_id
					left join users u on u.id = c.users_id
					left join vendors_activations va on va.ref_code = c.ref_code
					left join users u2 on u2.mobile = c.mobile
					left join retailers r on r.id = c.retailers_id
					where c.tag_id = $tagId and $whr c.date between '$fromDate' and '$toDate'
					$query_where
					order by c.created desc");
		}
                
                $temp_calls = array();
                foreach($calls as $call) {
                        $call['r']['user_id'] != '' && $temp_calls[] = $call['r']['user_id'];
                }
                
                $device = $this->Slaves->query("select * from (select user_id, device_type from user_profile"
                        . " where user_id IN (" . implode(',', $temp_calls) . ") order by updated desc) as tb1 group by user_id");
                
                $device_type = array();
                foreach($device as $d_t) {
                        if($d_t != '') 
                                $device_type[$d_t['tb1']['user_id']] = $d_t['tb1']['device_type'];
                }
                $this->set('device_type', $device_type);
		
		if(count($calls) > 0){
			$tag = $this->Slaves->query("select name from taggings where id = $tagId");
			$fromDate = date_format(date_create($fromDate), 'd-m-Y');
			$toDate = date_format(date_create($toDate), 'd-m-Y');
			
			foreach($calls as $k=>$d){
				if($time_where)
					$calls[$k]['cc']['medium'] = $d['va']['api_flag'];
				$status = '';
				if($d['va']['status'] == '0'){
					$status = 'In Process';
				}else if($d['va']['status'] == '1'){
					$status = 'Successful';
				}else if($d['va']['status'] == '2'){
					$status = 'Failed';
				}else if($d['va']['status'] == '3'){
					$status = 'Reversed';
				}else if($d['va']['status'] == '4'){
					$status = 'Reversal In Process';
				}else if($d['va']['status'] == '5'){
					$status = 'Reversal declined';
				}
				$calls[$k]['va']['status'] = $status;
			}
			
			$this->set("tag", $tag);
			$this->set("from_date", $fromDate);
			$this->set("to_date", $toDate);
			$this->set("calls", $calls);
			$this->set('medium_map', $this->api_medium_map);
			$this->set('from_time', $from_time);
			$this->set('to_time', $to_time);
		}
		else
			echo "No calls were tagged";	 
	}
	
	function retailerRegistrationReport($fromDate, $toDate){
		$this->layout = 'sims';
		
		if(empty($fromDate) || empty($toDate)){
			$fromDate = $toDate = date("d-m-Y", time());
		}
		$from_date = date("Y-m-d", strtotime($fromDate));
		$to_date = date("Y-m-d", strtotime($toDate));
		$retailers = $this->Slaves->query("select r.id, r.user_id, r.mobile, r.name, r.email, r.created, r.area, r.area_id, r.balance,
				max(l.date) as last_sale_date, l.sale
				from retailers r
				left join retailers_logs l on r.id = l.retailer_id
				where r.created between '$from_date' and date_add('$to_date', interval 1 day)
				and r.retailer_type = 3
				group by r.id
				order by r.created desc");
		$this->set('count', count($retailers));
		$this->set('retailers', $retailers);
		$this->set('fromDate', $fromDate);
		$this->set('toDate', $toDate);
	}
	
	function getProcessTime($frm_date=null,$to_date=null,$frm_time=null,$to_time=null){
		
		if($frm_date == null || $to_date == null || $frm_time == null || $to_time == null){
			$frm_date = date('Y-m-d');
			$to_date = date('Y-m-d');
			$frm_time = (date('H')-1).':00:00';
			$to_time = (date('H')).':00:00';
                } else {
                        $frm_time = str_replace('.', ':', $frm_time.'.00');
                        $to_time = str_replace('.', ':', $to_time.'.00');
                }
	
		$frm = $frm_date.' '.$frm_time;
		$to = $to_date.' '.$to_time;
		
		$getModemProcessingTime = $this->Slaves->query("SELECT "
				. "vendors_activations.product_id,products.name,products.benchmark_failure,products.benchmark_processtime,count(*) as totalcount, vendors_activations.timestamp,Avg(TIME_TO_SEC(TIMEDIFF(vendors_transactions.processing_time,vendors_activations.timestamp))) AS processtime "
				. "FROM "
				. "vendors_activations "
				. "LEFT JOIN "
				. "vendors_transactions ON (vendors_transactions.ref_id =  vendors_activations.ref_code and vendors_activations.vendor_id = vendors_transactions.vendor_id) "
				. "INNER JOIN products ON (products.id = vendors_activations.product_id)"
				. " inner join vendors on (vendors.id = vendors_activations.vendor_id)"
				. " WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and vendors.update_flag='1' "
				. "GROUP BY "
				. "product_id"
				);


                       $getApiProcessingTime = $this->Slaves->query("SELECT vendors_activations.product_id,products.name,products.benchmark_failure,products.benchmark_processtime,count(*) as totalcount,AVG(TIME_TO_SEC(TIMEDIFF(vendors_messages.timestamp,vendors_activations.timestamp))) AS processtime FROM "
				. " vendors_activations INNER JOIN products on (products.id = vendors_activations.product_id) "
				. " LEFT JOIN vendors_messages on (vendors_activations.ref_code = vendors_messages.shop_tran_id) "
				. " inner join vendors on (vendors.id = vendors_activations.vendor_id)"
				. " WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and vendors.update_flag = 0 "
				. " GROUP BY "
				. "vendors_activations.product_id");
		


                        $percentageModems = $this->Slaves->query("SELECT "
				. "vendors_activations.product_id,products.name,vendors_activations.ref_code,vendors_activations.status, vendors_activations.timestamp,Avg(TIME_TO_SEC(TIMEDIFF(vendors_transactions.processing_time,vendors_activations.timestamp))) AS processtime "
				. "FROM "
				. "vendors_activations "
				. "LEFT JOIN "
				. "vendors_transactions ON (vendors_transactions.ref_id =  vendors_activations.ref_code and vendors_activations.vendor_id = vendors_transactions.vendor_id) "
				.  "LEFT JOIN vendors on vendors.id = vendors_activations.vendor_id "
				. "INNER JOIN products ON (products.id = vendors_activations.product_id)"
				. " WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and vendors.update_flag = 1  "
				. "GROUP BY "
				. "vendors_activations.ref_code");
		
	
		
		
		$getModemFaliure  = $this->Slaves->query("SELECT product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and status IN('2','3') and vendors.update_flag = '1' group by product_id");
		
		$getApiFaliure  = $this->Slaves->query("SELECT product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and status IN('2','3') and vendors.update_flag = '0' group by product_id");
		
		$getTotalModemCount = $this->Slaves->query("SELECT product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' and vendors.update_flag = '1' group by product_id");
		
		$getTotalApiCount  = $this->Slaves->query("SELECT product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date'  and vendors.update_flag = '0' group by product_id");
		
		$getOverallFaliure = $this->Slaves->query("Select product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to' and vendors_activations.date between '$frm_date' and '$to_date' AND vendors_activations.status IN ('2','3') group by product_id");
		
		$getOverallCount = $this->Slaves->query("Select product_id,count(*) as totalcount FROM `vendors_activations` inner join vendors on vendors.id = vendors_activations.vendor_id WHERE vendors_activations.timestamp between '$frm' and '$to'  and vendors_activations.date between '$frm_date' and '$to_date' group by product_id");
		
		$modemProcessTime = array();
		
		foreach ($getModemFaliure as $val) {

			$faliure[$val['vendors_activations']['product_id']]['modemfaliure'] = $val[0]['totalcount'];
		}
		foreach ($getApiFaliure as $val) {

			$faliure[$val['vendors_activations']['product_id']]['apifaliure'] = $val[0]['totalcount'];
		}

		foreach ($getTotalModemCount as $val) {

			$faliure[$val['vendors_activations']['product_id']]['modemcount'] = $val[0]['totalcount'];
		}
		foreach ($getTotalApiCount as $val) {

			$faliure[$val['vendors_activations']['product_id']]['apicount'] = $val[0]['totalcount'];
		}

		foreach ($getOverallFaliure as $val) {

			$faliure[$val['vendors_activations']['product_id']]['overallfaliure'] = $val[0]['totalcount'];
		}
		foreach ($getOverallCount as $val) {

			$faliure[$val['vendors_activations']['product_id']]['overaallcount'] = $val[0]['totalcount'];
		}
		

		
		foreach ($getModemProcessingTime as $val) {
			
			$modemProcessTime[$val['vendors_activations']['product_id']]["modemprocesstime"] = array("oprname" => $val['products']['name'], "processtime" => $val[0]['processtime'],"totatcount" => $val[0]['totalcount']);
			$modemProcessTime[$val['vendors_activations']['product_id']][] = array("benchmark_failure" => $val['products']['benchmark_failure'],"benchmark_processtime" => $val['products']['benchmark_processtime']);
		}
		
		foreach ($getApiProcessingTime as $val) {
			
			$modemProcessTime[$val['vendors_activations']['product_id']]["apiprocesstime"] =  array("oprname" => $val['products']['name'], "processtime" => $val[0]['processtime'],"totatcount" => $val[0]['totalcount']);
			if(!isset($modemProcessTime[$val['vendors_activations']['product_id']][0]['benchmark_failure'])){
			$modemProcessTime[$val['vendors_activations']['product_id']][] = array("benchmark_failure" => $val['products']['benchmark_failure'],"benchmark_processtime" => $val['products']['benchmark_processtime']);
			}
		}
		
		$data = array();
		
		foreach ($percentageModems as $val) {
			
			if(isset($val[0]['processtime']) && $val['vendors_activations']['status']!='2' && $val['vendors_activations']['status']!='3') {
			
			if ($val[0]['processtime'] <= 40) {

					$data[$val['vendors_activations']['product_id']]['0-40'][] = $val['vendors_activations']['ref_code'];
					
				} else if ($val[0]['processtime'] > 40 && $val[0]['processtime'] <= 60) {
				
					$data[$val['vendors_activations']['product_id']]['40-60'][] = $val['vendors_activations']['ref_code'];
					
				} else if ($val[0]['processtime'] > 60 && $val[0]['processtime'] <= 90) {
				
					$data[$val['vendors_activations']['product_id']]['60-90'][] = $val['vendors_activations']['ref_code'];
				} 
				else {
				
					$data[$val['vendors_activations']['product_id']]['90-100'][] = $val['vendors_activations']['ref_code'];
				}
			}
		}
		
		$this->set("processtime",$modemProcessTime);
		$this->set("data",$data);
		$this->set('frm',$frm_date);
		$this->set('to',$to_date);
		$this->set('frm_time',explode(':',$frm_time));
		$this->set('to_time',explode(':',$to_time));
		$this->set('faliure',$faliure);

	}
		
	function failureInfo(){
                    
		$productArray = array();
		$datestr = '';
		$frmstr = '';
		$timerangestr = '';
		$oprStr = '';
		$status = '';
		$vendorquery = '';
		
		if(isset($_REQUEST['date']) && !empty($_REQUEST['date'])){
			$date = $_REQUEST['date'];
                    $datestr = "AND vendors_activations.date = '".$date."'"; 
		}
		if(isset($_REQUEST['frm']) && isset($_REQUEST['to'])){
			$frm = $_REQUEST['frm'];
			$to = $_REQUEST['to'];
			$frm_time=date('Y-m-d H:i:s',strtotime($date.' '.$frm));
                        $to_time=date('Y-m-d H:i:s',strtotime($date.' '.$to));
			$frmstr = "AND vendors_activations.timestamp between  '$frm_time' and '$to_time'"; 
		}
		
		if(isset($_REQUEST['timerange']) && !empty($_REQUEST['timerange'])){
			$timerange = $_REQUEST['timerange'];
			
			$time_range = explode("-",$timerange);
			
			if(count($time_range)>1){
				
			if($time_range[0]=='90'){
				$timerangestr = "having  processtime > 90";
				$timerange = 'more than 90';
			} else {
			$timerangestr = "having  processtime between $time_range[0] and $time_range[1]";
			}
			
			}
		}
		
		if(isset($_REQUEST['oprId']) && !empty($_REQUEST['oprId'])){
			
			$oprId = $_REQUEST['oprId'];
			
			$oprStr = "And vendors_activations.product_id= ".$oprId;
		}
		
		if(isset($_REQUEST['type']) && !empty($_REQUEST['type'])){
			
			$status = " AND vendors_activations.status IN ('2','3')";
			
			if($_REQUEST['type']=='modem'){
				$vendorquery = " AND vendors.update_flag = '1' AND vendors.active_flag='1' ";
			} else if($_REQUEST['type']=='api') {
				$vendorquery = " AND vendors.update_flag = '0' AND vendors.active_flag='1' ";
			}
			
		} else{
			$status = " AND vendors_activations.status IN ('0','1')";
		}
		
		if(isset($_REQUEST['transtype']) && !empty($_REQUEST['transtype'])){
			
			if($_REQUEST['transtype']=='modem'){
				
				$vendorquery = " AND vendors.update_flag = '1' AND vendors.active_flag='1' ";
			} else if($_REQUEST['transtype']=='api') {
				
				$vendorquery = " AND vendors.update_flag = '0' AND vendors.active_flag='1' ";
			}
			
		}
		
		$failure = $this->Slaves->query("SELECT va.product_id,va.vendor_id,vm.response,vm.status,va.ref_code,va.status,hour(va.timestamp)  as hour,date(va.timestamp)  as cdate"
                                                . "   from vendors_messages as vm "
                                                . "  left join vendors_activations as va on (va.ref_code = vm.shop_tran_id and vm.service_vendor_id = va.vendor_id)"
                                                . " where va.date = '$date' and  va.status IN (2,3) and  va.timestamp between '$frm_time' and '$to_time' and  vm.status =  'failure' and va.product_id =".$oprId );
			
			
		
		foreach ($failure as $val):
		
		if (($val['va']['status'] == '2' || $val['va']['status'] == '3')) {

				 $failureArray[$val[0]['cdate']][$val[0]['hour']][] = $val;

				 $failurearraydate[$val[0]['cdate']][] = $val;
				
				 if (strpos($val['vm']['response'], 'Manual reversal') !== false ){
			
			      $val['vm']['response'] = 'Manual reversal';
			
		          }
				  else if (strpos($val['vm']['response'], '24 :: Error of connection') !== false ){
			
			      $val['vm']['response'] = 'error_connection';
			
		          }
				 
				  if(!isset($refcode[$val['va']['ref_code']])){
				
					   $refcode[$val['va']['ref_code']] = $val['va']['ref_code'];
				
				       $failureType[$val[0]['cdate']][$val[0]['hour']][$val['vm']['response']][] = $val;
				  }
					  
				//  $errorType[str_replace(',','',$val['vm']['response'])] = str_replace(',','',$val['vm']['response']);
				  
				  $errorType[$val['vm']['response']] = $val['vm']['response'];
				 
			}
		endforeach;
		
		
		
		$alltransdata = $this->Slaves->query("SELECT "
				. "vendors_activations.product_id,vendors_activations.vendor_id,vendors_activations.mobile,vendors_activations.vendor_refid,vendors_activations.ref_code,"
				. "vendors_activations.amount,vendors_activations.status,vendors_activations.shop_transaction_id,vendors_activations.timestamp,"
				. "hour(vendors_activations.timestamp)  as hour,date(vendors_activations.timestamp)  as cdate,"
				. "vendors_activations.shop_transaction_id,products.name,vendors.company,products.benchmark_failure,products.benchmark_processtime,vendors_messages.internal_error_code,"
				. "vendors_activations.timestamp,Avg(TIME_TO_SEC(TIMEDIFF(vendors_transactions.processing_time,vendors_activations.timestamp))) AS processtime, "
				. "retailers.shopname,retailers.mobile,vendors.update_flag,vendors.active_flag "
				. "FROM "
				. "vendors_activations "
				. "LEFT JOIN "
				. "vendors_transactions ON (vendors_transactions.ref_id =  vendors_activations.ref_code) "
				. "INNER JOIN products ON (products.id = vendors_activations.product_id) "
				. " LEFT JOIN retailers ON (retailers.id = vendors_activations.retailer_id)"
				. " LEFT JOIN vendors on vendors.id = vendors_activations.vendor_id "
				. " LEFT JOIN vendors_messages ON  (vendors_activations.ref_code = vendors_messages.shop_tran_id and vendors_activations.vendor_id = vendors_messages.service_vendor_id) "
				. " WHERE vendors_activations.date = '$date' AND  vendors_activations.timestamp between '$frm_time' and '$to_time' $oprStr  $vendorquery"
				. "GROUP BY vendors_activations.ref_code "
				
				);

		$transdata = $this->Slaves->query("SELECT * FROM (SELECT t.*, vendors_messages.timestamp as update_time FROM (SELECT "
				. "vendors_activations.product_id,vendors_activations.mobile,vendors_activations.vendor_refid,vendors_activations.ref_code,vendors_activations.amount,vendors_activations.status,vendors_activations.shop_transaction_id,vendors_activations.timestamp,products.name,vendors.company,products.benchmark_failure,products.benchmark_processtime, vendors_messages.response, Avg(TIME_TO_SEC(TIMEDIFF(vendors_transactions.processing_time,vendors_activations.timestamp))) AS processtime, retailers.shopname,retailers.mobile as retailer_mobile,mobile_numbering_area.area_name "
				. "FROM "
				. "vendors_activations "
				. "LEFT JOIN vendors_transactions ON (vendors_transactions.ref_id =  vendors_activations.ref_code) "
				. "INNER JOIN products ON (products.id = vendors_activations.product_id) "
				. " LEFT JOIN retailers ON (retailers.id = vendors_activations.retailer_id)"
				. " LEFT JOIN vendors on vendors.id = vendors_activations.vendor_id "
                                . " LEFT JOIN vendors_messages ON  (vendors_activations.ref_code = vendors_messages.shop_tran_id and vendors_activations.vendor_id = vendors_messages.service_vendor_id and vendors_messages.status='failure') "
				. " LEFT JOIN mobile_operator_area_map on (mobile_operator_area_map.number = LEFT(vendors_activations.mobile, 5))"
                                . " LEFT JOIN mobile_numbering_area on (mobile_numbering_area.area_code = mobile_operator_area_map.area)"
                                . " WHERE vendors_activations.date = '$date' AND  vendors_activations.timestamp between '$frm_time' and '$to_time' $oprStr $status $vendorquery"
				. "GROUP BY vendors_transactions.ref_id "
				. " $timerangestr "
				. ") as t LEFT JOIN vendors_messages ON (t.ref_code = vendors_messages.shop_tran_id) ORDER BY vendors_messages.timestamp DESC) as z GROUP BY z.ref_code"
				);
                
                $transdata_temp = array();
                $i = 0;
                foreach($transdata as $td) {
                    $transdata_temp[$i]['vendors_activations']      = array('product_id'=>$td['z']['product_id'],'mobile'=>$td['z']['mobile'],'vendor_refid'=>$td['z']['vendor_refid'],'ref_code'=>$td['z']['ref_code'],'amount'=>$td['z']['amount'],'status'=>$td['z']['status'],'shop_transaction_id'=>$td['z']['shop_transaction_id'],'timestamp'=>$td['z']['timestamp']);
                    $transdata_temp[$i]['products']                 = array('name'=>$td['z']['name'],'benchmark_failure'=>$td['z']['benchmark_failure'],'benchmark_processtime'=>$td['z']['benchmark_processtime']);
                    $transdata_temp[$i]['vendors']                  = array('company'=>$td['z']['company']);
                    $transdata_temp[$i]['vendors_messages']         = array('response'=>$td['z']['response'],'update_time'=>$td['z']['update_time']);
                    $transdata_temp[$i][0]                          = array('processtime'=>$td['z']['processtime']);
                    $transdata_temp[$i]['retailers']                = array('shopname'=>$td['z']['shopname'],'mobile'=>$td['z']['retailer_mobile']);
                    $transdata_temp[$i++]['mobile_numbering_area']    = array('area_name'=>$td['z']['area_name']);
                }
                
                $transdata = $transdata_temp;
                
		if(isset($_REQUEST['timerange']) && !empty($_REQUEST['timerange']) || $_REQUEST['type']=='all'){
		
		foreach ($alltransdata as $val) {
				//failure data
		

			$datearray[$val[0]['cdate']] = $val[0]['cdate'];
			
			$totalcount[$val[0]['cdate']][$val[0]['hour']][] = $val;
			
			$totalcountdate[$val[0]['cdate']][] = $val;
			
			$modemType[$val['vendors_activations']['vendor_id']] = $val['vendors']['company'];
			
			$hourarray[$val[0]['hour']] = $val[0]['hour'];
				//success data with processtime,daywiseprocesstime
			if (($val['vendors_activations']['status'] != '2' && $val['vendors_activations']['status'] != '3') && ($val['vendors']['update_flag'] == '1') && ($val['vendors']['active_flag'] == '1')) {

				if ($val[0]['processtime'] <= 40) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['0-40'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['0-40'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['0-40'][] = $val['vendors_activations']['ref_code'];
					
				} else if ($val[0]['processtime'] > 40 && $val[0]['processtime'] <= 60) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['40-60'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['40-60'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['30-60'][] = $val['vendors_activations']['ref_code'];
				} else if ($val[0]['processtime'] > 60 && $val[0]['processtime'] <= 90) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['60-90'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['60-90'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['60-90'][] = $val['vendors_activations']['ref_code'];
				} else {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['90-100'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['90-100'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['90-100'][] = $val['vendors_activations']['ref_code'];
				}

				
			}
			
			if($val['vendors']['update_flag']=='1'&& $val['vendors']['active_flag']='1'){
				$successcount[$val[0]['cdate']][$val[0]['hour']][] = $val;

				$sucesscountdatearray[$val[0]['cdate']][] = $val;
			}
		}
		
		foreach ($datearray as $datval) {
			foreach ($hourarray as $hourval) {
					foreach ($modemType as $modkey => $modval) {
				$data[$datval][$hourval] = isset($failureArray[$datval][$hourval]) ? round(count($failureArray[$datval][$hourval]) / count($totalcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['0-40'] = isset($processstimearray[$datval][$hourval]['0-40']) ? round(count($processstimearray[$datval][$hourval]['0-40']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['40-60'] = isset($processstimearray[$datval][$hourval]['40-60']) ? round(count($processstimearray[$datval][$hourval]['40-60']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['60-90'] = isset($processstimearray[$datval][$hourval]['60-90']) ? round(count($processstimearray[$datval][$hourval]['60-90']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['90-100'] = isset($processstimearray[$datval][$hourval]['90-100']) ? round(count($processstimearray[$datval][$hourval]['90-100']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				
				
				if (isset($modemCount[$datval][$hourval][$modkey]['0-40'])) {
							$modemProcessTime[$datval][$hourval]['0-40'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['0-40']) / count($processstimearray[$datval][$hourval]['0-40']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['0-40']['success'] = $processTimedata[$datval][$hourval]['0-40'];
							$modemProcessTime[$datval][$hourval]['0-40']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['0-40']);
						}  if (isset($modemCount[$datval][$hourval][$modkey]['40-60'])) {
							
							$modemProcessTime[$datval][$hourval]['40-60'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['40-60']) / count($processstimearray[$datval][$hourval]['40-60']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['40-60']['success'] = $processTimedata[$datval][$hourval]['40-60'];
							$modemProcessTime[$datval][$hourval]['40-60']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['40-60']);
							
						}  if (isset($modemCount[$datval][$hourval][$modkey]['60-90'])) {

							$modemProcessTime[$datval][$hourval]['60-90'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['60-90']) / count($processstimearray[$datval][$hourval]['60-90']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['60-90']['success'] = $processTimedata[$datval][$hourval]['60-90'];
							$modemProcessTime[$datval][$hourval]['60-90']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['60-90']);
							
						}  if (isset($modemCount[$datval][$hourval][$modkey]['90-100'])) {

							$modemProcessTime[$datval][$hourval]['90-100'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['90-100']) / count($processstimearray[$datval][$hourval]['90-100']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['90-100']['success'] = $processTimedata[$datval][$hourval]['90-100'];
							$modemProcessTime[$datval][$hourval]['90-100']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['90-100']);
						}
				
				}
				foreach ($errorType as $errkey => $errval){
				if(isset($failureType[$datval][$hourval][$errval])){
				$failureTypepercentage[$datval][$hourval][$errval] = round(count($failureType[$datval][$hourval][$errval]) / count($totalcount[$datval][$hourval]) * 100, 2);
				
				$failureTypepercentage[$datval][$hourval]['failure'] = $data[$datval][$hourval];
                                $failureTypepercentage[$datval][$hourval]['totalCount'] = count($totalcount[$datval][$hourval]);
                                $failureTypepercentage[$datval][$hourval]['totalFail'] = count($failureType[$datval][$hourval][$errval]);
				}
			}
			}

                        $dateWiseFailuredata[$datval][] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$failuredata[] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$totalfailuredata[$datval] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['0-40'] = round(count($daywiseProcesstime[$datval]['0-40']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['40-60'] = round(count($daywiseProcesstime[$datval]['40-60']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['60-90'] = round(count($daywiseProcesstime[$datval]['60-90']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['90-100'] = round(count($daywiseProcesstime[$datval]['90-100']) / count($sucesscountdatearray[$datval]) * 100, 2);
		}
		
		
                
		foreach ($data as $key => $val) {
			foreach ($val as $k => $v) {
				$data1[$key]['name'] = $key;
				$data1[$key]['data'][] = $v;
				$processarray[$key]['name'] = $key;
				$processarray[$key]['data']['0-40'][] = $processTimedata[$key][$k]['0-40'];
				$processarray[$key]['data']['40-60'][] = $processTimedata[$key][$k]['40-60'];
				$processarray[$key]['data']['60-90'][] = $processTimedata[$key][$k]['60-90'];
				$processarray[$key]['data']['90-100'][] = $processTimedata[$key][$k]['90-100'];
				$dateprocesstime[$key]['name'] = $key;
				$dateprocesstime[$key]['data']['0'] = $dateWiseProcesstimedata[$key]['0-40'];
				$dateprocesstime[$key]['data']['1'] = $dateWiseProcesstimedata[$key]['40-60'];
				$dateprocesstime[$key]['data']['2'] = $dateWiseProcesstimedata[$key]['60-90'];
				$dateprocesstime[$key]['data']['3'] = $dateWiseProcesstimedata[$key]['90-100'];
				

			}
		}
		
		foreach ($data1 as $val) {
			$data2[] = $val;
		}
		
		$data6 = array();
		foreach ($processarray as $val) {
			$data3['0-40'][] = array("name" => $val['name'], "data" => $val['data']['0-40']);
			$data3['40-60'][] = array("name" => $val['name'], "data" => $val['data']['40-60']);
			$data3['60-90'][] = array("name" => $val['name'], "data" => $val['data']['60-90']);
			$data3['90-100'][] = array("name" => $val['name'], "data" => $val['data']['90-100']);
		}
		
		foreach ($data3 as $key => $val) {
			$data4[$key] = $val;
		}
		
		}
		
		$productInfo = $this->Slaves->query("Select * from products ");
		
		foreach ($productInfo as $val){
			
			$productArray[$val['products']['id']] = $val['products']['name'];
		}
		

		$this->set('failuredata',$transdata);
		$this->set('products',$productArray);
		$this->set('timerange',isset($timerange) ? $timerange : "");
		$this->set('oprId',$oprId);
		$this->set('data2', $data2);
		$this->set('hourarray', $hourarray);
		$this->set('fromDate', $frmdate);
		$this->set('toDate', $todate);
		$this->set('processtimedata', $data4);
		$this->set('frm',$frm);
		$this->set('to',$to);
		$this->set('date',$date);
		$this->set('oprId',$oprId);
		$this->set('dayFailure',isset($countFailuredata) ? $countFailuredata : " ");
		$this->set('datearray',$datearray);
		$this->set('errortype',$errorType);
		$this->set('failurepercen',$failureTypepercentage);
		$this->set('modemSuccesspercent',json_encode($modemProcessTime));
		$this->set('totalfailure',json_encode($totalfailuredata));
		$this->set('type',isset($_REQUEST['type']) ? $_REQUEST['type'] : "");
		
		
		
	}
	
	function graphReport(){
		
		$this->layout = 'sims';
		
		if(isset($_REQUEST['date'])){
			$date = $_REQUEST['date'];
		} else {
			$date = date('Y-m-d');
		}
		
		$oprId = '';
		$frm = '';
		$to = '';
		$todate = $date;
		$hourquery = '';
	    $frmdate = date("Y-m-d", strtotime("-2 day", strtotime($todate)));

		
		if(isset($_REQUEST['oprId'])){
			
			$oprId = $_REQUEST['oprId'];
		}
		if(isset($_REQUEST['frm'])){
			
			$frm = $_REQUEST['frm'];
		}
		if(isset($_REQUEST['to'])){
			
			$to = $_REQUEST['to'];
		}
		
		if(isset($_REQUEST['frm']) && isset($_REQUEST['to'])){
			$hourquery =  " AND HOUR(vendors_activations.timestamp) between '$frm' and '$to'";
		}
		
		if(isset($_REQUEST['type']) && $_REQUEST['type']=='weekwise'){
			
			$frmdate = date("Y-m-d", strtotime("-6 day", strtotime($todate)));
			$type = $_REQUEST['type'];
											
		} 
		
		$failureArray = array();
		
		$transarray = array();
		
		$totalcount = array();
		
		$hourarray = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23);
		
		$datearray = array();
		
		$processstimearray = array();
		
		$failurearraydate = array();
		
		$totalcountdate = array();
		
		$dayWiseFailuredata = array();
		
		$sucesscountdatearray = array();
		
		$failureType = array();
		
		$failureTypepercentage = array();
		
		$modemCount = array();
		
		$modemType  = array();
		
		$processarray = array();
		
		$dateprocesstime = array();
		
		$processTimedata = array();

		$dateWiseFailuredata = array();

		$dateWiseProcesstimedata = array();
		
	     while (strtotime($frmdate) <= strtotime($todate)) { 
			
						
		
			$failure = $this->Slaves->query("SELECT va.product_id,va.vendor_id,vm.response,vm.status,va.ref_code,va.status,hour(va.timestamp)  as hour,date(va.timestamp)  as cdate"
											. "   from vendors_messages as vm "
											. "  left join vendors_activations as va on (va.ref_code = vm.shop_tran_id and vm.service_vendor_id = va.vendor_id)"
											. " where va.date = '$frmdate' and  va.status IN (2,3) and vm.status =  'failure' and va.product_id =".$oprId);
			
			
		
		foreach ($failure as $val):
		
		if (($val['va']['status'] == '2' || $val['va']['status'] == '3')) {

				 $failureArray[$val[0]['cdate']][$val[0]['hour']][] = $val;

				 $failurearraydate[$val[0]['cdate']][] = $val;
				
				 if (strpos($val['vm']['response'], 'Manual reversal') !== false ){
			
			      $val['vm']['response'] = 'Manual reversal';
			
		          }
				  else if (strpos($val['vm']['response'], '24 :: Error of connection') !== false ){
			
			      $val['vm']['response'] = 'error_connection';
			
		          }
				 
				  if(!isset($refcode[$val['va']['ref_code']])){
				
					   $refcode[$val['va']['ref_code']] = $val['va']['ref_code'];
				
				       $failureType[$val[0]['cdate']][$val[0]['hour']][$val['vm']['response']][] = $val;
				  }
					  
				  $errorType[$val['vm']['response']] = $val['vm']['response'];
				 
			}
		endforeach;
		
		
		$result = $this->Slaves->query(" SELECT vendors_activations.product_id,count(*) as totalcount,vendors_activations.status,vendors_activations. vendor_id,vendors_activations.ref_code,vendors_activations.timestamp,Avg(TIME_TO_SEC(TIMEDIFF(vendors_transactions.processing_time,vendors_activations.timestamp))) AS processtime,vendors.company,hour(vendors_activations.timestamp)  as hour,date(vendors_activations.timestamp)  as cdate,vendors.update_flag,vendors.active_flag,vendors_transactions.status,vendors_messages.internal_error_code  "
										. " FROM "
										. " vendors_activations"
										. " LEFT JOIN "
										. " vendors_transactions "
										. " ON (vendors_transactions.ref_id = vendors_activations.ref_code and vendors_activations.vendor_id = vendors_transactions.vendor_id)"
										. " LEFT JOIN vendors_messages ON  (vendors_activations.ref_code = vendors_messages.shop_tran_id and vendors_activations.vendor_id = vendors_messages.service_vendor_id) "
										. " LEFT JOIN vendors "
										. " ON (vendors_activations.vendor_id = vendors.id) "
										. " WHERE"
										. " vendors_activations.date ='$frmdate'"
				                        . " $hourquery"
										. " AND vendors_activations.product_id = '".$oprId."'"
										. "  group by vendors_activations.ref_code order by vendors_activations.date asc "
				                       );
		
		
		
		foreach ($result as $val) {
				//failure data
			
			$datearray[$val[0]['cdate']] = $val[0]['cdate'];
			
			$totalcount[$val[0]['cdate']][$val[0]['hour']][] = $val;
			
			$totalcountdate[$val[0]['cdate']][] = $val;
			
			$modemType[$val['vendors_activations']['vendor_id']] = $val['vendors']['company'];
			
			
				//success data with processtime,daywiseprocesstime
			if (($val['vendors_activations']['status'] != '2' && $val['vendors_activations']['status'] != '3') && ($val['vendors']['update_flag'] == '1') && ($val['vendors']['active_flag'] == '1')) {

				if ($val[0]['processtime'] <= 40) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['0-40'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['0-40'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['0-40'][] = $val['vendors_activations']['ref_code'];
					
				} else if ($val[0]['processtime'] > 40 && $val[0]['processtime'] <= 60) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['40-60'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['40-60'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['40-60'][] = $val['vendors_activations']['ref_code'];
				} else if ($val[0]['processtime'] > 60 && $val[0]['processtime'] <= 90) {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['60-90'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['60-90'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['60-90'][] = $val['vendors_activations']['ref_code'];
				} else {

					$processstimearray[$val[0]['cdate']][$val[0]['hour']]['90-100'][] = $val['vendors_activations']['ref_code'];

					$daywiseProcesstime[$val[0]['cdate']]['90-100'][] = $val['vendors_activations']['ref_code'];
					
					$modemCount[$val[0]['cdate']][$val[0]['hour']][$val['vendors_activations']['vendor_id']]['90-100'][] = $val['vendors_activations']['ref_code'];
				}

				
			}
			if(($val['vendors']['update_flag'] == '1') && ($val['vendors']['active_flag'] == '1')){
				
				$successcount[$val[0]['cdate']][$val[0]['hour']][] = $val;

				$sucesscountdatearray[$val[0]['cdate']][] = $val;
				
			}
		}
		  $frmdate = date ("Y-m-d", strtotime("+1 day", strtotime($frmdate)));
		  
		
		 }
		 
		//calculation of percentage of modem

		foreach ($datearray as $datval) {
			foreach ($hourarray as $hourval) {
				foreach ($errorType as $errkey => $errval){
					foreach ($modemType as $modkey => $modval) {

				$data[$datval][$hourval] = isset($failureArray[$datval][$hourval]) ? round(count($failureArray[$datval][$hourval]) / count($totalcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['0-40'] = isset($processstimearray[$datval][$hourval]['0-40']) ? round(count($processstimearray[$datval][$hourval]['0-40']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['40-60'] = isset($processstimearray[$datval][$hourval]['40-60']) ? round(count($processstimearray[$datval][$hourval]['40-60']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['60-90'] = isset($processstimearray[$datval][$hourval]['60-90']) ? round(count($processstimearray[$datval][$hourval]['60-90']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				$processTimedata[$datval][$hourval]['90-100'] = isset($processstimearray[$datval][$hourval]['90-100']) ? round(count($processstimearray[$datval][$hourval]['90-100']) / count($successcount[$datval][$hourval]) * 100, 2) : 0;
				if(isset($failureType[$datval][$hourval][$errval])){
				$failureTypepercentage[$datval][$hourval][$errval] = round(count($failureType[$datval][$hourval][$errval]) / count($totalcount[$datval][$hourval]) * 100, 2);
				
				$failureTypepercentage[$datval][$hourval]['failure'] = $data[$datval][$hourval];
                                $failureTypepercentage[$datval][$hourval]['totalCount'] = count($totalcount[$datval][$hourval]);
                                $failureTypepercentage[$datval][$hourval]['totalFail'] = count($failureType[$datval][$hourval][$errval]);
				}
				
				if (isset($modemCount[$datval][$hourval][$modkey]['0-40'])) {
							$modemProcessTime[$datval][$hourval]['0-40'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['0-40']) / count($processstimearray[$datval][$hourval]['0-40']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['0-40']['success'] = $processTimedata[$datval][$hourval]['0-40'];
							$modemProcessTime[$datval][$hourval]['0-40']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['0-40']);
						}  if (isset($modemCount[$datval][$hourval][$modkey]['40-60'])) {
							
							$modemProcessTime[$datval][$hourval]['40-60'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['40-60']) / count($processstimearray[$datval][$hourval]['40-60']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['40-60']['success'] = $processTimedata[$datval][$hourval]['40-60'];
							$modemProcessTime[$datval][$hourval]['40-60']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['40-60']);
							
						}  if (isset($modemCount[$datval][$hourval][$modkey]['60-90'])) {

							$modemProcessTime[$datval][$hourval]['60-90'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['60-90']) / count($processstimearray[$datval][$hourval]['60-90']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['60-90']['success'] = $processTimedata[$datval][$hourval]['60-90'];
							$modemProcessTime[$datval][$hourval]['60-90']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['60-90']);
							
						}  if (isset($modemCount[$datval][$hourval][$modkey]['90-100'])) {

							$modemProcessTime[$datval][$hourval]['90-100'][$modval] = round(count($modemCount[$datval][$hourval][$modkey]['90-100']) / count($processstimearray[$datval][$hourval]['90-100']) * 100, 2);
							$modemProcessTime[$datval][$hourval]['90-100']['success'] = $processTimedata[$datval][$hourval]['90-100'];
							$modemProcessTime[$datval][$hourval]['90-100']['count'][$modval] = count($modemCount[$datval][$hourval][$modkey]['90-100']);
						}
				
				}
			}
			}
		
			$dateWiseFailuredata[$datval][] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$failuredata[] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$totalfailuredata[$datval] = round(count($failurearraydate[$datval]) / count($totalcountdate[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['0-40'] = round(count($daywiseProcesstime[$datval]['0-40']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['40-60'] = round(count($daywiseProcesstime[$datval]['40-60']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['60-90'] = round(count($daywiseProcesstime[$datval]['60-90']) / count($sucesscountdatearray[$datval]) * 100, 2);
			$dateWiseProcesstimedata[$datval]['90-100'] = round(count($daywiseProcesstime[$datval]['90-100']) / count($sucesscountdatearray[$datval]) * 100, 2);
		}
		
		
		foreach ($data as $key => $val) {
			foreach ($val as $k => $v) {
				$data1[$key]['name'] = $key;
				$data1[$key]['data'][] = $v;
				$processarray[$key]['name'] = $key;
				$processarray[$key]['data']['0-40'][] = $processTimedata[$key][$k]['0-40'];
				$processarray[$key]['data']['40-60'][] = $processTimedata[$key][$k]['40-60'];
				$processarray[$key]['data']['60-90'][] = $processTimedata[$key][$k]['60-90'];
				$processarray[$key]['data']['90-100'][] = $processTimedata[$key][$k]['90-100'];
				$dateprocesstime[$key]['name'] = $key;
				$dateprocesstime[$key]['data']['0'] = $dateWiseProcesstimedata[$key]['0-40'];
				$dateprocesstime[$key]['data']['1'] = $dateWiseProcesstimedata[$key]['40-60'];
				$dateprocesstime[$key]['data']['2'] = $dateWiseProcesstimedata[$key]['60-90'];
				$dateprocesstime[$key]['data']['3'] = $dateWiseProcesstimedata[$key]['90-100'];
				

			}
		}
		
		foreach ($data1 as $val) {
			$data2[] = $val;
		}
		
		$data6 = array();
		foreach ($processarray as $val) {
			$data3['0-40'][] = array("name" => $val['name'], "data" => $val['data']['0-40']);
			$data3['40-60'][] = array("name" => $val['name'], "data" => $val['data']['40-60']);
			$data3['60-90'][] = array("name" => $val['name'], "data" => $val['data']['60-90']);
			$data3['90-100'][] = array("name" => $val['name'], "data" => $val['data']['90-100']);
		}
		
		foreach ($data3 as $key => $val) {
			$data4[$key] = $val;
		}
		
		if ($_REQUEST['type'] == 'weekwise') {

			foreach ($dateWiseFailuredata as $key => $val) {
				$dayWiseFailuredata[$key]['name'] = $key;
				$dayWiseFailuredata[$key]['data'] = $failuredata;
			}
			foreach ($dateprocesstime as $key => $val) {
				$data6[] = $val;
			}
			
			foreach ($dayWiseFailuredata as $val){
				
				$countFailuredata[] = $val;
			}
		}
		
		$this->set('data2', $data2);
		$this->set('hourarray', $hourarray);
		$this->set('fromDate', $frmdate);
		$this->set('toDate', $todate);
		$this->set('processtimedata', $data4);
		$this->set('frm',$frm);
		$this->set('to',$to);
		$this->set('date',$date);
		$this->set('oprId',$oprId);
		$this->set('dayFailure',isset($countFailuredata) ? $countFailuredata : " ");
		$this->set('datearray',$datearray);
		$this->set('type',isset($type) ? $type : "");
		$this->set('data6',$data6);
		$this->set('errortype',$errMsg);
		$this->set('failurepercen',$failureTypepercentage);
		$this->set('modemSuccesspercent',json_encode($modemProcessTime));
		$this->set('totalfailure',json_encode($totalfailuredata));
	}
	
	function exceedComplainDetails($id,$fdate,$tdate,$ftime,$ttime){
		
		if (isset($fdate) && isset($tdate) && isset($ftime) && isset($ttime)) {
                    $fd = $fdate;
                    $td = $tdate;
                    $ft = str_replace('.', ':', $ftime);
                    $tt = str_replace('.', ':', $ttime);

                } else {
                    $fd = date('Y-m-d');
                    $td = date('Y-m-d');
                    $ft = "00:00:00";
                    $tt = "23:59:59";
                }
                
                $key = "";
                if($id != 'total') {
                    $key = "vendors_activations.vendor_id ='$id' AND";
                }
		
		$query = "SELECT vendors_activations.*,products.name,vendors.company,vendors.id,CONCAT_WS( ' ', resolve_date,resolve_time) as resolvetime,turnaround_time"
                        . " FROM "
                        . " complaints LEFT JOIN"
                        . " vendors_activations "
                        . " on complaints.vendor_activation_id = vendors_activations.id "
                        . " inner join products on products.id = vendors_activations.product_id "
                        . " inner join vendors on vendors.id = vendors_activations.vendor_id "
                        ." WHERE $key complaints.in_date between '$fd' AND '$td' AND complaints.in_time between '$ft' AND '$tt' group by vendors_activations.id order by vendors_activations.date desc";
		
		$getexceedresult = $this->Slaves->query($query);
		
		foreach($getexceedresult as $val):
			
			if(strtotime($val[0]['resolvetime'])>strtotime($val['complaints']['turnaround_time'])):
		
				$exceedresult[] = $val;
		
			endif;
			
		endforeach;
		
		$this->set('exceedresult',$exceedresult);
	}
	
	function retailers() {
		$fromDateV = isset($_POST['fromDateV']) ? $_POST['fromDateV'] : "";
		$toDateV = isset($_POST['toDateV']) ? $_POST['toDateV'] : "";
		$fromDateD = isset($_POST['fromDateD']) ? $_POST['fromDateD'] : "";
		$toDateD = isset($_POST['toDateD']) ? $_POST['toDateD'] : "";
		
		$retailers_query = "select r.*, ur.*, rks.*, max(rks.document_timestamp) as d_date, max(rks.verified_timestamp) as v_date,
					group_concat(rks.verified_state) as gvs, group_concat(rks.document_state) as gds, group_concat(rs.service_id) as service_ids
				from retailers r
				left join unverified_retailers ur on ur.retailer_id = r.id
				left join retailers_kyc_states rks on rks.retailer_id = r.id
				left join retailers_services rs on rs.retailer_id = r.id
				where 1";

		$trained = isset ( $_POST ['trained'] ) ? $_POST ['trained'] : "";
		$distributor_id = $_POST ['distributor_id'] ? $_POST ['distributor_id'] : "";
		$search_term = isset ( $_POST ['search_term'] ) ? trim ( $_POST ['search_term'] ) : "";
		$document_state = isset ( $_POST ['document_state'] ) ? $_POST ['document_state'] : "";
		$verified_state = isset ( $_POST ['verified_state'] ) ? $_POST ['verified_state'] : "";
		
		if ($trained === "0" || $trained) {
			$retailers_query .= " and r.trained = $trained ";
		}
		if ($distributor_id) {
			$retailers_query .= " and r.parent_id = $distributor_id ";
		}
		if ($search_term) {
			$retailers_query .= " and (r.mobile like '%" . $search_term . "%' or ur.name like '%" . $search_term . "%'
									or ur.shopname like '%" . $search_term . "%') ";
		}
		
		$retailers_having_query = array();
		if(!empty($fromDateV) && !empty($toDateV)){
			$fdarr = explode("-", $fromDateV);
			$tdarr = explode("-", $toDateV);
			$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
			$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
			$retailers_having_query[] = " max(rks.verified_date) between '$fd' and '$ft' ";
		}
		else {
			$fromDateV = $toDateV = "";
		}
		
		if(!empty($fromDateD) && !empty($toDateD)){
			$fdarr = explode("-", $fromDateD);
			$tdarr = explode("-", $toDateD);
			$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
			$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
				
			$retailers_having_query[] = " max(rks.document_date) between '$fd' and '$ft' ";
		}
		else {
			$fromDateD = $toDateD = "";
		}
		
		if($verified_state === "0"){
			$retailers_having_query[] = " avg(rks.verified_state) = 0 ";
		}
		else if($verified_state == 1){
			$retailers_having_query[] = " group_concat(rks.verified_state) like '%1%' 
										and group_concat(rks.verified_state) not like '1,1,1'";
		}
		else if($verified_state == 2){ 
			$retailers_having_query[] = " group_concat(rks.verified_state) like '1,1,1' ";
		}
		
		if($document_state === "0"){
			$retailers_having_query[] = " group_concat(rks.document_state) like '%0%' ";
		}
		else if($document_state == 1){
			$retailers_having_query[] = " group_concat(rks.document_state) like '%1%' ";
		}
		
		$retailers_having_query = implode(" and ", $retailers_having_query);
		
		if($retailers_having_query){
			$retailers_having_query = " having $retailers_having_query";
		}
		
		$retailers_query .= " 	group by r.id 
								$retailers_having_query
								order by max(rks.document_date) desc, r.modified desc ";
	
		$distributors = $this->Slaves->query ( "select distributors.id, distributors.company, users.mobile
												from distributors, users
												WHERE users.id = distributors.user_id
												order by company" );
		$distributor_mobile = array ();
		foreach ( $distributors as $d ) {
			$distributor_mobile [$d ['distributors'] ['id']] = $d ['users'] ['mobile'];
		}
		
		$memcache_options = array(
			'memcache_key' => "retailers_kyc_panel",
			'memcache_duration' => "3600"	
		);
		$retailers = $this->paginate_query ($retailers_query, 100, $memcache_options);	
		
		$this->set ( 'retailers', $retailers );
		$this->set ( 'distributors', $distributors );
		$this->set ( 'verify_flag', $verify_flag );
		$this->set ( 'trained', $trained );
		$this->set ( 'distributor_id', $distributor_id );
		$this->set ( 'search_term', $search_term );
		$this->set ( 'distributor_mobile', $distributor_mobile );
		$this->set('fromDateD', $fromDateD);
		$this->set('toDateD', $toDateD);
		$this->set('fromDateV', $fromDateV);
		$this->set('toDateV', $toDateV);
		$this->set('verified_state', $verified_state);
		$this->set('document_state', $document_state);
		
		$this->layout = 'sims';
	}
	
	/*function retailers2() {
		$fromDate = isset($_POST['fromDate']) ? $_POST['fromDate'] : "";
		$toDate = isset($_POST['toDate']) ? $_POST['toDate'] : "";
	
		$retailers_query = "select r.*, ur.*, max(rd.created) as max_created
				from retailers r
				left join unverified_retailers ur on ur.retailer_id = r.id
				left join retailers_details rd on rd.retailer_id = r.id
				where 1";
	
		// $page = $_POST['page'] ? $_POST['page'] : 1;
		$verify_flag = isset ( $_POST ['verify_flag'] ) ? $_POST ['verify_flag'] : 2;
		$trained = isset ( $_POST ['trained'] ) ? $_POST ['trained'] : "";
		$distributor_id = $_POST ['distributor_id'] ? $_POST ['distributor_id'] : "";
		$search_term = isset ( $_POST ['search_term'] ) ? trim ( $_POST ['search_term'] ) : "";
		// $offset = ($page - 1) * 100;
	
		if ($verify_flag === "0" || in_array ( $verify_flag, array (
				1,
				2,
				-1
		) )) {
			$retailers_query .= " and r.verify_flag = $verify_flag ";
		}
		if ($trained === "0" || $trained) {
			$retailers_query .= " and r.trained = $trained ";
		}
		if ($distributor_id) {
			$retailers_query .= " and r.parent_id = $distributor_id ";
		}
		if ($search_term) {
			$retailers_query .= " and (r.mobile like '%" . $search_term . "%' or r.name like '%" . $search_term . "%'
									or r.shopname like '%" . $search_term . "%') ";
		}
		$retailers_having_query = "";
		if(!empty($fromDate) && !empty($toDate)){
			$fdarr = explode("-", $fromDate);
			$tdarr = explode("-", $toDate);
			$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
			$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
				
			$retailers_having_query = " having max(rd.created) between '$fd' and date_add('$ft', interval 1 day) ";
		}
		else {
			$fromDate = $toDate = "";
		}
		$retailers_query .= " 	group by r.id
		$retailers_having_query
		order by max(rd.created) desc ";
		// $limit_query = " limit $offset, 100 ";
	
		// $retailers = $this->Slaves->query($retailers_query.$limit_query);
		// $total_count = count($this->Slaves->query($retailers_query));
	
		$distributors = $this->Slaves->query ( "	select distributors.id, distributors.company, users.mobile
												from distributors, users
												WHERE users.id = distributors.user_id
												order by company" );
		$distributor_mobile = array ();
		foreach ( $distributors as $d ) {
			$distributor_mobile [$d ['distributors'] ['id']] = $d ['users'] ['mobile'];
		}
	
		$retailers = $this->paginate_query ( $retailers_query );
	
		foreach($retailers as $r){
			if($retailers[0]['r']['verify_flag'] != 1){
				foreach($retailers[0]['ur'] as $key => $row){
					if(!in_array($key, array('id', 'rental_flag')))
						$retailers[0]['r'][$key] = $retailers[0]['ur'][$key];
				}
					
				$retailers[0]['r']['shopname'] = $retailers[0]['r']['shop_name'];
				$retailers[0]['r']['pin'] = $retailers[0]['r']['pin_code'];
			}
		}
	
		$this->set ( 'retailers', $retailers );
		$this->set ( 'distributors', $distributors );
		// $this->set('page', $page);
		// $this->set('total_count', $total_count);
		// $this->set('total_pages', ceil($total_count / 100));
		$this->set ( 'verify_flag', $verify_flag );
		$this->set ( 'trained', $trained );
		$this->set ( 'distributor_id', $distributor_id );
		$this->set ( 'search_term', $search_term );
		$this->set ( 'distributor_mobile', $distributor_mobile );
		$this->set('fromDate', $fromDate);
		$this->set('toDate', $toDate);
	
		$this->layout = 'sims';
	}*/
	
	/**
	 * Retailer Trial And Verification (RTV) State Machine
	 * ===================================================
	 * 
	 * verify_flag state values
	 * 0 => Unverified and documents not submitted
	 * 1 => Documents submitted and verfied
	 * 2 => Documents submitted and unverified
	 * 
	 * trial_flag state values
	 * 0 => Full-time retailer, verify_flag = [0, 1, 2] (states possible)
	 * 1 => Unverified retailer in trial period, verify_flag = [0, 2]
	 * 2 => Trial period ended and retailer unverified, verify_flag = [0, 2]
	 * 
	 * ----------------------------------------------------
	 * 
	 * RTV state codes
	 * code	verify_flag	trial_flag				value
	 * 	00		0			0		Full-time retailer, unverified, documents not submitted
	 * 	10		1			0		Full-time retailer, verified
	 * 	20		2			0		Full-time retailer, documents submitted (pending verification), unverified
	 * 	01		0			1		Unverified retailer on trial, documents not submitted
	 * 	11		1			1		<State not permissible>
	 * 	21		2			1		Unverified retailer on trial, documents submitted (pending verification)
	 * 	02		0			2		Suspened retailer, trial period ended, unverified, documents not submitted
	 * 	12		1			2		<State not permissible>
	 * 	22		2			2		Suspened retailer, trial period ended, unverified, documents submitted (pending verification)
	 * 
	 * -----------------------------------------------------
	 * 
	 * 		current_codes			verify_event	result_code			action and comment
	 * [00, 01, 02, 11, 12]				0				<NA>			<Event not permissible>
	 * 			10						0				00			Verification fault, retailer has to submit new documents
	 * 			20						0				00			Verification failed, retailer has to submit new documents
	 * 			21						0				01			Verification failed, retailer has to submit new documents
	 * 			22						0				02			Verification failed, retailer has to submit new documents
	 * [00, 01, 02, 10, 11, 12]			1				<NA>			<Event not permissible>
	 * 			20						1				10			Full-time retailer verified, copy unverified details to retailers table
	 * 			21						1				10			Trial to full-time retailer verified, copy unverified details to retailers table
	 * 			22						1				10			Suspended to full-time retailer, copy unverified details to retailers table
	 * 		[00, 10]					2				20			Full-time retailer, documents submitted (pending verification)
	 * 	[20, 11, 21, 12, 22]			2				<NA>			<Event not permissible>
	 * 			01						2				21			Retailer on trial, documents submitted (pending verification)
	 * 			02						2				22			Suspended retailer, trial period ended, unverified, documents submitted (pending verification)
	 * 		
	 */
	
	function setVerifyFlag(){
		if(isset($_POST['verify_flag']) && $_POST['retailer_id']){
			$retailer_id = $_POST['retailer_id'];
			$verify_event = $_POST['verify_flag'];
			
			if(in_array($verify_event, array("0", "2"))){
				$this->User->query("update retailers
								set verify_flag = ".$verify_event.",
								modified = '".date('Y-m-d H:i:s')."'
								where id = ".$retailer_id);
				echo "true";
			}
			else if($verify_event == 1){
				$verify_flag = $this->General->update_verify_flag($retailer_id);
				if($verify_flag == 1){
					echo "true";
				}
				else {
					echo "false";
				}
			}
		}
		else 
			echo "false";
		exit;
	}
	
	function toggleTrained(){
		$this->autoRender = false;
		$retailer_id = $_POST['retailer_id'];
		if($retailer_id){
			$retailers = $this->User->query("select * from retailers r where id = $retailer_id ");
			if($retailers){
				$this->User->query("update retailers
						set trained = ".(1 - $retailers[0]['r']['trained']).",
						modified = '".date('Y-m-d H:i:s')."'
						where id = $retailer_id ");
				echo "true";
			}
			else
				echo "false"; 
		}
		else 
			echo "false";
		exit;
	}
	
	function retailerVerification($retailer_id){
		if($retailer_id){
			if(isset($_POST['verify_flag'])){
				$retailer = array();
				isset($_POST['shopname']) AND $retailer['shopname'] = $_POST['shopname'];
				isset($_POST['name']) AND $retailer['name'] = $_POST['name']; 
// 				isset($_POST['pincode']) AND $retailer['pin'] = $_POST['pincode'];
// 				isset($_POST['address']) AND $retailer['address'] = $_POST['address'];
				isset($_POST['shop_type']) AND $retailer['shop_type'] = $_POST['shop_type'];
				isset($_POST['shop_type_value']) AND $retailer['shop_type_value'] = $_POST['shop_type_value'];
				isset($_POST['location_type']) AND $retailer['location_type'] = $_POST['location_type'];
				
				if(!empty($retailer)){
					if($_POST['verify_flag'] == 1){
						$update_query = "update retailers set ";
						foreach($retailer as $k => $r){
							$update_query .= " $k = '$r', ";
						}
						$update_query .= " modified = '".date('Y-m-d H:i:s')."'
    						where id = ".$retailer_id;
					}
					else {
						$update_query = "update unverified_retailers set ";
						foreach($retailer as $k => $r){
							$update_query .= " $k = '$r', ";
						}
						$update_query .= " modified = '".date('Y-m-d H:i:s')."'
    						where retailer_id = ".$retailer_id;
					}
					$this->User->query($update_query);
					$retailers = $this->User->query("select * from retailers
							where id = ".$retailer_id);
					$this->General->updateRetailerAddress($retailer_id, $retailers['0']['retailers']['user_id'], $_POST);
				}
				
				foreach($_FILES as $document_type => $file){
					if(!empty($file['name'][0])){
						$this->Shop->removeDocument($retailer_id, $_POST['section_id'], $_POST['verify_flag']);
						App::import('Controller', 'Shops');
						$ShopsController = new ShopsController;
						$ShopsController->constructClasses();
						
						$ShopsController->uploadImages($document_type, $document_type . "_" . $retailer_id);
						$this->Shop->setKYCState($retailer_id, $_POST['section_id'], 0);
					}	
				}
				$this->redirect("/panels/retailerVerification/".$retailer_id);
			}
			
			$retailers = $this->User->query("select r.*, ur.*, up.latitude, up.longitude,
						a.name, c.name, s.name, ua.name, uc.name, us.name, u.mobile, d.name,
						group_concat(rs.service_id) as service_ids
	    			from retailers r
	    			left join unverified_retailers ur on ur.retailer_id = r.id
					left join retailers_services rs on rs.retailer_id = r.id
					left join distributors d on d.id = r.parent_id
					left join users u on u.id = d.user_id
					left join user_profile up on up.user_id = r.user_id and up.device_type = 'online'
					left join locator_area a ON a.id = r.area_id
					left join locator_city c ON c.id = a.city_id
					left join locator_state s ON s.id = c.state_id 
					left join locator_area ua ON ua.id = ur.area_id
					left join locator_city uc ON uc.id = ua.city_id
					left join locator_state us ON us.id = uc.state_id
	    			where r.id = ".$retailer_id."
					group by r.id");
			$retailer = $retailers[0];
			$retailer_images = $this->User->query("select * from retailers_details rd
					left join users u on u.id = rd.uploader_user_id
					left join groups g on g.id = u.group_id 
					where rd.retailer_id = ".$retailer_id);
			$retailer['images'] = $retailer_images;
			
			$retailer_verified_images = $this->User->query("select * from retailers_docs rd
					left join users u on u.id = rd.uploader_user_id
					left join groups g on g.id = u.group_id 
					where rd.retailer_id = ".$retailer_id);
			$retailer['verified_images'] = $retailer_verified_images;
			
			$retailer_kyc_states = $this->User->query("select * from retailers_kyc_states rks
					where retailer_id = ".$retailer_id);
			$retailer['kyc_states'] = $retailer_kyc_states;
			
			$this->set('retailer', $retailer);
			
			$shop_types = $this->Shop->retailerTypes();
			$location_types = $this->Shop->locationTypes();
			
			$this->set('shop_types', $shop_types);
			$this->set('location_types', $location_types);
		}
		$this->layout = 'sims';
	}
	
	function verifySection(){
		$this->autoRender = false;
		$retailer_id = $_POST['retailer_id'];
		$section_id = $_POST['section_id'];
		
		$retailer_before_score_change = $this->User->query("select * 
				from retailers r
				where r.id = ".$retailer_id);
		$this->Shop->setKYCState($retailer_id, $section_id, 2);
		$retailer_after_score_change = $this->User->query("select *
				from retailers r
				where r.id = ".$retailer_id);
		
		$message = "";
		if($retailer_before_score_change[0]['r']['kyc_score'] < 100 && $retailer_after_score_change[0]['r']['kyc_score'] == 100){
			$message = "Congratulations! You can now use our toll free calling.";
		}
		else if($retailer_before_score_change[0]['r']['kyc_score'] + $retailer_after_score_change[0]['r']['kyc_score'] == 200){
			$message = "You are a Verified Pay1 Retail Partner now. Customers will be able to locate you easily.";
		}
		if($message){
			$this->General->sendMessage($retailer_after_score_change[0]['r']['mobile'], $message, 'notify');
		}
	}
	
	function rejectSection(){
		$this->autoRender = false;
		$retailer_id = $_POST['retailer_id'];
		$section_id = $_POST['section_id'];
		$reason = $_POST['reason'];
		
		$this->Shop->setKYCState($retailer_id, $section_id, 1, $reason);
		
		$retailers = $this->User->query("select *
						from retailers r
						where r.id = ".$retailer_id);
		$message = "Your information has been rejected because: $reason. Kindly update details to enjoy toll free calling.";
		$this->General->sendMessage($retailers[0]['r']['mobile'], $message, 'notify');
	}
	
	function verifyDocuments(){
		$this->autoRender = false;
		$retailer_id = $_POST['retailer_id'];
		$document_type = $_POST['document_type'];
		if(!empty($retailer_id) && in_array($document_type, array('idProof', 'addressProof', 'shop'))){
			$retailer_documents = $this->User->query("select * from retailers_details
					where retailer_id = ".retailer_id."
					and type = '".$document_type."'");
			if(!empty($retailer_documents)){
				$this->User->query("delete from retailers_details
						where verify_flag = 1
						and retailer_id = ".retailer_id."
						and type = '".$document_type."'");
				if($this->User->query("update retailers_details
						set verify_flag = 1
						where verify_flag = 0 
						and retailer_id = ".retailer_id."
						and type = '".$document_type."'")){
						$this->General->update_verify_flag($retailer_id);
						echo "done";
				}
				else 
					echo "Could not update documents.";
			}
			else 
				echo "No documents on record to verify.";
		}
		else 
			echo  "Proper parameters not provided";
		exit;
	}
	
	function rejectDocument(){
		$this->autoRender = false;
		$retailer_id = $_POST['retailer_id'];
		$document_type = $_POST['document_type'];
		$reason = $_POST['reason'];
		
		if(!empty($retailer_id) && in_array($document_type, array('idProof', 'addressProof', 'shop')) && $reason){
			$retailer_documents = $this->User->query("select * from retailers_details
					where retailer_id = ".$retailer_id."
					and type = '".$document_type."'
					and verify_flag = 0");
			if(!empty($retailer_documents)){
				$this->User->query("update retailers_details
					set verify_flag = -1,
					comment = '".$reason."'	
					where retailer_id = ".$retailer_id."
					and type = '".$document_type."'
					and verify_flag = 0");
				$this->User->query("update unverified_retailers
					set documents_submitted = -1,
					modified = '".date('Y-m-d H:i:s')."'	
					where retailer_id = ".$retailer_id);
				$this->User->query("update retailers
					set verify_flag = -1,
					modified = '".date('Y-m-d H:i:s')."'	
					where id = ".$retailer_id);
				
				$retailers = $this->User->query("select * from retailers r where r.id = ".$retailer_id);
				$message = "Your KYC (".$document_type." photo ) was rejected. Reason: $reason
				Kindly, upload appropriate documents.";
				$this->General->sendMessage($retailers[0]['r']['mobile'], $message, 'notify');
					
				echo "done";
			}
			else
				echo "No documents on record to verify.";
		}
		else
			echo "Provide a proper reason for rejection";
			exit;
	}
	
	function deleteDocument(){
		$this->autoRender = false;
		$src = $_POST['src'];
		$reason = $_POST['reason'];
		
		if($src && $reason){
			$response = $this->Shop->deleteDocument($src);
			$retailers = $this->User->query("select rd.*, r.* 
					from retailers_details rd
					join retailers on rd.retailer_id = r.id
					where image_name like '$src'");
			$this->User->query("update unverified_retailers
					set documents_submitted = -1,
					modified = '".date('Y-m-d H:i:s')."'
					where retailer_id = ".$retailers[0]['r']['id']);
			$kyc_score = $this->General->kyc_level($retailers[0]['r']['id']);
			if($kyc_score < 1){
				$this->User->query("update retailers
						set verify_flag = 0,
						modified = '".date('Y-m-d H:i:s')."'
						where id = ".$retailers[0]['r']['id']);
			}
//			$message = "Your KYC (".$retailers[0]['rd']['type']." photo ) was unverified. Reason: $reason  
//					Kindly, upload appropriate documents.";
                        
                        $paramdata['RETAILERS_TYPE'] = $retailers[0]['rd']['type'];
                        $paramdata['REASON'] = $reason;
                        $MsgTemplate = $this->General->LoadApiBalance(); 
                        $content=  $MsgTemplate['Retailer_DeleteKYCDocs_MSG'];
                        $message = $this->General->ReplaceMultiWord($paramdata,$content);
                        
			$this->General->sendMessage($retailers[0]['r']['mobile'], $message, 'notify');
			
			echo $response;
		}
		else 
			echo "Provide proper url for the image"; 
		exit;	
	}
	
	function vendorsCommissions(){
		$this->layout = 'sims';
// 		$results_per_page = 10;
	
	        $vendor_id = $_POST['vendor_id'] ? $_POST['vendor_id'] : "";
		$product_id = $_POST['product_id'] ? $_POST['product_id'] : "";
	
// 		$page = $_POST['page'] ? $_POST['page'] : 1;
// 		$offset = ($page - 1) * $results_per_page;
	
// 		$limit_query = " limit $offset, $results_per_page ";
	
// 		$vendors = $this->Slaves->query("select id, company from vendors where show_flag = 1");
// 		$products = $this->Slaves->query("select id, name from products where to_show = 1");
		$vendors = $this->Slaves->query("select id, company from vendors");
                $products = $this->Slaves->query("select id, name from products");
                $circles = $this->Slaves->query("select area_code as id, area_name from mobile_numbering_area");
                
                if($vendor_id.$product_id){
			$vendors_commissions_query = "select vc.is_deleted,vc.id, v.id, p.id, v.company, p.name, vc.discount_commission, vc.active, vc.cap_per_min, vc.tat_time,
	                        vc.oprDown, vc.circle, vc.circles_yes, vc.circles_no, vc.timestamp, vc.updated_by, u.name,if(v.update_flag=0 and machine_id=0,1,0) as is_api 
	                    from vendors_commissions as vc
	                    LEFT JOIN vendors v on (vc.vendor_id = v.id)
	                    LEFT JOIN products p on (vc.product_id = p.id)
						LEFT JOIN users u on u.id = vc.updated_by 
                                               where 1";
	           //         where vc.is_deleted=0";

                        if($vendor_id){
				$vendors_commissions_query .= " and v.id = ".$vendor_id;
				$this->set("vendor_id", $vendor_id);
			}
			if($product_id){
				$vendors_commissions_query .= " and p.id = ".$product_id;
				$this->set("product_id", $product_id);
			}
	// 		$total_count = count($this->User->query($vendors_commissions_query));
			$vendors_commissions = $this->Slaves->query($vendors_commissions_query);
		}
		$this->set("vendors", $vendors);
		$this->set("products", $products);
		$this->set("circles", $circles);
		$this->set("vendors_commissions", $vendors_commissions);
	
// 		$this->set('page', $page);
// 		$this->set('total_count', $total_count);
// 		$this->set('total_pages', ceil($total_count / $results_per_page));
	}
	
       	function saveVendorCommission($vc_id){
		$vendor_id = $_POST['add_vendor_id'];
		$product_id = $_POST['add_product_id'];
		$circle_code = $_POST['circle_id'];
		$circles_yes = $_POST['cy'];
		$circles_no = $_POST['cn'];
 		$discount_commission = $_POST['is_api']=='0'?$_POST['discount_commission']:0;
		$tat_time = $_POST['tat_time'] > 0 ? $_POST['tat_time'] : 0;
		$cap_per_min = $_POST['cap_per_min'];
		$user_id = $_SESSION['Auth']['User']['id'];
		
		if($vc_id){
                    
                                                        $sql="update vendors_commissions set vendor_id = '$vendor_id',product_id = '$product_id',circle = '$circle_code',"
                                                                . " circles_yes = '$circles_yes',circles_no = '$circles_no',tat_time = '$tat_time',cap_per_min = '$cap_per_min',updated_by = '$user_id' ";
                                                        
                                                       $sql.=$_POST['is_api']=='0'? ",discount_commission='$discount_commission' ":" ";
                                          
                                                       $sql .= " where id = $vc_id";
                                                        
			$this->User->query($sql);
		}
		else {
			$this->User->query("insert into vendors_commissions
						(vendor_id, product_id, circle, circles_yes, circles_no,tat_time,timestamp, updated_by)
				values ('$vendor_id', '$product_id', '$circle_code', '$circles_yes', '$circles_no','$tat_time', '".date('Y-m-d H:i:s')."', '$user_id')");
		}
		exit;
	}
      
    function apiRecon($date = null) {
        $this->layout = 'sims';
        $date = empty($_REQUEST['date']) ? date("Y-m-d") : $_REQUEST['date'];
        $serverstatusQuery = '';
        $vendorstatusQuery = '';
        $vendorQuery = '';
        if ($_REQUEST['status'] == 'All' || (!isset($_REQUEST['status']))) {

            $serverstatusQuery = "AND `api_transactions`.server_status IN ('success','failure','pending')";

            $vendorstatusQuery = "AND `api_transactions`.vendor_status IN ('success','failure','pending')";
        } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {
            $serverstatusQuery = "AND `api_transactions`.server_status = '{$_REQUEST['status']}'";

            $vendorstatusQuery = "AND `api_transactions`.vendor_status = '{$_REQUEST['status']}'";
        }

        $vendor_activation_cond = "";
        if (isset($_REQUEST['vendor']) && !empty($_REQUEST['vendor'])) {
            $vendorQuery = "AND `api_transactions`.vendor_id = '{$_REQUEST['vendor']}'";
        }
        $qry = "SELECT `api_transactions`.*, vendors.id, vendors.company, vendors.shortForm, vendors_activations.vendor_refid, vendors_activations.status FROM `api_transactions` "
                . " LEFT JOIN vendors ON (vendors.id = api_transactions.vendor_id ) "                
                . " LEFT JOIN vendors_activations ON (api_transactions.txn_id = vendors_activations.ref_code and api_transactions.date = vendors_activations.date and api_transactions.vendor_id = vendors_activations.vendor_id ) "
                . "WHERE `api_transactions`.date='$date' AND vendors_activations.date = '$date' $vendorstatusQuery "
                . "  $serverstatusQuery AND `api_transactions`.vendor_status != `api_transactions`.server_status $vendorQuery ";

        $api_result = $this->Slaves->query($qry);
        $apiVendors = $this->Slaves->query("Select vendors.id,vendors.company from vendors where update_flag = '0'");
        
        $status_mapping = array("Inprocess","Success","Failed","Reverse","Reversal in process","Reversal declined");
                
        $this->set('apiVendors', $apiVendors);
        $this->set('date', $date);
        $this->set('status_map', $status_mapping);
        $this->set('apiResult', $api_result);
        $this->set('vendorId', isset($_REQUEST['vendor']) ? $_REQUEST['vendor'] : "");
        $this->set('status', isset($_REQUEST['status']) ? $_REQUEST['status'] : "");
    }
    
    function apiReconSuccessTxn($id){
    	$this->User->query("UPDATE api_transactions SET vendor_status='success',flag=0 WHERE id = $id");
    	$this->autoRender = false;
    }
    
function inprocessReport(){

		 $frmdate = isset($_REQUEST['frmdate']) ? $_REQUEST['frmdate'] : date('Y-m-d ');
		 $todate = isset($_REQUEST['frmdate']) ? $_REQUEST['frmdate'] : date('Y-m-d ');
                
//		$date1 = date_create($frmdate);
//		$date2 = date_create($todate);
//		$diff=date_diff($date1,$date2);
//		$days = $diff->format("%a");
                
                $from_time = isset($_REQUEST['from_time']) ? $_REQUEST['from_time'] : '';
                $to_time = isset($_REQUEST['to_time']) ? $_REQUEST['to_time'] : '';
                $Modem_flag = isset($_REQUEST['modem_flag']) ? $_REQUEST['modem_flag'] : 1; 
                $API_flag = isset($_REQUEST['api_flag']) ? $_REQUEST['api_flag'] : 1;
                
                $frm = '';
		$to = '';
//                if($days>=2){
//			$todate = date("Y-m-d", strtotime("+2 day", strtotime($frmdate)));
//		}
                
                //If Both Modem_flag and API_flag are checked i.e. $Modem_flag & $API_flag = 1
                if(($Modem_flag) && ($API_flag)){
                  $API_Modem_Condition = '';
                }else{
                    if($Modem_flag){
                      $API_Modem_Condition =  "and vendors.update_flag = 1";   //(If MODEM than vendors.update_flag = 1 )
                    }else if($API_flag){
                      $API_Modem_Condition =  "and vendors.update_flag = 0";   //(If API than vendors.update_flag = 0 ) 
                    }     
                }    
                if(!empty($from_time) && ($from_time > 0 )){
                    $frm = $from_time;
                    $from_time_Condition =  "and vendors_messages.timestamp >='$frmdate $from_time:00:00'";
                }
                if(!empty($to_time) && ($to_time > 0 )){
                    $to = $to_time;
                    $to_time_Condition =   "and vendors_messages.timestamp <='$todate $to_time:00:00'";
                }
                
		$inprocessdata = $this->Slaves->query("SELECT * from (Select vendors_activations.id,vendors_activations.status,vendors_activations.ref_code,vendors.update_flag,vendors_activations.timestamp as vatimestamp,vendors_messages.timestamp as vmtimestamp,
												vendors_activations.complaintNo,vendors_activations.date,
												TIME_TO_SEC(TIMEDIFF(vendors_messages.timestamp,vendors_activations.timestamp)) as processtime
												from 
												vendors_activations use index(idx_date) 
												inner join vendors_messages on (vendors_messages.shop_tran_id = vendors_activations.ref_code and vendors_messages.service_vendor_id = vendors_activations.vendor_id)
												inner join vendors on (vendors.id = vendors_activations.vendor_id)
												where vendors_activations.date between '$frmdate' and  '$todate' $from_time_Condition $to_time_Condition $API_Modem_Condition
												 order by vendors_messages.id desc) as t group by t.ref_code"
				                           );
                
		if(!empty($inprocessdata)):
			$manualfailue = 0;
			$manualsuccess = 0;
			$modemInprocess = 0;
			$apiinProcess = 0;
			$autosuccess = 0;
			$autofailure = 0;
			$exceedautoSuccess = 0;
			$exceedautoFailure = 0;
                        $exceedmanualfailure = 0;
			$exceedmanualsuccess = 0;
			$pending = 0;
			$totalexceed = 0;
                        
                        //Total Auto Success and Failed Transaction above 5 mins or 300 secs
                        $autoTotalSuccees = 0;
                        $autoTotalFailed = 0;
                        
                        //Total Manually Success and Failed Transaction above 5 mins or 300 secs
                        $manualTotalSuccees = 0;
                        $manualTotalFailed = 0;
                        
                        
                        //Auto Success Proccessing timings
                        $autoSuccessProcessing_Time_5_to_15 = 0;
                        $autoSuccessProcessing_Time_15_to_45 = 0;
                        $autoSuccessProcessing_Time_45_to_115 = 0;
                        $autoSuccessProcessing_Time_115_to_2 = 0;
                        $autoSuccessProcessing_Time_200_to_more = 0;
                        
                        //Manual Success Proccessing timings
                        $manuallySuccessProcessing_Time_5_to_15 = 0;
                        $manuallySuccessProcessing_Time_15_to_45 = 0;
                        $manuallySuccessProcessing_Time_45_to_115 = 0;
                        $manuallySuccessProcessing_Time_115_to_2 = 0;
                        $manuallySuccessProcessing_Time_200_to_more = 0;
                        
                        //Auto Failed Proccessing timings
                        $autoFailProcessing_Time_5_to_15 = 0;
                        $autoFailProcessing_Time_15_to_45 = 0;
                        $autoFailProcessing_Time_45_to_115 = 0;
                        $autoFailProcessing_Time_115_to_2 = 0;
                        $autoFailProcessing_Time_200_to_more = 0;
                        
                        //Manual Failed Proccessing timings
                        $manuallyFailProcessing_Time_5_to_15 = 0;
                        $manuallyFailProcessing_Time_15_to_45 = 0;
                        $manuallyFailProcessing_Time_45_to_115 = 0;
                        $manuallyFailProcessing_Time_115_to_2 = 0;
                        $manuallyFailProcessing_Time_200_to_more = 0;
                        
			$userdata = array();
			$manualsuccesscount = array();
			$manualfailurecount = array();
			$datearray = array();
			
			foreach ($inprocessdata as $val):
				if(!isset($datearray[$val['t']['date']]['pending'])){
						$datearray[$val['t']['date']]['pending'] = 0;
				}
//				if(!isset($datearray[$val['t']['date']]['totalexceed'])){
//						$datearray[$val['t']['date']]['totalexceed'] = 0;
//				}
				if(!isset($datearray[$val['t']['date']]['manualfailure'])){
					$datearray[$val['t']['date']]['manualfailure'] = 0;
				}
				if(!isset($datearray[$val['t']['date']]['manualsuccess'])){
						$datearray[$val['t']['date']]['manualsuccess']=0;
				}
				if(!isset($datearray[$val['t']['date']]['autosuccess'])){
						$datearray[$val['t']['date']]['autosuccess'] = 0;
				}
//				if(!isset($datearray[$val['t']['date']]['exceedautoSuccess'])){
//						$datearray[$val['t']['date']]['exceedautoSuccess'] = 0;
//				}
//				if(!isset($datearray[$val['t']['date']]['apiinProcess'])){
//						$datearray[$val['t']['date']]['apiinProcess'] = 0;
//				}
				if(!isset($datearray[$val['t']['date']]['autofailure'])){
						$datearray[$val['t']['date']]['autofailure']=0;
				}
//				if(!isset($datearray[$val['t']['date']]['modemInprocess'])){
//						$datearray[$val['t']['date']]['modemInprocess'] = 0;
//				}
//				if(!isset($datearray[$val['t']['date']]['exceedautoFailure'])){
//						$datearray[$val['t']['date']]['exceedautoFailure'] = 0;
//				}
//				if(!isset($datearray[$val['t']['date']]['exceedmanualsuccess'])){
//						$datearray[$val['t']['date']]['exceedmanualsuccess'] = 0;
//				}
//				if(!isset($datearray[$val['t']['date']]['exceedmanualfailure'])){
//						   $datearray[$val['t']['date']]['exceedmanualfailure'] = 0;
//				}
                                
                                 //Total Auto Success and Failed Transaction above 5 mins or 300 secs
                                if(!isset($datearray[$val['t']['date']]['autoTotalSuccees'])){
						$datearray[$val['t']['date']]['autoTotalSuccees']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoTotalFailed'])){
						$datearray[$val['t']['date']]['autoTotalFailed']=0;
				}
                                
                                //Total Manually Success and Failed Transaction above 5 mins or 300 secs
                                if(!isset($datearray[$val['t']['date']]['manualTotalSuccees'])){
						$datearray[$val['t']['date']]['manualTotalSuccees']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manualTotalFailed'])){
						$datearray[$val['t']['date']]['manualTotalFailed']=0;
				}
                                
                                
                                //Decalre AutoSuceessProcessing Time Variable
                                if(!isset($datearray[$val['t']['date']]['autoSuccessProcessing_Time_5_to_15'])){
						$datearray[$val['t']['date']]['autoSuccessProcessing_Time_5_to_15']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoSuccessProcessing_Time_15_to_45'])){
						$datearray[$val['t']['date']]['autoSuccessProcessing_Time_15_to_45']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoSuccessProcessing_Time_45_to_115'])){
						$datearray[$val['t']['date']]['autoSuccessProcessing_Time_45_to_115']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoSuccessProcessing_Time_115_to_2'])){
						$datearray[$val['t']['date']]['autoSuccessProcessing_Time_115_to_2']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoSuccessProcessing_Time_200_to_more'])){
						$datearray[$val['t']['date']]['autoSuccessProcessing_Time_200_to_more']=0;
				}
                                
                                //Decalre ManualSuceessProcessing Time Variable
                                if(!isset($datearray[$val['t']['date']]['manuallySuccessProcessing_Time_5_to_15'])){
						$datearray[$val['t']['date']]['manuallySuccessProcessing_Time_5_to_15']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallySuccessProcessing_Time_15_to_45'])){
						$datearray[$val['t']['date']]['manuallySuccessProcessing_Time_15_to_45']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallySuccessProcessing_Time_45_to_115'])){
						$datearray[$val['t']['date']]['manuallySuccessProcessing_Time_45_to_115']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallySuccessProcessing_Time_115_to_2'])){
						$datearray[$val['t']['date']]['manuallySuccessProcessing_Time_115_to_2']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallySuccessProcessing_Time_200_to_more'])){
						$datearray[$val['t']['date']]['manuallySuccessProcessing_Time_200_to_more']=0;
				}
                                
                                //Decalre AutoFailedProcessing Time Variable
                                if(!isset($datearray[$val['t']['date']]['autoFailProcessing_Time_5_to_15'])){
						$datearray[$val['t']['date']]['autoFailProcessing_Time_5_to_15']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoFailProcessing_Time_15_to_45'])){
						$datearray[$val['t']['date']]['autoFailProcessing_Time_15_to_45']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoFailProcessing_Time_45_to_115'])){
						$datearray[$val['t']['date']]['autoFailProcessing_Time_45_to_115']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoFailProcessing_Time_115_to_2'])){
						$datearray[$val['t']['date']]['autoFailProcessing_Time_115_to_2']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['autoFailProcessing_Time_200_to_more'])){
						$datearray[$val['t']['date']]['autoFailProcessing_Time_200_to_more']=0;
				}
                                
                                //Decalre ManualFailedProcessing Time Variable
                                if(!isset($datearray[$val['t']['date']]['manuallyFailProcessing_Time_5_to_15'])){
						$datearray[$val['t']['date']]['manuallyFailProcessing_Time_5_to_15']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallyFailProcessing_Time_15_to_45'])){
						$datearray[$val['t']['date']]['manuallyFailProcessing_Time_15_to_45']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallyFailProcessing_Time_45_to_115'])){
						$datearray[$val['t']['date']]['manuallyFailProcessing_Time_45_to_115']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallyFailProcessing_Time_115_to_2'])){
						$datearray[$val['t']['date']]['manuallyFailProcessing_Time_115_to_2']=0;
				}
                                if(!isset($datearray[$val['t']['date']]['manuallyFailProcessing_Time_200_to_more'])){
						$datearray[$val['t']['date']]['manuallyFailProcessing_Time_200_to_more']=0;
				}
                                
				
				if($val['t']['status']==0){
					 // pending transactions
					$datearray[$val['t']['date']]['pending']++;
				}
				

	    $processtime = $val['t']['processtime'];
//                                if($processtime){
//                                    echo 'Process Time:-'.$processtime/60;
//					//$totalexceed++;
//                                
//				}
               
                                
            //Counting AutoSuccess Process Time                 
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>=300)&&($processtime<=900))){
                  $datearray[$val['t']['date']]['autoSuccessProcessing_Time_5_to_15']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>900)&&($processtime<=2700))){
                  $datearray[$val['t']['date']]['autoSuccessProcessing_Time_15_to_45']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>2700)&&($processtime<=4500))){
                  $datearray[$val['t']['date']]['autoSuccessProcessing_Time_45_to_115']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>4500)&&($processtime<=7200))){
                  $datearray[$val['t']['date']]['autoSuccessProcessing_Time_115_to_2']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && ($processtime>7200)){
                  $datearray[$val['t']['date']]['autoSuccessProcessing_Time_200_to_more']++;
            }
            
            //Total Auto Success and Failed Transaction above 5 mins or 300 secs
            if(($val['t']['complaintNo']==0) && ($val['t']['status']== '1' || $val['t']['status']== '5') && ($processtime>=300)){
		 $datearray[$val['t']['date']]['autoTotalSuccees']++;
	    }
            if(($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')&& ($processtime>=300)){
		 $datearray[$val['t']['date']]['autoTotalFailed']++;
	    }
            
            //Total Manually Success and Failed Transaction above 5 mins or 300 secs
            if(!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5') && ($processtime>=300)){
		 $datearray[$val['t']['date']]['manualTotalSuccees']++;
	    }
            if(!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')&& ($processtime>=300)){
		 $datearray[$val['t']['date']]['manualTotalFailed']++;
	    }
            
            //Counting ManualSuccess Process Time
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>=300)&&($processtime<=900))){
                  $datearray[$val['t']['date']]['manuallySuccessProcessing_Time_5_to_15']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>900)&&($processtime<=2700))){
                  $datearray[$val['t']['date']]['manuallySuccessProcessing_Time_15_to_45']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>2700)&&($processtime<=4500))){
                  $datearray[$val['t']['date']]['manuallySuccessProcessing_Time_45_to_115']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && (($processtime>4500)&&($processtime<=7200))){
                  $datearray[$val['t']['date']]['manuallySuccessProcessing_Time_115_to_2']++;
            } 
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '1' || $val['t']['status']== '5')) && ($processtime>7200)){
                  $datearray[$val['t']['date']]['manuallySuccessProcessing_Time_200_to_more']++;
            }
            
            
             //Counting AutoFailed Process Time                 
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>=300)&&($processtime<=900))){
                  $datearray[$val['t']['date']]['autoFailProcessing_Time_5_to_15']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>900)&&($processtime<=2700))){
                  $datearray[$val['t']['date']]['autoFailProcessing_Time_15_to_45']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>2700)&&($processtime<=4500))){
                  $datearray[$val['t']['date']]['autoFailProcessing_Time_45_to_115']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>4500)&&($processtime<=7200))){
                  $datearray[$val['t']['date']]['autoFailProcessing_Time_115_to_2']++;
            }
            if((($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && ($processtime>7200)){
                  $datearray[$val['t']['date']]['autoFailProcessing_Time_200_to_more']++;
            }
            
            //Counting ManualFailed Process Time
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>=300)&&($processtime<=900))){
                  $datearray[$val['t']['date']]['manuallyFailProcessing_Time_5_to_15']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>900)&&($processtime<=2700))){
                  $datearray[$val['t']['date']]['manuallyFailProcessing_Time_15_to_45']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>2700)&&($processtime<=4500))){
                  $datearray[$val['t']['date']]['manuallyFailProcessing_Time_45_to_115']++;
            }
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && (($processtime>4500)&&($processtime<=7200))){
                  $datearray[$val['t']['date']]['manuallyFailProcessing_Time_115_to_2']++;
            } 
            if((!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')) && ($processtime>7200)){
                  $datearray[$val['t']['date']]['manuallyFailProcessing_Time_200_to_more']++;
            }
            
                                
//				if($processtime>=900){
//					//$totalexceed++;
//					$datearray[$val['t']['date']]['totalexceed']++;
//				}
			
				if(!empty($val['t']['complaintNo']) && ($val['t']['status']== '2' || $val['t']['status']== '3')){
					///$manualfailue++;  // manual failure
				   // $manualfailurecount[$val['vendors_activations']['complaintNo']]['manualfailure'][]=  $val['vendors_activations']['ref_code'];
				
				    $datearray[$val['t']['date']]['manualfailure']++;
					//$datearray[$date]['manualfailurecount'] = $manualfailurecount;
				}
				
				if(!empty($val['t']['complaintNo']) && $val['t']['status']== '1'){
					//$manualsuccess++;   // manual success
				    //$manualsuccesscount[$val['vendors_activations']['complaintNo']]['manualsuccess'][]=  $val['vendors_activations']['ref_code'];
					//$datearray[$val['t']['date']]['manualsuccess'] = $manualsuccess;
					
					$datearray[$val['t']['date']]['manualsuccess']++;
				 	//$datearray[$date]['manualsuccesscount'] = $manualsuccesscount;
				}
				
				if(($val['t']['complaintNo']==0) && ($val['t']['status']== '1') && ($processtime>=60)){
					
					//$autosuccess++;     // auto success
					//$datearray[$val['t']['date']]['autosuccess'] = $autosuccess;
					
					$datearray[$val['t']['date']]['autosuccess']++;
					
				}
				
				if(($val['t']['complaintNo']==0) && ($val['t']['status']== '2' || $val['t']['status']== '3')&& ($processtime>=60)){
					
					//$autofailure++;    // auto failure
					
					$datearray[$val['t']['date']]['autofailure']++;
					//$datearray[$val['t']['date']]['autofailure'] = $autofailure;
					

				}
				
				if($val['t']['update_flag']==0 && $processtime>=300){
					//$apiinProcess++;   // api inprocess
					
					$datearray[$val['t']['date']]['apiinProcess']++;
				}
				if($val['t']['update_flag']==1 && $processtime>=300){
					
					//$modemInprocess++;     // modem in process
					$datearray[$val['t']['date']]['modemInprocess']++;
				}
				
//				if($processtime>=900 && ($val['t']['complaintNo']==0 && $val['t']['status']=='1')){
//					//$exceedautoSuccess++; // excedd auto success transaction  more than 15 mins 
//					
//					$datearray[$val['t']['date']]['exceedautoSuccess']++;
//				}
				
//				if($processtime>=900 && $val['t']['complaintNo']==0 && 
//						($val['t']['status']== '2' || $val['t']['status']== '3')){
//					///$exceedautoFailure++;
//					
//					$datearray[$val['t']['date']]['exceedautoFailure']++;
//				}
				
//				if($processtime>=900 && !empty($val['t']['complaintNo']) && $val['t']['status']== '1'){
//					
//					//$exceedmanualsuccess++;
//					
//					$datearray[$val['t']['date']]['exceedmanualsuccess']++;
//				}
				
//				if($processtime>=900 && !empty($val['t']['complaintNo']) && 
//						($val['t']['status']== '2' || $val['t']['status']== '3')){
//					  // $exceedmanualfailure++;	
//					   
//					   $datearray[$val['t']['date']]['exceedmanualfailure']++;
//				}
				
//				if($val['t']['complaintNo']!=0){
//					
//					$userdata[$val['users']['name']] = array("failure" => count($manualfailurecount[$val['vendors_activations']['complaintNo']]['manualfailure']),"success" => count($manualsuccesscount[$val['vendors_activations']['complaintNo']]['manualsuccess']));
//				}
				
			endforeach;
                  
			$this->set('frmdate',$frmdate);
			$this->set('todate',$todate);
                        $this->set('frm',$frm);
		        $this->set('to',$to);
                        $this->set('datearray',$datearray);
                        $this->set('modem_flag', $Modem_flag);
		        $this->set('api_flag', $API_flag);
			
		endif;
        }
        
        function inProcessTransactionList($frmdate, $todate, $from_time, $to_time, $Modem_flag, $API_flag, $cat, $page = 1, $recs = 100) {

            $from_time  = empty($from_time) ? '' : $from_time;
            $to_time    = empty($to_time) ? '' : $to_time;
            $limit      = ($page - 1) * $recs . ',' . $recs;
            
            if(($Modem_flag) && ($API_flag)){
                $API_Modem_Condition = '';
            }else{
                if($Modem_flag){
                    $API_Modem_Condition =  "and vendors.update_flag = 1";   //(If MODEM than vendors.update_flag = 1 )
                }else if($API_flag){
                    $API_Modem_Condition =  "and vendors.update_flag = 0";   //(If API than vendors.update_flag = 0 ) 
                }     
            }    
            if(!empty($from_time) && ($from_time > 0 )){
                $frm = $from_time;
                $from_time_Condition =  "and vendors_messages.timestamp >='$frmdate $from_time:00:00'";
            }
            if(!empty($to_time) && ($to_time > 0 )){
                $to = $to_time;
                $to_time_Condition =   "and vendors_messages.timestamp <='$todate $to_time:00:00'";
            }
            
            if($cat == 'whole_total') {
                $query  = " and vendors_activations.status IN (1,2,3,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'total_success') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'total_fail') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'success_5_to_15') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'fail_5_to_15') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'success_15_to_45') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'fail_15_to_45') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'success_45_to_115') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'fail_45_to_115') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'success_115_to_2') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'fail_115_to_2') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'success_200_to_more') {
                $query  = " and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 7200 ";
            } else if($cat == 'fail_200_to_more') {
                $query  = " and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 7200 ";
            }else if($cat == 'whole_auto') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,2,3,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'auto_total_success') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'auto_total_fail') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'auto_success_5_to_15') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'auto_fail_5_to_15') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'auto_success_15_to_45') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'auto_fail_15_to_45') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'auto_success_45_to_115') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'auto_fail_45_to_115') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'auto_success_115_to_2') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'auto_fail_115_to_2') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'auto_success_200_to_more') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 7200 ";
            } else if($cat == 'auto_fail_200_to_more') {
                $query  = " and (vendors_activations.complaintNo IS NULL or vendors_activations.complaintNo = '' or vendors_activations.complaintNo = '0') and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 7200 ";
            }else if($cat == 'whole_manual') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,2,3,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'manual_total_success') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'manual_total_fail') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 ";
            } else if($cat == 'manual_success_5_to_15') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'manual_fail_5_to_15') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime >= 300 and processtime <= 900 ";
            } else if($cat == 'manual_success_15_to_45') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'manual_fail_15_to_45') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 900 and processtime <= 2700 ";
            } else if($cat == 'manual_success_45_to_115') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'manual_fail_45_to_115') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 2700 and processtime <= 4500 ";
            } else if($cat == 'manual_success_115_to_2') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'manual_fail_115_to_2') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 4500 and processtime <= 7200 ";
            } else if($cat == 'manual_success_200_to_more') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (1,5) ";
                $having = " having processtime > 7200 ";
            } else if($cat == 'manual_fail_200_to_more') {
                $query  = " and vendors_activations.complaintNo > 0 and vendors_activations.status IN (2,3) ";
                $having = " having processtime > 7200 ";
            }
            
            $countrecords = $this->Slaves->query("SELECT count(1) FROM (SELECT * from (Select vendors_activations.id,vendors_activations.status,vendors_activations.ref_code,vendors_activations.timestamp as vatimestamp,vendors_messages.timestamp as vmtimestamp,
                                vendors_activations.complaintNo,vendors_activations.date,
                                TIME_TO_SEC(TIMEDIFF(vendors_messages.timestamp,vendors_activations.timestamp)) as processtime
                                from vendors_activations use index(idx_date) 
                                inner join vendors_messages on (vendors_messages.shop_tran_id = vendors_activations.ref_code and vendors_messages.service_vendor_id = vendors_activations.vendor_id)
                                left join vendors on (vendors.id = vendors_activations.vendor_id)
                                left join products on (products.id = vendors_activations.product_id)
                                left join users on (users.id = vendors_activations.complaintNo)
                                where vendors_activations.date between '$frmdate' and  '$todate' $from_time_Condition $to_time_Condition $API_Modem_Condition $query
                                order by vendors_messages.id desc) as t group by t.ref_code $having) as vendor");
            
            $totalpages = ceil($countrecords[0][0]['count(1)'] / $recs);

            $inprocessdata = $this->Slaves->query("SELECT * from (Select vendors_activations.id,vendors.company,products.name,vendors_activations.product_id,vendors_activations.status,vendors_activations.ref_code,vendors_activations.timestamp as vatimestamp,vendors_messages.timestamp as vmtimestamp,
                                vendors_activations.complaintNo,users.name as username,users.email,vendors_activations.date,
                                TIME_TO_SEC(TIMEDIFF(vendors_messages.timestamp,vendors_activations.timestamp)) as processtime, vendors.update_flag
                                from vendors_activations use index(idx_date) 
                                inner join vendors_messages on (vendors_messages.shop_tran_id = vendors_activations.ref_code and vendors_messages.service_vendor_id = vendors_activations.vendor_id)
                                left join vendors on (vendors.id = vendors_activations.vendor_id)
                                left join products on (products.id = vendors_activations.product_id)
                                left join users on (users.id = vendors_activations.complaintNo)
                                where vendors_activations.date between '$frmdate' and  '$todate' $from_time_Condition $to_time_Condition $API_Modem_Condition $query
                                order by vendors_messages.id desc) as t group by t.ref_code $having ORDER BY 1 DESC LIMIT $limit");

            $this->set('inprocessdata', $inprocessdata);
            $this->set('totalrecords', $countrecords[0][0]['count(1)']);
            $this->set('totalpages', $totalpages);
            $this->set('page', $page);
            $this->set('recs', $recs);
            $this->set('modem_flag', $Modem_flag); 
            $this->set('api_flag', $API_flag);
        }

    function inprocessReportMongo(){
      //  error_reporting(E_ALL);
 //ini_set("display_errors", 1);
        $frmdate = isset($_REQUEST['frmdate']) ? $_REQUEST['frmdate'] : date('Y-m-d ');
        $todate = isset($_REQUEST['todate']) ? $_REQUEST['todate'] : date('Y-m-d ');
        $from_time = isset($_REQUEST['from_time']) ? $_REQUEST['from_time'] : '';
        $to_time = isset($_REQUEST['to_time']) ? $_REQUEST['to_time'] : '';
        if(!empty($from_time) && ($from_time > 0 )){
                    
                $start =  "$frmdate $from_time:00:00";
            }else{
                $start = "$frmdate 00:00:00";
            }
            if(!empty($to_time) && ($to_time > 0 )){
                $end =   "$todate $to_time:00:00";
            }else{
                $end = "$todate 24:00:00";
            }
        try{
            $m = new MongoClient("mongodb://52.72.28.148:27017");
        }
        catch(Exception $e){
             die('Error connecting to MongoDB server');
        }

        $db = $m->shops;
        $col = $db->VAVM;
       
        $start = new MongoDate(strtotime($start));
        $end = new MongoDate(strtotime($end)); 
        
        $Modem_flag = isset($_REQUEST['modem_flag']) ? $_REQUEST['modem_flag'] : 1; 
        $API_flag = isset($_REQUEST['api_flag']) ? $_REQUEST['api_flag'] : 1;
        $is_api =  '1';
        if(($Modem_flag) && ($API_flag)){
                  $is_api = array('$in'=>array(0,1));
        }else{
            
            if($Modem_flag){
              $is_api = 0;  //(If MODEM than vendors.update_flag = 1 )
            }else if($API_flag){
              $is_api =  1;   //(If API than vendors.update_flag = 0 ) 
            }     
        }    
        $ops = array(
            array('$match' => array('$and'=>array(array('date'=>array('$gte'=>$start,'$lte'=>$end)),array('is_api'=> $is_api))) ),
            array('$unwind'=>'$vendors_messages'),
            array('$group'=>array(
                    '_id'=>'$id',
                    'vatimestamp'=>array('$first'=>'$timestamp'),
                    'vmtimestamp'=>array('$last'=>'$vendors_messages.timestamp'),
                    'transactionday'=>array('$first'=>'$date'),
                    'status'=>array('$first'=>'$status'),
                    'complaintNo'=>array('$first'=>'$complaintNo'),
                     )
                ),
           array('$project'=>array(
                    'transactionday'=>1,'status'=>1,'complaintNo'=>array('$ifNull'=>array('$complaintNo','0')),
		'datediff'=>array('$divide'=>array(array('$subtract'=>array('$vmtimestamp','$vatimestamp')),1000)
           ))),
           array('$project'=>array(
               'transactionday'=>1,
               'range'=>array('$concat'=>array( 
                   array('$cond'=>array(array('$and'=>array(array('$gte'=>array('$datediff',300)),array('$lte'=>array('$datediff',900)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'auto success range 5-15 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gte'=>array('$datediff',300)),array('$lte'=>array('$datediff',900)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'auto fail range 5-15 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gte'=>array('$datediff',300)),array('$lte'=>array('$datediff',900)),array('$ne'=>array('$complaintNo','0')),array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            ))))),'manual success range 5-15 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gte'=>array('$datediff',300)),array('$lte'=>array('$datediff',900)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'manual fail range 5-15 min','')),
                   
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',900)),array('$lte'=>array('$datediff',2700)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'auto success range 15-45 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',900)),array('$lte'=>array('$datediff',2700)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'auto fail range 15-45 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',900)),array('$lte'=>array('$datediff',2700)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'manual success range 15-45 min','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',900)),array('$lte'=>array('$datediff',2700)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'manual fail range 15-45 min','')),
                   
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',2700)),array('$lte'=>array('$datediff',4500)),array('$eq'=>array('$complaintNo','0')),array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'auto success range 45-1.5 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',2700)),array('$lte'=>array('$datediff',4500)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'auto fail range 45-1.5 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',2700)),array('$lte'=>array('$datediff',4500)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'manual success range 45-1.5 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',2700)),array('$lte'=>array('$datediff',4500)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'manual fail range 45-1.5 hr','')),
                   
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',4500)),array('$lte'=>array('$datediff',7200)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'auto success range 1.5-2 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',4500)),array('$lte'=>array('$datediff',7200)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'auto fail range 1.5-2 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',4500)),array('$lte'=>array('$datediff',7200)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'manual success range 1.5-2 hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',4500)),array('$lte'=>array('$datediff',7200)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'manual fail range 1.5-2 hr','')),
                   
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',7200)),array('$ne'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','1')
                                            ),
//                                            array(
//                                            '$eq'=>array('$status','4')
//                                            ),
                                            array(
                                            '$eq'=>array('$status','5')
                                            )))
                       )),'manual success range 2+ hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',7200)),array('$eq'=>array('$complaintNo','0')),array('$eq'=>array('$status','1')))),'auto success range 2+ hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',7200)),array('$ne'=>array('$complaintNo','0')),
                         array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'manual fail range 2+ hr','')),
                   array('$cond'=>array(array('$and'=>array(array('$gt'=>array('$datediff',7200)),array('$eq'=>array('$complaintNo','0')),
                       array('$or'=>
                                    array(array(
                                            '$eq'=>array('$status','2')
                                            ),
                                            array(
                                            '$eq'=>array('$status','3')
                                            )
                                        ))
                       )),'auto fail range 2+ hr','')),
                 
               ))
           )),
           array('$group'=>array(
                    '_id'=>array('range'=>'$range','day'=>'$transactionday'),
                    'quantity'=>array('$sum'=>1)
           )),array('$sort'=>array('_id'=>-1))
    );
        $options = array('allowDiskUse' => true);
   
    $cursor = $col->aggregate($ops,$options);
    $values = array();

    $i=0;
    foreach($cursor['result'] as $row){
            $result[date('Y-m-d',$row['_id']['day']->sec)][$row['_id']['range']]=  $row['quantity'];
            $i++;
    }
    
        $this->set('frmdate',$frmdate);
        $this->set('todate',$todate);
        $this->set('frm',$frm);
        $this->set('to',$to);
        $this->set('datearray',$result);
        $this->set('modem_flag', $Modem_flag);
        $this->set('api_flag', $API_flag);
    }        
   
    function updateDSN(){
    	$this->autoRender = false;
    	
    	$retailer_id = $_POST['retailer_id'];
    	$dsn = $_POST['dsn'];
    	
    	$this->User->query("update retailers
    			set device_serial_no = '$dsn',
    			modified = '".date('Y-m-d H:i:s')."'
    			where id = ".$retailer_id);
    	
    	echo "true";
    	return;
    }
    
    function activateMPOS(){
    	$this->autoRender = false;
    	
    	$retailer_id = $_POST['retailer_id'];
    	$activate_flag = $_POST['activate_flag'];
    	
    	$retailers = $this->Slaves->query("select * 
    			from retailers r
    			left join retailers_services rs on rs.retailer_id = r.id and rs.service_id = 8
    			where r.id = ".$retailer_id);
    	
    	if($activate_flag == 1){
	    	if(empty($retailers[0]['r']['device_serial_no'])){
	    		echo "No device serial no found. Add device serial no to activate mPOS service";
	    		return;
	    	}	
	    	$this->User->query("insert into retailers_services
	    			(retailer_id, service_id)
	    			values ('$retailer_id', '8')");
	    	echo "mPOS service activated.";
    	}
    	else {
    		$this->User->query("delete from retailers_services
    				where retailer_id = ".$retailer_id."
    				and service_id = 8");
    		echo "mPOS service deactivated.";
    	} 
    	return;
    }
    
    function hide()
        {
            $this->autoRender = false;
            $vid=$this->params['form']['vid'];
            //echo $vid;
            $pid=$this->params['form']['pid'];
            //echo $pid;
            $query="update vendors_commissions set is_deleted=1 where vendor_id=$vid and product_id=$pid";
            //echo $query;
            $isdeleted=$this->User->query($query);
            echo json_encode(array('status'=>'done'));

        }


function updateOperatorFlag() {

		if ($this->RequestHandler->isAjax()) {
			if ($_POST['auto_check'] == "true"): $autocheck = 1;
			else : $autocheck = 0;
			endif;
			
		
			
			$oprId = isset($_POST['oprid']) ? $_POST['oprid'] : 0;
			$updateQuery = $this->User->query("UPDATE  products SET auto_check = '".$autocheck."',modified = '".date('Y-m-d H:i:s')."' where id IN($oprId)");
		}
		$this->autoRender = false;
	}
		
        function errorMsg(){

                $this->autoRender = false;
                  echo "<span style='color:red;font-size:20'>You don't have permission to access this page</span>";
                  echo "<img title=''  src='/img/no.png' ></img>";
        }
        
    function show()
    {
        $this->autoRender = false;
        $vid=$this->params['form']['vid'];
        $pid=$this->params['form']['pid'];
        $query="update vendors_commissions set is_deleted=0 where vendor_id=$vid and product_id=$pid";
        $isdeleted=$this->User->query($query);
        echo json_encode(array('status'=>'done'));
    }
    
    /**
     * It will get current status of API transaction from vendor
     * 
     */
    function check_current_api_txn_status(){
        $this->autoRender = false;
		
        $transId = $_REQUEST['id'];
        $vendor = $_REQUEST['vendor'];
        $date = $_REQUEST['date'];
        $refId = $_REQUEST['ref_id'];
        $vendor_id = $_REQUEST['vendor_id'];
        
        App::import('Controller', 'Recharges');
        $obj = new RechargesController;
        $obj->constructClasses();

        ob_start(); 
        $vend_status = $obj->tranStatus($transId, $vendor, $date, $refId,true);
        ob_end_clean();
        
        if(!in_array(strtolower(trim($vend_status['status'])), array('success','failure'))){
            echo "pending";
            exit();
        }
        $dt = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,operator_id,date FROM vendors_activations use index (idx_vend_date) ,products WHERE vendors_activations.product_id=products.id AND vendor_id = '$vendor_id' AND vendors_activations.date = '$date' and ref_code='$transId'");
        
        if(empty($dt['vendors_activations']['operator_id']) && !empty($vend_status['operator_id'])){
            $query .= "operator_id = '".$vend_status['operator_id']."'";
        }
        if(empty($dt['vendors_activations']['vendor_refid']) && !empty($vend_status['vendor_id'])){
            if(!empty($query)) $query .= ", ";
            $query .= "vendor_refid = '".$vend_status['vendor_id']."'";
        }
        
        if(!empty($query)){
            $this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
        }
        
        if($dt['vendors_activations']['status'] == 0 ){
            if(strtolower(trim($vend_status['status'])) == 'success'){
                $this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
                $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
                $this->User->query("UPDATE api_transactions set vendor_status = 'success' where txn_id='$transId' and vendor_id='$vendor_id'");
            }
            else if($vend_status['status'] == 'failure'){
                $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");
                $this->User->query("UPDATE api_transactions set vendor_status = 'failure' where txn_id='$transId' and vendor_id='$vendor_id'");
            }
        }else{
            if(strtolower(trim($vend_status['status'])) == 'success'){
                $this->User->query("UPDATE api_transactions set vendor_status = 'success' where txn_id='$transId' and vendor_id='$vendor_id'");
            }
            else if($vend_status['status'] == 'failure'){            
                $this->User->query("UPDATE api_transactions set vendor_status = 'failure' where txn_id='$transId' and vendor_id='$vendor_id'");
            }
        }
        
        $vend_status['status'] = (isset($vend_status['status']) && in_array(strtolower(trim($vend_status['status'])), array('success','failure'))) ? $vend_status['status'] : "pending";
        
        echo $vend_status['status'];        
    }
	
	function shiftbalance(){
		
		
		if ($this->RequestHandler->isAjax()) {
			
			$supplierId = $_REQUEST['supplier_id'];
			$oprId =   $_REQUEST['oprId'];
			$bal  = $_REQUEST['bal'];
			$newSimId = $_REQUEST['new_sim_id'];
			$oldSimId = $_REQUEST['old_sim_id'];
			$sourceVendorId = $_REQUEST['modemId'];
			
			
			
            $checkifexists=$this->Slaves->query("Select id,scid,vendor_id from devices_data where opr_id='{$oprId}'  AND inv_supplier_id='{$supplierId}' AND scid ='{$newSimId}' and sync_date = '".date('Y-m-d')."' and device_num>0");
            
            if(empty($checkifexists)):
                 echo  json_encode(array('data'=>'Error'));
                 exit();
            endif;
					
					$updatenewsim = "query=shiftbalance&target_vendor_id={$checkifexists[0]['devices_data']['vendor_id']}&source_vendor_id={$sourceVendorId}&balance={$bal}&opr_id={$oprId}&supplier_id={$supplierId}&new_scid={$newSimId}&old_scid={$oldSimId}&reqtype=update_target_vendor";
					
					$updatedeviceData = $this->Shop->modemRequest($updatenewsim,$checkifexists[0]['devices_data']['vendor_id']);
					
					if($updatedeviceData['status'] == 'success' && $updatedeviceData['data']=='Success'){
						
						$updatedeviceData = json_decode($updatedeviceData['data'],TRUE);
						
						$updateoldsim = "query=shiftbalance&source_vendor_id={$sourceVendorId}&opr_id={$oprId}&supplier_id={$supplierId}&scid={$oldSimId}";
						
						$updatedeviceData = $this->Shop->modemRequest($updateoldsim,$sourceVendorId);
						
						if($updatedeviceData['status'] == 'success'){
							
							 echo json_encode($updatedeviceData);
			                 die;
							
						}
						
						}
					
		  }
			 

		
		
		$this->autoRender = false;
		
	}
        
        function tranDiffReport($frm_date = NULL,$to_date = NULL,$vendor = 'all',$product = 'all') {
            
                if($frm_date == NULL || $to_date == NULL) {
                        $frm_date = $to_date = date('Y-m-d');
                }
                
                $vendor_q = "";
                if($vendor != 'all') {
                    $vendor_q = " va.vendor_id = '$vendor' AND ";
                }
                
                $product_q = "";
                if($product != 'all') {
                    $product_q = " va.product_id = '$product' AND ";
                }
                
                $tran_data = $this->Slaves->query("SELECT va.id va_id,va.vendor_id,va.product_id,va.amount,va.ref_code,va.status va_status,va.vendor_refid,va.date,va.timestamp,vm.id vm_id,vm.shop_tran_id,vm.status vm_status,vm.response 
                                    FROM vendors_activations va 
                                    JOIN vendors_messages vm ON (va.ref_code = vm.shop_tran_id AND vm.status = 'success') 
                                    WHERE $vendor_q $product_q va.date >= '$frm_date' AND va.date <= '$to_date' AND va.status = ".TRANS_REVERSE." 
                                    ORDER BY va_id DESC");
                
                $vendors_temp  = $this->Slaves->query("SELECT id,company FROM vendors");
                
                foreach($vendors_temp as $v_t) {
                        $vendors[$v_t['vendors']['id']]   = $v_t['vendors']['company'];
                }
                
                $products_temp = $this->Slaves->query("SELECT id,name FROM products");
                
                foreach($products_temp as $p_t) {
                        $products[$p_t['products']['id']] = $p_t['products']['name'];
                }
                
                $this->set('tran_data', $tran_data);
                $this->set('vendors', $vendors);
                $this->set('products', $products);
                $this->set('sel_vendor', $vendor);
                $this->set('sel_product', $product);
                $this->set('frm_date', $frm_date);
                $this->set('to_date', $to_date);
            
                $this->layout = "plain";
        }
        
        function vendors($page = 1, $recs = 100) {
            
                $this->layout  = "plain";
                
                $limit         = ($page - 1) * $recs . ',' . $recs;
                
                $count_records = $this->Slaves->query("SELECT count(1) count FROM vendors");
            
                $listing_data  = $this->Slaves->query("SELECT id,user_id,company,shortForm,update_flag,show_flag,machine_id,update_time FROM vendors ORDER BY 1 DESC LIMIT $limit");
                
                $this->set('listing_data', $listing_data);
                $this->set('totalrecords', $count_records[0][0]['count']);
                $this->set('page', $page);
                $this->set('recs', $recs);
        }
        
//        function deleteRec() {
//            
//                $this->autoRender = FALSE;
//                
//                $id   = $_POST['id'];
//                
//                $data = $this->User->query("DELETE FROM vendors WHERE id = $id");
//                
//                return json_encode($data);
//        }
        
        function addEditVendor($id=NULL) {
            
                $this->layout = 'plain';
                
                if($id != NULL) {
                        $data = $this->Slaves->query("SELECT id,user_id user,company,shortForm,show_flag,machine_id FROM vendors WHERE id = $id");
                        $this->set('vendor_data', $data[0]['vendors']);
                }
        }
        
        function addEditBackVendor($id=NULL) {
            
                $this->autoRender = FALSE;
                
                $machine_id = $_POST['machine_id'];
                $company    = $_POST['company'];
                $shortform  = $_POST['shortform'];
                
                if($id == NULL) {
                    
                        $show_flag = $_POST['show_flag'];
                        
                        $this->User->query("INSERT INTO vendors (company,shortForm,balance,ip,bridge_ip,port,bridge_flag,update_flag,active_flag,show_flag,svn_flag,health_factor,machine_id,update_time,last30bal) 
                            VALUES ('$company','$shortform','0.00',0,'','',0,1,1,'$show_flag',0,0,'$machine_id','".date('Y-m-d H:i:s')."','0.00')");
                } else {
                    
                        $user = $_POST['user'];
                        
                        if(strlen($user) >= 10) {
                                $exist = $this->checkMobileExist($user);
                                if($exist != '') {
                                        $user = $exist;
                                } else {
                                        $new_user = $this->General->registerUser($user,ONLINE_REG,VENDOR);
                                        $user = $new_user['User']['id'];
                                }
                        }
                        
                        $this->User->query("UPDATE vendors SET user_id='$user',update_time='".date('Y-m-d H:i:s')."' WHERE id = $id");
                }
                
                $record = $id == NULL ? "Inserted" : "Updated";
                $this->Session->setFlash("Record ".$record." Successfully !!!");
                
                $this->redirect('vendors');
        }
        
        function checkMobileExist($mobile=NULL) {
            
                $this->autoRender = FALSE;
            
                if($mobile == NULL) {
                        $mobile = $_POST['mobile'];
                        $column = "count(*) count";
                } else {
                        $column = "id";
                }
                
                $exist = $this->Slaves->query("SELECT $column FROM users WHERE mobile = '$mobile'");
                
                if(isset($_POST['mobile'])) {
                        echo json_encode($exist[0][0]['count']);
                } else {
                        return $exist[0]['users']['id'];
                }
        }
        
        function changeFlag() {
            
                $this->autoRender = FALSE;
                
                $id = $_POST['id'];
                
                $exist_val = $this->Slaves->query("SELECT show_flag FROM vendors WHERE id = $id");
                
                $up_val = $exist_val[0]['vendors']['show_flag'] == '0' ? '1' : '0';
                
                $res = $this->User->query("UPDATE vendors SET show_flag = '$up_val' WHERE id = $id");
                
                return json_encode($res);
        }
}