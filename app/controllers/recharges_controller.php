 <?php
class RechargesController extends AppController {
    

	var $name = 'Recharges';
	var $components = array('RequestHandler','Shop','Busvendors','General','B2cextender');
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator');
	var $uses = array('User','Slaves');
    var $sdiff=array();
    
	var $cp_errs = array('7'=>'Invalid amount','11' => 'Transaction does not exist','20'=>'The payment is being completed.','21'=>'Not enough funds for effecting the payment','22'=>'The payment has not been accepted. Funds transfer error.',
				'23' => 'Invalid phone number/subid','223' => 'Not appropriate subscriber contract for top-up','24'=>'Error of connection with the provider’s server','25'=>'Effecting of this type of payments is suspended.',
				'26' => 'Payments of this Dealer are temporarily blocked', '27'=>'Operations with this account are suspended','30'=>'General system failure.','31'=>'Exceeded number of simultaneously processed requests','32'=>'Repeated payment within 60 minutes',
				'34' => 'Transaction does not exist','43' => 'TransactionId expired','45'=>'No license is available for accepting payments to the benefit of this operator.','52' =>'Dealer blocked','54'=>'Operator blocked','81'=>'Max limit of amount exceeded',
				'82'=> 'Daily debit amount limit exceeded');	

	var $mapping = array(
			'mobRecharge' =>array(
				'1'=>array(
					'operator'=>'Aircel',
					'opr_code'=>'AC',
					'flexi'=>array('id' => '1','oss'=>'AIC','pp'=>'85','payt'=>'7','cbz'=>'RC','rdu'=>'ARCL','uva'=>'AIRC','anand'=>'aircel','apna'=>'AIR','magic'=>'RC','rio'=>'RC','gem'=>'AS','gitech'=>'HACL',
                            'rkit'=>1,'joinrec'=>4,'mypay'=>2,'smsdaak'=>'ACP','hitecrec'=>'RC','practic'=>'Aircel',"simple"=>"AIR","manglam" =>"RC","bulk"=>'RC'),
					'voucher'=>array('id' => '','oss'=>'AIC','pp'=>'85')
	),
				'2'=>array(
					'operator'=>'Airtel',
					'opr_code'=>'AT',
					'flexi'=>array('id' => '2','oss'=>'AR','pp'=>'51','payt'=>'1302','cbz'=>'RA','rdu'=>'ARTL','uva'=>'AIRT','uni'=>'211','anand'=>'airtel','apna'=>'A','magic'=>'RA','rio'=>'RC','gem'=>'AT','durga'=>'AirTel','gitech'=>'HART','rkit'=>2,'a2z'=>2,'joinrec'=>42,
                            'mypay'=>1,'smsdaak'=>'ATP','aporec'=>2,'hitecrec'=>'RA','practic'=>'Airtel',"simple"=>"1","krac"=>"RA","manglam" =>"RA","bulk"=>'RA',"bimco" => "AT"),
					'voucher'=>array('id' => '','oss'=>'AR','pp'=>'51')
	        ),
				'3'=>array(
					'operator'=>'BSNL',
					'opr_code'=>'CG',
					'flexi'=>array('id' => '3','oss'=>'BSN','pp'=>'53','payt'=>'1','cbz'=>'BR','rdu'=>'BSNL','uva'=>'BSNL','magic'=>'TB','rio'=>'TB','gem'=>'BS',
                            'gitech'=>'HBST','rkit'=>5,'joinrec'=>10,'mypay'=>4,'smsdaak'=>'BGP','hitecrec'=>'TB','practic' => 'BSNL',"simple"=>"BT","manglam" =>"RB"),
					'voucher'=>array('id' => '34','oss'=>'BSN','pp'=>'53','payt'=>'1','cbz'=>'BV','rdu'=>'BR','uva'=>'BSNL','magic'=>'RB','rio'=>'RB','gem'=>'BR',
                            'gitech'=>'HBSV','rkit'=>5,'joinrec'=>10,'smsdaak'=>'BVP','mypay'=>4,'hitecrec'=>'TB',"simple"=>"BR","manglam" =>"TB")
	),
				'4'=>array(
					'operator'=>'Idea',
					'opr_code'=>'ID',
					'flexi'=>array('id' => '4','oss'=>'IDE','pp'=>'107','payt'=>'5','cbz'=>'RI','rdu'=>'IDEA','uva'=>'IDEA','uni'=>'233','anand'=>'idea','apna'=>'I','magic'=>'RI',
                            'rio'=>'RI','gem'=>'ID','durga'=>'Idea','rkit'=>6,'gitech'=>'HIDE','joinrec'=>48,'mypay'=>11,'smsdaak'=>'IDP','rio2'=>'RI','aporec'=>11,'hitecrec'=>'RI','practic' => 'Idea',"simple"=>"I","manglam" =>"RI","bulk"=>"RI","bimco" => "IC","rajan" => "ID"),
					'voucher'=>array('id' => '','oss'=>'IDE','pp'=>'107')
	),
				'5'=>array(
					'operator'=>'Loop/BPL',
					'opr_code'=>'LM',
					'flexi'=>array('id' => '5','oss'=>'LOP','pp'=>'1','payt'=>'14','cbz'=>'BP','rdu'=>'LOOP','uva'=>'LOOP','apna'=>'LM','gem'=>'LP'),
					'voucher'=>array('id' => '','oss'=>'LOP','pp'=>'1')
	),
				'6'=>array(
					'operator'=>'MTS',
					'opr_code'=>'MT',
					'flexi'=>array('id' => '6','oss'=>'MTS','pp'=>'133','payt'=>'278','cbz'=>'DM','rdu'=>'MTS','rio'=>'RM','uva'=>'MTS','gem'=>'MT','gitech'=>'HMTS','joinrec'=>13,'rkit'=>'13','smsdaak'=>'MTP','practic' => "MTS","simple"=>"M","manglam" =>"RM"),
					'voucher'=>array('id' => '','oss'=>'MTS','pp'=>'133')
	),
				'7'=>array(
					'operator'=>'Reliance CDMA',
					'opr_code'=>'RC',
					'flexi'=>array('id' => '7','oss'=>'RC','pp'=>'23','payt'=>'1303','cbz'=>'RR','rdu'=>'RIMC','uva'=>'RELC','uni'=>'244','apna'=>'RC','magic'=>'RL','rio'=>'RR','gem'=>'RC','gitech'=>'HREC','rkit'=>14,'a2z'=>1,'joinrec'=>20,'mypay'=>6,'practic' => "RelianceCDMA","simple"=>"RC","manglam" =>"RG"),
					'voucher'=>array('id' => '','oss'=>'RC','pp'=>'23')
	              ),
				'8'=>array(
					'operator'=>'Reliance GSM',
					'opr_code'=>'RG',
					'flexi'=>array('id' => '8','oss'=>'RC','pp'=>'84','payt'=>'6','cbz'=>'RR','rdu'=>'RIMG','uva'=>'RELG','uni'=>'244','apna'=>'RG','magic'=>'RR','rio'=>'RR','gem'=>'RG','ecom'=>'R','gitech'=>'HREG','rkit'=>16,'a2z'=>2,'joinrec'=>7,'mypay'=>7,'smsdaak'=>'RGP','practic' =>"RelianceGSM","simple"=>"RG","manglam" =>"RG"),
					'voucher'=>array('id' => '','oss'=>'RC','pp'=>'84')
        	),
				'9'=>array(
					'operator'=>'Tata Docomo',
					'opr_code'=>'TD',
					'flexi'=>array('id' => '9','oss'=>'DOC','pp'=>'108','payt'=>'18','cbz'=>'RD','rdu'=>'DOCO','uva'=>'DOCO','apna'=>'D','magic'=>'TD','rio'=>'TD','gem'=>'TD','gitech'=>'HTAD','rkit'=>35,'a2z'=>8,'joinrec'=>1,'mypay'=>8,'smsdaak'=>'TGP',
                            'hitecrec'=>'TD',"simple"=>"D",'practic'=>"Docomo","manglam" =>"RD","bulk"=>"RD"),
					'voucher'=>array('id' => '27','oss'=>'DOC','pp'=>'108','payt'=>'18','cbz'=>'RDR','rdu'=>'DOCOS','uva'=>'DOCO','apna'=>'DS','magic'=>'RD','rio'=>'RD','gem'=>'TL','gitech'=>'HTDS','rkit'=>35,'a2z'=>7,'joinrec'=>1,'smsdaak'=>'TSP','hitecrec'=>'RD','practic'=>"Docomo","simple"=>"DS","manglam" =>"TD")
	      ),
				'10'=>array(
					'operator'=>'Tata Indicom',
					'opr_code'=>'TI',
					'flexi'=>array('id' => '10','oss'=>'TTS','pp'=>'26','payt'=>'3','cbz'=>'DI','rdu'=>'INDI','uva'=>'INDI','apna'=>'T','gem'=>'TA','gitech'=>'HTAI','rkit'=>19,'a2z'=>8,'joinrec'=>1,'mypay'=>10,'smsdaak'=>'TCP','hitecrec'=>'TD',"practic" => "Tataindicom","simple"=>"T","manglam" =>"TI"),
					'voucher'=>array('id' => '27','oss'=>'TTS','pp'=>'26','payt'=>'3','cbz'=>'RDR','rdu'=>'DOCOS','uva'=>'INDI','gitech'=>'HTAI','rkit'=>19,'a2z'=>7,'joinrec'=>1)
	),
				'11'=>array(
					'opr_code'=>'UN',	
					'flexi'=>array('id' => '11','oss'=>'UNI','pp'=>'129','payt'=>'790','cbz'=>'UN','rdu'=>'UNR','uva'=>'UNIN','anand'=>'uninor','apna'=>'U','rio'=>'RU','gem'=>'UN','gitech'=>'HUNI','rkit'=>20,'joinrec'=>26,'mypay'=>9,'smsdaak'=>'UGP','hitecrec'=>'TU',"practic" => "Uninor","simple"=>"U","manglam" =>'RU'),
					'operator'=>'Uninor',
					'voucher'=>array('id' => '29','oss'=>'UNI','pp'=>'129','payt'=>'790','cbz'=>'UNR','rdu'=>'UNRS','uva'=>'UNIN','anand'=>'uninor','apna'=>'US','gem'=>'US','gitech'=>'HUNS','rkit'=>21,'joinrec'=>31,'smsdaak'=>'USP','hitecrec'=>'RU',"simple"=>"US","manglam" =>'TU')
	),
				'12'=>array(
					'operator'=>'Videocon',
					'opr_code'=>'DC',
					'flexi'=>array('id' => '12','oss'=>'VID','pp'=>'134','cbz'=>'VR','rdu'=>'VCON','uva'=>'vidgsm','apna'=>'VD','magic'=>'TE','rio'=>'TE','gem'=>'VT','gitech'=>'HVID','rkit'=>22,'mypay'=>5,'smsdaak'=>'VGP',"simple"=>"VD","manglam" =>"RN"),
					'voucher'=>array('id' => '28','oss'=>'VID','pp'=>'134','cbz'=>'VS','rdu'=>'VCONS','uva'=>'vidgsm','apna'=>'VS','magic'=>'RE','rio'=>'RE','gem'=>'VS','gitech'=>'HVIS','rkit'=>23,'smsdaak'=>'VSP',"simple"=>"VS","manglam" =>"TN")
	),
				'13'=>array(
					'operator'=>'Virgin CDMA',
					'opr_code'=>'VC',
					'flexi'=>array('id' => '13','oss'=>'VR','pp'=>'52','mypay'=>21,"simple"=>"VC","manglam" => "RD"),
					'voucher'=>array('id' => '','oss'=>'VR','pp'=>'52',"simple"=>"VC")
	),
				'14'=>array(
					'operator'=>'Virgin GSM',
					'opr_code'=>'VG',
					'flexi'=>array('id' => '14','oss'=>'VG','pp'=>'52','mypay'=>20,"simple"=>"VG","manglam" => "TD"),
					'voucher'=>array('id' => '','oss'=>'VG','pp'=>'52',"simple"=>"VG")
	),
				'15'=>array(
					'operator'=>'Vodafone',
					'opr_code'=>'VF',
					'flexi'=>array('id' => '15','oss'=>'VF','pp'=>'50','payt'=>'8','cbz'=>'RV','rdu'=>'VODA','uva'=>'VODA','uni'=>'292','anand'=>'vodafone','apna'=>'V','magic'=>'RV','rio'=>'RV','gem'=>'VF','durga'=>'Vodafone','gitech'=>'HVOD','rkit'=>27,'a2z'=>5,'joinrec'=>9,'mypay'=>3,
                            'smsdaak'=>'VFP','aporec'=>6,'hitecrec'=>'RV',"practic"=> "Vodafone","simple"=>"V","manglam" => "RV","bulk"=>"RV","bimco"=> "VF"),
					'voucher'=>array('id' => '','oss'=>'VF','pp'=>'50')
	                  ),
				'30'=>array(
					'operator'=>'MTNL',
					'opr_code'=>'MT',
					'flexi'=>array('id' => '30','payt'=>'13','rdu'=>'MTNL','uva'=>'MTNL','apna'=>'MTT','gem'=>'ML','gitech'=>'HMTN','rkit'=>'12','smsdaak'=>'MMP',"manglam" => "MT"),
					'voucher'=>array('id' => '31','payt'=>'13','rdu'=>'MTNLS','uva'=>'MTNL','apna'=>'MTR','gem'=>'MR','rkit'=>'12','smsdaak'=>'MSP')
	)
	),
			'dthRecharge' =>array(
				'1'=>array(
					'operator'=>'Airtel DTH',
					'flexi'=>array('id' => '16','oss'=>'ADT','pp'=>'152','payt'=>'15','cbz'=>'RH','rdu'=>'DA','uva'=>'AIRTELTV','uni'=>'255','magic'=>'DA','rio'=>'DA','rkit'=>11,'joinrec'=>14,
                            'mypay'=>15,'smsdaak'=>'ATV','hitecrec'=>'DA',"practic" => "AirtelDTH","simple"=>"ATV","manglam" => "DA")
	),
				'2'=>array(
					'operator'=>'Big TV DTH',
					'flexi'=>array('id' => '17','oss'=>'BTV','pp'=>'131','payt'=>'279','cbz'=>'DB','rdu'=>'DB','uva'=>'BIGTV','uni'=>'277','magic'=>'DB','rio'=>'DB','gitech'=>'HBTV','rkit'=>10,'joinrec'=>21,'mypay'=>19,'smsdaak'=>'RTV',"practic" => "BigTV","simple"=>"BTV","manglam" => "DB")
	),
				'3'=>array(
					'operator'=>'Dish TV DTH',
					'flexi'=>array('id' => '18','oss'=>'DIS','pp'=>'128','payt'=>'12','cbz'=>'DD','rdu'=>'DD','uva'=>'DISHTV','magic'=>'DD','rio'=>'DD','rio2'=>'DD','gitech'=>'HDIS','rkit'=>24,'joinrec'=>29,
                            'mypay'=>16,'smsdaak'=>'DTV','aporec'=>17,'hitecrec'=>'DD',"practic" => "DishTV","simple"=>"DTV","manglam" => "DD","bulk"=>"DD")
	),
				'4'=>array(
					'operator'=>'Sun TV DTH',
					'flexi'=>array('id' => '19','oss'=>'SUN','pp'=>'74','payt'=>'11','cbz'=>'DS','rdu'=>'DS','uva'=>'SUNTV','magic'=>'DS','rio'=>'DS','gitech'=>'HSUN','rkit'=>9,'joinrec'=>19,
                            'mypay'=>18,'smsdaak'=>'STV','hitecrec'=>'DS',"practic" => "SunDirect","simple"=>"STV","manglam" => "DS")
	),
				'5'=>array(
					'operator'=>'Tata Sky DTH',
					'flexi'=>array('id' => '20','oss'=>'TAS','pp'=>'44','payt'=>'10','cbz'=>'DT','rdu'=>'DT','uva'=>'TATASKY','magic'=>'DT','rio'=>'DT','rio2'=>'DT','gitech'=>'HTSY','rkit'=>28,'joinrec'=>22,
                            'mypay'=>14,'smsdaak'=>'TTV','hitecrec'=>'DT',"practic" => "Tatasky","simple"=>"TTV","manglam" => "DT","bulk" =>'DT')
	),
				'6'=>array(
					'operator'=>'Videocon DTH',
					'flexi'=>array('id' => '21','oss'=>'D2H','pp'=>'132','payt'=>'20','cbz'=>'VDOC','rdu'=>'DV','uva'=>'videocond2h','magic'=>'DV','rio'=>'DV','gitech'=>'HVIH','rkit'=>4,'joinrec'=>23,
                            'mypay'=>17,'smsdaak'=>'VTV','hitecrec'=>'DV',"practic" => "VideoconDTH","simple"=>"VTV","manglam" => "DV","bulk" => "DV")
	              )
	),
            'busBooking' =>array(
				'1'=>array(
					'operator'=>'Red Bus',
					'flexi'=>array('id' => '42')
	)
	),

            'billPayment' =>array(
                                            '1'=>array(
                                                    'operator'=>'Docomo Postpaid',
                                                    'flexi'=>array('id' => '36','apna'=>'PD','gitech'=>'HTDP','smsdaak'=>'TDC')
	),
                                            '2'=>array(
                                                    'operator'=>'Loop Mobile PostPaid',
                                                    'flexi'=>array('id' => '37','apna'=>'PLM')
	),
                                            '3'=>array(
                                                    'operator'=>'Cellone PostPaid',
                                                    'flexi'=>array('id' => '38','apna'=>'PCL','gitech'=>'HBSP','smsdaak'=>'BGC')
	),
                                            '4'=>array(
                                                    'operator'=>'IDEA Postpaid',
                                                    'flexi'=>array('id' => '39','apna'=>'IP','gitech'=>'HIDP','rkit'=>37,'smsdaak'=>'IDC')
	),
                                            '5'=>array(
                                                    'operator'=>'Tata TeleServices PostPaid',
                                                    'flexi'=>array('id' => '40','apna'=>'PTT','joinrec'=>37,'rkit'=>42)
	),
                                            '6'=>array(
                                                    'operator'=>'Vodafone Postpaid',
                                                    'flexi'=>array('id' => '41','uni'=>'266','apna'=>'VP','gitech'=>'HVOP','rkit'=>38,'smsdaak'=>'VFC')
	),
											'7'=>array(
                                                    'operator'=>'Airtel Postpaid',
                                                    'flexi'=>array('id' => '42','apna'=>'AP','joinrec'=>30,'gitech'=>'HATP','rkit'=>36,'smsdaak'=>'ATC')
	),
											'8'=>array(
                                                    'operator'=>'Reliance GSM Postpaid',
                                                    'flexi'=>array('id' => '43','apna'=>'RGP','joinrec'=>32,'gitech'=>'HREP','rkit'=>39,'smsdaak'=>'RGC')
	)
	),
	'utilityBillPayment' =>array(
                                            '1'=>array(
                                                    'operator'=>'Reliance Energy (Mumbai)',
                                                    'flexi'=>array('id' => '45','smsdaak'=>'REE')
	),
                                            '2'=>array(
                                                    'operator'=>'BSES Rajdhani',
                                                    'flexi'=>array('id' => '46','smsdaak'=>'BRE')
	),
                                            '3'=>array(
                                                    'operator'=>'BSES Yamuna',
                                                    'flexi'=>array('id' => '47','smsdaak'=>'BYE')
	),
                                            '4'=>array(
                                                    'operator'=>'North Delhi Power Limited',
                                                    'flexi'=>array('id' => '48','smsdaak'=>'NDE')
	),
                                            '5'=>array(
                                                    'operator'=>'Airtel Landline',
                                                    'flexi'=>array('id' => '49','smsdaak'=>'ATL')
	),
                                            '6'=>array(
                                                    'operator'=>'MTNL Delhi Landline',
                                                    'flexi'=>array('id' => '50','smsdaak'=>'MDL')
	),
											'7'=>array(
                                                    'operator'=>'Mahanagar Gas Limited',
                                                    'flexi'=>array('id' => '51','smsdaak'=>'MMG')
	)
	)
	);
        
        var $cp_opr_map = array(
            '1'=>array('op_short_code'=>'ac','op_code'=>'1'),
            '2'=>array('op_short_code'=>'at','op_code'=>'0'),
            '3'=>array('op_short_code'=>'mm','op_code'=>'205'),
            '34'=>array('op_short_code'=>'mm','op_code'=>'219'),
            '4'=>array('op_short_code'=>'id','op_code'=>'0'),
            '5'=>array('op_short_code'=>'lm','op_code'=>'0'),
            '6'=>array('op_short_code'=>'mt','op_code'=>'0'),
            '7'=>array('op_short_code'=>'rl','op_code'=>'0'),
            '8'=>array('op_short_code'=>'rl','op_code'=>'0'),
            '9'=>array('op_short_code'=>'dc','op_code'=>'0'),
            '27'=>array('op_short_code'=>'dc','op_code'=>'0'),
            '10'=>array('op_short_code'=>'tt','op_code'=>'0'),
            '11'=>array('op_short_code'=>'un','op_code'=>'0'),
            '29'=>array('op_short_code'=>'un','op_code'=>'0'),
            '12'=>array('op_short_code'=>'vm','op_code'=>'0'),
            '28'=>array('op_short_code'=>'vm','op_code'=>'0'),
            '15'=>array('op_short_code'=>'vd','op_code'=>'0'),
            '30'=>array('op_short_code'=>'mm','op_code'=>'212'),
            '31'=>array('op_short_code'=>'mm','op_code'=>'215'),
            '16'=>array('op_short_code'=>'ad','op_code'=>'0'),
            '17'=>array('op_short_code'=>'bt','op_code'=>'0'),
            '18'=>array('op_short_code'=>'dt','op_code'=>'0'),
            '19'=>array('op_short_code'=>'mm','op_code'=>'213'),
            '20'=>array('op_short_code'=>'ts','op_code'=>'0'),
            '21'=>array('op_short_code'=>'vc','op_code'=>'0'),
            '36'=>array('op_short_code'=>'bu','op_code'=>'228'),
            '37'=>array('op_short_code'=>'bu','op_code'=>'230'),
            '38'=>array('op_short_code'=>'bu','op_code'=>'231'),
            '39'=>array('op_short_code'=>'bu','op_code'=>'232'),
            '40'=>array('op_short_code'=>'bu','op_code'=>'233'),
            '41'=>array('op_short_code'=>'bu','op_code'=>'234'),
            '42'=>array('op_short_code'=>'ad','op_code'=>'225'),
            '43'=>array('op_short_code'=>'rl','op_code'=>'251'),
            '45'=>array('op_short_code'=>'bu','op_code'=>'235'),
            '46'=>array('op_short_code'=>'bu','op_code'=>'236'),
            '47'=>array('op_short_code'=>'bu','op_code'=>'237'),
            '48'=>array('op_short_code'=>'bu','op_code'=>'238'),
            '49'=>array('op_short_code'=>'bu','op_code'=>'239'),
            '50'=>array('op_short_code'=>'bu','op_code'=>'240'),
            '51'=>array('op_short_code'=>'bu','op_code'=>'241'),
            );
        
        var $cpurls = array();
	
	function beforeFilter() {parent::beforeFilter ();$this->Auth->allow('*'); $this->create_cp_url_mapping(); }
            
        function create_cp_url_mapping(){
            $cp_url = CYBERP_URL;
            $cpurls = array();
            foreach ($this->cp_opr_map as $key=>$value){
                $cpv = ($value['op_code'] > 0 ) ? $cp_url.$value['op_short_code'].'/'.$value['op_short_code'].'_pay_check.cgi/'.$value['op_code']:$cp_url.$value['op_short_code'].'/'.$value['op_short_code']."_pay_check.cgi";
                $cpl = ($value['op_code'] > 0 ) ? $cp_url.$value['op_short_code'].'/'.$value['op_short_code'].'_pay.cgi/'.$value['op_code']:$cp_url.$value['op_short_code'].'/'.$value['op_short_code']."_pay.cgi";
                $cps = $cp_url.$value['op_short_code'].'/'.$value['op_short_code'].'_pay_status.cgi';
                $this->cpurls[$key] = array('cpv'=>$cpv,'cpl'=>$cpl,'cps'=>$cps);
            }
        }
	//reversal

	function modemTransactionStatus($transId = null,$data = null){
		if($transId == null){
			$data = $_REQUEST;
			//$ip = trim($_SERVER['REMOTE_ADDR']);
		}
		$vendor = $data['vendor'];
//		if(isset($ip) && !empty($ip)){
//			$this->Shop->healthyVendor($vendor,$ip);
//		}
		if(!isset($data['trans_id']))$TransactionIds = trim($transId);
		else $TransactionIds = trim($data['trans_id']);

		$cause = urldecode($data['cause']);
		$status = $data['status'];

       
		if (empty($TransactionIds))
            return;
        $ids = explode(",", $TransactionIds);
        $vendorIds = explode(",", $data['vendor_id']);
        $opr_ids = explode(",", $data['opr_id']);
        $products = explode(",", $data['prod_id']);
        
        $causes = explode(",", $cause);

        if (isset($data['processing_time'])) {
            $process_times = explode(",", $data['processing_time']);
            $sim_bals = explode(",", $data['sim_balance']);
            $sim_nos = explode(",", $data['sim_no']);
        } else {

            $process_times = array();
            $sim_bals = array();
            $sim_nos = array();
        }
        
         if (isset($data['timestamp'])) {
            $timestamps = explode(",", $data['timestamp']);
         }

         if(isset($data['status']) && $status == "failure" && empty($data['error'])){
             $data['error'] = '';
         }
         
        $i=0;

			$this->General->logData("/mnt/logs/modemTxns.txt",date('Y-m-d H:i:s')."::$vendor: ".json_encode($data));
		foreach($ids as $TransactionId){
			$vendorId = $vendorIds[$i];
			$opr_id = $opr_ids[$i];
			$product = $products[$i];
			$pro_time = isset($process_times[$i])?$process_times[$i]:'';
			$timestamp = isset($timestamps[$i])?$timestamps[$i]:'';
			$sim_bal = isset($sim_bals[$i])?$sim_bals[$i]:0;
			$sim_no = isset($sim_nos[$i])?$sim_nos[$i]:0;
			$cause = isset($causes[$i])?$causes[$i]:$cause;
			$i++;

			$TransactionId = trim($TransactionId);
			//$vm = $this->User->query("SELECT vendors_activations.id,vendor_refid,status,service_id,vendor_id,timestamp,product_id,date,operator_id FROM vendors_activations LEFT JOIN products ON (vendors_activations.product_id=products.id) WHERE ref_code like '$TransactionId'");
			$vm = $this->User->query("SELECT vendors_activations.id,vendors_activations.vendor_refid,vendors_activations.status,products.service_id,vendor_id,vendors_activations.timestamp,product_id,date,operator_id,vm.timestamp,complaintNo,vendors.company FROM vendors_activations LEFT JOIN vendors ON (vendors.id = vendor_id) LEFT JOIN products ON (vendors_activations.product_id=products.id) left join vendors_messages as vm ON (vm.shop_tran_id=ref_code AND vm.service_vendor_id=vendor_id AND vm.status='pending') WHERE ref_code like '$TransactionId'");
			
			$this->General->logData("/mnt/logs/modemTxns.txt",date('Y-m-d H:i:s')."::$vendor:: $TransactionId: ".json_encode($vm));
		
			if(empty($vm)){
				$log = $this->User->getDataSource()->getLog(false,false);
				$count = count($log['log']);
				
				$this->General->logData("/mnt/logs/modemTxns.txt",date('Y-m-d H:i:s')."::ids: ".json_encode($ids)."::query: SELECT vendor_refid,status,service_id,vendor_id,timestamp,product_id,date,operator_id FROM vendors_activations LEFT JOIN products ON (vendors_activations.product_id=products.id) WHERE ref_code like '$TransactionId':::  ".json_encode($log));
					
				if(strtolower($status) == 'success'){
					$this->General->sendMails('Modem: Txn not found for modem',"$TransactionId <br/>Txn not found for vendor $vendor<br/>Ref id: $vendorId<br/>Operator txnid: $opr_id<br/>IP: $ip<br/>Query fired: SELECT vendor_refid,status,service_id,vendor_id,timestamp,product_id,date,operator_id FROM vendors_activations LEFT JOIN products ON (vendors_activations.product_id=products.id) WHERE ref_code like '$TransactionId'",array('ashish@mindsarray.com'));
				}
			}
			else {
				if(!empty($timestamp) && !empty($vm['0']['vm']['timestamp']) && $vendor == $vm['0']['vendors_activations']['vendor_id']){
					$timediff = strtotime($vm['0']['vm']['timestamp']) - strtotime($timestamp);
					$pro_time1 = $pro_time;
					$pro_time = date('Y-m-d H:i:s',strtotime($pro_time.'+ '.$timediff.' seconds'));
					if(time() - strtotime($pro_time) > 86400){
						$pro_time = $pro_time1;
					}
				}
				
				if(strtolower($status) == 'success' && $vendor == $vm['0']['vendors_activations']['vendor_id']){
					if($vm['0']['vendors_activations']['status'] == 2 || $vm['0']['vendors_activations']['status'] == 3){
						//$this->User->query("UPDATE vendors_transactions SET status=1,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal' WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
						$this->General->update_in_vendor_transaction("status=1,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal'","ref_id='$TransactionId',vendor_id=$vendor");
                                                        
						$res = $this->User->query("SELECT timestamp FROM vendors_messages WHERE shop_tran_id='$TransactionId' AND service_vendor_id='$vendor' AND status = 'failure'");
			
						if(!empty($res) && empty($vm['0']['vendors_activations']['complaintNo']) && $res['0']['vendors_messages']['timestamp'] >= date('Y-m-d H:i:s',strtotime('-12 hours'))){
							$this->General->sendMails('Modem: Autopullback',"$TransactionId <br/>Txn found for vendor ".$vm['0']['vendors']['company'].", Previous status was ".$vm['0']['vendors_activations']['status']."<br/>Operator txnid: $opr_id",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
							$this->Shop->autopullbackTransaction($TransactionId,$vendor);	
						}
						else {
							$this->General->sendMails('Modem: Success after failure problem',"$TransactionId <br/>Txn found for vendor ".$vm['0']['vendors']['company'].", Previous status was ".$vm['0']['vendors_activations']['status']."<br/>Ref id: $vendorId<br/>Operator txnid: $opr_id<br/>IP: $ip",array('ashish@mindsarray.com','chirutha@mindsarray.com','lalit@mindsarray.com'),'mail');
							$this->User->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$vm[0]['vendors_activations']['id']."','".$vendor."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");	
						}
					}
					else {
						if(isset($pro_time)){
							//$this->User->query("UPDATE vendors_transactions SET status=1,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal' WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
                                                        $this->General->update_in_vendor_transaction("status=1,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal'","ref_id='$TransactionId',vendor_id=$vendor");
						}
						
						if(!$this->Shop->checkStatus($TransactionId,$vendor)){
							$this->General->logData("/var/www/html/shops/status.txt","status: $TransactionId: false");
							continue;
						}
						//$this->General->logData("/var/www/html/shops/status.txt","$ip: $status: $TransactionId");

						$this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
						$this->User->query("UPDATE vendors_activations SET prevStatus =status,status='".TRANS_SUCCESS."',operator_id='$opr_id' WHERE ref_code='".$TransactionId."'");
						$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$TransactionId."','$vendorId','1',$vendor,'13','".addslashes($cause)."','success','".date("Y-m-d H:i:s")."')");
						$this->Shop->addStatus($TransactionId,$vendor);
						

					}

				}
				else if(((strtolower($status) == 'failure') || (!empty($data['error_desc']) && $data['error_desc'] == 'NA')) && $vendor == $vm['0']['vendors_activations']['vendor_id']){
					if($vm['0']['vendors_activations']['status'] == 1){
						$this->General->sendMails('Modem: Failure after Success problem',"Kindly check the txn and fail it manually<br/>$TransactionId <br/>Txn found for vendor ".$vm['0']['vendors']['company'].", Previous status was ".$vm['0']['vendors_activations']['status']."<br/>Ref id: $vendorId<br/>Operator txnid: $opr_id<br/>IP: $ip",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
						
						continue;
					}
					
					if(isset($pro_time)){
						//$this->User->query("UPDATE vendors_transactions SET status=2,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal' WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
                                                $this->General->update_in_vendor_transaction("status=2,processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal'","ref_id='$TransactionId',vendor_id=$vendor");
					}
					if(!$this->Shop->checkStatus($TransactionId,$vendor)){
						continue;
					}
					//$this->General->logData("/var/www/html/shops/status.txt","$ip: $status: $TransactionId");

					$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$TransactionId."','$vendorId','1',$vendor,'14','".addslashes($cause)."','failure','".date("Y-m-d H:i:s")."')");
					
					$this->Shop->addStatus($TransactionId,$vendor);

					$try = false;
					if(isset($data['error'])){
						$err_code = $data['error'];

						
                        // ----------------------tata adhock changes
                        $err_flag_chk = $this->Shop->getMemcache("ERR_FLAG_".$TransactionId);
                        if( $err_flag_chk === false && in_array($product,array(9,10,27)) && $err_code == 3 ){
                            $this->Shop->setMemcache("ERR_FLAG_".$TransactionId,"1",60*5);
                            $err_code = 6;
                        }
                            $this->changeTataId($err_code,$product,$TransactionId);
                            
						if($err_code != 3 && $err_code != 4){
							$try = $this->transferToOtherRoutes($TransactionId,$vendor);
						}
					}

					 $errormapping = array("3"=>"6","1"=>"37","4" => "5","2" => "47","5" => "47","6"=>"6");
					 
					if(!$try){
						$this->Shop->reverseTransaction($TransactionId,null,$errormapping[$err_code]);
						$this->Shop->deleteStatus($TransactionId,$vendor);
					}
				}
				else if((strtolower($status) == 'pending' || strtolower($status) == 'inprocess') && $vendor == $vm['0']['vendors_activations']['vendor_id']){
					if(isset($pro_time)){
						//$this->User->query("UPDATE vendors_transactions SET processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal' WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
                                                $this->General->update_in_vendor_transaction("processing_time='$pro_time',sim_num='$sim_no',closing_bal='$sim_bal'","ref_id='$TransactionId',vendor_id=$vendor");
					}
				}
				else if(!empty($vendor) && $vendor != $vm['0']['vendors_activations']['vendor_id']){
					$check = $this->User->query("SELECT status FROM vendors_transactions WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
					if(strtolower($status) == 'success'){
						//$this->User->query("UPDATE vendors_transactions SET status=1,sim_num='$sim_no',closing_bal='$sim_bal' WHERE ref_id='$TransactionId' AND vendor_id = $vendor");
                                                $this->General->update_in_vendor_transaction("status=1,sim_num='$sim_no',closing_bal='$sim_bal'","ref_id='$TransactionId',vendor_id=$vendor");
                                                
						$this->General->sendMails('Modem: Success after failure problem (Double Transaction)',"$TransactionId <br/>Txn found for vendor ".$vm['0']['vendors']['company'].", Previous status was ".$vm['0']['vendors_activations']['status']."<br/>Ref id: $vendorId<br/>Operator txnid: $opr_id<br/>IP: $ip",array('ashish@mindsarray.com','backend@mindsarray.com','lalit@mindsarray.com'),'mail');
						$this->User->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$vm[0]['vendors_activations']['id']."','".$vendor."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");
					}
				}
			}
		}
	}
	
	function updateModemBalance(){

		$vendor = trim($_REQUEST['vendor']);
		$this->General->logData("/mnt/logs/modem_balance.txt",date('Y-m-d H:i:s')."::$vendor");

		$bal = trim($_REQUEST['balance']);
                    	$bal1 = json_decode($_REQUEST['balance'],true);
                
             
		//$ip = trim($_SERVER['REMOTE_ADDR']);
		$this->Shop->healthyVendor($vendor);
                
                                    if(!empty($bal1)){
			$this->Shop->setMemcache("balance_$vendor",$_REQUEST['balance'],5*24*60*60);
			$this->Shop->setMemcache("balance_timestamp_$vendor"."_last",date('Y-m-d H:i:s'),5*24*60*60);
			if(isset($_REQUEST['ports'])){
				$this->Shop->setMemcache("balance_ports_$vendor",$_REQUEST['ports'],5*24*60*60);
			}

			/*
			 * Set SimPanel Data into Memcache & Devices table
			 * Start
			 */
                                                        $this->sdiff=array();        
			$this->pushSimsintoMemcache($vendor,$_REQUEST['balance']);
			$this->pushSimsintoDevices_data($vendor,$_REQUEST['balance']);
                                                       /*
			 * End
			 */
			
                           	}
		else {
			$bal = $this->Shop->getMemcache("balance_$vendor");
			if($bal === false){
				$this->Shop->setMemcache("balance_$vendor",json_encode(array()),5*24*60*60);
				$this->Shop->setMemcache("balance_timestamp_$vendor"."_last",date('Y-m-d H:i:s'),5*24*60*60);
				if(isset($_REQUEST['ports'])){
					$this->Shop->setMemcache("balance_ports_$vendor",$_REQUEST['ports'],5*24*60*60);
				}
			}
		}

		/*if(isset($_REQUEST['simstatus'])){
			$status = json_decode($_REQUEST['simstatus'],true);
			foreach($status as $opr=>$val){
			//$this->Shop->setMemcache("status_$opr"."_".$vendor,intval($val['total']),24*60*60);
			if(isset($val['50']))
			$this->Shop->setMemcache("status_$opr"."_50_".$vendor,intval($val['50']),24*60*60);
			if(isset($val['30']))
			$this->Shop->setMemcache("status_$opr"."_30_".$vendor,intval($val['30']),24*60*60);
			if(isset($val['10']))
			$this->Shop->setMemcache("status_$opr"."_10_".$vendor,intval($val['10']),24*60*60);
			if(isset($val['0']))
			$this->Shop->setMemcache("status_$opr"."_0_".$vendor,intval($val['0']),24*60*60);
			}
			}*/



		 

		$this->autoRender = false;
	}
        
        
                  public function pushSimsintoDevices_data($modem_id,$simInfo=null)
                  {
                      $this->autoRender = false;
                      
                        $temp_s_o_id=array();
                        $sqlarray=array();
                        $so_ids=  $this->Slaves->query("Select id,supplier_id,operator_id from inv_supplier_operator");

                        foreach ($so_ids as $so_id):
                        $temp_s_o_id[$so_id['inv_supplier_operator']['supplier_id']."_".$so_id['inv_supplier_operator']['operator_id']]=$so_id['inv_supplier_operator']['id'];
                        endforeach;
                        
                         $vendor_id=$modem_id;
                         
                           $data=$simInfo; 
                          $date=date('Y-m-d');
                         
                           if(!empty($data)):
                     			$data_before_insert = $this->Slaves->query("SELECT sum(sale) as totsale, count(id) as cts from devices_data where sync_date='{$date}' AND vendor_id='{$vendor_id}' ");
                                sleep(1);
                     			
                                $tot_sale = 0;
                                $tot_count = 0;
                          
                                            $data=  json_decode($data);

                                            foreach($data as $sim):
                                                    $sql="";
                                                    //if($sim->inv_supplier_id>0 && $sim->opr_id>0):
                                            
                                                            $suppplier_operator_id=  array_key_exists($sim->inv_supplier_id."_".$sim->opr_id,$temp_s_o_id)?$temp_s_o_id[$sim->inv_supplier_id."_".$sim->opr_id]:0;
			    $curr_timestamp = date("Y-m-d H:i:s");
                                                            $sql.= "('{$vendor_id}',";
                                                            $sdiffkey=$sim->opr_id."_".$sim->mobile."_".$sim->vendor;
                                                            $process_time=$sim->process_time?$sim->process_time:"";
                                                            $server_diff=isset($this->sdiff[$sdiffkey])?$this->sdiff[$sdiffkey]:0;
                                                            $sql.="'{$sim->id}','{$suppplier_operator_id}','{$sim->inv_supplier_id}','{$date}','{$curr_timestamp}','{$sim->machine_id}','{$sim->scid}','{$sim->mobile}','{$sim->operator}','{$sim->circle}','{$sim->type}',";
                                                            $sql.="'{$sim->pin}','{$sim->balance}','{$sim->opr_id}','{$sim->commission}','{$sim->limit}','{$sim->limit_today}','{$sim->roaming_limit}',";
                                                            $sql.="'{$sim->roaming_today}','".addslashes($sim->vendor)."','{$sim->last_block_date}','".addslashes($sim->vendor_tag)."','{$sim->block}',";
                                                            $sql.="'{$sim->device_num}','{$sim->bus}','{$sim->bus_dev}','{$sim->par_bal}','{$sim->state}',";
                                                            $sql.="'{$sim->active_flag}','{$sim->recharge_flag}','{$sim->receive_flag}','{$sim->stop_flag}','{$sim->signal}',";
                                                            $sql.="'{$sim->tfr}','{$sim->inc}','{$sim->opening}','{$sim->sale}','{$sim->success}','{$sim->last}','{$server_diff}','{$process_time}','{$sim->recharge_method}','{$sim->block_tag}','{$sim->bal_range}' ";
                                                            $sql.=")";
                                                            $sqlarray[]=$sql;
                                                            $tot_sale += $sim->sale;
                                                            $tot_count++;

                                                    //endif;
                                            endforeach;

                                             $dataSource=$this->User->getDataSource();
                                             $dataSource->begin();
                                             
                                              $isDeleted=$this->User->query("Delete from devices_data where sync_date='{$date}' AND vendor_id='{$vendor_id}' ");
                                              
                                             $batch=  $this->createSqlBatch($sqlarray);

                                            if(!empty($batch)):
                                                      $f=1;  foreach($batch as $q):
                                                                        if(!$this->User->query($q)):  
                                                                                 $this->General->logData('/mnt/logs/realtimesyncdata'.date('Y-m-d').'.txt',$q);
                                                                                 $f=0; break;
                                                                         endif;
                                                                 endforeach;
                                            endif;
                                            
                                            
                                sleep(1);
                             //   $data_after_insert = $this->User->query("SELECT sum(sale) as totsale, count(id) as cts from devices_data where sync_date='{$date}' AND vendor_id='{$vendor_id}' ");
                                $mail_body = "Vendor: $vendor_id , timestamp:" . date('Y-m-d H:i:s');
                                $mail_body .= "<br/>Sale before new data: " . $data_before_insert[0][0]['totsale'];
                                $mail_body .= "<br/>Sale in new data: " . $tot_sale;
                            

                                $mail_body .= "<br/>Sim count before new data: " . $data_before_insert[0][0]['cts'];
                                $mail_body .= "<br/>Sim count in new data: " . $tot_count;

                                try{
                                        if($isDeleted && $f && ($tot_sale-$data_before_insert[0][0]['totsale']>=-10000 && $tot_sale-$data_before_insert[0][0]['totsale']<=50000) || (date('H:i')<'00:10') ):
                                              $dataSource->commit();
                                        else:
                                             $dataSource->rollback();
                                             $this->General->sendMails('Modem data mismatch', $mail_body, array('ashish@mindsarray.com', 'vibhas@mindsarray.com', 'nandan@mindsarray.com', 'lalit@mindsarray.com','chetan@mindsarray.com'), 'mail');
                                        endif;
                                }
                                catch(Exception $e){
                                      $dataSource->rollback();
                                      $this->General->sendMails('Modem data mismatch', $mail_body, array('ashish@mindsarray.com', 'vibhas@mindsarray.com', 'nandan@mindsarray.com', 'lalit@mindsarray.com','chetan@mindsarray.com'), 'mail');
                                }
                                
                                echo "Done";
                            endif;
            
                  }
                  
                   public function createSqlBatch($sqlarray)
                            {
                                if(!empty($sqlarray)):

                                     $tobeReturned=""; 
                                     $mainsql="INSERT  INTO devices_data(vendor_id,"
                                                                              . "device_id,supplier_operator_id,inv_supplier_id,sync_date,sync_timestamp,machine_id,scid,mobile,operator,circle,type,"
                                                                              . "pin,balance,opr_id,commission,`limit`,limit_today,roaming_limit,"
                                                                              . "roaming_today,vendor,last_block_date,vendor_tag,block,"
                                                                              . "device_num,bus,bus_dev,par_bal,state,"
                                                                              . "active_flag,recharge_flag,receive_flag,stop_flag,`signal`,"
                                                                              . "tfr,inc,opening,sale,success,`last`,server_diff,process_time,recharge_method,blocktag_id,bal_range) values";
                                     $i=0;

                                     $temp=  array_chunk($sqlarray,100);

                                     foreach($temp as $key=>$value):
                                        $tobeReturned[]=$mainsql.implode(',',$value);
                                     endforeach;


                                     return $tobeReturned;

                              endif;

                              return ;


                            }
       
                   public function pushSimsintoMemcache($modem_id,$simInfo=null)
                   {
                       $this->autoRender = false;
                       
                      //$simInfo=$this->Shop->getMemcache("balance_$modem_id");
                       
                       $date=date('Y-m-d');
                       
                        if(!empty($simInfo)):

                                        $simInfo=json_decode($simInfo);

                                         // Get Server Diff
                                         $diffs=  $this->getServerDiffByModemId($modem_id, $date);

                                         // Push Server Diff into sims 
                                         $simInfo=  $this->setServerDiff($simInfo,$diffs);

                                         $UniqueSupplierList=array();

                                         // Iterate Operators array
                                           $operators = $this->Shop->getProducts();
                                           
                                          foreach($operators as $key=>$operator):

                                              foreach ($simInfo as $value):

                                                     $value=(object)$value;

                                                      if($value->opr_id==$operator['products']['id']):

                                                             $tobeReturned[$operator['products']['id']]['products']['name']=$operator['products']['name'];
                                                             $tobeReturned[$operator['products']['id']]['products']['id']=$operator['products']['id'];

                                                             //$this->__fillUniqueSupplierList($value); 

                                                             if(!in_array(strtolower(trim($value->vendor_tag)),$UniqueSupplierList) && !empty($value->vendor_tag)):

                                                             $UniqueSupplierList[]=strtolower(trim($value->vendor_tag));

                                                             endif;

                                                             @$tobeReturned[$operator['products']['id']]['products']['totalSims']+=1;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalActiveSims']+=(($value->active_flag!=0) && ($value->block!=1) && ($value->stop_flag==0) && ($value->balance >10))?1:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalBalance']+=$value->balance;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalBlockedSims']+=$value->block?1:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalStoppedSims']+=($value->state==2)?1:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalBlockedBalance']+=($value->block)?$value->balance:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalSale']+=$value->sale;

                                                             @$tobeReturned[$operator['products']['id']]['products']['totalOpening']+=$value->opening;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalIncoming']+=$value->tfr;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalIncomingClo']+=$value->inc?$value->inc:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalClosing']+=$value->closing?$value->closing:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalHomeSale']+=($value->opr_id=='4' && $value->sale>0)?($value->sale-$value->roaming_today):0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalRoamingSale']+=$value->roaming_today?$value->roaming_today:0;


                                                             @$tobeReturned[$operator['products']['id']]['products']['totalServerDiffnew']+=($value->serverDiffnew>0)?$value->serverDiffnew:0;


                                                       endif;

                                             endforeach;

                                     endforeach;

                                     
                                     foreach($tobeReturned as $key=>$value):
                                         
                                                $tobeReturned[$key]['products']['suppliers']=  $this->__getReportBySupplierName($value['products']['id'],$UniqueSupplierList,$simInfo);
                                     
                                     endforeach;
                                     
                                     $data['operators']=$tobeReturned;
                                     
                                     $data['last']=$this->Shop->getMemcache("balance_timestamp_$modem_id"."_last");
                                     $data['ports']=$this->Shop->getMemcache("balance_ports_$modem_id");
                                     
                                     
                                     $this->Shop->setMemcache("DistinctOperatorwiseReportByModemId_$modem_id",json_encode($data,5*24*60*60));
                                     
                              endif;
                                 
                     
                        
                        
                   }
                   
                    function  __getReportBySupplierName($operator_id,$UniqueSupplier,$simInfo)
     {
                        
                            $supplierList=  $UniqueSupplier;

                            foreach($supplierList as $key=>$supplier):

                            $totalSims=0;
                            $totalActiveSims=0;
                            $totalBalance=0;
                            $totalBlockedSims=0;
                            $totalStoppedSims=0;
                            $totalBlockedBalance=0;
                            $totalSale=0;
                            $totalOpening=0;
                            $totalIncoming=0;
                            $totalClosing=0;
                            $totalIncomingClo=0;
                            $totalHomeSale=0;
                            $totalRoamingSale=0;
                            $totalServerDiffnew=0;

                            $sims=array();

                                                    foreach ($simInfo as $value):

                                                            if((strtolower(trim($value->vendor_tag))==strtolower($supplier)) && ($value->opr_id==$operator_id) && !empty($value->vendor_tag)):

                                                            $totalSims++;
                                                            $totalActiveSims+=(($value->active_flag!=0) && ($value->block!=1) && ($value->state!=2))?1:0;
                                                            $totalBalance+=$value->balance;
                                                            $totalBlockedSims+=$value->block?1:0;
                                                            $totalStoppedSims+=($value->state==2)?1:0;
                                                            $totalBlockedBalance+=($value->block)?$value->balance:0;


                                                            $totalSale+=$value->sale;
                                                            @$totalOpening+=$value->opening;
                                                            $totalIncoming+=$value->tfr;
                                                            $totalIncomingClo+=$value->inc;
                                                            @$totalClosing+=$value->closing;

                                                            $totalHomeSale+=($value->opr_id=='4' && $value->sale>0)?($value->sale-$value->roaming_today):0;
                                                            $totalRoamingSale+=$value->roaming_today;

                                                            $totalServerDiffnew+=$value->serverDiffnew;

                                                            // $value->serverDiff=$this->__setServerDiff($value);
                                                            $sims[]=$value;


                                                            endif;

                                                    endforeach;

                                                    if($totalSims):

                                                    $tobeReturned[$supplier]=array(
                                                    'totalSims'=>$totalSims,
                                                    'totalActiveSims'=>$totalActiveSims,
                                                    'totalBalance'=>$totalBalance,
                                                    'totalBlockedSims'=>$totalBlockedSims,
                                                    'totalStoppedSims'=>$totalStoppedSims,
                                                    'totalBlockedBalance'=>$totalBlockedBalance,
                                                    'totalSale'=>$totalSale,
                                                    'totalOpening'=>$totalOpening,
                                                    'totalIncoming'=>$totalIncoming,
                                                    'totalClosing'=>$totalClosing,
                                                    'totalIncomingClo'=>$totalIncomingClo,
                                                    'totalHomeSale'=>$totalHomeSale,
                                                    'totalRoamingSale'=>$totalRoamingSale,
                                                    'totalServerDiffnew'=>$totalServerDiffnew
                                                    );

                                                    $tobeReturned[$supplier]['sims']=$sims;

                                                    endif;




                            endforeach;

                            return $tobeReturned;
     }
                   
                   function setServerDiff($simInfo,$diffs=array())
                   {
                       $serverdiff=0;
                       
                       foreach($simInfo as $key=>$value):

                            $simInfo[$key]->serverDiffnew=$serverdiff;

                            $value=(object)$value;

                                        foreach($diffs as $k=>$v):

                                                if(($value->opr_id==$v['operator_id']) && ($value->mobile==$v['sim_num']) && (strtolower(trim($value->vendor))==strtolower(trim($v['vendor'])))):

                                                        $serverdiff=$v['server_diff'];

                                                        $simInfo[$key]->serverDiffnew=$serverdiff;
                                                        
                                                        $this->sdiff[$value->opr_id."_".$value->mobile."_".$value->vendor]=$serverdiff;

                                                        $serverdiff=0;

                                                        break;

                                                endif;

                                        endforeach;

                      endforeach;
                      
                      return $simInfo;
                      
                   }
                   
                    function  getServerDiffByModemId($modem_id,$date,$echo_flag=0)
                    {

                        App::import('Controller','Panels');

                        $Obj=new PanelsController();

                        $Obj->constructClasses();

                        if(!$echo_flag)return @$Obj->get_server_diff_by_vendor($modem_id,$date);
                        else {
                        	echo json_encode(@$Obj->get_server_diff_by_vendor($modem_id,$date));
                        	$this->autoRender = false;
                        }

                    }
	                   
    function ppiReversal(){
		$url = SITE_NAME . "recharges/ppiReversal1";
		$pars['url'] = $_REQUEST['url'];
		$pars['MobileNo'] = $_REQUEST['MobileNo'];
		$pars['CustomerNo'] = $_REQUEST['CustomerNo'];
		$pars['Amount'] = $_REQUEST['Amount'];
		$this->General->curl_post_async($url,$pars);
		$this->autoRender = false;
	}


	function ppiReversal1(){
		$url = explode("=",$_REQUEST['url']);
		$TransactionId = $url[1];
		$MobileNo = $_REQUEST['MobileNo'];
		$CustomerNo = $_REQUEST['CustomerNo'];
		$Amount = $_REQUEST['Amount'];

		$try = false;
		$ref_code = $this->User->query("SELECT vendors_activations.status,vendors_activations.prevStatus,vendors_activations.vendor_id,vendors_activations.ref_code, products.service_id,products.id,vendors_activations.timestamp,vendors_activations.mobile,vendors_activations.amount,vendors_activations.param FROM vendors_activations join  products on (vendors_activations.product_id = products.id) WHERE vendors_activations.vendor_refid= '".$TransactionId."'");
		if(empty($ref_code)){
			if(empty($CustomerNo))
			$ref_code = $this->User->query("SELECT vendors_activations.status,vendors_activations.prevStatus,vendors_activations.vendor_id,vendors_activations.vendor_refid,vendors_activations.ref_code, products.service_id,products.id,vendors_activations.timestamp,vendors_activations.mobile,vendors_activations.amount,vendors_activations.param FROM vendors_activations join  products on (vendors_activations.product_id = products.id) WHERE vendors_activations.mobile= '".$MobileNo."' AND vendors_activations.amount='".$Amount."' AND vendors_activations.vendor_id = 2 order by vendors_activations.id desc limit 1");
			else
			$ref_code = $this->User->query("SELECT vendors_activations.status,vendors_activations.prevStatus,vendors_activations.vendor_id,vendors_activations.vendor_refid,vendors_activations.ref_code, products.service_id,products.id,vendors_activations.timestamp,vendors_activations.mobile,vendors_activations.amount,vendors_activations.param FROM vendors_activations join  products on (vendors_activations.product_id = products.id) WHERE vendors_activations.param= '".$CustomerNo."' AND vendors_activations.amount='".$Amount."' AND vendors_activations.vendor_id = 2 order by vendors_activations.id desc limit 1");
		}


		$vm = $this->User->query("SELECT * FROM vendors_messages WHERE shop_tran_id = '".$ref_code['0']['vendors_activations']['ref_code']."' AND service_vendor_id = 2 AND status = 'failure'");

		if(empty($vm)){
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_code['0']['vendors_activations']['ref_code']."','$TransactionId',".$ref_code['0']['products']['service_id'].",'2','10','','failure','".date("Y-m-d H:i:s")."')");

			if(empty($ref_code)) {
				$this->General->sendMails('ppi reversal no match',json_encode($_REQUEST),array('backend@mindsarray.com'));
				return;
			}

			$try = $this->transferToOtherRoutes($ref_code['0']['vendors_activations']['ref_code'],'ppi');
			if(!$try)$this->Shop->reverseTransaction($ref_code['0']['vendors_activations']['ref_code']);
		}
		else {
			$this->General->sendMails('ppi: double failure',json_encode($_REQUEST),array('backend@mindsarray.com'));
		}
		$this->autoRender = false;
	}
	

	function transferToOtherRoutes($transId,$vendor_name,$lock=false){
		if(!$lock){
			if(!$this->Shop->lockTransaction($transId)){
				return false;
			}
		}

		if($this->Shop->getMemcache("txnExp$transId")){
			$data = $this->Shop->getMemcache("txn$transId");
		}
		else {
			$this->Shop->unlockTransaction($transId);
			return false;
		}

		$try = false;

		$i = 0;
        $addition_params = isset($data['amount'])?array('amount'=>$data['amount']):array();
		$next = $this->Shop->getActiveVendor($data['product_id'],$data['mobile'],null,false,$addition_params);

		if($next['status'] == 'success'){
			$vendors = $next['info']['vendors'];
			$this->General->logData("/mnt/logs/abc.txt","$transId::".$data['product_id']."::already done::".json_encode($data['vendors'])."::all vendors::".json_encode($vendors));
					
			if(true){
				$vendor = $this->Shop->findOptimalVendor($vendors,$data['product_id'],$next['info']['automate'],$data['vendors'],$transId);
				
				while(!empty($vendor)){
                                        if($this->Shop->getMemcache("txnExp$transId")){
                                                $data = $this->Shop->getMemcache("txn$transId");
                                        }
                    $vendor_id = $vendor['vendor_id'];               
					$this->General->logData("/mnt/logs/abc.txt","$transId::".$data['product_id']."::".$vendor['vendor_id']."::next");
						
					$try = true;
					$reply = $this->processTransaction($data,$vendor,$transId);
					$data = $reply['data'];
					$vendorReply = $reply['reply'];
					if($vendorReply['status'] != 'failure')break;
					else {
						if(isset($vendor['key_vendor'])){
							$this->Shop->incrementMemcache($vendor['key_vendor']);
						}
						$vendor = $this->Shop->findOptimalVendor($vendors,$data['product_id'],$next['info']['automate'],$data['vendors'],$transId);
					}
				}
			}
			else {
				foreach($vendors as $vendor){
					if(in_array($vendor['vendor_id'],$data['vendors']))continue;
					$try = true;
		
					$reply = $this->processTransaction($data,$vendor,$transId);
					$data = $reply['data'];
					$vendorReply = $reply['reply'];
					if($vendorReply['status'] != 'failure')break;
				}
			}
			
            $vendorReply['operator_id'] = empty($vendorReply['operator_id']) ? "" : $vendorReply['operator_id'];
            $vendorReply['pinRefNo'] = empty($vendorReply['pinRefNo']) ? "" : $vendorReply['pinRefNo'];
			$this->Shop->updateTransaction($transId,$vendorReply['tranId'],$vendorReply['status'],$vendorReply['code'],$vendorReply['description'],$vendorReply['pinRefNo'],$vendorReply['operator_id'],$vendor_id,$data['product_id'],$vendorReply['disc_comm']);
		}

		$this->Shop->unlockTransaction($transId);
		return $try;
	}
	
	function processTransaction($data,$vendor,$transId){
		$data['vendors'][] = $vendor['vendor_id'];
		$this->Shop->setMemcache("txn$transId",$data,15*60);

		$param = array();
		$param['mobileNumber'] = $data['mobile'];
		$param['amount'] = $data['amount'];

		$param['type'] = "flexi";

		$opr = $this->Shop->smsProdCodes($data['product_id']);
		$param['operator'] = $opr['operator'];
			
		if($data['service_id'] == '1'){
			$funName = $vendor['shortForm']."MobRecharge";
			$param['area'] = $data['area'];
			$param['circle'] = "";
			$param['subId'] = $param['mobileNumber'];
			$funName1 = "modemMobRecharge";
		}
		else if($data['service_id'] == '2'){
			$funName = $vendor['shortForm']."DthRecharge";
			$param['subId'] = $data['param'];
			$funName1 = "modemDthRecharge";
		}
		else if($data['service_id'] == '4'){
			$funName = $vendor['shortForm']."BillPayment";
			$param['area'] = $data['area'];
			$param['circle'] = "";
			$param['subId'] = $param['mobileNumber'];
			$funName1 = "modemBillPayment";
		}
		else if($data['service_id'] == '6'){
			$funName = $vendor['shortForm']."UtilityBillPayment";
			$param['accountNumber'] = $data['param'];
			$param['param'] = $data['extra'];
			$param['circle'] = "";
			$param['subId'] = $param['mobileNumber'];
			$funName1 = "";
		}

		//if($this->User->query("UPDATE vendors_activations SET vendor_id = '".$vendor['vendor_id']."',discount_commission=".$vendor['discount_commission'].",vendor_refid='',status=0 WHERE ref_code = '$transId'")){
                    //$this->General->update_in_vendors_activations("vendor_id='".$vendor['vendor_id']."',discount_commission=".$vendor['discount_commission'].",vendor_refid='',status=0","ref_code='$transId'");
                    $this->General->update_in_vendors_activations(array('vendor_id'=>$vendor['vendor_id'],'discount_commission'=>$vendor['discount_commission'],'vendor_refid'=>'','status'=>0),array('ref_code'=>$transId));
                            
			//$this->User->query("insert into vendors_transactions(ref_id,vendor_id,status,date) values ('$transId',".$vendor['vendor_id'].",0,'".date('Y-m-d')."')");
			$this->General->log_in_vendor_transaction(array($transId,$vendor['vendor_id'],0,date('Y-m-d')));
                        
			if(!method_exists($this, $funName) && method_exists($this, $funName1)){
				$vendorReply = $this->$funName1($transId,$param,$data['product_id'],$vendor['vendor_id'],$vendor['shortForm']);
			}
			else if(method_exists($this, $funName)){
				$vendorReply = $this->$funName($transId,$param,$data['product_id']);	
			}
			else {
				$vendorReply['status'] = 'failure';
			}
			$vendorReply['disc_comm'] = $vendor['discount_commission'];
			//$this->General->sendMails("Transaction Failed From $vendor_name","Re-tried from ".$vendor['vendors']['shortForm'] . "<br/>Transaction Id: " . $ref_code['0']['vendors_activations']['ref_code'],array('ashish@mindsarray.com','dinesh@mindsarray.com'));

			//if($vendorReply['status'] != 'failure' || in_array($vendorReply['code'],array(5,6))) break;
				
			return array('data'=>$data,'reply'=>$vendorReply);
			//if($vendorReply['status'] != 'failure')break;
		//}
	}

	function reverseTransaction($ref_id, $session = null){
		$this->autoRender = false;
		if($_SERVER['HTTP_HOST'] != PROCESSOR_HOSTNAME){
			if(empty($_SESSION['Auth']['User']['group_id'])){
				echo "NOT allowed";
				exit;
			}
			$session = urlencode($_SESSION['Auth']['User']['group_id']. "_" .$_SESSION['Auth']['User']['id'] . "_" . $_SESSION['Auth']['User']['name']);
			$out = $this->General->curl_post(TRANS_REVERSAL_URL."$ref_id/$session",null,'GET');
			echo $out['output'];
		}
		else {
			if(!empty($session)){
				$session = urldecode($session);
				$session = explode("_",$session);
				$grpId = $session[0];
				$usrId = $session[1];
				$usrName = $session[2];
			}
			else {
				$grpId = $_SESSION['Auth']['User']['group_id'];
				$usrId = $_SESSION['Auth']['User']['id'];
				$usrName = $_SESSION['Auth']['User']['name'];
			}
			///if(in_array($grpId,array(ADMIN,CUSTCARE,16,19,20,21)) || $usrId == 1){
				
				$ref_code = $this->Slaves->query("SELECT vendors_activations.*,vendors.update_flag,vendors.shortForm, products.service_id,products.id,`vendors_transactions`.sim_num "
                        . "FROM vendors_activations join  products on (vendors_activations.product_id = products.id) "
                        . "LEFT JOIN vendors ON (vendors_activations.vendor_id = vendors.id) "
                        . "LEFT JOIN `vendors_transactions` ON (vendors_activations.ref_code = `vendors_transactions`.ref_id AND vendors_activations.vendor_id = `vendors_transactions`.vendor_id) "
                        . "WHERE vendors_activations.ref_code= '".$ref_id."'");
				
				if(!empty($ref_code)){
					$vendor = $ref_code[0]['vendors_activations']['vendor_id'];
					
					if(!$this->Shop->checkStatus($ref_id,$vendor)){
						return;
					}
					
					if($ref_code['0']['products']['id'] == WALLET_ID){
						$out = $this->General->b2c_pullback($ref_code['0']['vendors_activations']['vendor_refid'],$ref_id);
						if($out['status'] == 'failure'){
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, Amount is less in user account, so cannot be pulled back','success','".date("Y-m-d H:i:s")."')");
							echo "Amount is less in user account, so cannot be pulled back";
							return;
						}
					}else{
                        
                        
                        $result = $ref_code;
                        $statusList = array('0' => 'pending', '1' => 'success', '2' => 'failure', '3' => 'failure', '4' => 'success', '5' => 'success'); //---status list
                        $transId = $result['0']['vendors_activations']['ref_code'];
                        $transvendor = $result['0']['vendors_activations']['vendor_id'];
                        $transvendorname = trim($result['0']['vendors']['shortForm']);
                        $transdate = $result['0']['vendors_activations']['date'];
                        $transvrefId = $result['0']['vendors_activations']['vendor_refid'];
                        $transoprId = $result['0']['vendors_activations']['operator_id'];
                        $trans_server_Status = $statusList[$result['0']['vendors_activations']['status']];
                        $vendors_activations_id = $result['0']['vendors_activations']['id'];

                        ob_start(); //to supress the printable output of below function call
                        $tranResponse = $this->tranStatus($transId,$transvendorname,$transdate,$transvrefId,1);
                        ob_end_clean();                        

                                                   
                        if($ref_code[0]['vendors']['update_flag'] == 0 ){ // contraints for API
                            $MAX_ALLOWED = 2000;
                            $tranResponse_status = isset($tranResponse['status']) ? strtolower(trim($tranResponse['status'])) : "";
                            $allowed_status = array('pending','success');
                            if($tranResponse_status != 'failure'){
                                //if(in_array($tranResponse_status, $allowed_status)){
                                    $amountqry= "SELECT sum(amount) as total "
                                            . "FROM `vendors_activations` "
                                            . "INNER JOIN `trans_pullback` ON (`vendors_activations`.id = `trans_pullback`.vendors_activations_id) "
                                            . "LEFT JOIN vendors ON  (`vendors_activations`.vendor_id = vendors.id) "
                                            . "WHERE `trans_pullback`.date = '".date('Y-m-d')."' AND vendors.update_flag=0 AND `trans_pullback`.pullback_by = 0 "
                                            . "AND `trans_pullback`.reported_by = 'Non-system'";

                                    $total_used_amount_arr = $this->Slaves->query($amountqry);
                                    $total_used_amount = empty($total_used_amount_arr[0][0]['total']) ? 0 : $total_used_amount_arr[0][0]['total'];
                                    /*if( ($MAX_ALLOWED - $total_used_amount) > 0 ||  $grpId == ADMIN){
                                        $req_data = array('id'=>'','vendors_activations_id'=>$vendors_activations_id,'vendor_id'=>$vendor,'status'=>'1','timestamp'=>date('Y-m-d H:i:s'),
                                            'pullback_by'=>0,'pullback_time'=>'0000-00-00 00:00:00','reported_by'=>'Non-system','date'=>date('Y-m-d'));
                                        $this->General->manage_transPullback($req_data);  
                                    }else{
                                        $res_msg = "reversal amount exceeding MAX Allowed Amount";
                                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, $res_msg , so cannot be reversed','success','".date("Y-m-d H:i:s")."')");
                                        echo $res_msg;
                                        return;
                                    }*/
                            }
                        }else if($ref_code[0]['vendors']['update_flag'] == 1){ // contraints for MODEM
                            $tranResponse_arr = json_decode($tranResponse,true);
                            $tranResponse['status'] = isset($tranResponse_arr['status']) ? strtolower($tranResponse_arr['status']) :"";
                            if(!in_array($tranResponse['status'],array('failure','invalid')) && empty($transoprId)){
                                $sim_num = $result['0']['vendors_transactions']['sim_num'];
                                $trans_oprId = $this->Shop->getOtherProds($result['0']['vendors_activations']['product_id']);
                                $sim_qry = "SELECT * FROM `devices_data` WHERE mobile='".substr($sim_num,-10)."' AND vendor_id='$vendor' AND opr_id in ($trans_oprId) AND sync_date='$transdate'";
                                $sim_result = $this->Slaves->query($sim_qry);
                                if(!empty($sim_result)){
                                	$sim_result[0]['devices_data']['closing'] = ($transdate == date('Y-m-d')) ? $sim_result[0]['devices_data']['balance'] : $sim_result[0]['devices_data']['closing'];
                                	$diff =  $sim_result[0]['devices_data']['sale'] - $sim_result[0]['devices_data']['opening'] - $sim_result[0]['devices_data']['tfr'] + $sim_result[0]['devices_data']['closing'] - $sim_result[0]['devices_data']['inc'];
                                    
                                    $this->General->logData('/mnt/logs/reverse.txt',"transId: $transId ::diff: $diff:: server_diff:".$sim_result[0]['devices_data']['server_diff']);
                                    
                                    /*if(intval($diff) <= intval($sim_result[0]['devices_data']['server_diff']) && $grpId != ADMIN){
                                        $res_msg = "server_diff more than modem_diff";
                                        $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13', 'Manual reversal by $usrName, $res_msg , so cannot be reversed','success','".date("Y-m-d H:i:s")."')");
                                        echo $res_msg;
                                        return;
                                    }elseif(intval($diff) <= intval($sim_result[0]['devices_data']['server_diff']) && $grpId == ADMIN){
                                        $req_data = array('id'=>'','vendors_activations_id'=>$vendors_activations_id,'vendor_id'=>$vendor,'status'=>'1','timestamp'=>date('Y-m-d H:i:s'),
                                            'pullback_by'=>0,'pullback_time'=>'0000-00-00 00:00:00','reported_by'=>'Non-system','date'=>date('Y-m-d'));
                                        $this->General->manage_transPullback($req_data);  
                                    }*/
                                }
                            }
                        }
                        
                    }
					$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$ref_id."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','14','Manual reversal by $usrName','failure','".date("Y-m-d H:i:s")."')");
						
					$this->Shop->addStatus($ref_id,$vendor);
					$this->Shop->reverseTransaction($ref_id,null,null,$grpId,$usrId);
					
					$current_date = date('Ymd');
					$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside reverseTransaction::".$ref_id);
				}
				else {
					echo "NOT allowed";
				}
			//}
			//else {
				//echo "NOT allowed";
			//}
		}

	}

	function reversalDeclined($ref_id,$send){
		$data = $this->Slaves->query("SELECT vendors_activations.id,vendors_activations.status,vendors_activations.mobile,vendors_activations.product_id,vendors_activations.retailer_id,resolve_flag FROM vendors_activations left join complaints ON (vendors_activations.id = vendor_activation_id AND resolve_flag = 0) where ref_code='".$ref_id."'");


		if($data['0']['vendors_activations']['status'] == TRANS_REVERSE_PENDING || $data['0']['complaints']['resolve_flag'] == '0') {
			$this->User->query("update vendors_activations set status='".TRANS_REVERSE_DECLINE."',complaintNo='".$_SESSION['Auth']['User']['id']."' where ref_code='".$ref_id."'");
			$ret = $this->Slaves->query("SELECT retailers.mobile,vendors_activations.*,products.service_id,products.name FROM vendors_activations join retailers on ( vendors_activations.retailer_id = retailers.id) join products on (vendors_activations.product_id = products.id) where vendors_activations.ref_code='".$ref_id."'");
			$this->User->query("UPDATE complaints SET resolve_flag=1,closedby='".$_SESSION['Auth']['User']['id']."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

			$msg = 'Complaint for Txn Id '.substr($ret['0']['vendors_activations']['ref_code'],-5).' is resolved';
			$msg .= "\nTransaction is successful";
			$msg .= "\nDate: " . date('j M, g:i A',strtotime($ret['0']['vendors_activations']['timestamp']));
			if($ret['0']['products']['service_id'] == 2){
				$msg .= "\nSub Id: " .  $ret['0']['vendors_activations']['param'];
			}
			$msg .= "\nMob: " .  $ret['0']['vendors_activations']['mobile'];
			$msg .= "\nOpr: " .  $ret['0']['products']['name'];
                       if($ret['0']['products']['service_id'] == 2 && (!empty($ret['0']['vendors_activations']['operator_id'])  || !is_null($ret['0']['vendors_activations']['operator_id'])))
                               { $msg .= "\nOpr Id: " .  $ret['0']['vendors_activations']['operator_id'];}
			$msg .= "\nAmt: " .  $ret['0']['vendors_activations']['amount'];

			if($send == 1)
			$this->General->sendMessage($ret['0']['retailers']['mobile'],$msg,'notify');
				
				
			if($data['0']['vendors_activations']['retailer_id'] == B2C_RETAILER){
				$partnerLogs =  $this->Slaves->query("SELECT partners_log.partner_req_id FROM partners_log left join vendors_activations ON (partners_log.vendor_actv_id = vendors_activations.ref_code) WHERE vendors_activations.ref_code = '$ref_id'");

				if(empty($partnerLogs))return;
				$url = B2C_REVERSAL_DECLINE_URL;
				$data = array('transaction_id'=>$partnerLogs['0']['partners_log']['partner_req_id']);
				$this->General->curl_post_async($url,$data);
			}
			$current_date = date('Ymd');
			$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside reversalDeclined::".$ref_id);
		}
		$this->autoRender = false;
	}
	
	function updateCommentsForReversalNew(){
		$current_date = date('Ymd');
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside updateCommentsForReversalNew::". json_encode($_REQUEST));
		$loggedInUser = $_SESSION['Auth']['User']['mobile'];
		$tId = $_REQUEST['tId'];
		$reason = $_REQUEST['reason'];
		$userMobile = $_REQUEST['userMobile'];
		$flag = $_REQUEST['flag'];
		$retMobile = $_REQUEST['retMobile'];
		$retId = $_REQUEST['retId'];
		$callTypeId = $_REQUEST['callTypeId'];
		$tagId = $_REQUEST['tagId'];
		
		$callTypeId == 'none' && $callTypeId = null;
		$tagId == 'none' && $tagId = null;
		
		if($flag == 3){
			$message = "Complaint for transactin id ".$tId."has been declined . ";
	
			$this->General->sendMessage($retMobile, $message, 'notify');
		}
		
		$userIdResult = $this->Slaves->query("select id from users where mobile='$userMobile' ");
		$userId = empty($userIdResult) ? 0 : $userIdResult['0']['users']['id'];
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "inside updateCommentsForReversalNew ADD COMMENT::" . json_encode(array($userId, $retId, $tId, $reason, $loggedInUser, null, $tagId, $callTypeId)));
		$this->Shop->addComment($userId, $retId, $tId, $reason, $loggedInUser, null, $tagId, $callTypeId);
		$comment = $this->Slaves->query("SELECT c.comments,c.ref_code,u.name,u.mobile,c.created
					from comments c join users u on(c.mobile=u.mobile)
					where c.ref_code = '$tId'  order by c.created desc limit 1");
		echo "<tr bgcolor='#CEF6F5' style='border: 2px solid white'>";
		echo "<td><span style='font-size:12px;'>By ".$comment[0]['u']['name']." @ ".$comment[0]['c']['created']." on ".$comment[0]['c']['ref_code']."</span></br>".$comment[0]['c']['comments']."</td>"; 
		echo "</tr>";
		$this->General->logData('/mnt/logs/comments_'.$current_date.'.txt', "end updateCommentsForReversalNew" . json_encode($comment));
		$this->autoRender=false;
	}
	
	function updateCommentsForReversal()
	{
		$loggedInUser= $_SESSION['Auth']['User']['mobile'];
		$tId=$_REQUEST['tId'];
		$reason=$_REQUEST['reason'];
		$userMobile=$_REQUEST['userMobile'];
		$flag=$_REQUEST['flag'];
		$retMobile=$_REQUEST['retMobile'];
		$retId=$_REQUEST['retId'];
		echo "Retailer Mobile is ".$retMobile;
		//$retIdResult=$this->User->query("select id from retailers where mobile='$retMobile' ");
		//$retId=$retIdResult['0']['retailers']['id'];

		if($flag==3) // for tagging of transaction function, flag=3.
		{
			$message="Complaint for transactin id ".$tId."has been declined . ";
			echo "user mobile is ".$userMobile;
			echo "Message is ".$message;

			$this->General->sendMessage($retMobile,$message,'notify');

		}

		echo "retailer id is ".$retId;
		//		echo "Transactin Id is ".$tId;
		$userIdResult=$this->Slaves->query("select id from users where mobile='$userMobile' ");
		$userId=$userIdResult['0']['users']['id'];

		$this->Shop->addComment($userId,$retId,$tId,$reason,$loggedInUser);
		//echo "userid for mobile ".$usermobile." is ".$userId;

		//comments entered by
		/*$commentsEnteredByResult=$this->User->query("select mobile,name from users where id=$loggedInUser");
		 $commentsEnteredBy=$commentsEnteredByResult['0']['users']['mobile'];
		 $commentsEnteredByName=$commentsEnteredByResult['0']['users']['name'];
		 //echo "Comments entered by : ".$commentsEnteredBy;
		 $newDate=date("Y-m-d H:i:s");
		 $this->User->query("insert into comments(users_id,retailers_id,mobile,ref_code,flag,comments,created) values ($userId,$retId,$commentsEnteredBy,$tId,$flag,'".addslashes($reason)."','$newDate') ");

		 if(empty($commentsEnteredByName))
			$var=$commentsEnteredBy;
			else
			$var=$commentsEnteredByNamel;
			echo $var."&nbsp;&nbsp&nbsp;&nbsp;".$newDate."&nbsp;</br>".$reason;*/
		//echo "Inserted in comments table";
		$this->autoRender=false;
	}

	function modemStatusCheck($time='3',$ref_id_type=null){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		if(empty($ref_id_type)){
			$data = $this->User->query("SELECT va.ref_code, va.vendor_id, va.timestamp from vendors_activations va,vendors v WHERE va.vendor_id = v.id AND v.update_flag = 1 AND va.date between '".date('Y-m-d')."' and '".date('Y-m-d')."' AND (va.status = 0 OR (va.prevStatus = 0 AND va.status = 4)) AND va.timestamp <= '$time' order by va.id desc");
		}
		else {
			$data = $this->User->query("SELECT va.ref_code, va.vendor_id, va.timestamp from vendors_activations va,vendors v WHERE va.vendor_id = v.id AND v.update_flag = 1 AND va.date between '".date('Y-m-d')."' and '".date('Y-m-d')."' AND (va.status = 0 OR (va.prevStatus = 0 AND va.status = 4)) AND va.timestamp <= '".date('Y-m-d H:i:s',strtotime('-90 seconds'))."' AND vendor_refid = '' order by va.id desc");
		}
		$ids_3 = array();
		$ids_6 = array();

		$time_6 = date('Y-m-d H:i:s',strtotime('-6 minutes'));
		foreach($data as $dt){
			if(!$this->Shop->checkStatus($dt['va']['ref_code'],$dt['va']['vendor_id'])){
				continue;
			}
			if($dt['va']['timestamp'] < $time_6){
				$ids_6[$dt['va']['vendor_id']][] =  $dt['va']['ref_code'];
			}
			else {
				$ids_3[$dt['va']['vendor_id']][] =  $dt['va']['ref_code'];
			}
		}

		foreach($ids_3 as $vendor=>$transids){
			if(count($transids) > 5){
				$chunks = array_chunk($transids,10);
				foreach($chunks as $chunk){
					//$this->General->sendMails("Bulk modem update in $vendor",json_encode($chunk),array('ashish@mindsarray.com'),'mail');
					$this->modemUpdateStatus($chunk,$vendor);
				}
			}
		}

		foreach($ids_6 as $vendor=>$transids){
			$chunks = array_chunk($transids,10);
			foreach($chunks as $chunk){
				//$this->General->sendMails("Bulk modem update in $vendor",json_encode($chunk),array('ashish@mindsarray.com'),'mail');
				$this->modemUpdateStatus($chunk,$vendor);
			}
		}

		$this->autoRender = false;
	}

	function paytStatusCheck($time='1',$ord = 'desc',$days='1'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id FROM vendors_activations use index (idx_vend_date) ,products WHERE vendors_activations.product_id=products.id AND vendor_id = 5 AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days.' days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id $ord LIMIT 50");
		foreach($data as $dt){

			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,5)){
				continue;
			}
			$status = $this->paytTranStatus($transId);
				
			if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status['ref_id'])){
				$this->User->query("UPDATE vendors_activations SET vendor_refid = '".$status['ref_id']."' WHERE ref_code='$transId' AND vendor_id = 5");
				//$dt['vendors_activations']['vendor_refid'] = $status['ref_id'];
			}

			if($status['status'] == 'success'){
				//$this->General->sendMails("Paytronics Status Check: Success","Ref_code: " . $transId,array('ashish@mindsarray.com'));
				$this->Shop->addStatus($transId,5);
				$this->Shop->setMemcache("vendor5"."_last",date('Y-m-d H:i:s'),24*60*60);

				$this->Shop->setProdVendorHealth(5, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['ref_id']."','".$dt['products']['service_id']."','5','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,5);
				$this->Shop->setProdVendorHealth(5, $dt['vendors_activations']['product_id'],1);
				//$this->General->sendMails("Paytronics Status Check: Failed","Ref_code: " . $dt['vendors_activations']['ref_code'],array('ashish@mindsarray.com'));
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['ref_id']."','".$dt['products']['service_id']."','5','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'payt');
				if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
			}
			else {
				//$this->General->sendMails("Paytronics Status Check: Don't know what to do","Ref_code: " . $dt['vendors_activations']['ref_code'],array('ashish@mindsarray.com'));

			}
		}
		$this->autoRender = false;
	}

	function cbzStatusCheck($time = '1'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));

		$data = $this->Slaves->query("SELECT vendors_activations.id,ref_code,vendor_refid,status,service_id,timestamp,product_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = 11 AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-1 days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id LIMIT 200");

		$datas = array_chunk($data, 5);
		foreach($datas as $data){
			$transIds = array();
			foreach($data as $dt){
				if(!$this->Shop->checkStatus($dt['vendors_activations']['ref_code'],11)){
					continue;
				}
				$transIds[] = $dt['vendors_activations']['ref_code'];
			}
			$status = $this->cbzTranStatus($transIds);
				
			foreach($data as $dt){
				$transId = $dt['vendors_activations']['ref_code'];
					
				if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status[$transId]['refId'])){
					$this->User->query("UPDATE vendors_activations SET vendor_refid='".$status[$transId]['refId']."' WHERE ref_code='$transId' AND vendor_id = 11");
					//$dt['vendors_activations']['vendor_refid'] = $status[$transId]['refId'];
				}

				if($status[$transId]['status'] == 'success'){
					$this->Shop->addStatus($transId,11);
					$this->Shop->setMemcache("vendor11_last",date('Y-m-d H:i:s'),24*60*60);
					$this->Shop->setProdVendorHealth(11, $dt['vendors_activations']['product_id'],1);

					//$this->General->sendMails("Paytronics Status Check: Success","Ref_code: " . $transId,array('ashish@mindsarray.com'));
					$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
					$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status[$transId]['refId']."','".$dt['products']['service_id']."','11','13','','success','".date("Y-m-d H:i:s")."')");
				}
				else if($status[$transId]['status'] == 'failure'){
					$this->Shop->addStatus($transId,11);
					$this->Shop->setProdVendorHealth(11, $dt['vendors_activations']['product_id'],1);
					//$this->General->sendMails("Paytronics Status Check: Failed","Ref_code: " . $dt['vendors_activations']['ref_code'],array('ashish@mindsarray.com'));
					$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status[$transId]['refId']."','".$dt['products']['service_id']."','11','14','','failure','".date("Y-m-d H:i:s")."')");

					$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'cbz');
					if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
				}
				else if($status[$transId]['status'] == 'NA'){
					$status1 = $this->cbzTranStatus($transId);
					if($status1[$transId]['status'] == 'failure'){
						$this->Shop->addStatus($transId,11);
						$this->Shop->setProdVendorHealth(11, $dt['vendors_activations']['product_id'],1);
							
						$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','11','14','','failure','".date("Y-m-d H:i:s")."')");

						$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'cbz');
						if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
					}
				}
			}
		}


		$this->autoRender = false;
	}

	function rduStatusCheck($time='1'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations use index (idx_vend_date),products WHERE vendors_activations.product_id=products.id AND vendor_id = 16 AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-1 days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id desc LIMIT 50");
		foreach($data as $dt){

			$transId = $dt['vendors_activations']['ref_code'];
				
			if(!$this->Shop->checkStatus($transId,16)){
				continue;
			}
			$status = $this->rduTranStatus($transId,$dt['vendors_activations']['date']);
			$query = "";
			if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status['vendor_id'])){
				$query = "vendor_refid = '".$status['vendor_id']."'";
			}
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['opr_ref_id'])){
				if(!empty($query))$query .= ",";
				$query = "operator_id = '".$status['opr_ref_id']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = 16");
			}

			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,16);
				$this->Shop->setMemcache("vendor16_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth(16, $dt['vendors_activations']['product_id'],1);

				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['vendor_id']."','".$dt['products']['service_id']."','16','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,16);
				$this->Shop->setProdVendorHealth(16, $dt['vendors_activations']['product_id'],1);

				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['vendor_id']."','".$dt['products']['service_id']."','16','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'rdu');
				if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
			}
		}
		$this->autoRender = false;
	}

	function uvaStatusCheck($time='1',$ord='desc'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$vendor_id = 18;
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations use index (idx_vend_date),products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-1 days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id $ord LIMIT 100");
		foreach($data as $dt){

			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
				
			$power = null;
			if($dt['vendors_activations']['product_id'] == 2 && (time() - strtotime($dt['vendors_activations']['timestamp'])) > 300){
				$power = 1;
			}
			$status = $this->uvaTranStatus($transId,null,$dt['vendors_activations']['vendor_refid'],$power);
			$query = "";
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['operator_id'])){
				$query = "operator_id = '".$status['operator_id']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
			}

			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['vendor_id']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['vendor_id']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'uva');
				if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
			}
		}
		$this->autoRender = false;
	}

	function uniStatus(){
		if($_SERVER['REMOTE_ADDR'] == '182.19.0.60'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uni.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['tid'];
			$this->Shop->setMemcache("uni".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/uni.sh $transId");
		}
		$this->autoRender = false;
	}


	function uniStatCheck($time = '30',$ord = 'desc',$days='1'){
		$vendor_id = 19;
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,operator_id FROM vendors_activations use index (idx_vend_date),products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days.' days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id $ord LIMIT 50");

		//$data = $this->User->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		foreach($data as $dt){
			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
			$query = "";
			$status = $this->uniTranStatus($transId);
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['operator_id'])){
				$query = "operator_id = '".$status['operator_id']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
			}

			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($transId,'uni');
				if(!$try)$this->Shop->reverseTransaction($transId);
			}
		}
		$this->autoRender = false;
	}

	function uniStatusCheck($transId){

		$status = $this->Shop->getMemcache("uni".$transId);

		if($status['opid'] == 'NA')$status['opid'] = "";
		$vendor_id = 19;
		$data = $this->Slaves->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		foreach($data as $dt){

			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
			$query = "";
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['opid'])){
				$query = "operator_id = '".$status['opid']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
			}

			if($status['status'] == '1'){
                $status['description'] = empty($status['description']) ? "" : $status['description'];
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == '0'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($transId,'uni');
				if(!$try)$this->Shop->reverseTransaction($transId);
			}
		}
		$this->autoRender = false;
	}

	function anandStatus(){
		if($_SERVER['REMOTE_ADDR'] == '180.149.242.51'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/anand.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['accountId'];
			$this->Shop->setMemcache("anand".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/anand.sh $transId");
		}
		$this->autoRender = false;
	}

	function anandStatCheck($time = '30',$ord = 'desc',$days='1'){
		$vendor_id = 9;
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,operator_id,date FROM vendors_activations use index (idx_vend_date) ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days.' days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id $ord LIMIT 50");

		//$data = $this->User->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		foreach($data as $dt){
			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
			$query = "";
			$status = $this->anandTranStatus($transId,$dt['vendors_activations']['date'],$dt['vendors_activations']['vendor_refid']);
				
			/*if(empty($dt['vendors_activations']['operator_id']) && !empty($status['operator_id'])){
				$query = "operator_id = '".$status['operator_id']."'";
				}
					
				if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
				}*/

			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($transId,'anand');
				if(!$try)$this->Shop->reverseTransaction($transId);
			}
		}
		$this->autoRender = false;
	}

	function anandStatusCheck($transId){

		$status = $this->Shop->getMemcache("anand".$transId);

		if($status['txid'] == 'NA')$status['txid'] = "";
		$vendor_id = 9;
		$data = $this->Slaves->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		foreach($data as $dt){

			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
			$query = "";
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['txid'])){
				$query = "operator_id = '".$status['txid']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
			}

			if($status['transtype'] == 's'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','success','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['transtype'] == 'f'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','failure','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($transId,'anand');
				if(!$try)$this->Shop->reverseTransaction($transId);
			}
		}
		$this->autoRender = false;
	}

	function apnaStatus(){
		if($_SERVER['REMOTE_ADDR'] == '122.170.103.214'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/apna.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['Client_id'];
			$this->Shop->setMemcache("apna".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 23");
		}
		$this->autoRender = false;
	}

	function magicStatus(){
		//if($_SERVER['REMOTE_ADDR'] == '103.39.241.23'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/magic.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['transid'];
			$this->Shop->setMemcache("magic".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 24");
		//}
		$this->autoRender = false;
	}
	
	function rioStatus(){
		if($_SERVER['REMOTE_ADDR'] == '117.218.64.210'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['transid'];
			$this->Shop->setMemcache("rio".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 36");
		}
		$this->autoRender = false;
	}

    function rio2Status(){
		//if($_SERVER['REMOTE_ADDR'] == '117.218.64.210'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio2.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['transid'];
			$this->Shop->setMemcache("rio2".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 62");
		//}
		$this->autoRender = false;
	}
    
	function durgaStatus(){
		if($_SERVER['REMOTE_ADDR'] == '117.218.206.47'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/durga.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = $_REQUEST['tid'];
			$this->Shop->setMemcache("durga".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 30");
		}
		$this->autoRender = false;
	}
	
	function rkitStatus(){
		if($_SERVER['REMOTE_ADDR'] == '43.254.40.236'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rkit.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = trim($_REQUEST['TRANNO']);
			$this->Shop->setMemcache("rkit".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 34");
		}
		$this->autoRender = false;
	}
	
	function a2zStatus(){
		// 117.205.177.110
		//if($_SERVER['REMOTE_ADDR'] == '182.18.175.162'){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/a2z.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($_REQUEST));
			$transId = trim($_REQUEST['TRANNO']);
			$this->Shop->setMemcache("a2z".$transId,$_REQUEST,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 47");
			
			print_r($_REQUEST);
		//}
		$this->autoRender = false;
	}
	
    function mypayStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/mypay.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $txn_data = $this->Slaves->query("SELECT ref_code,vendor_refid FROM vendors_activations USE INDEX ( idx_vendorrefid ) WHERE vendor_refid = '".$data['accountId']."'");
        
        $transId = isset($txn_data[0]['vendors_activations']['ref_code']) ? trim($txn_data[0]['vendors_activations']['ref_code']):"";
        $this->Shop->setMemcache("mypay".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 57");

		$this->autoRender = false;
	}
    
    function smsdaakStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $transId = trim($data['agent_id']);
        $this->Shop->setMemcache("smsdaak".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 58");
		$this->autoRender = false;
	}
    
	function joinrecStatus(){
		//if($_SERVER['REMOTE_ADDR'] == '184.107.11.157'){
			
			$data = $this->General->xml2array(trim($_REQUEST['xmldata']));
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));
			
			$transId = trim($data['RechargeStatus']['MerOid']);
			$this->Shop->setMemcache("joinrec".$transId,$data,10*60);
			shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 48");
		//}
		$this->autoRender = false;
	}

	function statCheck($vendor_id,$time = '30',$ord = 'desc',$days='1',$limit='100'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$vend_info = $this->Shop->getVendorInfo($vendor_id);
		$vend_name = $vend_info['shortForm'];

		$data = $this->User->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,operator_id,date FROM vendors_activations use index (idx_vend_date) ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR status = '".TRANS_REVERSE_PENDING."') AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days.' days'))."' AND vendors_activations.timestamp <= '$time' order by vendors_activations.id $ord LIMIT $limit");

		//$data = $this->User->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		foreach($data as $dt){
			$status = array();
			
			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,$vendor_id)){
				continue;
			}
			$query = "";
			$method = $vend_name."TranStatus";
			$status = $this->$method($transId,$dt['vendors_activations']['date'],$dt['vendors_activations']['vendor_refid']);
				
			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['operator_id'])){
				$query .= "operator_id = '".$status['operator_id']."'";
			}
			if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status['vendor_id'])){
				if(!empty($query)) $query .= ", ";
				$query .= "vendor_refid = '".$status['vendor_id']."'";
			}
				
			if(!empty($query)){
				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
			}

			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor_id);
				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($transId,$vend_name);
				if(!$try)$this->Shop->reverseTransaction($transId);
			}
		}
		$this->autoRender = false;
	}
    
    /***************** statCheckOnclick *************************************/
    /*
     * This function is use to check API txn status and update it on server side
     * 
     */
//    function statCheckOnclick($vendor_id, $txn_id, $time = '30'){
//		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
//		$vend_info = $this->Shop->getVendorInfo($vendor_id);
//		$vend_name = $vend_info['shortForm'];
//
//		$data = $this->User->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id,operator_id,date FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR status = '".TRANS_REVERSE_PENDING."') AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days.' days'))."' AND vendors_activations.timestamp <= '$time' AND vendors_activations.ref_code ='$txn_id' ");
//
//		//$data = $this->User->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
//		foreach($data as $dt){
//			$status = array();
//			
//			$transId = $dt['vendors_activations']['ref_code'];
//			if(!$this->Shop->checkStatus($transId,$vendor_id)){
//				continue;
//			}
//			$query = "";
//			$method = $vend_name."TranStatus";
//			$status = $this->$method($transId,$dt['vendors_activations']['date'],$dt['vendors_activations']['vendor_refid']);
//				
//			if(empty($dt['vendors_activations']['operator_id']) && !empty($status['operator_id'])){
//				$query .= "operator_id = '".$status['operator_id']."'";
//			}
//			if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status['vendor_id'])){
//				if(!empty($query)) $query .= ", ";
//				$query .= "vendor_refid = '".$status['vendor_id']."'";
//			}
//				
//			if(!empty($query)){
//				$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
//			}
//
//			if($status['status'] == 'success'){
//				$this->Shop->addStatus($transId,$vendor_id);
//				$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
//				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
//				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
//				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($status['description'])."','success','".date("Y-m-d H:i:s")."')");
//			}
//			else if($status['status'] == 'failure'){
//				$this->Shop->addStatus($transId,$vendor_id);
//				$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
//				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($status['description'])."','failure','".date("Y-m-d H:i:s")."')");
//
//				$try = $this->transferToOtherRoutes($transId,$vend_name);
//				if(!$try)$this->Shop->reverseTransaction($transId);
//			}
//		}
//		$this->autoRender = false;
//	}
    
    /****************************************/
	function statusCheck($transId,$vendor_id){
		$vend_info = $this->Shop->getVendorInfo($vendor_id);
		$vend_name = $vend_info['shortForm'];
		$status = $this->Shop->getMemcache($vend_name.$transId);

		$this->General->logData("/var/www/html/shops/app/webroot/logs/$vend_name".".txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($status));
			
		$data = $this->User->query("SELECT vendors_activations.id,vendor_refid,status,service_id,timestamp,product_id,date,operator_id,vendor_id,vendors.company,complaintNo FROM vendors_activations ,products,vendors WHERE vendors_activations.product_id=products.id AND vendors.id = vendor_id AND ref_code='$transId'");
                //$data = $this->User->query("SELECT vendor_refid,status,service_id,timestamp,product_id,date,operator_id FROM vendors_activations ,products WHERE vendors_activations.product_id=products.id AND vendor_id = $vendor_id AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND ref_code='$transId'");
		if(empty($data)){
        	//$this->General->sendMails('SOS: Multiple time status response.',"vendor : ".$vend_name."<br>TransactionId : ".$transId,array('nandan@mindsarray.com','cc.support@mindsarray.com','naziya@mindsarray.com'),'mail');
                    return;
                }
        
       	$dt = $data[0];
		
		
		if($vendor_id == 23){//apnaeasy
			$dt['vendors_activations']['vendor_refid'] = "";
			$vendor_refid = $status['Txid'];
			$operator_id  = $status['Opt_id'];
			$stat = strtolower(trim($status['Status']));
			$message = $status['Message'];
		}
		else if($vendor_id == 24 || $vendor_id == 36 || $vendor_id == 62){//magic pay | rio | rio2
			$operator_id  = $status['optransid'];
			$stat = strtolower(trim($status['status']));
			$message = $status['opmsg'];
			$vendor_refid = "";
		}
		else if($vendor_id == 30){//durgawati pay
			$operator_id  = $status['opid'];
			if($operator_id == "N")$operator_id = "";
				
			if(trim($status['status']) == '1')$stat = 'success';
			else if(trim($status['status']) == '0')$stat = 'failure';
				
			$vendor_refid = "";
			$message = "";
		}
		else if($vendor_id == 34){//RKIT 
			$operator_id  = $status['ORIGINALTRANNO'];
			if($operator_id == "NA")$operator_id = "";
				
			if(trim($status['STATUS']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['STATUS']) == 'NOT SUCCESS')$stat = 'failure';
				
			$vendor_refid = "";
			$message = "";
		}
		
		else if( $vendor_id == 47){//A2Z
			$operator_id  = $status['ORIGINALTRANNO'];
			if($operator_id == "NA")$operator_id = "";
				
			if(trim($status['STATUS']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['STATUS']) == 'NOT SUCCESS')$stat = 'failure';
				
			$vendor_refid = isset($status['RECNO']) ? $status['RECNO'] : "";
			$message = "";
		}
		else if($vendor_id == 48){//Joinrecharge
			$operator_id  = $status['RechargeStatus']['OperatorTxnId'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['RechargeStatus']['Status']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['RechargeStatus']['Status']) == 'FAILED')$stat = 'failure';
				
			$vendor_refid = $status['RechargeStatus']['OrderId'];
			$message = $status['RechargeStatus']['Description'];
		}
        else if($vendor_id == 58){//
			$operator_id  = $status['opr_id'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['status']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['status']) == 'REFUND')$stat = 'failure';
				
			$vendor_refid = $status['ipay_id'];
			$message = $status['res_msg'];
		}
        else if($vendor_id == 57){//mypay
			$operator_id  = $status['txid'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(strtoupper(trim($status['Transtype'])) == 'S')$stat = 'success';
			else if(strtoupper(trim($status['Transtype'])) == 'F')$stat = 'failure';
				
			$vendor_refid = $status['accountId'];
			$message = "";
		}else if($vendor_id == 63){//
			$operator_id  = $status['OPID'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(strtoupper(trim($status['status'])) == 'SUCCESS')$stat = 'success';
			else if(strtoupper(trim($status['status'])) == 'FAILED')$stat = 'failure';
            else if(strtoupper(trim($status['status'])) == 'REFUNDED')$stat = 'failure';
				
			$vendor_refid = $status['trans_id'];
			$message = "";
		}else if($vendor_id == 65){//
			$operator_id  = $status['txid'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(strtoupper(trim($status['transtype'])) == 'SUCCESS')$stat = 'success';
			else if(strtoupper(trim($status['transtype'])) == 'FAILED')$stat = 'failure';
            else if(strtoupper(trim($status['sts'])) == 'REFUNDED')$stat = 'failure';				
			
			$message = "";
		}
		//practic modem
		else if($vendor_id == 68){//
			$operator_id  = $status['ORef'];
			if(in_array(strtolower(trim($operator_id)),array('nil','null','array','#') ))$operator_id = "";
				
			if(trim($status['RecS']) == '1')$stat = 'success';
			else if(trim($status['RecS']) == '7' || trim($status['RecS']) == '4')$stat = 'failure';
            
			
			$message = "";
		}
		//simple recharge modem
		else if($vendor_id == 69){//
			$operator_id  = $status['operator_id'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['status']) == 'Success')$stat = 'success';
			else if(trim($status['status']) == 'Failure')$stat = 'failure';
            
			
			$message = "";
		}
		
		else if($vendor_id == 87){//
			$operator_id  = $status['operator_id'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['status']) == 'Success')$stat = 'success';
			else if(trim($status['status']) == 'Failure')$stat = 'failure';
			
			$vendor_refid = isset($status['transaction_id']) ? $status['transaction_id'] : "";
			
			$message = "";
		}
		
		else if($vendor_id == 105){//bulk recharge
			$operator_id  = $status['operatorid'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['status']) == 'Success')$stat = 'success';
			else if(trim($status['status']) == 'Failure')$stat = 'failure';
			
			$vendor_refid = isset($status['transaction_id']) ? $status['transaction_id'] : "";
			
			$message = "";
		}
                
                
                
                else if($vendor_id == 123){//bimco recharge
			$operator_id  = $status['TransactionID'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['Status']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['Status']) == 'FAILURE')$stat = 'failure';
			
			$vendor_refid = isset($status['OrderId']) ? $status['OrderId'] : "";
			
			$message = "";
		}
                
                 else if($vendor_id == 125){//rajan recharge
			$operator_id  = $status['transid'];
			if($operator_id == "#" || $operator_id == "Array")$operator_id = "";
				
			if(trim($status['status']) == 'SUCCESS')$stat = 'success';
			else if(trim($status['status']) == 'FAILURE')$stat = 'failure';
			
			$vendor_refid = isset($status['ref']) ? $status['ref'] : "";
			
			$message = "";
		}
		
		if($vendor_id == $dt['vendors_activations']['vendor_id'] && ((in_array($dt['vendors_activations']['status'],array(2,3)) && $stat == 'failure') OR (in_array($dt['vendors_activations']['status'],array(1)) && $stat == 'success'))){//same status
			return;
		}
		$this->General->logData("/mnt/logs/$vend_name".".txt",date('Y-m-d H:i:s').":I m here: $vendor_id $stat ". json_encode($dt));

		if($vendor_id == $dt['vendors_activations']['vendor_id'] && in_array($dt['vendors_activations']['status'],array(1,5)) && $stat == 'failure'){//failure after success
			$this->General->sendMails('API: Failure after Success problem',"$transId <br/>Txn found for vendor ". $dt['vendors']['company']. ", Previous status was ".$dt['vendors_activations']['status']."<br/>Operator txnid: $operator_id",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
			return;
		}
		else if($vendor_id == $dt['vendors_activations']['vendor_id'] && in_array($dt['vendors_activations']['status'],array(2,3)) && $stat == 'success'){//success after failure
			$this->General->logData("/mnt/logs/pullback.txt",date('Y-m-d H:i:s').":Query: "."INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$dt['vendors_activations']['id']."','".$vendor_id."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");
		
			$res = $this->User->query("SELECT timestamp FROM vendors_messages WHERE shop_tran_id='$transId' AND service_vendor_id='$vendor_id' AND status = 'failure'");
			
			if(!empty($res) && empty($dt['vendors_activations']['complaintNo']) && $res['0']['vendors_messages']['timestamp'] >= date('Y-m-d H:i:s',strtotime('-12 hours'))){
				$this->General->sendMails('API: Autopullback',"$transId <br/>Txn found for vendor ".$dt['vendors']['company'].", Previous status was ".$dt['vendors_activations']['status']."<br/>Operator txnid: $operator_id",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
				$this->Shop->autopullbackTransaction($transId,$vendor_id);
				
			}
			else {
				$this->General->sendMails('API: Success after Failure problem',"$transId <br/>Txn found for vendor ".$dt['vendors']['company'].", Previous status was ".$dt['vendors_activations']['status']."<br/>Operator txnid: $operator_id",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
				$this->User->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$dt['vendors_activations']['id']."','".$vendor_id."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");
				
			}
					
			return;	
		}
		else if($vendor_id != $dt['vendors_activations']['vendor_id'] && $stat == 'success'){//success after failure(double txn)
			$this->General->sendMails('API: Success after Failure problem(Double Txn)',"$transId <br/>Txn found for vendor ".$dt['vendors']['company'].", Previous status was ".$dt['vendors_activations']['status']."<br/>Operator txnid: $operator_id",array('ashish@mindsarray.com','chirutha@mindsarray.com','cc.support@mindsarray.com'),'mail');
			$this->General->logData("/mnt/logs/pullback.txt",date('Y-m-d H:i:s').":Query: "."INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$dt['vendors_activations']['id']."','".$vendor_id."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");
		
			$this->User->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$dt['vendors_activations']['id']."','".$vendor_id."','1','".date('Y-m-d H:i:s')."','','','System','".date('Y-m-d')."')");
					
			return;	
		}else if($vendor_id != $dt['vendors_activations']['vendor_id']){//success after failure(double txn)
			return;	
		}
			
		if(!$this->Shop->checkStatus($transId,$vendor_id)){
			return;
		}
		$query = "";

		if(empty($dt['vendors_activations']['operator_id']) && !empty($operator_id)){
			$query = "operator_id = '$operator_id'";
		}
		if(empty($dt['vendors_activations']['vendor_refid']) && !empty($vendor_refid)){
			if(!empty($query))$query .= ",";
			$query .= "vendor_refid = '$vendor_refid'";
		}

		if(!empty($query)){
			$this->User->query("UPDATE vendors_activations SET $query WHERE ref_code='$transId' AND vendor_id = $vendor_id");
		}
			
		if($stat == 'success'){
			$this->Shop->addStatus($transId,$vendor_id);
			$this->Shop->setMemcache("vendor$vendor_id"."_last",date('Y-m-d H:i:s'),24*60*60);
			$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
			$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS." WHERE ref_code='$transId'");
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','13','".addslashes($message)."','success','".date("Y-m-d H:i:s")."')");
		}
		else if($stat == 'failure'){
			$this->Shop->addStatus($transId,$vendor_id);
			$this->Shop->setProdVendorHealth($vendor_id, $dt['vendors_activations']['product_id'],1);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('$transId','".$dt['vendors_activations']['vendor_refid']."','".$dt['products']['service_id']."','$vendor_id','14','".addslashes($message)."','failure','".date("Y-m-d H:i:s")."')");

			$try = $this->transferToOtherRoutes($transId,$vend_name);
			if(!$try)$this->Shop->reverseTransaction($transId);
		}

		$this->autoRender = false;
	}

	function ossStatusCheck(){
		$time = date('Y-m-d H:i:s',strtotime('-1 minutes'));

		$data = $this->User->query("SELECT ref_code,vendor_refid,status,service_id,timestamp FROM vendors_activations ,products WHERE products.id = vendors_activations.product_id AND vendor_id = 1 AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.date >= '".date('Y-m-d',strtotime('-1 days'))."' AND timestamp <= '".$time."' order by vendors_activations.id desc LIMIT 20");
		foreach($data as $dt){

			$rechargesReq = '<GetTransactionStatusRequest><TransactionRefNo></TransactionRefNo><MerchantRefNo>'.$dt['vendors_activations']['ref_code'].'</MerchantRefNo></GetTransactionStatusRequest>';
			try{
				$rechargesRes = $this->ossApi('GetTransactionStatus',$rechargesReq);
				$status = $rechargesRes['GetTransactionStatusResponse']['TransactionStatus']['Status'];
				$vendorRefId = $rechargesRes['GetTransactionStatusResponse']['TransactionStatus']['TransactionRefNo'];

				if(empty($dt['vendors_activations']['vendor_refid']) && !empty($vendorRefId))
				$this->User->query("UPDATE vendors_activations SET vendor_refid='".$vendorRefId."' WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");

				if($rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['Status'] != ''){
					if($status == '1' || $rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['Status'] == 'Failed'){//failure
						//$this->General->sendMails("OSS Status Check: Failed","Ref_code: " . $dt['vendors_activations']['ref_code'],array('tadka@mindsarray.com'));
						$errMsg = addslashes('ErrorCode:'.$rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['ErrorDesc']);
						$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','$vendorRefId',".$dt['products']['service_id'].",'1','12','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");

						$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'oss');
						if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
					}
					else if($status == '0'){
						//$this->General->sendMails("OSS Status Check: Success","Ref_code: " . $dt['vendors_activations']['ref_code'],array('tadka@mindsarray.com'));
						$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status='".TRANS_SUCCESS."',extra='".$rechargesRes['GetTransactionStatusResponse']['TransactionStatus']['PINRefNo']."' WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
						$errMsg = addslashes('Status: '.$rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetTransactionStatusResponse']['ResponseStatus']['ErrorDesc']);
						$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','$vendorRefId','".$dt['products']['service_id']."','1','13','".$errMsg."','success','".date("Y-m-d H:i:s")."')");
					}
				}
			}catch(Exception $e){
				echo $this->Shop->errors(30);
			}
		}
		$this->autoRender = false;
	}

	function cpStatusCheck($time = '1',$ord = 'desc',$days='1'){
		$time = date('Y-m-d H:i:s',strtotime('-'.$time.' minutes'));
		$data = $this->Slaves->query("SELECT ref_code,vendor_refid,status,service_id,timestamp,product_id FROM vendors_activations use index (idx_vend_date) ,products WHERE vendors_activations.product_id=products.id AND vendor_id = 8 AND (status = 0 OR (status = '".TRANS_REVERSE_PENDING."' AND prevStatus = 0)) AND vendors_activations.timestamp <= '$time' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-' . $days . ' days'))."' order by vendors_activations.id $ord LIMIT 50");
		foreach($data as $dt){

			$transId = $dt['vendors_activations']['ref_code'];
			if(!$this->Shop->checkStatus($transId,8)){
				continue;
			}
			$status = $this->cpTranStatus($transId);

			if(empty($dt['vendors_activations']['vendor_refid']) && !empty($status['transaction_id'])){
				$this->User->query("UPDATE vendors_activations SET vendor_refid = '".$status['transaction_id']."' WHERE ref_code='$transId' AND vendor_id = 8");
				//$dt['vendors_activations']['vendor_refid'] = $status['transaction_id'];
			}
				
			if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,8);
				$this->Shop->setMemcache("vendor8"."_last",date('Y-m-d H:i:s'),24*60*60);
				$this->Shop->setProdVendorHealth(8, $dt['vendors_activations']['product_id'],1);
				$this->User->query("UPDATE vendors_activations SET prevStatus = '".$dt['vendors_activations']['status']."',status=".TRANS_SUCCESS.",operator_id='".$status['operator_id']."' WHERE ref_code='".$dt['vendors_activations']['ref_code']."'");
				$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['transaction_id']."','".$dt['products']['service_id']."','8','13','".addslashes($status['status'])."','success','".date("Y-m-d H:i:s")."')");
			}
			else if($status['status'] == 'failure' || $status['status'] == 'incomplete'){
				$this->Shop->addStatus($transId,8);
				$this->Shop->setProdVendorHealth(8, $dt['vendors_activations']['product_id'],1);
				$this->General->logData("/mnt/logs/cp.txt","Status Cehck::$transId".json_encode($status)."::step1");
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$dt['vendors_activations']['ref_code']."','".$status['transaction_id']."','".$dt['products']['service_id']."','8','14','".addslashes($status['errMsg'] . " (".$status['errCode'].")")."','failure','".date("Y-m-d H:i:s")."')");

				$try = $this->transferToOtherRoutes($dt['vendors_activations']['ref_code'],'cp');
				$this->General->logData("/mnt/logs/cp.txt","Status Cehck::$transId".json_encode($status)."::step2::$try");
				
				if(!$try)$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code'],null,$this->Shop->errorCodeMapping(8,$status['errCode']));
			}
		}
		$this->autoRender = false;
	}

	function ossAddComplaint($shopTranId,$ossTranId){//

		//if($status == '0' || $status == '1'){
		$pinRefNo = $this->User->query("select extra from vendors_activations where ref_code = '".$shopTranId."'");
		$pin = stripslashes($pinRefNo['0']['vendors_activations']['extra']);
		if(empty($pin)){
			$rechargesReq = '<GetTransactionStatusRequest><TransactionRefNo>'.$ossTranId.'</TransactionRefNo><MerchantRefNo>'.$shopTranId.'</MerchantRefNo></GetTransactionStatusRequest>';
			try{
				$rechargesRes = $this->ossApi('GetTransactionStatus',$rechargesReq);
				$pin = stripslashes($rechargesRes['GetTransactionStatusResponse']['TransactionStatus']['PINRefNo']);
				$this->User->query("UPDATE vendors_activations SET extra = '$pin' WHERE ref_code='".$shopTranId."'");
			}catch(Exception $e){
				echo $this->Shop->errors(30);
			}
		}
		if(!empty($pin)){
			$compReq = '<ProcessComplaintRequest><TransactionRefNo>'.$ossTranId.'</TransactionRefNo><MerchantRefNo>'.$shopTranId.'</MerchantRefNo><PINRefNo>'.$pin.'</PINRefNo><ComplaintDesc>Recharge failed</ComplaintDesc></ProcessComplaintRequest>';
			try{
				$compRes = $this->ossApi('ProcessComplaint',$compReq);
				if(strtolower(trim($compRes['ProcessComplaintResponse']['ResponseStatus']['Status'])) == 'success'){
					$this->User->query("update vendors_activations set complaintNo = '".$compRes['ProcessComplaintResponse']['ComplaintDetails']['ComplaintRefNo']."' where ref_code= '".$shopTranId."'");
				}
			}catch(Exception $e){
				echo $this->Shop->errors(30);
			}
		}
		//}
		$this->autoRender = false;
	}


	function ossComplaintCheck(){//

		$data = $this->User->query("SELECT complaintNo,extra,ref_code,status,vendor_refid FROM vendors_activations WHERE vendor_id = 1 AND complaintNo is not null AND status = " . TRANS_REVERSE_PENDING . " LIMIT 10");
		foreach($data as $dt){
			$compStatusReq = '<GetComplaintStatusRequest><ComplaintRefNo>'.stripslashes($dt['vendors_activations']['complaintNo']).'</ComplaintRefNo><PINRefNo>'.stripslashes($dt['vendors_activations']['extra']).'</PINRefNo><MerchantRefNo>'.$dt['vendors_activations']['ref_code'].'</MerchantRefNo><TransactionRefNo>'.$dt['vendors_activations']['vendor_refid'].'</TransactionRefNo></GetComplaintStatusRequest>';
			try{
				$compRes = $this->ossApi('GetComplaintStatus',$compStatusReq);
				if(strtolower(trim($compRes['GetComplaintStatusResponse']['ResponseStatus']['Status'])) == 'success'){

					$sub = "Pay1: Complaint No: " . $dt['vendors_activations']['complaintNo'];
					$body = "Complaint Status: " . $compRes['GetComplaintStatusResponse']['GetComplaintStatus']['ComplaintStatus'];
					$body .= "<br/>Refund Amount: " . $compRes['GetComplaintStatusResponse']['GetComplaintStatus']['TotalRefundAmount'];

					//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com'));

					/*if(strtolower($compRes['GetComplaintStatusResponse']['GetComplaintStatus']['ComplaintStatus']) == 'cc'){
						if($compRes['GetComplaintStatusResponse']['GetComplaintStatus']['TotalRefundAmount'] > 0){//reversed
						$sub = "OSS Reversal request is successfull";
						$msg = "OSS Transaction ID: ".$dt['vendors_activations']['vendor_refid']."<br/>";
						$msg .= "Our Reference ID: ".$dt['vendors_activations']['ref_code']."<br/>";
						$msg .= "Refund Amount: ".$compRes['GetComplaintStatusResponse']['GetComplaintStatus']['TotalRefundAmount']."<br/>";
							
						$this->General->sendMails($sub,$msg,array('tadka@mindsarray.com','nausheen@mindsarray.com'));
						$this->Shop->reverseTransaction($dt['vendors_activations']['ref_code']);
						}
						else {
						$sub = "OSS Reversal request is declined";
						$msg = "OSS Transaction ID: ".$dt['vendors_activations']['vendor_refid']."<br/>";
						$msg .= "Our Reference ID: ".$dt['vendors_activations']['ref_code']."<br/>";
						if($dt['vendors_activations']['status'] != TRANS_REVERSE){
						$this->User->query("update vendors_activations set status = '".TRANS_REVERSE_DECLINE."' where ref_code= '".$dt['vendors_activations']['ref_code']."'");
						}
						$this->General->sendMails($sub,$msg,array('tadka@mindsarray.com','nausheen@mindsarray.com'));
						}
						}*/
				}
			}catch(Exception $e){
				echo $this->Shop->errors(30);
			}

			$compStatusReq = '<ViewComplaintResponseRequest><ComplaintRefNo>'.stripslashes($dt['vendors_activations']['complaintNo']).'</ComplaintRefNo><TransactionRefNo>'.$dt['vendors_activations']['vendor_refid'].'</TransactionRefNo></ViewComplaintResponseRequest>';
			try{
				//$compRes = $this->ossApi('ViewComplaintResponses',$compStatusReq);
				if(strtolower(trim($compRes['ViewComplaintResponse']['ResponseStatus']['Status'])) == 'success'){

					$sub = "OSS response for Pay1 complaint againts tran id: " . $dt['vendors_activations']['ref_code'];
					$body = "Complaint Description: " . $compRes['ViewComplaintResponse']['ViewComplaint']['ComplaintDescription'];
					$body .= "<br/>Complaint Status: " . $compRes['ViewComplaintResponse']['ViewComplaint']['Status'];
					$body .= "<br/>Response Description: " . $compRes['ViewComplaintResponse']['ViewComplaint']['ResponseDescription'];
					$body .= "<br/>Admin Comments: " . $compRes['ViewComplaintResponse']['ViewComplaint']['AdminComments'];

					/*if(trim($compRes['ViewComplaintResponse']['ViewComplaint']['ComplaintDescription']) != '' || trim($compRes['ViewComplaintResponse']['ViewComplaint']['Status']) != '' || trim($compRes['ViewComplaintResponse']['ViewComplaint']['ResponseDescription']) != '' || trim($compRes['ViewComplaintResponse']['ViewComplaint']['AdminComments']) != '')
					 $this->General->sendMails($sub,$body,array('chirutha@mindsarray.com','ashish@mindsarray.com'));*/
				}
			}catch(Exception $e){
				echo $this->Shop->errors(30);
			}
		}
		$this->autoRender = false;
	}

	//mobile recharges
	function mobRecharge($params=null){
        if(!isset($this->mapping['mobRecharge'][$params['operator']]))
        return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));

        $prodId = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['id'];

        if($prodId == '')return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));

        $mobileNo = $params['mobileNumber'];

        if(!isset($params['power']) || $params['power'] != 1){
            $opData = $this->General->getMobileDetailsNew($mobileNo);
            //$opData = $this->General->getMobileDetails($mobileNo, "5");
            if($prodId == 7 || $prodId == 8){
                if(($opData['operator'] == 'RC' && $prodId == 8) || ($opData['operator'] == 'RG' && $prodId == 7)){
                    return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));
                }
            }
        }

        /* decimal amount check */
        if (strpos($params['amount'], '.')){
            return array('status'=>'failure','code'=>'6','description'=>$this->Shop->errors(6));
        }

        if( isset($params['special']) && $params['special'] == '1'){
			if($params['operator'] == '9' || $params['operator'] == '10'){
				$prodId = 27;
			}
			else if($params['operator'] == '11'){
				$prodId = 29;
			}
			else if($params['operator'] == '12'){
				$prodId = 28;
			}
			else if($params['operator'] == '30'){
				$prodId = 31;
			}
			else if($params['operator'] == '3'){
				$prodId = 34;
			}
		}
                
		$params['api_partner'] = empty($params['api_partner']) ? "" : $params['api_partner'];
		/*if(in_array($prodId,array('9','10','27'))){//tata
			$query = "SELECT product_id FROM vendors_activations WHERE mobile = '$mobileNo' AND amount =".$params['amount']." AND STATUS =1 ORDER BY id DESC LIMIT 1";
			$check_prod = $this->User->query($query);
			if(!empty($check_prod)){
				$prod_last = $check_prod['0']['vendors_activations']['product_id'];
				if($prodId != $prod_last && in_array($prod_last,array('9','10','27'))){
					$prodId = $prod_last;
				}
			}
		}
		else if($prodId == 3){//bsnl
			if((intval($params['amount']) % 5) != 0) $prodId = 34;	
		}*/
		if($prodId == 30){//mtnl
			if((intval($params['amount']) % 10) != 0) $prodId = 31;	
		}
		else if($prodId == 11){//uninor
			if((intval($params['amount']) % 5) != 0) $prodId = 29;	
		}
		//get active vendor
        $additional_param = array('amount'=>$params['amount'],'dist_id'=>$_SESSION['Auth']['parent_id'],'retailer_created'=>$_SESSION['Auth']['created'],'retailer_id'=>$_SESSION['Auth']['id']);
	
                /* control related to specific distributor 0-start 1-stop */
                $stop_airtel_flag = $this->General->findVar("stop_airtel_by_distributor");
                if($stop_airtel_flag == 1 && isset($params['operator']) && $params['operator'] == 2){
                    $distributor_list = array('633','914','396','1094','563','1048','1027','952','1047','832','233','524','661','649','821','1113','875','273','907','1095','641','509','342','432','557','337','921','1106','686','739','939','962','1115','767','595','645','970','1096','924','1025','1081','702','910','961','916','198','317','442','940','703','968','343','899','728','116','341','886','894','537','764','1076','1114','624','514','660','945','219','824','758','761','579','1085','1061','642','213','1031','1059','1109','1100','893','339','413','1054','708','727','489','888','570','783','1008','693','1062','861','787','1093','404','1070','352','830','852','949','807','776','997','454','504','874','881','164','735','329','985','999','920','478','1079','953','212','292','822','1111','480','92','837');
                    if(in_array($additional_param['dist_id'],$distributor_list)){
                        $desc_msg = "Sirf AIRTEL Recharge ki service temporarily STOP ki gai hai.Aap baki sabhi Operators ke Recharges kar sakte hain";
                        return array('status'=>'failure','code'=>'37','description'=>$desc_msg);
                    }
                }
                /* ----- */
                
		$vendorData = $this->Shop->getActiveVendor($prodId,$mobileNo,$params['api_partner'],true,$additional_param);
		
		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}
       
        if($vendorData['status'] == 'failure'){
            $this->General->sendMails('SOS: No active vendor for mobile recharge.','Product : '.$vendorData['info']['name'] . '<br/>'.$prodId,array('ashish@mindsarray.com'));
            return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
        }

        if($vendorData['info']['oprDown'] == '1'){
            return array('status'=>'failure','code'=>'43','description'=>$vendorData['info']['down_note']);
        }

        $opr = trim(substr($mobileNo,0,4));
       
        $vendorId = $vendorData['primary']['vendor_id'];
        if(empty($vendorId)){
            $vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
            $vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];
        }
        else $vendorShortform = $vendorData['primary']['shortForm'];
       
       
        if(trim($params['amount']) < $vendorData['info']['min']){
            return array('status'=>'failure','code'=>'33','description'=>'Minimum recharge amount is Rs.'.$vendorData['info']['min']);
        }

        if(trim($params['amount']) > $vendorData['info']['max']){
            return array('status'=>'failure','code'=>'34','description'=>'Maximum recharge amount is Rs.'.$vendorData['info']['max']);
        }

        if(in_array($params['amount'],explode(",",$vendorData['info']['invalid']))){//mtnl 125
            $desc = 'Recharge of Rs. '.trim($params['amount']).' is temporary not available';
            return array('status'=>'failure','code'=>'33','description'=> $desc);
        }


        $createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],null,$params['ip']);
        if($createTran['status'] == 'failure') return $createTran;

		$this->General->logData("/mnt/logs/abc_$vendorId.txt",$createTran['tranId']."::$prodId::$vendorId::first::".json_encode($_SESSION));
		
		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = $vendorShortform;
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'prepaid';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
        $params['retailer_name'] = isset($_SESSION['Auth']['shopname'])?$_SESSION['Auth']['shopname']:"";
        $params['retailer_mobile'] = isset($_SESSION['Auth']['mobile'])?$_SESSION['Auth']['mobile']:"";
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";

        $params['area'] = empty($opData['area']) ? "" : $opData['area'];
        $pars['params'] = json_encode($params);
        $pars['product_id'] = $prodId;
       
        if(isset($vendorData['primary']['key_vendor'])){
            $pars['key_vendor'] = $vendorData['primary']['key_vendor'];
        }
        $this->send_request_via_tps($createTran['tranId'],$pars);
        //if(SITE_NAME == "http://54.235.193.96/"){
//            $handler_Q = "TXN_REQUEST_QUEUE";
//            $TPS_REQUEST_HASH = "TPS_REQUEST_DATA";
//            $redisObj = $this->Shop->redis_connector();
//            if($redisObj != false){
//                $redisObj->hset($TPS_REQUEST_HASH,$createTran['tranId'],  json_encode($pars));
//                $redisObj->lpush($handler_Q,$createTran['tranId']);
//            }
//        }else{
//            $this->General->curl_post_async($url,$pars);
//        }

        return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId'],'service_charge'=>$createTran['service_charge']);
    }
    
        function send_request_via_tps($txnid,$pars){
            $handler_Q = "TXN_REQUEST_QUEUE";
            $TPS_REQUEST_HASH = "TPS_REQUEST_DATA";
            try{
                $redisObj = $this->Shop->redis_connector();
                if($redisObj != false){
                    $tpsq_len = $redisObj->llen($handler_Q);
                    if($tpsq_len > 40 ){
                            $this->General->logData("/mnt/logs/new_request.txt", "Recharge request::".$txnid." :: Recharges queue length is greater than threshold . current size : $tpsq_len ");
                            $redisObj->incr("TPS_MARKER");
                    }
                    $redisObj->hset($TPS_REQUEST_HASH,$txnid,  json_encode($pars));
                    $this->Shop->setMemcache('TPS_REQUEST_DATA_'.$txnid,json_encode($pars),5*60);
                    usleep(100000);
                    $redisObj->lpush($handler_Q,$txnid);
                }else{
                    throw new Exception("Unable to connect redis");
                }                
            }catch(Exception $ex){
                $this->General->logData("/mnt/logs/new_tps_request.txt","txn id = ".$txnid." | exception : ".$ex->getMessage());
                $url = SERVER_PROTECTED . "recharges/afterTransaction";
                $pars['tranId'] = $txnid;
                $pars['encSign'] = urlencode(strtoupper(sha1(encKey.$txnid)));
                $this->General->curl_post($url,$pars);
            }
            if(!empty($redisObj) || $redisObj !== false )$redisObj->quit();
        }

	//dth recharges
	function dthRecharge($params){
		if(!isset($this->mapping['dthRecharge'][$params['operator']]))
		return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));

        /* decimal amount check */
        if (strpos($params['amount'], '.')){
            return array('status'=>'failure','code'=>'6','description'=>$this->Shop->errors(6));
        }
        
		$prodId = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['id'];

		if($prodId == '')
		return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));


		//get active vendor
        $params['api_partner'] = isset($params['api_partner']) ? $params['api_partner'] : "";
		$additional_param = array('amount'=>$params['amount'],'dist_id'=>$_SESSION['Auth']['parent_id'],'retailer_created'=>$_SESSION['Auth']['created']);
		
        $vendorData = $this->Shop->getActiveVendor($prodId,null,$params['api_partner'],true,$additional_param);
		/*if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
			}*/

		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for DTH recharge.','Product : '.$vendorData['info']['name'],array('backend@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		$vendorId = $vendorData['primary']['vendor_id'];
		if(empty($vendorId)){
			$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
			$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];
		}
		else $vendorShortform = $vendorData['primary']['shortForm'];

		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum recharge amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum recharge amount is Rs.'.$vendorData['info']['max']);
		}

		if(in_array($params['amount'],explode(",",$vendorData['info']['invalid']))){
			$desc = 'Recharge of Rs. '.trim($params['amount']).' is not valid';
			return array('status'=>'failure','code'=>'33','description'=> $desc);
		}

		/*if($prodId == 21 && $params['amount'] < 500 && !in_array($params['amount'],array(150,200,250,300,350,400,450))){//Videocon d2h
			$desc = "Videocon d2h Min Recharge is Rs.150 & upto Rs.500 can be done only in multiples of Rs.50. Recharges >500 can be done of any value";
			return array('status'=>'failure','code'=>'33','description'=>$desc);
			}*/

		$len = strlen($params['subId']);
		if($prodId == 16 && ($len != 10 || substr($params['subId'],0,2) != "30")){//Airtel DTH
			return array('status'=>'failure','code'=>'39','description'=>'Customer ID should start with 30 & should be 10 digit long');
		}
		else if($prodId == 17 && ($len != 12 || substr($params['subId'],0,2) != "20")){//Big TV
			return array('status'=>'failure','code'=>'39','description'=>'Smart card no. should start with 20 & should be 12 digit long');
		}
		else if($prodId == 18 && ($len != 11)){//Dish TV
			return array('status'=>'failure','code'=>'39','description'=>'Viewing card no. should start with 0 & should be 11 digit long');
		}
		else if($prodId == 19 && ($len != 11 || !in_array(substr($params['subId'],0,1),array(1,4)))){//Sun TV
			return array('status'=>'failure','code'=>'39','description'=>'Smart card no. should start with 1 or 4 & should be 11 digit long');
		}
		else if($prodId == 20 && ($len != 10 || (substr($params['subId'],0,2) != "10" && substr($params['subId'],0,2) != "11" && substr($params['subId'],0,2) != "12"))){//Tata Sky
			return array('status'=>'failure','code'=>'39','description'=>'Subscriber ID should start with 10, 11 or 12 & should be 10 digit long');
		}
		
		//call to shop api to insert a record in produts_users
		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],$params['subId'],$params['ip']);
		if($createTran['status'] == 'failure') return $createTran;
		
		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = $vendorShortform;
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'dth';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
        $params['retailer_name'] = isset($_SESSION['Auth']['shopname'])?$_SESSION['Auth']['shopname']:"";
		$params['retailer_mobile'] = isset($_SESSION['Auth']['mobile'])?$_SESSION['Auth']['mobile']:"";
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";

        $params['area'] = isset($opData['area']) ? $opData['area'] : "";
		$pars['product_id'] = $prodId;
		$pars['params'] = json_encode($params);
		//$this->General->curl_post_async($url,$pars);
                $this->send_request_via_tps($createTran['tranId'],$pars);
		return array('status'=>'success','balance'=>$createTran['balance'],'description' => $createTran['tranId'],'service_charge'=>$createTran['service_charge']);
	}

	//mobile recharges
	function billPayment($params=null){
		/*$fh = fopen("d:\\bill.txt","a+");
		 fwrite($fh,json_encode($params)."\n--");
		 fclose($fh);*/
		if(!isset($this->mapping['billPayment'][$params['operator']]))
		return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));

		$prodId = $this->mapping['billPayment'][$params['operator']][$params['type']]['id'];

		if($prodId == '')return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));

		$mobileNo = $params['mobileNumber'];

		//get active vendor
        $params['api_partner'] = isset($params['api_partner']) ? $params['api_partner'] : "";
		$vendorData = $this->Shop->getActiveVendor($prodId,$mobileNo,$params['api_partner']);
			
		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}

		if(in_array($_SESSION['Auth']['rental_flag'],array(1,2)) && $vendorData['info']['service_id'] == 4){//if retailer is on rental, he is not allowed to do bill payment
			return array('status'=>'failure','code'=>'44','description'=>$this->Shop->errors(44));
		}

		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for mobile bill payment.','Product : '.$vendorData['info']['name'] . '<br/>'.$prodId,array('ashish@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		$opr = trim(substr($mobileNo,0,4));
		$vendorId = $vendorData['primary']['vendor_id'];
		if(empty($vendorId)){
			$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
			$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];
		}
		else $vendorShortform = $vendorData['primary']['shortForm'];
		
		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum bill amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum bill amount is Rs.'.$vendorData['info']['max']);
		}

		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],null,$params['ip']);
		if($createTran['status'] == 'failure') return $createTran;

		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = $vendorShortform;
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'postpaid';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
        $params['retailer_name'] = isset($_SESSION['Auth']['shopname'])?$_SESSION['Auth']['shopname']:"";
		$params['retailer_mobile'] = isset($_SESSION['Auth']['mobile'])?$_SESSION['Auth']['mobile']:"";
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";
        
		$params['area'] = isset($opData['area']) ? $opData['area'] : "";
		$pars['params'] = json_encode($params);
		$pars['product_id'] = $prodId;
		//$this->General->curl_post_async($url,$pars);
                $this->send_request_via_tps($createTran['tranId'],$pars);
		return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId'],'service_charge'=>$createTran['service_charge']);
	}

	function pay1Wallet($params){
		$prodId = WALLET_ID;
		//$vendorId = WALLET_VENDOR;

		$mobileNo = $params['mobileNumber'];
		//get active vendor
		$vendorData = $this->Shop->getActiveVendor($prodId,$mobileNo);

        /* decimal amount check */
        if (strpos($params['amount'], '.')){
            return array('status'=>'failure','code'=>'6','description'=>$this->Shop->errors(6));
        }
        
		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for mobile recharge.','Product : '.$vendorData['info']['name'] . '<br/>'.$prodId,array('ashish@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
		$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];

		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum recharge amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum recharge amount is Rs.'.$vendorData['info']['max']);
		}

		if(in_array($params['amount'],explode(",",$vendorData['info']['invalid']))){//mtnl 125
			$desc = 'Recharge of Rs. '.trim($params['amount']).' is not valid';
			return array('status'=>'failure','code'=>'33','description'=> $desc);
		}

		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}

		/*if($_SESSION['Auth']['rental_flag'] == 1){
			return array('status'=>'failure','code'=>'44','description'=>$this->Shop->errors(44));
			}*/

		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],null,$params['ip']);
		if($createTran['status'] == 'failure') return $createTran;

		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = 'b2c';
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'wallet';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
		$params['retailer_name'] = $_SESSION['Auth']['shopname'];
		$params['retailer_mobile'] = $_SESSION['Auth']['mobile'];
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";

		$pars['params'] = json_encode($params);
		$pars['product_id'] = $prodId;
		//$this->General->curl_post_async($url,$pars);
                $this->send_request_via_tps($createTran['tranId'],$pars);
		return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId']);
	}
    
    /*
     * cashpg payment main function
     */
    function cashpgPayment($params){
         App::Import('Model', 'C2d');  
        $C2d = new C2d(); 
        $prodId = $params['operator'];
		$mobileNo = $params['mobileNumber'];
        $logger = $this->General->dumpLog('get_pending_request_by_mobile', 'cash_pg_payment');
        $logger->info("Received param : ".  json_encode($params));
		//get active vendor
		$vendorData = $this->Shop->getActiveVendor($prodId,$mobileNo);
        /* decimal amount check */
                
		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for mobile recharge.','Product : '.$vendorData['info']['name'] . '<br/>'.$prodId,array('ashish@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
		$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];

		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum recharge amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum recharge amount is Rs.'.$vendorData['info']['max']);
		}

		if(in_array($params['amount'],explode(",",$vendorData['info']['invalid']))){//mtnl 125
			$desc = 'Recharge of Rs. '.trim($params['amount']).' is not valid';
			return array('status'=>'failure','code'=>'33','description'=> $desc);
		}

		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}

		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],null,$params['ip']);
        
        $logger->info("createTran : ".  json_encode($createTran));
        
		if($createTran['status'] == 'failure') return $createTran;

        //-----------------------------------
        if (isset($params['ref_id']) && !empty($params['ref_id'])) {
            $cash_txn_qry = "SELECT * FROM cash_payment_txn where id='" . $params['ref_id'] . "' and status=0 ";
            $cash_txn_detail = $C2d->query($cash_txn_qry);
            if (empty($cash_txn_detail)) {
                $desc = array('status'=>'failure','errCode'=>'1001','description'=>'Invalid transaction Id');
            }elseif (isset($cash_txn_detail[0]['cash_payment_txn']['amount']) &&  $cash_txn_detail[0]['cash_payment_txn']['amount'] != $params['amount'] ) {
                $desc = array('status'=>'failure','errCode'=>'1003','description'=>'Invalid transaction amount');
            } 
            else {
                $processtime=date('Y-m-d H:i:s');
                $txn_update_qry = "UPDATE cash_payment_txn SET status=1,updated_time='".$processtime."',va_refcode='".$createTran['tranId']."' where id='" . $params['ref_id'] . "' and status=0";
                $txnstatus = $C2d->query($txn_update_qry);
                
                $logger->info("txn status : ".  json_encode($txnstatus));
                
                if ($txnstatus) {
                    $clientId = $cash_txn_detail[0]['cash_payment_txn']['cash_client_id'];
                    $cash_client_detail_qry = "SELECT * FROM cash_payment_client WHERE id='" . $clientId . "'";
                    $cash_client_detail = $C2d->query($cash_client_detail_qry);

                    $params_to_send['request_id'] = $params['ref_id'];
                    $params_to_send['client_ref_id'] = $cash_txn_detail[0]['cash_payment_txn']['id'];
                    $params_to_send['status'] = 1;
                    $params_to_send['timestamp'] = date('Y-m-d H:i:s');
                    $params_to_send['mobile'] = $mobileNo;
                    $params_to_send['amount'] = $params['amount'];
                    
                    $logger->info("params to send : ". json_encode($params_to_send));
                    
                    $client_callback_url = $cash_client_detail[0]['cash_payment_client']['callback_api'];
                    $this->General->curl_post_async($client_callback_url, $params_to_send);
                    $desc = array('status'=>'success','errCode'=>0,'description'=>array('transaction_id'=>$params_to_send['client_ref_id']));
                }
            }
        } else {
            //--- Missing / Blank transaction
            $desc = array('status'=>'failure','errCode'=>'1002','description'=>'Missing ref_id');
            $logger->warn(" failure : ". json_encode($params_to_send));
        }
        
        $transId = $createTran['tranId'];
        
        $this->Shop->addStatus($transId, $vendorId);
        $vndrId = isset($params['ref_id']) ? $params['ref_id'] : "";
        $logger->debug("out - ".  json_encode($desc));
        
        if ($desc['status'] == 'success') {
            $txnId = $desc['description']['transaction_id'];            
            $logger->debug(" update param : ". $transId."|".$vndrId."|".$desc['status']."|31|".$this->Shop->errors(31)."|".null."|".$prodId."|".$vendorId."|".$prodId);
            $this->Shop->updateTransaction($transId,$vndrId,$desc['status'],31,$this->Shop->errors(31),null,$prodId,$vendorId,$prodId);            
            $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('" . $transId . "','$txnId','7','$vendorId','13','Successful','success','" . date("Y-m-d H:i:s") . "')");
            return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId']);
            //return array('status' => 'success', 'code' => '31', 'description' => $this->Shop->errors(31), 'tranId' => $txnId, 'pinRefNo' => '', 'operator_id' => '');
        } else if ($desc['status'] == 'failure') {
            $err_code = $desc['errCode'];
            $msg = $desc['description'];
            $logger->debug(" update param : ". $transId."|".$vndrId."|".$desc['status']."|30|".$this->Shop->errors(30)."|".null."|".$prodId."|".$vendorId."|".$prodId);
            $this->Shop->updateTransaction($transId,$vndrId,$desc['status'],30,$this->Shop->errors(30),null,$prodId,$vendorId,$prodId);
            $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('" . $transId . "','','7','$vendorId','14','" . addslashes("Error code: $err_code, " . $msg) . "','failure','" . date("Y-m-d H:i:s") . "')");
            return array('status' => 'failure', 'code' => $desc['errCode'], 'description' => $desc['description']);
        }
        //-----------------------------------        
	}

	//vas recharge
	function vasRecharge($params){
		if(!isset($params['Amount'])){
			$data = $this->Slaves->query("SELECT product_code,price,params FROM products_info WHERE product_id = " . $params['product']);

			if(empty($data)){
				return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));
			}
		}
		$prodId = $params['product'];

		/*$ret = $this->Shop->checkPossibility($prodId,$params['Mobile'],$params['amount'],$params['power']);
		 if($ret != null) {
			//$desc = $this->Shop->errors(37);
			//$desc .= "\nMobile: $mobileNo, Amount: ".$params['amount'];
			return array('status'=>'failure','code'=>'37','description'=>$ret);
			}*/
		//get active vendor
		$vendorData = $this->Shop->getActiveVendor($prodId);
		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}

		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('VAS Product not active.','Product: '.$vendorData['info']['name'],array('backend@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		if($prodId == 35){
			$pack = $this->getDittoPWList($params['Amount']);
			$validAmt = $pack['status'];
			//$denomDet = $pack['denomDet'];
			//$type = $pack['type'];
			if(!$validAmt){
				return array('status'=>'failure','code'=>'6','description'=>"Invalid pack/wallet amount.");
			}

		}

		$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
		$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];


		$allParams = json_decode($data['0']['products_info']['params'],true);

		$verify = $this->Shop->verifyParams($params,$allParams);
		if($verify['status'] == 'failure'){
			return $verify;
		}

		if(!empty($data['0']['products_info']['price'])){
			$price = $data['0']['products_info']['price'];
		}
		else {
			$price = $params['Amount'];
		}
		if(isset($params['param'])){
			$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['Mobile'],$price,$params['param'],$params['ip']);
		}
		else {
			$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['Mobile'],$price,null,$params['ip']);
		}
		if($createTran['status'] == 'failure') return $createTran;

		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = $vendorShortform;
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'vas';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
        $params['retailer_name'] = isset($_SESSION['Auth']['shopname'])?$_SESSION['Auth']['shopname']:"";
		$params['retailer_mobile'] = isset($_SESSION['Auth']['mobile'])?$_SESSION['Auth']['mobile']:"";
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";
        
		$pars['params'] = json_encode($params);
		$pars['product_id'] = $prodId;
		//$this->General->curl_post_async($url,$pars);
                $this->send_request_via_tps($createTran['tranId'],$pars);
		return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId']);
	}

	function busBooking($params){
		if(!isset($this->mapping['busBooking'][$params['operator']]))
		return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));

		$prodId = $this->mapping['busBooking'][$params['operator']][$params['type']]['id'];

		if($prodId == '')
		return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));

		//get active vendor
		$vendorData = $this->Shop->getActiveVendor($prodId);

		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for bus booking.','Product : '.$vendorData['info']['name'],array('backend@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}

		$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
		$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];
		//echo "<pre>vendorData="; print_r($vendorData);
		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum recharge amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum recharge amount is Rs.'.$vendorData['info']['max']);
		}

		if(in_array($params['amount'],explode(",",$vendorData['info']['invalid']))){
			$desc = 'Recharge of Rs. '.trim($params['amount']).' is not valid';
			return array('status'=>'failure','code'=>'33','description'=> $desc);
		}
		/*if($prodId == 21 && $params['amount'] < 500 && !in_array($params['amount'],array(150,200,250,300,350,400,450))){//Videocon d2h
			$desc = "Videocon d2h Min Recharge is Rs.150 & upto Rs.500 can be done only in multiples of Rs.50. Recharges >500 can be done of any value";
			return array('status'=>'failure','code'=>'33','description'=>$desc);
			}*/

		//call to shop api to insert a record in produts_users
		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],$params['subId'],$params['ip']);
		//echo "createTran===<pre>"; print_r($createTran);
		if($createTran['status'] == 'failure') return $createTran;

		if($params['amount'] == ''){
			$reply['code'] = 'failure';
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','3','14','14','','failure','".date("Y-m-d H:i:s")."')");
		}else {
			//echo "<br/>amount====".$params['amount'];
			App::import('Controller', 'Redbus');
			$obj = new RedbusController;
			$obj->constructClasses();
			$Rec_Data = $obj->bookTicket($params);
				
			exit;

			if(!is_array($Rec_Data)){
				$reply['tranId'] = null;
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$reply['tranId']."','4','12','','".addslashes(serialize($Rec_Data))."','failure','".date("Y-m-d H:i:s")."')");
				$reply['status'] = 'failure';
				$reply['description'] = 'Error response from vendor: ' . serialize($Rec_Data);
				$reply['code'] = 'failure';
				//$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
				//logger
			}else{
					
				try{

					$resArr = explode(",",$Rec_Data);

					if(!empty($resArr['availableTripId'])){
						if( $resArr['availableTripId']!=""){//success state
							//success
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$resArr['tranId']."','4','12','','".addslashes(serialize($Rec_Data))."','success','".date("Y-m-d H:i:s")."')");
							$reply['tranId'] = $resArr['availableTripId'];
							$reply['status'] = 'success';
							$reply['description'] = 'successful transaction';
							$reply['code'] = 'success';
							$this->General->sendMails("Success: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));

						}else {//Failure state
							$reply['tranId'] = empty($resArr[0])?0 : $resArr[0];
								
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$reply['tranId']."','3','14','14','".addslashes($resArr[1])."','failure','".date("Y-m-d H:i:s")."')");
							$reply['status'] = 'failure';
							$reply['description'] = $resArr[1];
							$reply['code'] = 'failure:'.$resArr[1];
							$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
							if(trim($resArr[1])=="Login Failed" && $failed == null){//If failed due  to the reason "Login Failed" then rehit api
								$this->dittoRecharge($transId,$params,$prodId = null , $failed = "LoginFailed");
							}
						}
					}else{//InProcess State , when responce is not in proper format

						$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','14','15','No response from vendor','process','".date("Y-m-d H:i:s")."')");

						$reply['tranId'] = null;
						$reply['status'] = 'process';
						$reply['description'] = 'Invalid response from vendor: ' . $Rec_Data;
						$reply['code'] = 'process';
						$this->General->sendMails("Process: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
					}

				}catch(Exception $e){
					//echo "Exception";
				}
			}

		}
		$reply['pinRefNo'] = null;
		//                echo "<pre>";
		//                print_r($reply);
		//                echo "<pre>";
		return $reply;
	}

	function utilityBillPayment($params=null){
		if(!isset($this->mapping['utilityBillPayment'][$params['operator']]))
		return array('status'=>'failure','code'=>'8','description'=>$this->Shop->errors(8));

		$prodId = $this->mapping['utilityBillPayment'][$params['operator']][$params['type']]['id'];

		if($prodId == '')return array('status'=>'failure','code'=>'9','description'=>$this->Shop->errors(9));

		$mobileNo = $params['mobileNumber'];

		//get active vendor
		$vendorData = $this->Shop->getActiveVendor($prodId,$mobileNo,$params['api_partner']);
			
		if(in_array($_SESSION['Auth']['slab_id'],$vendorData['info']['blocked_slabs'])){
			return array('status'=>'failure','code'=>'43','description'=>$this->Shop->errors(43));
		}

		if($_SESSION['Auth']['rental_flag'] == 1 && $vendorData['info']['service_id'] == 6){//if retailer is not on kit he can't do utility bill payments
			return array('status'=>'failure','code'=>'44','description'=>$this->Shop->errors(44));
		}

		if($vendorData['status'] == 'failure'){
			$this->General->sendMails('SOS: No active vendor for utility bill payment.','Product : '.$vendorData['info']['name'] . '<br/>'.$prodId,array('ashish@mindsarray.com'));
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29));
		}

		if($vendorData['info']['oprDown'] == '1'){
			return array('status'=>'failure','code'=>'33','description'=>$vendorData['info']['down_note']);
		}
		
		$len = strlen($params['accountNumber']);
		if(isset($params['param']))$len_cycle = strlen($params['param']);
			
		if($prodId == 45 && ($len != 9 || $len_cycle != 2)){//Reliance Energy
			if($len != 9){
				return array('status'=>'failure','code'=>'46','description'=>'Account number should be a 9 digit number');	
			}
			else if($len_cycle != 2){
				return array('status'=>'failure','code'=>'46','description'=>'Cycle number should be a 2 digit number');	
			}
		}
		else if(($prodId == 46 || $prodId== 47) && !($len >= 9 && $len <=10)){//BSES
			return array('status'=>'failure','code'=>'46','description'=>'Wrong account number');
		}
		else if($prodId == 48 && !($len >= 11 && $len <=12)){//North Delhi Power Limited
			return array('status'=>'failure','code'=>'46','description'=>'Wrong account number');
		}
		else if($prodId == 49 && $len < 10){//Airtel Landline
			return array('status'=>'failure','code'=>'46','description'=>'Please enter phone number with std code');
		}
		else if($prodId == 50 && ($len != 8 || $len_cycle != 10)){//MTNL Delhi
			if($len_cycle != 10){
				return array('status'=>'failure','code'=>'46','description'=>'Account number should be a 10 digit number');	
			}
			else if($len != 8){
				return array('status'=>'failure','code'=>'46','description'=>'Phone number should be a 8 digit number');	
			}
		}
		else if($prodId == 51 && $len != 12){//Mahanagar Gas Limited
			return array('status'=>'failure','code'=>'46','description'=>'Wrong account number');
		}

		$opr = trim(substr($mobileNo,0,4));
		$vendorId = $vendorData['info']['vendors']['0']['vendor_id'];
		$vendorShortform = $vendorData['info']['vendors']['0']['shortForm'];

		if(trim($params['amount']) < $vendorData['info']['min']){
			return array('status'=>'failure','code'=>'33','description'=>'Minimum bill amount is Rs.'.$vendorData['info']['min']);
		}

		if(trim($params['amount']) > $vendorData['info']['max']){
			return array('status'=>'failure','code'=>'34','description'=>'Maximum bill amount is Rs.'.$vendorData['info']['max']);
		}

		$par = $params['accountNumber'];
		if(!empty($params['param']))$par = "$par*".$params['param'];
		$createTran = $this->Shop->createTransaction($prodId,$vendorId,$params['api_flag'],$params['mobileNumber'],$params['amount'],$par,$params['ip']);
		if($createTran['status'] == 'failure') return $createTran;

		$url = SERVER_PROTECTED . "recharges/afterTransaction";
		$pars['vendor_short'] = $vendorShortform;
		$pars['vendor_id'] = $vendorId;
		$pars['tranId'] = $createTran['tranId'];
		$pars['type'] = 'utility';
		$params['retailer_code'] = $_SESSION['Auth']['id'];
        $params['retailer_name'] = isset($_SESSION['Auth']['shopname'])?$_SESSION['Auth']['shopname']:"";
		$params['retailer_mobile'] = isset($_SESSION['Auth']['mobile'])?$_SESSION['Auth']['mobile']:"";
        $params['b2c_campaign_flag'] = isset($_SESSION['Auth']['b2c_campaign_flag'])?$_SESSION['Auth']['b2c_campaign_flag']:"";

		$params['area'] = isset($opData['area']) ? $opData['area'] : "";
		$pars['params'] = json_encode($params);
		$pars['product_id'] = $prodId;
		//$this->General->curl_post_async($url,$pars);
                $this->send_request_via_tps($createTran['tranId'],$pars);

		return array('status'=>'success','balance'=>$createTran['balance'],'description'=>$createTran['tranId'],'service_charge'=>$createTran['service_charge']);
	}

	function b2cPay1Wallet($transId,$params,$prodId = null){
		$vendorId = 22;
		$url = B2C_REFILL_URL;
		
        $_SESSION['Auth']['User']['id'] = empty($_SESSION['Auth']['User']['id']) ? "" : $_SESSION['Auth']['User']['id'];
		$lat_long = $this->Slaves->query("SELECT longitude,latitude FROM user_profile WHERE user_id=".$_SESSION['Auth']['User']['id']." AND longitude != 0 AND latitude != 0 ORDER BY updated DESC LIMIT 1");
					
		$data = array('trans_id'=>$transId,'mobile_number'=>$params['mobileNumber'],'amount'=>$params['amount'],'retailer_id'=>$params['retailer_code'],'retailer_shop'=>$params['retailer_name'],'retailer_mobile'=>$params['retailer_mobile']);
		if(!empty($lat_long)){
			$data['latitude'] = $lat_long[0]['user_profile']['latitude'];
			$data['longitude'] = $lat_long[0]['user_profile']['longitude'];
		}
		
		$Rec_Data = $this->General->curl_post($url,$data);

		if(!$Rec_Data['success']){
			if($Rec_Data['timeout']){
				//$this->User->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','5','$vendorId','14','Connectivity timeout from b2c server','failure','".date("Y-m-d H:i:s")."')");                               
                                $this->General->log_in_vendor_message(array($transId,'','5',$vendorId,'14','Connectivity timeout from b2c server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $Rec_Data['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/pay1.txt",date('Y-m-d H:i:s').":Request Sent: ".$url."::".json_encode($data)."::output: ".$out);

		$desc = json_decode($out,true);

		$this->Shop->addStatus($transId,$vendorId);
		if($desc['status'] == 'success'){
			$txnId = $desc['description']['transaction_id'];
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$txnId','5','$vendorId','13','Successful','success','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'5',$vendorId,'13','Successful','success',date("Y-m-d H:i:s")));
			return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>'');
		}
		else if($desc['status'] == 'failure'){
			$err_code = $desc['errCode'];
			$msg = $desc['description'];
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','5','$vendorId','14','".addslashes("Error code: $err_code, ". $msg)."','failure','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,'','5',$vendorId,'14',addslashes("Error code: $err_code, ". $msg),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','5','$vendorId','15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','5',$vendorId,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
	}

	function chatRecharge($transId,$params,$prodId = null){
		$data = $this->Slaves->query("SELECT product_code,price,params FROM products_info WHERE product_id = " . $params['product']);

		$retailer_code = $params['retailer_code'];
		$productId = $data['0']['products_info']['product_code'];
		$mobile = $params['Mobile'];

		if($productId == 'CB99'){
			$duration = 30;
		}
		else if($productId == 'CB249'){
			$duration = 90;
		}

		$retData = $this->Shop->getShopDataById($retailer_code,RETAILER);

		$requestXML = "<strMobileNo>$mobile</strMobileNo><Duration>$duration</Duration><PackName>$productId</PackName><RequestType>Paid</RequestType>";
		$response = $this->chatApi('SCSubscriber',$requestXML);

		if(isset($response['string']['0'])){
			if($response['string']['0'] == 'Fail'){
				$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','6','14','".addslashes($response['string']['1'])."','failure','".date("Y-m-d H:i:s")."')");
					
				$reply['tranId'] = null;
				$reply['status'] = 'failure';
				$reply['description'] = 'Wrong ' . $response['string']['1'];
				$reply['code'] = 'failure';
				//$this->General->sendMails("Failed: CHAT Product - $productId","Subscribed by $mobile, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
			}
			else if($response['string']['0'] == 'SUCCESS'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$response['string']['1']."','3','6','13','','success','".date("Y-m-d H:i:s")."')");
					
				$reply['tranId'] = $response['string']['1'];
				$reply['status'] = 'success';
				$reply['description'] = $Rec_Data['resultActivation']['message'];
				$reply['code'] = 'success';
				//$this->General->sendMails("Success: CHAT Product - $productId","Subscribed by $mobile, Retailer: ".$retData['shopname']."(".$retData['mobile'].")",array('tadka@mindsarray.com'));
			}
		}
		else{
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','6','14','No response from vendor','failure','".date("Y-m-d H:i:s")."')");

			$reply['tranId'] = null;
			$reply['status'] = 'failure';
			$reply['description'] = 'No response from vendor';
			$reply['code'] = 'failure';
			//$this->General->sendMails("Failed: CHAT Product - $productId","Subscribed by $mobile, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
		}
		$reply['pinRefNo'] = null;
		return $reply;
	}

	function dittoRechargeOld($transId,$params,$prodId = null){
		$retailer_code = $params['retailer_code'];
		$amount = intval($params['Amount']);
		$mobile = substr($params['Mobile'],-10);

		$valids = array(60,99,129,349);
		if(false){
			//if(!in_array($amount,$valids)){
			$reply['tranId'] = null;
			$reply['status'] = 'failure';
			$reply['description'] = 'Invalid amount';
			$reply['code'] = 'failure';
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','3','14','14','','failure','".date("Y-m-d H:i:s")."')");
		}
		else {
			$retData = $this->Shop->getShopDataById($retailer_code,RETAILER);

			$url = DITTO_RECHARGE_URL;
			$content = "orderId=$transId&vendor=$retailer_code&amount=$amount&mobileno=91$mobile";

			$key = "53faifj23hs";
			$hash_key = urlencode(sha1("$transId:$amount:91$mobile:$key"));
			$content = "$content&hash_key=$hash_key";
				
			$Rec_Data = $this->General->curl_post($url,$content);
				
			if(!$Rec_Data['success']){
				if($Rec_Data['timeout']){
					$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','14','14','Connectivity failure with the server','failure','".date("Y-m-d H:i:s")."')");
						
					$reply['tranId'] = null;
					$reply['status'] = 'failure';
					$reply['description'] = 'Connectivity failure with the server';
					$reply['code'] = 'failure';
					$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
					$reply['pinRefNo'] = null;
					return $reply;
				}
			}
			$Rec_Data = trim($Rec_Data['output']);
				
			$response = explode("-",$Rec_Data);
			if(count($response) == 1){
				$response = explode(":",$Rec_Data);
			}
				
			if(strtoupper(trim($response[0])) == 'SUCCESS'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$response[1]."','3','14','13','','success','".date("Y-m-d H:i:s")."')");
					
				$reply['tranId'] = $response['1'];
				$reply['status'] = 'success';
				$reply['description'] = 'successful';
				$reply['code'] = 'success';
				$this->General->sendMails("Success: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
			}
			else if(in_array(strtoupper(trim($response[0])),array('FAILED','ERROR'))){
				$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','14','14','".addslashes($response['1'])."','failure','".date("Y-m-d H:i:s")."')");
					
				$reply['tranId'] = null;
				$reply['status'] = 'failure';
				$reply['description'] = $response['1'];
				$reply['code'] = 'failure';
				$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
			}
			else {//inprocess state
				$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','14','15','No response from vendor','process','".date("Y-m-d H:i:s")."')");

				$reply['tranId'] = null;
				$reply['status'] = 'process';
				$reply['description'] = 'Invalid response from vendor: ' . $Rec_Data;
				$reply['code'] = 'process';
				$this->General->sendMails("Process: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
			}
		}

		$reply['pinRefNo'] = null;
		return $reply;
	}


	function getDittoPWList($amount=null){
		$response = $this->Shop->getMemcache('dittoPacks');

		if($response === false){
			$method = 'getUserPackDetails';
			$params = '';
			$response = $this->callDittoApi($method,$params);
			$response = $this->General->xml2array($response);
			$this->Shop->setMemcache('dittoPacks',$response,3*60*60);
		}
		if(!empty($amount)){
			$pwList = $response;
			$validAmt = false;
			$denomDet = array();
			$type = "";
			foreach ($pwList['ditto_packs'] as $key => $value) {//subwallets   subpacks
				foreach ($value as $k => $v) {
					if($key == 'subpacks'){
						foreach ($v as $keyi => $valuei) {
							if( $valuei['amount'] == $amount){
								$validAmt = true;
								$denomDet = $valuei;
								$type = $k;
								break 3;
							}
						}
					}else{
						if( $v['amount'] == $amount){
							$validAmt = true;
							$denomDet = $v;
							$type = $k;
							break 2;
						}
					}
				}
			}

			$response = array(
                            "status"=>$validAmt,
                            "denomDet"=>$denomDet,
                            "type"=>$type
			);
		}
		return( $response);
	}

	function callDittoApi($method,$params){

		App::import('vendor', 'soap', array('file' => 'soaplib/nusoap.php'));
		$proxyhost = '';//isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
		$proxyport = '';//isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
		$proxyusername = '';//isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
		$proxypassword = '';//isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
		$useCURL = '0';//isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
		$namespace  = SOAP_NAMESPACE_URL;
		$client = new nusoap_client(DITTO_SOAP_URL, false,$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$err = $client->getError();

		$headers = '<AuthenticationHeader xmlns="'.SOAP_NAMESPACE_URL.'"><UserName>payone</UserName><Password>p@y0n3</Password></AuthenticationHeader>';

		$res =  $client->call($method, $params,SOAP_NAMESPACE_URL,$namespace.$method,$headers);

		return( $res);

	}

	function testDittoRecharge(){
	 $transId = "10101010";
	 $params['retailer_code'] = "2";
	 $params['Amount'] = 10;
	 $params['Mobile'] = "9819032643";
	 $this->dittoRecharge($transId,$params,$prodId = null);

	}
	function dittoRecharge($transId,$params,$prodId = null , $failed = null){
			
		$retailer_code = $params['retailer_code'];
		$amount = intval($params['Amount']);
		$mobile = "0091".substr($params['Mobile'],-10);

		$pack = $this->getDittoPWList($amount);
		$validAmt = $pack['status'];
		$denomDet = $pack['denomDet'];
		$type = $pack['type'];
			
		if(!$validAmt){
			$reply['tranId'] = null;
			$reply['status'] = 'failure';
			$reply['description'] = 'Invalid amount';
			$reply['code'] = 'failure';
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','3','14','14','','failure','".date("Y-m-d H:i:s")."')");
		}else {
			$retData = $this->Shop->getShopDataById($retailer_code,RETAILER);
			$method = "";
			if($type == "wallet"){
				$method = 'topupWallet_complete';
				$params ='<mobileno>'.$mobile.'</mobileno>
                                    <pwd></pwd>
                                    <region>ind</region>
                                    <currency>'.(empty($denomDet['currency'])?"INR":$denomDet['currency']).'</currency>
                                    <amount>'.$amount.'</amount>
                                    <validitytype>'.$denomDet['validitytype'].'</validitytype>
                                    <validity>'.$denomDet['validity'].'</validity>
                                    <txnid>'.$transId.'</txnid>';
			}else if($type == "pack"){
				$method = 'activationPack_complete';
				$params ='<mobileno>'.$mobile.'</mobileno>
                                    <pwd></pwd>
                                    <region>ind</region>
                                    <packid>'.$denomDet['guid'].'</packid>
                                    <amount>'.$amount.'</amount>
                                    <validitytype>'.$denomDet['validitytype'].'</validitytype>
                                    <validity>'.$denomDet['validity'].'</validity>
                                    <txnid>'.$transId.'</txnid>';
			}

			$Rec_Data = $this->callDittoApi($method,$params);
			if(is_array($Rec_Data)){
				$reply['tranId'] = null;
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$reply['tranId']."','3','14','14','".addslashes(serialize($Rec_Data))."','failure','".date("Y-m-d H:i:s")."')");
				$reply['status'] = 'failure';
				$reply['description'] = 'Error response from vendor: ' . serialize($Rec_Data);
				$reply['code'] = 'failure';
				$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
			}else{
					
				try{

					$resArr = explode(",",$Rec_Data);

					if(!empty($resArr[1])){
						if( $resArr[1]=="success"){//success state
							//success
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$resArr[0]."','3','14','13','','success','".date("Y-m-d H:i:s")."')");
							$reply['tranId'] = $resArr[0];
							$reply['status'] = 'success';
							$reply['description'] = 'successful transaction';
							$reply['code'] = 'success';
							$this->General->sendMails("Success: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));

						}else {//Failure state
							$reply['tranId'] = empty($resArr[0])?0 : $resArr[0];
								
							$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$reply['tranId']."','3','14','14','".addslashes($resArr[1])."','failure','".date("Y-m-d H:i:s")."')");
							$reply['status'] = 'failure';
							$reply['description'] = $resArr[1];
							$reply['code'] = 'failure:'.$resArr[1];
							$this->General->sendMails("Failed: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
							if(trim($resArr[1])=="Login Failed" && $failed == null){//If failed due  to the reason "Login Failed" then rehit api
								$this->dittoRecharge($transId,$params,$prodId = null , $failed = "LoginFailed");
							}
						}
					}else{//InProcess State , when responce is not in proper format

						$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','3','14','15','No response from vendor','process','".date("Y-m-d H:i:s")."')");

						$reply['tranId'] = null;
						$reply['status'] = 'process';
						$reply['description'] = 'Invalid response from vendor: ' . $Rec_Data;
						$reply['code'] = 'process';
						$this->General->sendMails("Process: Ditto TV Product","Subscribed by $mobile, Amount: $amount, Retailer: ".$retData['shopname']."(".$retData['mobile']."), Reason: ". $reply['description'],array('tadka@mindsarray.com'));
					}

				}catch(Exception $e){
					//echo "Exception";
				}
			}

		}
		$reply['pinRefNo'] = null;
		//                echo "<pre>";
		//                print_r($reply);
		//                echo "<pre>";
		return $reply;
	}

	function afterTransaction(){
		$ips = explode(",",SERVER_IPS);
                $_SERVER['REMOTE_ADDR'] = (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "") ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];    
                
                $this->General->logData("/mnt/logs/request.txt","precheck Recharge request::".json_encode($_REQUEST));
		$sha_enc_hash=urlencode(strtoupper(sha1(encKey.$_REQUEST['tranId'])));
                if($sha_enc_hash !== $_REQUEST['encSign']){
                   exit; 
                }
                /*if(!in_array($_SERVER['REMOTE_ADDR'],$ips)){
			exit;
		}*/
		
		$this->General->logData("/mnt/logs/request.txt","Recharge request::".json_encode($_REQUEST));
		$param = json_decode($_REQUEST['params'],true);
        $cycle = "";
		if($_REQUEST['type'] == 'prepaid'){
			$funName = $_REQUEST['vendor_short']."MobRecharge";
			$service_id = 1;
			$mobile = $param['mobileNumber'];
			$par = null;
			$amount = $param['amount'];
			$funName1 = "modemMobRecharge";
		}
		else if($_REQUEST['type'] == 'dth'){
			$funName = $_REQUEST['vendor_short']."DthRecharge";
			$service_id = 2;
			$mobile = $param['mobileNumber'];
			$par = $param['subId'];
			$amount = $param['amount'];
			$funName1 = "modemDthRecharge";
		}
		else if($_REQUEST['type'] == 'vas'){
			$funName = $_REQUEST['vendor_short']."Recharge";
			$service_id = 3;
			$mobile = $param['Mobile'];
			$par = null;
			$amount = $param['Amount'];
			$funName1 = "modemRecharge";
		}else if($_REQUEST['type'] == 'postpaid'){
			$funName = $_REQUEST['vendor_short']."BillPayment";
			$service_id = 4;
			$mobile = $param['mobileNumber'];
			$par = null;
			$amount = $param['amount'];
			$funName1 = "modemBillPayment";
		}else if($_REQUEST['type'] == 'wallet'){
			$funName = "b2cPay1Wallet";
			$service_id = 5;
			$mobile = $param['mobileNumber'];
			$par = null;
			$amount = $param['amount'];
			$funName1 = "";
		}else if($_REQUEST['type'] == 'utility'){
			$funName = $_REQUEST['vendor_short']."UtilityBillPayment";
			$service_id = 6;
			$mobile = $param['mobileNumber'];
			$par = $param['accountNumber'];
			$cycle = $param['param'];
			$amount = $param['amount'];
			$funName1 = "";
		}     
		
		$transId = $_REQUEST['tranId'];

		if(!$this->Shop->lockTransaction($transId)){
			exit;
		}
        //--------tata adhock changes
        if(in_array($_REQUEST['product_id'],array(9)))$_REQUEST['product_id'] = 27;
        
		$circle = isset($param['area']) ? $param['area'] : "";
		$this->Shop->addMemcache("txn$transId",array('ref_code'=>$transId,'area'=>$circle,'service_id'=>$service_id,'product_id'=>$_REQUEST['product_id'],'mobile'=>$mobile,'param'=>$par,'amount'=>$amount,'extra'=>$cycle,'vendors'=>array($_REQUEST['vendor_id']),'timestamp'=>date('Y-m-d H:i:s')),15*60);
		
		if(isset($param['api_partner']) && $param['api_partner'] == 6) {//allowing b2c recharge window to be 15 mins
			$this->Shop->addMemcache("txnExp$transId",time() + 15*60,15*60);
		}
		else {
			$this->Shop->addMemcache("txnExp$transId",time() + 5*60,5*60);
		}

		//$this->User->query("insert into vendors_transactions(ref_id,vendor_id,status,date) values ('$transId',".$_REQUEST['vendor_id'].",0,'".date('Y-m-d')."')");
		$this->General->log_in_vendor_transaction(array($transId,$_REQUEST['vendor_id'],0,date('Y-m-d')));
                
		if(!method_exists($this, $funName) && method_exists($this, $funName1)){
			$vendorReply = $this->$funName1($transId,$param,$_REQUEST['product_id'],$_REQUEST['vendor_id'],$_REQUEST['vendor_short']);
		}
		else $vendorReply = $this->$funName($transId,$param,$_REQUEST['product_id']);

			
    	$try = false;
		if($vendorReply['status'] == 'failure' && !in_array($vendorReply['code'],array(5,6))){
			if(isset($_REQUEST['key_vendor'])){
				$this->Shop->incrementMemcache($_REQUEST['key_vendor']);
			}
			$try = $this->transferToOtherRoutes($transId,$_REQUEST['vendor_short'],true);
            
		}
        if(!$try) { $vendorReply['operator_id'] = empty ($vendorReply['operator_id']) ? ""  : $vendorReply['operator_id'] ; $this->Shop->updateTransaction($transId,$vendorReply['tranId'],$vendorReply['status'],$vendorReply['code'],$vendorReply['description'],$vendorReply['pinRefNo'],$vendorReply['operator_id'],$_REQUEST['vendor_id'],$_REQUEST['product_id']); }
        
        $this->General->logData("/mnt/logs/b2cextender.txt","i m above b2cextender condition $mobile ". $param['api_partner']. " ". $vendorReply['status'] . " $try  " . $param['b2c_campaign_flag']);
        	
        if(empty($param['api_partner']) && ($vendorReply['status'] != 'failure' || ($vendorReply['status'] == 'failure' && $try != false )) &&  !(in_array($_REQUEST['type'],array('wallet','cashpgPayment')))){
            //$this->General->logData("/mnt/logs/b2cextender.txt","i m inside b2cextender condition $mobile");
            $req_data = array('trans_id'=>$transId,'area'=>$circle,'flag'=>$service_id,'product_id'=>$_REQUEST['product_id'],
                    'mobile'=>$mobile,'param'=>$par,'amount'=>$amount,'timestamp'=>date('Y-m-d H:i:s'),'retailer_code'=>$param['retailer_code'],
                    'retailer_name'=>$param['retailer_name'],'retailer_mobile'=>$param['retailer_mobile'],
                    'b2c_campaign_flag'=>$param['b2c_campaign_flag']);            
        
            $this->B2cextender->manage_request_from_b2c_user($req_data);
        }
	}

        /**
         * 
         * @param type $request_id
         */
        function fetch_formated_request_data($request_id){
            $TPS_REQUEST_HASH = "TPS_REQUEST_DATA";
            $resquest_data = "";
            $this->General->logData("/mnt/logs/tps_changes-".date('Ymd').".txt",date('Y-m-d H:i:s').": in updater : data : ".$request_id);
            try{
                $redisObj = $this->Shop->redis_connector();
                if($redisObj == false){
                    throw new Exception("cannot create redis object");
                }else{
                    $resquest_data = $redisObj->hget($TPS_REQUEST_HASH,$request_id);
                    if(empty($resquest_data) || trim($resquest_data) == "" || $resquest_data === false ){
                        throw new Exception("txn id not found in redis hash");
                    }
                    $result = $redisObj->hdel($TPS_REQUEST_HASH,$request_id);
                    $_REQUEST = json_decode($resquest_data, true);
                    $this->General->logData("/mnt/logs/tps_changes.txt",date('Y-m-d H:i:s').": in updater : data : ".$TPS_REQUEST_HASH."| ".$request_id . " | " . $request_data);
                    
                    if($redisObj->hexists($TPS_REQUEST_HASH,$request_id)){
                        $this->General->logData("/mnt/logs/tps_changes.txt",date('Y-m-d H:i:s').": hash key not deleted ");
                        $redisObj->hdel($TPS_REQUEST_HASH,$request_id);
                    }
                }
            }catch(Exception $ex){
                $EXCEPTION_MSG = "";
                try{
                    $this->General->logData("/mnt/logs/tps_changes.txt",date('Y-m-d H:i:s')." | $request_id : Exception msg : ".$ex->getMessage());
                    $EXCEPTION_MSG = " redis exception : ".$ex->getMessage();
                    $resquest_data = $this->Shop->getMemcache('TPS_REQUEST_DATA_'.$request_id);
                    $_REQUEST = json_decode($resquest_data, true);
                    if(empty($_REQUEST)){
                        throw new Exception("value not found in memcache");
                    }
                } catch (Exception $ex1) {
                    $this->General->logData("/mnt/logs/tps_changes.txt",date('Y-m-d H:i:s')." | $request_id : Exception msg : ".$ex1->getMessage());
                    $EXCEPTION_MSG .= " memcach exception : ".$ex1->getMessage();
                }
                $this->General->sendMails('TSP redis connection issue'," date : ".date('Y-m-d H:i:s')." | txnid : ".$request_id."<br>".$EXCEPTION_MSG ,array('nandan@mindsarray.com','ashish@mindsarray.com'),'mail');
            }
            if(!empty($redisObj) || $redisObj !== false )$redisObj->quit();
            return $_REQUEST;
            $this->autoRender = false;
        }
        
        /**
         * 
         * @param type $request_id
         */
        function afterTransaction1($request_id) {
            $this->General->logData("/mnt/logs/new_request.txt", "Recharge request::".$request_id);
            $_REQUEST = $this->fetch_formated_request_data($request_id);
            $_SERVER['DOCUMENT_ROOT'] = '/var/www/html/shops/app/webroot';
            $this->General->logData("/mnt/logs/request.txt", "Recharge request::" . json_encode($_REQUEST));
            $param = json_decode($_REQUEST['params'], true);
            $cycle = "";
            if ($_REQUEST['type'] == 'prepaid') {
                $funName = $_REQUEST['vendor_short'] . "MobRecharge";
                $service_id = 1;
                $mobile = $param['mobileNumber'];
                $par = null;
                $amount = $param['amount'];
                $funName1 = "modemMobRecharge";
            } else if ($_REQUEST['type'] == 'dth') {
                $funName = $_REQUEST['vendor_short'] . "DthRecharge";
                $service_id = 2;
                $mobile = $param['mobileNumber'];
                $par = $param['subId'];
                $amount = $param['amount'];
                $funName1 = "modemDthRecharge";
            } else if ($_REQUEST['type'] == 'vas') {
                $funName = $_REQUEST['vendor_short'] . "Recharge";
                $service_id = 3;
                $mobile = $param['Mobile'];
                $par = null;
                $amount = $param['Amount'];
                $funName1 = "modemRecharge";
            } else if ($_REQUEST['type'] == 'postpaid') {
                $funName = $_REQUEST['vendor_short'] . "BillPayment";
                $service_id = 4;
                $mobile = $param['mobileNumber'];
                $par = null;
                $amount = $param['amount'];
                $funName1 = "modemBillPayment";
            } else if ($_REQUEST['type'] == 'wallet') {
                $funName = "b2cPay1Wallet";
                $service_id = 5;
                $mobile = $param['mobileNumber'];
                $par = null;
                $amount = $param['amount'];
                $funName1 = "";
            } else if ($_REQUEST['type'] == 'utility') {
                $funName = $_REQUEST['vendor_short'] . "UtilityBillPayment";
                $service_id = 6;
                $mobile = $param['mobileNumber'];
                $par = $param['accountNumber'];
                $cycle = $param['param'];
                $amount = $param['amount'];
                $funName1 = "";
            } else if ($_REQUEST['type'] == 'vendorWallet') {
                $funName = $_REQUEST['vendor_short']."Topup";
                $service_id = $_REQUEST['service_id'];
                $mobile = $param['mobileNumber'];
                $par = $param['param'];
                $amount = $param['amount'];
                $funName1 = "";
            }

            $transId = $_REQUEST['tranId'];

            if (!$this->Shop->lockTransaction($transId)) {
                exit;
            }
            //--------tata adhock changes
            if (in_array($_REQUEST['product_id'], array(9)))
                $_REQUEST['product_id'] = 27;

            $circle = isset($param['area']) ? $param['area'] : "";
            $this->Shop->addMemcache("txn$transId", array('ref_code' => $transId, 'area' => $circle, 'service_id' => $service_id, 'product_id' => $_REQUEST['product_id'], 'mobile' => $mobile, 'param' => $par, 'amount' => $amount, 'extra' => $cycle, 'vendors' => array($_REQUEST['vendor_id']), 'timestamp' => date('Y-m-d H:i:s')), 15 * 60);

            if (isset($param['api_partner']) && $param['api_partner'] == 6) {//allowing b2c recharge window to be 15 mins
                $this->Shop->addMemcache("txnExp$transId", time() + 15 * 60, 15 * 60);
            } else {
                $this->Shop->addMemcache("txnExp$transId", time() + 5 * 60, 5 * 60);
            }

            //$this->User->query("insert into vendors_transactions(ref_id,vendor_id,status,date) values ('$transId'," . $_REQUEST['vendor_id'] . ",0,'" . date('Y-m-d') . "')");
            $this->General->log_in_vendor_transaction(array($transId,$_REQUEST['vendor_id'],0,date('Y-m-d')));
            
            if (!method_exists($this, $funName) && method_exists($this, $funName1)) {
                $vendorReply = $this->$funName1($transId, $param, $_REQUEST['product_id'], $_REQUEST['vendor_id'], $_REQUEST['vendor_short']);
            } else
                $vendorReply = $this->$funName($transId, $param, $_REQUEST['product_id']);


            $try = false;
            if ($vendorReply['status'] == 'failure' && !in_array($vendorReply['code'], array(5, 6))) {
                if (isset($_REQUEST['key_vendor'])) {
                    $this->Shop->incrementMemcache($_REQUEST['key_vendor']);
                }
                $try = $this->transferToOtherRoutes($transId, $_REQUEST['vendor_short'], true);
            }
            if (!$try) {
                $vendorReply['operator_id'] = empty($vendorReply['operator_id']) ? "" : $vendorReply['operator_id'];
                $this->Shop->updateTransaction($transId, $vendorReply['tranId'], $vendorReply['status'], $vendorReply['code'], $vendorReply['description'], $vendorReply['pinRefNo'], $vendorReply['operator_id'], $_REQUEST['vendor_id'], $_REQUEST['product_id']);
            }

            $this->General->logData("/mnt/logs/b2cextender.txt", "i m above b2cextender condition $mobile " . $param['api_partner'] . " " . $vendorReply['status'] . " $try  " . $param['b2c_campaign_flag']);

            if (empty($param['api_partner']) && ($vendorReply['status'] != 'failure' || ($vendorReply['status'] == 'failure' && $try != false )) && !(in_array($_REQUEST['type'], array('wallet', 'cashpgPayment')))) {
                //$this->General->logData("/mnt/logs/b2cextender.txt","i m inside b2cextender condition $mobile");
                $req_data = array('trans_id' => $transId, 'area' => $circle, 'flag' => $service_id, 'product_id' => $_REQUEST['product_id'],
                    'mobile' => $mobile, 'param' => $par, 'amount' => $amount, 'timestamp' => date('Y-m-d H:i:s'), 'retailer_code' => $param['retailer_code'],
                    'retailer_name' => $param['retailer_name'], 'retailer_mobile' => $param['retailer_mobile'],
                    'b2c_campaign_flag' => $param['b2c_campaign_flag']);

                $this->B2cextender->manage_request_from_b2c_user($req_data);
            }
        }
      
	function ongoTopup($transaction_id, $params, $product_id){
		$filename = "wallets_integration_".date('Ymd').".txt";
		
		$this->General->logData('/mnt/logs/'.$filename, $label."::".json_encode(array($transaction_id, $params, $product_id)));
		
		App::import('Controller', 'Wallets');
		$obj = new WalletsController;
		$obj->constructClasses();
		$response = $obj->ongoTopup($params);
		
		$this->General->logData('/mnt/logs/'.$filename, $label."::".json_encode($response));
		
		$this->Shop->addStatus($transaction_id, $params['vendor_id']);
		if($response['status'] == 'success'){
			$this->General->log_in_vendor_message(
				array(
					$transaction_id, $response['vendor_refid'], '5', $params['vendor_id'], '13', 'Successful', 
					'success', date("Y-m-d H:i:s")
				)
			);
			return array('status' => 'success', 'code' => '31 ', 'description'=>$this->Shop->errors(31), 
					'tranId' => $params['vendor_refid'], 'pinRefNo' => '', 'operator_id' => '');
		}
		else if($response['status'] == 'failure'){
			$this->General->log_in_vendor_message(
				array(
					$transaction_id, '', '5', $params['vendor_id'], '14', $response['description'],
					'failure', date("Y-m-d H:i:s")
				)
			);
			return array('status' => 'failure', 'code' => '30', 'description' => $this->Shop->errors(30),
					'tranId' => '', 'pinRefNo' => '', 'operator_id'=>'');
		}
		else {
			$this->General->log_in_vendor_message(
				array($transId, '', '5', $params['vendor_id'], '15', '', 'pending', date("Y-m-d H:i:s"))
			);
			return array('status' => 'pending', 'code'=>'31', 'description' => $this->Shop->errors(31),
					'tranId' => '', 'pinRefNo' => '', 'operator_id' => '');
		}
	}
	
	function modemMobRecharge($transId,$params,$prodId,$vendor=4,$shortForm='modem'){
		$mobileNo = $params['mobileNumber'];

		$adm = "query=recharge&oprId=$prodId&mobile=$mobileNo&amount=".$params['amount']."&type=1&transId=$transId&circle=".$params['area'];
		try{
			$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
			if($Rec_Data['status'] == 'failure'){
				//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'36','".addslashes($Rec_Data['error'])."','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'36',addslashes($Rec_Data['error']),'failure',date("Y-m-d H:i:s")));
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                                $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                                
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}
			else {
				$Rec_Data = trim($Rec_Data['data']);
			}
		}catch(Exception $e){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'36','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'36','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'');
		}

		$product = $this->mapping['mobRecharge'][$params['operator']]['operator'];

		if(strpos($Rec_Data, 'Error') !== false){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'36','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'36','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm not responding";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
		
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "1"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'30','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'30','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Not able to add request in $product";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "2"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'40','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'40','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: No active sim/no balance in $product";
			$body = json_encode($params);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			//$this->General->sendMails($sub,$body,array('notifications@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "3"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'41','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'41','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Dropped due to lot of pending requests in $product";
			$body = json_encode($params);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
		
			//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com','chirutha@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_transactions(ref_id,vendor_id,status,date) values ('$transId',$vendor,0,'".date('Y-m-d')."')");
			
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$Rec_Data,'pinRefNo'=>'');
		}
	}

	function modemBillPayment($transId,$params,$prodId,$vendor=4,$shortForm='modem'){
		$mobileNo = $params['mobileNumber'];

		$adm = "query=recharge&oprId=$prodId&mobile=$mobileNo&amount=".$params['amount']."&type=1&transId=$transId&circle=".$params['area'];
		try{
			$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
			if($Rec_Data['status'] == 'failure'){
				//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'36','".addslashes($Rec_Data['error'])."','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'36',addslashes($Rec_Data['error']),'failure',date("Y-m-d H:i:s")));
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                                $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                                
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}
			else {
				$Rec_Data = $Rec_Data['data'];
			}
		}catch(Exception $e){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'36','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'36','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'');
		}

		$product = $this->mapping['billPayment'][$params['operator']]['operator'];

		if(strpos($Rec_Data, 'Error') !== false){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'36','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'36','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm not responding";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
		
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "1"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'30','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'30','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Not able to add request in $product";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
		
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "2"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'40','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'40','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: No active sim/no balance in $product";
			$body = json_encode($params);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			//$this->General->sendMails($sub,$body,array('notifications@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "3"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4,$vendor,'41','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'41','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Dropped due to lot of pending requests in $product";
			$body = json_encode($params);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com','chirutha@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','4',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_transactions(ref_id,vendor_id,status,date) values ('$transId',$vendor,0,'".date('Y-m-d')."')");
			
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$Rec_Data,'pinRefNo'=>'');
		}
	}

    
	function paytConnection($content){
		$domain = PAYT_URL;

		$data = $this->General->curl_post($domain,$content);
		if(!$data['success']){
			if($data['timeout']){
				$this->Shop->unHealthyVendor(5);
			}
		}
		else {
			$this->Shop->healthyVendor(5);
		}
		return $data;
	}

	function paytMobRecharge($transId,$params,$prodId = null){
		$mobileNo = $params['mobileNumber'];
		$type = 1;
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
			$type = 3;
		}
		$operator = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['payt'];

		$message="$operator|0|$mobileNo|".$params['amount']."|$type";
		$date = date('YmdHis');
		$terminal = PAYT_USER_CODE;
		$sha=strtoupper(sha1($terminal.$transId.$message.$date.PAYT_PASSWORD));
		$message = urlencode($message);
		$sha = urlencode($sha);
		$content = "OperationType=1&TerminalId=$terminal&TransactionId=$transId&DateTimeStamp=$date&Message=$message&Hash=$sha";

		$Rec_Data = $this->paytConnection($content);
		if(!$Rec_Data['success']){
			if($Rec_Data['timeout']){
				$this->Shop->addStatus($transId,5);
				$this->User->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1','5','14','Connectivity timeout from payt','failure','".date("Y-m-d H:i:s")."')");

				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$Rec_Data = $Rec_Data['output'];
		$response = explode("|",$Rec_Data);

                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/payt.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($response));
                
		if(count($response) == 6 && $transId == trim($response[1])){
			$ref_id = trim($response[2]);
			$status = trim($response[0]);
			$opr_id = trim($response[4]);
			$err_code = trim($response[3]);
			if($err_code == 'NA')$opr_id = "";
			if($opr_id == 'NA')$opr_id = "";
			$desc = trim($response[5]);

			if($status == '0' && $err_code != '8'){//success
				$this->Shop->addStatus($transId,5);
				$this->Shop->setMemcache("vendor5_last",date('Y-m-d H:i:s'),24*60*60);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','1','5','13','".addslashes($desc)."','success','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Success: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
					
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status == '1' || $err_code == '8'){//under process
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','1','5','15','".addslashes($desc)."','pending','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Pending: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
					
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status == '2'){//failure
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','1','5','14','".addslashes("Error code: $err_code, ". $desc)."','failure','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Failure: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
				$code = ($err_code == 11) ? 5 : (($err_code == 13) ? 6 : 30);
				$this->changeTataId($code,$prodId,$transId);

				return array('status'=>'failure','code'=>30,'description'=>$this->Shop->errors($code),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		/*else if(!empty($Rec_Data)){
			//$this->General->sendMails("Error in request: Paytronics",$content ."<br/>".$Rec_Data,array('ashish@mindsarray.com'));

			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','5','14','".addslashes($Rec_Data)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}*/
		else {
			//$this->General->sendMails("Empty response: Paytronics",$content ."<br/>".$Rec_Data,array('ashish@mindsarray.com'));

			$data = $this->paytTranStatus($transId);

			if($data['status'] == 'success'){
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','1','5','13','".addslashes($data['description'])."','success','".date("Y-m-d H:i:s")."')");
					
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}
			else if($data['status'] == 'pending'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','1','5','15','".addslashes($data['description'])."','pending','".date("Y-m-d H:i:s")."')");
					
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}
			else if($data['status'] == 'failure'){
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','1','5','14','".addslashes($data['description'])."','failure','".date("Y-m-d H:i:s")."')");

				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}
		}
	}


	function ppiMobRecharge($transId,$params,$prodId = null){
		$mobileNo = $params['mobileNumber'];
		$operator = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['pp'];
		//$soap = '<transactionRefId>'.$transId.'</transactionRefId><amount>'.$params['amount'].'</amount><mobileNumber>'.$mobileNo.'</mobileNumber><operatorCode>'.$operator.'</operatorCode>';
		$soap = '<transactionRefId>'.$transId.'</transactionRefId><amount>'.$params['amount'].'</amount><mobileNumber>'.$mobileNo.'</mobileNumber><operatorCode>'.$operator.'</operatorCode>';
		try{
			$processTran = $this->ppiApi('TelecomERecharge',$soap);
		}catch(Exception $e){
			$this->General->log_in_vendor_message(array($transId,'','1','2','10','','failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','2','10','','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(empty($processTran)){
			$this->General->log_in_vendor_message(array($transId,'','1','2','10','','failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','2','10','','failure','".date("Y-m-d H:i:s")."')");
			//$this->General->sendMails('PPI Mobile recharge: Blank Transaction status','tran id: '.$transId,array('chirutha@mindsarray.com','ashish@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(strtolower(trim($processTran['Response']['TransactionStatus'])) == '0'){//failed
		   $this->General->log_in_vendor_message(array($transId,$processTran['Response']['TransactionId'],'1','2','10',addslashes($processTran['Response']['Msg']),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['Response']['TransactionId']."','1','2','10','".addslashes($processTran['Response']['Msg'])."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>$processTran['Response']['TransactionId']);
		}else{
			$this->General->log_in_vendor_message(array($transId,$processTran['Response']['TransactionId'],'1','2','13',addslashes($processTran['Response']['Msg']),'success',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['Response']['TransactionId']."','1','2','13','".addslashes($processTran['Response']['Msg'])."','success','".date("Y-m-d H:i:s")."')");
			return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['Response']['TransactionId']);
		}

		$this->autoRender = false;
	}

	function ossMobRecharge($transId,$params,$prodId = null){
		//return array('status'=>'success','description'=>'success','tranId'=> rand(10000,100000));
		//get circle
		$mobileNo = $params['mobileNumber'];
		$opr = trim(substr($mobileNo,0,5));

		if(empty($params['circle'])){
			$circleQuery = $this->User->query("select mobile_numbering_area.oss_code 
					from mobile_operator_area_map 
					join mobile_numbering_area on (mobile_numbering.area = mobile_numbering_area.area_code) 
					where mobile_operator_area_map.number = '".$opr."'");
			$params['circle'] = $circleQuery['0']['mobile_numbering_area']['oss_code'];
			if(empty($params['circle'])){
				//$this->General->sendMails("Pay1: Telecom Circle Not found","Retailer: ".$_SESSION['Auth']['mobile']."<br/>Customer Number: ".$mobileNo."<br/>Operator: ".$this->mapping['mobRecharge'][$params['operator']]['opr_code'],array('ashish@mindsarray.com','dharmesh@mindsarray.com'));
			}
		}

		$circle = $params['circle'];
			
		//get recharge codes
		$operator = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['oss'];
		$rechCode = 0;
		$rechargeCode = '';
		$rechargesReq = '<GetRechargesRequest><CategoryCode>mr</CategoryCode><OperatorCode>'.$operator.'</OperatorCode><CircleCode>'.$circle.'</CircleCode></GetRechargesRequest>';
		try{
			$rechargesRes = $this->ossApi('GetRecharges',$rechargesReq);
		}catch(Exception $e){
			//system fail find recharges from db
			$dbRecCode = $this->User->query("SELECT rec_code FROM oss_rec_codes WHERE opr_code= '".$operator."' and (circle_code = '".$circle."' || circle_code = 'ali') and (denomination = '".$params['amount']."' || denomination = '0')");
			if(!empty($dbRecCode)){
				$rechargeCode = $dbRecCode['0']['oss_rec_codes']['rec_code'];
				$rechCode = 1;
			}

			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','11','','failure','".date("Y-m-d H:i:s")."')");
		}

		if(empty($rechargesRes) && $rechCode == '0'){
			//system fail find recharges from db
			$dbRecCode = $this->User->query("SELECT rec_code FROM oss_rec_codes WHERE opr_code= '".$operator."' and (circle_code = '".$circle."' || circle_code = 'ali') and (denomination = '".$params['amount']."' || denomination = '0')");
			if(!empty($dbRecCode)){
				$rechargeCode = $dbRecCode['0']['oss_rec_codes']['rec_code'];
				$rechCode = 1;
			}

			if($rechCode == '0'){ // no rec found in DB
				$sub = "OSS server not responding. Also no recharge code found in DB";
				$body = json_encode($params);
				//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com'));
				$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','36','','failure','".date("Y-m-d H:i:s")."')");
					
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}else if($rechCode == '1'){
				$sub = "OSS server not responding. Recharge code found in DB ";
				$body = "Request processed using Recharge code: ".$rechargeCode."</br></br>".json_encode($params);
				//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com'));
			}
		}
			
		if(strtolower($rechargesRes['GetRechargesResponse']['ResponseStatus']['Status']) != 'success' && $rechCode == '0'){
			$circle = 'ALI';
			$rechargesReq = '<GetRechargesRequest><CategoryCode>mr</CategoryCode><OperatorCode>'.$operator.'</OperatorCode><CircleCode>'.$circle.'</CircleCode></GetRechargesRequest>';
			$rechargesRes = $this->ossApi('GetRecharges',$rechargesReq);
			if(strtolower($rechargesRes['GetRechargesResponse']['ResponseStatus']['Status']) != 'success'){
				//system fail find recharges from db
				$circle = $params['circle'];

				$dbRecCode = $this->User->query("SELECT rec_code FROM oss_rec_codes WHERE opr_code= '".$operator."' and (circle_code = '".$circle."' || circle_code = 'ali') and (denomination = '".$params['amount']."' || denomination = '0')");
				if(!empty($dbRecCode)){
					$rechargeCode = $dbRecCode['0']['oss_rec_codes']['rec_code'];
					$rechCode = 1;
				}

				if($rechCode == '0'){ // no rec found in DB
					$errMsg = addslashes('Status: '.$rechargesRes['GetRechargesResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorDesc']);
					$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','22','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
					//$this->General->sendMails('OSS: Mobile recharge code not available nor found in DB.','Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator,array('ashish@mindsarray.com'));
					return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29),'tranId'=>'');
				}else if($rechCode == '1'){
					$errMsg = addslashes('Status: '.$rechargesRes['GetRechargesResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorDesc']);
					//$this->General->sendMails('OSS: Mobile recharge code not available. Found in DB.','Recharge Code: '.$rechargeCode.' Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator,array('ashish@mindsarray.com'));
				}
			}
		}

		$avail = '';

		if($rechCode == '0'){
			if(isset($rechargesRes['GetRechargesResponse']['RechargeDetails']['OperatorName'])){
				if((int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == 0  || (int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == $params['amount']){
					$rechargeCode = trim($rechargesRes['GetRechargesResponse']['RechargeDetails']['RechargeCode']);
					$rechCode = 1;
				}
			}else{
				foreach($rechargesRes['GetRechargesResponse']['RechargeDetails'] as $r){
					$avail .= $r['Denomination'].", ";
					if((int)$r['Denomination'] == 0 || (int)$r['Denomination'] == $params['amount']){
						$rechargeCode = trim($r['RechargeCode']);
						$rechCode = 1;
						break;
					}
				}
			}
		}

		if($rechCode == '0'){
			$errMsg = '';
			if(isset($rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'])){
				$errMsg .= addslashes('Status: '.$rechargesRes['GetRechargesResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorDesc']);
			}
			$errMsg .= 'Available recharges: '.$avail;

			//system fail find recharges from db
			$circle = $params['circle'];

			$dbRecCode = $this->User->query("SELECT rec_code FROM oss_rec_codes WHERE opr_code= '".$operator."' and (circle_code = '".$circle."' || circle_code = 'ali') and (denomination = '".$params['amount']."' || denomination = '0')");
			if(!empty($dbRecCode)){
				$rechargeCode = $dbRecCode['0']['oss_rec_codes']['rec_code'];
				$rechCode = 1;
			}
			if($rechCode == '0'){ // no rec found in DB
				$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','21','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails('OSS: Mobile Flexi recharge code not available nor found in DB.','Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator. '<br/>' . 'Available recharges: '.$avail);
				return array('status'=>'failure','code'=>'29','description'=>'Available recharges: Rs.'.$avail,'tranId'=>'');
			}else if($rechCode == '1'){
				//$this->General->sendMails('OSS: Mobile Flexi recharge code not available. Found in DB.','Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator,array('ashish@mindsarray.com'));
			}
		}
		//return array('status'=>'failure','description'=>$transId."==".$rechargeCode.json_encode($rechargesRes['GetRechargesResponse']['RechargeDetails']).'success','tranId'=> rand(10000,100000));
		//$this->General->sendMails('OSS recharge code testing.','recharge code: '.$rechargeCode.' Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator);
		$requestXML = '<ProcessTransactionRequest><MerchantRefNo>'.$transId.'</MerchantRefNo><RechargeCode>'.$rechargeCode.'</RechargeCode><Qty>1</Qty><CategoryCode>MR</CategoryCode><OperatorCode>'.$operator.'</OperatorCode><CircleCode>'.$circle.'</CircleCode><ConsumerNo>'.$mobileNo.'</ConsumerNo><CustMobileNo>'.$mobileNo.'</CustMobileNo><Denomination>'.$params['amount'].'</Denomination><PNRNo></PNRNo><PGCode>'.OSS_PG_CODE.'</PGCode><DistCustCode>'.OSS_DIST_CUST_CODE.'</DistCustCode><CardNo></CardNo><TranDistCustCode>'.OSS_TRAN_DIST_CUST_CODE.'</TranDistCustCode><TranPassword>'.OSS_TRAN_PASSWORD.'</TranPassword></ProcessTransactionRequest>';
		try{
			$processTran = $this->ossApi('ProcessTransaction',$requestXML);
		}catch(Exception $e){
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','12','','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		//return array('status'=>'failure','description'=>$processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorCode'],'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
		/*if(empty($processTran)){
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1','1','36','','failure','".date("Y-m-d H:i:s")."')");
			$sub = "OSS server not responding";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('tadka@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}*/

		if(trim(strtolower($processTran['ProcessTransactionResponse']['ResponseStatus']['Status'])) == ''){
			//$this->General->sendMails('OSS mobile recharge: Blank Transaction status','tran id: '.$transId,array('chirutha@mindsarray.com'));
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','1','1','15','','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
		}else if(strtolower($processTran['ProcessTransactionResponse']['ResponseStatus']['Status']) != 'success'){
			$errMsg = addslashes('Status: '.$processTran['ProcessTransactionResponse']['ResponseStatus']['Status'].' ErrorCode:'.$processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorDesc']);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','1','1','12','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
			if(trim($processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorCode']) == '67653'){
				return array('status'=>'failure','code'=>'38','description'=>$this->Shop->errors(38),'tranId'=>'');
			}
			else if(trim($processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorCode']) == '67654'){
				return array('status'=>'failure','code'=>'37','description'=>$this->Shop->errors(37),'tranId'=>'');
			}
			else{
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'');
			}
		}else{
			if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '2'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','1','1','15','','pending','".date("Y-m-d H:i:s")."')");
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}else if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '1'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','1','1','14','','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}else if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '0'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','1','1','13','','success','".date("Y-m-d H:i:s")."')");
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}
		}

		$this->autoRender = false;
	}

	function cpConnect($url,$param){
		$out = $this->General->curl_post($url,$param);
		/*if(!$out['success']){
			$this->Shop->unHealthyVendor(8);
			}
			else {
			$this->Shop->healthyVendor(8);
			}*/
		return $out;
	}

	function cpMobRecharge($transId,$params,$prodId,$recheck=false,$attempt=0){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;
                //$prodId = ($prodId == '27' && $recheck === false) ? 9 : $prodId;
                
		$extra = "ACCOUNT=\r\n";
		if(in_array($prodId,array('27','28','29','31','34'))){
			$extra="ACCOUNT=2\r\n";
		}
		else if(in_array($prodId,array('12'))){
			$extra="ACCOUNT=1\r\n";
		}
		
		if($prodId == '2'){
			$store_data = $this->Slaves->query("SELECT store_code FROM cp_retailers WHERE service_id='Prepaid' AND active_flag = 1 ORDER BY RAND() LIMIT 1");
			if(!empty($store_data)){
				$extra.="TERM_ID=".trim($store_data[0]['cp_retailers']['store_code'])."\r\n";
			}
			else {
				return array('status'=>'failure','code'=>'25','description'=>$this->Shop->errors(25),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		
		$mobileNo = $params['mobileNumber'];
		$amount = $params['amount'];

		$verify_url = $this->cpurls[$prodId]['cpv'];
		$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$mobileNo\r\n".$extra."AMOUNT=".floatval($amount)."\r\nAMOUNT_ALL=$amount\r\nCOMMENT=\r\n";
		
		
		
		$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
		$out = $this->cpConnect($verify_url,array('inputmessage'=>$res[1]));
                $cp_encode_input_data = $res;
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,8);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
		$out = $out['output'];

		$res = ipriv_verify($out, $pub);
		$result = $this->cpArray($res);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Mob Recharge - Step1*: $verify_url, Input=> ".$data."\n".json_encode($result)." | out : ".  json_encode($out)." | cp_encode_input_data:".json_encode($cp_encode_input_data));
                if(isset($cp_encode_input_data[0]) && $cp_encode_input_data[0] == "-9"){
                    $this->General->logData("cp_null_log.txt"," |<$transId>|".$sec."||");
                }
		if(empty($result) && intval($attempt) < 1){
                    $attempt = intval($attempt) + 1;
                    return $this->cpMobRecharge($transId,$params,$prodId,$recheck,$attempt);
                }
                
		if(isset($result['RESULT']) && $result['RESULT'] == 0 && empty($result['ERROR'])){
			if(isset($result['AUTHCODE']))$operator_id = $result['AUTHCODE'];

			$live_url = $this->cpurls[$prodId]['cpl'];
			$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$mobileNo\r\n".$extra."AMOUNT=".floatval($amount)."\r\nPAY_TOOL=0\r\n";
			$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
				
			$out = $this->cpConnect($live_url,array('inputmessage'=>$res[1]));
			
			
			if(!$out['success']){
				if($out['timeout']){
					$this->Shop->addStatus($transId,8);
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'','1',8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
					return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
				else {
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',8,'15','Request timed out while recharging','pending','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'','1',8,'15','Request timed out while recharging','pending',date("Y-m-d H:i:s")));
					return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
			}
			else {
				$out = $out['output'];
			}

			$res = ipriv_verify($out, $pub);
			$result = $this->cpArray($res);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Mob Recharge - Step2*: $live_url, Input=> ".$data."\n".json_encode($result));
		
			$vendorTransId = (isset($result['TRANSID'])) ? $result['TRANSID'] : 0;
			$opr_id = (isset($result['AUTHCODE'])) ? $result['AUTHCODE'] : 0;
			$trans_status = (isset($result['TRNXSTATUS'])) ? $result['TRNXSTATUS'] : 3;

			if($trans_status != 7){
				$status = $this->cpTranStatus($transId,null,null,$prodId);
			}
			else {
				$status['status'] = 'success';
				$status['transaction_id'] = $vendorTransId;
			}
			
			$error = isset($status['errCode']) ? $status['errCode'] : $result['ERROR'];
			$errCode = $this->Shop->errorCodeMapping(8,$error);

			if($status['status'] == 'error' || $status['status'] == 'inprocess' || $status['status'] == 'incomplete'){//in process
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','1',8,'15','".$status['status']."::".$status['errMsg']."(".$status['errCode'].")','pending','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'1',8,'15',$status['status']."::".$status['errMsg']."(".$status['errCode'].")",'pending',date("Y-m-d H:i:s")));
				return array('status'=>'pending','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,8);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','1',8,'14','".$status['errMsg']."(".$status['errCode'].")','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'1',8,'14',$status['errMsg']."(".$status['errCode'].")",'failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,8);
				$this->Shop->setMemcache("vendor8_last",date('Y-m-d H:i:s'),24*60*60);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','1',8,'13','','success','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'1',8,'13','','success',date("Y-m-d H:i:s")));
				return array('status'=>'success','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		else {
			
			$error = $result['ERROR'];
			
			$errCode = $this->Shop->errorCodeMapping(8,$error) ;
			
			$this->Shop->addStatus($transId,8);
			//$code = (!empty($result['ERROR']) && $result['ERROR'] == 23) ? 5 : ((!empty($result['ERROR']) && $result['ERROR'] == 7) ? 6 : 42);
			$tatachangeID = $this->changeTataId($errCode,$prodId,$transId);
//                        if($tatachangeID !== false && $recheck === false ){
//                            return $this->cpMobRecharge($transId,$params,$tatachangeID,true);
//                        }
//                        else{
                            $err = "Error code: $error," . $this->cp_errs[$error];
                            $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Mob Recharge - Authorization error *: txnid = $transId |code = $error ");		
                            //$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',8,'42','".addslashes($err)."','failure','".date("Y-m-d H:i:s")."')");                            
                            $this->General->log_in_vendor_message(array($transId,'','1',8,'42',addslashes($err),'failure',date("Y-m-d H:i:s")));
                            return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>'');
//                        }
                        }
		}

	function changeTataId($code,$prodId,$transId){
		if( $code == 6 && in_array($prodId,array(9,10,27))){
			if($prodId == 9 || $prodId == 10)$prodId = 27;
			else $prodId = 9;
			$this->User->query("UPDATE vendors_activations SET product_id = $prodId WHERE ref_code='".$transId."'");
				
			$mdata = $this->Shop->getMemcache("txn$transId");
                        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/changetata.txt"," before change txnid = $transId |code = $code | mdata =".json_encode($mdata));		
			if($mdata !== false){
				$mdata['product_id'] = $prodId;
				$this->Shop->setMemcache("txn$transId",$mdata,5*60);
                                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/changetata.txt"," after change txnid = $transId |code = $code | mdata =".json_encode($mdata));		
                                return $prodId;
			}
		}
                return false;
	}

	function cbzMobRecharge($transId,$params,$prodId){
		$special = 'tp';
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
			$special = 'rc';
		}

		$mobileNo = $params['mobileNumber'];
		$amount = $params['amount'];

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['cbz'];
		$vendor = 11;

		$mins = array('2'=>50,'4'=>50,'15'=>50);

		if(isset($mins[$prodId]) && $amount < $mins[$prodId]){
			$this->Shop->addStatus($transId,$vendor);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'33','Min Recharge should be ".$mins[$prodId]."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'33','description'=>$this->Shop->errors(33),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
		
                $url = CBZ_RECHARGE_URL;
		//$out = $this->General->cbzApi($url,array('username'=>'RT59524449','password'=>'123456','key'=>'cell1pay1825money','provider'=>$provider,'no'=>$mobileNo,'amount'=>$amount,'rechargeType'=>$special,'circle'=>'','trans_id'=>$transId));
                $out = $this->General->cbzApi($url,array('username'=>CBZ_REC_USERNAME,'password'=>CBZ_REC_PASSWORD,'key'=>CBZ_REC_KEY,'provider'=>$provider,'no'=>$mobileNo,'amount'=>$amount,'rechargeType'=>$special,'circle'=>'','trans_id'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cbz.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		$res = $this->General->xml2array($out);

		if($res['root']['result'] == "fail"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','1',$vendor,'30','".$res['root']['message']."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'30',$res['root']['message'],'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($res['root']['result'] == "received" && $res['root']['req_id'] == $transId){
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$res['root']['tr_id']."','1',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,$res['root']['tr_id'],'1',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$res['root']['tr_id'],'pinRefNo'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'');
		}
	}

	function rduMobRecharge($transId,$params,$prodId){
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}

		$mobileNo = $params['mobileNumber'];
		$amount = $params['amount'];

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['rdu'];
		$vendor = 16;
		
                $url = RDU_RECHARGE_URL;
		//$out = $this->General->rduApi($url,array('userId'=>'MATPL','pwd'=>'nu@k06','rechargeServiceCode'=>'Mobile','memberCode'=>'EA10P421','operatorCode'=>$provider,'mobileNo'=>$mobileNo,'amount'=>$amount,'clientTrnId'=>$transId));
                $out = $this->General->rduApi($url,array('userId'=>RDU_REC_USERNAME,'pwd'=>RDU_REC_PASSWORD,'rechargeServiceCode'=>'Mobile','memberCode'=>'EA10P421','operatorCode'=>$provider,'mobileNo'=>$mobileNo,'amount'=>$amount,'clientTrnId'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rdu.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		$res = explode(":",$out);
		$code = trim($res['0']);
		$msg = trim($res['2']);

		/*if(in_array($code,array(0,2))){
			$this->Shop->setMemcache("vendor16_last",date('Y-m-d H:i:s'),24*60*60);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','1',$vendor,'13','".addslashes($msg)."','success','".date("Y-m-d H:i:s")."')");
				
			return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>trim($res['3']),'pinRefNo'=>'');
			}
			else*/ if(in_array($code,array(0,2,528))){
				$this->General->log_in_vendor_message(array($transId,trim($res['3']),'1',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
		//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','1',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
		return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>trim($res['3']),'pinRefNo'=>'');
			}
			else {
				$this->General->log_in_vendor_message(array($transId,trim($res['3']),'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
				//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>trim($res['3']));
			}
	}

	function uvaMobRecharge($transId,$params,$prodId){
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
			$type = "STV";
		}
		else {
			$type = "ETOP";
		}

		$mobileNo = $params['mobileNumber'];
		$amount = $params['amount'];

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['uva'];
		$vendor = 18;
		$uniqueid = UVA_REC_UNIQID;
		$code = md5($uniqueid.$mobileNo.$amount.$transId);
		
		//$out = $this->General->uvaApi($url,array('uniqueid'=>$uniqueid,'amount'=>$amount,'mobile'=>$mobileNo,'type'=>$type,'prodcode'=>$provider,'refno'=>$transId,'code'=>$code));
                $url = UVA_RECHARGE_URL;
                $out = $this->General->uvaApi($url,array('uniqueid'=>$uniqueid,'amount'=>$amount,'mobile'=>$mobileNo,'type'=>$type,'prodcode'=>$provider,'refno'=>$transId,'code'=>$code));
                
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uva.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		///echo $out;
		$res = explode("|",$out);
		$code = trim($res['0']);
		$exp = explode("-",trim($res['1']));
		$msg = trim($exp['0']);
		$txnId = trim($exp['1']);


		if(in_array($code,array(0,1))){
			//$this->Shop->setMemcache("vendor18_last",date('Y-m-d H:i:s'),24*60*60);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','1',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId);
		}
	}

	function uniMobRecharge($transId,$params,$prodId){
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['uni'];
		$vendor = 19;

                $url = UNI_RECHARGE_URL;
		$out = $this->General->uniApi($url,array('rcm'=>$mobileNo,'rca'=>$amount,'crqid'=>$transId,'cro'=>$provider));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uni.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		/////0|Transaction Successful | 12121212121 | 121139144523204238;
		$res = explode("|",$out);

		if(count($res) == 2 || trim($res[0]) == '405'){
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$code = trim($res['0']);
			$msg = trim($res['1']);
			$txnId = trim($res['3']);
			$this->General->log_in_vendor_message(array($transId,trim($txnId),'1',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','1',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}


	function anandMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		if(in_array($prodId,array('27','28','29','31','34'))){
			$type = "stv";
		}
		else {
			$type = "Rr";
		}
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['anand'];
		$vendor = 9;

                $url = ANAND_RECHARGE_URL;
		$out = $this->General->anandApi($url,array('Mob'=>ANAND_MOB,'message'=>"$type $provider $mobileNo $amount ".ANAND_PIN,'myTxId'=>$transId,'source'=>'API'));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/anand.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		/////0|Transaction Successful | 12121212121 | 121139144523204238;
		$res = explode(",",$out);
		$msg = trim($res[0]);

		$txnId = "";
		if(isset($res[2])){
			$txnId = trim($res[2]);
			$txnId = explode(":",$txnId);
			$txnId = trim($txnId[1]);
		}

		if(in_array(strtolower($msg),array('your request have been successfull','your request have been processed'))){
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($msg)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($msg),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}

	function apnaMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['apna'];
		$vendor = 23;

                $url = APNA_RECHARGE_URL;
		$out = $this->General->apnaApi($url,array('username'=>APNA_USERNAME,'pwd'=>APNA_PASSWD,'circlecode'=>12,'operatorcode'=>$provider,'number'=>$mobileNo,'amount'=>$amount,'client_id'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                               $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/apna.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$res = explode("#",$out);
		$status = strtolower(trim($res[1]));
		$txnId = trim($res[0]);

		if(in_array($status,array("pending","success"))){
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}
	 
	function magicMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
                
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['magic'];
		$vendor = 24;

                $url = MAGIC_RECHARGE_URL;
		$out = $this->General->magicApi($url,array('uid'=>MAGIC_USERNAME,'pwd'=>MAGIC_PASSWD,'rcode'=>$provider,'mobileno'=>$mobileNo,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/magic.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}
	
	function rioMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['rio'];
		$vendor = 36;      
        
		$url = RIO_RECHARGE_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO_USERNAME,'pwd'=>RIO_PASSWD,'rcode'=>$provider,'mobileno'=>$mobileNo,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}

    function rio2MobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['rio2'];
		$vendor = 62;      
        
		$url = RIO2_RECHARGE_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO2_USERNAME,'pwd'=>RIO2_PASSWD,'rcode'=>$provider,'mobileno'=>$mobileNo,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio2.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}
    
	function infogemMobRecharge($transId,$params,$prodId = null){
		$mobileNo = $params['mobileNumber'];
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
		$operator = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['gem'];

		$message="$operator|$mobileNo|".$params['amount'];
		$vendor = 27;
		$terminal = GEM_USERNAME;
		$sha=strtoupper(sha1($terminal.$transId.$message.GEM_PASSWORD));

		$url = GEM_RECHARGE_URL;
		$out = $this->General->gemApi($url,array('PartnerId'=>$terminal,'TransId'=>$transId,'Message'=>$message,'Hash'=>$sha));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gem.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		$Rec_Data = $out['output'];
		$response = explode("|",$Rec_Data);
		$status = trim($response[0]);
		$txnId = trim($response[2]);
		$opr_id = trim($response[3]);
		$desc = trim($response[4]);

		if($opr_id == 'NA')$opr_id = "";

		$status_array = array('100'=>'Transaction Successful','99'=>'Recharge Failed','101'=>'Invalid Login','102'=>'Insufficient Balance','103'=>'Invalid Amount','104'=>'Invalid Trans ID','105'=>'Trans ID already exists','106'=>'Service Unavailable for user','107'=>'Invalid phone Number','110'=>'Invalid Transaction amount','111'=>'Daily Limit reached','121'=>'Account Blocked','123'=>'Technical Failure','165'=>'Response waiting','170'=>'Wrong Requested Ip','171'=>'Repeated Request','173'=>'Operator temporarly not available','172'=>'Invalid request','174'=>'Hash Value MisMatch');
		if(empty($desc))$desc = $status_array[$status];
		if(empty($desc))$desc = $Rec_Data;

		if(empty($status) || $status == '165'){//pending
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($desc)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($desc),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'operator_id'=>$opr_id);
		}
		else if($status == '100'){//success
			$this->Shop->addStatus($transId,$vendor);
			$this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,staresponsetus,timestamp) values ('".$transId."','$txnId','1',$vendor,'13','".addslashes($desc)."','success','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'13',addslashes($desc),'success',date("Y-m-d H:i:s")));
			return array('status'=>'success','code'=>'13','description'=>$this->Shop->errors(13),'tranId'=>$txnId,'operator_id'=>$opr_id);
		}
		else {//failure
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($desc)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($desc),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId,'operator_id'=>$opr_id);
		}
	}

	function durgaMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['durga'];
		$vendor = 30;

		$rt_array = array('2'=>'1','15'=>'2','4'=>'3');

		$url = DURGA_RECHARGE_URL;
		$out = $this->General->durgaApi($url,array('mobile'=>$mobileNo,'amt'=>$amount,'ctxnid'=>$transId,'operator'=>$provider,'type'=>'Recharge','cid'=>10,'rt'=>$rt_array[$prodId]));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/durga.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		/////0|Transaction Successful | 12121212121 | 121139144523204238;
		$res = explode("|",$out);

		if(count($res) == 2 || trim($res[0]) == '405'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$code = trim($res['0']);
			$msg = trim($res['1']);
			$txnId = trim($res['3']);
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','1',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,trim($txnId),'1',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	
	function rkitMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);                

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['rkit'];
		$vendor = 34;
		$rcType = 'NORMAL';
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rcType = 'SPECIAL';
		}

                $url = RKIT_RECHARGE_URL;
                $out = $this->General->rkitApi($url,array('USERID'=>RKIT_USER,'PASSWORD'=>RKIT_AUTH,'SUBSCRIBER'=>$mobileNo,'AMOUNT'=>$amount,
                'TRANNO'=>$transId,'RECTYPE'=>$rcType,'OPERATOR'=>$provider));
        
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rkit.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		//1. SUCCESS# RECHARGE POSTED # TRANREFNO #98888
		//3. ERROR# INVALID MOBILE ENTRY #
        //TRANNO:9836-STATUS:SUCCESS
        //ERRCODE:104-OPERATOR IS NOT AVAILABLE
        //$out1 = explode(":",$out);
        $txnId = "";
		
		
		if($out['NODE']['STATUS'] == 'FAILED'){
            $description = trim($out1['NODE']['STATUS']);
			$this->Shop->addStatus($transId,$vendor);
			$txnId = $out['NODE']['TXNID'];
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
            $txnId = $out['NODE']['TXNID'];
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	
	
	function a2zMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['a2z'];
		$vendor = 47;
		$rcType = 'NORMAL';
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rcType = 'SPECIAL';
		}
		
//		$transId = 321456378262;
//		
//		$provider = 2;
//		
//		$amount = 10;
//		
//		$mobileNo = 9975629244;
	
		$url = A2Z_RECHARGE_URL;
		
		$params = array('USERID'=>A2Z_AGTCODE,'PASSWORD'=>A2Z_AUTH,'OPERATOR'=>$provider,'AMOUNT'=>$amount,'SUBSCRIBER'=>$mobileNo,'TRANNO'=>$transId,'RECTYPE'=>$rcType);
		
		$out = $this->General->a2zApi($url,$params);
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);                                 
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) "
                                //        . "values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $this->General->xml2array("<NODE>".$out['output']."</NODE>");
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/a2z.txt","*Mob Recharge*: Input=> ".json_encode($params)."<br/>".json_encode($out));

		//1. SUCCESS# RECHARGE POSTED # TRANREFNO #98888
		//3. ERROR# INVALID MOBILE ENTRY #
		
		
		if($out['NODE']['STATUS'] == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$out['NODE']['TXNID']."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$out['NODE']['TXNID'],'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$out['NODE']['TXNID']."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$out['NODE']['TXNID'],'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	
	
	function joinrecMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['joinrec'];
		$vendor = 48;
		$rcType = '';
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rcType = 'special';
		}

        /*---only related to joinrecharge airtel */
        if($prodId == 2 && $amount < 50){
            return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
        }
        
		$url = JOINREC_RECHARGE_URL;
		$out = $this->General->joinrecApi($url,array('reseller_id'=>JOINREC_ID,'reseller_pass'=>JOINREC_PWD,'mobilenumber'=>$mobileNo,'denomination'=>$amount,'meroid'=>$transId,'voucher'=>$rcType,'operatorid'=>$provider,'circleid'=>'*'));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['Data']['Error'])){
			$status = 'FAILED';
			$description = trim($out['Data']['Error']);
		}
		else {
			$status = trim($out['Data']['Status']);
			$description = trim($out['Data']['Description']);
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['Data']['OrderId']);
		}
		
		
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($description)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($description),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	
    function mypayMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['mypay'];
		$vendor = 57;
		$rcType = '0';
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rcType = '1';
		}
		
		$url = MYPAYURL;
        $pwd = MYPAYPASSWD;
        $dt_time = date('m.d.Y H:i:s');
        
        //pwd|mob|amt|opr|type|datetime
        $req_str = $pwd."|".$mobileNo."|".$amount."|".$provider."|".$rcType."|".$dt_time."|".$transId;
        
		$out = $this->General->mypayApi($url,array('_prcsr'=>MYPAYUSER,'_urlenc'=>$req_str,'_encuse'=>0));
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/mypay.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['_ApiResponse']['statusCode']) && strtolower($out['_ApiResponse']['statusCode']) != "10008"){
			$status = 'FAILED';
			$description = trim($out['_ApiResponse']['statusDescription']);
		}
		else {
			$status = trim($out['_ApiResponse']['statusCode']);
			$description = isset($out['_ApiResponse']['statusDescription'])?trim($out['_ApiResponse']['statusDescription']):"NA";
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['_ApiResponse']['requestID']);
		}
		
		
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($description)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	
    /*
     * smsdaak trans status check
     */
    function smsdaakTranStatus($transId,$date=null,$refId=null){
		$url = SMSDAAK_TRANS_URL;
        
		$request_param = array('token'=>SMSDAAK_TOKEN,'agentid'=>$transId,'format'=>'xml');
        
		$out = $this->General->smsdaakApi($url,$request_param);
	
		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt",date('Y-m-d H:i:s').":smsdaakTranStatus: ".json_encode($out));

		$status = isset($out['xml']['status'])?trim($out['xml']['status']):"";
		$operator_id = isset($out['xml']['opr_id'])?trim($out['xml']['opr_id']):"";
		$vendor_id = isset($out['xml']['ipay_id'])?trim($out['xml']['ipay_id']):"";
		
		if(isset($out['xml']['ipay_errorcode'])){
			$description = trim($out['xml']['ipay_errordesc']);
            if(!in_array($out['xml']['ipay_errorcode'],array("TXN","TUP"))){
                $status = "FAILED";
            }            
		}
		else $description = trim($out['xml']['res_msg']);
		
		if($status == 'SUCCESS'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if(in_array($status,array('FAILED','REFUND'))){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
    
    /*
     * smsdaak prepaid mobile recharges
     */
    function smsdaakMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
        
                if(in_array($prodId,array('27','28','29','31','34'))){
                                $params['type'] = 'voucher';
		}
        
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['smsdaak'];
		$vendor = 58;
				
		$url = SMSDAAK_RECHARGE_URL;
        //--- request parameter
        $request_param = $validation_param = array('token'=>SMSDAAK_TOKEN,'spkey'=>$provider,'agentid'=>$transId,'account'=>$mobileNo,'amount'=>$amount,'format'=>'xml');
        
        //---validating request        
        $validation_param['mode'] = 'VALIDATE';
		$out = $this->General->smsdaakApi($url,$validation_param);
        $service_id = '1';
        
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Mob Recharge validation request *: Input=> $transId<br/>".json_encode($out)." : input : ".json_encode($validation_param));
        
        //-----------handle success / failure of validation hit
        if(strtoupper($out['xml']['ipay_errorcode']) == 'TXN'){
             $out = "";
             $out = $this->General->smsdaakApi($url,$request_param);
             if(!$out['success']){
                if($out['timeout']){
                    $this->Shop->addStatus($transId,$vendor);
                    $this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                  //  $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
                }
             }
            $out = $out['output'];
            $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out)." : input : ".json_encode($request_param));
            
            $txnId = isset($out['xml']['ipay_id']) ? $out['xml']['ipay_id'] : "";
            $opr_id = isset($out['xml']['status'])?$out['xml']['opr_id']:"";
            if(isset($out['xml']['status'])){
                $error = !empty($out['xml']['res_code']) ? $out['xml']['res_code'] : "" ;
                $errCode = $this->Shop->errorCodeMapping($vendor,$error);
                if(strtoupper(trim($out['xml']['status'])) == "SUCCESS"){
                    $this->Shop->addStatus($transId,$vendor);
                    $description = trim($out['xml']['res_msg']);
                    
                    if(empty($description))$description = 'success';
                    $this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
                    $this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
                    //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'success','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
                }else{
                    //------considered non - success status as pending
		    $this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
                   // $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'pending','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'');
                }                
            }            
        }
        
        //----------- handle failure of validation and payment hit
        if(isset($out['xml']['ipay_errorcode']) && strtoupper($out['xml']['ipay_errorcode']) != 'TXN'){
                $this->Shop->addStatus($transId,$vendor);
                $description = trim($out['xml']['ipay_errordesc']);
                $error = !empty($out['xml']['ipay_errorcode']) ? $out['xml']['ipay_errorcode'] : "" ;
		$errCode = $this->Shop->errorCodeMapping($vendor,$error);
                $txnId = "";
                $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
                //$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
                return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId);
        }
        
	}

    /*
     * smsdaak dth recharge
     */
    function smsdaakDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['smsdaak'];
		$vendor = 58;
				
		$url = SMSDAAK_RECHARGE_URL;
        //--- request parameter
        $request_param = $validation_param = array('token'=>SMSDAAK_TOKEN,'spkey'=>$provider,'agentid'=>$transId,'account'=>$mobileNo,'amount'=>$amount,'format'=>'xml');
        
        //---validating request        
        $validation_param['mode'] = 'VALIDATE';
		$out = $this->General->smsdaakApi($url,$validation_param);
        $service_id = '2';
        
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Dth Recharge validation request *: Input=> $transId<br/>".json_encode($out));
        
        //-----------handle success / failure of validation hit
        if(strtoupper($out['xml']['ipay_errorcode']) == 'TXN'){
             $out = "";
             $out = $this->General->smsdaakApi($url,$request_param);
             if(!$out['success']){
                if($out['timeout']){
                    $this->Shop->addStatus($transId,$vendor);
					$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                   // $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
                }
             }
            $out = $out['output'];
            $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Dth Recharge*: Input=> $transId<br/>".json_encode($out));
            $opr_id = isset($out['xml']['status'])?$out['xml']['opr_id']:"";
            $txnId = isset($out['xml']['ipay_id'])? $out['xml']['ipay_id'] : "";
            if(isset($out['xml']['status'])){
		      	$error = !empty($out['xml']['res_code']) ? $out['xml']['res_code'] : "" ;
			    $errCode = $this->Shop->errorCodeMapping($vendor,$error);
                if(strtoupper(trim($out['xml']['status'])) == "SUCCESS"){
                    $this->Shop->addStatus($transId,$vendor);
                    $description = trim($out['xml']['res_msg']);
                    if(empty($description))$description = 'success';
                    $this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
    		    $this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
                    //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'success','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
                }else{
                    //------considered non - success status as pending
                    $this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
		    //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'pending','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'');
                }                
            }            
        }
        
        //----------- handle failure of validation and payment hit
        if(isset($out['xml']['ipay_errorcode']) && strtoupper($out['xml']['ipay_errorcode']) != 'TXN'){
            $this->Shop->addStatus($transId,$vendor);
			$description = trim($out['xml']['ipay_errordesc']);
			$txnId = "";
			$error = !empty($out['xml']['ipay_errorcode']) ? $out['xml']['ipay_errorcode'] : "" ;
			$errCode = $this->Shop->errorCodeMapping($vendor,$error);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId);
        }
        
	}
    
    /*
     * smsdaak Bill Payment
     */
    function smsdaakBillPayment($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['billPayment'][$params['operator']][$params['type']]['smsdaak'];
		$vendor = 58;
				
		$url = SMSDAAK_RECHARGE_URL;
        //--- request parameter
        $request_param = $validation_param = array('token'=>SMSDAAK_TOKEN,'spkey'=>$provider,'agentid'=>$transId,'account'=>$mobileNo,'amount'=>$amount,'format'=>'xml');
        
        //---validating request        
        $validation_param['mode'] = 'VALIDATE';
		$out = $this->General->smsdaakApi($url,$validation_param);
        $service_id = '4';
        
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*BillPayment validation request *: Input=> $transId<br/>".json_encode($out));
        
        //-----------handle success / failure of validation hit
        if(strtoupper($out['xml']['ipay_errorcode']) == 'TXN'){
             $out = "";
             $out = $this->General->smsdaakApi($url,$request_param);
             if(!$out['success']){
                if($out['timeout']){
                    $this->Shop->addStatus($transId,$vendor);
					$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                    //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
                }
             }
            $out = $out['output'];
            $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","* BillPayment *: Input=> $transId<br/>".json_encode($out));
            
            $txnId = isset($out['xml']['ipay_id']) ? $out['xml']['ipay_id'] : "";
            $opr_id = isset($out['xml']['status'])?$out['xml']['opr_id']:"";
            
            if(isset($out['xml']['status'])){
				$error = !empty($out['xml']['res_code']) ? $out['xml']['res_code'] : "" ;
				$errCode = $this->Shop->errorCodeMapping($vendor,$error);
                if(strtoupper(trim($out['xml']['status'])) == "SUCCESS"){
                    $this->Shop->addStatus($transId,$vendor);
                    $description = trim($out['xml']['res_msg']);

                    if(empty($description))$description = 'success';
                    $this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
					$this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
                   // $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'success','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
                }else{
                    //------considered non - success status as pending
                   // $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                    $this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
					return array('status'=>'pending','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'');
                }                
            }            
        }
        
        //----------- handle failure of validation and payment hit
        if(isset($out['xml']['ipay_errorcode']) && strtoupper($out['xml']['ipay_errorcode']) != 'TXN'){
            $this->Shop->addStatus($transId,$vendor);
			$description = trim($out['xml']['ipay_errordesc']);
			$error = !empty($out['xml']['ipay_errorcode']) ? $out['xml']['ipay_errorcode'] : "" ;
			$errCode = $this->Shop->errorCodeMapping($vendor,$error);
			$txnId = "";
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId);
        }        
	}
    
    /*
     * smsdaak check balance
     */
    function smsdaakBalance($panel = null,$min = null, $power = null){
		$vendor_id = 58;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
            $url = SMSDAAK_BAL_URL;
            $request_param = array('token'=>SMSDAAK_TOKEN,'format'=>'xml');
            
			$out = $this->General->smsdaakApi($url,$request_param);
		
            $out = $out['output'];
            
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt",date('Y-m-d H:i:s').":Balance Check: ".json_encode($out));
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		
		$bal = $out['xml']['Wallet'];
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("smsdaak Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                            $this->sendApibalanceLowAlert(array('apiname'=>'Smsdaak','min'=>$min,'currentbalance'=>$bal));  
			}
			$this->autoRender = false;
		}
		else {
			$ret = array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			
			if($min == -1){
				echo json_encode($ret);
				$this->autoRender = false;
			}
			else return $ret;
		}
	}
    /*
     * hitechrec
     */
    function hitechBalance($panel = null,$min = null, $power = null){
		$vendor_id = 65;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
        
		if($out === false || $power == 1){
            
            $url = HITECH_BAL_URL;            
            $request_param = array('uid'=>HITECH_USERID,'pass'=>HITECH_PASSWORD,'mno'=>HITECH_MNO,'msg'=>'cb');
            
			$out = $this->General->hitechrecApi($url,$request_param);
            $out_arr = $out['output'];
            $messag_arr = explode(" ",$out_arr['NODES']['MESSAGE']);
			
            $out = explode(':',$messag_arr[2]);
			
			$out = !empty($out[1]) ? $out[1] : ""; 
			
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/hitech.txt",date('Y-m-d H:i:s').":Balance Check: ".json_encode($out));
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		
		$bal = $out;
        
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("hitechrec Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			$this->autoRender = false;
		}
		else {
			$ret = array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			
			if($min == -1){
				echo json_encode($ret);
				$this->autoRender = false;
			}
			else return $ret;
		}
	}

    /*
     * hitech
     */
    function hitechMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
    	        
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['hitecrec'];
		$vendor = 65;
				
		$url = HITECH_RECHARGE_URL;
        //--- request parameter
        $request_param = array('uid'=>'1169','pass'=>'12345','mno'=>$mobileNo,'op'=>$provider,'amt'=>$amount,'Refid'=>$transId);
        
        $out = $this->General->hitechrecApi($url,$request_param);
        

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/hitech.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['NODES']) && strtolower($out['NODES']['STATUS']) == 'failed'){
			$status = 'FAILED';
			$description = isset($out['NODES']['REFID']) ? trim($out['NODES']['REFID']) : "";
		}
		else {
			$status = trim($out['NODES']['STATUS']);
			$description = "NA";
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['NODES']['REFID']);
		}
				
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");                       
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}    
    
    
    /*
     * Api online recharge "aporec"
     */
    function aporecBalance($panel = null,$min = null, $power = null){
		$vendor_id = 63;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
            $url = APOREC_BAL_URL;
            $request_param = array('apiid'=>APOREC_USERID,'apiname'=>APOREC_USER_NAME,'apipass'=>APOREC_PASSWORD);
            
			$out = $this->General->aporecApi($url,$request_param);
		
            $out = $out['output'];
            
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/aporec.txt",date('Y-m-d H:i:s').":Balance Check: ".json_encode($out));
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		
		$bal = $out;
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("aporec Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			$this->autoRender = false;
		}
		else {
			$ret = array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			
			if($min == -1){
				echo json_encode($ret);
				$this->autoRender = false;
			}
			else return $ret;
		}
	}

    /*
     * aporec prepaid mobile recharges
     */
    function aporecMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
		if(in_array($prodId,array('27','28','29','31','34'))){
			$params['type'] = 'voucher';
		}
    	if($prodId == '2' && $amount < 50){
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
        
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['aporec'];
		$vendor = 63;
				
		$url = APOREC_RECHARGE_URL;
        //--- request parameter
        $request_param = array('apiid'=>APOREC_USERID,'apiname'=>APOREC_USER_NAME,'apipass'=>APOREC_PASSWORD,
                'reqid'=>$transId,'custno'=>$mobileNo,'amount'=>$amount,'opcode'=>$provider,'storeid'=>'');
        
        $out = $this->General->aporecApi($url,$request_param);

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/aporec.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['ResponseCode']) && $out['ResponseCode'] == 1){
			$status = 'FAILED';
			$description = isset($out['Status']) ? trim($out['Status']) : (isset($out['Reason']) ? trim($out['Reason']) : "");
		}
		else {
			$status = trim($out['ResponseText']);
			$description = trim($out['ResponseText']);
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['TXNID']);
		}
				
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
    
    /*
     *api online recharges dth 
     */
    function aporecDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['aporec'];
		$vendor = 63;
		
        $url = APOREC_RECHARGE_URL;
        //--- request parameter
        $request_param = array('apiid'=>APOREC_USERID,'apiname'=>APOREC_USER_NAME,'apipass'=>APOREC_PASSWORD,
                'reqid'=>$transId,'custno'=>$mobileNo,'amount'=>$amount,'opcode'=>$provider,'storeid'=>'');
        
        $out = $this->General->aporecApi($url,$request_param);

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/aporec.txt","*Dth Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['ResponseCode']) && $out['ResponseCode'] == 1){
			$status = 'FAILED';
			$description = isset($out['Status']) ? trim($out['Status']) : (isset($out['Reason']) ? trim($out['Reason']) : "");
		}
		else {
			$status = trim($out['ResponseText']);
			$description = trim($out['ResponseText']);
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['TXNID']);
		}
				
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
    
    /*
     * Api Online recharge
     */
    function aporecStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/aporec.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $transId = trim($data['client_trans_id']);
        $this->Shop->setMemcache("aporec".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 63");
		$this->autoRender = false;
	}
    
    /*
     * hitech recharge
     */
    function hitechStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/hitech.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $transId = trim($data['rid']);
        $this->Shop->setMemcache("hitech".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 65");
		$this->autoRender = false;
	}
        
     /*
      * hitech tranStatus
      */
    function hitechTranStatus($transId,$params,$prodId){

	$url = HITECH_TRANSL_URL;
	       //--- request parameter
        $request_param = array('uid'=>HITECH_USERID,'pass'=>HITECH_PASSWORD,'mno'=>HITECH_MNO,'msg'=>"cs$transId");

        $out = $this->General->hitechrecApi($url,$request_param);

	$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/hitech.txt",date('Y-m-d H:i:s').":hitechTranStatus: ".json_encode($out));

	$description = $out['output']['NODES']['MESSAGE'];

	$status = (explode(" ",$out['output']['NODES']['MESSAGE']));

	if(in_array($status[3],array('S','MS'))){
		$ret =  array('status'=>'success','status-code'=>$status[3],'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
	}
	else if(in_array($status[3],array('F','MF')) || strstr(strtolower($description),"transaction not found") || strstr(strtolower($description),"no record found") ){
		$ret =  array('status'=>'failure','status-code'=>$status[3],'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
	}
	else {
		$ret =  array('status'=>'pending','status-code'=>$status[3],'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
	}

	$this->printArray($ret);
	return $ret;
	$this->autoRender = false;
    }
        
    /*
     * Api online recharge api Trans Status
     */
    function aporecTranStatus($transId,$date=null,$refId=null){
		$url = APOREC_TRANS_URL;
        
		$request_param = array('apiid'=>APOREC_USERID,'apiname'=>APOREC_USER_NAME,'apipass'=>APOREC_PASSWORD,'clientid'=>$transId);                
        
		$out = $this->General->aporecApi($url,$request_param);
	
		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/aporec.txt",date('Y-m-d H:i:s').":aporecTranStatus: ".json_encode($out));

		$status = isset($out['Status']) ? strtoupper(trim($out['Status'])) : "";
		$operator_id = isset($out['OPRID'])?trim($out['OPRID']):"";
		$vendor_id = isset($out['trans_id'])?trim($out['trans_id']):"";
		
		$description = "";
		
		if($status == 'SUCCESS'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if(in_array($status,array('FAILED','REFUNDED')) || strstr(strtolower($status),"client id not found")){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
    
	function gitechMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);
                
                if(in_array($prodId,array('27','28','29','31','34'))){
                                $params['type'] = 'voucher';
		}
                
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['gitech'];
		$item_desc = $this->mapping['mobRecharge'][$params['operator']]['operator'];
		$vendor = 35;

        $newtransId = $transId.$transId.$transId;
		$request = "<MobileBookingRequest>
<UsertrackId>$newtransId</UsertrackId>
<Itemid>$provider</Itemid>    
<ItemDesc>$item_desc</ItemDesc>
<MobileNo>$mobileNo</MobileNo>
<Amount>$amount</Amount>
</MobileBookingRequest>";
		$out = $this->General->gitechApi("MOBILEBOOKINGDETAILS",$request);
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gitech.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		$status = trim($out['MobileBookingResponse']['Status']);
		
		if($status == '1'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['MobileBookingResponse']['Remarks']));
			$txnId = trim($out['MobileBookingResponse']['TransNo']);
			
			if(empty($description))$description = 'success';
			$this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
			return array('status'=>'success','code'=>'13','description'=>$this->Shop->errors(13),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>'');
		}
		else if($status == '0'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['MobileBookingResponse']['Error']['Remarks']));
			$txnId = "";
			
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId);
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}

    
    /*
     * GI-tech Dth recharge
     * 
     */
    
    function gitechDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['gitech'];
		$item_desc = $this->mapping['dthRecharge'][$params['operator']]['operator'];
		$vendor = 35;

        $newtransId = $transId.$transId.$transId;
		$request = "<MobileBookingRequest>
<UsertrackId>$newtransId</UsertrackId>
<Itemid>$provider</Itemid>
<ItemDesc>$item_desc</ItemDesc>
<MobileNo>$mobileNo</MobileNo>
<Amount>$amount</Amount>
</MobileBookingRequest>";
		$out = $this->General->gitechApi("MOBILEBOOKINGDETAILS",$request);
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gitech.txt","*Dth Recharge*: Input=> $transId<br/>".json_encode($out));

		$status = trim($out['MobileBookingResponse']['Status']);
		
		if($status == '1'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['MobileBookingResponse']['Remarks']));
			$txnId = trim($out['MobileBookingResponse']['TransNo']);
			
			if(empty($description))$description = 'success';
			$this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
			return array('status'=>'success','code'=>'13','description'=>$this->Shop->errors(13),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>'');
		}
		else if($status == '0'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['MobileBookingResponse']['Error']['Remarks']));
			$txnId = "";
			
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId);
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}

    /*
     * 
     * GI TECH BILLING
     */
    
    function gitechBillPayment($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['billPayment'][$params['operator']][$params['type']]['gitech'];
		$item_desc = $this->mapping['billPayment'][$params['operator']]['operator'];
		$vendor = 35;

        $newtransId = $transId.$transId.$transId;
		$request = "<BillBookingRequest>
<UsertrackId>$newtransId</UsertrackId>
<Itemid>$provider</Itemid>
<ItemDesc>$item_desc</ItemDesc>
<MobileNo>$mobileNo</MobileNo>
<Amount>$amount</Amount>
</BillBookingRequest>";
		$out = $this->General->gitechApi("BILLPAYMENTBOOKINGDETAILS",$request);
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gitech.txt","*Bill Payment*: Input=> $transId<br/>".json_encode($out));

		$status = trim($out['BillBookingResponse']['Status']);
		
		if($status == '1'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['BillBookingResponse']['Remarks']));
			$txnId = trim($out['BillBookingResponse']['TransNo']);
			
			if(empty($description))$description = 'success';
			$this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response
                        $this->General->log_in_vendor_message(array($transId,$txnId,'4',$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
			return array('status'=>'success','code'=>'13','description'=>$this->Shop->errors(13),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>'');
		}
		else if($status == '0'){
			$this->Shop->addStatus($transId,$vendor);
			$description = json_encode(trim($out['BillBookingResponse']['Error']['Remarks']));
			$txnId = "";
			
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','4',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,$txnId,'4',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId);
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','4',$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");                       
                        $this->General->log_in_vendor_message(array($transId,$txnId,'4',$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
    
    /*
     * rkit DTH intergation
     * 
     */
	function rkitDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['rkit'];
		$vendor = 34;
		$rcType = 'NORMAL';
		

		$url = RKIT_RECHARGE_URL;
                $out = $this->General->rkitApi($url,array('USERID'=>RKIT_USER,'PASSWORD'=>RKIT_AUTH,'SUBSCRIBER'=>$mobileNo,'AMOUNT'=>$amount,
                'TRANNO'=>$transId,'RECTYPE'=>$rcType,'OPERATOR'=>$provider));
        
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rkit.txt","*Dth Recharge*: Input=> $transId<br/>$out");

		//1. SUCCESS# RECHARGE POSTED # TRANREFNO #98888
		//3. ERROR# INVALID MOBILE ENTRY #//TRANNO:9836-STATUS:SUCCESS
        //ERRCODE:104-OPERATOR IS NOT AVAILABLE
       
        $txnId = "";    
		
		if($out['NODE']['STATUS'] == 'FAILED'){
            $description = trim($out['NODE']['STATUS']);
			$this->Shop->addStatus($transId,$vendor);
			$txnId = $out['NODE']['TXNID'];
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
            $txnId = $out['NODE']['TXNID'];
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}

    /*
     * joinrec Dth 
     */
    
    function joinrecDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['joinrec'];
		$vendor = 48;
		$rcType = '';
		
		$url = JOINREC_RECHARGE_URL;
		$out = $this->General->joinrecApi($url,array('reseller_id'=>JOINREC_ID,'reseller_pass'=>JOINREC_PWD,'mobilenumber'=>$mobileNo,'denomination'=>$amount,'meroid'=>$transId,'voucher'=>$rcType,'operatorid'=>$provider,'circleid'=>'*'));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt","*Dth Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['Data']['Error'])){
			$status = 'FAILED';
			$description = trim($out['Data']['Error']);
		}
		else {
			$status = trim($out['Data']['Status']);
			$description = trim($out['Data']['Description']);
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['Data']['OrderId']);
		}
		
		
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
    
    function mypayDthRecharge($transId,$params,$prodId){

		$mobileNo = $params['subId'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['mypay'];
		$vendor = 57;
		$rcType = 3;
						
		$url = MYPAYURL;
        $pwd = MYPAYPASSWD;
        
        $dt_time = date('m.d.Y H:i:s');
        
        //pwd|mob|amt|opr|type|datetime
        $req_str = $pwd."|".$mobileNo."|".$amount."|".$provider."|".$rcType."|".$dt_time."|".$transId;
        
		$out = $this->General->mypayApi($url,array('_prcsr'=>MYPAYUSER,'_urlenc'=>$req_str,'encuse'=>0));
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/mypay.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['_ApiResponse']['statusCode']) && strtolower($out['_ApiResponse']['statusCode']) != "10008"){
			$status = 'FAILED';
			$description = trim($out['_ApiResponse']['statusDescription']);
		}
		else {
			$status = trim($out['_ApiResponse']['statusCode']);
			$description = isset($out['_ApiResponse']['statusDescription'])?trim($out['_ApiResponse']['statusDescription']):"NA";
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['_ApiResponse']['requestID']);
		}
		
		
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($description)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($description),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
    
	function ppiDthRecharge($transId,$params,$prodId=null){
		//return array('status'=>'success','description'=>'success','tranId'=> rand(10000,100000));
		$subId = $params['subId'];
		$operator = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['pp'];
		//$soap = '<transactionRefId>'.$transId.'</transactionRefId><amount>'.$params['amount'].'</amount><clientCode>'.$subId.'</clientCode><operatorCode>'.$operator.'</operatorCode>';
		$soap = '<transactionRefId>'.$transId.'</transactionRefId><amount>'.$params['amount'].'</amount><clientCode>'.$subId.'</clientCode><operatorCode>'.$operator.'</operatorCode>';
		try{
			$processTran = $this->ppiApi('DirectToHomeERecharge',$soap);
		}catch(Exception $e){
			$this->General->log_in_vendor_message(array($transId,'','2','2','16','','failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','2','16','','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(empty($processTran)){
			$this->General->log_in_vendor_message(array($transId,'','2','2','16','','failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','2','16','','failure','".date("Y-m-d H:i:s")."')");
			$this->General->sendMails('PPI DTH recharge: Blank Transaction status','tran id: '.$transId,array('notifications@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(strtolower(trim($processTran['Response']['TransactionStatus'])) == '0'){//failed
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['Response']['TransactionId']."','2','2','10','".addslashes($processTran['Response']['Msg'])."','failure','".date("Y-m-d H:i:s")."')");
			$this->General->log_in_vendor_message(array($transId,$processTran['Response']['TransactionId'],'2','2','10',addslashes($processTran['Response']['Msg']),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>$processTran['Response']['TransactionId']);
		}else{
			$this->General->log_in_vendor_message(array($transId,$processTran['Response']['TransactionId'],'2','2','13',addslashes($processTran['Response']['Msg']),'success',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['Response']['TransactionId']."','2','2','13','".addslashes($processTran['Response']['Msg'])."','success','".date("Y-m-d H:i:s")."')");
			return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['Response']['TransactionId']);
		}

		$this->autoRender = false;
	}

	function ossDthRecharge($transId,$params,$prodId=null){
		//return array('status'=>'success','description'=>'success','tranId'=> rand(10000,100000));
		//get circle
		$mobileNo = $params['mobileNumber'];
		$circle = 'ALI';

		//get recharge codes
		$operator = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['oss'];

		$rechargesReq = '<GetRechargesRequest><CategoryCode>dt</CategoryCode><OperatorCode>'.$operator.'</OperatorCode><CircleCode>'.$circle.'</CircleCode></GetRechargesRequest>';
		try{
			$rechargesRes = $this->ossApi('GetRecharges',$rechargesReq);
		}catch(Exception $e){
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','1','17','','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(empty($rechargesRes)){
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','1','36','','failure','".date("Y-m-d H:i:s")."')");
			$sub = "OSS server not responding";
			$body = json_encode($params);
			//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com','chirutha@mindsarray.com'));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(strtolower($rechargesRes['GetRechargesResponse']['ResponseStatus']['Status']) != 'success'){
			$errMsg = addslashes('Status: '.$rechargesRes['GetRechargesResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorDesc']);
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','1','23','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
			//$this->General->sendMails('OSS: DTH recharge code not available.','Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator);
			return array('status'=>'failure','code'=>'29','description'=>$this->Shop->errors(29),'tranId'=>'');
		}

		$rechargeCode = '';
		$avail = '';
		if(isset($rechargesRes['GetRechargesResponse']['RechargeDetails']['OperatorName'])){
			if((int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == 0  || (int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == $params['amount']){
				$rechargeCode = trim($rechargesRes['GetRechargesResponse']['RechargeDetails']['RechargeCode']);
			}
		}else{
			foreach($rechargesRes['GetRechargesResponse']['RechargeDetails'] as $r){
				$avail .= $r['Denomination'].", ";
				if((int)$r['Denomination'] == 0 || (int)$r['Denomination'] == $params['amount']){
					$rechargeCode = trim($r['RechargeCode']);
					break;
				}
			}
		}

		if($rechargeCode == ''){
			$errMsg = '';
			if(isset($rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'])){
				$errMsg .= addslashes('Status: '.$rechargesRes['GetRechargesResponse']['ResponseStatus']['Status'].' ErrorCode:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$rechargesRes['GetRechargesResponse']['ResponseStatus']['ErrorDesc']);
			}
			$errMsg .= 'Available recharges: '.$avail;

			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','1','24','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
			//$this->General->sendMails('OSS: DTH Flexi recharge code not found.','Mobile No: '.$params['mobileNumber'].' Circle:'.$circle.' Operator:'.$operator);
			return array('status'=>'failure','code'=>'29','description'=>'Available recharges: Rs.'.$avail,'tranId'=>'');
		}

		$requestXML = '<ProcessTransactionRequest><MerchantRefNo>'.$transId.'</MerchantRefNo><RechargeCode>'.$rechargeCode.'</RechargeCode><Qty>1</Qty><CategoryCode>dt</CategoryCode><OperatorCode>'.$operator.'</OperatorCode><CircleCode>'.$circle.'</CircleCode><ConsumerNo>'.$params['subId'].'</ConsumerNo><CustMobileNo>'.$mobileNo.'</CustMobileNo><Denomination>'.$params['amount'].'</Denomination><PNRNo></PNRNo><PGCode>'.OSS_PG_CODE.'</PGCode><DistCustCode>'.OSS_DIST_CUST_CODE.'</DistCustCode><CardNo></CardNo><TranDistCustCode>'.OSS_TRAN_DIST_CUST_CODE.'</TranDistCustCode><TranPassword>'.OSS_TRAN_PASSWORD.'</TranPassword></ProcessTransactionRequest>';
		try{
			$processTran = $this->ossApi('ProcessTransaction',$requestXML);
		}catch(Exception $e){
			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','1','18','','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		if(trim(strtolower($processTran['ProcessTransactionResponse']['ResponseStatus']['Status'])) == ''){
			//$this->General->sendMails('OSS DTH recharge: Blank Transaction status','tran id: '.$transId,array('chirutha@mindsarray.com'));
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','2','1','15','','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
		}else if(strtolower($processTran['ProcessTransactionResponse']['ResponseStatus']['Status']) != 'success'){
			$errMsg = addslashes('Status: '.$processTran['ProcessTransactionResponse']['ResponseStatus']['Status'].' ErrorCode:'.$processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorCode'].' ErrorDesc:'.$processTran['ProcessTransactionResponse']['ResponseStatus']['ErrorDesc']);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','2','1','18','".$errMsg."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'');
		}else{
			if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '2'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','2','1','15','','pending','".date("Y-m-d H:i:s")."')");
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}else if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '1'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','2','1','14','','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}else if($processTran['ProcessTransactionResponse']['TransactionDetails']['Status'] == '0'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo']."','2','1','13','','success','".date("Y-m-d H:i:s")."')");
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['TransactionNo'],'pinRefNo'=>$processTran['ProcessTransactionResponse']['TransactionDetails']['PinRefNo']);
			}
		}
		$this->autoRender = false;
	}

	function paytDthRecharge($transId,$params,$prodId = null){
		$mobileNo = $params['subId'];
		$type = 1;
		$operator = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['payt'];

		if($prodId == 18 && $params['amount'] < 250){
			$this->Shop->addStatus($transId,5);
			$this->User->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2','5','14','Amount issues payt','failure','".date("Y-m-d H:i:s")."')");

			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
		
		$message="$operator|0|$mobileNo|".$params['amount']."|$type";
		$date = date('YmdHis');
		$terminal = PAYT_USER_CODE;
		$sha=strtoupper(sha1($terminal.$transId.$message.$date.PAYT_PASSWORD));
		$message = urlencode($message);
		$sha = urlencode($sha);
		$content = "OperationType=1&TerminalId=$terminal&TransactionId=$transId&DateTimeStamp=$date&Message=$message&Hash=$sha";

		$Rec_Data = $this->paytConnection($content);
		if(!$Rec_Data['success']){
			if($Rec_Data['timeout']){
				$this->Shop->addStatus($transId,5);
				$this->User->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2','5','14','Connectivity timeout from payt','failure','".date("Y-m-d H:i:s")."')");

				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$Rec_Data = $Rec_Data['output'];
		$response = explode("|",$Rec_Data);
                
                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/payt.txt","*Dth Recharge*: Input=> $transId<br/>".json_encode($response));
                
		if(count($response) == 6 && $transId == trim($response[1])){
			$ref_id = trim($response[2]);
			$status = trim($response[0]);
			$opr_id = trim($response[4]);
			$err_code = trim($response[3]);
			if($err_code == 'NA')$opr_id = "";
			if($opr_id == 'NA')$opr_id = "";
			$desc = trim($response[5]);

			if($status == '0' && $err_code != '8'){//success
				$this->Shop->addStatus($transId,5);
				$this->Shop->setMemcache("vendor5_last",date('Y-m-d H:i:s'),24*60*60);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','2','5','13','".addslashes($desc)."','success','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Success: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
					
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status == '1' || $err_code == '8'){//under process
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','2','5','15','".addslashes($desc)."','pending','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Pending: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
					
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status == '2'){//failure
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$ref_id','2','5','14','".addslashes("Error code: $err_code, ". $desc)."','failure','".date("Y-m-d H:i:s")."')");
				//$this->General->sendMails("Failure: Paytronics","TransId: $transId<br/>$desc",array('ashish@mindsarray.com'));
					
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$ref_id,'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		/*else if(!empty($Rec_Data)){
			$this->Shop->addStatus($transId,5);
			//$this->General->sendMails("Error in request: Paytronics",$content ."<br/>".$Rec_Data,array('ashish@mindsarray.com'));

			$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2','5','14','".addslashes($Rec_Data)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}*/
		else {
			//$this->General->sendMails("Empty response: Paytronics",$content ."<br/>".$Rec_Data,array('ashish@mindsarray.com'));

			$data = $this->paytTranStatus($transId);

			if($data['status'] == 'success'){
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','2','5','13','".addslashes($data['description'])."','success','".date("Y-m-d H:i:s")."')");
					
				return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}
			else if($data['status'] == 'pending'){
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','2','5','15','".addslashes($data['description'])."','pending','".date("Y-m-d H:i:s")."')");
					
				return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}
			else if($data['status'] == 'failure'){
				$this->Shop->addStatus($transId,5);
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$data['ref_id']."','2','5','14','".addslashes($data['description'])."','failure','".date("Y-m-d H:i:s")."')");

				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$data['ref_id'],'pinRefNo'=>'','operator_id'=>'');
			}

			//return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
		}
	}


	function modemDthRecharge($transId,$params,$prodId,$vendor=4,$shortForm='modem'){
		$mobileNo = $params['mobileNumber'];

		$adm = "query=recharge&oprId=$prodId&mobile=$mobileNo&amount=".$params['amount']."&param=".$params['subId']."&type=1&transId=$transId";

		try{
			$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
			if($Rec_Data['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'36','".addslashes($Rec_Data['error'])."','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'36',addslashes($Rec_Data['error']),'failure',date("Y-m-d H:i:s")));
				//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                                $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
		
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			}
			else {
				$Rec_Data = $Rec_Data['data'];
			}
		}catch(Exception $e){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'10','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'10','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}

		$product = $this->mapping['dthRecharge'][$params['operator']]['operator'];


		if(strpos($Rec_Data, 'Error') !== false){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'36','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'36','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm not responding";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "1"){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'30','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'30','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Not able to add request in $product";
			$body = json_encode($params);
			$this->General->sendMails($sub,$body,array('backend@mindsarray.com'));
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "2"){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'40','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'40','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: No active sim/no balance in $product";
			$body = json_encode($params);
			$this->Shop->addStatus($transId,$vendor);
			//$this->General->sendMails($sub,$body,array('notifications@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($Rec_Data == "3"){
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'41','','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'41','','failure',date("Y-m-d H:i:s")));
			$sub = "Recharge $shortForm: Dropped due to lot of pending requests in $product";
			$body = json_encode($params);
			$this->Shop->addStatus($transId,$vendor);
			//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com','chirutha@mindsarray.com'));
			//$this->User->query("UPDATE vendors_transactions SET status = 2 WHERE ref_id='$transId' AND vendor_id= $vendor");
                        $this->General->update_in_vendor_transaction("status=2","ref_id='$transId',vendor_id=$vendor");
                        
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$Rec_Data,'pinRefNo'=>'');
		}

	}

	function cpDthRecharge($transId,$params,$prodId){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;
                
		$extra = "ACCOUNT=\r\n";
		if($prodId == '16'){
			$store_data = $this->Slaves->query("SELECT store_code FROM cp_retailers WHERE service_id='DTH' AND active_flag = 1 ORDER BY RAND() LIMIT 1");
			if(!empty($store_data)){
				$extra.="TERM_ID=".trim($store_data[0]['cp_retailers']['store_code'])."\r\n";
			}
			else {
				return array('status'=>'failure','code'=>'27','description'=>$this->Shop->errors(27),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$subId = $params['subId'];
		$amount = $params['amount'];

		$verify_url = $this->cpurls[$prodId]['cpv'];
		$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$subId\r\n".$extra."AMOUNT=".floatval($amount)."\r\nAMOUNT_ALL=$amount\r\nCOMMENT=\r\n";
		$res = ipriv_sign($data, $sec, CYBER_PASSWORD);

		$out = $this->cpConnect($verify_url,array('inputmessage'=>$res[1]));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,8);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,'','2',8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];

		$res = ipriv_verify($out, $pub);
		$result = $this->cpArray($res);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Dth Recharge*: $verify_url, Input=> ".$data."\n".json_encode($result));
		
		if(isset($result['RESULT']) && $result['RESULT'] == 0 && empty($result['ERROR'])){
			if(isset($result['AUTHCODE']))$operator_id = $result['AUTHCODE'];

			$live_url = $this->cpurls[$prodId]['cpl'];
			$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$subId\r\n".$extra."AMOUNT=".floatval($amount)."\r\nPAY_TOOL=0\r\n";
			$res = ipriv_sign($data, $sec, CYBER_PASSWORD);

			$out = $this->cpConnect($live_url,array('inputmessage'=>$res[1]));
			if(!$out['success']){
				if($out['timeout']){
					$this->Shop->addStatus($transId,8);
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'','2',8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
					return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
				else {
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',8,'15','Request timed out while recharging','pending','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'','2',8,'15','Request timed out while recharging','pending',date("Y-m-d H:i:s")));
					return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
			}
			else {
				$out = $out['output'];
			}

			$res = ipriv_verify($out, $pub);
			$result = $this->cpArray($res);
			
			$vendorTransId = (isset($result['TRANSID'])) ? $result['TRANSID'] : 0;
			$opr_id = (isset($result['AUTHCODE'])) ? $result['AUTHCODE'] : 0;
			$trans_status = (isset($result['TRNXSTATUS'])) ? $result['TRNXSTATUS'] : 3;

			if($trans_status != 7){
				$status = $this->cpTranStatus($transId,null,null,$prodId);
			}
			else {
				$status['status'] = 'success';
				$status['transaction_id'] = $vendorTransId;
			}
			
			$error = isset($status['errCode']) ? $status['errCode'] : $result['ERROR'];
			//$error = $result['ERROR'];
			
			$errorCode = $this->Shop->errorCodeMapping(8,$error);
			
			if($status['status'] == 'error' || $status['status'] == 'inprocess' || $status['status'] == 'incomplete'){//in process
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','2',8,'15','".$status['status']."::".$status['errMsg']."(".$status['errCode'].")','pending','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'2',8,'15',$status['status']."::".$status['errMsg']."(".$status['errCode'].")",'pending',date("Y-m-d H:i:s")));
				return array('status'=>'pending','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,8);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','2',8,'14','".$status['errMsg']."(".$status['errCode'].")','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'2',8,'14',$status['errMsg']."(".$status['errCode'].")",'failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,8);
				$this->Shop->setMemcache("vendor8_last",date('Y-m-d H:i:s'),24*60*60);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','2',8,'13','','success','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],'2',8,'13','','success',date("Y-m-d H:i:s")));
				return array('status'=>'success','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		else {
			
			
			$error = $result['ERROR'];
			$errorCode = $this->Shop->errorCodeMapping(8,$error);
			$this->Shop->addStatus($transId,8);
			$err = "Error code: ".$result['ERROR'] . "," . $this->cp_errs[$result['ERROR']];
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',8,'42','".addslashes($err)."','failure','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,'','2',8,'42',addslashes($err),'failure',date("Y-m-d H:i:s")));
			$sub = "Cyberplate: Error in Payment authorization";
			$body = "Request: ".json_encode($params) . "<br/>Response: " . json_encode($result) . "<br/>Error: " . $this->cp_errs[$result['ERROR']];
			//$this->General->sendMails($sub,$body,array('ashish@mindsarray.com','chirutha@mindsarray.com'));
			return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>'');
		}
	}

	function cbzDthRecharge($transId,$params,$prodId){
		$special = 'tp';

		$subid = $params['subId'];
		$amount = $params['amount'];
		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['cbz'];
		$vendor = 11;
		$url = CBZ_RECHARGE_URL;
		$out = $this->General->cbzApi($url,array('username'=>CBZ_REC_USERNAME,'password'=>CBZ_REC_PASSWORD,'key'=>CBZ_REC_KEY,'provider'=>$provider,'no'=>$subid,'amount'=>$amount,'rechargeType'=>$special,'circle'=>'','trans_id'=>$transId));
			
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
			else {
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'15','Request timed out','pending','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'15','Request timed out','pending',date("Y-m-d H:i:s")));
				return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
		else {
			$out = $out['output'];
		}

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cbz.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		$res = $this->General->xml2array($out);


		if($res['root']['result'] == "fail"){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','2',$vendor,'30','".$res['root']['message']."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'30',$res['root']['message'],'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else if($res['root']['result'] == "received" && $res['root']['req_id'] == $transId){
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$res['root']['tr_id']."','2',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$res['root']['tr_id'],'2',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$res['root']['tr_id'],'pinRefNo'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'15','','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','2',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>'','pinRefNo'=>'');
		}
	}

	function rduDthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$amount = $params['amount'];

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['rdu'];
		$vendor = 16;

		$url = RDU_RECHARGE_URL;
		$out = $this->General->rduApi($url,array('userId'=>RDU_REC_USERNAME,'pwd'=>RDU_REC_PASSWORD,'rechargeServiceCode'=>'DTH','memberCode'=>'EA10P421','operatorCode'=>$provider,'mobileNo'=>$subid,'amount'=>$amount,'clientTrnId'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rdu.txt","*DTH Recharge*: Input=> $transId<br/>$out");


		$res = explode(":",$out);
		$code = trim($res['0']);
		$msg = trim($res['2']);

		/*if(in_array($code,array(0,2))){
			$this->Shop->setMemcache("vendor16_last",date('Y-m-d H:i:s'),24*60*60);
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','1',$vendor,'13','".addslashes($msg)."','success','".date("Y-m-d H:i:s")."')");
				
			return array('status'=>'success','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>trim($res['3']),'pinRefNo'=>'');
			}
			else*/ if(in_array($code,array(0,2,528))){
		//$this->Shop->setMemcache("vendor16_last",date('Y-m-d H:i:s'),24*60*60);
		$this->General->log_in_vendor_message(array($transId,trim($res['3']),'2',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
		//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','2',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
		return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>trim($res['3']),'pinRefNo'=>'');
			}
			else {
				$this->General->log_in_vendor_message(array($transId,trim($res['3']),'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
				//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($res['3'])."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>trim($res['3']));
			}
	}

	function uvaDthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$amount = $params['amount'];
		$mobileNo = $params['mobileNumber'];

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['uva'];
		$vendor = 18;

		$uniqueid = '496091';
		$code = md5($uniqueid.$mobileNo.$amount.$transId);

		$url = UVA_DTHRECHARGE_URL;
		$out = $this->General->uvaApi($url,array('uniqueid'=>$uniqueid,'amount'=>$amount,'mobile'=>$mobileNo,'dthno'=>$subid,'dthname'=>$provider,'refno'=>$transId,'code'=>$code));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uva.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		$res = explode("|",$out);
		$code = trim($res['0']);
		$exp = explode("-",trim($res['1']));
		$msg = trim($exp['0']);
		$txnId = trim($exp['1']);

		if(in_array($code,array(0,1))){
			//$this->Shop->setMemcache("vendor18_last",date('Y-m-d H:i:s'),24*60*60);
			$this->General->log_in_vendor_message(array($transId,trim($txnId),'2',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			///$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','2',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'31','description'=>$this->Shop->errors(31),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,trim($txnId),'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId);
		}
	}

	function uniDthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$amount = intval($params['amount']);
		$mobileNo = $params['mobileNumber'];

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['uni'];
		$vendor = 19;

		$url = UNI_RECHARGE_URL;
		$out = $this->General->uniApi($url,array('rcm'=>$subid,'rca'=>$amount,'crqid'=>$transId,'cro'=>$provider));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uva.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		/////0|Transaction Successful | 12121212121 | 121139144523204238;
		$res = explode("|",$out);

		if(count($res) == 2){
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$code = trim($res['0']);
			$msg = trim($res['1']);
			$txnId = trim($res['3']);
			$this->General->log_in_vendor_message(array($transId,trim($txnId),'2',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','2',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}

	}
	
	function magicDthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['magic'];
		$vendor = 24;

                $url = MAGIC_RECHARGE_URL;
		$out = $this->General->magicApi($url,array('uid'=>MAGIC_USERNAME,'pwd'=>MAGIC_PASSWD,'rcode'=>$provider,'mobileno'=>$subid,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/magic.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}
	
	
	function rioDthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['rio'];
		$vendor = 36;

		$url = RIO_RECHARGE_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO_USERNAME,'pwd'=>RIO_PASSWD,'rcode'=>$provider,'mobileno'=>$subid,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
			$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}

        function rio2DthRecharge($transId,$params,$prodId){
		$subid = $params['subId'];
		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['rio2'];
		$vendor = 62;

		$url = RIO2_RECHARGE_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO2_USERNAME,'pwd'=>RIO2_PASSWD,'rcode'=>$provider,'mobileno'=>$subid,'amt'=>$amount,'transid'=>$transId));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio2.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		//297499#Pending#ACCEPT
		$status = trim($out);
		$txnId = time();
		$txnId .= rand(100,999);

		$status_array = array('1200'=>'Request Accepted','1201'=>'Invalid Login','1202'=>'Invalid Mobile Number','1203'=>'Invalid Amount','1204'=>'Transaction ID missing','1205'=>'Operator not found','1206'=>'Permission Required','1207'=>'Balance Limit','1208'=>'Low Balance','1209'=>'Duplicate Request','1210'=>'Request not accepted','1211'=>'Recharge server not connected','1212'=>'Authentication Failed');
		$out = $status_array[$status];

		if(empty($status) || $status == '1200'){
			//$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                        
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'15','','pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
	}

	function cpBillPayment($transId,$params,$prodId){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;

                $extra = "ACCOUNT=\r\n";
		if($prodId == '42'){
			$store_data = $this->Slaves->query("SELECT store_code FROM cp_retailers WHERE service_id='Postpaid' AND active_flag = 1 ORDER BY RAND() LIMIT 1");
			if(!empty($store_data)){
				$extra.="TERM_ID=".trim($store_data[0]['cp_retailers']['store_code'])."\r\n";
			}
			else {
				return array('status'=>'failure','code'=>'25','description'=>$this->Shop->errors(25),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
		
		$mobileNo = $params['mobileNumber'];
		$amount = $params['amount'];
		$service_id = 4;
		$vendor = 8;

		$verify_url = $this->cpurls[$prodId]['cpv'];
		$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$mobileNo\r\n".$extra."AMOUNT=".floatval($amount)."\r\nAMOUNT_ALL=$amount\r\nCOMMENT=\r\n";
		$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
		$out = $this->cpConnect($verify_url,array('inputmessage'=>$res[1]));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
		$out = $out['output'];

		$res = ipriv_verify($out, $pub);
		$result = $this->cpArray($res);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Bill Payment*: $verify_url, Input=> ".$data."\n".json_encode($result));
		
		if(isset($result['RESULT']) && $result['RESULT'] == 0 && empty($result['ERROR'])){
			if(isset($result['AUTHCODE']))$operator_id = $result['AUTHCODE'];

			$live_url = $this->cpurls[$prodId]['cpl'];
			$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$mobileNo\r\n".$extra."AMOUNT=".floatval($amount)."\r\nPAY_TOOL=0\r\n";
			$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
				
			$out = $this->cpConnect($live_url,array('inputmessage'=>$res[1]));
			if(!$out['success']){
				if($out['timeout']){
					$this->Shop->addStatus($transId,$vendor);
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
					return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
				else {
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'15','Request timed out while recharging','pending','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'15','Request timed out while recharging','pending',date("Y-m-d H:i:s")));
					return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
			}
			else {
				$out = $out['output'];
			}

			$res = ipriv_verify($out, $pub);
			$result = $this->cpArray($res);
			
			$vendorTransId = (isset($result['TRANSID'])) ? $result['TRANSID'] : 0;
			$opr_id = (isset($result['AUTHCODE'])) ? $result['AUTHCODE'] : 0;
			$trans_status = (isset($result['TRNXSTATUS'])) ? $result['TRNXSTATUS'] : 3;

			if($trans_status != 7){
				$status = $this->cpTranStatus($transId,null,null,$prodId);
			}
			else {
				$status['status'] = 'success';
				$status['transaction_id'] = $vendorTransId;
			}
			
			$error = $result['ERROR'];
			$errorCode = $this->Shop->errorCodeMapping(8,$error);

			if($status['status'] == 'error' || $status['status'] == 'inprocess' || $status['status'] == 'incomplete'){//in process
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'15','".$status['status']."::".$status['errMsg']."(".$status['errCode'].")','pending','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'15',$status['status']."::".$status['errMsg']."(".$status['errCode'].")",'pending',date("Y-m-d H:i:s")));
				return array('status'=>'pending','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'14','".$status['errMsg']."(".$status['errCode'].")','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'14',$status['errMsg']."(".$status['errCode'].")",'failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor);
				$this->Shop->setMemcache("vendor8_last",date('Y-m-d H:i:s'),24*60*60);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'13','','success','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'13','','success',date("Y-m-d H:i:s")));
				return array('status'=>'success','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			//$code = ($result['ERROR'] == 23) ? 5 : (($result['ERROR'] == 7) ? 6 : 42);
			$error = $result['ERROR'];
			$errorCode = $this->Shop->errorCodeMapping(8,$error);
			$err = "Error code: ".$result['ERROR'] . "," . $this->cp_errs[$result['ERROR']];
				
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$service_id',8,'42','".addslashes($err)."','failure','".date("Y-m-d H:i:s")."')");                        
                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'42',addslashes($err),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>'');
		}
	}


	function uniBillPayment($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['billPayment'][$params['operator']][$params['type']]['uni'];
		$vendor = 19;

		$url = UNI_RECHARGE_URL;
		$out = $this->General->uniApi($url,array('rcm'=>$mobileNo,'rca'=>$amount,'crqid'=>$transId,'cro'=>$provider));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','4',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','4',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uni.txt","*Bill Payment*: Input=> ".json_encode(array('rcm'=>$mobileNo,'rca'=>$amount,'crqid'=>$transId,'cro'=>$provider))."\n".json_encode($params)."\n$out");

		/////0|Transaction Successful | 12121212121 | 121139144523204238;
		$res = explode("|",$out);

		if(count($res) == 2 || trim($res[0]) == '405'){
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,'','4',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','4',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$code = trim($res['0']);
			$msg = trim($res['1']);
			$txnId = trim($res['3']);
			$this->General->log_in_vendor_message(array($transId,trim($txnId),'4',$vendor,'15',addslashes($msg),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".trim($txnId)."','4',$vendor,'15','".addslashes($msg)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}

     /*
     * joinrec Bill payment
     */
    
    function joinrecBillPayment($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		$amount = intval($params['amount']);

		$provider = $this->mapping['billPayment'][$params['operator']][$params['type']]['joinrec'];
		$vendor = 48;
		$rcType = '';
		
		$url = JOINREC_RECHARGE_URL;
		$out = $this->General->joinrecApi($url,array('reseller_id'=>JOINREC_ID,'reseller_pass'=>JOINREC_PWD,'mobilenumber'=>$mobileNo,'denomination'=>$amount,'meroid'=>$transId,'voucher'=>$rcType,'operatorid'=>$provider,'circleid'=>'*'));

		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','4',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','4',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt","*Bill Payment*: Input=> $transId<br/>".json_encode($out));

		if(isset($out['Data']['Error'])){
			$status = 'FAILED';
			$description = trim($out['Data']['Error']);
		}
		else {
			$status = trim($out['Data']['Status']);
			$description = trim($out['Data']['Description']);
			if($description == 'NA')$description = "Request accepted";
			$txnId = trim($out['Data']['OrderId']);
		}
		
		
		if($status == 'FAILED'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','4',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'4',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','4',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'4',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
	}
	       
	function cpUtilityBillPayment($transId,$params,$prodId){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;
                
		$extra = "";
		if($prodId == 45 || $prodId == 50){//Reliance & MTNL Delhi
			$extra = $params['param'];
		}
		
		$extra = "ACCOUNT=$extra\r\n";
		$number = $params['accountNumber'];
		$amount = $params['amount'];
		$service_id = 6;
		$vendor = 8;

		$verify_url = $this->cpurls[$prodId]['cpv'];
		$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$number\r\n".$extra."AMOUNT=".floatval($amount)."\r\nAMOUNT_ALL=$amount\r\nCOMMENT=\r\n";
		
		$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
		$out = $this->cpConnect($verify_url,array('inputmessage'=>$res[1]));

		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
		$out = $out['output'];

		$res = ipriv_verify($out, $pub);
		$result = $this->cpArray($res);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Utility Bill Payment*: $verify_url, Input=> ".$data."\n".json_encode($result));
		
		if(isset($result['RESULT']) && $result['RESULT'] == 0 && empty($result['ERROR'])){
			if(isset($result['AUTHCODE']))$operator_id = $result['AUTHCODE'];

			$live_url = $this->cpurls[$prodId]['cpl'];
			$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\nNUMBER=$number\r\n".$extra."AMOUNT=".floatval($amount)."\r\nPAY_TOOL=0\r\n";
			$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
				
			$out = $this->cpConnect($live_url,array('inputmessage'=>$res[1]));
			if(!$out['success']){
				if($out['timeout']){
					$this->Shop->addStatus($transId,$vendor);
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
					return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
				else {
					//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','$service_id',8,'15','Request timed out while recharging','pending','".date("Y-m-d H:i:s")."')");                                        
                                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,'15','Request timed out while recharging','pending',date("Y-m-d H:i:s")));
					return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
				}
			}
			else {
				$out = $out['output'];
			}

			$res = ipriv_verify($out, $pub);
			$result = $this->cpArray($res);
			
			$vendorTransId = (isset($result['TRANSID'])) ? $result['TRANSID'] : 0;
			$opr_id = (isset($result['AUTHCODE'])) ? $result['AUTHCODE'] : 0;
			$trans_status = (isset($result['TRNXSTATUS'])) ? $result['TRNXSTATUS'] : 3;

			if($trans_status != 7){
				$status = $this->cpTranStatus($transId,null,null,$prodId);
			}
			else {
				$status['status'] = 'success';
				$status['transaction_id'] = $vendorTransId;
			}
			$error = $result['ERROR'];
			$errorCode = $this->Shop->errorCodeMapping(8,$error);
			
			if($status['status'] == 'error' || $status['status'] == 'inprocess' || $status['status'] == 'incomplete'){//in process
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'15','".$status['status']."::".$status['errMsg']."(".$status['errCode'].")','pending','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'15',$status['status']."::".$status['errMsg']."(".$status['errCode'].")",'pending',date("Y-m-d H:i:s")));
				return array('status'=>'pending','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'failure'){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'14','".$status['errMsg']."(".$status['errCode'].")','failure','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'14',$status['errMsg']."(".$status['errCode'].")",'failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
			else if($status['status'] == 'success'){
				$this->Shop->addStatus($transId,$vendor);
				$this->Shop->setMemcache("vendor8_last",date('Y-m-d H:i:s'),24*60*60);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$status['transaction_id']."','$service_id',8,'13','','success','".date("Y-m-d H:i:s")."')");                                
                                $this->General->log_in_vendor_message(array($transId,$status['transaction_id'],$service_id,8,'13','','success',date("Y-m-d H:i:s")));
				return array('status'=>'success','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>$status['transaction_id'],'pinRefNo'=>'','operator_id'=>$opr_id);
			}
		}
		else {
			$this->Shop->addStatus($transId,$vendor);
			$error = $result['ERROR'];
			$errorCode = $this->Shop->errorCodeMapping(8,$error);
			//$code = ($result['ERROR'] == 23) ? 5 : (($result['ERROR'] == 7) ? 6 : 42);
			$err = "Error code: ".$result['ERROR'] . "," . $this->cp_errs[$result['ERROR']];
				
			if(isset($result['ADDINFO'])){
				$err .= ":: Info: " . $result['ADDINFO'];
			}
			if(isset($result['DUEDATE'])){
				$err .= ":: DueDate" . $result['DUEDATE'];
			}
			//$this->User->query("insert into vendors_messages(shop_tran_id,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','$service_id',8,'$code','".addslashes($err)."','failure','".date("Y-m-d H:i:s")."')");                       
                        $this->General->log_in_vendor_message(array($transId,'',$service_id,8,$code,addslashes($err),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>$errorCode,'description'=>$this->Shop->errors($errorCode),'tranId'=>'');
		}
	}
    
    /**
     * 
     * @param type $transId
     * @param type $params
     * @param type $prodId
     * @return type
     */
    function smsdaakUtilityBillPayment($transId,$params,$prodId){
        $optional1 = "";
        $optional2 = "";
        
        $account_number = isset($params['accountNumber']) ? $params['accountNumber'] : "";
		$amount = intval(isset($params['amount']) ? $params['amount'] : 0);
		$mobileNo = isset($params['mobileNumber']) ? $params['mobileNumber'] : "";
		$cycle_number = isset($params['param']) ? $params['param'] : "";
        $operator = isset($params['operator']) ? $params['operator'] : "";
        $type = isset($params['type']) ? $params['type'] : "";
        
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Utility Bill Params request *:".  json_encode($params));
        
        /*$option_array = array('45'=>array('account'=>$account_number,'optional1'=>$cycle_number),
        	'46'=>array('account'=>$mobileNo,'optional1'=>''),
        	'47'=>array('account'=>$mobileNo,'optional1'=>''),
        	'48'=>array('account'=>$mobileNo,'optional1'=>''),
            '50'=>array('account'=>$account_number,'optional1'=>$cycle_number)
        );*/
        		
        $option_array = array('45'=>array('account'=>$account_number,'optional1'=>$cycle_number),
        	'46'=>array('account'=>$account_number,'optional1'=>''),
        	'47'=>array('account'=>$account_number,'optional1'=>''),
        	'48'=>array('account'=>$account_number,'optional1'=>''),
            '50'=>array('account'=>$account_number,'optional1'=>$cycle_number)
        );
        // over-riding variable as per option mapping
        $account_number = isset($option_array[$prodId]['account']) ? $option_array[$prodId]['account'] : $account_number;
        $optional1 = isset($option_array[$prodId]['optional1']) ? $option_array[$prodId]['optional1'] : $optional1;
        $optional2 = isset($option_array[$prodId]['optional2']) ? $option_array[$prodId]['optional2'] : $optional2;
        
        $provider = $this->mapping['utilityBillPayment'][$operator][$type]['smsdaak'];
        $vendor = 58;                
        $url = SMSDAAK_RECHARGE_URL;
        //--- request parameter
        $request_param = $validation_param = array('token'=>SMSDAAK_TOKEN,'spkey'=>$provider,'agentid'=>$transId,'account'=>$account_number,'amount'=>$amount,'optional1'=>$optional1,'optional2'=>$optional2,'format'=>'xml');        
        //---validating request        
        $validation_param['mode'] = 'VALIDATE';
        $out = $this->General->smsdaakApi($url,$validation_param);
        $service_id = '6';        
        if(!$out['success']){
            if($out['timeout']){
                $this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
            }
        }
        $out = $out['output'];
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Utility Bill Payment validation request *: Input=> $transId<br/>".json_encode($out)." : input : ".json_encode($validation_param));        
        //-----------handle success / failure of validation hit
        if(strtoupper($out['xml']['ipay_errorcode']) == 'TXN'){
             $out = "";
             $out = $this->General->smsdaakApi($url,$request_param);
             if(!$out['success']){
                if($out['timeout']){
                    $this->Shop->addStatus($transId,$vendor);
					$this->General->log_in_vendor_message(array($transId,'',$service_id,$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
                   // $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','',$service_id,$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
                }
             }
            $out = $out['output'];
            $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/smsdaak.txt","*Utility Bill Payment*: Input=> $transId<br/>".json_encode($out)." : input : ".json_encode($request_param));            
            $txnId = $out['xml']['ipay_id'];
            $opr_id = isset($out['xml']['status'])?$out['xml']['opr_id']:"";
            if(isset($out['xml']['status'])){
				$error = !empty($out['xml']['res_code']) ? $out['xml']['res_code'] : "" ;
				$errCode = $this->Shop->errorCodeMapping($vendor,$error);
                if(strtoupper(trim($out['xml']['status'])) == "SUCCESS"){
                    $this->Shop->addStatus($transId,$vendor);
                    $description = trim($out['xml']['res_msg']);                    
                    if(empty($description))$description = 'success';
                    $this->Shop->setMemcache("vendor$vendor"."_last",date('Y-m-d H:i:s'),24*60*60);
					$this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'13',addslashes($description),'success',date("Y-m-d H:i:s")));
                   /// $this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'13','".addslashes($description)."','success','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'success','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
                }else{
                    //------considered non - success status as pending
					$this->General->log_in_vendor_message(array($transId,$txnId,$service_id,$vendor,'15',json_encode($out),'pending',date("Y-m-d H:i:s")));
                    //$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."',$service_id,$vendor,'15','".json_encode($out)."','pending','".date("Y-m-d H:i:s")."')");
                    return array('status'=>'pending','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId,'pinRefNo'=>'');
                }                
            }            
        }        
        //----------- handle failure of validation and payment hit
        if(isset($out['xml']['ipay_errorcode']) && strtoupper($out['xml']['ipay_errorcode']) != 'TXN'){
            $this->Shop->addStatus($transId,$vendor);
            $description = trim($out['xml']['ipay_errordesc']);
            $txnId = "";
			$error = !empty($out['xml']['ipay_errorcode']) ? $out['xml']['ipay_errorcode'] : "" ;
			$errCode = $this->Shop->errorCodeMapping($vendor,$error);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($description),'failure',date("Y-m-d H:i:s")));
            
            //$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($description)."','failure','".date("Y-m-d H:i:s")."')");
            return array('status'=>'failure','code'=>$errCode,'description'=>$this->Shop->errors($errCode),'tranId'=>$txnId);
        }        
    }
	
	//oss transaction details

	function ossBalance($panel = null,$min = null){
		$rechargesReq = '<GetBalanceRequest><PGCode>'.OSS_PG_CODE.'</PGCode><TranDistCustCode>'.OSS_TRAN_DIST_CUST_CODE.'</TranDistCustCode><TranPassword>'.OSS_TRAN_PASSWORD.'</TranPassword></GetBalanceRequest>';
		$rechargesRes = $this->ossApi('GetBalance',$rechargesReq);
		$status = $rechargesRes['GetBalanceResponse']['ResponseStatus']['Status'];
		if($panel == null){
			if(strtolower($rechargesRes['GetBalanceResponse']['ResponseStatus']['Status']) == 'success'){
				if($rechargesRes['GetBalanceResponse']['GetBalance']['Balance'] < $min){
					$this->General->sendMails("OSS Balance below ".$min,"Current balance: Rs." .$rechargesRes['GetBalanceResponse']['GetBalance']['Balance'],array('limits@mindsarray.com','backend@mindsarray.com'));
                                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'OSS','min'=>$min,'currentbalance'=>$rechargesRes['GetBalanceResponse']['GetBalance']['Balance'])); 
				}
			}
			$this->autoRender = false;
		}
		else {
			$dt = $this->User->query("SELECT timestamp FROM `vendors_activations` WHERE vendor_id =1 AND STATUS =1 AND date = '".date('Y-m-d')."' ORDER BY timestamp DESC LIMIT 1");
			if(empty($dt)){
				return array('balance'=>$rechargesRes['GetBalanceResponse']['GetBalance']['Balance']);
			}
			else {
				return array('balance'=>$rechargesRes['GetBalanceResponse']['GetBalance']['Balance'],'last'=>$dt['0']['vendors_activations']['timestamp']);
			}
		}
	}

	function ppiBalance($panel = null,$min = null){
		$rechargesReq = '';
		$rechargesRes = $this->ppiApi('TradeBalance',$rechargesReq);
		$status = '';
		if($panel == null){
			if($rechargesRes['Response']['balance'] < $min){
				$this->General->sendMails("PPI Balance below ".$min,"Current balance: Rs." .$rechargesRes['Response']['balance'],array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'PPI','min'=>$min,'currentbalance'=>$rechargesRes['Response']['balance'])); 
                                                    }
			$this->autoRender = false;
		}
		else {
			$dt = $this->User->query("SELECT timestamp FROM `vendors_activations` WHERE vendor_id =2 AND STATUS =1 AND date = '".date('Y-m-d')."' ORDER BY timestamp DESC LIMIT 1");
			if(empty($dt)){
				return array('balance'=>$rechargesRes['Response']['balance']);
			}
			else {
				return array('balance'=>$rechargesRes['Response']['balance'],'last'=>$dt['0']['vendors_activations']['timestamp']);
			}
		}
	}

	function paytBalance($panel = null,$min = null,$power=null){
		$date = date('YmdHis');
		$terminal = PAYT_USER_CODE;
		$sha=strtoupper(sha1($terminal.$date.PAYT_PASSWORD));
		$content = "OperationType=3&TerminalId=$terminal&DateTimeStamp=$date&Hash=$sha";

		$Rec_Data = $this->Shop->getMemcache("balance_5");
		
		if($Rec_Data === false || $power == 1){
			$Rec_Data = $this->paytConnection($content);
			if(!$Rec_Data['success']){
				return array('balance'=>'');
			}
	
			$Rec_Data = $Rec_Data['output'];
			$this->Shop->setMemcache("balance_5",$Rec_Data,24*60*60);
		}
		$response = explode("|",$Rec_Data);
		if(trim($response[0]) == 0){
			$balance = trim($response[1]);
			$message = trim($response[2]);
			if($panel == null){
				if($balance < $min){
					$this->General->sendMails("Paytronics Balance below ".$min,"Current balance: Rs." .$balance,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Paytronics Balance below','min'=>$min,'currentbalance'=>$balance)); 
				}
				$this->autoRender = false;
			}
			else {
				//$dt = $this->User->query("SELECT timestamp FROM `vendors_activations` WHERE vendor_id =5 AND STATUS =1 AND date = '".date('Y-m-d')."' ORDER BY timestamp DESC LIMIT 1");
				return array('balance'=>$balance,'last'=>$this->Shop->getMemcache("vendor5_last"));
			}
		}
		else {
			if($panel == null){
				$this->General->sendMails("Not able to get paytronics balance","",array('tadka@mindsarray.com'));
				$this->autoRender = false;
			}
			else {
				return array('balance'=>'');
			}
		}
	}

	function modemBalance($date=null,$vendor=4,$modem_src=true){
		if(empty($date))$date = date('Y-m-d');
		$adm = "query=balance&date=$date";
		$info = $this->Shop->getVendorInfo($vendor);

		$Rec_Data = false;
		if($date == date('Y-m-d'))$Rec_Data = $this->Shop->getMemcache("balance_$vendor");

		if($Rec_Data === false && $info['show_flag'] == 1){
			if($modem_src){
				$Rec_Data = $this->Shop->modemRequest($adm,$vendor,$info);
				$Rec_Data = $Rec_Data['data'];
			}
			else {
				$this->General->logData("modem_test.txt","i m finding data in devices_data now");
				$data = $this->Slaves->query("SELECT * FROM `devices_data` WHERE sync_date = '$date' AND vendor_id ='$vendor'");
				$Rec_Data = array();
				foreach($data as $dt){
					$Rec_Data[] = $dt['devices_data'];
				}
				$Rec_Data = json_encode($Rec_Data);
			}
		}

		if(!empty($Rec_Data)){
			$Rec_Data = json_decode($Rec_Data,true);
			$time = $this->Shop->getMemcache("balance_timestamp_$vendor"."_last");
			$ports = $this->Shop->getMemcache("balance_ports_$vendor");
			if($time !== false){
				$Rec_Data['lasttime'] = $time;
			}
			if($ports !== false){
				$Rec_Data['ports'] = $ports;
			}

			return $Rec_Data;
		}
		else return null;
	}

	function cpBalance($panel = null,$min = null,$power=null){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;
                
		$url = CYBERP_BAL_URL;

		$result = $this->Shop->getMemcache("balance_8");
		if($result === false || $power == 1){
			$transId = "12" . time() . "". rand(0,10000);
			$data = "SD=".CYBER_SD."\r\nAP=".CYBER_AP."\r\nOP=".CYBER_OP."\r\nSESSION=$transId\r\n";
			$res = ipriv_sign($data, $sec, CYBER_PASSWORD);
			$out = $this->cpConnect($url,array('inputmessage'=>$res[1]));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = $out['output'];
			$res = ipriv_verify($out, $pub);
			$result = $this->cpArray($res);
			if(!empty($result))
			$this->Shop->setMemcache("balance_8",$result,24*60*60);
		}
		$status = '';
		if($panel == null){
			if($result['REST'] < $min){
				$this->General->sendMails("Cyberplate Balance below ".$min,"Current balance: Rs." .$result['REST'],array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                           $this->sendApibalanceLowAlert(array('apiname'=>'Cyberplate(CP)','min'=>$min,'currentbalance'=>$result['REST'])); 
                                
                                                        }
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$result['REST'],'last'=>$this->Shop->getMemcache("vendor8_last"));
		}
		
	}


	function cbzBalance($panel = null,$min = null){
		$url = CBZ_BAL_URL;
		$out = $this->General->cbzApi($url,array('username'=>CBZ_REC_USERNAME,'password'=>CBZ_REC_PASSWORD,'key'=>CBZ_REC_KEY,'cmd'=>'balance'));

		if(!$out['success']){
			return array('balance'=>'');
		}
		else {
			$out = $out['output'];
			$result = $this->General->xml2array($out);
				
			$status = '';
			if($panel == null){
				if($result['root']['balance'] < $min){
					$this->General->sendMails("Cellbiz Balance below ".$min,"Current balance: Rs." .$result['root']['balance'],array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Cellbiz Balance below','min'=>$min,'currentbalance'=>$result['root']['balance'])); 
				}
				$this->autoRender = false;
			}
			else {
				return array('balance'=>$result['root']['balance'],'last'=>$this->Shop->getMemcache("vendor11_last"));
				/*$this->Shop->getMemcache("vendor11_last")
				 $dt = $this->User->query("SELECT timestamp FROM `vendors_activations` WHERE vendor_id =11 AND STATUS =1 AND date = '".date('Y-m-d')."' ORDER BY timestamp DESC LIMIT 1");
				 if(empty($dt)){
					return array('balance'=>$result['root']['balance']);
					}
					else {
					return array('balance'=>$result['root']['balance'],'last'=>$dt['0']['vendors_activations']['timestamp']);
					}*/
			}
		}
	}

	function rduBalance($panel = null,$min = null){
		$url = RDU_BAL_URL;
		$out = $this->General->rduApi($url,array('userId'=>RDU_REC_USERNAME,'pwd'=>RDU_REC_PASSWORD));

		if(!$out['success']){
			return array('balance'=>'');
		}
		else {
			$out = trim($out['output']);
				
			$status = '';
			if($panel == null){
				if($out < $min){
					$this->General->sendMails("Rduniya Balance below ".$min,"Current balance: Rs." .$out,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Rduniya','min'=>$min,'currentbalance'=>$out)); 
				}
				$this->autoRender = false;
			}
			else {
				return array('balance'=>$out,'last'=>$this->Shop->getMemcache("vendor16_last"));
			}
		}
	}

	function uvaBalance($panel = null,$min = null,$power = null){
		
		$out = $this->Shop->getMemcache("balance_18");
		if($out === false  || $power == 1){
			$url = UVA_BAL_URL;
			$out = $this->General->uvaApi($url,array('username'=>UVA_REC_USERID,'uniqueid'=>UVA_REC_UNIQID));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = explode(":",trim($out['output']));
			$this->Shop->setMemcache("balance_18",$out,24*60*60);
		}
		$bal = $out[1];
			
		$status = '';
		if($panel == null){
			if($out < $min){
				$this->General->sendMails("UvaPoint Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                            $this->sendApibalanceLowAlert(array('apiname'=>'UvaPoint','min'=>$min,'currentbalance'=>$bal));  
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor18_last"));
		}
		
	}

	function uniBalance($panel = null,$min = null,$power=null){
		
		$out = $this->Shop->getMemcache("balance_19");
		if($out === false  || $power == 1){
			$url = UNI_BAL_URL;
			$out = $this->General->uniApi($url,array('cname'=>UNI_CNAME,'cmob'=>UNI_MNUMBER));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = explode("|",trim($out['output']));
			$this->Shop->setMemcache("balance_19",$out,24*60*60);
		}
		$bal = trim($out[1]);
			
		$status = '';
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("UniServices Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                            $this->sendApibalanceLowAlert(array('apiname'=>'Uniservices','min'=>$min,'currentbalance'=>$bal));  
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor19_last"));
		}
		
	}

	function anandBalance($panel = null,$min = null){
		$url = ANAND_RECHARGE_URL;
		$out = $this->General->anandApi($url,array('Mob'=>ANAND_MOB,'message'=>"bal ".ANAND_PIN,'source'=>'API'));

		if(!$out['success']){
			return array('balance'=>'');
		}
		else {
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/anand.txt",date('Y-m-d H:i:s').":Balance Check: $out");
				
			$info = $this->General->matchTemplate($out,"Your Balance is @__balance__@");
			$vars = $info['vars'];
			$bal = $vars['balance'];
				
			$status = '';
			if($panel == null){
				if($bal < $min){
					$this->General->sendMails("AnandApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Anandapi','min'=>$min,'currentbalance'=>$bal)); 
				}
				$this->autoRender = false;
			}
			else {
				return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor9_last"));
			}
		}
	}

	function apnaBalance($panel = null,$min = null){
		$url = APNA_BAL_URL;
		$out = $this->General->apnaApi($url,array('username'=>APNA_USERNAME,'pwd'=>APNA_PASSWD));
		$vendor_id = 23;

		if(!$out['success']){
			return array('balance'=>'');
		}
		else {
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/apna.txt",date('Y-m-d H:i:s').":Balance Check: $out");
				
			$bal = trim($out);
				
			if($panel == null){
				if($bal < $min){
					$this->General->sendMails("ApnaApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                                               $this->sendApibalanceLowAlert(array('apiname'=>'Paytronics Balance below','min'=>$min,'currentbalance'=>$bal));   
				}
				$this->autoRender = false;
			}
			else {
				return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			}
		}
	}

	function magicBalance($panel = null,$min = null,$power = null){
		$vendor_id = 24;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
                        $url = MAGIC_BAL_URL;
			$out = $this->General->magicApi($url,array('uid'=>MAGIC_USERNAME,'pwd'=>MAGIC_PASSWD));
			
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/magic.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("MagicApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                            $this->sendApibalanceLowAlert(array('apiname'=>'MagicApi','min'=>$min,'currentbalance'=>$bal));  
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}

	function rioBalance($panel = null,$min = null,$power = null){
		$vendor_id = 36;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = RIO_BAL_URL;
			$out = $this->General->rioApi($url,array('uid'=>RIO_USERNAME,'pwd'=>RIO_PASSWD));
			
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("Recharge India Online Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Recharge India Online API','min'=>$min,'currentbalance'=>$bal)); 
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
    function rio2Balance($panel = null,$min = null,$power = null){
		$vendor_id = 62;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = RIO2_BAL_URL;
			$out = $this->General->rioApi($url,array('uid'=>RIO2_USERNAME,'pwd'=>RIO2_PASSWD));
			
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio2.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("Recharge India Online Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                           $this->sendApibalanceLowAlert(array('apiname'=>'Recharge India Online','min'=>$min,'currentbalance'=>$bal));   
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
    
	function infogemBalance($panel = null,$min = null, $power=null){
		$vendor_id = 27;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		if($out === false || $power == 1){
			$terminal = GEM_USERNAME;
			$sha=strtoupper(sha1($terminal.GEM_PASSWORD));
	        
			$url = GEM_BAL_URL;
			$out = $this->General->gemApi($url,array('PartnerId'=>$terminal,'Hash'=>$sha));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gem.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		
		$bal = intval(trim($out));
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("GemApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                            $this->sendApibalanceLowAlert(array('apiname'=>'Genapi','min'=>$min,'currentbalance'=>$bal));      
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}

	function durgaBalance($panel = null,$min = null,$power = null){
		$vendor_id = 30;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = DURGA_BAL_URL;
			$out = $this->General->durgaApi($url,array('cid'=>DURGA_UID,'mob'=>DURGA_MNUMBER));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = explode("|",trim($out['output']));
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		$bal = trim($out[1]);
			
		$status = '';
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("Durgawati Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                         $this->sendApibalanceLowAlert(array('apiname'=>'Durgawati','min'=>$min,'currentbalance'=>$bal)); 
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
	function rkitBalance($panel = null,$min = null,$power = null){
		$vendor_id = 34;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
         
		 
		if($out === false || $power == 1){
			///echo "inside if";
			$url = RKIT_BAL_URL;
			$out = $this->General->rkitApi($url,array('USERID'=>RKIT_USER, 'PASSWORD' => RKIT_AUTH));
			
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			//$out = explode('#',trim($out['output']));
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rkit.txt",date('Y-m-d H:i:s').":Balance Check: ".$out['output']['NODE']['BALANCE']);
                        
			$this->Shop->setMemcache("balance_".$vendor_id,$out['output']['NODE']['BALANCE'],24*60*60);
	     }
	

	    $bal =  (is_array($out)) ? $out['output']['NODE']['BALANCE'] : $out ;
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("RKITApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
				$this->sendApibalanceLowAlert(array('apiname'=>'RKIT api','min'=>$min,'currentbalance'=>$bal));
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
	}
	
	function a2zBalance($panel = null,$min = null,$power = null){
		$vendor_id = 47;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		                
		if($out === false || $power == 1){
			$url = A2Z_BAL_URL;
			$out = $this->General->a2zApi($url,array('USERID'=>A2Z_AGTCODE,'PASSWORD'=>A2Z_AUTH));
			
			
			if(!$out['success']){
				return array('balance'=>'');
			}
				
			$out = explode('#',$out['output']);
                        
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/a2z.txt",date('Y-m-d H:i:s').":Balance Check: $out[1]");
			$this->Shop->setMemcache("balance_".$vendor_id,$out[1],24*60*60);
                        $out = $out[1];
		}

		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("A2ZApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
				$this->sendApibalanceLowAlert(array('apiname'=>'A2Z ','min'=>$min,'currentbalance'=>$bal));
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}

	}
	
	function joinrecBalance($panel = null,$min = null,$power = null){

		$vendor_id = 48;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);

		if($out === false || $power == 1){
			$url = JOINREC_BAL_URL;
			$out = $this->General->joinrecApi($url,array('reseller_id'=>JOINREC_ID,'reseller_pass'=>JOINREC_PWD));
				
			if(!$out['success']){
				return array('balance'=>'');
			}
				
			$out = $out['output'];
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt",date('Y-m-d H:i:s').":Balance Check: ".json_encode($out));

			if(isset($out['Data']['Error'])){

			}
			else {
				$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
				$this->sendApibalanceLowAlert(array('apiname'=>'JOINREC','min'=>$min,'currentbalance'=>trim($out['Data']['Balance'])));
			}
		}

		$bal = trim($out['Data']['Balance']);

		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("JOINRECApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}

	}
	
	function gitechBalance($panel = null,$min = null, $power = null){
		$vendor_id = 35;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$out = $this->General->gitechApi("CheckQuota",'','gitechBalance',array('0'=>$panel,'1'=>-1));
		
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gitech.txt",date('Y-m-d H:i:s').":Balance Check: ".json_encode($out));
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
		
		if(isset($out['balance'])) $bal = $out['balance'];
		else $bal = $out['GetBalanceResponse']['REMAININGAMOUNT'];
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("Gitech Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                                                             $this->sendApibalanceLowAlert(array('apiname'=>'Gitech','min'=>$min,'currentbalance'=>$bal)); 
			}
			$this->autoRender = false;
		}
		else {
			$ret = array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			
			if($min == -1){
				echo json_encode($ret);
				$this->autoRender = false;
			}
			else return $ret;
		}
	}

	function mypayBalance($panel = null,$min = null,$power = null){
		$vendor_id = '57';
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		 
		if($out === false || $power == 1){
                        $url = MYPAYURL;
			$pwd = MYPAYPASSWD;
            
			$req_str = $pwd."|bal";
			 
			$out = $this->General->mypayApi($url,array('_prcsr'=>MYPAYUSER,'_urlenc'=>$req_str));
			 
			 
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/mypay.txt",date('Y-m-d H:i:s').": Check: ".json_encode($out));
			 
			if(!$out['success']){
				return array('balance'=>'');
			}
			$out = $out['output'];
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}

		$bal = isset($out['_ApiResponse']['availableBalance'])?($out['_ApiResponse']['availableBalance']):"";
		 
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("mypayApi Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
				$this->sendApibalanceLowAlert(array('apiname'=>'Mypay ','min'=>$min,'currentbalance'=>$bal));
			}
			$this->autoRender = false;
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}

	}
    
	function vendorMonitoring(){
		
		$data = $this->Slaves->query("SELECT shortForm,balance FROM `vendors` WHERE update_flag = 0 AND balance > 0");

		foreach($data as $dt){
			$funName = $dt['vendors']['shortForm'] . "Balance";
			$this->$funName(null,$dt['vendors']['balance']);
		}

		$this->autoRender = false;
	}

	/*function lastModemTransactions($vendor,$device,$page,$limit=null,$date=null){
		$adm = "query=last&device=$device&page=$page&limit=$limit&date=$date";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
		$Rec_Data = $Rec_Data['data'];
		$this->set('device',$device);
		$this->set('page',$page);
		$this->set('data',json_decode($Rec_Data,true));
		$this->render('last_transactions');
		$this->autoRender = false;
		}*/
	function lastModemTransactions($vendor,$device,$page,$limit=null,$date=null){
            
		$pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];

		$adm = "query=last&device=$device&page=$page&limit=$limit&date=$date&vendor=$vendor";
                   if($this->isUpfromlast5min($vendor,$date)):
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor,null,120);
		$Rec_Data = $Rec_Data['data'];
		$this->set('device',$device);
		$this->set('page',$page);
		$data = json_decode($Rec_Data,true);
			
		$this->set('pageType',$pageType);
		if($pageType == 'csv'){
			App::import('Helper','csv');
			$this->layout = null;
			$this->autoLayout = false;
			$csv = new CsvHelper();
			$line = array("Sr. No", 	"Mobile/Sub Id", 	"Amount", 	"Ref Id", 	"Status", 	"Incentive", 	"Trials", 	"Cause", 	"SIM Balance", 	"SMS Received", 	"Added at", 	"Processed at", 	"Status updated at");
			$csv->addRow($line);
			$i=1;
			foreach($data as $md){

				$status = "";
				if($md['status'] == 0){
					$status= "In Process";
				}
				else if($md['status'] == 1){
					$status= "Successful";
				}
				else {
					$status= "Failed";
				}
				$temp = array($i,$md['mobile']."/".$md['param'],$md['amount'],$md['vendor_refid'],$status,$md['incentive'],$md['trials'],$md['cause'],$md['sim_balance'],$md['message'],$md['timestamp'],$md['processing_time'],$md['updated']);
				$csv->addRow($temp);
				$i++;
			}

			echo $csv->render("lastTransactions_device=".$device."_page=".$page."_limit=".$limit."_date=$date".".csv");

		}else{
			$this->set('data',$data);
		}
                        
		$this->render('last_transactions');
		$this->autoRender = false;
                
                else:
                                echo "FROM BACKUP";echo "<br/>";
                                $adm.="&vendor_id=$vendor";
                                echo MODEM_BACKUP_SERVER_URL."?$adm";
                                $data=file_get_contents(MODEM_BACKUP_SERVER_URL."?$adm");
                                $data = json_decode($data,true);
                                 $this->set('device',$device);
                                $this->set('page',$page);
                                $this->set('data',$data);
                                $this->set('pageType',$pageType);
                                $this->render('last_transactions');
                                 $this->autoRender = false;
                endif;
	}

	function lastModemSMSes($vendor,$device,$page,$limit=null,$date=null){
            
                    // Check if last sync timestamp of modem
                     if($this->isUpfromlast5min($vendor,$date)):
		$adm = "query=sms&device=$device&page=$page&limit=$limit&date=$date";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
		$Rec_Data = $Rec_Data['data'];
		$this->printArray(json_decode($Rec_Data,true));
		$this->autoRender = false;
                    else:
                    // call lalits api       
                                 echo "FROM BACKUP";echo "<br/>";
                                 $adm = "query=sms&device=$device&page=$page&limit=$limit&date=$date&vendor_id=$vendor";
                                 echo MODEM_BACKUP_SERVER_URL."?$adm";
                                 echo "<br/>";
                                 $data=file_get_contents(MODEM_BACKUP_SERVER_URL."?$adm");
                                 $this->printArray(json_decode($data,true));
                                 $this->autoRender = false;
                    
                        
                    endif;
	}
        
                    function isUpfromlast5min($vendor,$date=null)
                    {
     
                          $lasttimestamp=$this->Shop->getMemcache("balance_timestamp_$vendor"."_last");
                     //   echo "<br/>";

                        if(strtotime($lasttimestamp)>strtotime('-5 minutes')):
                                     return true;
                        endif;

                           return false;
                    }
		
	function allBalance($modem_id = 0,$date=null,$last=0){
		
		$office_ips = explode(",",OFFICE_IPS);
		
		if(in_array($_SERVER['REMOTE_ADDR'],$office_ips) || $_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['id'] == 1 || $_SESSION['Auth']['User']['group_id'] == 9){
				
		}
		else $this->redirect('/shops/view');

		$data = $this->Shop->getVendors();
		foreach($data as $dt){
			if($dt['vendors']['user_id'] == $_SESSION['Auth']['User']['id'])
				$check = $dt;
		}
		//$check = $this->User->query("SELECT * FROM `vendors` WHERE user_id = ".$_SESSION['Auth']['User']['id']);
		if($modem_id != '12323'){
			if((empty($check) && $_SESSION['Auth']['User']['group_id'] == 9) || (!empty($check) && $check['vendors']['id'] != $modem_id)){
				echo "Invalid access"; exit;
			}
		}
		
		if($modem_id == '12323' || empty($date))$date = date('Y-m-d');
		$this->set('date',$date);

		$ips = array();
		$modems = array();
		$map = array();

		foreach($data as $dt){
			$inactive = $this->Shop->getInactiveVendors();
				
			if($dt['vendors']['update_flag'] == 1){
				
				if($modem_id != 0 && $modem_id != '12323' && $dt['vendors']['id'] != $modem_id) continue;
				
				$modem_bal = $this->modemBalance($date,$dt['vendors']['id'],false);
				if(empty($modem_bal)) continue;
				
				$modem_bal['inactive'] = 0;
				if(in_array($dt['vendors']['id'],$inactive)) $modem_bal['inactive'] = 1;
				
				$modems[$dt['vendors']['id']] = $modem_bal;
				$ips[$dt['vendors']['id']] = $dt['vendors']['ip'].":".$dt['vendors']['port'];
				$map[$dt['vendors']['id']] = $dt['vendors']['company'];
			}
		}
       
		$vendors = $this->Slaves->query("SELECT * FROM `vendors` where update_flag = 0 AND show_flag = 1");
		$balances = array();
		$total = 0;
		$modem_bals = array();
		if(empty($vendors))$vendors = array();
		foreach($vendors as $vend){
			//if($vend['vendors']['update_flag'] == 0 && $vend['vendors']['active_flag'] == 1){
				$id = $vend['vendors']['id'];
				if($modem_id != 0 && $modem_id != '12323' && $id != $modem_id) continue;
				
				//$id = $vend['vendors']['id'];
				$name = $vend['vendors']['shortForm'];
			    $method = $name . "Balance";
               
				if(method_exists($this, $method)){
					$balances[$name] = $this->$method(1);
					$total += $balances[$name]['balance'];
					$modem_bals[$id] = $balances[$name]['balance'];
				}
			//}
		} 
        
		if($modem_id == '12323'){
			set_time_limit(0);
			ini_set("memory_limit","-1");
			//$this->General->logData($_SERVER['DOCUMENT_ROOT']."/log.txt",date('Y-m-d H:i:s').":All balance first line");
				
			$body = "";
			//$this->General->logData($_SERVER['DOCUMENT_ROOT']."/log.txt",date('Y-m-d H:i:s').":".json_encode($modem_bals));
				
			foreach($modems as $key=>$modemc){
				$body .= "<br/>" . $map[$key] . " Balances:";
				if(isset($modemc['lasttime']))$body .= " (".$modemc['lasttime'].")<br/>";
				$modem_bal = 0;
				$opening =0; $closing=0; $tfr = 0; $sale =0; $diff_tot = 0;$inc=0;
				$body .= "<table border=1>";
				$body .= "<tr>
							<th>Device Id/Port</th>
							<th>Signal</th>
							<th>Vendor</th>
							<th>Operator</th>
							<th>Number</th>
							<th>Margin</th>
		  					<th>Curr Bal</th>
		  					<th>Opening</th>
		  		    		<th>Closing</th>
		  		    		<th>Incoming</th>
		  		    		<th>Sale</th>
		  		    		<th>Inc</th>
		  		    		<th>Diff</th>
		  		    		<th>Action</th>
		  				</tr>";
				foreach($modemc as $md){
					if($md['active_flag'] == 0) $color = '#99ff99';
					else $color = '#008000';
					$body .= "<tr id='device".$md['id']."' style='bgcolor:$color'>";
					$body .= "<td>".$md['id']."/".$md['device_num']."</td>";
					$body .= "<td>".$md['signal']."</td>";
					$body .= "<td>".$md['vendor_tag']."/".$md['vendor']."</td>";
					$body .= "<td>".$md['operator']."</td>";
					$body .= "<td>".$md['mobile']."</td>";
					$body .= "<td>".$md['commission']."%</td>";
					$body .= "<td>".$md['balance']."</td>";
					$body .= "<td>".$md['opening']."</td>";
					$body .= "<td>".$md['closing']."</td>";
					$body .= "<td>".$md['tfr']."</td>";
					$body .= "<td>".$md['sale']."</td>";
					$body .= "<td>".intval($md['inc'])."</td>";

					$opening += $md['opening'];
					$closing += $md['closing'];
					$inc += intval($md['inc']);
					$tfr += $md['tfr'];
					$sale += $md['sale'];

					if($date != date('Y-m-d')){
						$diff = $md['sale'] - ($md['opening'] + $md['tfr'] -  $md['closing']);
					}
					else {
						$diff = $md['sale'] - ($md['opening'] + $md['tfr'] -  $md['balance']);
					}

					$diff = $diff - $md['inc'];
					$diff_tot += $diff;
					$body .= "<td>$diff</td>";
					$body .= "<td><a href='javascript:void(0)' onclick='resetDevice(".$md['id'].")'>Reset</a></td>";

					$modem_bal += $md['balance'];
					$body .= "</tr>";
				}

				$body .= "<tr><td></td><td></td><td></td><td></td><td></td><td><b>Total<b/></td><td><b>$modem_bal<b/></td><td><b>$opening<b/></td><td><b>$closing<b/></td><td><b>$tfr<b/></td><td><b>$sale<b/></td><td><b>$inc<b/></td><td><b>$diff_tot<b/></td></tr>";
				$body .= "</table>";
				$body .= "<b>Total ". $map[$key]." Balance: $modem_bal (".($tfr + $diff_tot).")<br/>";
				$total += $modem_bal;
				$modem_bals[$key] = $modem_bal;
			}
				
				
			foreach($balances as $vend=>$bal){
				$body .= "<br/><br/>".strtoupper($vend)." Balance: ".$bal['balance'] ." (" . $bal['last'] . ")";
			}
			$body .= "<br/><br/>Total Balance: $total";


			$last_date = date('Y-m-d',strtotime("- $last days"));
			$days = 4;
				
			$comm = $this->Slaves->query("SELECT sum(vendors_activations.amount) as sale,sum(vendors_activations.amount*vendors_activations.discount_commission/100) as expected, vendors_activations.vendor_id, vendors_activations.date, earnings_logs.opening,earnings_logs.closing FROM vendors_activations left join earnings_logs ON (earnings_logs.vendor_id = vendors_activations.vendor_id AND earnings_logs.date = vendors_activations.date) WHERE vendors_activations.product_id != 44 AND vendors_activations.status != 2 AND vendors_activations.status != 3 AND vendors_activations.date >= '".date('Y-m-d',strtotime('-'.$days. ' days'))."' AND vendors_activations.date <= '$last_date' group by vendors_activations.vendor_id,vendors_activations.date");

			$reversals = $this->Slaves->query("SELECT SUM( shop_transactions.amount ) AS reversal, vendors_activations.vendor_id
FROM vendors_activations
INNER JOIN shop_transactions ON ( vendors_activations.shop_transaction_id = shop_transactions.ref2_id
AND shop_transactions.type =11
AND shop_transactions.type_flag =1 )
WHERE shop_transactions.date = '$last_date' AND vendors_activations.product_id != 44
AND vendors_activations.date >= '".date('Y-m-d',strtotime("- 4 days"))."'
GROUP BY vendors_activations.vendor_id");
			
			$data = array();
			foreach($comm as $com){
				$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['sale'] = $com['0']['sale'];
				$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['expected'] = $com['0']['expected'];
				$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['opening'] = $com['earnings_logs']['opening'];
				$data[$com['vendors_activations']['vendor_id']][$com['vendors_activations']['date']]['closing'] = $com['earnings_logs']['closing'];
			}
			
			foreach($reversals as $revs){
				$data[$revs['vendors_activations']['vendor_id']][$last_date]['reversal'] = $revs['0']['reversal'];
			}
				
			foreach($modem_bals as $key=>$bal){
				if(!isset($data[$key][$last_date])){
					$data[$key][$last_date]['sale'] = 0;
					$data[$key][$last_date]['expected'] = 0;
				}
				$data[$key][$last_date]['closing'] = $bal;
			}
				
			foreach($data as $key=>$dt){
				foreach($dt as $date=>$vals){
					if($date == $last_date){
						if($this->User->query("UPDATE earnings_logs SET closing='".$vals['closing']."', sale='".$vals['sale']."',old_reversal='".$vals['reversal']."',expected_earning='".$vals['expected']."' WHERE vendor_id = $key AND date = '$last_date'")){
							
						}
						else {
							$this->User->query("INSERT INTO earnings_logs (closing,sale,expected_earning,old_reversal,vendor_id,date) VALUES ('".$vals['closing']."','".$vals['sale']."','".$vals['expected']."','".$vals['reversal']."',$key,'".$last_date."')");	
						}
						$this->User->query("INSERT INTO earnings_logs (opening,vendor_id,date) VALUES ('".$vals['closing']."',$key,'".date('Y-m-d',strtotime($last_date .' +1 days'))."')");
					}
					else if(empty($vals['opening'])){
						$this->User->query("INSERT INTO earnings_logs (closing,sale,vendor_id,expected_earning,old_reversal,date) VALUES ('".$vals['closing']."','".$vals['sale']."',$key,'".$vals['expected']."','".$vals['reversal']."','$date')");
					}
					else {
						//$this->User->query("UPDATE earnings_logs SET closing='".$vals['closing']."' WHERE vendor_id = $key AND date = '$date'");
						$this->User->query("UPDATE earnings_logs SET closing='".$vals['closing']."', sale='".$vals['sale']."',expected_earning='".$vals['expected']."',old_reversal='".$vals['reversal']."' WHERE vendor_id = $key AND date = '$date'");
					}
					
					
					/*if($date < $last_date){
						$this->User->query("UPDATE earnings_logs SET sale='".$vals['sale']."' WHERE vendor_id = $key AND date = '$date'");
					}*/
				}
			}
				
			/*foreach($comm as $com){
				if($com['vendors_activations']['date'] == $last_date){
				$last = $this->User->query("SELECT opening,invested FROM earnings_logs WHERE vendor_id = ".$com['vendors_activations']['vendor_id']." AND date = '$last_date'");
				$expected = $last['0']['earnings_logs']['opening'] + $last['0']['earnings_logs']['invested'] + $com['0']['expected'] - $com['0']['sale'];
				$vendor = $com['vendors_activations']['vendor_id'];
				if(empty($modem_bals[$vendor]))$closing = $expected; else $closing = $modem_bals[$vendor];
				$this->User->query("UPDATE earnings_logs SET closing=$closing, sale=".$com['0']['sale'].",expected_earning=".$com['0']['expected']." WHERE vendor_id = ".$com['vendors_activations']['vendor_id']." AND date = '$last_date'");
				$this->User->query("INSERT INTO earnings_logs (opening,vendor_id,date) VALUES ($closing,".$com['vendors_activations']['vendor_id'].",'".date('Y-m-d',strtotime('+1 days'))."')");
				}
				else {
				$this->User->query("UPDATE earnings_logs SET sale=".$com['0']['sale'].",expected_earning=".$com['0']['expected']." WHERE vendor_id = ".$com['vendors_activations']['vendor_id']." AND date = '".$com['vendors_activations']['date']."'");
				}
				}*/

			$this->General->sendMails('Closing Balance',$body,array('backend@mindsarray.com','vinit@mindsarray.com'),'mail');
			$this->autoRender = false;
		}
		else {
			if($_SESSION['Auth']['User']['group_id'] != 9){
				$data = $this->Slaves->query("SELECT count(vendors_activations.id) as ids, vendors_activations.vendor_id,vendors_activations.product_id,products.name,vendors.shortForm,sum(if(vendors_activations.status !=2 AND vendors_activations.status !=3,1,0)) as success,sum(if(vendors_activations.status =2 OR vendors_activations.status =3,1,0)) as failure,vendors_commissions.active,vendors.update_flag FROM `vendors_activations`,products,vendors,vendors_commissions WHERE vendors_commissions.vendor_id= vendors.id AND vendors_commissions.product_id = products.id AND products.id = vendors_activations.product_id AND vendors.id = vendors_activations.vendor_id AND vendors_activations.date = '".date('Y-m-d')."' AND vendors_activations.timestamp >= '".date('Y-m-d H:i:s',strtotime('-30 minutes'))."' group by vendors_activations.product_id,vendors_activations.vendor_id order by vendors_activations.product_id");
				if(empty($data))$data = array();
				
				$prods = array();
				foreach($data as $dt){
					if(empty($prods[$dt['vendors_activations']['product_id']])){
						$prods[$dt['vendors_activations']['product_id']]['max'] = $dt['vendors_activations']['vendor_id'];
						$prods[$dt['vendors_activations']['product_id']]['total'] = $dt['0']['ids'];
						$prods[$dt['vendors_activations']['product_id']]['count'] = $dt['0']['success'];
						$prods[$dt['vendors_activations']['product_id']]['vendor'] = $dt['vendors']['shortForm'];
						$prods[$dt['vendors_activations']['product_id']]['failure'] = $dt['0']['failure'];
						$prods[$dt['vendors_activations']['product_id']]['active'] = $dt['vendors_commissions']['active'];
						$prods[$dt['vendors_activations']['product_id']]['modem_flag'] = $dt['vendors']['update_flag'];
					}
					else {
						if($prods[$dt['vendors_activations']['product_id']]['count'] < $dt['0']['ids']){
							$prods[$dt['vendors_activations']['product_id']]['max'] = $dt['vendors_activations']['vendor_id'];
							$prods[$dt['vendors_activations']['product_id']]['count'] = $dt['0']['success'];
							$prods[$dt['vendors_activations']['product_id']]['vendor'] = $dt['vendors']['shortForm'];
							$prods[$dt['vendors_activations']['product_id']]['modem_flag'] = $dt['vendors']['update_flag'];
							$prods[$dt['vendors_activations']['product_id']]['active'] = $dt['vendors_commissions']['active'];
							//$prods[$dt['vendors_activations']['product_id']]['failure'] = $dt['0']['failure'];
						}
						$prods[$dt['vendors_activations']['product_id']]['total'] = $prods[$dt['vendors_activations']['product_id']]['total'] + $dt['0']['ids'];
						$prods[$dt['vendors_activations']['product_id']]['failure'] = $prods[$dt['vendors_activations']['product_id']]['failure'] + $dt['0']['failure'];
					}
					$prods[$dt['vendors_activations']['product_id']]['name'] = $dt['products']['name'];

					$data1 = $this->Shop->getVendors();
					$last_array = array();
					foreach($data1 as $dt){
						$time = $this->Shop->getMemcache("vendor".$dt['vendors']['id']."_last");
						if(!empty($time)){
							$last_array[] = array('shortForm'=>$dt['vendors']['shortForm'],'timestamp'=>$time);
						}
					}
					//$data1 = $this->User->query("SELECT max(timestamp) as timestamp,vendors.shortForm FROM vendors left join `vendors_activations` ON (vendors_activations.vendor_id= vendors.id AND vendors_activations.date = '".date('Y-m-d')."') WHERE vendors.active_flag = 1 AND vendors_activations.status = 1 group by vendors.id");
					$this->set('last',$last_array);
				}

				$array = array();
				$data = $this->Shop->getProducts();

				foreach($data as $prod){
					if($prod['products']['monitor'] == 1){
						if(!isset($prods[$prod['products']['id']])){
							$array[$prod['products']['id']]['vendor'] = 0;
							$array[$prod['products']['id']]['count'] = 0;
							$array[$prod['products']['id']]['total'] = 1;
							$array[$prod['products']['id']]['name'] = $prod['products']['name'];
							$array[$prod['products']['id']]['failure'] = 0;
						}
						else if($prods[$prod['products']['id']]['modem_flag'] != 1 || $prods[$prod['products']['id']]['count']*100/$prods[$prod['products']['id']]['total'] < 60 || $prods[$prod['products']['id']]['failure']*100/$prods[$prod['products']['id']]['total'] > 20){
							$array[$prod['products']['id']] = $prods[$prod['products']['id']];
						}
					}
				}
				$this->set('prods',$array);
					
			}

			$this->set('balances',$balances);
			$this->set('modems',$modems);
			$this->set('map',$map);
			$this->set('ips',$ips);

			$modemRequests = $this->Slaves->query("SELECT * FROM modem_request_log order by created desc limit 0 , 100");
			$this->set('modemRequests',$modemRequests);
		}
	}

	function resetModemDevice(){
		$device_id = $_REQUEST['device'];
		$vendor = $_REQUEST['vendor'];
		$adm = "query=reset&device=$device_id";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
		$this->autoRender = false;
	}

	function stopModemDevice(){
		$device_id = $_REQUEST['device'];
		$stop = $_REQUEST['stop'];
		$vendor = $_REQUEST['vendor'];

		$adm = "query=stop&device=$device_id&stop=$stop";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);
		$Rec_Data = $Rec_Data['data'];
		echo $Rec_Data;
		$this->autoRender = false;
	}

	//actual API calls
	function ossApi($method,$requestXML){
		App::import('vendor', 'soap', array('file' => 'soaplib/nusoap.php'));
		$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
		$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
		$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
		$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
		$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
		$namespace  = OSS_NAMESPACE_URL;
		$client = new nusoap_client(OSS_SOAP_URL, false,$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$client->setUseCurl($useCURL);
		$err = $client->getError();

		$params = "<requestXML>".htmlspecialchars($requestXML)."</requestXML>";
		//$params = "<strRequestXML>&lt;VerifyMobile&gt;&lt;MobileNo&gt;9099959156&lt;/MobileNo&gt;&lt;/VerifyMobile&gt;</strRequestXML>";
		$headers = '<AuthHeader xmlns="'.OSS_NAMESPACE_URL.'"><InterfaceCode>'.OSS_INTERFACE_CODE.'</InterfaceCode><InterfaceAuthKey>'.OSS_INTERFACE_AUTH_KEY.'</InterfaceAuthKey></AuthHeader>';

		$res =  $client->call($method, $params,OSS_NAMESPACE_URL,$namespace.''.$method,$headers);
		/*echo '<h2>Request</h2>';
		 echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		 echo '<h2>Response</h2>';
		 echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		 */

		return $this->General->xml2array($res);
	}

	function ppiApi($method=null,$requestXML=null){
		if(SITE_NAME != ACTIVESTORE_URL){
			$url = PPI_RECHARGE_URL;

			$adm = "method=".urlencode($method)."&requestXML=".urlencode($requestXML);
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$Rec_Data = trim(curl_exec($ch));
			curl_close($ch);

			return json_decode($Rec_Data,true);
		}
		else {
			$method = $_REQUEST['method'];
			$requestXML = $_REQUEST['requestXML'];
			$this->ppiApi1($method,$requestXML);
		}
	}

	function ppiApi1($method,$requestXML){
		App::import('vendor', 'soap', array('file' => 'soaplib/nusoap.php'));
		$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
		$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
		$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
		$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
		$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
		$namespace  = PPI_NAMESPACE_URL;
		$client = new nusoap_client(PPI_SOAP_URL, false,$proxyhost, $proxyport, $proxyusername, $proxypassword);

		$client->setUseCurl($useCURL);
		$err = $client->getError();
		$headers = '';
		$request = '<userCode>'.PPI_USER_CODE.'</userCode><password>'.PPI_PASSWORD.'</password><authCode>'.PPI_AUTH_CODE.'</authCode>'.$requestXML;
		$res =  $client->call($method, $request,PPI_NAMESPACE_URL,$namespace.''.$method,$headers);

		/*echo '<h2>Request</h2>';
		 echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		 echo '<h2>Response</h2>';
		 echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		 */

		echo json_encode($this->General->xml2array($res));
		$this->autoRender = false;
	}

	function chatApi($method,$requestXML){
		App::import('vendor', 'soap', array('file' => 'soaplib/nusoap.php'));
		$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
		$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
		$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
		$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
		$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
		$namespace  = OSS_NAMESPACE_URL;
		$client = new nusoap_client(CHAT_SOAP_URL, false,$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$client->setUseCurl($useCURL);
		$err = $client->getError();

		$params = $requestXML;
		$headers = '';

		$res =  $client->call($method, $params,OSS_NAMESPACE_URL,$namespace.''.$method,$headers);
		/*echo '<h2>Request</h2>';
		 echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		 echo '<h2>Response</h2>';
		 echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';*/

		return $res;
	}

	function ossTest($type=null,$opr=null,$amt=null){
		if(isset($type) && isset($opr)){
			//get recharges - dth
			//$requestXML = '<ProcessTransactionRequest><MerchantRefNo>11231232</MerchantRefNo><RechargeCode>X4HT9HGOBE</RechargeCode><Qty>1</Qty><CategoryCode>MR</CategoryCode><OperatorCode>bsn</OperatorCode><CircleCode>ap</CircleCode><ConsumerNo>9180471155</ConsumerNo><CustMobileNo>9180471155</CustMobileNo><Denomination>10</Denomination><PNRNo></PNRNo><PGCode>OSSPG</PGCode><DistCustCode>MR00000156</DistCustCode><CardNo></CardNo><TranDistCustCode>BP00000021</TranDistCustCode><TranPassword>bp21@oss</TranPassword></ProcessTransactionRequest>';
			$requestXML = '<GetRechargesRequest><CategoryCode>'.$type.'</CategoryCode><OperatorCode>'.$opr.'</OperatorCode><CircleCode></CircleCode></GetRechargesRequest>';
			$rechargesRes = $this->ossApi('GetRecharges',$requestXML);
			$this->set('rec',$rechargesRes);

			$rechargeCode = '';
			if(isset($rechargesRes['GetRechargesResponse']['RechargeDetails']['OperatorName'])){
				$this->User->query("insert into oss_rec_codes(opr_code,circle_code,denomination,rec_code) values ('".$rechargesRes['GetRechargesResponse']['RechargeDetails']['OperatorCode']."','".$rechargesRes['GetRechargesResponse']['RechargeDetails']['CircleCode']."','".(int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination']."','".$rechargesRes['GetRechargesResponse']['RechargeDetails']['RechargeCode']."')");
				if((int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == 0  || (int)$rechargesRes['GetRechargesResponse']['RechargeDetails']['Denomination'] == 10){
					$rechargeCode = trim($rechargesRes['GetRechargesResponse']['RechargeDetails']['RechargeCode']);
				}
			}else{
				foreach($rechargesRes['GetRechargesResponse']['RechargeDetails'] as $r){
					$this->User->query("insert into oss_rec_codes(opr_code,circle_code,denomination,rec_code) values ('".$r['OperatorCode']."','".$r['CircleCode']."','".(int)$r['Denomination']."','".$r['RechargeCode']."')");
					$avail .= "Rs.".$r['Denomination'].",";
					if((int)$r['Denomination'] == 0 || (int)$r['Denomination'] == $amt){
						$rechargeCode .= trim($r['RechargeCode'])."    ";
					}
				}
			}
			$this->set('recCode',$rechargeCode);

			$this->set('selOpr',$opr);
			$this->set('type',$type);
			$this->set('amt',$amt);
			//$requestXML = '<ProcessTransactionRequest><MerchantRefNo>12345556447</MerchantRefNo><RechargeCode>'.$rechargeCode.'</RechargeCode><Qty>1</Qty><CategoryCode>MR</CategoryCode><OperatorCode>rc</OperatorCode><CircleCode>ali</CircleCode><ConsumerNo>9321756345</ConsumerNo><CustMobileNo>9321756345</CustMobileNo><Denomination>10</Denomination><PNRNo></PNRNo><PGCode>'.OSS_PG_CODE.'</PGCode><DistCustCode>'.OSS_DIST_CUST_CODE.'</DistCustCode><CardNo></CardNo><TranDistCustCode>'.OSS_TRAN_DIST_CUST_CODE.'</TranDistCustCode><TranPassword>'.OSS_TRAN_PASSWORD.'</TranPassword></ProcessTransactionRequest>';
			//$rechargesRes = $this->ossApi('ProcessTransaction',$requestXML);
		}
		$this->set('opr',$this->mapping);
		//$this->autoRender = false;

	}

	function tranStatus($tranId,$vendor,$date=null,$refId=null,$ret=false){
		$method = $vendor."TranStatus";
		$get = false;
			
		if(method_exists($this, $method)){
			if($vendor == 'modem')$date = 4;
			$abc = $this->$method($tranId,$date,$refId);
			$get = true;
			//return $abc;
		}
		else {
			$vendors = $this->Shop->getVendors();
			foreach($vendors as $ven){
				if($ven['vendors']['shortForm'] == $vendor){
					$abc = $this->modemTranStatus($tranId,$ven['vendors']['id'],$date);
					$get = true;
					break;
				}
			}
		}
		if(!$get)echo "Not implemented yet :)";
        
		$this->autoRender = false;
        //ob_clean();
        //flush();
        if($ret) return $abc;
	}
	
	function isAfterTransaction(){
		$this->autoRender = false;
		
		$tranId = $_REQUEST['id'];
		$vendor = $_REQUEST['vendor'];
		$date = $_REQUEST['date'];
		$refId = $_REQUEST['ref_id'];
		
		ob_start();
		$response1 = $this->tranStatus($tranId, $vendor, $date, $refId,true);
		ob_end_clean();
		$response = json_decode($response1,true);
		
		if(isset($response['trans_history']) && !empty($response['trans_history'])){
			if(isset($response['trans_history']['after1'])){
				echo "true";
			}
			else 
				echo "false";
		}
		else
			echo "false";
		
		die;
	}
	
	
	function simNo(){
		$this->autoRender = false;
		
		$tranId = $_REQUEST['id'];
		$vendor = $_REQUEST['vendor'];
		$date = $_REQUEST['date'];
		$refId = $_REQUEST['ref_id'];
		
		ob_start();
		$response1 = $this->tranStatus($tranId, $vendor, $date, $refId,true);
		ob_end_clean();
		$response = json_decode($response1,true);
		if(isset($response['sent_by']) && !empty($response['sent_by'])){
			$nos = explode("/", $response['sent_by']);
			$simNumber = $nos[1];
			echo $simNumber;
		}
		else
			echo "NOT FOUND";
	}
	
	function ossTranStatus($tranId,$date){
		//get recharges - dth
		$date = date('d-M-Y',strtotime($date));
		$requestXML = '<TransactionHistoryRequest><TransactionRefNo></TransactionRefNo><MerchantRefNo>'.$tranId.'</MerchantRefNo><DistCustCode></DistCustCode><FromDate>'.$date.'</FromDate><ToDate>'.$date.'</ToDate><CircleCode></CircleCode><OperatorCode></OperatorCode><Denomination>-1</Denomination><RechargeCode></RechargeCode><RechargeTypeCode></RechargeTypeCode><OrderStatus></OrderStatus></TransactionHistoryRequest>';
			
		$rechargesRes = $this->ossApi('TransactionHistory',$requestXML);
		$requestXML = '<GetTransactionStatusRequest><TransactionRefNo></TransactionRefNo><MerchantRefNo>'.$tranId.'</MerchantRefNo></GetTransactionStatusRequest>';
		$rechargesRes1 = $this->ossApi('GetTransactionStatus',$requestXML);
			
		$this->printArray($rechargesRes1);
		$this->printArray($rechargesRes);
		$this->autoRender = false;
		//return $rechargesRes;
	}

	function cpTranStatus($tranId,$date=null,$refId=null,$prodId=null){
		$sec = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_SECKEY);
		$pub = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/pub_keys/'.CYBER_PUBKEY);
                
                $sec = defined('CYBER_PUBKEY_STR') ? CYBER_SECKEY_STR : $sec;
                $pub = defined('CYBER_PUBKEY_STR') ? CYBER_PUBKEY_STR : $pub;
                
		if($prodId == null){
			$ref_code = $this->Slaves->query("SELECT vendors_activations.product_id FROM vendors_activations WHERE vendors_activations.ref_code= '".$tranId."'");
			$prodId = $ref_code['0']['vendors_activations']['product_id'];
		}
		$status_url = $this->cpurls[$prodId]['cps'];
		$data = "SESSION=$tranId\r\n";
		$res = ipriv_sign($data, $sec, CYBER_PASSWORD);

		$out = $this->cpConnect($status_url,array('inputmessage'=>$res[1]));
		if(!$out['success']){
			if($out['timeout']){
				$ret = array('status' => 'pending','errCode' => '','errMsg'=>'Connection timeout from server');
			}
			else {
				$ret = array('status' => 'pending','errCode' => '','errMsg'=>'Request timeout from server');
			}
			$this->printArray($ret);
			return $ret;
		}

		$out = $out['output'];
		$res = ipriv_verify($out, $pub);
		$result = $this->cpArray($res);
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cp.txt","*Status Check*: $status_url, Input=> ".$data."\n".json_encode($result));
		
		if(isset($result['RESULT']) && !empty($result['RESULT'])){
			if($result['RESULT'] == 1){
				$ret = array('status' => 'failure','transaction_id' => $result['TRANSID'],'operator_id' => $result['AUTHCODE'],'errCode' => $result['ERROR']);
			}
			else if(1 < $result['RESULT'] && $result['RESULT'] < 7){
				$ret = array('status' => 'pending','transaction_id' => $result['TRANSID'],'operator_id' => $result['AUTHCODE'],'errMsg' => 'Result is unknown at the moment','errCode' => $result['ERROR']);
			}
			else if($result['RESULT'] == 7 && $result['ERROR'] == 0){
				$ret = array('status' => 'success','transaction_id' => $result['TRANSID'],'operator_id' => $result['AUTHCODE'],'errCode' => $result['ERROR']);
			}
			else if(isset($result['ERROR']) && !empty($result['ERROR'])){
				$error = $result['ERROR'];
				$ret = array('status'=>'failure','transaction_id' => $result['TRANSID'],'operator_id' => $result['AUTHCODE'],'errMsg'=>$error . " :: " . $this->cp_errs[$error],'errCode'=>$error);
			}
			else {
				$ret = array('status' => 'pending','errCode' => '','errMsg'=>json_encode($result));
			}
		}
		else if(isset($result['ERROR']) && $result['ERROR'] != '0' && !empty($result['ERROR'])){
			$error = $result['ERROR'];
			$ret = array('status'=>'failure','transaction_id' => $result['TRANSID'],'operator_id' => $result['AUTHCODE'],'errMsg'=>$error . "::" .$this->cp_errs[$error],'errCode'=>$error);
		}
		else {
			$ret = array('status' => 'pending','errCode' => '','errMsg'=>'Error in status check');
		}

		$this->printArray($ret);
		return $ret;
	}

	function manualSuccess(){
		$tranId = $_REQUEST['id'];
		$ref_code = $this->Slaves->query("SELECT vendors_activations.vendor_id, vendors_activations.vendor_refid, products.service_id FROM vendors_activations join  products on (vendors_activations.product_id = products.id) WHERE vendors_activations.ref_code= '".$tranId."' AND vendors_activations.status not in (2,3)");

		if(!empty($ref_code)){
			if(!$this->Shop->checkStatus($tranId,$ref_code['0']['vendors_activations']['vendor_id'])){
				echo "Error";
			}
			else {
				if(!$this->Shop->lockTransaction($tranId)){
					echo "Error";
					return;
				}
				$this->Shop->unlockTransaction($tranId);
				$this->User->query("UPDATE vendors_activations SET prevStatus =status,status='".TRANS_SUCCESS."' , complaintNo='".(empty($_SESSION['Auth']['User']['id'])? 0 :$_SESSION['Auth']['User']['id'])."' WHERE ref_code='$tranId'");
				$this->Shop->addStatus($tranId,$ref_code['0']['vendors_activations']['vendor_id']);
					
				$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$tranId."','".$ref_code['0']['vendors_activations']['vendor_refid']."','".$ref_code['0']['products']['service_id']."','".$ref_code['0']['vendors_activations']['vendor_id']."','13','Manual Success by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");

				echo "Done";

			}
		}
		else {
			echo "Not found";
		}
		$this->autoRender = false;
	}
	
	function manualFailure(){
		$ref_code = $_REQUEST['id'];
		$va = $this->Slaves->query("select va.retailer_id
				from vendors_activations va
				where va.ref_code = '".$ref_code."'");
	
		if(!empty($va)){
			$mobile = $_SESSION['Auth']['User']['mobile'];
			$this->reverseTransaction($ref_code);
			$this->Shop->addComment(0, $va[0]['va']['retailer_id'], $ref_code, null, $mobile, null, 29, 12);
			echo "DONE";
		}
		else
			echo "NOT FOUND";
			$this->autoRender = false;
	}
	
	function modemUpdateStatus($tranId=null,$vendor=null){
		$ret = true;
		if($tranId == null){
			header('Access-Control-Allow-Origin: *');
			//header('Access-Control-Allow-Methods: GET, POST');
			$tranId = $_REQUEST['id'];
			$vendor = $_REQUEST['vendor'];
			$ret = false;
		}
		$count = 1;
		if(is_array($tranId)){
			$count = count($tranId);
			$tranId = implode(",",$tranId);
		}

		$adm = "query=update&transId=$tranId";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);

		if($Rec_Data['status'] == 'failure'){
			$data['error_desc'] = 'Recharge modem not responding';
		}
		else {
			$Rec_Data = $Rec_Data['data'];
			$data = json_decode($Rec_Data,true);
		}

		$res = array();
		if($count == 1){
			$res[$tranId] = $data;
		}
		else {
			$res = $data;
		}

		if(!empty($res))foreach($res as $tranId1=>$data1){
			//if(strtolower($data1['status']) == 'success' || strtolower($data1['status']) == 'failure')$this->Shop->addStatus($tranId1,$vendor);
			$this->modemTransactionStatus($tranId1,$data1);
		}

		if(!$ret){
			if(isset($data['error_desc'])) echo $data['error_desc'];
			else echo $data['status'];
			$this->autoRender = false;
		}
		else return;
	}


	function modemTranStatus($tranId,$vendor=4,$date=null){
		$adm = "query=status&transId=$tranId";
		$Rec_Data = $this->Shop->modemRequest($adm,$vendor);

		if($Rec_Data['status'] == 'failure'){
			echo 'Recharge modem not responding';
			$this->autoRender = false;
		}
		else {
			$Rec_Data = $Rec_Data['data'];
			$this->printArray(json_decode($Rec_Data,true));
			$this->autoRender = false;
		}
        return $Rec_Data;
	}

	function paytTranStatus($transId,$date=null,$refid=null){
		$date = date('YmdHis');
		$terminal = PAYT_USER_CODE;
		$sha=strtoupper(sha1($terminal.$transId.$date.PAYT_PASSWORD));
		$content = "OperationType=2&TerminalId=$terminal&TransactionId=$transId&DateTimeStamp=$date&Hash=$sha";

		$Rec_Data = $this->paytConnection($content);
		$Rec_Data = $Rec_Data['output'];
		$response = explode("|",$Rec_Data);
		if(count($response) == 5 && $transId == trim($response[2])){
			if(trim($response[0]) == '0'){
				$status = trim($response[1]);
				$opr_id = trim($response[3]);

				if($opr_id == 'NA')$opr_id = "";
				$desc = trim($response[4]);

				if($status == '0'){//success
					$ret =  array('status'=>'success','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'ref_id'=>$opr_id);
				}
				else if($status == '1' || $status == '4'){//under success
					$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'ref_id'=>$opr_id);
				}
				else if($status == '2' || $status == '3'){//failure
					$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'ref_id'=>$opr_id);
				}
				$this->printArray($ret);
				return $ret;
			}
			else if(trim($response[0]) == '1' || trim($response[0]) == '2'){
				echo "Transaction not found";
				$this->General->sendMails('Paytronics Transaction not found',$content."<br/>".json_encode($response),array('ashish@mindsarray.com'));
					
				return array('status' => 'failure','description' => 'Transaction not found');
			}
			else {
				echo "Technical Error - Some issue in request";
				//$this->General->sendMails('Paytronics Transaction check: Error in message',$content."<br/>".json_encode($response),array('ashish@mindsarray.com'));
					
				return array('status' => 'pending','description' => 'Technical Error');
			}
		}
		else {
			echo "Technical Error - Some issue in connection";
			//$this->General->sendMails('Paytronics Transaction check: Error in message',$content."<br/>".json_encode($response),array('ashish@mindsarray.com'));

			return array('status' => 'pending','description' => 'Technical Error');
		}

		$this->autoRender = false;
	}

	function cbzTranStatus($transId,$date=null,$refid=null){
		if(!is_array($transId))$transId = array($transId);

		$url = CBZ_TRANS_URL;
		$out = $this->General->cbzApi($url,array('username'=>CBZ_REC_USERNAME,'password'=>CBZ_REC_PASSWORD,'key'=>CBZ_REC_KEY,'cmd'=>'tr_status','trans_id'=>implode(",",$transId)));
		$out = $out['output'];


		if(!empty($date) && $date < date('Y-m-d',strtotime('-1 days'))){
			echo "Please check on cellbiz panel";
			//$this->General->sendMails('Paytronics Transaction check: Error in message',$content."<br/>".json_encode($response),array('ashish@mindsarray.com'));

			return array('status' => 'NA','description' => 'Check on cellbiz panel');
		}

		$rec = $this->General->xml2array($out);
		if($date == null && $refid == null){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/cbz.txt","*Trans Check*: Input=> ".implode(",",$transId)."<br/>".json_encode($rec));
		}

		$status = array();
		if(!empty($rec)){
			$result = array();
			if(!isset($rec['root']['rec']['0']) && isset($rec['root']['rec']))$result[] = $rec['root']['rec'];
			else if(isset($rec['root']['rec'])) $result = $rec['root']['rec'];
			else {
				foreach($transId as $tid){
					$data = array();
					$data['status'] = $rec['root']['result'];
					$data['req_id'] = $tid;
					$result[] = $data;
				}
			}
			foreach($result as $res){
				$status[$res['req_id']]['tranId'] = $res['req_id'];
				$status[$res['req_id']]['refId'] = $res['tr_id'];
				if(strtoupper($res['status']) == 'FAIL' || strtoupper($res['status']) == 'REVERT'){
					$status[$res['req_id']]['status'] = 'failure';
				}
				else if(strtoupper($res['status']) == 'SUCCESSFUL'){
					$status[$res['req_id']]['status'] = 'success';
				}
				else {
					$status[$res['req_id']]['status'] = 'process';
				}
			}
				
			foreach($transId as $tid){
				if(!isset($status[$tid])){
					$status[$tid]['status'] = 'NA';
					$status[$tid]['tranId'] = $tid;
				}
			}
				
			$this->printArray($status);
			return $status;
			//$status['status'] = 'success';
		}
		else {
			echo "Technical Error";
			//$this->General->sendMails('Paytronics Transaction check: Error in message',$content."<br/>".json_encode($response),array('ashish@mindsarray.com'));

			return array('status' => 'NA','description' => 'Technical Error');
		}

	}

	function rduTranStatus($transId,$date=null,$ref_id=null){
		$url = RDU_TRANS_URL;
		$out = $this->General->rduApi($url,array('userId'=>RDU_REC_USERNAME,'pwd'=>RDU_REC_PASSWORD,'clientTrnId'=>$transId,'date'=>$date));
		$out = $out['output'];

		if($date == null && $refid == null){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rdu.txt","*Trans Check*: Input=> $transId<br/>$out");
		}

		$response = explode(":",$out);

		if(count($response) >= 6 && $transId == trim($response[3])){
			$vendor_trans_id = trim($response[5]);
			$opr_id = trim($response[6]);
			$status = trim($response[0]);
			$desc = trim($response[2]);
			//if($opr_id == 'NA')$opr_id = "";

			if($status == '0' || $status == '2'){//success
				$ret =  array('status'=>'success','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_trans_id,'opr_ref_id'=>$opr_id);
			}
			else if($status == '528'){//under success
				$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_trans_id,'opr_ref_id'=>$opr_id);
			}
			else {//failure
				$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_trans_id,'opr_ref_id'=>$opr_id);
			}
			$this->printArray($ret);
			return $ret;
		}
		else if(trim($response[0]) == '527'){
			echo "Transaction not found";

			return array('status' => 'failure','description' => 'Transaction not found');
		}
		else {
			echo "Technical Error - Some issue in request";

			return array('status' => 'pending','description' => 'Technical Error');
		}

		$this->autoRender = false;

	}

	function uvaTranStatus($transId,$date=null,$refId,$power=null){
		$url = UVA_TRANS_URL;
		$out = $this->General->uvaApi($url,array('username'=>UVA_REC_USERID,'uniqueid'=>UVA_REC_UNIQID,'clid'=>$transId));
		$out = $out['output'];

		if($date == null){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uva.txt","*Trans Check*: Input=> $transId<br/>$out");
		}

		$response = explode("|",$out);
		$status = trim($response[0]);
		$desc = trim($response[1]);
		$oprId = ($response[2] == 'N' || $response[2] == 'NA')?"":trim($response[2]);

		if($status == 0 && (!empty($oprId) || $power == 1)){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$refId,'operator_id'=>$oprId);
		}
		else if($status == 503){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$refId,'operator_id'=>$oprId);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$refId,'operator_id'=>$oprId);
		}

		$this->printArray($ret);
		return $ret;

		$this->autoRender = false;
	}

	function uniTranStatus($transId,$date=null,$refId=null){
		$url = UNI_TRANS_URL;
		$out = $this->General->uniApi($url,array('crqid'=>$transId,'cro'=>'2'));
		$out = $out['output'];

		//505|Not found | |501|Not Process |
		/*if($date == null){
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/uni.txt","*Trans Check*: Input=> $transId<br/>$out");
			}*/

		$response = explode("|",$out);
		$status = trim($response[0]);

		if($status == 0){
			$oprId = (trim($response[1]) == 'NA')?"":trim($response[1]);
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>'Success','tranId'=>$transId,'vendor_id'=>'','operator_id'=>$oprId);
		}
		else if(in_array($status,array(503,505))){
			$desc = trim($response[1]);
			$oprId = trim($response[3]);
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>$oprId);
		}
		else {
			$desc = trim($response[1]);
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>'');
		}

		$this->printArray($ret);
		return $ret;

		$this->autoRender = false;
	}

	function anandTranStatus($transId,$date=null,$refId=null){
		$url = ANAND_TRANS_URL;
		$out = $this->General->anandApi($url,array('Mob'=>ANAND_MOB,'message'=>"TxId $refId ". ANAND_PIN,'source'=>'API'));
		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/anand.txt",date('Y-m-d H:i:s').":AnandTranStatus: ".json_encode(array('Mob'=>ANAND_MOB,'message'=>"myTxId $transId ". ANAND_PIN,'source'=>'API')));

		$response = explode(",",$out);
		$status = trim($response[0]);

		$status = explode(":",$status);

		$stat = strtolower(trim($status[1]));
		if($stat == 'success'){
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>'');
		}
		else if($stat == 'fail'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>'');
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>'');
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}

	function apnaTranStatus($transId,$date=null,$refId=null){
		$url = APNA_TRANS_URL;
		$out = $this->General->apnaApi($url,array('username'=>APNA_USERNAME,'pwd'=>APNA_PASSWD,'client_id'=>$transId));

		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/apna.txt",date('Y-m-d H:i:s').":ApnaTranStatus: ".$out);

		//Success#MU20061014170010#200708912#297617
		//Failure##200709042#297740
		$response = explode("#",$out);
		$status = strtolower(trim($response[0]));
		$operator_id = trim($response[1]);
		$vendor_id = trim($response[2]);

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == 'failure'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}

	function magicTranStatus($transId,$date=null,$refId=null){
                $url = MAGIC_TRANS_URL;
		$out = $this->General->magicApi($url,array('uid'=>MAGIC_USERNAME,'pwd'=>MAGIC_PASSWD,'transid'=>$transId));

		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/magic.txt",date('Y-m-d H:i:s').":MagicTranStatus: ".$out);

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		$response = explode(";",$out);

		$status = explode("=",strtolower(trim($response[1])));
		$status = strtolower($status[1]);

		$operator_id = explode("=",strtolower(trim($response[2])));
		$operator_id = strtolower($operator_id[1]);

		$vendor_id = "";

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == 'failure' || $status == 'cancel'){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
	
	function rioTranStatus($transId,$date=null,$refId=null){
		$url = RIO_TRANS_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO_USERNAME,'pwd'=>RIO_PASSWD,'transid'=>$transId));

		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio.txt",date('Y-m-d H:i:s').":RIOTranStatus: ".$out);

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		$response = explode(";",$out);

		$status = explode("=",strtolower(trim($response[1])));
		$status = strtolower($status[1]);

		$operator_id = explode("=",strtolower(trim($response[2])));
		$operator_id = strtolower($operator_id[1]);

		$vendor_id = "";

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == 'failure' || $status == 'cancel' || trim($out) == 'Not Found'){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
    
    function rio2TranStatus($transId,$date=null,$refId=null){
		$url = RIO2_TRANS_URL;
		$out = $this->General->rioApi($url,array('uid'=>RIO2_USERNAME,'pwd'=>RIO2_PASSWD,'transid'=>$transId));

		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rio2.txt",date('Y-m-d H:i:s').":RIOTranStatus: ".$out);

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		$response = explode(";",$out);

		$status = explode("=",strtolower(trim($response[1])));
		$status = strtolower($status[1]);

		$operator_id = explode("=",strtolower(trim($response[2])));
		$operator_id = strtolower($operator_id[1]);

		$vendor_id = "";

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == 'failure' || $status == 'cancel' || trim($out) == 'Not Found'){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}

	function infogemTranStatus($transId,$date=null,$refId=null){
		$vendor = 27;
		$terminal = GEM_USERNAME;
		$sha=strtoupper(sha1($terminal.$transId.GEM_PASSWORD));

		$url = GEM_TRANS_URL;
		$out = $this->General->gemApi($url,array('PartnerId'=>$terminal,'TransId'=>$transId,'Hash'=>$sha));

		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gem.txt",date('Y-m-d H:i:s').":GemTranStatus: ".$out);

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		$response = explode("|",$out);
		$status = trim($response[0]);
		$vendor_id = trim($response[2]);
		$operator_id = trim($response[3]);
		$desc = trim($response[4]);
		$pay1_transid = trim($response[1]);

		if($operator_id == 'NA')$operator_id = "";

		$status_array = array('100'=>'Transaction Successful','99'=>'Recharge Failed','101'=>'Invalid Login','102'=>'Insufficient Balance','103'=>'Invalid Amount','104'=>'Invalid Trans ID','105'=>'Trans ID already exists','106'=>'Service Unavailable for user','107'=>'Invalid phone Number','110'=>'Invalid Transaction amount','111'=>'Daily Limit reached','121'=>'Account Blocked','123'=>'Technical Failure','165'=>'Response waiting','170'=>'Wrong Requested Ip','171'=>'Repeated Request','173'=>'Operator temporarly not available','172'=>'Invalid request','174'=>'Hash Value MisMatch');
		if(empty($desc))$desc = $status_array[$status];
		if(empty($desc))$desc = $out;


		if(empty($status) || $status == '165' || strlen($status) > 5 || $pay1_transid != $transId){//pending
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == '100'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gem.txt",date('Y-m-d H:i:s').":ReturnGemTranStatus: ".json_encode($ret)." status:$status");

		return $ret;
		$this->autoRender = false;
	}

	function durgaTranStatus($transId,$date=null,$refId=null){
		$url = DURGA_TRANS_URL;
		$out = $this->General->durgaApi($url,array('ctxnid'=>$transId,'cid'=>'10'));
		$out = trim($out['output']);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/durga.txt","*Trans Check*: Input=> $transId<br/>$out");
		//505|Not found | |501|Not Process |
		/*if($date == null){

		}*/

		//503|Failed | BC2012092485485 | 1592560
		$response = explode("|",$out);
		$status = trim($response[0]);

		if($status == 0 && !empty($out)){
			$oprId = (trim($response[1]) == 'N')?"":trim($response[1]);
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>'Success','tranId'=>$transId,'vendor_id'=>'','operator_id'=>$oprId);
		}
		else if(in_array($status,array(503,505))){
			$desc = trim($response[1]);
			$oprId = trim($response[3]);
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>$oprId);
		}
		else {
			$desc = trim($response[1]);
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>'','operator_id'=>'');
		}

		$this->printArray($ret);
		return $ret;

		$this->autoRender = false;
	}

	function pay1TranStatus($transId,$date=null,$refId=null){
		$url = B2C_TRANS_URL;
		$out = $this->General->curl_post($url,array('client_req_id'=>$transId));
		$out = $out['output'];

		$response = json_decode($out,true);
		$status = trim($response['status']);

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/pay1.txt",date('Y-m-d H:i:s').":Request Sent: ".$url."::".$transId);

		if($status == 'success'){
			$ret =  array('status'=>'success','description'=>'success','tranId'=>$transId,'vendor_id'=>$response['description']);
		}
		else if($status == 'failure'){
			$ret =  array('status'=>'failure','description'=>$response['description'],'tranId'=>$transId,'errCode'=>$response['errCode']);
		}

		$this->printArray($ret);
		return $ret;

		$this->autoRender = false;
	}
	
	function rkitTranStatus($transId,$date=null,$refId=null){
                $url = RKIT_TRANS_URL;
		
                $out = $this->General->rkitApi($url,array('USERID'=>RKIT_USER,'PASSWORD'=>RKIT_AUTH,'TRANNO'=>$transId));
		
		//$out = $this->General->rkitApi($url,array('AGTCODE'=>RKIT_AGTCODE,'API_AUTHENTICATION'=>RKIT_AUTH,'TRANID'=>$transId,'CD'=>'STATUS'));

		$out = $out['output'];
		

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rkit.txt",date('Y-m-d H:i:s').":RkitTranStatus: ".json_encode($out));

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		//$response = explode("#",trim($out));

		$status = strtolower(trim($out['NODE']['STATUS']));
		
		//$par = strtolower(trim($response[0]));
		
		$operator_id = strtolower(trim($out['NODE']['OPTTRAN']));

		$vendor_id = "";

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if(in_array($status,array('failed','transaction not found'))){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
	
	function a2zTranStatus($transId,$date=null,$refId=null){
		$url = A2Z_TRANS_URL;
		$out = $this->General->a2zApi($url,array('USERID'=>A2Z_AGTCODE,'PASSWORD'=>A2Z_AUTH,'TRANNO'=>$transId));

		$out = $out['output'];
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/a2z.txt",date('Y-m-d H:i:s').":A2ZTranStatus: ".$out);

		//transid=<transaction_id>;status=<status>;optransid=<operator_transaction_id>
		
		$out = $this->General->xml2array("<NODE>".$out."</NODE>");
		
		$status = strtolower(trim($out['NODE']['STATUS']));
		
		$vendor_id = "";
		
		$operator_id = $out['NODE']['OPTTRAN'];

		if($status == 'success'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if(in_array($status,array('failed','transaction not found'))){
		
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
	
	
	function joinrecTranStatus($transId,$date=null,$refId=null){
		$url = JOINREC_TRANS_URL;
		$out = $this->General->joinrecApi($url,array('reseller_id'=>JOINREC_ID,'reseller_pass'=>JOINREC_PWD,'meroid'=>$transId));
	
		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/joinrec.txt",date('Y-m-d H:i:s').":JoinRecTranStatus: ".json_encode($out));

		$status = trim($out['RechargeStatus']['Status']);
		$operator_id = trim($out['RechargeStatus']['OperatorTxnId']);
		$vendor_id = trim($out['RechargeStatus']['OrderId']);
		
		if(isset($out['RechargeStatus']['Error'])){
			$description = trim($out['RechargeStatus']['Error']);
		}
		else $description = trim($out['RechargeStatus']['Description']);
		
		if($status == 'SUCCESS'){
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == 'FAILED'){
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
	
    /******/
        function mypayTranStatus($transId,$date=null,$refId=null){
                $url = MYPAYURL;
                $pwd = MYPAYPASSWD;

                $dt_time = date('m.d.Y H:i:s');

                //pwd|status|refId
                //$req_str = $pwd."|status|".$refId;        
                $req_str = $pwd."|status|".$transId;
		$out = $this->General->mypayApi($url,array('_prcsr'=>MYPAYUSER,'_urlenc'=>$req_str));
	
		$out = $out['output'];

		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/mypay.txt",date('Y-m-d H:i:s').":mypay TranStatus: ".json_encode($out));

		$status_arr = explode("|",$out['_ApiResponse']['statusDescription']);
                $status = isset($status_arr[1])?strtoupper($status_arr[1]):"";
		$operator_id = "";
		$vendor_id = "";
                $description = "";
		$statusCode = $out['_ApiResponse']['statusCode'];
		if($status == 'SUCCESS'){
                        $operator_id = isset($status_arr[3])?trim($status_arr[3]):"";
			$ret =  array('status'=>'success','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if(($status == 'FAILED' || !(in_array($statusCode,array('10008','10010','10019','10020','10021')))) && (!empty($status))){
                        $description = isset($status_arr[3])?trim($status_arr[3]):"";
			$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$description,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	}
	
	function gitechTranStatus($transId,$date=null,$refId=null){
			
        $newtransId = $transId.$transId.$transId;
		$request = "<CheckTransReq>
<UserTrackID>$newtransId</UserTrackID>
</CheckTransReq>";
		$out = $this->General->gitechApi("CheckTransactionStatus",$request,'gitechTranStatus',array('0'=>$transId,'1'=>-1));
		
		if(isset($out['status'])){
			$ret = $out;
		}
		else {
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/gitech.txt",date('Y-m-d H:i:s').":GitechTranStatus: request is $request::".json_encode($out));
	
			$status = trim($out['CheckTransRes']['StatusCode']);
			$desc = trim($out['CheckTransRes']['Remarks']);
			$vendor_id = "";
			$operator_id = "";
	
			if($status == 1){
				$vendor_id = $desc;
				$ret =  array('status'=>'success','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
			}
			else if($status == 3){
				$ret =  array('status'=>'failure','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
			}
			else {
				$ret =  array('status'=>'pending','status-code'=>$status,'description'=>$desc,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
			}
				
		}
        //$this->printArray($ret);
		if(isset($out['status']) || $date == -1){
            echo json_encode($ret);
            return $ret;
		}
		else return $ret;
		$this->autoRender = false;
	}

	function ossReconcile(){
		$query = "SELECT vendors_messages.shop_tran_id,vendors_messages.response,vendors_activations.date,vendors_activations.status,vendors_activations.amount,vendors_activations.timestamp  FROM `vendors_messages`,`vendors_activations` WHERE `response` LIKE '%71503%' AND ref_code = shop_tran_id";
		$data = $this->User->query($query);
		foreach($data as $dt){
			$ref_code = $dt['vendors_messages']['shop_tran_id'];
			$res = $this->tranStatus($ref_code,'oss',$dt['vendors_activations']['date']);
			$this->User->query("INSERT INTO oss_reconcile (ref_code,trans_id,amount,our_status,oss_status,date,response_before,response_after) VALUES ('$ref_code','".$res['GetTransactionStatusResponse']['TransactionStatus']['TransactionRefNo']."',".$dt['vendors_activations']['amount'].",".$dt['vendors_activations']['status'].",".$res['GetTransactionStatusResponse']['TransactionStatus']['Status'].",'".$dt['vendors_activations']['timestamp']."','".addslashes($dt['vendors_messages']['response'])."','".addslashes(json_encode($res))."')");
		}
		$this->autoRender = false;
	}

	function hextostr($hex)
	{
		$str='';
		for ($i=0; $i < strlen($hex)-1; $i+=2)
		{
			$str .= chr(hexdec($hex[$i].$hex[$i+1]));
		}
		return $str;
	}

	function ppiTest($vendor=null){
		
		$data = $this->modemBalance(null,$vendor);
		$this->General->logData("modem_test.txt",json_encode($data));
		$this->Shop->delMemcache("balance_$vendor");
		$data = $this->modemBalance(null,$vendor,false);
		$this->General->logData("modem_test.txt",json_encode($data));
		

		echo 1;
		$this->autoRender = false;
	}

	function cpTest(){
		$transId = "81323827390322";
		$prodId = 15;
		//$test = $this->uvaMobRecharge($transId,array('subId'=>'10000012012','mobileNumber'=>'9920419144','amount'=>10,'type'=>'flexi','operator'=>15),$prodId);
		$this->printArray($test);
		$this->autoRender = false;
	}

	function cpArray($res){
		$array = array();
			
		if($res[0] == 0){
			$output = explode("\n",$res[1]);

			foreach($output as $oput){
				$exp = explode("=",$oput);
				if(!empty($exp[0]))$array[trim($exp[0])] = trim($exp[1]);
			}
		}
		return $array;
                
	}
    function getModemsimsDetails($vendor) {
       
        
        $param = "query=sims&vendor_id=$vendor";
        $simData = $this->Shop->modemRequest($param, $vendor);
        if ($simData['status'] == 'failure') {
            echo 'Recharge modem not responding';
        } else {
            $simData = $simData['data'];
            $simData = json_decode($simData, true);
            $this->set('simData', $simData);
        }
        
//        echo "<pre>";
//        print_r($simData);
         //$query = "query=sims";
         //$url = "http://start.loc/start.php";
       
//        $query .= "&vendor_id=$vendor";
//        $Rec_Data = $this->General->curl_post($url,$query,'POST');
//        if($Rec_Data['success']=='1'){
//            $Rec_Data = json_decode($Rec_Data['output'],true);
//        }
        $oprData = $this->Slaves->query("SELECT  `id` ,`name` FROM products WHERE service_id IN ('1','2')");
        $circleData = $this->Slaves->query("SELECT `id`,`area_code`,`area_name` from `mobile_numbering_area` where `area_code`!='ZZ'");
        //$this->set('simData', $Rec_Data);
        $this->set('oprData', $oprData);
        $this->set('circleData',$circleData);
        $supplierList=  $this->Slaves->query("Select suppliers.id,suppliers.name from inv_suppliers suppliers JOIN inv_supplier_vendor_mapping sv ON suppliers.id=sv.supplier_id and sv.vendor_id={$vendor}");
        $this->set('vendors', $supplierList);
        $this->set('VendorId',$vendor);
		$vendordata = $this->Slaves->query('Select * from vendors where update_flag="1" and show_flag = "1"');
		$this->set('vendorsdata',$vendordata);
        
    }
    
    function updateSimData() {
        if ($this->RequestHandler->isAjax()) {
            
            // Check is SOID exists
            $checkifexists=$this->Slaves->query("Select id from inv_supplier_operator where operator_id='{$_POST['operator']}'  AND supplier_id='{$_POST['inv_supplier_id']}' ");
            
            if(empty($checkifexists)):
                 echo json_encode(array('data'=>'Error : Supplier-Operator Mapping Doesnot exists'));
                 exit();
            endif;
            
            $oprData = $this->Slaves->query("SELECT  `id` ,`name` FROM products WHERE service_id IN ('1','2')");
            $oprArray = array();
            foreach ($oprData as $key => $val) {
                $oprArray[$val['products']['id']] = $val['products']['name'];
            }
           
            $vendorID = isset($_POST['Vendorid']) ? $_POST['Vendorid'] : "";
            $balance = isset($_POST['balance']) ? $_POST['balance'] : "";
            $circle = isset($_POST['circle']) ? $_POST['circle'] : "";
            $comm = isset($_POST['comm']) ? $_POST['comm'] : "";
            $limit = isset($_POST['limit']) ? $_POST['limit'] : "";
            $mobile = isset($_POST['mobile']) ? $_POST['mobile'] : "";
            $operator = isset($_POST['operator']) ? $_POST['operator'] : "";
            $pin = isset($_POST['pin']) ? $_POST['pin'] : "";
            $roaming = isset($_POST['roaming']) ? $_POST['roaming'] : "";
            $showFlag = isset($_POST['showflag']) ? $_POST['showflag'] : "";
            $type = isset($_POST['type']) ? $_POST['type'] : "";
           // $vendorTag = isset($_POST['vendortag']) ? $_POST['vendortag'] : "";
            $vendorName = isset($_POST['vendor']) ? $_POST['vendor'] : "";
            $parbal = isset($_POST['parbal']) ? $_POST['parbal'] : "";
            $machineId = isset($_POST['machineid']) ? $_POST['machineid'] : "";
            $simId = isset($_POST['simid']) ? $_POST['simid'] : "";
            $oprId = $oprArray[$operator];
            $id = isset($_POST['id']) ? $_POST['id'] : "";
            $insert = isset($_POST['insert']) ? $_POST['insert'] : "";
            if($_POST['block']=="true"): $block=1; else: $block=0; endif;
			if(trim($_POST['vendor_tag'])!=""): $vendor_tag=$_POST['vendor_tag']; else: $vendor_tag=""; endif;			
			$vendor_id = isset($_POST['inv_supplier_id']) ? $_POST['inv_supplier_id'] : "0";
			if($_POST['checkmultiple']=="true"): $multiple = 1; else : $multiple=0; endif;
			$merge = isset($_POST['merge']) ? $_POST['merge'] : "";
            $query = "query=updateSimdata&operator=$oprId&mobile=$mobile&circle=$circle&type=$type&pin=$pin&balance=$balance&comm=$comm&limit=$limit&roaming=$roaming&showflag=$showFlag&id=$id&oprId=$operator&vendorname=$vendorName&parbal=$parbal&machineid=$machineId&simid=$simId&insert=$insert&block=$block&multiple=$multiple&vendor_tag=$vendor_tag&supplier_id=$vendor_id&merge=$merge";
            $Rec_Data = $this->Shop->modemRequest($query,$vendorID);
             if (isset($Rec_Data['status'])) {
               echo  json_encode($Rec_Data);
			   die;
            }
        }
        $this->autoRender = false;
    }
    
    function checkPassword() {
        
          if ($this->RequestHandler->isAjax()) {
            $userName = $_SESSION['Auth']['User']['mobile'];
             $password = $this->Auth->password($_POST['password']); 
             $checkData = $this->Slaves->query("SELECT  `id`  FROM users WHERE mobile = '$userName' AND Password = '$password'");
            if (count($checkData)) {
                echo json_encode(array("result" => "success"));
            } else {
                echo json_encode(array("result" => "failure"));
             }
             die;
          }
          $this->autoRender = false;
    }
	
	
	function updateOperatorFlag() {

		if ($this->RequestHandler->isAjax()) {
			if ($_POST['auto_check'] == "true"): $autocheck = 1;
			else : $autocheck = 0;
			endif;
			
		
			
			$oprId = isset($_POST['oprid']) ? $_POST['oprid'] : 0;
			$updateQuery = $this->User->query("UPDATE  products SET auto_check = '".$autocheck."',modified = '".date('Y-m-d H:i:s')."' where id IN($oprId)");
		}
		$this->autoRender = false;
	}

    /*
     * Function to be called by dispatcher script
     */
    function modem_response_updater($request_id){
        $RESPONSE_HASH = "modems_response_data";
        $response_data = "";
        $this->General->logData("/mnt/logs/updaterchanges-".date('Ymd').".txt",date('Y-m-d H:i:s').": in updater : data : ".$request_id);
        //--------------creating connection
        $redisObj = $this->Shop->openservice_redis();
        
        
        if($redisObj != false){
            $response_data = $redisObj->hget($RESPONSE_HASH,$request_id);
            $result = $redisObj->hdel($RESPONSE_HASH,$request_id);
            $_REQUEST = json_decode($response_data, true);
            $this->General->logData("/mnt/logs/updaterchanges.txt",date('Y-m-d H:i:s').": in updater : data : ".$RESPONSE_HASH."| ".$request_id . " | " . $response_data);
            if($redisObj->hexists($RESPONSE_HASH,$request_id)){
                $this->General->logData("/mnt/logs/updaterchanges.txt",date('Y-m-d H:i:s').": hash key not deleted ");
                $redisObj->hdel($RESPONSE_HASH,$request_id);
            }
            
            $method = isset($_REQUEST['method_name'])?$_REQUEST['method_name']:"";
            if(method_exists($this, $method)){
                unset($_REQUEST['method_name']);
                $this->$method();
            }
            //-----closing connection
            $redisObj->quit();            
        }
        unset($_REQUEST);
        $this->autoRender = false;
    }
	
	function shiftSims(){
		
		$this->autoRender = false;
		
		if ($this->RequestHandler->isAjax()) {
			
			$supplierId = $_REQUEST['supplier_id'];
			$shifted_modem_id = $_REQUEST['shifted_modem_id'];
			$vendor =   $_REQUEST['modemId'];
			$parbal  = $_REQUEST['parbal'];
			
			// Check is SOID exists
            $checkifexists=$this->Slaves->query("Select id from inv_supplier_operator where operator_id='{$_POST['oprId']}'  AND supplier_id='{$supplierId}' ");
            
            if(empty($checkifexists)):
                 echo json_encode(array('data'=>'Error : Supplier-Operator Mapping Doesnot exists'));
                 exit();
            endif;
			
			$checkVendorMapping = $this->Slaves->query("SELECT * FROM  `inv_supplier_vendor_mapping` where supplier_id = '{$supplierId}' AND vendor_id = '{$shifted_modem_id}' ");
			
			   if(empty($checkVendorMapping)):
			   echo json_encode(array('data'=>'Error : Vendor-Supplier Mapping Doesnot exists'));
                 exit();
		///get data from source vendor	
                else:
				 $param = "query=shiftsims&source_vendor_id=$vendor&target_vendor_id=$shifted_modem_id&parbal=$parbal";
				
                 $simData = $this->Shop->modemRequest($param, $vendor);
				 
				if($simData['status'] =='success'){
					$data = json_decode($simData['data'],TRUE);
					
					$querydata = json_encode($data['data']);
					
					$insertparam = "query=shiftsims&target_vendor_id={$data['target_vendor_id']}&source_vendor_id={$data['source_vendor_id']}&insertdata=".urlencode($querydata)."&parbalance=".$data['parbal'];
		//insert data in targeted vendor_id			
				    $insertdeviceData = $this->Shop->modemRequest($insertparam,$data['target_vendor_id']);
					
					if($insertdeviceData['status'] == 'success'){
						
			//update balance of source vendor_id			
					$updateData = json_decode($insertdeviceData['data'],TRUE);
						
					$updateparam = "query=shiftsims&target_vendor_id={$updateData['target_vendor_id']}&source_vendor_id={$updateData['source_vendor_id']}&parbalance=".$updateData['parbal']."&reqtype=shiftsim";
						
					$updatedeviceData = $this->Shop->modemRequest($updateparam,$updateData['source_vendor_id']);
						
					if($updatedeviceData['status'] == 'success'){
							
							 echo json_encode($updatedeviceData);
			                 die;
						}
					}
					
				}
			 
				
			endif;

		}
	}
    
    public function sendApibalanceLowAlert($data)
    {
        $this->autoRender=false;
       // $data=array('apiname'=>'Cyberplate(CP)','min'=>'105','currentbalance'=>'10256.56');
        
       Configure::load('checkapibalance');
             
        $smstemplate= Configure::read('apibalance.smstemplates');
        
        $mobile=  Configure::read('apibalance.mobile');
      
        foreach($mobile as $row):
        					if($data['min'] > 0){
                          		$messageString=  sprintf($smstemplate,$data['apiname'],$data['min'],$data['currentbalance']);
                          		$this->General->sendMessage($row,$messageString,'shops');
        					}
        endforeach;
     
    }
	
	/*** practicsoft api Integration ****/
	
	
	function practicMobRecharge($transId,$params,$prodId){

		//9769480014
		$mobileNo = $params['mobileNumber'];
		
		//$mobileNo = 9769480014;
//		
		$amount = intval($params['amount']);
		
		//$amount = 20;

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['practic'];
		
		//$provider = 'Vodafone';
		
		//$transId = '32145678910';
		
		$rechargeType = "R";
		$vendor = 68;
		
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rechargeType = 'S';
		}
		
		$url = PRACTIC_RECHARGE_URL;
		$out = $this->General->practicApi($url,array('Operator'=>$provider,
										             'Number'=>$mobileNo,
													'Amount'=>$amount,
													'RechargeType'=>$rechargeType,
													'ReferenceID'=>$transId,
													'Result'=>1,
													'Repeat'=>0,
													'UserID'=>PRACTIC_USERID,
													"Password"=>PRACTIC_PASSWORD,
													"Key"=>PRACTIC_KEY)
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/practic.txt","*Mob Recharge*: Input=> $transId<br/>$out");

		$res = explode("^",$out);
		
		$status = strtolower(trim($res[0]));
		
		$txnId = trim($res[2]);

		if(empty($status) || $status == 'requestaccepted'){
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		} else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			
		}
		
		$this->autoRender = false;
		
	}
        
        function practicDthRecharge($transId,$params,$prodId){
	
		$mobileNo = $params['subId'];
                
		$amount = intval($params['amount']);
		
		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['practic'];
		
		$rechargeType = "R";
		$vendor = 68;
		
		if(in_array($prodId,array('27','28','29','31','34'))){
			$rechargeType = 'S';
		}
		
		$url = PRACTIC_RECHARGE_URL;
		$out = $this->General->practicApi($url,array('Operator'=>$provider,
                                                            'Number'=>$mobileNo,
                                                            'Amount'=>$amount,
                                                            'RechargeType'=>$rechargeType,
                                                            'ReferenceID'=>$transId,
                                                            'Result'=>1,
                                                            'Repeat'=>0,
                                                            'UserID'=>PRACTIC_USERID,
                                                            "Password"=>PRACTIC_PASSWORD,
                                                            "Key"=>PRACTIC_KEY)
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/practic.txt","*DTH Recharge*: Input=> $transId<br/>$out");

		$res = explode("^",$out);
		
		$status = strtolower(trim($res[0]));
		
		$txnId = trim($res[2]);

		if(empty($status) || $status == 'requestaccepted'){
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		} else {
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($out),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($out)."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
			
		}
		
		$this->autoRender = false;
		
	}
	
	function practicTranStatus($transId,$date=null,$refId=null){
		$url = PRACTIC_TRANS_URL;
		
		$out = $this->General->practicApi($url,array('u'=>PRACTIC_USERID,'p'=>PRACTIC_PASSWORD,'c'=>$transId));
		
		$out = $out['output'];
		$check = simplexml_load_string($out);
		if(!$check){
			if(trim($out) == 'Record not found.' && intVal((time() - strtotime($date)) / 86400) >= 2 ){
                            $status = 9;
                        }elseif(trim($out) == 'Record not found.'){
                            $status = 7;
                        }
			else{
                            $status = 0;
                        }
		}
		else {
			//		0 – Recharge request in process.
//		1 – Recharge successful.
//		2 – Request accepted.
//		3 – Recharge suspense.
//		4 – System reserved.
//		7 – Recharge failure.
//		9 – status not provided by vendor.                    
			$out = $this->General->xml2array($out);
			$vendor_id = isset($out['RechargeRequest']['RequestResponse']['APIRef']) ? $out['RechargeRequest']['RequestResponse']['APIRef'] : "" ;
		
			$operator_id = isset($out['RechargeRequest']['RequestResponse']['ORef']) ? $out['RechargeRequest']['RequestResponse']['ORef'] : ""; 
                        
                        $operator_id = in_array(strtolower(trim($operator_id)),array("nil","null"))?"":$operator_id;
                        
			$status = isset($out['RechargeRequest']['RequestResponse']['RecS']) ? $out['RechargeRequest']['RequestResponse']['RecS'] : 0;
		}
	
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/practic.txt",date('Y-m-d H:i:s').":PracticTranStatus:$transId: ".json_encode($out));

		if($status == '1'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out['RechargeRequest']['RequestResponse'],'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
		else if($status == '7' || $status == '4'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}
                elseif($status == '9' ){
                        $out = 'status not available. Kindly check on vendor\'s panel';
                        $ret =  array('status'=>'status not available. Kindly check on vendor\'s panel','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
                }
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
	} 
	
	function practicStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/practic.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $transId = trim($data['YRef']);
        $this->Shop->setMemcache("practic".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 68");
		$this->autoRender = false;
	}
        
	function practicBalance($panel = null,$min = null,$power = null){
				
		$vendor_id = 68;
		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = PRACTIC_BAL_URL;
			$out = $this->General->practicApi($url,array('a'=>PRACTIC_KEY));
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/practic.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("Practicsoft Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
	
	function simpleMobRecharge($transId,$params,$prodId){

		$mobileNo = $params['mobileNumber'];
		
                $amount = intval($params['amount']);
                
                if(in_array($prodId,array('27','28','29','31','34'))){
                   $params['type'] = 'voucher';
                }
				
//		if(isset($params['area']) && $params['area'] == 'MH' && $prodId == 4){
//                    $provider = 8;
//                } else {
                    $provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['simple'];
//                }
                
		$vendor = 69;
		
		$url = SIMPLE_RECHARGE_URL;
		
		$out = $this->General->simpleApi($url,array('username'=>SIMPLE_USERID,
										            'pwd'=>SIMPLE_PASSWORD,
													'circlecode'=>'*',
													'operatorcode'=>$provider,
													'number'=>$mobileNo,
													'amount' => $amount,
													'orderid' => $transId
													 )
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt","*Mob Recharge*: Input=> $transId<br/>$out<br/>params=>".json_encode($params));
		
		$res = explode("#", $out);
		
		if (count($res) == 1) {
			$res = explode("::", $out);
		} else {
			$txnId = trim($res[0]);
		}
		
		
		if(trim($res[0]) == 'ERROR'){
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($res[1]),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[1])."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
		      $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($out)."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		$this->autoRender = false;
		
	}  
	
	
	function simpleBalance($panel = null,$min = null,$power = null){
				
		$vendor_id = 69;

		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = SIMPLE_BAL_URL;
			$out = $this->General->simpleApi($url,array('username'=>SIMPLE_USERID,'pwd' =>SIMPLE_PASSWORD));
			
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("simpleRecharge Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
	function simpleTranStatus($transId,$date=null,$refId=null){
		
		$url = SIMPLE_TRANS_URL;
		
		$out = $this->General->simpleApi($url,
				                        array(
												'username' => SIMPLE_USERID,
												'pwd' => SIMPLE_PASSWORD,
												'order_id'=>$transId
				                            ));
		$out = $out['output'];
				
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt",date('Y-m-d H:i:s').":SimpleTranStatus: ".$out);

		if($out == 'Success'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out,'tranId'=>$transId);
		}
		else if($out == 'Failure'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId);
		}

		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
		
	}
	
	function simpleStatus(){
        $data = $_REQUEST;
        $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

        $transId = trim($data['client_id']);
        $this->Shop->setMemcache("simple".$transId,$data,10*60);
        shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 69");
		$this->autoRender = false;
	}
	
	function simpleDthRecharge($transId,$params,$prodId){
		

		$mobileNo = $params['subId'];
		
        $amount = intval($params['amount']);
		
		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['simple'];
		
		$vendor = 69;
		
		$url = SIMPLE_RECHARGE_URL;
		
		$out = $this->General->simpleApi($url,array('username'=>SIMPLE_USERID,
										            'pwd'=>SIMPLE_PASSWORD,
													'circlecode'=>'*',
													'operatorcode'=>$provider,
                                                     'number'=>$mobileNo,
                                                     'amount' => $amount,
                                                     'orderid' => $transId
                                                             )
                          );
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt","*dth Recharge*: Input=> $transId<br/>$out");
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				$this->General->log_in_vendor_message(array($transId,'','2',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','2',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/simple.txt","*dth Recharge*: Input=> $transId<br/>$out");
		
		$res = explode("#", $out);
		$txnId = "";
		if (count($res) == 1) {
			$res = explode("::", $out);
		} else {
			$txnId = trim($res[0]);
		}
		$opr_id = !empty($res[2]) ? $res[2] : "";
                
                //$this->User->query("UPDATE vendors_activations SET vendor_refid = '".$txnId."',operator_id='".$opr_id."' WHERE ref_code='$transId' AND vendor_id = $vendor");
                
		if((trim($res[1]) == 'Failure')){
			$this->Shop->addStatus($transId,$vendor);
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'30',addslashes($res[2]),'failure',date("Y-m-d H:i:s")));
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'30','".addslashes($res[2])."','failure','".date("Y-m-d H:i:s")."')");
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>$txnId,'operator_id'=>$opr_id);
		}
		else if((trim($res[1]) == 'Success')){
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'success',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','success','".date("Y-m-d H:i:s")."')");
			return array('status'=>'success','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
		} else {
			
			$this->General->log_in_vendor_message(array($transId,$txnId,'2',$vendor,'15',addslashes($out),'pending',date("Y-m-d H:i:s")));
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','2',$vendor,'15','".addslashes($out)."','success','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'','operator_id'=>$opr_id);
			
		}
		
		
		$this->autoRender = false;
		
	}
	
	function manglamMobRecharge($transId,$params,$prodId){
		
		$mobileNo = $params['mobileNumber'];

		$amount = intval($params['amount']);
		
		if (in_array($prodId, array('27', '28', '29', '31', '34'))) {
			$params['type'] = 'voucher';
		}

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['manglam'];
		
		
		$vendor = 87;
		
		$url = MANGALAM_RECHARGE_URL;
		
		$message = $provider.$mobileNo."A".$amount."REF".$transId;
		
		$out = $this->General->manglamApi($url,array('login_id'=>MANGALAM_USERID,
										            'transaction_password'=>MANGALAM_PASSWORD,
													'message'=>$message,
													'response_type'=>'CSV',
													 )
				                          );
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$res = explode(",",$out);
		
		$txnId = !empty($res[1]) ? $res[1] : "";
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/manglam.txt","*Mob Recharge*: Input=> $transId<br/>$out<br/>params=>".json_encode($params));
				
		if($res[0] =='Failure'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[5])."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($res[5]),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($res[5])."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'15',addslashes($res[5]),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		$this->autoRender = false;
		
	}
	
	function manglamDthRecharge($transId,$params,$prodId){
		
		$mobileNo = $params['subId'];

		$amount = intval($params['amount']);
		
		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['manglam'];
		
		$vendor = 87;
		
		$url = MANGALAM_RECHARGE_URL;
		
		$message = $provider.$mobileNo."A".$amount."REF".$transId;
		
		$out = $this->General->manglamApi($url,array('login_id'=>MANGALAM_USERID,
										            'transaction_password'=>MANGALAM_PASSWORD,
													'message'=>$message,
													'response_type'=>'CSV',
													 )
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$res = explode(",",$out);
		
		$txnId = !empty($res[1]) ? $res[1] : "";
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/manglam.txt","*dth Recharge*: Input=> $transId<br/>$out<br/>params=>".json_encode($params));
				
		if($res[0] =='Failure'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[5])."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$txnId,'1',$vendor,'30',addslashes($res[5]),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($res[5])."','pending','".date("Y-m-d H:i:s")."')");
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		$this->autoRender = false;
		
	}
	
	function manglamBalance($panel = null,$min = null,$power = null){
				
		$vendor_id = 87;

		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			
			$url = MANGALAM_BAL_URL;
			
			$out = $this->General->manglamApi($url,array('login_id'=>MANGALAM_USERID,'transaction_password'=>MANGALAM_PASSWORD));
			
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = trim($out['output']);
			
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/manglam.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("manglamRecharge Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
	function manglamTranStatus($transId,$date=null,$refId=null){
		
		$url = MANGALAM_TRANS_URL;
		
		$out = $this->General->manglamApi($url,
				                        array(
												'login_id' => MANGALAM_USERID,
												'transaction_password' => MANGALAM_PASSWORD,
												'CLIENTID'=>$transId,
												'response_type' => 'XML'
				                            ));
		
		$out = $this->General->xml2array($out['output']);
				
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/manglam.txt",date('Y-m-d H:i:s')."transId=>$transId".":manglamTranStatus: ".json_encode($out));

		if($out['RESPONSE']['STATUS'] == 'Success'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out,'tranId'=>$transId);
		}elseif($out['RESPONSE']['STATUS'] == 'Failure' && $out['RESPONSE']['MESSAGE'] == 'Transaction Not Found' && intVal((time() - strtotime($date)) / 86400) >=2 ){
                        $ret =  array('status'=>'status not available. Kindly check on vendor\'s panel','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'vendor_id'=>$vendor_id,'operator_id'=>$operator_id);
                }
		else if($out['RESPONSE']['STATUS'] == 'Failure'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId);
		}
		
		
		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
		
		}
		
	function manglamStatus(){
                $data = $_REQUEST;
                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/manglam.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

                $transId = trim($data['uniqueid']);
                $this->Shop->setMemcache("manglam".$transId,$data,10*60);
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 87");
		$this->autoRender = false;
	}
	
	function bulkMobRecharge($transId,$params,$prodId){
		
		$mobileNo = $params['mobileNumber'];

		$amount = intval($params['amount']);

		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['bulk'];
		
		$vendor = 105;
		
		$url = BULK_RECHARGE_URL;
		
		$out = $this->General->bulkApi($url,array('username'=>BULK_USERID,
										            'password'=>BULK_PASSWORD,
													'number'=>$mobileNo,
													'circlecode'=>'12',
													'operatorcode'=>$provider,
													'amount' => $amount,
													'clientid'=>$transId
													 )
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$res = explode(",",$out);
		
		$clinetId = (isset($res[1]) && !empty($res[1])) ? $res[1] : ""; 
		$status =  (isset($res[0]) && !empty($res[0])) ? $res[0] : ""; 
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bulk.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($res)."<br/>params=>".json_encode($params));
				
		if($status =='failure'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[5])."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$clinetId,'1',$vendor,'30',addslashes($res[0]),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($res[5])."','pending','".date("Y-m-d H:i:s")."')");
            $this->General->log_in_vendor_message(array($transId,$clinetId,'1',$vendor,'15',addslashes($res[0]),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		$this->autoRender = false;
		
	}
	
	function bulkBalance($panel = null,$min = null,$power = null){
		
		//$this->autoRender = false;
		
		$vendor_id = 105;

		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		
		if ($out === false || $power == 1) {

			$url = BULK_BAL_URL;

			$out = $this->General->bulkApi($url, array('username' => BULK_USERID, 'password' => BULK_PASSWORD));

			if (!$out['success']) {
				return array('balance' => '');
			}

			$bal = explode(',', $out['output']);

			$out = (isset($bal[1]) && !empty($bal[1])) ? $bal[1] : "";
			
			

			$this->General->logData($_SERVER['DOCUMENT_ROOT'] . "/logs/bulk.txt", date('Y-m-d H:i:s') . ":Balance Check: $out");
			$this->Shop->setMemcache("balance_" . $vendor_id, $out, 24 * 60 * 60);
		}

		$bal = trim($out);
		
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("bulkRecharge Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
	
	function bulkTranStatus($transId,$date=null,$refId=null){
            
		$url = BULK_TRANS_URL;
           
		$out = $this->General->bulkApi($url,
				                        array(
												'username' => BULK_USERID,
												'password' => BULK_PASSWORD,
												'rcid'=>$transId
												
				                            ));
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bulk.txt",date('Y-m-d H:i:s')."transId=>$transId".":bulkTranStatus: ".json_encode($out['output']));
         
		$response = explode(",",$out['output']);
		
		if($response[0] == 'success'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out['output'],'tranId'=>$transId);
		}
		else if($response[0] == 'failure'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out['output'],'tranId'=>$transId);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out['output'],'tranId'=>$transId);
		}
		
		
		$this->printArray($ret);
		return $ret;
		$this->autoRender = false;
		
	}
	
	function bulkStatus(){
                $data = $_REQUEST;
                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bulk.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

                $transId = trim($data['clientid']);
                $this->Shop->setMemcache("bulk".$transId,$data,10*60);
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 105");
		$this->autoRender = false;
	} 
	
	function bulkDthRecharge($transId,$params,$prodId){
		$mobileNo = $params['subId'];

		$amount = intval($params['amount']);

		$provider = $this->mapping['dthRecharge'][$params['operator']][$params['type']]['bulk'];
		
		$vendor = 105;
		
		$url = BULK_RECHARGE_URL;
		
		$out = $this->General->bulkApi($url,array('username'=>BULK_USERID,
										            'password'=>BULK_PASSWORD,
													'number'=>$mobileNo,
													'circlecode'=>'12',
													'operatorcode'=>$provider,
													'amount' => $amount,
													'clientid'=>$transId
													 )
				                          );
		
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $out['output'];
		
		$res = explode(",",$out);
		
		$clinetId = (isset($res[1]) && !empty($res[1])) ? $res[1] : ""; 
		$status =  (isset($res[0]) && !empty($res[0])) ? $res[0] : ""; 
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bulk.txt","*dth Recharge*: Input=> $transId<br/>".json_encode($res)."<br/>params=>".json_encode($params));
				
		if($status =='failure'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[5])."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$clinetId,'1',$vendor,'30',addslashes($res[0]),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($res[5])."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,$clinetId,'1',$vendor,'15',addslashes($res[0]),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
		$this->autoRender = false;
	}
        
        function bimcoMobRecharge($transId,$params,$prodId,$res=null){
            
            
                if(is_string($params)){
                    
                    $string = parse_str($params);
                    
                    $params['mobileNumber'] = $mobileNumber;
                    $params['operator'] = $operator;
                    $params['type'] = $type;
                    $params['amount'] = $amount;
                }
            
	        $mobileNo = $params['mobileNumber'];
               
		$amount = intval($params['amount']);

                $provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['bimco'];
                
		$vendor = 123;
		
		$url = BIMCO_RECHARGE_URL;
		
		$out = $this->General->bimcoApi($url,array('apikey'=>BIMCO_APIKEY,
							  'username'=>BIMCO_USERNAME,
							  'mobile'=>$mobileNo,
							  'amount'=>$amount,
							  'opcode'=>$provider,
							  'Merchantrefno' => $transId,
							  'ServiceType'=>'MR'
							),"bimcoMobRecharge",array("0"=>$transId,"1"=>$params,"2"=>$prodId,"3" =>'-1')
				                          );
               
               
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}
                
                
                if($out['success']) {

		$out = $out['output'];
                
		$res = explode(" ",$out);
                 
		$status =  (isset($res[1]) && !empty($res[1])) ? $res[1] : ""; 
                
                $clientId =  (isset($res[0]) && !empty($res[0])) ? $res[0] : ""; 
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bimco.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($res)."<br/>params=>".json_encode($params));
				
		if($status =='FAILURE'){
			$this->Shop->addStatus($transId,$vendor);
                        $this->General->log_in_vendor_message(array($transId,$clientId,'1',$vendor,'30',addslashes($res[0]),'failure',date("Y-m-d H:i:s")));
			$ret =  array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			
                         $this->General->log_in_vendor_message(array($transId,$clientId,'1',$vendor,'15',addslashes($res[0]),'pending',date("Y-m-d H:i:s")));
			$ret = array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>$txnId,'pinRefNo'=>'');
		}
                
                if($res=='-1'){
                     echo json_encode($ret);
                } else {
                    
                    return $ret;
                }
                } else {
                    
                    $ret = json_decode($out,True);
                    return $ret;
                }
               
		$this->autoRender = false;
		
	}
        
        function bimcoBalance($panel = null,$min = null,$power = null){
		
		$vendor_id = 123;

		$out = $this->Shop->getMemcache("balance_".$vendor_id);
               
		if ($out === false || $power == 1) {
                     
			$url = BIMCO_BAL_URL;
                        
			$out = $this->General->bimcoApi($url,array('apikey' => BIMCO_APIKEY, 'username' => BIMCO_USERNAME),"bimcoBalance",array('0'=>$panel,'1'=>-1));
                    
                        
                        if(isset($out['success'])){
                            
			$bal = explode(':', $out['output']);

			$out = (isset($bal[1]) && !empty($bal[1])) ? $bal[1] : "";

			$this->General->logData($_SERVER['DOCUMENT_ROOT'] . "/logs/bimco.txt", date('Y-m-d H:i:s') . ":Balance Check: $out");
			$this->Shop->setMemcache("balance_" . $vendor_id, $out, 24 * 60 * 60);
                        
                        $out = trim($out);
                        
                        } else {
                            $output = json_decode($out,True);                        
                                    
                            $out = $output['balance'];
                        }
                         
                        
		         }
                         $bal = $out;
                       
                        if($panel == null){
                                if($bal < $min){
                                        $this->General->sendMails("bimcoRecharge Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
                                }
                               
                        }
                        else {
                        $ret = array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
			
			if($min == -1){
			echo json_encode($ret);
			}
                      
			else return $ret;
                        $this->autoRender = false;
                        
			
		     }
               
		
	}
       
        function bimcoTranStatus($transId,$date=null,$refId=null){
            
                $this->autoRender = false;
		
	        $url = BIMCO_TRANS_URL;
                 
		$out = $this->General->bimcoApi($url,
				                        array(
							    'apikey' =>BIMCO_APIKEY,
							    'username' =>BIMCO_USERNAME,
                                                            'Merchantrefno' => $transId,
							     'ServiceType' => 'MR'
				                            ),"bimcoTranStatus",
                                                          array('0'=>$transId,'1'=>-1)
                                                 );
                
              
               
                if(isset($out['success'])){
                 
                $out = explode("||",$out['output']);
                
                $status =  explode("=",$out[2]);
                $operatorId = explode("=",$out[1]);
                $vendorId = explode("=",$out[3]);
				
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bimco.txt",date('Y-m-d H:i:s')."transId=>$transId".":bimcoTranStatus: ".json_encode($out));

		if($status[1] == 'SUCCESS'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$out,'tranId'=>$transId,'operator_id' => $operatorId[1],"vendor_id" => $vendorId[1]);
		}
		else if($status[1] == 'FAILURE'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$out,'tranId'=>$transId,'operator_id' => $operatorId[1],"vendor_id" => $vendorId[1]);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$out,'tranId'=>$transId,'operator_id' => $operatorId[1],"vendor_id" => $vendorId[1]);
		}
                    if ($date == '-1') {
                         echo json_encode($ret);
                    } else {
                         return $ret;
                   }
                } else {

                        $ret = json_decode($out, true);
                        $this->printArray($ret);
                        return $ret;
                    }
          }

            function bimcoStatus(){
                $data = $_REQUEST;
                
                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/bimco.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

                $transId = trim($data['Merchantrefno']);
                $this->Shop->setMemcache("bimco".$transId,$data,10*60);
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 123");
		$this->autoRender = false;
                
	         }
                 
               public function rajanMobRecharge($transId,$params,$prodId){
                     
                $mobileNo = $params['mobileNumber'];
                
		$amount = intval($params['amount']);
		
		$provider = $this->mapping['mobRecharge'][$params['operator']][$params['type']]['rajan'];
		
		$vendor = 125;
		
		$url = RAJAN_RECHARGE_URL;
		
		$out = $this->General->rajanApi($url,array('uname'=>RAJAN_USERNAME,
							   'password'=>RAJAN_PASSWORD,
			                                   'provider'=>$provider,
							    'mobno'=>$mobileNo,
                                                            'amount' =>$amount,
                                                            'uid' => $transId
							   )
				                          );
		if(!$out['success']){
			if($out['timeout']){
				$this->Shop->addStatus($transId,$vendor);
				//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','','1',$vendor,'14','Not able to connect to server','failure','".date("Y-m-d H:i:s")."')");
                                $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'14','Not able to connect to server','failure',date("Y-m-d H:i:s")));
				return array('status'=>'failure','code'=>'14','description'=>$this->Shop->errors(14),'tranId'=>'','pinRefNo'=>'','operator_id'=>'');
			}
		}

		$out = $this->General->xml2array($out['output']);
              
                $status = isset($out['response']['status']) ?  $out['response']['status'] : "";
                
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rajan.txt","*Mob Recharge*: Input=> $transId<br/>".json_encode($out)."s<br/>params=>".json_encode($params));
				
		if($status =='FAILURE'){
			$this->Shop->addStatus($transId,$vendor);
			//$this->User->query("INSERT INTO vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'30','".addslashes($res[5])."','failure','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'30',addslashes($out['response']['status']),'failure',date("Y-m-d H:i:s")));
			return array('status'=>'failure','code'=>'30','description'=>$this->Shop->errors(30),'tranId'=>'');
		}
		else {
			//$this->User->query("insert into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$transId."','".$txnId."','1',$vendor,'15','".addslashes($res[5])."','pending','".date("Y-m-d H:i:s")."')");
                        $this->General->log_in_vendor_message(array($transId,'','1',$vendor,'15',addslashes($out['response']['status']),'pending',date("Y-m-d H:i:s")));
			return array('status'=>'pending','code'=>'15','description'=>$this->Shop->errors(15),'tranId'=>'','pinRefNo'=>'');
		}
		$this->autoRender = false;
		
                     
                 }
                 
                 
        function rajanBalance($panel = null,$min = null,$power = null){
				
		$vendor_id = 125;

		$out = $this->Shop->getMemcache("balance_".$vendor_id);
		
		if($out === false || $power == 1){
			$url = RAJAN_BAL_URL;
			$out = $this->General->rajanApi($url,array('uname'=>RAJAN_USERNAME,'password' =>RAJAN_PASSWORD));
			
	
			if(!$out['success']){
				return array('balance'=>'');
			}
			
			$out = $this->General->xml2array($out['output']);
                       
                        $out = isset($out['response']['balance']) ? $out['response']['balance'] : ""; 
			$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rajan.txt",date('Y-m-d H:i:s').":Balance Check: $out");
			$this->Shop->setMemcache("balance_".$vendor_id,$out,24*60*60);
		}
			
		$bal = trim($out);
                
			
		if($panel == null){
			if($bal < $min){
				$this->General->sendMails("rajanRecharge Api Balance below ".$min,"Current balance: Rs." .$bal,array('backend@mindsarray.com','limits@mindsarray.com'),'mail');
			}
			
		}
		else {
			return array('balance'=>$bal,'last'=>$this->Shop->getMemcache("vendor$vendor_id"."_last"));
		}
		
	}
        
        function rajanTranStatus($transId,$date=null,$refId=null){
	
                $this->autoRender = false;
            
		$vendor_id = 125;

		$url = RAJAN_TRANS_URL;
           
		$out = $this->General->rajanApi($url,
				                        array('uname' => RAJAN_USERNAME,
					                      'password' => RAJAN_PASSWORD,
							       'uid'=>$transId
												
				                            ));
		
		$this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rajan.txt",date('Y-m-d H:i:s')."transId=>$transId".":rajanTranStatus: ".json_encode($out['output']));
         
		$response = $this->General->xml2array($out['output']);
                
                $status = $response['response']['status'];
		
		if($status == 'SUCCESS'){ 
			$ret =  array('status'=>'success','status-code'=>'success','description'=>$response,'tranId'=>$transId);
		}
		else if($response[0] == 'FAILURE'){
			$ret =  array('status'=>'failure','status-code'=>'failure','description'=>$response,'tranId'=>$transId);
		}
		else {
			$ret =  array('status'=>'pending','status-code'=>'pending','description'=>$response,'tranId'=>$transId);
		}
		
		
		$this->printArray($ret);
		return $ret;
		
	}
        
            function rajanStatus(){
                $data = $_REQUEST;
                
                $this->General->logData($_SERVER['DOCUMENT_ROOT']."/logs/rajan.txt",date('Y-m-d H:i:s').":AutoStatusUpdateLog: ".json_encode($data));

                $transId = trim($data['uid']);
                $this->Shop->setMemcache("rajan".$transId,$data,10*60);
                shell_exec("sh " . $_SERVER['DOCUMENT_ROOT']."/scripts/status.sh $transId 125");
		$this->autoRender = false;
                
	         }
        
			}
