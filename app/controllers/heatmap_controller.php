<?php

class HeatmapController extends AppController {
    
	var $name = 'Heatmap';
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator','GChart','Csv');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Retailer','Slaves');
        
        public function index($state = 'All', $city = 'All', $area = 'All') {

            $this->autoLayout = FALSE;

            $states = $this->Slaves->query('SELECT * FROM locator_state');
            $this->set('states', $states);

            if($state == 'All') {

                $res = $this->Slaves->query("SELECT * FROM "
                        . "(SELECT user_id, latitude, longitude FROM user_profile "
                        . "WHERE latitude > 0 AND longitude > 0 AND area_id > 0 AND Date(updated) > '" . date('Y-m-d H:i:s', strtotime('-7 days')) . "' "
                        . "ORDER BY 1 DESC) as tbl group by user_id");

                $zoom = 5;
            } else {

                $where_clause = " locator_state.id = $state ";
                $table = 'locator_state';
                $zoom = 7;

                if($city != 'All') {
                    $where_clause .= " AND locator_city.id = $city ";
                    $table = 'locator_city';
                    $zoom = 9;
                }
                if($area != 'All') {
                    $where_clause .= " AND locator_area.id = $area ";
                    $table = 'locator_area';
                    $zoom = 13;
                }
                $res = $this->Slaves->query("SELECT $table.name, $table.lat, $table.long, tbl.latitude, tbl.longitude "
                        . "FROM (SELECT user_id, latitude, longitude, area_id FROM user_profile WHERE latitude > 0 AND longitude > 0 AND area_id > 0 AND Date(updated) > '" . date('Y-m-d H:i:s', strtotime('-7 days')) . "' ORDER BY 1 DESC) as tbl "
                        . "LEFT JOIN locator_area ON locator_area.id = tbl.area_id "
                        . "LEFT JOIN locator_city ON locator_city.id = locator_area.city_id "
                        . "LEFT JOIN locator_state ON locator_state.id = locator_city.state_id "
                        . "WHERE $where_clause GROUP BY tbl.user_id");
            }

            $data = array();
            foreach($res as $temp) {
                $data[] = array('lat' => $temp['tbl']['latitude'], 'lng' => $temp['tbl']['longitude']);
            }

            $this->set('data', json_encode($data));
            $this->set('center', array(($res[0][$table]['lat'] == '') ? 23.3302095 : $res[0][$table]['lat'] , ($res[0][$table]['long'] == '') ? 78.0576766 : $res[0][$table]['long'], $zoom, $state, $city, $area));
        }

        public function filterCityArea() {

            $this->autoRender = FALSE;

            $search     = $this->params['form']['search'];
            $location   = $this->params['form']['location'];

            if($search == 'City') {
                $data = $this->Slaves->query("SELECT id, name FROM locator_city WHERE state_id = $location");
            } else {
                $data = $this->Slaves->query("SELECT id, name FROM locator_area WHERE city_id = $location");
            }

            echo json_encode($data);
        }
}