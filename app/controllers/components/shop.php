<?php

class ShopComponent extends Object {
	var $components = array('General','B2cextender');
	var $Memcache = null;
	
	function __construct(){
		if(class_exists("Memcache")){
			$memcache = new Memcache;
			if($memcache->pconnect(MEMCACHE_IP,11211)){
				$this->Memcache = $memcache;
			}
		}
	}
	
	function addMemcache($key,$value,$duration){
		if(!empty($this->Memcache)){
			return $this->Memcache->add($key,$value,false,$duration);
		}
		else return false;
	}
	
	function setMemcache($key,$value,$duration=null){
		if(!empty($this->Memcache)){
			if(empty($duration))$duration = 0;
			return $this->Memcache->set($key,$value,false,$duration);
		}
		else return false;
	}
	
	function incrementMemcache($key,$counter=1){
		if(!empty($this->Memcache)){
			return $this->Memcache->increment($key,$counter);
		}
		else return false;
	}
	
	function decrementMemcache($key,$counter=1){
		if(!empty($this->Memcache)){
			return $this->Memcache->decrement($key,$counter);
		}
		else return false;
	}
	
	function getMemcache($key){
		if(!empty($this->Memcache)){
			return $this->Memcache->get($key);
		}
		else return false;
	}
	
	function getMemcacheStats(){
		if(!empty($this->Memcache)){
			return $this->Memcache->getStats();
		}
		else return false;
	}
	
	function getMemcacheKeys($prefix=null){
		if(!empty($this->Memcache)){
			$list = array();
		    $allSlabs = $this->Memcache->getExtendedStats('slabs');
		    $items = $this->Memcache->getExtendedStats('items');
		    $i = 0;
		    foreach($allSlabs as $server => $slabs) {
		        foreach($slabs AS $slabId => $slabMeta) {
		            $cdump = $this->Memcache->getExtendedStats('cachedump',(int)$slabId);
		            foreach($cdump AS $keys => $arrVal) {
		                if (!is_array($arrVal)) continue;
		                foreach($arrVal AS $k => $v) {
		                	if(empty($prefix) || (strpos($k,$prefix) !== false && strpos($k,$prefix) == 0)){                   
		                    	$list[$k] = $this->getMemcache($k);
		                	}
		                }
		            }
		        }
		    }
		    return $list;
		}
		else return false;
	}
	
	function delMemcache($key){
		if(!empty($this->Memcache)){
			return $this->Memcache->delete($key);
		}
		else return false;
	}
	
	function redis_connect(){
		try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->redis = new Predis\Client(array(
                   'host' => REDIS_HOST,
                   'port' => REDIS_PORT 
			));
           
                   //'password' => REDIS_PASSWORD,
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->redis = false;
		}
		return $this->redis;
	}
        
        /**
         * Only for request TPS LINE
         * @return type
         */
        function redis_connector(){
		try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->redis = new Predis\Client(array(
                            'host' => TPS_REDIS,
                            'port' => TPS_REDIS_PORT
			));
           
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->redis = false;
		}
		return $this->redis;
	}
		
	function errors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Wrong operator code/Wrong Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Try after some time.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
			'42'=>'Payment Authorization failed',
			'43'=>'Operator is currently down. Try after some time.',
			'44'=>'Please call customer care on 022-67242288 to activate this service.',//Transfer to kit to use this service',
			'45'=>'Complaint already taken',
			'46'=>'Wrong account/phone number',
                        '47'=>'Operator General Error',
			'48'=>'You are using an older version of Pay1. Please update the app to recharge.',
			'55'=>'Kindly create a strong password',
                                                         '60'=>'Exception Occured'   
			);
			return 	$err[$code];
	}
	function apiErrors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Wrong operator code/Wrong Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Please try this transaction after 15 minutes.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
			'42'=>"Are you sure you want to repeat this recharge?",
			'43'=>"Are you sure you want to contnue?",	
			'44'=>"No mobile or password found",
			'45'=>"The new number cannot be registered as a retailer.",
			'46'=>"Invalid mobile or password",
			'47'=>"OTP did not match. Mobile number did not change.",
			'48'=>"OTP expired. Try again.",	
			'49'=>"Your mobile did not match with our records.",	
                        '50'=>"Incorrect MOBILE NUMBER or PIN",
                        '51'=>"Leads is already created",
                        '52'=>"Retailer already exists with this mobile number",
                        '53'=>"Distributor already exists with this mobile number",
                        '54'=>"OTP did not match or expired. Try again.",
                        '55'=>"Lead generated successfully.",
                        '56'=>"Lead not generated.",
                        '57'=>"Incorrect MOBILE NUMBER or OTP",
                        '58'=>"Incorrect MOBILE NUMBER or INTEREST",
                        '59'=>"OTP has been sent to your mobile number",
                        '60'=>"Your number is already registered with Pay1",
                        '61'=>"Please wait! You will get OTP on call shortly on your registered number.. ",
                        '62'=>"Please wait! Your request is under process.. ",
                    
                        //------API Error Codes-------
                        'E000'=>'Some error occured.',
                        'E001'=>'Invalid Operation Type.',
                        'E002'=>'Account disabled.',
                        'E003'=>'Invalid inputs.',// Empty of insufficient input
                        'E004'=>'Invalid partner ID.',
                        'E005'=>'Authentication Error ( Invalid Access Key ).',
                        'E006'=>'Authentication Error ( Invalid access location ).',
                        'E007'=>'Not enough balance',
                        'E008'=>"Wrong operator code/Wrong Mobile Number.",
						'E009'=>"Wrong operator code/Wrong Subscriber id.",
						'E010'=>"Invalid amount.",
                        'E011'=>"Request already in process",
						'E012'=>"Try after some time",
						'E013'=>"Operator General Error",
						'E014'=>"Duplicate request",
						'E015'=>"Invalid Transaction Id",
						'E016'=>"Invalid operator access",
						'E017'=>"No of transactions should be less or equal to 10.",
						'E018'=>"Complaint already taken.",
                        'E019'=>"Cannot create retailer on this mobile number",
						'E020'=>"Retailer creation limit reached. Cannot create more retailers.",
				        'E021'=>"You cannot create retailer, contact Pay1",
				        'E022'=>"You have 0 kits left. Buy more retailer kits to enjoy this benefit.",
				   		'E023'=>"The Retailer could not be saved. Please, try again.",
						'E024'=>"field left empty",
						'E025'=> "Retailer not found. Cannot send OTP",
						'E026'=>"Incorrect OTP or PIN",
						'E027'=>"Authentication failure. OTP did not match.",
				
				//KYC error codes
				"E101" => "Update KYC to enjoy Toll Free Calling.",
				"E102" => "Your information is rejected.",
				"E103" => "Your information is under review. Wait for 48 hours.",
				"E104" => "Sorry! We are available between 8am to 11pm.",
				"E105" => "Incomplete data. Please update to enjoy Toll Free Calling."
			);
			return 	$err[$code];
	}
	
// 	function retailerTypes($type=null){
// 		$arr = array('1' => 'Telecom',
// 					 '2' => 'Kirana / General Stores',
// 					 '3' => 'Medical Store',
// 					 '4' => 'Cyber Cafes',
// 					 '5' => 'Travel Agency',
// 					 '6' => 'STD / PCO',
// 					 '7' => 'Footwear shops',
// 					 '8' => 'Fast Food Joints / Eateries',
// 					 '9' => 'Freelancer (without shop)',
// 					 '10' => 'Electronics',
// 					 '11' => 'Electrical and Hardware',
// 					 '12' => 'Computer Stationery',
// 					 '13' => 'Xerox and Stationery',
// 					 '14' => 'Estate Agent',
// 					 '15' => 'Clothing',
// 					 '16' => 'Bags and Other Accessories dealers',
// 					 '17' => 'Pan Bidi shop',
// 					 '18' => 'Saloon',
// 					 '19' => 'Tailors',
// 					 '20' => 'Others',
// 					 '21' => 'Mobile Store',
// 					 '22' => 'Stationery Shop',
// 					 '23' => 'Grocery Store',
// 					 '24' => 'Photocopy Store',
// 					 '25' => 'Hardware Shop'	
// 		);
		
// 		if(empty($type)) return $arr;
// 		else return $arr[$type];
// 	}
	
// 	function locationTypes($type=null){
// 		$arr = array('1' => 'Near Station',
// 					 '2' => 'Posh Area',
// 					 '3' => 'Slum Area',
// 					 '4' => 'Residential Area',
// 					 '5' => 'Market',
// 					 '6' => 'Industrial Area',
// 					 '7' => 'Commercial Area',
// 		);
		
// 		if(empty($type)) return $arr;
// 		else return $arr[$type];
// 	}
	
	function retailerTypes($type=null){
		$arr = array(
			'1' => 'Mobile Store',
        	'2' => 'Stationery Shop',
        	'3' => 'Medical Store',
			'4' => 'Grocery Store',
        	'5' => 'Photocopy Store',
			'6' => 'Travel Agency',
        	'7' => 'Hardware Shop',
        	'8'	=> 'Others'
		);
	
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function locationTypes($type=null){
		$arr = array(
			'1' => 'Residential Area',
			'2' => 'Commercial Area',
			'3' => 'Industrial Area'	
		);
	
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function structureTypes($type=null){
		$arr = array(
					'1' => 'Permanent',
					'2' => 'Temporary',
		);
		
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function kycSectionMap($section_id = null){
		$map = array(
			'1' => array(
				'fields' => array('name'),
				'documents' => array('PAN_CARD')	
			),
			'2' => array(
				'fields' => array('shopname', 'area_id', 'address', 'pin', 'latitude', 'longitude'),
				'documents' => array('ADDRESS_PROOF')
			),
			'3' => array(
				'fields' => array('shop_type', 'shop_type_value', 'location_type'),
				'documents' => array('SHOP_PHOTO')
			)
		);
		
		if(empty($section_id))
			return $map;
		else 
			return $map[$section_id];
	}
	
	function mapApiErrs($code){
		$err = array('1' => 'E002','2' => 'E001','3' => 'E000','4' => 'E003','5' => 'E008','6' => 'E010','7' => 'E003',
		'8' => 'E008', '9' => 'E003','10' => 'E013','11' => 'E013','12' => 'E013','14' => 'E013','16'=>'E013','17'=>'E013','18'=>'E013',
		'19'=>'E013','20'=>'E013','21'=>'E013','22'=>'E013','23'=>'E013','24'=>'E013','25'=>'E013','26'=>'E007','27'=>'E013','29'=>'E013',
		'30'=>'E000','33'=>'E010','34'=>'E010','35'=>'E013','36'=>'E013','37'=>'E012','38'=>'E011','39'=>'E009','40'=>'E013','41'=>'E013','42'=>'E013','43'=>'E016','45'=>'E018');
		return 	$err[$code];
	}
   	function ussdErrors($code){
			$err = array(      
                    "1"=>"Unknown subscriber",
                    "105"=>"SDP:SIM Card data does not exist.",
                    "11"=>"Teleservice not provisioned",
                    "110"=>"Map Dialog P Abort Indication : Source is MAP",
                    "111"=>"Map Dialog P Abort Indication : Source is TCAP",
                    "112"=>"Map Dialog P Abort Indication : Source is Network",
                    "13"=>"CallBarred",
                    "130"=>"Map Dialogue rejected refuse reason :invalid dest reference",
                    "132"=>"Map Dialog Rejected Refuse Reason: Application Context not supported",
                    "137"=>"Map Dialog Rejected Provider Reason: Resource Limitation",
                    "138"=>"Map Dialogue rejected provider reason : Maintenance Activity",
                    "150"=>"Map Dialog User Abort User Reason: User specific reason",
                    "151"=>"MAP_DLGUA_UsrRsn_UsrResourceLimitation",
                    "153"=>"Map Dialog User Abort User Reason: App procedure cancelled",
                    "176"=>"Invalid service code",
                    "177"=>"Access Denied Blacklist",
                    "178"=>"CDREC_IMSIBlackListed",
                    "183"=>"VLR black listed",
                    "192"=>"Unexpected session release server/3rd party APP",
                    "193"=>"No reseponse with configurable time from servers/3rd party application",
                    "197"=>"Application Specific Error from 3rd partyClient",
                    "209"=>"Timer expired for SRISM Response",
                    "210"=>"Timer expired for N/W initaited USSRN",
                    "211"=>"Timer expired for Mobile initaited USSRN",
                    "229"=>"Received Invalid MAP message",
                    "231"=>"IMSI unavailable in SRISM Response",
                    "232"=>"VLR unavailable in SRISM Response",
                    "235"=>"Exit Option Selected By Subscriber",
                    "236"=>"Menu is Subscriber based/but subscriber type could not be resolved at the point in transaction.",
                    "242"=>"Service not configured/Menu Incomplete",
                    "243"=>"Invalid user input",
                    "244"=>"Access Denied Blacklisted",
                    "245"=>"Client not connected",
                    "27"=>"Absent Subscriber",
                    "31"=>"subscriber busy for Mt sms",
                    "34"=>"System failure",
                    "35"=>"dataMissing",
                    "36"=>"unexpectedDataValue",
                    "9"=>"Illegal subscriber",
                    "72"=>"Ussd Busy"
			);
            return 	$err[$code];
        }      
	function smsProdCodes($code){
		$prod = array(		
				'1'=>array(
					'operator'=>'Aircel',
					'params'=>array('method' => 'mobRecharge','operator'=>'1','type'=>'flexi')
				),
				'2'=>array(
					'operator'=>'Airtel',
					'params'=>array('method' => 'mobRecharge','operator'=>'2','type'=>'flexi')
				),
				'3'=>array(
					'operator'=>'BSNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi')
				),
				'4'=>array(
					'operator'=>'Idea',
					'params'=>array('method' => 'mobRecharge','operator'=>'4','type'=>'flexi')
				),
				'5'=>array(
					'operator'=>'Loop/BPL',
					'params'=>array('method' => 'mobRecharge','operator'=>'5','type'=>'flexi')
				),
				'6'=>array(
					'operator'=>'MTS',
					'params'=>array('method' => 'mobRecharge','operator'=>'6','type'=>'flexi')
				),
				'7'=>array(
					'operator'=>'Reliance CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'7','type'=>'flexi')
				),
				'8'=>array(
					'operator'=>'Reliance GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'8','type'=>'flexi')
				),
				'9'=>array(
					'operator'=>'Tata Docomo',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi')
				),
				'10'=>array(
					'operator'=>'Tata Indicom',
					'params'=>array('method' => 'mobRecharge','operator'=>'10','type'=>'flexi')
				),
				'11'=>array(
					'operator'=>'Uninor',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi')
				),
				'12'=>array(
					'operator'=>'Videocon',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi')
				),
				'13'=>array(
					'operator'=>'Virgin CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'13','type'=>'flexi')
				),
				'14'=>array(
					'operator'=>'Virgin GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'14','type'=>'flexi')
				),
				'15'=>array(
					'operator'=>'Vodafone',
					'params'=>array('method' => 'mobRecharge','operator'=>'15','type'=>'flexi')
				),
				'27'=>array(
					'operator'=>'Tata SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi','special'=>1)
				),
				'28'=>array(
					'operator'=>'Videocon SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi','special'=>1)
				),
				'29'=>array(
					'operator'=>'Uninor SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi','special'=>1)
				),
				'30'=>array(
					'operator'=>'MTNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi')
				),
				'31'=>array(
					'operator'=>'MTNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi','special'=>1)
				),
				'34'=>array(
					'operator'=>'BSNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi','special'=>1)
				),
				'16'=>array(
					'operator'=>'Airtel DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'1','type'=>'flexi')
				),
				'17'=>array(
					'operator'=>'Big TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'2','type'=>'flexi')
				),
				'18'=>array(
					'operator'=>'Dish TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'3','type'=>'flexi')
				),
				'19'=>array(
					'operator'=>'Sun TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'4','type'=>'flexi')
				),
				'20'=>array(
					'operator'=>'Tata Sky DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'5','type'=>'flexi')
				),
				'21'=>array(
					'operator'=>'Videocon DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'6','type'=>'flexi')
				),
				'22'=>array(
					'operator'=>'Dil Vil Pyar Vyar',
					'params'=>array('method' => 'vasRecharge','operator'=>'22','type'=>'fix')
				),
				'23'=>array(
					'operator'=>'Naughty Jokes',
					'params'=>array('method' => 'vasRecharge','operator'=>'23','type'=>'fix')
				),
				'24'=>array(
					'operator'=>'PNR Alert',
					'params'=>array('method' => 'vasRecharge','operator'=>'24','type'=>'fix')
				),
				'25'=>array(
					'operator'=>'Instant Cricket',
					'params'=>array('method' => 'vasRecharge','operator'=>'25','type'=>'fix')
				),
				'32'=>array(
					'operator'=>'Chatpati Baate Mini Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'32','type'=>'fix')
				),
				'33'=>array(
					'operator'=>'Chatpati Baate Mega Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'33','type'=>'fix')
				),
				'35'=>array(
					'operator'=>'Ditto TV',
					'params'=>array('method' => 'vasRecharge','operator'=>'35','type'=>'flexi')
				),
                                /*  36 - Docomo Postpaid 
                                    37 - Loop Mobile PostPaid
                                    38 - Cellone PostPaid 
                                    39 - IDEA Postpaid 
                                    40 - Tata TeleServices PostPaid 
                                    41 - Vodafone Postpaid*/
                                '36'=>array(
					'operator'=>'Docomo Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'1','type'=>'flexi')
				),
                                '37'=>array(
					'operator'=>'Loop Mobile PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'2','type'=>'flexi')
				),
                                '38'=>array(
					'operator'=>'Cellone PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'3','type'=>'flexi')
				),
                                '39'=>array(
					'operator'=>'IDEA Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'4','type'=>'flexi')
				),
                                '40'=>array(
					'operator'=>'Tata TeleServices PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'5','type'=>'flexi')
				),
                                '41'=>array(
					'operator'=>'Vodafone Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'6','type'=>'flexi')
				),
				
                                '42'=>array(
					'operator'=>'Airtel Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'7','type'=>'flexi')
				),
				
                                '43'=>array(
					'operator'=>'Reliance Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'8','type'=>'flexi')
				),
				
                                '44'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'pay1Wallet','operator'=>'1','type'=>'flexi')
				),
								'45'=>array(
					'operator'=>'Reliance Energy (Mumbai)',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'1','type'=>'flexi')
				),
								'46'=>array(
					'operator'=>'BSES Rajdhani',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'2','type'=>'flexi')
				),
								'47'=>array(
					'operator'=>'BSES Yamuna',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'3','type'=>'flexi')
				),
								'48'=>array(
					'operator'=>'North Delhi Power Limited',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'4','type'=>'flexi')
				),
								'49'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'5','type'=>'flexi')
				),
								'50'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'6','type'=>'flexi')
				),
								'51'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'7','type'=>'flexi')
				)
			);
			return  isset($prod[$code]['params']) ? $prod[$code]['params'] : array() ;
	}
	
	function getRetailerTrnsDetails($mobile){
		$data = $this->getMemcache($mobile."_retTxn");
		return $data;
	}
	
	function setRetailerTrnsDetails($mobile,$data){
		$this->setMemcache($mobile."_retTxn",$data,3*24*60*60);
		return;
	}
	
	function getSalesmanDeviceData($mobile){
		$data = $this->getMemcache($mobile."_device_data");
		return $data;
	}
	
	function setSalesmanDeviceData($mobile, $data){
		$data['trans_type'] = $data['trans_type'] . "_distributor";
		$this->setMemcache($mobile."_device_data", $data, 3*24*60*60);
		return;
	}
	
	function shopTransactionUpdate($type,$amount,$ref1_id,$ref2_id,$user_id=null,$discount=null,$type_flag = null,$note=null,$dataSource=null){
                                    if(is_null($dataSource)):
                                        
                                 
		$this->data = null;
		$transObj = ClassRegistry::init('ShopTransaction');
                                        
                                                            $this->data['ShopTransaction']['ref1_id'] = $ref1_id;
                                                            $this->data['ShopTransaction']['ref2_id'] =  $ref2_id;
                                                            $this->data['ShopTransaction']['amount'] =  $amount;
                                                            $this->data['ShopTransaction']['type'] =  $type;
                                                            $this->data['ShopTransaction']['timestamp'] =  date('Y-m-d H:i:s');
                                                            $this->data['ShopTransaction']['date'] =  date('Y-m-d');
                                                            $this->data['ShopTransaction']['note'] =  $note;
                                                            if($type == RETAILER_ACTIVATION){
                                                                    $this->data['ShopTransaction']['confirm_flag'] =  1;
                                                            }

                                                            if($user_id != null){
                                                                    $this->data['ShopTransaction']['user_id'] =  $user_id;
                                                            }
                                                            if($discount != null){
                                                                    $this->data['ShopTransaction']['discount_comission'] =  $discount;
                                                            }
                                                            if($type_flag != null){
                                                                    $this->data['ShopTransaction']['type_flag'] =  $type_flag;
                                                            }
                                                            $transObj->create();
                                                            if($transObj->save($this->data)){
                                                                    return $transObj->id;
                                                            }
                                                            else return false;
                
                                    else:
                                                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Creating ST ',FILE_APPEND | LOCK_EX);
                                                    $confirm_flag=($type==RETAILER_ACTIVATION)?1:0;
                                                    $user_id=  !is_null($user_id)?$user_id:null;
                                                    $discount_comission=!is_null($discount)?$discount:null;
                                                    $type_flag=!is_null($type_flag)?$type_flag:null;
                                                    $date=date('Y-m-d');
                                                    $timestamp=date('Y-m-d H:i:s');
                                                    $sql=" Insert into shop_transactions(ref1_id,ref2_id,amount,type,timestamp,date,note,confirm_flag,user_id,discount_comission,type_flag)  "
                                                            . "  values('{$ref1_id}','{$ref2_id}','{$amount}','{$type}','{$timestamp}','{$date}','{$note}','{$confirm_flag}','{$user_id}','{$discount_comission}','{$type_flag}' ) ";

                                                   if($dataSource->query($sql)):        
                                                        $lastInserIdQuery = $dataSource->query("SELECT LAST_INSERT_ID() as id FROM shop_transactions limit 1 ");
                                                        //return $dataSource->lastInsertId();
                                                         $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"ST id :  {$lastInserIdQuery[0][0]['id']}",FILE_APPEND | LOCK_EX); 
                                                        return $lastInserIdQuery[0][0]['id'];
                                                   else:
                                                        $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"ST Failed :  {$lastInserIdQuery[0][0]['id']}",FILE_APPEND | LOCK_EX); 
                                                        return false;
                                                   endif;
                                  
                                  endif;
	}
	
	function addAppRequest($method,$mobile,$amount,$opr,$type,$ret_id){
		
		$ret = $this->addMemcache("request_".$mobile."_".$amount."_".$opr."_".$ret_id, 1, 5*60);
		if($ret !== false) return 1;
		else return 0;
		/*$reqObj = ClassRegistry::init('Request');
		
		//$reqObj->query("DELETE FROM requests WHERE timestamp < now() - 300");
		
		$this->data['Request']['method'] = $method;
		$this->data['Request']['mobile'] = $mobile;
		$this->data['Request']['amount'] =  $amount;
		$this->data['Request']['operator'] =  $opr;
		$this->data['Request']['type'] =  $type;
		$this->data['Request']['timestamp'] =  NULL;
		$reqObj->create();
        $insertQry = "INSERT INTO requests (method,mobile,amount,operator,type,timestamp) VALUES ('".$method."','".$mobile."','".$amount."','".$opr."','".$type."',NULL)";
        $lastId = "SELECT LAST_INSERT_ID() as 'insertId'";
        
        if(in_array(strtolower($method),array('cashpgpayment'))){
            if($reqObj->query($insertQry)){
                $lastId_arr = $reqObj->query($lastId);
                return $lastId_arr[0][0]['insertId'];
            }
        }elseif($reqObj->save($this->data)){
			return $reqObj->id;
		}        
		
        $reqObj->query("INSERT INTO requests_dropped (retailer_id,method,mobile,amount,operator,timestamp) VALUES (".$_SESSION['Auth']['id'].",'$method','$mobile',$amount,$opr,'".date('Y-m-d H:i:s')."')");
        return null;*/
		
		
	}
	
	function deleteAppRequest($mobile,$amount,$opr,$ret_id){
		$this->delMemcache("request_".$mobile."_".$amount."_".$opr."_".$ret_id);
		/*$reqObj = ClassRegistry::init('Request');
		
		$reqObj->query("DELETE FROM requests WHERE mobile='$mobile' AND amount=$amount");*/
	}
	
	function addComment($userId,$retId,$transId,$test,$loggedInUser,$name=null,$tag_id,$call_type_id){
		$userObj = ClassRegistry::init('User');
		
// 		if(!empty($test)){
// 			$medium = -1;
// 			if(!empty($transId)){
// 				$medium = $userObj->query("select api_flag from vendors_activations where ref_code = '$transId'");
// 				$medium = $medium[0]['vendors_activations']['api_flag'];
// 			}

			$userObj->query("insert into comments(users_id,retailers_id,mobile,ref_code,comments,created,tag_id,call_type_id,date) values('$userId','$retId','$loggedInUser','$transId','".addslashes($test)."','".date('Y-m-d H:i:s')."','".$tag_id."','".$call_type_id."','".date('Y-m-d')."')");	
			if(!empty($transId)){
				$slaveObj = ClassRegistry::init('Slaves');
				$comments_count = $slaveObj->query("select ref_code, count from comments_count where ref_code = '$transId' and date = '".date('Y-m-d')."'");
				$va = $slaveObj->query("select vendor_id, product_id, api_flag from vendors_activations where ref_code = '$transId'");
				
				if($comments_count)
					$userObj->query("update comments_count set count = count + 1 where ref_code = '$transId' and date = '".date('Y-m-d')."'");
				else
					$userObj->query("insert into comments_count(ref_code, count, vendor_id, product_id, retailer_id, medium, date) values('$transId', 1, ".$va[0]['vendors_activations']['vendor_id'].", ".$va[0]['vendors_activations']['product_id'].", '$retId', ".$va[0]['vendors_activations']['api_flag'].", '".date('Y-m-d')."')");
			}
			/*$user = $this->General->getUserDataFromMobile($loggedInUser);
			$data['user'] = $user['name'];
			if(!empty($transId)){
				$txid = $transId;
				$data['txtype'] = 1;
				
				$extra = $userObj->query("SELECT vendors.shortForm,vendors_activations.amount,products.name FROM vendors_activations,vendors,products WHERE products.id = product_id AND vendors.id = vendor_id AND ref_code = '$transId'");	
				$data['extra'] = $extra['0']['vendors']['shortForm'] . " | " . $extra['0']['products']['name'] . " | " . $extra['0']['vendors_activations']['amount'];
			}
			else if(!empty($retId)){
				$retData = $userObj->query("SELECT mobile FROM retailers WHERE id = $retId");	
		
				$txid = $retData['0']['retailers']['mobile'];
				$data['txtype'] = 2;
			}
			else {
				$user = $this->General->getUserDataFromId($userId);
				$txid = $user['mobile'];
				$data['txtype'] = 3;
			}
			$data['txid'] = $txid;
			$data['process'] = 'cc';
			$data['msg'] = $test;
			$this->General->curl_post_async("http://pay1.in/cclive/server.php",$data);
			*/
		
// 		}
	}
	
	function shopCreditDebitUpdate($type,$amount,$from,$to,$to_groupId,$desc,$numbering,$api_flag=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		if($api_flag == null)$api_flag = 0;
		if(!empty($from))
			$transObj->query("INSERT INTO shop_creditdebit (from_id,to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($from,$to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
		else 
			$transObj->query("INSERT INTO shop_creditdebit (to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
	}
	
	function transactionsOnRetailerActivation($retailer_id,$product,$user_id,$amount,$ip=NULL,$apiflag=NULL,$dataSource=null){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->recursive = -1;
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		
		//transaction entries
		$perData = $this->getCommissionPercent($ret_shop['slab_id'],$product);
		$percent = (isset($perData['percent']))?$perData['percent']:0;
		$trans_id = $this->shopTransactionUpdate(RETAILER_ACTIVATION,$amount,$retailer_id,$product,$user_id,$percent,null,$ip,$dataSource);
		if($trans_id === false) return false;
		
		$commission_retailer = $percent*$amount/100;
		//$commission_retailer = $this->calculateCommission($retailer_id,RETAILER,$product,$amount,$ret_shop['slab_id']);
		
		
		$distributor_id = $ret_shop['parent_id'];
		$dist_shop = $this->getShopDataById($distributor_id,DISTRIBUTOR);
		/*
		$superdistributor_id = $dist_shop['parent_id'];
		$superdist_shop = $this->getShopDataById($superdistributor_id,SUPER_DISTRIBUTOR);
		
		$commission_distributor = $this->calculateCommission($distributor_id,DISTRIBUTOR,$product,$amount);
		
		$commission_superdistributor = $this->calculateCommission($superdistributor_id,SUPER_DISTRIBUTOR,$product,$amount);
		$sd_percent = $this->getCommissionPercent($superdist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$commission_superdistributor,$superdistributor_id,$trans_id,null,$sd_percent);
		
		if(TAX_MODEL == 0){
			$tds_superdistributor = $this->calculateTDS($commission_superdistributor);
			$this->shopTransactionUpdate(TDS_SUPERDISTRIBUTOR,$tds_superdistributor,$superdistributor_id,$trans_id);
			$commission_superdistributor -= $tds_superdistributor;
		}
		$d_percent = $this->getCommissionPercent($dist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$commission_distributor,$distributor_id,$trans_id,null,$d_percent);
		if($superdist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
			$tds_distributor = $this->calculateTDS($commission_distributor);
			$this->shopTransactionUpdate(TDS_DISTRIBUTOR,$tds_distributor,$distributor_id,$trans_id);
			$commission_distributor -= $tds_distributor;
		}*/
		
		if($commission_retailer > 0){ 
			$this->shopTransactionUpdate(COMMISSION_RETAILER,$commission_retailer,$retailer_id,$trans_id,null,$percent,$dataSource);
			if($dist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
				$tds_retailer = $this->calculateTDS($commission_retailer);
				$this->shopTransactionUpdate(TDS_RETAILER,$tds_retailer,$retailer_id,$trans_id,$dataSource);
				$commission_retailer -= $tds_retailer;	
			}
		}
		
		$service_charge = (isset($perData['service_charge']) && !empty($perData['service_charge']))? ($perData['service_charge'] * $amount/100): (in_array($apiflag,array(0,2)) ? 0 : 0);
                if($product > 35){
                    $this->General->logData('/mnt/logs/commisionlog.txt',  json_encode($perData)." | service charge : ".$service_charge." | product :".$product."| amount :".$amount);
                }
		if($service_charge > 0){
			if($perData['service_tax'] == 1 && !empty($perData['service_charge'])){
				$service_charge = ($service_charge<5)? 5 : $service_charge;
                                
                                $service_charge = $service_charge + ($service_charge * 14.5/100);
				//$service_charge = $service_charge*114.5/100;
			}
			
		}
			
		if($commission_retailer > 0 && $commission_retailer < $service_charge){
			$service_charge = 0;
		}
		
		if($service_charge > 0){
			$this->shopTransactionUpdate(SERVICE_CHARGE,$service_charge,$retailer_id,$trans_id,RETAILER,$dataSource);
		}
		
		$service_charge = round($service_charge,2);
		//balance update
		//$amount_distributor = $commission_distributor - $commission_retailer;
		//$amount_superdistributor = $commission_superdistributor - $commission_distributor;
		$amount_retailer = $amount - $commission_retailer + $service_charge;
		
		$bal = $this->shopBalanceUpdate($amount_retailer,'subtract',$retailer_id,RETAILER,$dataSource);
                
                                    if($bal<0){
                                         $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Negative bal encountered Rolling Back  : {$bal}",FILE_APPEND | LOCK_EX); 
                                         return false;
                                    }
                                    
		$this->addOpeningClosing($retailer_id,RETAILER,$trans_id,$bal+$amount_retailer,$bal,$dataSource);
		//$this->shopBalanceUpdate($amount_distributor,'add',$distributor_id,DISTRIBUTOR);
		//$this->shopBalanceUpdate($amount_superdistributor,'add',$superdistributor_id,SUPER_DISTRIBUTOR);
		return array($bal,$trans_id,$service_charge);
	}
	
	function addOpeningClosing($shop_id,$group_id,$shoptrans_id,$opening,$closing,$dataSource=null){
		$prodObj = is_null($dataSource)?ClassRegistry::init('Product'):$dataSource;
		$prodObj->query("INSERT INTO opening_closing (shop_id,group_id,shop_transaction_id,opening,closing,timestamp) VALUES ($shop_id,$group_id,$shoptrans_id,$opening,$closing,'".date('Y-m-d H:i:s')."')");
                                     $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Adding Opening Closing entry  : ",FILE_APPEND | LOCK_EX); 
	}
	
	function createVendorActivation($vendor_id,$product_id,$api_flag,$mobile,$amount,$shop_trans_id,$ret_id,$param = null,$dataSource=null){
                
		if(empty($api_flag))$api_flag = 0;
                
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$this->data = null;
		$this->data['VendorsActivation']['vendor_id'] = $vendor_id;
		$this->data['VendorsActivation']['product_id'] =  $product_id;
		$this->data['VendorsActivation']['mobile'] =  $mobile;
		$this->data['VendorsActivation']['amount'] =  $amount;
		$this->data['VendorsActivation']['shop_transaction_id'] =  $shop_trans_id;
		$this->data['VendorsActivation']['retailer_id'] =  $ret_id;
		$this->data['VendorsActivation']['timestamp'] =  date('Y-m-d H:i:s');
		$this->data['VendorsActivation']['date'] =  date('Y-m-d');
		$this->data['VendorsActivation']['api_flag'] =  $api_flag;
                $this->data['VendorsActivation']['ref_code'] =  uniqid(rand(0, 1000));
                //$this->data['VendorsActivation']['hour'] =  date('H');
		if($param != null){
			$this->data['VendorsActivation']['param'] =  $param;
		}
		
		$data = $vendorActObj->query("SELECT discount_commission FROM vendors_commissions WHERE vendor_id=$vendor_id AND product_id=$product_id");
		$this->data['VendorsActivation']['discount_commission'] = $data['0']['vendors_commissions']['discount_commission'];
                		                
                                    $timestamp=date('Y-m-d H:i:s');
                                    $date=date('Y-m-d');
                                    $ref_code=uniqid(rand(0, 1000));
                                    $param=!is_null($param)?$param:null;
                                    $sql="Insert into vendors_activations(vendor_id,product_id,mobile,amount,shop_transaction_id,retailer_id,timestamp,date,api_flag,ref_code,param,discount_commission)"
                                            . "  values('{$vendor_id}','{$product_id}','{$mobile}','{$amount}','{$shop_trans_id}','{$ret_id}','{$timestamp}','{$date}','{$api_flag}','{$ref_code}','{$param}','{$data['0']['vendors_commissions']['discount_commission']}') ";
		
		//$vendorActObj->create();
                                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Creating VA entry ',FILE_APPEND | LOCK_EX);
                                            //if($vendorActObj->save($this->data)){
                                            if($dataSource->query($sql)){
                                                        $lastInserIdQuery = $dataSource->query("SELECT LAST_INSERT_ID() as id FROM vendors_activations limit 1 ");
			//$id = $dataSource->lastInsertId();
                                                        $id = $lastInserIdQuery[0][0]['id'];
                                                        $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA id : {$id} ",FILE_APPEND | LOCK_EX);
                                                        
			$ref_code =  3022*100000000 + intval($id);
			//$ref_code =  "3022" . sprintf('%08d', $id);
			if($dataSource->query("UPDATE vendors_activations SET ref_code = '$ref_code' WHERE id=$id")){
			//if($vendorActObj->updateAll(array('VendorsActivation.ref_code' => $ref_code), array('VendorsActivation.id' => $id))){
                                                            $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," Updating ref code in VA refcode: {$ref_code} ",FILE_APPEND | LOCK_EX);
				return $ref_code;
			}
			else {
                file_put_contents('/mnt/logs/vendor_activation_failure.log', " error in updating vendor_activation \n" , FILE_APPEND | LOCK_EX);
				//$dataSource->query("DELETE FROM vendors_activations WHERE id = $id");
                                                                  $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," Updating ref code in VA failed ",FILE_APPEND | LOCK_EX);
				return false;
			}
		}else{
            file_put_contents('/mnt/logs/vendor_activation_failure.log', " error in saving vendoractivation object : data provided : (".json_encode($this->data).") \n" , FILE_APPEND | LOCK_EX);
            $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA query Failed  ",FILE_APPEND | LOCK_EX);
        }
		
		return false;
	}
	
	function calculateExpectedEarning($data){
		if($data['vendors']['update_flag'] == 1){
			$exp_earn = $data['earnings_logs']['incoming'] - $data['earnings_logs']['invested'];
		}
		else if($data['earnings_logs']['vendor_id'] == 36){
			$exp_earn = intval($data['earnings_logs']['invested']*3.7/100);
		}
		else if($data['earnings_logs']['vendor_id'] == 62){
			$exp_earn = intval($data['earnings_logs']['invested']*3.5/100);
		}
		else $exp_earn = $data['earnings_logs']['expected_earning'];
		
		return $exp_earn;
	}
	
	function updateVendorActivation($ref_code,$trans_id,$status,$code,$cause,$extra,$opr_id,$vendor_id,$discount_comm=null){
		//$vendorActObj = ClassRegistry::init('VendorsActivation');
		//$vendorActObj->recursive = -1;
		
		//$vendorActObj->query("update vendors_activations set vendor_refid = '".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id' where ref_code='".$ref_code."'");
       	if(empty($vendor_id))return;
		
		if(empty($discount_comm)){
        		$this->General->update_in_vendors_activations(array('vendor_refid'=>$trans_id,'code'=>$code,'cause'=>addslashes($cause),'status'=>$status,'extra'=>addslashes($extra),'operator_id'=>$opr_id,'vendor_id'=>$vendor_id),array('ref_code'=>$ref_code));
                        //$this->General->update_in_vendors_activations("vendor_refid='".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id',vendor_id='$vendor_id'","ref_code='$ref_code'");
       	}
       	else {
       		$this->General->update_in_vendors_activations(array('vendor_refid'=>$trans_id,'code'=>$code,'cause'=>addslashes($cause),'status'=>$status,'extra'=>addslashes($extra),'operator_id'=>$opr_id,'vendor_id'=>$vendor_id,'discount_commission'=>$discount_comm),array('ref_code'=>$ref_code));
                //$this->General->update_in_vendors_activations("vendor_refid='".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id',vendor_id='$vendor_id',discount_commission='$discount_comm'","ref_code='$ref_code'");
       	}
      }
	
	function createTransaction($prodId,$venId,$api_flag,$mobNo,$amt,$custId=null,$ip=null){
		//$shopData = $this->getShopDataById($_SESSION['Auth']['id'],$_SESSION['Auth']['User']['group_id']);
            
		$this->General->logData('/mnt/logs/createTransaction.log',"In create Transaction: $mobNo $amt $venId");
		 
		if($api_flag != 4){//api partner
		$check_wait_time = $this->addMemcache("requested_user_".$_SESSION['Auth']['id'],$_SESSION['Auth']['id'] , 6);
                
                if(!$check_wait_time){
                    return array('status'=>'failure','code'=>'37','description'=>$this->errors(37));	
                }
		}
                
                try{
                	$this->General->logData('/mnt/logs/createTransaction.log',"Getting retailer balance : $mobNo $amt $venId");
                	
                $balance = $this->getBalance($_SESSION['Auth']['id'],$_SESSION['Auth']['User']['group_id']);
		
		if($balance < $amt){
                                                        $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
			return array('status'=>'failure','code'=>'26','description'=>$this->errors(26));	
		}
		else {
                                                        /*
                                                        * Create Transaction
                                                        * Start
                                                        */
			
			$this->General->logData('/mnt/logs/createTransaction.log',"Starting begin transaction : $mobNo $amt $venId");
			 
                                                       $userObj = ClassRegistry::init('User');
                                                       $dataSource=$userObj->getDataSource();
                                                       $dataSource->begin();
                                                       $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Created DataSource Object ',FILE_APPEND | LOCK_EX);
                                                       
			$exists = $this->General->checkIfUserExists($mobNo);
			if(!$exists){
				$user = $this->General->registerUser($mobNo,RETAILER_REG);
				$user = $user['User'];
			}
			else {
				$user = $this->General->getUserDataFromMobile($mobNo);
			}
			$ret = $this->transactionsOnRetailerActivation($_SESSION['Auth']['id'],$prodId,$user['id'],$amt,$ip,$api_flag,$dataSource);
			if($ret === false){
                                                                         $dataSource->rollback();
                                                                            $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
				return array('status'=>'failure','code'=>'30','description'=>$this->errors(26));
			}            
			$ref_id = $this->createVendorActivation($venId,$prodId,$api_flag,$mobNo,$amt,$ret[1],$_SESSION['Auth']['id'],$custId,$dataSource);
			if($ref_id === false || empty($ref_id)){                
                $req_content = $venId."|".$prodId."|".$api_flag."|".$mobNo."|".$amt."|".$ret[1]."|".$_SESSION['Auth']['id']."|".$custId;
                file_put_contents('/mnt/logs/vendor_activation_failure.log', "vendor_act param : ".$req_content." : result_ref_id - ".$ref_id."  var_dump : ". json_encode($ref_id)." \n" , FILE_APPEND | LOCK_EX);
				$shop_transid = $ret[1];
                                                                           $dataSource->rollback();
                                                                             $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA failed",FILE_APPEND | LOCK_EX);
				/*
				$vendorActObj = ClassRegistry::init('VendorsActivation');
				$vendorActObj->query("DELETE FROM shop_transactions WHERE id = '$shop_transid'");
				$vendorActObj->query("DELETE FROM opening_closing WHERE shop_transaction_id	= '$shop_transid'");
				$vendorActObj->query("UPDATE retailers SET balance = balance + $amt, modified = '".date('Y-m-d H:i:s')."' WHERE id= '".$_SESSION['Auth']['id']."'");
				*/
                                                                          $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);   
				return array('status'=>'failure','code'=>'30','description'=>$this->errors(30));
			}
                                                        else 
                                                    { 
                                                            $dataSource->commit(); 
                                                               $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," DONE ! ! !  ",FILE_APPEND | LOCK_EX);
                                                               $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
                                                            return array('status'=>'success','tranId'=>$ref_id,'balance'=>$ret[0],'service_charge'=>$ret[2]);
                                                     }		
		}
                }
                catch(Exception $e)
                {
                    
                    $dataSource->rollback();
                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','in Catch Exception occured ',FILE_APPEND | LOCK_EX);
                    $this->General->logData('/mnt/log/Exception Encountered : '.date('Y-m-d').'.log',json_encode($e),FILE_APPEND | LOCK_EX);
                    return array('status'=>'failure','code'=>'60','description'=>$this->errors(60));
                }
                
	}
	
	
	function setProdVendorHealth($vendor_id,$prod_id,$status){
		$info = $this->getVendorInfo($vendor_id);
		
		$map_arr = array('9'=>array(9,10,27),'10'=>array(9,10,27),'27'=>array(9,10,27),'11'=>array(11,29),'29'=>array(11,29),'7'=>array(7,8),'8'=>array(7,8),'7'=>array(7,8),'12'=>array(12,28),'28'=>array(12,28),'3'=>array(3,34),'34'=>array(3,34),'30'=>array(30,31),'31'=>array(30,31));
		$prod_arr = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
		        		
		$prod_id = isset($prod_arr[$prod_id]) ? $prod_arr[$prod_id] : $prod_id;
		$key = "health_$vendor_id"."_".$prod_id."_".date('Ymd');
        
        $cap_inprocess_limit = $this->getMemcache("cap_inprocess_$prod_id"."_$vendor_id");        
		$max_cap = (!empty($cap_inprocess_limit)) ? ($cap_inprocess_limit * 4) :50;
        
		$get = $this->getMemcache($key);
		$prods = isset($map_arr[$prod_id]) ? $map_arr[$prod_id] : array($prod_id);
		
		if($info['update_flag'] == 0){
			if($status == 1){
				if($get === false){
					$this->setMemcache($key,0,2*60*60);
				}
				else if($get > 0){
                    $health_percent = 45;
					$val = $this->decrementMemcache($key);
					if($val < ($max_cap * $health_percent)/100 && $this->getMemcache("disabled_$vendor_id"."_".$prod_id)){
						$vendorActObj = ClassRegistry::init('VendorsActivation');
						
						$prod_name = array();
						foreach($prods as $p){
							$ex_data = $vendorActObj->query("SELECT oprDown FROM vendors_commissions WHERE vendor_id = $vendor_id AND product_id = $p");
							if($ex_data[0]['vendors_commissions']['oprDown'] == 1){
								$vendorActObj->query("UPDATE vendors_commissions SET oprDown = 0 WHERE vendor_id = $vendor_id AND product_id = $p AND oprDown = 1");
								$prodInfo = $this->setProdInfo($p);
								$prod_name[] = $prodInfo['name'];
							}
						}
						if(!empty($prod_name)){
							$this->General->sendMails("(SOS)Enabling ".implode(",", $prod_name) ." of vendor ".$info['company'],"Transactions cleared so activated it automatically",array('backend@mindsarray.com'),'mail');
							$this->delMemcache("disabled_$vendor_id"."_".$prod_id);
	                        $this->General->logData('/mnt/logs/vendor_status.txt',"Enabling : $vendor_id"."_".$prod_id);
						}
                        //$this->setMemcache($key,0,30*60);
					}
                    
					$this->General->logData('/mnt/logs/vendor_status.txt',"decrement:: $key: $val");
				}
			}
			else if($status == 0){
				if($get === false){
					$this->setMemcache($key,1,2*60*60);
				}
				else {
					$val = $this->incrementMemcache($key);
					if($val > $max_cap){
						$vendorActObj = ClassRegistry::init('VendorsActivation');
						$prod_name = array();
						foreach($prods as $p){
							$ex_data = $vendorActObj->query("SELECT oprDown FROM vendors_commissions WHERE vendor_id = $vendor_id AND product_id = $p");
							if($ex_data[0]['vendors_commissions']['oprDown'] == 0){
								$vendorActObj->query("UPDATE vendors_commissions SET oprDown = 1 WHERE vendor_id = $vendor_id AND product_id = $p AND oprDown = 0");
								$prodInfo = $this->setProdInfo($p);
								$prod_name[] = $prodInfo['name'];
							}
						}
						if(!empty($prod_name)){
							$this->General->sendMails("(SOS)Disabling ".implode(",", $prod_name) ." of vendor ".$info['company'],"Lot of transactions are going in process. Kindly check and activate it manually",array('backend@mindsarray.com'),'mail');
							//$this->setMemcache($key,0);
							$this->setMemcache("disabled_$vendor_id"."_".$prod_id,1);
	                        $this->General->logData('/mnt/logs/vendor_status.txt',"Disabling : $vendor_id"."_".$prod_id);
						}
					}
					$this->General->logData('/mnt/logs/vendor_status.txt',"increment:: $key: $val");
				}
			}
		}
	}
	
	function updateTransaction($tranId,$venTranId,$status,$code,$desc,$extra=null,$opr_id=null,$vendor_id=null,$prod_id=null,$discount_comm=null){
		if($status == 'success') {
			$status = TRANS_SUCCESS;
		}
		else if($status == 'failure') {
			$status = TRANS_REVERSE;
		}
		else if($status == 'pending') {
			$status = 0;
		}
		if(!empty($vendor_id) && !empty($prod_id))$this->setProdVendorHealth($vendor_id,$prod_id,$status);
		$this->unlockTransaction($tranId);
		if(is_null($extra))
		$extra = '';
		
		$this->updateVendorActivation($tranId,$venTranId,$status,$code,$desc,$extra,$opr_id,$vendor_id,$discount_comm);
		if($status == TRANS_REVERSE){
			//$err = ($code == 5) ? 4 : (($code == 6) ? 3 : null);
			$this->reverseTransaction($tranId,1,$code);
		}
	}
	
	function checkStatus($transId,$vendor_id){
		$vendors = $this->addMemcache("transCheck$transId@$vendor_id",1,5*60);
		//$this->General->logData("/tmp/status.txt",date('Y-m-d H:i:s')."::$transId: ".json_encode($vendors));
		
		if($vendors === false){
			return false;
		}
		else {
			$this->delMemcache("transCheck$transId@$vendor_id");
			return true;
		}
		/*if(!in_array($vendor_id,$vendors)){
			return true;
		}*/
	}
	
	
	function autopullbackTransaction($transId,$vendorId){
		if(!$this->checkStatus($transId,$vendorId)){
			return;
		}
		$vendorActObj = ClassRegistry::init('VendorsActivation');

		$result = $vendorActObj->query("SELECT vendors_activations.*,retailers.mobile,shop_transactions.* FROM vendors_activations inner join shop_transactions ON (shop_transactions.ref2_id = vendors_activations.shop_transaction_id AND type = " . REVERSAL_RETAILER.") inner join retailers ON (retailers.id = retailer_id) WHERE ref_code = '$transId'");
                
                if(empty($result)){
                    return;
                }
                
		$shop_id = 	$result['0']['vendors_activations']['shop_transaction_id'];
		
		$vendorActObj->query("UPDATE vendors_activations SET vendor_id=$vendorId,prevStatus=status,status=1, complaintNo='' WHERE ref_code='$transId'");
		$vendorActObj->query("UPDATE shop_transactions SET confirm_flag=1 WHERE id=$shop_id");
		 
		$vendorActObj->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$vendorId."','13','Auto Pulled back by System','success','".date("Y-m-d H:i:s")."')");
		$this->addStatus($transId,$vendorId);
		//$this->General->logData("/var/www/html/shops/status.txt",$result['0']['vendors_activations']['ref_code'] . "::pullback: "."INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','13','Pulled back by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");

		//$result1 = $vendorActObj->query("SELECT ref1_id,amount FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
        
		$amt = $result['0']['shop_transactions']['amount'];
		$ret_id = $result['0']['shop_transactions']['ref1_id'];


		$bal = $this->shopBalanceUpdate($amt,'subtract',$ret_id,RETAILER);
		$this->addOpeningClosing($ret_id,RETAILER,$shop_id,$bal+$amt,$bal);
		
		$this->unlockReverseTransaction($shop_id);
		$vendorActObj->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
	
		$vendorActObj->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$result['0']['vendors_activations']['id']."','".$vendorId."','1','".date('Y-m-d H:i:s')."','','".date('Y-m-d H:i:s')."','Auto-pullback','".date('Y-m-d')."')");
		$paramdata = array();
		if(empty($result['0']['vendors_activations']['param']))
		{
			$paramdata['PULLBACKTO'] ="Mobile: ".$result['0']['vendors_activations']['mobile']."\n";
		}
		else {
			$paramdata['PULLBACKTO'] ="Subscriber Id: ".$result['0']['vendors_activations']['param']."\n";
		}
		
		$paramdata['PULLED_AMOUNT'] = $amt;
		$paramdata['TRANSID'] = substr($transId,-5);
		$paramdata['AMOUNT'] = $result['0']['vendors_activations']['amount'];
		$paramdata['BALANCE'] = $bal;
		
		$MsgTemplate = $this->General->LoadApiBalance();
		$content =  $MsgTemplate['Panels_Pullback_MSG'];
		$msg = $this->General->ReplaceMultiWord($paramdata,$content);

		$this->General->sendMessage($result['0']['retailers']['mobile'],$msg,'notify');
	}
	
	function addStatus($transId,$vendor_id){
		$vendors = $this->setMemcache("transCheck$transId@$vendor_id",1,10*60);
		$this->General->logData("/mnt/logs/status.txt",date('Y-m-d H:i:s')."::$transId: 1".json_encode($vendors));
		/*if($vendors === false){
			
		}
		//$vendors[] = $vendor_id;
		$ret = $this->setMemcache("transCheck$transId",$vendors,10*60);	
		$this->General->logData("/var/www/html/shops/status.txt","$transId: ".json_encode($ret));
		*/
	}
	
	function deleteStatus($transId,$vendor_id){
		$vendors = $this->delMemcache("transCheck$transId@$vendor_id");
		$this->General->logData("/mnt/logs/status.txt",date('Y-m-d H:i:s')."delete status::$transId: 1".json_encode($vendors));
	}
	
	function lockTransactionDuplicates($prod,$number,$amount,$api_flag){
		$prod = $this->getOtherProds($prod);
		$prod = str_replace(",", "_", $prod);
		$ret = false;
		$ret1 = $this->addMemcache("dup_$number"."_".$prod."_$amount",$number,TIME_DURATION*60);
		if($ret1 !== false)$ret = true;
		
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_repeattxn VALUES (NULL,'$number','".addslashes($prod)."','$amount','$api_flag','".date('Y-m-d H:i:s',strtotime('+ '.TIME_DURATION.' minutes'))."')")){
			$ret = true;
		}*/
			
		return $ret;
	}
	
	/*function lockTransactionVendor($transId,$vendor_name,$vendor_id){
		//$ret = $this->addMemcache("txn$transId"."_$vendor_id",1,10*60);
		
		$ret1 = false;
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_transaction VALUES (NULL,'$transId','$vendor_name','$vendor_id','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
		else $ret1 = false;
		
		return $ret1;
	}*/
	
	function lockTransaction($transId){
		$ret1 = false;
		$ret = $this->addMemcache("temp$transId",1,10*60);
		if($ret !== false){
			$ret1 = true;
		}
		//else $ret1 = false;
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_txn VALUES (NULL,'$transId','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
		else $ret1 = false;*/
		
		return $ret1;
	}
	
	function lockReverseTransaction($transId){
		//$ret = $this->addMemcache("rev$transId",1,2*24*60*60);
		
		$ret1 = false;
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_reversed VALUES (NULL,'$transId','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
			
		return $ret1;
	}
	
	function lockBankTransaction($bank,$transId){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$ret = false;
		if($vendorActObj->query("INSERT INTO bank_transactions VALUES (NULL,'$bank','".addslashes($transId)."')")){
			$ret = true;
		}
		
		return $ret;
	}
	
	function unlockReverseTransaction($transId){
		//$this->delMemcache("rev$transId");
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_reversed WHERE shoptrans_id = '$transId'");
		return true;
	}
	
	function unlockTransactionDuplicates($prod,$number,$amount){
		$prod = $this->getOtherProds($prod);
		$prod = str_replace(",", "_", $prod);
		$this->delMemcache("dup_$number"."_".$prod."_$amount");
		/*$amount = intval($amount);
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_repeattxn WHERE (number='$number' AND amount='$amount' AND prods='$prod') OR (timestamp < '".date('Y-m-d H:i:s')."')");
		*/
		return true;
	}
	
	function unlockTransaction($transId){
		$ret = $this->delMemcache("temp$transId");
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_txn WHERE ref_code='$transId'");*/
			
		return true;
	}
	
	function reversalDeclined($ref_id,$send){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$data = $vendorActObj->query("SELECT vendors_activations.id,vendors_activations.status,vendors_activations.mobile,vendors_activations.product_id,vendors_activations.retailer_id,resolve_flag FROM vendors_activations left join complaints ON (vendors_activations.id = vendor_activation_id AND resolve_flag = 0) where ref_code='".$ref_id."'");


		if($data['0']['vendors_activations']['status'] == TRANS_REVERSE_PENDING || $data['0']['complaints']['resolve_flag'] == '0') {
			$vendorActObj->query("update vendors_activations set status='".TRANS_REVERSE_DECLINE."',complaintNo='".$_SESSION['Auth']['User']['id']."' where ref_code='".$ref_id."'");
			$ret = $vendorActObj->query("SELECT retailers.mobile,vendors_activations.*,products.service_id,products.name FROM vendors_activations join retailers on ( vendors_activations.retailer_id = retailers.id) join products on (vendors_activations.product_id = products.id) where vendors_activations.ref_code='".$ref_id."'");
			$vendorActObj->query("UPDATE complaints SET resolve_flag=1,closedby='".$_SESSION['Auth']['User']['id']."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

			$msg = 'Complaint for Trans Id '.substr($ret['0']['vendors_activations']['ref_code'],-5).' is resolved';
			$msg .= "\nTransaction is successful";
			$msg .= "\nDate: " . date('j M, g:i A',strtotime($ret['0']['vendors_activations']['timestamp']));
			if($ret['0']['products']['service_id'] == 2){
				$msg .= "\nSub Id: " .  $ret['0']['vendors_activations']['param'];
                                                                                                $msg .= "\nOprId: " .  $ret['0']['vendors_activations']['operator_id'];
			}
			$msg .= "\nMob: " .  $ret['0']['vendors_activations']['mobile'];
			$msg .= "\nOpr: " .  $ret['0']['products']['name'];
                                                                        $msg .= "\nAmt: " .  $ret['0']['vendors_activations']['amount'];

			if($send == 1)
			$this->General->sendMessage($ret['0']['retailers']['mobile'],$msg,'notify');
				
				
			if($data['0']['vendors_activations']['retailer_id'] == B2C_RETAILER){
				$partnerLogs =  $vendorActObj->query("SELECT partners_log.partner_req_id FROM partners_log left join vendors_activations ON (partners_log.vendor_actv_id = vendors_activations.ref_code) WHERE vendors_activations.ref_code = '$ref_id'");

				if(empty($partnerLogs))return;
				$url = B2C_REVERSAL_DECLINE_URL;
				$data = array('transaction_id'=>$partnerLogs['0']['partners_log']['partner_req_id']);
				$this->General->curl_post_async($url,$data);
			}
		}
		return;
	}
	
	function reverseTransaction($tranId,$update=null,$error=null,$group_id=null,$user_id=null){
		$filename = "reverse_transaction_".date('Ymd').".txt";
		
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		//$slaveObj = ClassRegistry::init('Slaves');
		//$vendorActObj->recursive = -1;

		$data = $vendorActObj->query("SELECT vendors_activations.*,products.service_id,products.name,vendors.update_flag,vendors.ip,vendors.port,shop_transactions.* FROM vendors_activations,products,vendors,shop_transactions WHERE vendors.id = vendor_id AND products.reverse_flag = 1 AND products.id = product_id AND ref_code = '$tranId' AND shop_transaction_id = shop_transactions.id");
		if(empty($data)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId:Either data is not present or its a cash pg transactions");
			return;
		}
		$shop_trans_id = $data['0']['vendors_activations']['shop_transaction_id'];
		
		/*$checkforApiTrans = $vendorActObj->query("Select SUM(amount) as amt  from vendors_activations inner join vendors on vendors_activations.vendor_id = vendors.id where vendors.update_flag!=1 and vendors_activations.date = '".$data[0]['vendors_activations']['date']."' AND vendors_activations.complaintNo = '".$_SESSION['Auth']['User']['id']."'");
		
		if($data[0]['vendors']['update_flag']!=1 && $checkforApiTrans[0]['amt'] >=MAX_API_REVERSAL_AMT && $group_id!=ADMIN){
			$this->General->logData("/var/log/pay1_reverse.log","You can not reverse the transction more than".MAX_API_REVERSAL_AMT);
			return;
		}*/
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: Got shoptransid as $shop_trans_id");
		$date = $data['0']['vendors_activations']['date'];
		
		if ($data['0']['vendors_activations']['status'] == TRANS_SUCCESS && $data['0']['vendors']['update_flag'] == 1 && !empty($data['0']['vendors_activations']['operator_id'])) {
			$this->General->logData("/var/log/pay1_reverse.log", "Reversal of Successful txn of modems cannot be done");
			return;
		}
		
		if($date < date('Y-m-d',strtotime('-30 days'))){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 30 days from backend admin");
			return;
		}//
		else if($date < date('Y-m-d',strtotime('-7 days')) && $date >= date('Y-m-d',strtotime('-30 days'))&& $group_id != BACKEND_ADMIN){ // backend operation expert 
			
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days from backend operation expert");
			return;
		}
		else if($date < date('Y-m-d',strtotime('-3 days')) && $date >= date('Y-m-d',strtotime('-7 days'))&& !in_array($group_id,array(BACKEND_OPERATION_EXPERT,BACKEND_ADMIN))){ // backend operation expert 
			
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days from backend operation expert");
			return;
		}
		else { // other group and cc after 30 mins.
			
			$currtime = date('Y-m-d H:i:s',strtotime('-30 minutes'));
			$timestamp = $data['0']['vendors_activations']['timestamp'];
			
			if($group_id == CUSTCARE && $timestamp > $currtime){
				$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is less than 30 minutes");
				return;
			}
		}

		/*else if($date >= date('Y-m-d',strtotime('-7 days')) || $user_id == 1){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is less than 7 days or from admin user");
		}elseif($date >= date('Y-m-d',strtotime('-30 days')) && $group_id == ADMIN){
            $this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days or from admin user");
        }
		else {
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is returned from here, group_id = $group_id, user_id = $user_id");
			return;
		}*/
		
		if($group_id != SUPER_ADMIN && empty($update)){
			if(!$this->lockTransaction($tranId)){
				$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: transaction is locked");
				return;
			}
			$this->unlockTransaction($tranId);
		}
		
		$this->unlockTransactionDuplicates($data['0']['vendors_activations']['product_id'],!empty($data['0']['vendors_activations']['param'])?$data['0']['vendors_activations']['param']:$data['0']['vendors_activations']['mobile'],$data['0']['vendors_activations']['amount']);
		
		if(!$this->lockReverseTransaction($shop_trans_id)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: is already reversed so just changing the status only");
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."' WHERE ref_code= '".$tranId."'");
			return;
		}
		
		if($update == null && ($data['0']['vendors_activations']['status'] == TRANS_REVERSE)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: current status or previous status is reversed so cannot be reversed from here");
			return;
		}

		
		$ret_id = $data['0']['shop_transactions']['ref1_id'];
		$amount = intval($data['0']['shop_transactions']['amount']);

		if($data['0']['products']['service_id'] == 4 && $data['0']['vendors_activations']['api_flag'] != 4){
			$msg_user = "Dear User\nYour request of bill payment of Rs $amount declined from your operator. Please take your money back if already paid to your retailer\nYour pay1 txnid: $tranId";
			$this->General->sendMessage(array($data['0']['vendors_activations']['mobile']),$msg_user,'shops');
		}
		
		$allRecords = $vendorActObj->query("SELECT * FROM shop_transactions WHERE ref2_id = '$shop_trans_id'");
		$ret_amount = 0;
		foreach($allRecords as $record){
			if($record['shop_transactions']['type'] == COMMISSION_RETAILER){
				$ret_amount = $ret_amount + $record['shop_transactions']['amount'];
			}
			else if($record['shop_transactions']['type'] == TDS_RETAILER){
				$ret_amount = $ret_amount - $record['shop_transactions']['amount'];
			}
			else if($record['shop_transactions']['user_id'] == RETAILER && $record['shop_transactions']['type'] == SERVICE_CHARGE){
				$ret_amount = $ret_amount - $record['shop_transactions']['amount'];
			}
		}

		$ret_amount = $amount - $ret_amount;

		if($date != date('Y-m-d')) $type_flag = 1;
		else $type_flag = 0;
		
		if($data['0']['vendors_activations']['status'] == TRANS_SUCCESS && $data['0']['vendors']['update_flag'] == 1){
			$this->General->sendMails('Modem: Txn failed after success',$tranId,array('ashish@mindsarray.com','chirutha@mindsarray.com','backend@mindsarray.com'),'mail');
		}
		
		$this->shopTransactionUpdate(REVERSAL_RETAILER,$ret_amount,$ret_id,$shop_trans_id,$user_id,null,$type_flag);
		
		
		$vendorActObj->query("UPDATE shop_transactions SET confirm_flag = 0,type_flag=$type_flag WHERE id = $shop_trans_id");

		$bal = $this->shopBalanceUpdate($ret_amount,'add',$ret_id,RETAILER);
		$this->addOpeningClosing($ret_id,RETAILER,$shop_trans_id,$bal-$ret_amount,$bal);
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: reversed successfully");
			

		if($date <= date('Y-m-d',strtotime('-1 days')) && $data['0']['vendors']['update_flag'] == 1){
			$this->updateDeviceData($tranId,$data['0']['vendors_activations']['vendor_id'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['amount']);
		}
		
		$bal = round($bal,2);
		//inform retailer by sms
		$shop_data = $this->getShopDataById($ret_id,RETAILER);

		if(empty($user_id))$user_id = 0;
		
		$vendorActObj->query("UPDATE complaints SET resolve_flag=1,closedby='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: updating complaints table::UPDATE complaints SET resolve_flag=1,closedby='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);
		
		$vendorActObj->query("UPDATE api_transactions SET flag=2 WHERE txn_id = '$tranId' AND vendor_id = ".$data['0']['vendors_activations']['vendor_id']);
		$this->deleteStatus($tranId,$data['0']['vendors_activations']['vendor_id']);

    	$err_code = "E013";
		
		$error = empty($error) ? 47 : $error;
		
		if($error==37){
			$err_code = 'E012';
		}
		else if($error==5){
			if($data['0']['products']['service_id'] == 1)
			$err_code = 'E008';
			else if($data['0']['products']['service_id'] == 2)
			$err_code = 'E009';
		}else if($error==6){
			$err_code = 'E010';
		}  else {
			$err_code = 'E013';
		}
		
		
		if(empty($data['0']['vendors_activations']['prevStatus'])){
			$vendorActObj->query("UPDATE vendors_activations SET prevStatus=status,status = '".TRANS_REVERSE."',code='$error',cause='".addslashes($this->errors($error))."', complaintNo='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."'  WHERE ref_code= '".$tranId."'");
		} else {
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."',code='$error',cause='".addslashes($this->errors($error))."' , complaintNo='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."' WHERE ref_code= '".$tranId."'");
		}

		if($data['0']['vendors_activations']['api_flag'] == 4){
			//update partners log entry
			$vendorActObj->query("UPDATE partners_log SET err_code= '$err_code',description = '".$this->apiErrors($err_code)."' WHERE vendor_actv_id= '".$tranId."'");
			 
			//call status_update url of api vendors ( pay1 api client )
			//$partnerRegObj = ClassRegistry::init('Partner');
			$partnerLog = $vendorActObj->query("SELECT partners_log.id,partners_log.partner_req_id,partners.status_update_url,partners.acc_id,partners.password FROM partners,partners_log WHERE partners.id = partners_log.partner_id AND partners_log.vendor_actv_id = '$tranId'");
			$status_update_url = $partnerLog[0]['partners']['status_update_url'];
			if(!empty($status_update_url)){

				$partnerId = $partnerLog['0']['partners']['acc_id'];
				$pay1_trans_id = "2082" . sprintf('%06d', $partnerLog['0']['partners_log']['id']);
				$client_trans_id = $partnerLog['0']['partners_log']['partner_req_id'];
				$open_bal = $bal-$ret_amount;
				$close_bal = $bal;

				$hash = sha1($partnerId.$client_trans_id.$partnerLog['0']['partners']['password']); // order -> acc_id + transId + refCode + password
				
				$sdata = array('open_bal'=>$open_bal,'close_bal'=>$close_bal,'trans_id'=>$pay1_trans_id,'client_req_id'=>$client_trans_id,'status'=>'failure','err_code'=>$err_code,'description'=>$this->apiErrors($err_code),'hash_code'=>$hash,'vendor_code' => 'Signal7');
				//$this->General->curl_post_async($status_update_url, $sdata);
                                $this->General->curl_post($status_update_url, $sdata);
				
				/*$fh = fopen('/var/www/html/shops/abc.txt','a+');
				fwrite($fh,$status_update_url . " " . json_encode($sdata));
				fclose($fh);*/
			}
		}else{
			$tranId = substr($tranId,-5);
			$msg = "";
			if(!empty($error)){
				$msg .= "Reason:".$this->apiErrors($err_code)."\n";
			}
			$msg .= "Reversal of Rs.".$ret_amount." is done.\n";

			$msg .="Trans Id: $tranId\n";
            $msg .="Operator: ". $data['0']['products']['name']. "\n";
			if( in_array( $data['0']['products']['service_id'] , array(1,3,4,5))  ){
				$msg .="Mobile: " . $data['0']['vendors_activations']['mobile'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['mobile'],$data['0']['vendors_activations']['amount'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['retailer_id']);
			}
			else if($data['0']['products']['service_id'] == 2 || $data['0']['products']['service_id'] == 6){
				$msg .="Subscriber Id: " . $data['0']['vendors_activations']['param'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['param'],$data['0']['vendors_activations']['amount'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['retailer_id']);
				
				//$this->General->logData("deleteappreq.txt","mobile =>".$data['0']['vendors_activations']['param']."<br/>"."amount=>".$data['0']['vendors_activations']['amount']."</br>"."operator=>".$data['0']['vendors_activations']['product_id']."<br/>"."retailerid=>".$data['0']['vendors_activations']['retailer_id']."msg=>request deleted successfully!!!!");
			}
			$msg .="Amount: ".$data['0']['vendors_activations']['amount']."\nYour current balance is Rs.".$bal;
			$this->General->sendMessage($shop_data['mobile'],$msg,'notify');
            /*-------------------*/
            //if($data['0']['vendors_activations']['mobile'] == '9819032643'){
				$req_reverse_data = array('mobile'=>$data['0']['vendors_activations']['mobile'],'trans_id'=>$data['0']['vendors_activations']['ref_code'],'amount'=>$data['0']['vendors_activations']['amount']);
            	$this->B2cextender->manage_request_from_b2c_user($req_reverse_data,'failure');
            //}
            /*-------------------*/
		}
		
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : just before memcache for repeat recharge delete");
		
		$this->delMemcache("recharge_".$data['0']['vendors_activations']['retailer_id']."_".$data['0']['vendors_activations']['product_id']."_".$data['0']['vendors_activations']['mobile']."_".$data['0']['vendors_activations']['amount']);
	}
	
	function updateDeviceData($transId,$vendor_id,$product_id,$amount,$reverse_flag=true){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		$data = $vendorActObj->query("SELECT sim_num,date FROM  `vendors_transactions` WHERE  `ref_id` LIKE  '$transId' AND vendor_id = $vendor_id");
		if(!empty($data)){
			$sim_num = $data[0]['vendors_transactions']['sim_num'];
			$date = $data[0]['vendors_transactions']['date'];
			$prod_id = $this->getOtherProds($product_id);
			$str = ($reverse_flag) ? "+" : "-";
			$vendorActObj->query("UPDATE devices_data SET server_diff = server_diff $str $amount WHERE opr_id in ($prod_id) AND mobile='$sim_num' AND vendor_id = $vendor_id AND sync_date='$date'");
		}
	}
	
    function openservice_redis(){
        try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->openredis = new Predis\Client(array(
                   'host' => '107.22.176.158',
                   'password' => REDIS_PASSWORD,
                   'port' => REDIS_PORT 
			));
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->openredis = false;
		}
		return $this->openredis;
    }
    
	function modemRequest_by_redis($query,$vendor=4,$data=null,$timeout=45){
		if(empty($data)) $data = $this->getVendorInfo($vendor);
		$uuid = $vendor."_".time().rand(1,99999);
		$query .= "&vendor_id=$vendor&uuid=$uuid&reqtime=".time();
		parse_str($query,$params);
        $arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
	    
        if(isset($params['oprId'])){
        	$opr_new = isset($arr_map[$params['oprId']]) ? $arr_map[$params['oprId']] : $params['oprId'];
			$log_file = '/mnt/logs/modemRequest_via_redis_'.$vendor.'_'.$opr_new.'_'.date('Y-m-d').'.log';
        }
        else {
        	$log_file = '/mnt/logs/modemRequest_via_redis_'.$vendor.'_'.date('Y-m-d').'.log';
        }
		//$logger = $this->General->dumpLog('modemRequest_by_redis', 'modemRequest_via_redis_'.$vendor.'_'.$opr_new);
        //$logger->info("Received param : data after getting from memcach : ".$query." | ".$vendor." | ".json_encode($data)." | ". $timeout );
        
		file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after getting from memcach  :".json_encode($data), FILE_APPEND | LOCK_EX);
        
		if(empty($data['machine_id'])){
			$data = $this->setVendorInfo($vendor);
            //$logger->info("$vendor machine Id after setting in memcach : ".json_encode($data));
			file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after setting in memcach  :".json_encode($data), FILE_APPEND | LOCK_EX);
			if(empty($data['machine_id'])){
				$vendorActObj = ClassRegistry::init('Slaves');
				$data = $vendorActObj->query("SELECT * FROM vendors WHERE id = $vendor");
				$data = $data[0]['vendors'];
                //$logger->info("$vendor machine Id after setting in memcach again : ".json_encode($data));
				file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after setting in memcach again  :".json_encode($data), FILE_APPEND | LOCK_EX);
			}
		}
		
		$errno = '';
        $errstr = '';        
        
        $vendor_q = $vendor."_".$data['machine_id'];
        $response = "";
        $max_limit = 30;
        $open_redis = $this->openservice_redis();
        
        if($open_redis == FALSE){
            file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." error in redis connection ", FILE_APPEND | LOCK_EX);
            //$logger->error("error in redis connection");
            return array('status'=>'failure','errno'=>515,'error'=>'error in redis connection');
        }
        //$logger->error("error in redis connection");
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." checking queue length ", FILE_APPEND | LOCK_EX);
        
        $vendor_q_lenght = $open_redis->llen($vendor_q);
        //$logger->info("comparing vendor Q len with max : ".$vender_q_lnght." | ".$max_limit);
        
        if($vendor_q_lenght > $max_limit){
            //$this->unHealthyVendor($vendor,$max_limit);
            return array('status'=>'failure','errno'=>515,'error'=>'max thresh limit reached');
        }
        
            
        if($params['query'] == 'recharge'){
	        $key_vendor_opr = "queue_".$vendor."_".$opr_new;
	        $vendor_q_opr = "Trans_". $vendor."_".$opr_new;
            
            //$logger->info("::queue test::".$params['transId']."::$key_vendor_opr::$vendor_q_opr::".var_dump($open_redis->exists($key_vendor_opr))."::queue size:".$open_redis->hlen($vendor_q_opr)."::max length:".$open_redis->get($key_vendor_opr));
	        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")."::queue test::".$params['transId']."::$key_vendor_opr::$vendor_q_opr::".var_dump($open_redis->exists($key_vendor_opr))."::queue size:".$open_redis->hlen($vendor_q_opr)."::max length:".$open_redis->get($key_vendor_opr), FILE_APPEND | LOCK_EX);
        
	        if($open_redis->exists($key_vendor_opr)){
	        	if($open_redis->hlen($vendor_q_opr) >= $open_redis->get($key_vendor_opr)){
	        		return array('status'=>'failure','errno'=>515,'error'=>'Dropped due to lot of pending requests - Server');
	        	}
	        	else {
	        		if(!$open_redis->exists("activestatus_".$vendor)){
	        			file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." dropping request due to network connectivity issues : ".$params['transId']." | data : ". $query, FILE_APPEND | LOCK_EX);
	        			
	        			//$this->General->sendMails("Internet fluctuations at : $vendor",$params['transId'],array('ashish@mindsarray.com'),'mail');
	        			return array('status'=>'failure','errno'=>515,'error'=>'Dropped due to network connectivity issues at modem - Server');
	        		}
	        		$open_redis->hset($vendor_q_opr,$params['transId'],time()+120);
	        	}
	        }
	        else{
	        	return array('status'=>'failure','errno'=>515,'error'=>'No active sim/no balance in sims - Server');
	        }
        }
        /** **/

        //$logger->info(" inserting data in Queue : $vendor_q | data : ". $query);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." inserting data in Queue : $vendor_q | data : ". $query, FILE_APPEND | LOCK_EX);
        
        $open_redis->set($uuid,"1");
        $open_redis->expire($uuid,20);       
        $open_redis->lpush($vendor_q,$query);
        
        //$logger->info(" waiting for response from hash : dynamic_service : uuid ". $uuid);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." waiting for response " , FILE_APPEND | LOCK_EX);
        $response_status = $open_redis->hexists('dynamic_service',$uuid);
        $counter = 1;
		while(!$response_status){
            if($counter > $timeout*5){                
                $this->unHealthyVendor($vendor);
                break;
            }
            usleep(200000);
            $response_status = $open_redis->hexists('dynamic_service',$uuid);                      
            $counter++;
        }
        if($response_status){
            $response = $open_redis->hget('dynamic_service',$uuid);
            
            //$logger->info("removing key from hash | key : ". $uuid);
            file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." | removing key from hash | key : ". $uuid, FILE_APPEND | LOCK_EX);
            $open_redis->hdel('dynamic_service',$uuid);
        }
        //$logger->info(" Response : ". $response);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." | Response : ". $response, FILE_APPEND | LOCK_EX);
        return array('status'=>'success','data'=>$response);
	}
	
    function modemRequest($query,$vendor=4,$data=null,$timeout=45){
    	//if(in_array($vendor,array(4,10,31)))
         return $this->modemRequest_by_redis($query,$vendor,$data,$timeout);
    	
		if(empty($data)) $data = $this->getVendorInfo($vendor);
		
		$ip = $data['ip'];
		$port = $data['port'];
		if($data['bridge_flag'] == 1 && !empty($data['bridge_ip'])){
			$ip = $data['bridge_ip'];
		}
		
		$url = "http://$ip:$port/start.php";
		$errno = '';
        $errstr = '';
		$query .= "&vendor_id=$vendor";
        $Rec_Data = $this->General->curl_post($url,$query,'POST',$timeout);
		
		if(!$Rec_Data['success']){
			$this->unHealthyVendor($vendor);	
			if($Rec_Data['timeout']){
				return array('status'=>'failure','errno'=>$errno,'error'=>$errstr);
			}
		}
        
		return array('status'=>'success','data'=>$Rec_Data['output']);
	}
    
	function healthyVendor($vendor,$ip=null){
		$data1 = $this->getVendorInfo($vendor);
		$name = $data1['company'];
		
		/* Don't declare a vendor healthy if the electricity is down*/
		$elec_check = $this->getMemcache("electricity_".$vendor);
		if($elec_check !== false && $elec_check == 0){
			return;
		}
		
		
		$data = array();
		
		$set = false;
		$ip_flag = false;
		if(!empty($ip) && $ip != $data1['ip']){
			$data['ip'] = $ip;
			$data['update_time'] = date('Y-m-d H:i:s');
			$set = true;
			$ip_flag = true;
		}
		
		if($data1['active_flag'] == 0){
			$data['active_flag'] = 1;
			$data['health_factor'] = 0;
			$set = true;
            $this->General->logData("/mnt/logs/log.txt",date('Y-m-d H:i:s').": Enabling $name Vendor : $vendor");
			$this->General->sendMails("(SOS)Enabling $name Vendor : $vendor","Got the connectivity. So enabling the vendor",array('backend@mindsarray.com','chetan@mindsarray.com'),'mail');			
		}
		
		if($data1['health_factor'] > 5){
			$data['health_factor'] = 0;
			$set = true;
		}
		
		if($set){
			$this->setVendorInfo($vendor,$data);	
			if($ip_flag)$this->setVendors();
			$this->setInactiveVendors();
		}
	}
	
	
	function unHealthyVendor($vendor,$health_factor=null){
		if(empty($vendor))return;
		
		$data = $this->getVendorInfo($vendor);
		$name = $data['company'];
		
		$set = false;
		$data1 = array();
		if($data['active_flag'] ==1){
			if(is_null($health_factor))$data1['health_factor'] = $data['health_factor'] + 1;
			else $data1['health_factor'] = $health_factor;
			
			if($data1['health_factor'] >= 20){
				$data1['active_flag'] = 0;
				$this->General->sendMails("(SOS)Disabling $name Vendor : $vendor","Connectivity issues. It will be activated once we get the connectivity",array('backend@mindsarray.com','chetan@mindsarray.com'),'mail');
				
				$this->General->logData("/mnt/logs/log.txt",date('Y-m-d H:i:s').": Disabling $name Vendor : $vendor");
				$set = true;
			}
			$this->setVendorInfo($vendor,$data1);	
			if($set)$this->setInactiveVendors();	
		}
	}
	
	
	function earnings($params){
		$pageNo = empty($params['page_no'])?0:$params['page_no'];
		$itemsPerPage = empty($params['items_per_page'])?0:$params['items_per_page'];

		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if(date('Y-m-d',  strtotime($date_to)) < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));
			}
			else $next_week = "";
		}
		 
		if($itemsPerPage <= 0 || $pageNo <= 0){
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
			//$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date";
			$query = "SELECT trim(sale) as amount, trim(earning) as income,trim(date) as date FROM retailers_logs WHERE retailer_id = ".$_SESSION['Auth']['id'] . " AND date >= '$date_from' AND date <= '$date_to'";
		}else{
			$ll = $itemsPerPage * ( $pageNo - 1 ) ;//+ 1; // lower limit
			$ul = $itemsPerPage * $pageNo ;// upper limit
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date limit $ll , $ul ";
			//$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date limit $ll , $ul";
			$query = "SELECT trim(sale) as amount, trim(earning) as income,trim(date) as date FROM retailers_logs WHERE retailer_id = ".$_SESSION['Auth']['id'] . " AND date >= '$date_from' AND date <= '$date_to' limit $ll , $ul";
		}
		$vendorActObj = ClassRegistry::init('Slaves');
		//$vendorActObj->recursive = -1;

		$data = $vendorActObj->query($query);
		$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date = '".date('Y-m-d')."'";
		$today = $vendorActObj->query($query);
		//$today = $vendorActObj->query("SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.") WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date = '".date('Y-m-d')."'");
		//print_r($data);
		return array($data,$today['0'],$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function topups($params){
		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);	
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if($date_to < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));	
			}
			else $next_week = "";	
		}
		
		$query = "SELECT sum(st1.amount) as amount, date(st1.timestamp) as day FROM shop_transactions as st1 WHERE st1.confirm_flag != 1 AND st1.type = ".DIST_RETL_BALANCE_TRANSFER." AND st1.ref2_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
		$vendorActObj = ClassRegistry::init('Slaves');
		$vendorActObj->recursive = -1;
		
		$data = $vendorActObj->query($query);
		return array($data,$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function verifyParams($params,$mapping){
		$ret = true;
		$msg = "";
		foreach($mapping['allParams']['param'] as $param){
			$field = trim($param['field']);
			if(!isset($params[$field])){
				$ret = false;
				$msg = $field . " not entered";
				break;
			}
			else if(strlen($params[$field]) >  $param['length'] || empty($params[$field])){
				$msg = $field . ": Enter valid value";
				$ret = false;
				break;
			}
			else if($field == 'Mobile' || $field == 'PNR' || $field == 'Amount'){
				if(strlen($params[$field]) != $param['length']){
					$msg = $field . ": Enter valid value";
					$ret = false;
					break;
				}
			}
		}
		if($ret){
			return array('status'=>'success');
		}
		else {
			return array('status'=>'failure','description' => $msg);	
		}
	}
	
	function mailToSuperDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$superdist_shop = $this->getShopDataById($dist_shop['parent_id'],SUPER_DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($superdist_shop['email']));
	}
	
	function mailToDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($dist_shop['email']));
	}
	
	/*function calculateCommission($id,$group_id,$product,$amount,$slab){
		//$shop = $this->getShopDataById($id,$group_id);
		//$slab = $shop['slab_id'];
		$commission = $this->getMemcache("commission_".$product."_".$slab);
		if($commission === false){
			$slabObj = ClassRegistry::init('SlabsUser');
			$prodData =  $slabObj->query("SELECT percent FROM slabs_products INNER JOIN products ON (product_id = products.id) WHERE slab_id = $slab AND product_id = $product");
			if(!empty($prodData))
				$commission = $amount*$prodData['0']['slabs_products']['percent']/100;
			else $commission = 0;
			$this->setMemcache("commission_".$product."_".$slab,$commission,24*60*60);
		}
		
		return $commission;
	}*/
	
	function getCommissionPercent($slab_id,$product){
		$commission = $this->getMemcache("commission_".$product."_".$slab_id);
		if($commission === false){
			$slabObj = ClassRegistry::init('Slaves');
			$prodData =  $slabObj->query("SELECT percent,service_charge,service_tax FROM slabs_products WHERE slab_id = $slab_id AND product_id = $product");
	        if(isset($prodData) && !empty($prodData)){
				$commission = $prodData['0']['slabs_products'];
	        }
	        else $commission = array();
	        $this->setMemcache("commission_".$product."_".$slab_id,$commission,24*60*60);
		}
		
		return $commission;
	}
	
	function getAllCommissions($id,$group_id,$slab,$service=null){
		$qryStr = '';
		if(!is_null($service)) $qryStr = ' and products.service_id = '.$service.' ';
		$slabObj = ClassRegistry::init('SlabsUser');
		$table = array();
		/*if($group_id == SUPER_DISTRIBUTOR){
			$slab = SDIST_SLAB;			
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['SD'] = $prodData;
		}
		
		if($group_id <= DISTRIBUTOR){
			$slab = DIST_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['D'] = $prodData;
		}*/
		
		if($group_id <= RETAILER){
			//$slab = RET_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['R'] = $prodData;
		}
		
		return $table;
	}
	
	function calculateTDS($commission){
		return ($commission*TDS_PERCENT/100);
	}
	
	function shopBalanceUpdate($price,$type,$id,$group_id,$dataSource=null){
            
            
		$userObj = is_null($dataSource)?ClassRegistry::init('User'):$dataSource;
		
		if($group_id == SUPER_DISTRIBUTOR){
			$table = 'super_distributors';
		}
		else if($group_id == DISTRIBUTOR){
			$table = 'distributors';
		}
		else if($group_id == RETAILER){
			$table = 'retailers';
		}
		
		if($type == 'subtract'){
			$userObj->query("UPDATE $table SET balance = balance - $price WHERE id = $id");	
		}
		else if($type == 'add'){
			$userObj->query("UPDATE $table SET balance = balance + $price WHERE id = $id");
		}
		
		     $qry = "SELECT ".$table.".balance,users.mobile FROM $table left join users ON (users.id = user_id) WHERE $table".".id = $id";
		$bal = 	$userObj->query($qry);
                
		$final_bal = sprintf('%.2f', $bal['0'][$table]['balance']);
                                     $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Retailer Final Bal :  {$final_bal}",FILE_APPEND | LOCK_EX); 
		if($final_bal < 0){
			$this->General->logData('/mnt/logs/negative_bal.txt',"$table ID : $id | Final balance : $final_bal | Mobile number: ".$bal['0']['users']['mobile']);
			
			
                                                        if(is_null($dataSource)): // Send message only if this function is called from somewhere else other than during transaction.
                                                            $this->General->sendMessage('9819032643,9820595052,9967054833,9004895333',"Negative balance Alert, $table ID : $id | Final balance : $final_bal | Mobile number: ".$bal['0']['users']['mobile'],'shops');
                                                            $this->General->sendMails("Alert : Negative Balance","$table ID : $id <br/> Final balance : $final_bal <br/> Mobile number: ".$bal['0']['users']['mobile'],array('nandan@mindsarray.com','ashish@mindsarray.com','vibhas@mindsarray.com'),'mail');
                                                        endif;
		}
		return sprintf('%.2f', $bal['0'][$table]['balance']);
	}
	
	function getNewInvoice($shop_id,$from_id,$group_id,$invoice_type){
		$invoiceObj = ClassRegistry::init('Invoice');
		$this->data['Invoice']['ref_id'] = $shop_id;
		$this->data['Invoice']['from_id'] = $from_id;
		$this->data['Invoice']['group_id'] = $group_id;
		$this->data['Invoice']['invoice_type'] =  $invoice_type;
		$this->data['Invoice']['timestamp'] =  date('Y-m-d H:i:s');

		$invoiceObj->create();
		$invoiceObj->save($this->data);

		return $invoiceObj->id;
	}
	
	function addToInvoice($invoice_id,$trans_id){
		$invoiceObj = ClassRegistry::init('Invoice');
		$invoiceObj->query("INSERT INTO invoices_transactions (invoice_id,shoptransaction_id,timestamp) VALUES ($invoice_id,$trans_id,'".date('Y-m-d H:i:s')."')");
	}
	
	/*function getInvoiceAmount($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		$invoiceObj->recursive = -1;
		$invoice = $invoiceObj->findById($invoice_id);
		$id = $invoice['Invoice']['ref_id'];
		$group_id = $invoice['Invoice']['group_id'];
		$invoice_type = $invoice['Invoice']['invoice_type'];
		$data['date'] = date('Y-m-d', strtotime($invoice['Invoice']['timestamp']));
		
		$shop = $this->getShopDataById($id,$group_id);
		if($group_id != SUPER_DISTRIBUTOR){
			$parent = $shop['parent_id'];
			if($group_id == DISTRIBUTOR){
				$parentShop = $this->getShopDataById($parent,SUPER_DISTRIBUTOR);
			}
			else if($group_id == RETAILER){
				$parentShop = $this->getShopDataById($parent,DISTRIBUTOR);
			}
			$tds = $parentShop['tds_flag'];
		}
		else {
			$tds = 1;
		}
		
		$Totcommission = 0;
		$Totamount = 0;
		
		$transactions = $invoiceObj->query("SELECT ref2_id,shop_transactions.id FROM invoices_transactions,shop_transactions WHERE invoice_id = $invoice_id AND shop_transactions.id = invoices_transactions.shoptransaction_id");	
		if($invoice_type == DISTRIBUTOR_ACTIVATION){
			$coupons = array();
			foreach($transactions as $transaction){
				$coupons = array_merge($coupons,explode(",",$transaction['shop_transactions']['ref2_id']));
			}
			$couponObj = ClassRegistry::init('Coupon');
			$couponObj->recursive = -1;
			$prodData = $couponObj->find('all',array('fields' => array('GROUP_CONCAT(Coupon.serialNumber) as serials','COUNT(Coupon.id) as quantity','Product.id','Product.name','Product.price'), 'conditions' => array('Coupon.id in (' .implode(",",$coupons). ')' ),
					'joins' => array(
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Coupon.product_id = Product.id')
						)
					),
					'group' => array('Coupon.product_id')
			));
			foreach($prodData as $prod){
				$Totamount += $prod['0']['quantity']*$prod['Product']['price'];
				$product = array();
				$product[$prod['Product']['id']] = $prod['0']['quantity'];
				$Totcommission += $this->calculateCommission($id,$group_id,$product,$date);
			}
		}
		else {
			$products = array();
			$prodids = array();
			foreach($transactions as $transaction){
				$prod_id = $transaction['shop_transactions']['ref2_id'];
				$transObj = ClassRegistry::init('ShopTransaction');
				$transObj->recursive = -1;
			
				if($group_id == RETAILER){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_RETAILER)));
				}
				else if($group_id == DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_DISTRIBUTOR)));
				}
				else if($group_id == SUPER_DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_SUPERDISTRIBUTOR)));
				}
				$commission = $commission['ShopTransaction']['amount'];
				
				if(!isset($products[$prod_id])){
					$products[$prod_id]['quantity'] = 1;
					$products[$prod_id]['commission'] = $commission;
					$prodids[] = $prod_id;
				}
				else {
					$products[$prod_id]['quantity'] = $products[$prod_id]['quantity'] + 1;
					$products[$prod_id]['commission'] = $products[$prod_id]['commission'] + $commission;
				}
			}
			
			$prodObj = ClassRegistry::init('Product');
			$prodObj->recursive = -1;
			$prodData = $prodObj->find('all',array('fields' => array('Product.id','Product.name','Product.price'), 'conditions' => array('Product.id in (' .implode(",",$prodids). ')' )));	
			foreach($prodData as $prod){
				$Totamount += $products[$prod['Product']['id']]['quantity'] * $prod['Product']['price'];
				$Totcommission += $products[$prod['Product']['id']]['commission'];
			}
		}
		if($tds == 1){
			 $tds = $this->calculateTDS($Totcommission);
			 $Totcommission -= $tds;
		}
		
		$net = sprintf('%.2f',$Totamount - $Totcommission);
		return round($net);
	}*/
	
	function getOutstandingBalance($id,$type){
		$rcptObj = ClassRegistry::init('Receipt');
		$data = $rcptObj->find('all',array('fields' => array('min(Receipt.os_amount) as os'), 'conditions' => array('Receipt.receipt_ref_id' => $id,'Receipt.receipt_type' => $type)));
			
		if(empty($data['0']['0']['os'])){
			if($type == RECEIPT_INVOICE){
				$invObj = ClassRegistry::init('Invoice');
				$amount = $invObj->findById($id);
				$amount = $amount['Invoice']['amount'];
			}
			else if($type == RECEIPT_TOPUP){
				$transObj = ClassRegistry::init('ShopTransaction');
				$amount = $transObj->findById($id);
				$amount = $amount['ShopTransaction']['amount'];
			}
		}
		else {
			$amount = $data['0']['0']['os'];
		}
		return $amount;
	}
	
	function generateInvoice($id,$group_id,$type,$date=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		$last_trans = $transObj->query("SELECT max(shoptransaction_id) as trans_id FROM invoices_transactions WHERE invoice_id IN (SELECT max(id) FROM invoices WHERE ref_id=$id AND group_id = $group_id AND invoice_type = $type)");
		$trans_id = $last_trans['0']['0']['trans_id'];
		
		if($group_id == RETAILER && $type == RETAILER_ACTIVATION)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == DISTRIBUTOR && $type == RETAILER_ACTIVATION)
		{	
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_DISTRIBUTOR;
		}
		else if($group_id == DISTRIBUTOR && $type == DISTRIBUTOR_ACTIVATION){
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == SUPER_DISTRIBUTOR)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_SUPERDISTRIBUTOR;
		}
		
		if(!empty($trans_id)){
			$query .= " AND st1.id > $trans_id";
		}
		if($date != null){
			$query .= " AND Date(st1.timestamp) = '$date'";
		}
		$data = $transObj->query($query);
		if(!empty($data)){
			if($group_id == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($group_id == DISTRIBUTOR || $group_id == RETAILER){
				$sData = $this->getShopDataById($id,$group_id);
				$parent = $sData['parent_id'];
			}
			$new_invoice = $this->getNewInvoice($id,$parent,$group_id,$type);
			foreach($data as $trans){
				$this->addToInvoice($new_invoice,$trans['st1']['id']);
			}
			$amount = $this->getInvoiceAmount($new_invoice,date('Y-m-d'));
			$number = $this->getInvoiceNumber($new_invoice,date('Y-m-d'));
			$transObj->query("UPDATE invoices SET amount = $amount,invoice_number='$number' WHERE id = $new_invoice");
		}
	}
	
	
	
	
	function getShopDataById1($id,$group_id){
		$info = $this->getMemcache("shop".$id."_".$group_id);
		if($info === false){
			if($group_id == SUPER_DISTRIBUTOR){
				$userObj = ClassRegistry::init('SuperDistributor');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['SuperDistributor'];
			}
			else if($group_id == DISTRIBUTOR){
				$userObj = ClassRegistry::init('Distributor');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['Distributor'];
			}
			else if($group_id == RETAILER){
				$userObj = ClassRegistry::init('Retailer');
				$userObj->recursive = -1;
				$bal = $userObj->query("select * from retailers 
						left join unverified_retailers ur on ur.retailer_id = retailers.id
						where retailers.id = $id");
				
				foreach($bal[0]['ur'] as $key => $row){
					if(!in_array($key, array('id')))
						$bal[0]['retailers'][$key] = $bal[0]['ur'][$key];
				}
				
				$info = $bal['0']['retailers'];
			}else if($group_id == RELATIONSHIP_MANAGER){
				$userObj = ClassRegistry::init('Rm');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['Rm'];
			}
			
			$this->setMemcache("shop".$id."_".$group_id,$info,60*60*3);
		}
		return $info;
	}
	
	function getBalance($id,$group_id){
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from super_distributors where id=$id");
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			if(!empty($bal))return $bal['0']['super_distributors']['balance'];
			else return 0;
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from distributors where id=$id");
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			if(!empty($bal))return $bal['0']['distributors']['balance'];
			else return 0;
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from retailers where id=$id");
			if(!empty($bal))return $bal['0']['retailers']['balance'];
			else return 0;
		}
		else if($group_id == SALESMAN){
			$userObj = ClassRegistry::init('Salesman');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from salesmen where id=$id");
			if(!empty($bal))return $bal['0']['salesmen']['balance'];
			else return 0;
		}
	}
	
	function getShopDataById($id,$group_id){
		
		if($group_id == SUPER_DISTRIBUTOR){
                                        	$userObj = ClassRegistry::init('SuperDistributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['SuperDistributor'];
                                     }
		else if($group_id == DISTRIBUTOR){
                   		$userObj = ClassRegistry::init('Distributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Distributor'];
                   	}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			
			$bal = $userObj->query("select * from retailers
					left join unverified_retailers ur on ur.retailer_id = retailers.id
					where retailers.id = $id");
			
			foreach($bal[0]['ur'] as $key => $row){
				if(!in_array($key, array('id')))
					$bal[0]['retailers'][$key] = $bal[0]['ur'][$key];
			}
			
			return $bal['0']['retailers'];
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			//return $bal['Retailer'];
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Rm'];
                   	}
	}
	
	/*function disabledApps($uid){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			$dis = $userObj->query('select service_id from services_disabled where user_id = '.$uid);
			$str = '';
			foreach($dis as $d)
			$str .= $d['services_disabled']['service_id'].",";
			
			if($str == '')return $str;
			else return substr($str,0,-1);
	}*/
	
	function shortSerials($serials){
		$serials = explode(",",$serials);
		sort($serials);
		$serial_array = array();
		
		$min = $serials[0];
		$last = $serials[0];
		$serial_array[$min] = 1;
		foreach($serials as $serial){
			if($serial == $last + 1){
				$serial_array[$min] = $serial_array[$min] + 1;
				$last = $serial;
			}
			else if($serial != $min){
				$min = $serial;
				$last = $serial;
				$serial_array[$min] = 1;
			}
		}
		$strings = array();
		foreach($serial_array as $key=>$value){
			$string = $key;
			if($value > 1){
				$string .= "-" . ($key + $value - 1);
			}
			$strings[] = $string;
		}
		return implode(", ", $strings);
	}
	
	/*function getInvoiceNumber($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		$data = $invoiceObj->findById($invoice_id);
		if($data['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == RETAILER){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		$number1 = $ret_to['0']['0']['counts'] + 1;
		return "INV-" . $suffix1 . "/" . $suffix2 . "/" . date('Y',strtotime($date)). "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}
	
	function getTopUpReceiptNumber($transaction_id){
		$transObj = ClassRegistry::init('ShopTransaction');
		$data = $transObj->findById($transaction_id);
		if($data['ShopTransaction']['type'] == ADMIN_TRANSFER){
			$suffix1 = "ST";
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],SUPER_DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);	
		}
		else if($data['ShopTransaction']['type'] == SDIST_DIST_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['ShopTransaction']['type'] == DIST_RETL_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],RETAILER);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		$ret = $transObj->query("SELECT count(*) as counts FROM shop_transactions WHERE type = ".$data['ShopTransaction']['type']." and ref1_id = ".$data['ShopTransaction']['ref1_id']." and id < $transaction_id");
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "TP-". $suffix1 . "/". $suffix2 . "/" . sprintf('%06d', $number);
	}
	
	function getCreditDebitNumber($to_id,$from_id,$to_groupid,$type,$id=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		$query = "";
		if($id != null){
			$query = " AND id < $id";
		}
		if(empty($from_id)) $from = " is null";
		else $from = " = $from_id";
		
		$ret = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE to_groupid = $to_groupid AND from_id $from AND to_id = $to_id AND type=$type $query");
		$retAll = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE from_id $from AND type=$type $query");
		
		$shop = $this->getShopDataById($to_id,$to_groupid);
		if(!empty($from_id)){
			$parent_shop = $this->getShopDataById($from_id,$to_groupid-1);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		if(isset($shop['company']))
			$comp = explode(" ",$shop['company']);
		else 
			$comp = explode(" ",$shop['shopname']);

		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $retAll['0']['0']['counts'] + 1;
		$number1 = $ret['0']['0']['counts'] + 1;
		if($type == 0) $suffix0 = "CN";
		else if($type == 1) $suffix0 = "DN";
		return "$suffix0-" . $suffix1 . "/" . $suffix2 . "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}*/
	
	function getOtherProds($prodId){
		if($prodId == 3 || $prodId == 34) $prodId1 = "3,34";
		else if($prodId == 7 || $prodId == 8) $prodId1 = "7,8";
		else if($prodId == 9 || $prodId == 27 || $prodId == 10) $prodId1 = "9,10,27";
		else if($prodId == 11 || $prodId == 29) $prodId1 = "11,29";
		else if($prodId == 12 || $prodId == 28) $prodId1 = "12,28";
		else if($prodId == 30 || $prodId == 31) $prodId1 = "30,31";
		else $prodId1 = $prodId;
		
		return $prodId1;
	}
	
	function getParentProd($prodId){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
	    if(isset($arr_map[$prodId]))return $arr_map[$prodId];
	    else return $prodId;
	}
	
	function getProdInfo($prodId){
		$info = $this->getMemcache("prod$prodId");
		if($info === false){
			$info = $this->setProdInfo($prodId);
		}
		
		return $info;
	}
	
	function getVendorInfo($vendorId){
		$info = $this->getMemcache("vendor$vendorId");
		if($info === false){
			$info = $this->setVendorInfo($vendorId);
		}
		
		return $info;
	}
	
	function setVendorInfo($vendorId,$info=null){
		$invoiceObj = ClassRegistry::init('Invoice');
			
		$info1 = $invoiceObj->query("SELECT * FROM vendors WHERE id = $vendorId");
		$info1 = $info1['0']['vendors'];
        if(empty($info1)){
            return FALSE;
        }
		if(!empty($info)){
			$arr = array();
			foreach($info as $k => $v){
				if($k == 'machine_id') continue;
				$arr[] = "$k='$v'";
				$info1[$k] = $v;
			}
			$invoiceObj->query("UPDATE vendors SET ".implode(",",$arr)." WHERE id = $vendorId");
		}
		$this->setMemcache("vendor$vendorId",$info1,30*60);
		return $info1;
	}
	
	function getVMNList($src=null){
               //$ret  = array();
               if( $src == 'fromLogin'){//to send vmn list with loginResponse                   
                   $ret = $this->General->findVar('VMNList');
                   $ret = json_decode($ret,true);
                   $res = array();
                    if(count($ret)>0){
                   foreach ($ret as $key=>$value) {
                       array_push($res,$value['no']);
                   }
                   $ret = $res;
                    }
               }else{
                   $ret = $this->General->findVar('VMNList');
                   $ret = json_decode($ret,true);
               }
              return $ret;
	}
	
	/*
	function setProdInfo($prodId){
		$invoiceObj = ClassRegistry::init('Invoice');
		$id = $invoiceObj->query("SELECT services.id,services.name,vendors_commissions.vendor_id, vendors_commissions.discount_commission, products.circle_yes, products.circle_no, products.automate_flag, vendors.shortForm, vendors.update_flag, products.price, products.oprDown, products.down_note , products.name,products.min,products.max,products.invalid,products.blocked_slabs,vendors_commissions.circles_yes,vendors_commissions.circle, vendors_commissions.circles_no FROM products left join vendors_commissions ON (vendors_commissions.product_id= products.id AND vendors_commissions.oprDown = 0) left join vendors on (vendors_commissions.vendor_id=vendors.id) join services ON (products.service_id = services.id) WHERE products.id=$prodId order by vendors_commissions.active desc,discount_commission desc");
		$info = array();
		$info['vendors'] = array();
		$vendor_ids = array();
		$vendors_n = array();
		
		foreach($id as $res){
			$info['service_id'] = $res['services']['id'];
			$info['service_name'] = $res['services']['name'];
			$info['min'] = $res['products']['min'];
			$info['max'] = $res['products']['max'];
			$info['automate'] = $res['products']['automate_flag'];
			$info['price'] = $res['products']['price'];
			$info['invalid'] = $res['products']['invalid'];
			$info['name'] = $res['products']['name'];
			$info['oprDown'] = $res['products']['oprDown'];
			$info['down_note'] = $res['products']['down_note'];
			$info['circles_yes'] = strtoupper(trim($res['products']['circle_yes']));
			$info['circles_no'] = strtoupper(trim($res['products']['circle_no']));
			$info['blocked_slabs'] = explode(",",trim($res['products']['blocked_slabs']));
			if(!empty($res['vendors_commissions']['vendor_id'])){
				$info['vendors'][] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);	
				$vendor_ids[] = $res['vendors_commissions']['vendor_id'];
				$vendors_n[$res['vendors_commissions']['vendor_id']] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);
			}
		}
		
		
		if($info['automate'] == 1){
			$opr_ids = $this->getOtherProds($prodId);
			//$vendors_data = $invoiceObj->query("SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having sims > 0");
			
			$vendors_data = $invoiceObj->query("select vendor_id,sum(planned_sale) as planned_sale,sum(base_amount) as base_amount,sum(totalsale) as totalsale,sum(sims) as sims from (SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)) as t group by t.vendor_id");
			$priority_1 = array();
			$priority_2 = array();
			$cut_off = array();
			foreach ($vendors_data as $vendor){
				if(!in_array($vendor['t']['vendor_id'],$vendor_ids))continue;
				
				$r2_sale = $vendor['0']['base_amount'] - $vendor['0']['totalsale'];
				$r1_sale = $vendor['0']['planned_sale'] - $vendor['0']['totalsale'];
				if($vendor['0']['sims'] > 0 && $r1_sale > 0){
					$weight = $r1_sale/$vendor['0']['sims'];
					$priority_1[$weight] = $vendor['t']['vendor_id'];
				}
				else if($vendor['0']['sims'] > 0 && $r2_sale > 0){
					$weight = $r2_sale/$vendor['0']['sims'];
					$priority_2[$weight] = $vendor['t']['vendor_id'];
				}
				
				if($r1_sale < 0 && $r2_sale < 0){
					$cut_off[] = $vendor['t']['vendor_id'];
				}
			}
			
			//get other vendors as well
			$vendors_data = $invoiceObj->query("SELECT vendors.id,base_amount,planned_sale FROM `inv_planning_sheet` as ips inner join inv_supplier_operator as iso ON (ips.supplier_operator_id = iso.id) inner join inv_supplier_vendor_mapping as isvm ON (isvm.supplier_id = iso.supplier_id) inner join vendors ON (vendors.id = isvm.vendor_id) WHERE vendors.update_flag = 0 AND operator_id in ($opr_ids)");
			
			foreach($vendors_data as $vd){
				if(!in_array($vd['vendors']['id'],$vendor_ids))continue;

				$prod = $this->getParentProd($prodId);
                
				$cap_per_min = $this->getMemcache("cap_api_".$prod."_".$vd['vendors']['id']);
                
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prod:: ".$vd['vendors']['id']." ::cap_per_min $cap_per_min :: "."cap_api_".$prod."_".$vd['vendors']['id']);
            
				if($cap_per_min !== false && $cap_per_min > 0){
					//$sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
                    $sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
					$r2_sale = $vd['ips']['base_amount'] - $sale;
					$r1_sale = $vd['ips']['planned_sale'] - $sale;
	
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: ".$vd['vendors']['id']." ::r1_sale $r1_sale :: r2_sale $r2_sale :: $cap_per_min");
            
                    
					if($r1_sale > 0){
						$weight = $r1_sale/$cap_per_min;	
						$priority_1[$weight] = $vd['vendors']['id'];
					}
					else if($r2_sale > 0){
						$weight = $r2_sale/$cap_per_min;	
						$priority_2[$weight] = $vd['vendors']['id'];
					}
					
					if($r1_sale < 0 && $r2_sale < 0){
						$cut_off[] =$vd['vendors']['id'];
					}
				}
				
			}
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			krsort($priority_1);
			krsort($priority_2);
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: after sort :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: cutoff vendors :: $prodId:: ".  json_encode($cut_off));
            
			$info['vendors'] = array();
			foreach($priority_1 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			foreach($priority_2 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			
			foreach($vendors_n as $k => $v){
				if(in_array($k,$final))continue;
				if(in_array($k,$cut_off))continue;
				$info['vendors'][] = $v;
			}
            
            $this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: final :: $prodId :: ". json_encode($info['vendors']));
		}
        
		$this->setMemcache("prod$prodId",$info,5*60);
		return $info;
	}*/
        
	function setProdInfo($prodId){
		$invoiceObj = ClassRegistry::init('Slaves');
		$id = $invoiceObj->query("SELECT services.id,services.name,vendors_commissions.vendor_id, vendors_commissions.discount_commission, products.circle_yes, products.circle_no, products.automate_flag, vendors.shortForm, vendors.update_flag, products.price, products.oprDown, products.down_note , products.name,products.min,products.max,products.invalid,products.blocked_slabs,vendors_commissions.circles_yes,vendors_commissions.circle, vendors_commissions.circles_no FROM products left join vendors_commissions ON (vendors_commissions.product_id= products.id AND vendors_commissions.oprDown = 0) left join vendors on (vendors_commissions.vendor_id=vendors.id) join services ON (products.service_id = services.id) WHERE products.id=$prodId order by vendors_commissions.active desc,discount_commission desc");
		$info = array();
		$info['vendors'] = array();
		$vendor_ids = array();
		$vendors_n = array();
		
		foreach($id as $res){
			$info['service_id'] = $res['services']['id'];
			$info['service_name'] = $res['services']['name'];
			$info['min'] = $res['products']['min'];
			$info['max'] = $res['products']['max'];
			$info['automate'] = $res['products']['automate_flag'];
			$info['price'] = $res['products']['price'];
			$info['invalid'] = $res['products']['invalid'];
			$info['name'] = $res['products']['name'];
			$info['oprDown'] = $res['products']['oprDown'];
			$info['down_note'] = $res['products']['down_note'];
			$info['circles_yes'] = strtoupper(trim($res['products']['circle_yes']));
			$info['circles_no'] = strtoupper(trim($res['products']['circle_no']));
			$info['blocked_slabs'] = explode(",",trim($res['products']['blocked_slabs']));
			if(!empty($res['vendors_commissions']['vendor_id'])){
				$info['vendors'][] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);	
				$vendor_ids[] = $res['vendors_commissions']['vendor_id'];
				$vendors_n[$res['vendors_commissions']['vendor_id']] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);
			}
		}
		
		
		if($info['automate'] == 1){
			$opr_ids = $this->getOtherProds($prodId);
			$non_primary = array();
			$cap_non_primary = ($prodId == 15 ) ? 75 : 100; // 75% for vodafone and others 100%
			//$vendors_data = $invoiceObj->query("select vendor_id,sum(planned_sale) as planned_sale,sum(base_amount) as base_amount,sum(totalsale) as totalsale,sum(sims) as sims from (SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)) as t group by t.vendor_id");
			
			$vendors_data = $invoiceObj->query("SELECT devices_data.vendor_id,devices_data.supplier_operator_id,devices_data.inv_supplier_id,inv_planning_sheet.base_amount,inv_planning_sheet.max_sale_capacity,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance >= 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims, inv_modem_planning_sheet.target
FROM inv_planning_sheet 
INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id)
INNER JOIN inv_modem_planning_sheet ON (inv_modem_planning_sheet.supplier_operator_id = devices_data.supplier_operator_id AND devices_data.vendor_id =inv_modem_planning_sheet.vendor_id)
WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)");
			
			
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." ::$prodId:: modem_vendor_data:: ".  json_encode($vendors_data));
            
			$target_sale_supplier = array();
			$target_sale_vendor = array();
			foreach ($vendors_data as $vendor){
				if(!isset($target_sale_supplier[$vendor['devices_data']['supplier_operator_id']])) {
					$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['target'] = 0;
					$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['base'] = 0;
				}
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['target'] += $vendor['inv_modem_planning_sheet']['target'];
				
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['base'] = $vendor['inv_planning_sheet']['max_sale_capacity'];
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['supplier_id'] = $vendor['devices_data']['inv_supplier_id'];
				
				
				if(!isset($target_sale_vendor[$vendor['devices_data']['vendor_id']])){
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['target'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sims'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sale'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['base'] = 0;
				}
				
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['target'] += $vendor['inv_modem_planning_sheet']['target'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sims'] += $vendor['0']['sims'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sale'] += $vendor['0']['totalsale'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['base'] += $vendor['inv_planning_sheet']['max_sale_capacity'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['data'][] = $vendor;
			}
			
			$priority_1 = array();
			$priority_2 = array();
			$cut_off = array();
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." ::$prodId:: target_sale_vendor:: ".  json_encode($target_sale_vendor));
            
			
			foreach ($target_sale_vendor as $vendor_id=>$vendor){
				if(!in_array($vendor_id,$vendor_ids))continue;
			
				$r2_sale = $vendor['base'] - $vendor['sale'];
				$r1_sale = $vendor['target'] - $vendor['sale'];
			
				if($vendor['sale']*100/$vendor['target'] > $cap_non_primary){
					$non_primary[] = $vendor_id;
				}
				if($vendor['sims'] > 0 && $r1_sale > 0){
					$weight = $r1_sale/$vendor['sims'];
					$priority_1[$weight] = $vendor_id;
				}
				else if($vendor['sims'] > 0 && $r2_sale > 0){
					$weight = $r2_sale/$vendor['sims'];
					$priority_2[$weight] = $vendor_id;
				}
				
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::r1_sale $r1_sale :: r2_sale $r2_sale");
            
				
				//$modem_cutoff_arr = array();
				foreach($vendor['data'] as $dt){
					$supplier = $dt['devices_data']['supplier_operator_id'];
					$target_supp = $target_sale_supplier[$supplier]['target'];
					$base_supp = $target_sale_supplier[$supplier]['base'];
					
					$ratio = ($base_supp < $target_supp) ? 1 :  ($base_supp/$target_supp);
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id :: ".$target_sale_supplier[$supplier]['supplier_id']." :: base $base_supp:: target_supp ::$target_supp:: vendor_supp_target:: ".$dt['inv_modem_planning_sheet']['target']."::vendor_supp_sale::".$dt['0']['totalsale']." :: ratio $ratio");
            
					if($dt['0']['totalsale'] > $ratio*$dt['inv_modem_planning_sheet']['target']){
						$query = "query=salestop&supplier_id=".$target_sale_supplier[$supplier]['supplier_id']."&product=$opr_ids&flag=1";
						$this->async_request_via_redis($query,$vendor_id);
						$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::cutoff query :: $query");
					}
				}

				
				if($r1_sale < 0 && $r2_sale < 0){
					$cut_off[] = $vendor_id;
				}
				
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::vendors cutoffs  :: ".json_encode($cut_off));
			}
			
			
			//get other vendors as well
			$vendors_data = $invoiceObj->query("SELECT vendors.id,base_amount,max_sale_capacity,imps.target FROM `inv_planning_sheet` as ips inner join inv_supplier_operator as iso ON (ips.supplier_operator_id = iso.id) inner join inv_modem_planning_sheet as imps ON (imps.supplier_operator_id = iso.id) inner join inv_supplier_vendor_mapping as isvm ON (isvm.supplier_id = iso.supplier_id) inner join vendors ON (vendors.id = isvm.vendor_id) WHERE vendors.update_flag = 0 AND operator_id in ($opr_ids)");
			
			foreach($vendors_data as $vd){
				if(!in_array($vd['vendors']['id'],$vendor_ids))continue;

				$prod = $this->getParentProd($prodId);
                
				$cap_per_min = $this->getMemcache("cap_api_".$prod."_".$vd['vendors']['id']);
                
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prod:: ".$vd['vendors']['id']." ::cap_per_min $cap_per_min :: "."cap_api_".$prod."_".$vd['vendors']['id']);
            
				if($cap_per_min !== false && $cap_per_min > 0){
					//$sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
                                        $sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
					$r2_sale = $vd['ips']['max_sale_capacity'] - $sale;
					$r1_sale = $vd['imps']['target'] - $sale;
	
					if($sale*100/$vd['imps']['target'] > $cap_non_primary){
						$non_primary[] = $vd['vendors']['id'];
					}
					
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: ".$vd['vendors']['id']." ::r1_sale $r1_sale :: r2_sale $r2_sale :: $cap_per_min");
            
                    
					if($r1_sale > 0){
						$weight = $r1_sale/$cap_per_min;	
						$priority_1[$weight] = $vd['vendors']['id'];
					}
					else if($r2_sale > 0){
						$weight = $r2_sale/$cap_per_min;	
						$priority_2[$weight] = $vd['vendors']['id'];
					}
					
					if($r1_sale < 0 && $r2_sale < 0){
						$cut_off[] =$vd['vendors']['id'];
					}
				}
				
			}
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			krsort($priority_1);
			krsort($priority_2);
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: after sort :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: cutoff vendors :: $prodId:: ".  json_encode($cut_off));
            
			$info['vendors'] = array();
			foreach($priority_1 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			foreach($priority_2 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			
			foreach($vendors_n as $k => $v){
				if(in_array($k,$final))continue;
				if(in_array($k,$cut_off))continue;
				$info['vendors'][] = $v;
			}
            
                        /**
                         * adhock for all transaction between 23:45 - 24:00 Hrs priority vendors will be in reverse order, so that if api vendor 
                         * unable to clear txn modem should able to clear them.
                         */
                        if(date('H') == "23" && date("i") >= 45){
                            $info['vendors'] = array_reverse($info['vendors'],true);
                        }
                        
			$info['non_primary'] = $non_primary;
                        $this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: final :: $prodId :: ". json_encode($info['vendors']) . "::non-primary vendors::".json_encode($info['non_primary']));
		}
        
		$this->setMemcache("prod$prodId",$info,5*60);
		return $info;
	}
		
	function getVendors(){
		$vendors = $this->getMemcache('vendors');
		if(empty($vendors)){
			$vendors = $this->setVendors();
		}
		
		return $vendors;
	}
	
	function setVendors(){
		$invoiceObj = ClassRegistry::init('Slaves');
		$vendors = $invoiceObj->query("SELECT id,shortForm,company,balance,user_id,update_flag,ip,port FROM vendors WHERE show_flag = 1 order by company");	
		$this->setMemcache('vendors',$vendors,60*60*24);
		
		return $vendors;
	}
	
	function getInactiveVendors(){
		$vendors = $this->getMemcache('inactiveVendors');
		if($vendors === false){
			$vendors = $this->setInactiveVendors();
		}
		
		return $vendors;
	}
	
	function setInactiveVendors(){
		$invoiceObj = ClassRegistry::init('Slaves');
		$vendors = $invoiceObj->query("SELECT group_concat(id) as ids FROM vendors WHERE active_flag != 1");
		$vendors = explode(",",$vendors['0']['0']['ids']);
		$this->setMemcache('inactiveVendors',$vendors,2*60);
		
		return $vendors;
	}
	
	function getProducts(){
		$products = $this->getMemcache('products');
		if(empty($products)){
			$invoiceObj = ClassRegistry::init('Slaves');
			$products = $invoiceObj->query("SELECT * FROM products");	
			$this->setMemcache('products',$products,60*60);
		}
		
		return $products;
	}
	
	function findOptimalVendor($vendors,$prodId,$automate_flag,$existing=array(),$transId=null){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
		
		foreach($vendors as $vend){
			$vendor_id = $vend['vendor_id'];
			if(in_array($vendor_id,$existing))continue;
			
			if($vend['update_flag'] == 0){	
				$prod = (isset($arr_map[$prodId]))? $arr_map[$prodId] :  $prodId;
				$data = $this->getMemcache("status_$prod"."_$vendor_id");

				$this->General->logData("/mnt/logs/abc_$vendor_id.txt","before decrement::$transId::$prod::$vendor_id::$data");
					
				if($data !== false && $data > 0){
					$data = $this->decrementMemcache("status_$prod"."_$vendor_id");
					if($data >= 0){
					$this->General->logData("/mnt/logs/abc_$vendor_id.txt","after decrement::$transId::$prod::$vendor_id::$data");
					$vend['key_vendor'] = "status_$prod"."_$vendor_id";
					return $vend;
				}
				}
					
				if($data === false){
					return $vend;
				}
			}
			else return $vend;
		}
	}
	
	function getActiveVendor($prodId,$mobile=null,$apiPartner=null,$primary_flag=true,$additional_param=array()){
		$success = true;

		$info = $this->getProdInfo($prodId);
		if(empty($info['vendors'])){
			$info = $this->setProdInfo($prodId);
		}
		
		$non_primary = (isset($info['non_primary'])) ? $info['non_primary'] : array();
		$api_flag = false;
		if($mobile != null){
			$prod_d = $this->General->getMobileDetailsNew($mobile);// need to open for mobile numbering changes
                        //$prod_d = $this->General->getMobileDetails($mobile);
                        
			if(!$api_flag && !empty($info['circles_yes']) && !in_array($prod_d['area'],explode(",",$info['circles_yes']))){
				return array('status'=>'failure','code'=>'','description'=>'Cannot recharge on '.$prod_d['area'].' circle','name'=>$info['name']);
			}

			if(!$api_flag && !empty($info['circles_no']) && in_array($prod_d['area'],explode(",",$info['circles_no']))){
				return array('status'=>'failure','code'=>'','description'=>'Cannot recharge on '.$prod_d['area'].' circle','name'=>$info['name']);
			}
		}
                
		if($apiPartner == 6) {//b2c api partner
			$api_flag = true;
		}
		//distributor id & vendor id mapping, key will be distid_prodid & value will be vendorid 
                // 75 - cotton_green 71-kandivali 10-GSM modem2
                
                $dist_vendor_mapping = $this->generate_local_vendor_map();
                // removing mira-rd
                // mira-rd : '758_2'=>'84','776_2'=>'84','866_2'=>'84','920_2'=>'84','14477_2'=>'84','28199_2'=>'84','41433_2'=>'84','77467_2'=>'84','12051_2'=>'84','36831_2'=>'84','103479_2'=>'84','107208_2'=>'84',
                // santacruz, andheri, jogeshwari
                // jogeshwari : '467_2'=>'99','478_2'=>'99','529_2'=>'99','535_2'=>'99','4905_2'=>'99','7335_2'=>'99','8272_2'=>'99','8285_2'=>'99','8315_2'=>'99','8320_2'=>'99','8324_2'=>'99','8428_2'=>'99','8461_2'=>'99','8552_2'=>'99','8824_2'=>'99','8854_2'=>'99','9109_2'=>'99','9115_2'=>'99','9117_2'=>'99','9121_2'=>'99','9171_2'=>'99','9420_2'=>'99','10364_2'=>'99','10442_2'=>'99','10505_2'=>'99','10557_2'=>'99','10815_2'=>'99','10821_2'=>'99','11393_2'=>'99','11602_2'=>'99','11845_2'=>'99','11872_2'=>'99','12356_2'=>'99','12412_2'=>'99','12965_2'=>'99','13116_2'=>'99','13385_2'=>'99','14069_2'=>'99','14116_2'=>'99','15662_2'=>'99','17260_2'=>'99','17309_2'=>'99','17361_2'=>'99','17481_2'=>'99','19647_2'=>'99','19727_2'=>'99','20165_2'=>'99','20680_2'=>'99','21749_2'=>'99','22035_2'=>'99','22263_2'=>'99','22268_2'=>'99','22650_2'=>'99','22831_2'=>'99','23179_2'=>'99','23439_2'=>'99','23874_2'=>'99','25691_2'=>'99','25694_2'=>'99','25785_2'=>'99','29116_2'=>'99','29307_2'=>'99','32099_2'=>'99','32821_2'=>'99','32829_2'=>'99','32836_2'=>'99','32842_2'=>'99','33247_2'=>'99','33810_2'=>'99','35427_2'=>'99','36598_2'=>'99','37910_2'=>'99','37938_2'=>'99','41726_2'=>'99','42481_2'=>'99','44507_2'=>'99','47040_2'=>'99','48184_2'=>'99','48945_2'=>'99','53435_2'=>'99','53908_2'=>'99','55067_2'=>'99','55224_2'=>'99','56693_2'=>'99','57604_2'=>'99','60347_2'=>'99','60959_2'=>'99','61723_2'=>'99','62791_2'=>'99','68860_2'=>'99','69308_2'=>'99','70125_2'=>'99','70879_2'=>'99','75657_2'=>'99','75658_2'=>'99','81390_2'=>'99','81550_2'=>'99','86768_2'=>'99','89449_2'=>'99','93546_2'=>'99','104088_2'=>'99','111577_2'=>'99','112777_2'=>'99','112945_2'=>'99','114790_2'=>'99','121013_2'=>'99','121757_2'=>'99','122192_2'=>'99','122748_2'=>'99','125172_2'=>'99'
                $ret_vendor_mapping = array(
                    '282_2'=>'96','311_2'=>'96','339_2'=>'96','346_2'=>'96','364_2'=>'96','365_2'=>'96','400_2'=>'96','416_2'=>'96','444_2'=>'96','453_2'=>'96','454_2'=>'96','462_2'=>'96','475_2'=>'96','481_2'=>'96','486_2'=>'96','487_2'=>'96','491_2'=>'96','508_2'=>'96','511_2'=>'96','515_2'=>'96','519_2'=>'96','542_2'=>'96','547_2'=>'96','564_2'=>'96','566_2'=>'96','576_2'=>'96','585_2'=>'96','615_2'=>'96','623_2'=>'96','651_2'=>'96','655_2'=>'96','656_2'=>'96','711_2'=>'96','723_2'=>'96','741_2'=>'96','810_2'=>'96','831_2'=>'96','1008_2'=>'96','1025_2'=>'96','1059_2'=>'96','1165_2'=>'96','2181_2'=>'96','2186_2'=>'96','2190_2'=>'96','2207_2'=>'96','2209_2'=>'96','2212_2'=>'96','2223_2'=>'96','2224_2'=>'96','2227_2'=>'96','2229_2'=>'96','2235_2'=>'96','2238_2'=>'96','2247_2'=>'96','2248_2'=>'96','2251_2'=>'96','2254_2'=>'96','2257_2'=>'96','2259_2'=>'96','2260_2'=>'96','2261_2'=>'96','2262_2'=>'96','2263_2'=>'96','2270_2'=>'96','2273_2'=>'96','2277_2'=>'96','2279_2'=>'96','2282_2'=>'96','2285_2'=>'96','2287_2'=>'96','2289_2'=>'96','2291_2'=>'96','2293_2'=>'96','2308_2'=>'96','2316_2'=>'96','2325_2'=>'96','2331_2'=>'96','2349_2'=>'96','2354_2'=>'96','2357_2'=>'96','2362_2'=>'96','2363_2'=>'96','2364_2'=>'96','2367_2'=>'96','2375_2'=>'96','2377_2'=>'96','2378_2'=>'96','2387_2'=>'96','2398_2'=>'96','2410_2'=>'96','2419_2'=>'96','2434_2'=>'96','2461_2'=>'96','2468_2'=>'96','2486_2'=>'96','2489_2'=>'96','2491_2'=>'96','2521_2'=>'96','2522_2'=>'96','2523_2'=>'96','2546_2'=>'96','2608_2'=>'96','2610_2'=>'96','2621_2'=>'96','2639_2'=>'96','2649_2'=>'96','2650_2'=>'96','2660_2'=>'96','2703_2'=>'96','2708_2'=>'96','2755_2'=>'96','2763_2'=>'96','2805_2'=>'96','2828_2'=>'96','2847_2'=>'96','2887_2'=>'96','2914_2'=>'96','2916_2'=>'96','2979_2'=>'96','3006_2'=>'96','3008_2'=>'96','3009_2'=>'96','3024_2'=>'96','3105_2'=>'96','3112_2'=>'96','3398_2'=>'96','3399_2'=>'96','3579_2'=>'96','3666_2'=>'96','3694_2'=>'96','3696_2'=>'96','3796_2'=>'96','3801_2'=>'96','3818_2'=>'96','3826_2'=>'96','3827_2'=>'96','3969_2'=>'96','4020_2'=>'96','4089_2'=>'96','4341_2'=>'96','4346_2'=>'96','4385_2'=>'96','4386_2'=>'96','4401_2'=>'96','4416_2'=>'96','4421_2'=>'96','4609_2'=>'96','4640_2'=>'96','4653_2'=>'96','4655_2'=>'96','4700_2'=>'96','4730_2'=>'96','4764_2'=>'96','4775_2'=>'96','4785_2'=>'96','4818_2'=>'96','4838_2'=>'96','4854_2'=>'96','4938_2'=>'96','4980_2'=>'96','4981_2'=>'96','5016_2'=>'96','5022_2'=>'96','5133_2'=>'96','5229_2'=>'96','5231_2'=>'96','5391_2'=>'96','5430_2'=>'96','5523_2'=>'96','5531_2'=>'96','5729_2'=>'96','5809_2'=>'96','5954_2'=>'96','5984_2'=>'96','5989_2'=>'96','6100_2'=>'96','6224_2'=>'96','6247_2'=>'96','6434_2'=>'96','6453_2'=>'96','6508_2'=>'96','6519_2'=>'96','6594_2'=>'96','6683_2'=>'96','6685_2'=>'96','6742_2'=>'96','7323_2'=>'96','7521_2'=>'96','7653_2'=>'96','7796_2'=>'96','7800_2'=>'96','7849_2'=>'96','7901_2'=>'96','7990_2'=>'96','8065_2'=>'96','8107_2'=>'96','8295_2'=>'96','8338_2'=>'96','8404_2'=>'96','8894_2'=>'96','9027_2'=>'96','9100_2'=>'96','9114_2'=>'96','9137_2'=>'96','9175_2'=>'96','9225_2'=>'96','9326_2'=>'96','9387_2'=>'96','9397_2'=>'96','9413_2'=>'96','9414_2'=>'96','9459_2'=>'96','9479_2'=>'96','9587_2'=>'96','9678_2'=>'96','9796_2'=>'96','9810_2'=>'96','9923_2'=>'96','9977_2'=>'96','10171_2'=>'96','10512_2'=>'96','10520_2'=>'96','10799_2'=>'96','10836_2'=>'96','11207_2'=>'96','11255_2'=>'96','11278_2'=>'96','11628_2'=>'96','11964_2'=>'96','12053_2'=>'96','12127_2'=>'96','12142_2'=>'96','12167_2'=>'96','12241_2'=>'96','12273_2'=>'96','12284_2'=>'96','12304_2'=>'96','12479_2'=>'96','12510_2'=>'96','12535_2'=>'96','12551_2'=>'96','12716_2'=>'96','12740_2'=>'96','12803_2'=>'96','12805_2'=>'96','12835_2'=>'96','12843_2'=>'96','13004_2'=>'96','13054_2'=>'96','13065_2'=>'96','13135_2'=>'96','13139_2'=>'96','13239_2'=>'96','13273_2'=>'96','13462_2'=>'96','13523_2'=>'96','13594_2'=>'96','13677_2'=>'96','13720_2'=>'96','13996_2'=>'96','14008_2'=>'96','14010_2'=>'96','14012_2'=>'96','14043_2'=>'96','14099_2'=>'96','14137_2'=>'96','14156_2'=>'96','14302_2'=>'96','14316_2'=>'96','14526_2'=>'96','14742_2'=>'96','14828_2'=>'96','14839_2'=>'96','14909_2'=>'96','14989_2'=>'96','15042_2'=>'96','15165_2'=>'96','15534_2'=>'96','15607_2'=>'96','15888_2'=>'96','15968_2'=>'96','15969_2'=>'96','16004_2'=>'96','16188_2'=>'96','16232_2'=>'96','16287_2'=>'96','16324_2'=>'96','16334_2'=>'96','16581_2'=>'96','16631_2'=>'96','16642_2'=>'96','16792_2'=>'96','17123_2'=>'96','17336_2'=>'96','17743_2'=>'96','18004_2'=>'96','18185_2'=>'96','18246_2'=>'96','18336_2'=>'96','18534_2'=>'96','18674_2'=>'96','18706_2'=>'96','18724_2'=>'96','18737_2'=>'96','19011_2'=>'96','19459_2'=>'96','19486_2'=>'96','19516_2'=>'96','19526_2'=>'96','19528_2'=>'96','19558_2'=>'96','19561_2'=>'96','20060_2'=>'96','20115_2'=>'96','20224_2'=>'96','20355_2'=>'96','20499_2'=>'96','20500_2'=>'96','20622_2'=>'96','21176_2'=>'96','21254_2'=>'96','21817_2'=>'96','21849_2'=>'96','22177_2'=>'96','22518_2'=>'96','22566_2'=>'96','22616_2'=>'96','22692_2'=>'96','23040_2'=>'96','23062_2'=>'96','23221_2'=>'96','23646_2'=>'96','24326_2'=>'96','24434_2'=>'96','24481_2'=>'96','24531_2'=>'96','24614_2'=>'96','25215_2'=>'96','25400_2'=>'96','25413_2'=>'96','25546_2'=>'96','25844_2'=>'96','25856_2'=>'96','26022_2'=>'96','26227_2'=>'96','26235_2'=>'96','26260_2'=>'96','26343_2'=>'96','26388_2'=>'96','26958_2'=>'96','27422_2'=>'96','27677_2'=>'96','27756_2'=>'96','27823_2'=>'96','27959_2'=>'96','28392_2'=>'96','28403_2'=>'96','28472_2'=>'96','28673_2'=>'96','28693_2'=>'96','28758_2'=>'96','28802_2'=>'96','28809_2'=>'96','28947_2'=>'96','29004_2'=>'96','29164_2'=>'96','29285_2'=>'96','29386_2'=>'96','29557_2'=>'96','29775_2'=>'96','30070_2'=>'96','30294_2'=>'96','30347_2'=>'96','30426_2'=>'96','31010_2'=>'96','31410_2'=>'96','31617_2'=>'96','31623_2'=>'96','31792_2'=>'96','31797_2'=>'96','32214_2'=>'96','32242_2'=>'96','32281_2'=>'96','32455_2'=>'96','33047_2'=>'96','33198_2'=>'96','33581_2'=>'96','33750_2'=>'96','33760_2'=>'96','33816_2'=>'96','34532_2'=>'96','34664_2'=>'96','34779_2'=>'96','34852_2'=>'96','34940_2'=>'96','35282_2'=>'96','35283_2'=>'96','35298_2'=>'96','35329_2'=>'96','35396_2'=>'96','35512_2'=>'96','35715_2'=>'96','35838_2'=>'96','36541_2'=>'96','36977_2'=>'96','37549_2'=>'96','38097_2'=>'96','38320_2'=>'96','38427_2'=>'96','38437_2'=>'96','38721_2'=>'96','38742_2'=>'96','38750_2'=>'96','38894_2'=>'96','39491_2'=>'96','40097_2'=>'96','40342_2'=>'96','40920_2'=>'96','41014_2'=>'96','41471_2'=>'96','41542_2'=>'96','41548_2'=>'96','41587_2'=>'96','41858_2'=>'96','42385_2'=>'96','42500_2'=>'96','42665_2'=>'96','43833_2'=>'96','43907_2'=>'96','44176_2'=>'96','44380_2'=>'96','44385_2'=>'96','44545_2'=>'96','44558_2'=>'96','45316_2'=>'96','45396_2'=>'96','45555_2'=>'96','46181_2'=>'96','46464_2'=>'96','46572_2'=>'96','47335_2'=>'96','47337_2'=>'96','48095_2'=>'96','48099_2'=>'96','48586_2'=>'96','48594_2'=>'96','49739_2'=>'96','49840_2'=>'96','49931_2'=>'96','51179_2'=>'96','52540_2'=>'96','52741_2'=>'96','53173_2'=>'96','53186_2'=>'96','53270_2'=>'96','53333_2'=>'96','53936_2'=>'96','54377_2'=>'96','54523_2'=>'96','55140_2'=>'96','55236_2'=>'96','55262_2'=>'96','55556_2'=>'96','55869_2'=>'96','56725_2'=>'96','56846_2'=>'96','57006_2'=>'96','57195_2'=>'96','57399_2'=>'96','57966_2'=>'96','58024_2'=>'96','58914_2'=>'96','58943_2'=>'96','59333_2'=>'96','59645_2'=>'96','60038_2'=>'96','60644_2'=>'96','60687_2'=>'96','60888_2'=>'96','61329_2'=>'96','61489_2'=>'96','62364_2'=>'96','63138_2'=>'96','63505_2'=>'96','63854_2'=>'96','65711_2'=>'96','66122_2'=>'96','66412_2'=>'96','66498_2'=>'96','66729_2'=>'96','66975_2'=>'96','67909_2'=>'96','68880_2'=>'96','70464_2'=>'96','70931_2'=>'96','71039_2'=>'96','71298_2'=>'96','71323_2'=>'96','71362_2'=>'96','71962_2'=>'96','73320_2'=>'96','73529_2'=>'96','73620_2'=>'96','73995_2'=>'96','74024_2'=>'96','74295_2'=>'96','74666_2'=>'96','75268_2'=>'96','75879_2'=>'96','76041_2'=>'96','76783_2'=>'96','77533_2'=>'96','79096_2'=>'96','79265_2'=>'96','79928_2'=>'96','80410_2'=>'96','80951_2'=>'96','82234_2'=>'96','83568_2'=>'96','84707_2'=>'96','85005_2'=>'96','85232_2'=>'96','85283_2'=>'96','85812_2'=>'96','87441_2'=>'96','88362_2'=>'96','90051_2'=>'96','90296_2'=>'96','91004_2'=>'96','91275_2'=>'96','92158_2'=>'96','93200_2'=>'96','93340_2'=>'96','94487_2'=>'96','94852_2'=>'96','95516_2'=>'96','97116_2'=>'96','97198_2'=>'96','99058_2'=>'96','99751_2'=>'96','100092_2'=>'96','100343_2'=>'96','101068_2'=>'96','102080_2'=>'96','102258_2'=>'96','105545_2'=>'96','105759_2'=>'96','106608_2'=>'96','107033_2'=>'96','107676_2'=>'96','107912_2'=>'96','108281_2'=>'96','111766_2'=>'96','112334_2'=>'96','113219_2'=>'96','113541_2'=>'96','115526_2'=>'96','116749_2'=>'96','118155_2'=>'96','119967_2'=>'96','120934_2'=>'96','121031_2'=>'96','121615_2'=>'96','122051_2'=>'96','16183_2'=>'96','16247_2'=>'96','16647_2'=>'96','16692_2'=>'96','16885_2'=>'96','17073_2'=>'96','17598_2'=>'96','28161_2'=>'96','40474_2'=>'96','48316_2'=>'96','48360_2'=>'96','48398_2'=>'96','48418_2'=>'96','48512_2'=>'96','48539_2'=>'96','48547_2'=>'96','48641_2'=>'96','48680_2'=>'96','48823_2'=>'96','48930_2'=>'96','48960_2'=>'96','48997_2'=>'96','49032_2'=>'96','49162_2'=>'96','49253_2'=>'96','49432_2'=>'96','49456_2'=>'96','49704_2'=>'96','49953_2'=>'96','50184_2'=>'96','50375_2'=>'96','50599_2'=>'96','50602_2'=>'96','50611_2'=>'96','50800_2'=>'96','50834_2'=>'96','50898_2'=>'96','50911_2'=>'96','50967_2'=>'96','50968_2'=>'96','51121_2'=>'96','51383_2'=>'96','51517_2'=>'96','51799_2'=>'96','52000_2'=>'96','52289_2'=>'96','52311_2'=>'96','53051_2'=>'96','53847_2'=>'96','54042_2'=>'96','54067_2'=>'96','54621_2'=>'96','55147_2'=>'96','55269_2'=>'96','56200_2'=>'96','57617_2'=>'96','57743_2'=>'96','58397_2'=>'96','58595_2'=>'96','60642_2'=>'96','60851_2'=>'96','61846_2'=>'96','61890_2'=>'96','62622_2'=>'96','62684_2'=>'96','62800_2'=>'96','63618_2'=>'96','63622_2'=>'96','64379_2'=>'96','65167_2'=>'96','65562_2'=>'96','65717_2'=>'96','66460_2'=>'96','66625_2'=>'96','66799_2'=>'96','68231_2'=>'96','69496_2'=>'96','70386_2'=>'96','70976_2'=>'96','75374_2'=>'96','76203_2'=>'96','76377_2'=>'96','76818_2'=>'96','77839_2'=>'96','78776_2'=>'96','78863_2'=>'96','79054_2'=>'96','80332_2'=>'96','82415_2'=>'96','82440_2'=>'96','82463_2'=>'96','82507_2'=>'96','82584_2'=>'96','82615_2'=>'96','82680_2'=>'96','82738_2'=>'96','82775_2'=>'96','82784_2'=>'96','82808_2'=>'96','82820_2'=>'96','82948_2'=>'96','82962_2'=>'96','82964_2'=>'96','83237_2'=>'96','83286_2'=>'96','83331_2'=>'96','83342_2'=>'96','83635_2'=>'96','84019_2'=>'96','84036_2'=>'96','84437_2'=>'96','84561_2'=>'96','84614_2'=>'96','84638_2'=>'96','84721_2'=>'96','84756_2'=>'96','84846_2'=>'96','84895_2'=>'96','84958_2'=>'96','84960_2'=>'96','85004_2'=>'96','85109_2'=>'96','85118_2'=>'96','85147_2'=>'96','85178_2'=>'96','85195_2'=>'96','85247_2'=>'96','85362_2'=>'96','85366_2'=>'96','85450_2'=>'96','85597_2'=>'96','86014_2'=>'96','86121_2'=>'96','86129_2'=>'96','86155_2'=>'96','86238_2'=>'96','86281_2'=>'96','86449_2'=>'96','86498_2'=>'96','86537_2'=>'96','86915_2'=>'96','86962_2'=>'96','87178_2'=>'96','87220_2'=>'96','87280_2'=>'96','87311_2'=>'96','87733_2'=>'96','87766_2'=>'96','87782_2'=>'96','87807_2'=>'96','87811_2'=>'96','87879_2'=>'96','87893_2'=>'96','87920_2'=>'96','87927_2'=>'96','87931_2'=>'96','87941_2'=>'96','87998_2'=>'96','88264_2'=>'96','88313_2'=>'96','88335_2'=>'96','88374_2'=>'96','88378_2'=>'96','88397_2'=>'96','88423_2'=>'96','88524_2'=>'96','88639_2'=>'96','88684_2'=>'96','88771_2'=>'96','88811_2'=>'96','88886_2'=>'96','89020_2'=>'96','89072_2'=>'96','89157_2'=>'96','89183_2'=>'96','89804_2'=>'96','89817_2'=>'96','90021_2'=>'96','90055_2'=>'96','90057_2'=>'96','90094_2'=>'96','90234_2'=>'96','90280_2'=>'96','90493_2'=>'96','90570_2'=>'96','90600_2'=>'96','90872_2'=>'96','91380_2'=>'96','91504_2'=>'96','91661_2'=>'96','91855_2'=>'96','91944_2'=>'96','91954_2'=>'96','92049_2'=>'96','92121_2'=>'96','92698_2'=>'96','92917_2'=>'96','93195_2'=>'96','93437_2'=>'96','94058_2'=>'96','94334_2'=>'96','94397_2'=>'96','96394_2'=>'96','97635_2'=>'96','97790_2'=>'96','98425_2'=>'96','102357_2'=>'96','102361_2'=>'96','102862_2'=>'96','103026_2'=>'96','103073_2'=>'96','103337_2'=>'96','103685_2'=>'96','103920_2'=>'96','104199_2'=>'96','104201_2'=>'96','104223_2'=>'96','105240_2'=>'96','105494_2'=>'96','105649_2'=>'96','106139_2'=>'96','106214_2'=>'96','107243_2'=>'96','107858_2'=>'96','108228_2'=>'96','109442_2'=>'96','111713_2'=>'96','112070_2'=>'96','112668_2'=>'96','113116_2'=>'96','113117_2'=>'96','115837_2'=>'96','116017_2'=>'96','117971_2'=>'96','118962_2'=>'96','119997_2'=>'96','120138_2'=>'96','120994_2'=>'96','121667_2'=>'96','121738_2'=>'96','121783_2'=>'96','122039_2'=>'96','122303_2'=>'96','122788_2'=>'96','123173_2'=>'96','124139_2'=>'96','125006_2'=>'96',
                    '185_2'=>'95','187_2'=>'95','188_2'=>'95','194_2'=>'95','195_2'=>'95','198_2'=>'95','202_2'=>'95','204_2'=>'95','206_2'=>'95','210_2'=>'95','217_2'=>'95','219_2'=>'95','220_2'=>'95','223_2'=>'95','225_2'=>'95','228_2'=>'95','229_2'=>'95','230_2'=>'95','231_2'=>'95','235_2'=>'95','238_2'=>'95','239_2'=>'95','244_2'=>'95','248_2'=>'95','250_2'=>'95','251_2'=>'95','252_2'=>'95','254_2'=>'95','256_2'=>'95','258_2'=>'95','260_2'=>'95','261_2'=>'95','263_2'=>'95','265_2'=>'95','268_2'=>'95','274_2'=>'95','275_2'=>'95','276_2'=>'95','279_2'=>'95','280_2'=>'95','283_2'=>'95','287_2'=>'95','291_2'=>'95','292_2'=>'95','294_2'=>'95','296_2'=>'95','305_2'=>'95','307_2'=>'95','308_2'=>'95','309_2'=>'95','315_2'=>'95','316_2'=>'95','322_2'=>'95','323_2'=>'95','326_2'=>'95','327_2'=>'95','336_2'=>'95','342_2'=>'95','358_2'=>'95','361_2'=>'95','369_2'=>'95','370_2'=>'95','373_2'=>'95','383_2'=>'95','385_2'=>'95','388_2'=>'95','398_2'=>'95','399_2'=>'95','402_2'=>'95','403_2'=>'95','406_2'=>'95','417_2'=>'95','427_2'=>'95','436_2'=>'95','438_2'=>'95','442_2'=>'95','449_2'=>'95','450_2'=>'95','456_2'=>'95','459_2'=>'95','473_2'=>'95','480_2'=>'95','484_2'=>'95','488_2'=>'95','490_2'=>'95','493_2'=>'95','498_2'=>'95','499_2'=>'95','512_2'=>'95','513_2'=>'95','514_2'=>'95','516_2'=>'95','520_2'=>'95','521_2'=>'95','525_2'=>'95','526_2'=>'95','531_2'=>'95','540_2'=>'95','541_2'=>'95','550_2'=>'95','556_2'=>'95','560_2'=>'95','562_2'=>'95','565_2'=>'95','570_2'=>'95','572_2'=>'95','574_2'=>'95','581_2'=>'95','582_2'=>'95','583_2'=>'95','584_2'=>'95','587_2'=>'95','588_2'=>'95','590_2'=>'95','597_2'=>'95','598_2'=>'95','599_2'=>'95','601_2'=>'95','602_2'=>'95','606_2'=>'95','607_2'=>'95','608_2'=>'95','614_2'=>'95','616_2'=>'95','620_2'=>'95','622_2'=>'95','628_2'=>'95','629_2'=>'95','639_2'=>'95','640_2'=>'95','643_2'=>'95','644_2'=>'95','647_2'=>'95','658_2'=>'95','660_2'=>'95','669_2'=>'95','673_2'=>'95','674_2'=>'95','694_2'=>'95','698_2'=>'95','710_2'=>'95','718_2'=>'95','724_2'=>'95','733_2'=>'95','736_2'=>'95','737_2'=>'95','746_2'=>'95','766_2'=>'95','775_2'=>'95','778_2'=>'95','783_2'=>'95','785_2'=>'95','788_2'=>'95','789_2'=>'95','790_2'=>'95','793_2'=>'95','794_2'=>'95','796_2'=>'95','801_2'=>'95','802_2'=>'95','803_2'=>'95','805_2'=>'95','808_2'=>'95','809_2'=>'95','811_2'=>'95','812_2'=>'95','820_2'=>'95','824_2'=>'95','825_2'=>'95','826_2'=>'95','827_2'=>'95','829_2'=>'95','830_2'=>'95','835_2'=>'95','836_2'=>'95','842_2'=>'95','843_2'=>'95','844_2'=>'95','846_2'=>'95','847_2'=>'95','848_2'=>'95','849_2'=>'95','850_2'=>'95','853_2'=>'95','864_2'=>'95','875_2'=>'95','894_2'=>'95','895_2'=>'95','900_2'=>'95','902_2'=>'95','941_2'=>'95','962_2'=>'95','1000_2'=>'95','1024_2'=>'95','1038_2'=>'95','1054_2'=>'95','1055_2'=>'95','1058_2'=>'95','1069_2'=>'95','1084_2'=>'95','1106_2'=>'95','1107_2'=>'95','1145_2'=>'95','2267_2'=>'95','2372_2'=>'95','2407_2'=>'95','2431_2'=>'95','2442_2'=>'95','2444_2'=>'95','2689_2'=>'95','2751_2'=>'95','2922_2'=>'95','2997_2'=>'95','3022_2'=>'95','3232_2'=>'95','3397_2'=>'95','3547_2'=>'95','3551_2'=>'95','3553_2'=>'95','3565_2'=>'95','3589_2'=>'95','3647_2'=>'95','3695_2'=>'95','3710_2'=>'95','3716_2'=>'95','3807_2'=>'95','3815_2'=>'95','3862_2'=>'95','3998_2'=>'95','4005_2'=>'95','4075_2'=>'95','4086_2'=>'95','4106_2'=>'95','4117_2'=>'95','4139_2'=>'95','4148_2'=>'95','4151_2'=>'95','4169_2'=>'95','4175_2'=>'95','4186_2'=>'95','4195_2'=>'95','4198_2'=>'95','4205_2'=>'95','4290_2'=>'95','4326_2'=>'95','4331_2'=>'95','4342_2'=>'95','4361_2'=>'95','4379_2'=>'95','4417_2'=>'95','4517_2'=>'95','4539_2'=>'95','4543_2'=>'95','4559_2'=>'95','4566_2'=>'95','4618_2'=>'95','4621_2'=>'95','4690_2'=>'95','4699_2'=>'95','4710_2'=>'95','4757_2'=>'95','4782_2'=>'95','4822_2'=>'95','4840_2'=>'95','4849_2'=>'95','4869_2'=>'95','4883_2'=>'95','4892_2'=>'95','4901_2'=>'95','4918_2'=>'95','5013_2'=>'95','5038_2'=>'95','5041_2'=>'95','5052_2'=>'95','5236_2'=>'95','5388_2'=>'95','5398_2'=>'95','5405_2'=>'95','5472_2'=>'95','5544_2'=>'95','5682_2'=>'95','5780_2'=>'95','5885_2'=>'95','5946_2'=>'95','5949_2'=>'95','5983_2'=>'95','6068_2'=>'95','6119_2'=>'95','6226_2'=>'95','6384_2'=>'95','6416_2'=>'95','6419_2'=>'95','6442_2'=>'95','6520_2'=>'95','6532_2'=>'95','6586_2'=>'95','6598_2'=>'95','6651_2'=>'95','6672_2'=>'95','6749_2'=>'95','6821_2'=>'95','6843_2'=>'95','7064_2'=>'95','7162_2'=>'95','7163_2'=>'95','7692_2'=>'95','7847_2'=>'95','7863_2'=>'95','7864_2'=>'95','7884_2'=>'95','7897_2'=>'95','7991_2'=>'95','7995_2'=>'95','8095_2'=>'95','8105_2'=>'95','8141_2'=>'95','8154_2'=>'95','8246_2'=>'95','8337_2'=>'95','8351_2'=>'95','8382_2'=>'95','8417_2'=>'95','8452_2'=>'95','8454_2'=>'95','9072_2'=>'95','9135_2'=>'95','9270_2'=>'95','9286_2'=>'95','9296_2'=>'95','9324_2'=>'95','9370_2'=>'95','9389_2'=>'95','9624_2'=>'95','9777_2'=>'95','9920_2'=>'95','9983_2'=>'95','10462_2'=>'95','10618_2'=>'95','10621_2'=>'95','10754_2'=>'95','10789_2'=>'95','10818_2'=>'95','10830_2'=>'95','10850_2'=>'95','10975_2'=>'95','11001_2'=>'95','11055_2'=>'95','11086_2'=>'95','11684_2'=>'95','11731_2'=>'95','11852_2'=>'95','11873_2'=>'95','11898_2'=>'95','11936_2'=>'95','11940_2'=>'95','11991_2'=>'95','11998_2'=>'95','12011_2'=>'95','12104_2'=>'95','12105_2'=>'95','12511_2'=>'95','12627_2'=>'95','12709_2'=>'95','12856_2'=>'95','12999_2'=>'95','13412_2'=>'95','13496_2'=>'95','13518_2'=>'95','13564_2'=>'95','13574_2'=>'95','13648_2'=>'95','13659_2'=>'95','13678_2'=>'95','13682_2'=>'95','13691_2'=>'95','13700_2'=>'95','13715_2'=>'95','13728_2'=>'95','13729_2'=>'95','13732_2'=>'95','13794_2'=>'95','13930_2'=>'95','14013_2'=>'95','14640_2'=>'95','14805_2'=>'95','14812_2'=>'95','15027_2'=>'95','15174_2'=>'95','15293_2'=>'95','15344_2'=>'95','15393_2'=>'95','15429_2'=>'95','15437_2'=>'95','15582_2'=>'95','15696_2'=>'95','15794_2'=>'95','16355_2'=>'95','16409_2'=>'95','16495_2'=>'95','16785_2'=>'95','16812_2'=>'95','16931_2'=>'95','16986_2'=>'95','16987_2'=>'95','17045_2'=>'95','17056_2'=>'95','17135_2'=>'95','17203_2'=>'95','17244_2'=>'95','17333_2'=>'95','17408_2'=>'95','17584_2'=>'95','17853_2'=>'95','17956_2'=>'95','18019_2'=>'95','18192_2'=>'95','18358_2'=>'95','18360_2'=>'95','18526_2'=>'95','18537_2'=>'95','18565_2'=>'95','18604_2'=>'95','18663_2'=>'95','18778_2'=>'95','18785_2'=>'95','18903_2'=>'95','19188_2'=>'95','19211_2'=>'95','19531_2'=>'95','19580_2'=>'95','19586_2'=>'95','19822_2'=>'95','20052_2'=>'95','20232_2'=>'95','20444_2'=>'95','20717_2'=>'95','21011_2'=>'95','21145_2'=>'95','21191_2'=>'95','21625_2'=>'95','21630_2'=>'95','21721_2'=>'95','21757_2'=>'95','22311_2'=>'95','22346_2'=>'95','22354_2'=>'95','23142_2'=>'95','23151_2'=>'95','23538_2'=>'95','23842_2'=>'95','24000_2'=>'95','24132_2'=>'95','24182_2'=>'95','24200_2'=>'95','24265_2'=>'95','24267_2'=>'95','24281_2'=>'95','24296_2'=>'95','24297_2'=>'95','24381_2'=>'95','24480_2'=>'95','24701_2'=>'95','24745_2'=>'95','24770_2'=>'95','24934_2'=>'95','24935_2'=>'95','24942_2'=>'95','25126_2'=>'95','25271_2'=>'95','25280_2'=>'95','25382_2'=>'95','25383_2'=>'95','25438_2'=>'95','25457_2'=>'95','25649_2'=>'95','25650_2'=>'95','26081_2'=>'95','26082_2'=>'95','26084_2'=>'95','26095_2'=>'95','26127_2'=>'95','26171_2'=>'95','26182_2'=>'95','26710_2'=>'95','26711_2'=>'95','26724_2'=>'95','26757_2'=>'95','26809_2'=>'95','26872_2'=>'95','26994_2'=>'95','26995_2'=>'95','27058_2'=>'95','27059_2'=>'95','27060_2'=>'95','27121_2'=>'95','27247_2'=>'95','27352_2'=>'95','27587_2'=>'95','27592_2'=>'95','27798_2'=>'95','28062_2'=>'95','28308_2'=>'95','28421_2'=>'95','28469_2'=>'95','28520_2'=>'95','28585_2'=>'95','28587_2'=>'95','28755_2'=>'95','28853_2'=>'95','28861_2'=>'95','28864_2'=>'95','28865_2'=>'95','28938_2'=>'95','28939_2'=>'95','28940_2'=>'95','28949_2'=>'95','28993_2'=>'95','29033_2'=>'95','29195_2'=>'95','29380_2'=>'95','29444_2'=>'95','29446_2'=>'95','29461_2'=>'95','29464_2'=>'95','29644_2'=>'95','29814_2'=>'95','29816_2'=>'95','30496_2'=>'95','31218_2'=>'95','31456_2'=>'95','31657_2'=>'95','31910_2'=>'95','32348_2'=>'95','32349_2'=>'95','32382_2'=>'95','32720_2'=>'95','33026_2'=>'95','33071_2'=>'95','33072_2'=>'95','33393_2'=>'95','33859_2'=>'95','34060_2'=>'95','34068_2'=>'95','34077_2'=>'95','34083_2'=>'95','34084_2'=>'95','34142_2'=>'95','34161_2'=>'95','34483_2'=>'95','34816_2'=>'95','36402_2'=>'95','36424_2'=>'95','36487_2'=>'95','37010_2'=>'95','37066_2'=>'95','37067_2'=>'95','37329_2'=>'95','37486_2'=>'95','37501_2'=>'95','37507_2'=>'95','37623_2'=>'95','37624_2'=>'95','37994_2'=>'95','38049_2'=>'95','38160_2'=>'95','38161_2'=>'95','38286_2'=>'95','38431_2'=>'95','38601_2'=>'95','38811_2'=>'95','42095_2'=>'95','42373_2'=>'95','42504_2'=>'95','42769_2'=>'95','42858_2'=>'95','43781_2'=>'95','44642_2'=>'95','44892_2'=>'95','44946_2'=>'95','45962_2'=>'95','48643_2'=>'95','49984_2'=>'95','50316_2'=>'95','50328_2'=>'95','50670_2'=>'95','50679_2'=>'95','50680_2'=>'95','50682_2'=>'95','50683_2'=>'95','50684_2'=>'95','25_2'=>'95','26_2'=>'95','28_2'=>'95','29_2'=>'95','36_2'=>'95','37_2'=>'95','38_2'=>'95','39_2'=>'95','45_2'=>'95','46_2'=>'95','48_2'=>'95','49_2'=>'95','52_2'=>'95','53_2'=>'95','54_2'=>'95','55_2'=>'95','59_2'=>'95','60_2'=>'95','61_2'=>'95','64_2'=>'95','65_2'=>'95','66_2'=>'95','67_2'=>'95','81_2'=>'95','82_2'=>'95','109_2'=>'95','113_2'=>'95','116_2'=>'95','120_2'=>'95','125_2'=>'95','129_2'=>'95','137_2'=>'95','138_2'=>'95','140_2'=>'95','142_2'=>'95','144_2'=>'95','145_2'=>'95','152_2'=>'95','154_2'=>'95','157_2'=>'95','158_2'=>'95','159_2'=>'95','167_2'=>'95','183_2'=>'95','211_2'=>'95','224_2'=>'95','234_2'=>'95','241_2'=>'95','243_2'=>'95','245_2'=>'95','318_2'=>'95','319_2'=>'95','328_2'=>'95','343_2'=>'95','350_2'=>'95','357_2'=>'95','384_2'=>'95','421_2'=>'95','428_2'=>'95','429_2'=>'95','430_2'=>'95','445_2'=>'95','452_2'=>'95','471_2'=>'95','496_2'=>'95','497_2'=>'95','502_2'=>'95','509_2'=>'95','518_2'=>'95','544_2'=>'95','545_2'=>'95','546_2'=>'95','555_2'=>'95','573_2'=>'95','577_2'=>'95','579_2'=>'95','589_2'=>'95','595_2'=>'95','596_2'=>'95','600_2'=>'95','612_2'=>'95','617_2'=>'95','630_2'=>'95','631_2'=>'95','636_2'=>'95','650_2'=>'95','653_2'=>'95','654_2'=>'95','661_2'=>'95','670_2'=>'95','682_2'=>'95','691_2'=>'95','695_2'=>'95','696_2'=>'95','697_2'=>'95','703_2'=>'95','706_2'=>'95','709_2'=>'95','712_2'=>'95','722_2'=>'95','725_2'=>'95','729_2'=>'95','731_2'=>'95','734_2'=>'95','735_2'=>'95','738_2'=>'95','739_2'=>'95','743_2'=>'95','744_2'=>'95','748_2'=>'95','761_2'=>'95','769_2'=>'95','770_2'=>'95','779_2'=>'95','782_2'=>'95','817_2'=>'95','893_2'=>'95','961_2'=>'95','1056_2'=>'95','1074_2'=>'95','1085_2'=>'95','1100_2'=>'95','1147_2'=>'95','1187_2'=>'95','1630_2'=>'95','2204_2'=>'95','2300_2'=>'95','2360_2'=>'95','2365_2'=>'95','2472_2'=>'95','2575_2'=>'95','2601_2'=>'95','2641_2'=>'95','2661_2'=>'95','2813_2'=>'95','2839_2'=>'95','2851_2'=>'95','2870_2'=>'95','2931_2'=>'95','2944_2'=>'95','2978_2'=>'95','2991_2'=>'95','3059_2'=>'95','3120_2'=>'95','3355_2'=>'95','3476_2'=>'95','3563_2'=>'95','3564_2'=>'95','3650_2'=>'95','3884_2'=>'95','3975_2'=>'95','4053_2'=>'95','4065_2'=>'95','4066_2'=>'95','4134_2'=>'95','4135_2'=>'95','4140_2'=>'95','4259_2'=>'95','4551_2'=>'95','4578_2'=>'95','4709_2'=>'95','4900_2'=>'95','4904_2'=>'95','4912_2'=>'95','4924_2'=>'95','4931_2'=>'95','4933_2'=>'95','5078_2'=>'95','5097_2'=>'95','5099_2'=>'95','5139_2'=>'95','5297_2'=>'95','5720_2'=>'95','5986_2'=>'95','5999_2'=>'95','6474_2'=>'95','6624_2'=>'95','6631_2'=>'95','6660_2'=>'95','6844_2'=>'95','7529_2'=>'95','7643_2'=>'95','8881_2'=>'95','9133_2'=>'95','10271_2'=>'95','10446_2'=>'95','10787_2'=>'95','11545_2'=>'95','11738_2'=>'95','11783_2'=>'95','12099_2'=>'95','12717_2'=>'95','12922_2'=>'95','13738_2'=>'95','14423_2'=>'95','15388_2'=>'95','15460_2'=>'95','15536_2'=>'95','21687_2'=>'95','21874_2'=>'95','23190_2'=>'95','23238_2'=>'95','23469_2'=>'95','23545_2'=>'95','23930_2'=>'95','24856_2'=>'95','24879_2'=>'95','25672_2'=>'95','25910_2'=>'95','25962_2'=>'95','26113_2'=>'95','26167_2'=>'95','26250_2'=>'95','26494_2'=>'95','26899_2'=>'95','28192_2'=>'95','28895_2'=>'95','29022_2'=>'95','29023_2'=>'95','29821_2'=>'95','29990_2'=>'95','31057_2'=>'95','32055_2'=>'95','32056_2'=>'95','32093_2'=>'95','32383_2'=>'95','33018_2'=>'95','33824_2'=>'95','33832_2'=>'95','34497_2'=>'95','34822_2'=>'95','34910_2'=>'95','35395_2'=>'95','35516_2'=>'95','36092_2'=>'95','36293_2'=>'95','36313_2'=>'95','36323_2'=>'95','36330_2'=>'95','36340_2'=>'95','37118_2'=>'95','37685_2'=>'95','38638_2'=>'95','38959_2'=>'95','40982_2'=>'95','40984_2'=>'95','41367_2'=>'95','42024_2'=>'95','42226_2'=>'95','43831_2'=>'95','45331_2'=>'95','45663_2'=>'95','46179_2'=>'95','46408_2'=>'95','46490_2'=>'95','47812_2'=>'95','48419_2'=>'95','48695_2'=>'95','50189_2'=>'95','50515_2'=>'95','51683_2'=>'95','52668_2'=>'95','52701_2'=>'95','53883_2'=>'95','53960_2'=>'95','54421_2'=>'95','55627_2'=>'95','58538_2'=>'95','59507_2'=>'95','59559_2'=>'95','59844_2'=>'95','61114_2'=>'95','63279_2'=>'95','63506_2'=>'95','65027_2'=>'95','66042_2'=>'95','72817_2'=>'95','85138_2'=>'95','87770_2'=>'95','97099_2'=>'95','100268_2'=>'95','105111_2'=>'95','108684_2'=>'95','109878_2'=>'95','110241_2'=>'95','113912_2'=>'95','114894_2'=>'95','115751_2'=>'95','118081_2'=>'95','118301_2'=>'95','122125_2'=>'95','123365_2'=>'95','124326_2'=>'95','36898_2'=>'95','36903_2'=>'95','37093_2'=>'95','37193_2'=>'95','37215_2'=>'95','37253_2'=>'95','37335_2'=>'95','37337_2'=>'95','37353_2'=>'95','37408_2'=>'95','37568_2'=>'95','37579_2'=>'95','37861_2'=>'95','37941_2'=>'95','37946_2'=>'95','37951_2'=>'95','38095_2'=>'95','38113_2'=>'95','38171_2'=>'95','38175_2'=>'95','38533_2'=>'95','38621_2'=>'95','38624_2'=>'95','39152_2'=>'95','39230_2'=>'95','39595_2'=>'95','40140_2'=>'95','40204_2'=>'95','40303_2'=>'95','40386_2'=>'95','40424_2'=>'95','40689_2'=>'95','41718_2'=>'95','41808_2'=>'95','42035_2'=>'95','42214_2'=>'95','43017_2'=>'95','43582_2'=>'95','44210_2'=>'95','47144_2'=>'95','49297_2'=>'95','52602_2'=>'95','56675_2'=>'95','58304_2'=>'95','58883_2'=>'95','58884_2'=>'95','59098_2'=>'95','59152_2'=>'95','61220_2'=>'95','61718_2'=>'95','62459_2'=>'95','62660_2'=>'95','62709_2'=>'95','63671_2'=>'95','63772_2'=>'95','65062_2'=>'95','65063_2'=>'95','65111_2'=>'95','67131_2'=>'95','67142_2'=>'95','67302_2'=>'95','67371_2'=>'95','69495_2'=>'95','78727_2'=>'95','78731_2'=>'95','80685_2'=>'95','80767_2'=>'95','86130_2'=>'95','90052_2'=>'95','91462_2'=>'95','91463_2'=>'95','94968_2'=>'95','108248_2'=>'95','110092_2'=>'95','110320_2'=>'95','114251_2'=>'95','117492_2'=>'95','117842_2'=>'95','118214_2'=>'95','122323_2'=>'95','122535_2'=>'95','123195_2'=>'95','124671_2'=>'95','125553_2'=>'95','677_2'=>'95','1682_2'=>'95','34153_2'=>'95','94504_2'=>'95','94824_2'=>'95','94883_2'=>'95','94917_2'=>'95','96790_2'=>'95','96891_2'=>'95','106615_2'=>'95','107273_2'=>'95','107466_2'=>'95','109496_2'=>'95','112714_2'=>'95','117668_2'=>'95','123005_2'=>'95','123016_2'=>'95','123901_2'=>'95',
                    '467_2'=>'99','478_2'=>'99','529_2'=>'99','535_2'=>'99','4905_2'=>'99','7335_2'=>'99','8272_2'=>'99','8285_2'=>'99','8315_2'=>'99','8320_2'=>'99','8324_2'=>'99','8428_2'=>'99','8461_2'=>'99','8552_2'=>'99','8824_2'=>'99','8854_2'=>'99','9109_2'=>'99','9115_2'=>'99','9117_2'=>'99','9121_2'=>'99','9171_2'=>'99','9420_2'=>'99','10364_2'=>'99','10442_2'=>'99','10505_2'=>'99','10557_2'=>'99','10815_2'=>'99','10821_2'=>'99','11393_2'=>'99','11602_2'=>'99','11845_2'=>'99','11872_2'=>'99','12356_2'=>'99','12412_2'=>'99','12965_2'=>'99','13116_2'=>'99','13385_2'=>'99','14069_2'=>'99','14116_2'=>'99','15662_2'=>'99','17260_2'=>'99','17309_2'=>'99','17361_2'=>'99','17481_2'=>'99','19647_2'=>'99','19727_2'=>'99','20165_2'=>'99','20680_2'=>'99','21749_2'=>'99','22035_2'=>'99','22263_2'=>'99','22268_2'=>'99','22650_2'=>'99','22831_2'=>'99','23179_2'=>'99','23439_2'=>'99','23874_2'=>'99','25691_2'=>'99','25694_2'=>'99','25785_2'=>'99','29116_2'=>'99','29307_2'=>'99','32099_2'=>'99','32821_2'=>'99','32829_2'=>'99','32836_2'=>'99','32842_2'=>'99','33247_2'=>'99','33810_2'=>'99','35427_2'=>'99','36598_2'=>'99','37910_2'=>'99','37938_2'=>'99','41726_2'=>'99','42481_2'=>'99','44507_2'=>'99','47040_2'=>'99','48184_2'=>'99','48945_2'=>'99','53435_2'=>'99','53908_2'=>'99','55067_2'=>'99','55224_2'=>'99','56693_2'=>'99','57604_2'=>'99','60347_2'=>'99','60959_2'=>'99','61723_2'=>'99','62791_2'=>'99','68860_2'=>'99','69308_2'=>'99','70125_2'=>'99','70879_2'=>'99','75657_2'=>'99','75658_2'=>'99','81390_2'=>'99','81550_2'=>'99','86768_2'=>'99','89449_2'=>'99','93546_2'=>'99','104088_2'=>'99','111577_2'=>'99','112777_2'=>'99','112945_2'=>'99','114790_2'=>'99','121013_2'=>'99','121757_2'=>'99','122192_2'=>'99','122748_2'=>'99','125172_2'=>'99',
                    '8540_2'=>'111','9529_2'=>'111','15184_2'=>'111','15194_2'=>'111','15198_2'=>'111','15450_2'=>'111','15695_2'=>'111','15705_2'=>'111','8540_2'=>'111','9529_2'=>'111','15184_2'=>'111','15194_2'=>'111','15198_2'=>'111','15441_2'=>'111','15450_2'=>'111','15695_2'=>'111','15705_2'=>'111','15886_2'=>'111','16135_2'=>'111','16521_2'=>'111','16623_2'=>'111','16689_2'=>'111','22249_2'=>'111','23248_2'=>'111','24691_2'=>'111','27758_2'=>'111','29820_2'=>'111','29911_2'=>'111','32355_2'=>'111','33303_2'=>'111','53289_2'=>'111','63730_2'=>'111','78105_2'=>'111','79842_2'=>'111','107190_2'=>'111','107221_2'=>'111','4735_2'=>'111','5181_2'=>'111','33897_2'=>'111','34101_2'=>'111','42266_2'=>'111','50731_2'=>'111','51939_2'=>'111','85254_2'=>'111','87340_2'=>'111','92814_2'=>'111','94354_2'=>'111','95676_2'=>'111','96376_2'=>'111','96611_2'=>'111','96648_2'=>'111','96711_2'=>'111','96806_2'=>'111','96949_2'=>'111','97067_2'=>'111','97088_2'=>'111','97246_2'=>'111','97265_2'=>'111','98379_2'=>'111','98387_2'=>'111','99155_2'=>'111','99227_2'=>'111','99396_2'=>'111','99442_2'=>'111','99498_2'=>'111','99710_2'=>'111','99763_2'=>'111','99787_2'=>'111','99886_2'=>'111','99887_2'=>'111','100006_2'=>'111','100102_2'=>'111','100362_2'=>'111','100444_2'=>'111','100563_2'=>'111','100615_2'=>'111','103139_2'=>'111','103426_2'=>'111','103570_2'=>'111','104081_2'=>'111','104423_2'=>'111','105524_2'=>'111','105530_2'=>'111','105551_2'=>'111','105563_2'=>'111','106018_2'=>'111','106095_2'=>'111','106545_2'=>'111','106849_2'=>'111','106962_2'=>'111','107419_2'=>'111','107479_2'=>'111','108342_2'=>'111','108712_2'=>'111','111068_2'=>'111','112065_2'=>'111','113144_2'=>'111','119335_2'=>'111','119336_2'=>'111','122362_2'=>'111','123598_2'=>'111','126482_2'=>'111','4452_2'=>'111','4455_2'=>'111','4536_2'=>'111','4712_2'=>'111','4859_2'=>'111','4860_2'=>'111','5379_2'=>'111','5624_2'=>'111','5761_2'=>'111','8386_2'=>'111','11162_2'=>'111','11779_2'=>'111','11780_2'=>'111','12737_2'=>'111','12909_2'=>'111','13074_2'=>'111','14510_2'=>'111','14627_2'=>'111','15360_2'=>'111','15467_2'=>'111','15617_2'=>'111','15930_2'=>'111','15994_2'=>'111','15995_2'=>'111','17888_2'=>'111','17924_2'=>'111','17947_2'=>'111','17948_2'=>'111','18112_2'=>'111','18119_2'=>'111','18168_2'=>'111','18169_2'=>'111','18180_2'=>'111','18221_2'=>'111','18232_2'=>'111','18241_2'=>'111','18250_2'=>'111','18476_2'=>'111','18609_2'=>'111','18980_2'=>'111','19172_2'=>'111','19504_2'=>'111','19524_2'=>'111','19530_2'=>'111','19533_2'=>'111','19572_2'=>'111','19610_2'=>'111','19612_2'=>'111','19778_2'=>'111','19890_2'=>'111','20088_2'=>'111','20089_2'=>'111','20178_2'=>'111','20314_2'=>'111','20605_2'=>'111','20889_2'=>'111','20959_2'=>'111','21499_2'=>'111','21513_2'=>'111','22279_2'=>'111','24910_2'=>'111','27216_2'=>'111','27456_2'=>'111','27854_2'=>'111','27865_2'=>'111','28089_2'=>'111','28153_2'=>'111','30786_2'=>'111','30789_2'=>'111','30790_2'=>'111','30795_2'=>'111','30849_2'=>'111','30972_2'=>'111','31129_2'=>'111','31180_2'=>'111','31225_2'=>'111','32346_2'=>'111','32359_2'=>'111','32366_2'=>'111','32405_2'=>'111','32420_2'=>'111','32450_2'=>'111','32785_2'=>'111','33116_2'=>'111','33223_2'=>'111','33224_2'=>'111','33225_2'=>'111','33817_2'=>'111','33825_2'=>'111','33830_2'=>'111','33839_2'=>'111','33891_2'=>'111','33956_2'=>'111','33963_2'=>'111','33965_2'=>'111','34642_2'=>'111','37102_2'=>'111','37109_2'=>'111','37112_2'=>'111','37651_2'=>'111','37660_2'=>'111','37665_2'=>'111','38153_2'=>'111','42407_2'=>'111','42485_2'=>'111','43496_2'=>'111','44053_2'=>'111','44498_2'=>'111','44987_2'=>'111','45127_2'=>'111','45380_2'=>'111','48843_2'=>'111','48925_2'=>'111','51545_2'=>'111','52103_2'=>'111','52308_2'=>'111','54728_2'=>'111','54734_2'=>'111','56353_2'=>'111','56741_2'=>'111','59419_2'=>'111','60916_2'=>'111','61722_2'=>'111','63102_2'=>'111','66351_2'=>'111','72797_2'=>'111','76527_2'=>'111','79520_2'=>'111','86694_2'=>'111','93128_2'=>'111','93130_2'=>'111','97077_2'=>'111','110009_2'=>'111','112357_2'=>'111','12063_2'=>'111','12107_2'=>'111','15327_2'=>'111','15940_2'=>'111','23554_2'=>'111','29723_2'=>'111','34790_2'=>'111','34792_2'=>'111','34841_2'=>'111','40740_2'=>'111','41084_2'=>'111','52698_2'=>'111','58395_2'=>'111','75849_2'=>'111','76077_2'=>'111','77114_2'=>'111','82822_2'=>'111','90270_2'=>'111','102071_2'=>'111','102217_2'=>'111','102245_2'=>'111','102268_2'=>'111','102276_2'=>'111','102281_2'=>'111','102293_2'=>'111','102314_2'=>'111','102630_2'=>'111','102727_2'=>'111','102739_2'=>'111','102741_2'=>'111','103574_2'=>'111','104841_2'=>'111','104878_2'=>'111','104937_2'=>'111','105189_2'=>'111','106630_2'=>'111','106644_2'=>'111','106658_2'=>'111','106673_2'=>'111','106694_2'=>'111','107518_2'=>'111','113182_2'=>'111','121471_2'=>'111','121584_2'=>'111','123978_2'=>'111','124490_2'=>'111','4495_2'=>'111','89192_2'=>'111','89194_2'=>'111','89206_2'=>'111','89211_2'=>'111','89214_2'=>'111','89217_2'=>'111','89230_2'=>'111','89237_2'=>'111','89240_2'=>'111','89244_2'=>'111','89255_2'=>'111','89257_2'=>'111','89263_2'=>'111','89270_2'=>'111','89276_2'=>'111','89277_2'=>'111','89446_2'=>'111','89453_2'=>'111','89458_2'=>'111','89465_2'=>'111','89468_2'=>'111','89473_2'=>'111','89475_2'=>'111','89476_2'=>'111','89480_2'=>'111','89489_2'=>'111','89499_2'=>'111','89509_2'=>'111','89510_2'=>'111','89513_2'=>'111','89515_2'=>'111','89604_2'=>'111','89649_2'=>'111','89676_2'=>'111','89680_2'=>'111','89698_2'=>'111','89700_2'=>'111','89705_2'=>'111','89713_2'=>'111','89717_2'=>'111','89732_2'=>'111','89737_2'=>'111','89744_2'=>'111','89749_2'=>'111','89756_2'=>'111','89758_2'=>'111','89762_2'=>'111','89766_2'=>'111','89919_2'=>'111','89922_2'=>'111','89935_2'=>'111','89950_2'=>'111','90173_2'=>'111','90203_2'=>'111','90214_2'=>'111','90218_2'=>'111','90403_2'=>'111','90413_2'=>'111','90424_2'=>'111','90433_2'=>'111','90873_2'=>'111','90877_2'=>'111','90904_2'=>'111','90905_2'=>'111','90926_2'=>'111','90928_2'=>'111','90940_2'=>'111','90943_2'=>'111','91115_2'=>'111','91121_2'=>'111','91133_2'=>'111','91376_2'=>'111','91381_2'=>'111','91391_2'=>'111','91401_2'=>'111','91416_2'=>'111','91428_2'=>'111','91430_2'=>'111','91642_2'=>'111','91647_2'=>'111','91651_2'=>'111','91655_2'=>'111','91667_2'=>'111','91848_2'=>'111','91861_2'=>'111','92063_2'=>'111','92083_2'=>'111','92120_2'=>'111','92132_2'=>'111','92688_2'=>'111','92695_2'=>'111','92891_2'=>'111','92947_2'=>'111','93151_2'=>'111','93314_2'=>'111','93579_2'=>'111','93594_2'=>'111','93878_2'=>'111','93896_2'=>'111','94513_2'=>'111','94716_2'=>'111','94739_2'=>'111','95896_2'=>'111','96299_2'=>'111','96339_2'=>'111','97553_2'=>'111','97877_2'=>'111','97891_2'=>'111','98885_2'=>'111','100041_2'=>'111','100337_2'=>'111','100350_2'=>'111','101070_2'=>'111','101087_2'=>'111','101770_2'=>'111','102365_2'=>'111','102378_2'=>'111','102717_2'=>'111','103231_2'=>'111','103887_2'=>'111','105527_2'=>'111','106064_2'=>'111','106667_2'=>'111','107566_2'=>'111','108065_2'=>'111','108791_2'=>'111','111062_2'=>'111','96306_2'=>'111','115621_2'=>'111','115624_2'=>'111','115641_2'=>'111','115646_2'=>'111','115657_2'=>'111','115663_2'=>'111','115668_2'=>'111','115679_2'=>'111','115702_2'=>'111','115711_2'=>'111','115845_2'=>'111','115882_2'=>'111','115894_2'=>'111','115900_2'=>'111','115904_2'=>'111','115913_2'=>'111','115919_2'=>'111','115936_2'=>'111','115943_2'=>'111','115949_2'=>'111','115978_2'=>'111','115990_2'=>'111','115994_2'=>'111','116103_2'=>'111','116160_2'=>'111','116209_2'=>'111','116364_2'=>'111','116450_2'=>'111','116776_2'=>'111','116823_2'=>'111','117549_2'=>'111','118295_2'=>'111','118586_2'=>'111','118911_2'=>'111','119589_2'=>'111','119642_2'=>'111','119884_2'=>'111','121416_2'=>'111','123034_2'=>'111','123047_2'=>'111','124270_2'=>'111','125177_2'=>'111','3127_2'=>'111','4289_2'=>'111','4316_2'=>'111','4323_2'=>'111','4383_2'=>'111','4419_2'=>'111','4481_2'=>'111','7100_2'=>'111','9090_2'=>'111','10234_2'=>'111','11228_2'=>'111','11784_2'=>'111','11792_2'=>'111','11839_2'=>'111','11868_2'=>'111','11885_2'=>'111','11897_2'=>'111','11904_2'=>'111','11945_2'=>'111','11989_2'=>'111','11996_2'=>'111','11997_2'=>'111','12052_2'=>'111','12125_2'=>'111','12173_2'=>'111','12213_2'=>'111','12220_2'=>'111','12334_2'=>'111','12341_2'=>'111','12351_2'=>'111','12383_2'=>'111','12384_2'=>'111','12424_2'=>'111','12430_2'=>'111','12448_2'=>'111','12497_2'=>'111','12567_2'=>'111','12578_2'=>'111','12882_2'=>'111','12883_2'=>'111','12893_2'=>'111','12973_2'=>'111','12975_2'=>'111','12977_2'=>'111','13002_2'=>'111','13005_2'=>'111','13094_2'=>'111','13098_2'=>'111','13100_2'=>'111','13102_2'=>'111','13189_2'=>'111','13223_2'=>'111','13374_2'=>'111','13388_2'=>'111','13507_2'=>'111','13749_2'=>'111','13906_2'=>'111','13961_2'=>'111','14225_2'=>'111','14426_2'=>'111','14435_2'=>'111','14466_2'=>'111','14553_2'=>'111','14623_2'=>'111','14692_2'=>'111','14703_2'=>'111','14864_2'=>'111','14887_2'=>'111','14942_2'=>'111','14965_2'=>'111','15050_2'=>'111','15127_2'=>'111','15211_2'=>'111','15232_2'=>'111','15249_2'=>'111','15271_2'=>'111','15328_2'=>'111','15854_2'=>'111','15905_2'=>'111','15929_2'=>'111','16096_2'=>'111','16149_2'=>'111','16244_2'=>'111','16275_2'=>'111','16349_2'=>'111','16493_2'=>'111','16497_2'=>'111','16503_2'=>'111','16593_2'=>'111','16719_2'=>'111','16779_2'=>'111','16838_2'=>'111','16865_2'=>'111','16872_2'=>'111','16896_2'=>'111','16904_2'=>'111','17159_2'=>'111','17177_2'=>'111','17596_2'=>'111','17613_2'=>'111','17746_2'=>'111','17786_2'=>'111','17889_2'=>'111','17944_2'=>'111','18039_2'=>'111','18129_2'=>'111','18211_2'=>'111','18214_2'=>'111','18224_2'=>'111','18227_2'=>'111','18255_2'=>'111','18267_2'=>'111','18274_2'=>'111','18397_2'=>'111','18404_2'=>'111','18491_2'=>'111','18493_2'=>'111','18531_2'=>'111','18600_2'=>'111','18621_2'=>'111','18684_2'=>'111','18879_2'=>'111','18880_2'=>'111','19003_2'=>'111','19018_2'=>'111','19054_2'=>'111','19454_2'=>'111','19698_2'=>'111','19762_2'=>'111','19776_2'=>'111','19933_2'=>'111','20064_2'=>'111','20170_2'=>'111','20309_2'=>'111','20406_2'=>'111','20645_2'=>'111','20871_2'=>'111','21295_2'=>'111','21949_2'=>'111','22599_2'=>'111','22731_2'=>'111','26126_2'=>'111','29312_2'=>'111','31677_2'=>'111','31680_2'=>'111','31713_2'=>'111','31743_2'=>'111','31745_2'=>'111','31766_2'=>'111','32361_2'=>'111','32488_2'=>'111','34169_2'=>'111','34172_2'=>'111','34341_2'=>'111','42751_2'=>'111','42752_2'=>'111','43865_2'=>'111','43908_2'=>'111','44128_2'=>'111','44165_2'=>'111','44166_2'=>'111','44167_2'=>'111','46829_2'=>'111','47806_2'=>'111','47807_2'=>'111','47808_2'=>'111','47809_2'=>'111','47810_2'=>'111','48385_2'=>'111','52033_2'=>'111','56598_2'=>'111','57003_2'=>'111','64014_2'=>'111','72251_2'=>'111','72518_2'=>'111','72924_2'=>'111','73932_2'=>'111','91519_2'=>'111','94172_2'=>'111','104082_2'=>'111','115715_2'=>'111','17568_2'=>'111','18091_2'=>'111','18167_2'=>'111','29122_2'=>'111','53527_2'=>'111','71499_2'=>'111','73865_2'=>'111','73870_2'=>'111','73874_2'=>'111','73889_2'=>'111','73893_2'=>'111','73895_2'=>'111','73897_2'=>'111','73907_2'=>'111','74185_2'=>'111','74214_2'=>'111','74645_2'=>'111','74663_2'=>'111','74671_2'=>'111','74677_2'=>'111','74688_2'=>'111','74708_2'=>'111','75070_2'=>'111','75142_2'=>'111','76787_2'=>'111','77112_2'=>'111','77980_2'=>'111','78044_2'=>'111','78837_2'=>'111','78841_2'=>'111','78846_2'=>'111','78847_2'=>'111','78848_2'=>'111','78849_2'=>'111','78957_2'=>'111','78958_2'=>'111','78959_2'=>'111','78963_2'=>'111','79263_2'=>'111','79275_2'=>'111','79317_2'=>'111','79865_2'=>'111','80820_2'=>'111','81158_2'=>'111','82136_2'=>'111','82377_2'=>'111','82428_2'=>'111','84365_2'=>'111','84367_2'=>'111','85467_2'=>'111','85862_2'=>'111','90275_2'=>'111','90476_2'=>'111','90963_2'=>'111','91451_2'=>'111','92133_2'=>'111','92481_2'=>'111','92967_2'=>'111','93856_2'=>'111','94505_2'=>'111','95417_2'=>'111','95853_2'=>'111','96606_2'=>'111','99216_2'=>'111','101413_2'=>'111','105506_2'=>'111','105986_2'=>'111','105987_2'=>'111','109185_2'=>'111','111079_2'=>'111','112258_2'=>'111','112260_2'=>'111','125465_2'=>'111','2495_2'=>'111','2507_2'=>'111','13086_2'=>'111','55932_2'=>'111','55935_2'=>'111','56076_2'=>'111','56141_2'=>'111','56143_2'=>'111','56161_2'=>'111','56174_2'=>'111','56182_2'=>'111','56187_2'=>'111','56192_2'=>'111','56202_2'=>'111','56341_2'=>'111','56343_2'=>'111','57352_2'=>'111','57635_2'=>'111','59221_2'=>'111','62092_2'=>'111','63656_2'=>'111','64703_2'=>'111','65085_2'=>'111','65288_2'=>'111','66758_2'=>'111','68155_2'=>'111','70212_2'=>'111','72650_2'=>'111','73034_2'=>'111','73969_2'=>'111','78992_2'=>'111','80323_2'=>'111','83541_2'=>'111','85416_2'=>'111','89461_2'=>'111','94057_2'=>'111','95254_2'=>'111','105132_2'=>'111','112728_2'=>'111','112862_2'=>'111','114880_2'=>'111','115043_2'=>'111','125536_2'=>'111',
                    '30_2'=>'126','33_2'=>'126','34_2'=>'126','42_2'=>'126','63_2'=>'126','93_2'=>'126','94_2'=>'126','101_2'=>'126','104_2'=>'126','110_2'=>'126','117_2'=>'126','118_2'=>'126','119_2'=>'126','131_2'=>'126','133_2'=>'126','134_2'=>'126','161_2'=>'126','200_2'=>'126','236_2'=>'126','255_2'=>'126','266_2'=>'126','267_2'=>'126','273_2'=>'126','278_2'=>'126','288_2'=>'126','314_2'=>'126','360_2'=>'126','376_2'=>'126','610_2'=>'126','692_2'=>'126','759_2'=>'126','926_2'=>'126','1494_2'=>'126','1753_2'=>'126','2169_2'=>'126','2205_2'=>'126','2206_2'=>'126','2829_2'=>'126','2908_2'=>'126','3458_2'=>'126','3831_2'=>'126','4236_2'=>'126','4614_2'=>'126','5010_2'=>'126','5060_2'=>'126','6437_2'=>'126','6478_2'=>'126','7447_2'=>'126','7491_2'=>'126','7543_2'=>'126','7589_2'=>'126','7722_2'=>'126','7787_2'=>'126','7973_2'=>'126','8223_2'=>'126','8224_2'=>'126','8449_2'=>'126','8893_2'=>'126','9022_2'=>'126','9299_2'=>'126','9691_2'=>'126','9710_2'=>'126','11642_2'=>'126','13046_2'=>'126','13707_2'=>'126','14276_2'=>'126','14859_2'=>'126','15686_2'=>'126','16988_2'=>'126','17434_2'=>'126','17465_2'=>'126','17649_2'=>'126','17904_2'=>'126','18492_2'=>'126','19537_2'=>'126','19653_2'=>'126','19932_2'=>'126','19967_2'=>'126','19970_2'=>'126','20961_2'=>'126','21308_2'=>'126','22170_2'=>'126','23612_2'=>'126','23981_2'=>'126','26476_2'=>'126','26633_2'=>'126','26656_2'=>'126','27139_2'=>'126','28278_2'=>'126','28523_2'=>'126','29894_2'=>'126','30219_2'=>'126','32342_2'=>'126','32695_2'=>'126','33674_2'=>'126','34451_2'=>'126','38584_2'=>'126','38585_2'=>'126','38897_2'=>'126','38899_2'=>'126','40968_2'=>'126','41750_2'=>'126','43830_2'=>'126','44455_2'=>'126','44495_2'=>'126','45319_2'=>'126','45321_2'=>'126','45505_2'=>'126','47442_2'=>'126','47875_2'=>'126','51062_2'=>'126','51063_2'=>'126','52619_2'=>'126','53644_2'=>'126','53797_2'=>'126','53842_2'=>'126','54178_2'=>'126','54682_2'=>'126','54735_2'=>'126','55056_2'=>'126','55229_2'=>'126','55664_2'=>'126','55870_2'=>'126','56645_2'=>'126','60133_2'=>'126','62926_2'=>'126','63057_2'=>'126','64037_2'=>'126','75176_2'=>'126','75214_2'=>'126','75549_2'=>'126','84406_2'=>'126','89593_2'=>'126','93476_2'=>'126','94453_2'=>'126','96317_2'=>'126','96958_2'=>'126','102273_2'=>'126','111390_2'=>'126','112112_2'=>'126','115850_2'=>'126','119835_2'=>'126','121636_2'=>'126','122514_2'=>'126','123063_2'=>'126','128512_2'=>'126','47_2'=>'126','69_2'=>'126','85_2'=>'126','105_2'=>'126','111_2'=>'126','114_2'=>'126','181_2'=>'126','197_2'=>'126','289_2'=>'126','773_2'=>'126','2848_2'=>'126','3372_2'=>'126','3403_2'=>'126','3675_2'=>'126','3700_2'=>'126','3713_2'=>'126','3800_2'=>'126','3907_2'=>'126','4012_2'=>'126','4295_2'=>'126','4404_2'=>'126','5234_2'=>'126','5235_2'=>'126','5575_2'=>'126','7016_2'=>'126','7281_2'=>'126','7557_2'=>'126','7919_2'=>'126','9060_2'=>'126','9186_2'=>'126','9577_2'=>'126','12623_2'=>'126','12624_2'=>'126','12641_2'=>'126','12655_2'=>'126','12659_2'=>'126','12667_2'=>'126','12671_2'=>'126','12674_2'=>'126','12699_2'=>'126','12704_2'=>'126','12712_2'=>'126','12718_2'=>'126','12739_2'=>'126','12749_2'=>'126','12767_2'=>'126','12804_2'=>'126','12808_2'=>'126','12809_2'=>'126','12833_2'=>'126','12936_2'=>'126','12943_2'=>'126','12951_2'=>'126','12968_2'=>'126','12993_2'=>'126','13103_2'=>'126','13140_2'=>'126','13153_2'=>'126','13154_2'=>'126','13172_2'=>'126','13436_2'=>'126','13503_2'=>'126','13582_2'=>'126','13862_2'=>'126','13959_2'=>'126','14209_2'=>'126','14437_2'=>'126','14449_2'=>'126','14450_2'=>'126','14854_2'=>'126','14915_2'=>'126','15003_2'=>'126','15101_2'=>'126','15235_2'=>'126','15296_2'=>'126','15358_2'=>'126','15579_2'=>'126','15664_2'=>'126','15893_2'=>'126','16810_2'=>'126','17104_2'=>'126','17105_2'=>'126','17145_2'=>'126','17210_2'=>'126','17563_2'=>'126','17783_2'=>'126','17923_2'=>'126','18065_2'=>'126','18818_2'=>'126','19329_2'=>'126','19443_2'=>'126','19451_2'=>'126','19506_2'=>'126','19799_2'=>'126','19984_2'=>'126','20007_2'=>'126','20078_2'=>'126','20665_2'=>'126','21128_2'=>'126','21129_2'=>'126','21410_2'=>'126','21446_2'=>'126','21672_2'=>'126','21840_2'=>'126','21845_2'=>'126','22564_2'=>'126','22707_2'=>'126','23195_2'=>'126','23197_2'=>'126','23537_2'=>'126','24322_2'=>'126','25550_2'=>'126','25763_2'=>'126','25885_2'=>'126','26359_2'=>'126','26911_2'=>'126','27759_2'=>'126','27775_2'=>'126','27794_2'=>'126','27839_2'=>'126','28414_2'=>'126','28518_2'=>'126','28679_2'=>'126','28710_2'=>'126','28854_2'=>'126','29142_2'=>'126','29157_2'=>'126','29773_2'=>'126','29951_2'=>'126','30346_2'=>'126','30443_2'=>'126','30447_2'=>'126','31166_2'=>'126','31172_2'=>'126','31175_2'=>'126','31179_2'=>'126','31189_2'=>'126','31201_2'=>'126','31202_2'=>'126','31203_2'=>'126','31204_2'=>'126','31209_2'=>'126','31210_2'=>'126','31452_2'=>'126','32404_2'=>'126','32740_2'=>'126','32741_2'=>'126','32755_2'=>'126','32773_2'=>'126','33017_2'=>'126','33319_2'=>'126','33727_2'=>'126','34633_2'=>'126','34839_2'=>'126','36301_2'=>'126','36750_2'=>'126','36813_2'=>'126','37403_2'=>'126','37519_2'=>'126','37581_2'=>'126','37839_2'=>'126','37844_2'=>'126','38367_2'=>'126','39114_2'=>'126','39558_2'=>'126','40199_2'=>'126','40200_2'=>'126','40602_2'=>'126','40765_2'=>'126','41437_2'=>'126','42329_2'=>'126','43848_2'=>'126','43906_2'=>'126','43914_2'=>'126','48626_2'=>'126','48628_2'=>'126','49290_2'=>'126','49517_2'=>'126','53419_2'=>'126','53938_2'=>'126','54776_2'=>'126','66725_2'=>'126','68895_2'=>'126','69915_2'=>'126','69936_2'=>'126','70206_2'=>'126','73714_2'=>'126','74027_2'=>'126','77079_2'=>'126','77686_2'=>'126','77870_2'=>'126','80290_2'=>'126','85827_2'=>'126','85935_2'=>'126','87354_2'=>'126','94274_2'=>'126','95494_2'=>'126','107593_2'=>'126','116887_2'=>'126','124130_2'=>'126','130848_2'=>'126','5638_2'=>'126','21787_2'=>'126','27884_2'=>'126','33311_2'=>'126','51193_2'=>'126','51195_2'=>'126','51197_2'=>'126','51200_2'=>'126','51201_2'=>'126','51203_2'=>'126','51204_2'=>'126','51210_2'=>'126','51212_2'=>'126','51214_2'=>'126','51330_2'=>'126','51338_2'=>'126','51339_2'=>'126','51345_2'=>'126','51354_2'=>'126','51363_2'=>'126','51366_2'=>'126','51376_2'=>'126','51387_2'=>'126','51400_2'=>'126','51402_2'=>'126','51404_2'=>'126','51411_2'=>'126','51546_2'=>'126','51629_2'=>'126','51651_2'=>'126','51658_2'=>'126','51659_2'=>'126','51743_2'=>'126','52218_2'=>'126','52357_2'=>'126','52776_2'=>'126','53239_2'=>'126','53480_2'=>'126','53727_2'=>'126','54013_2'=>'126','55186_2'=>'126','58154_2'=>'126','60112_2'=>'126','60597_2'=>'126','65416_2'=>'126','68292_2'=>'126','71032_2'=>'126','71623_2'=>'126','73055_2'=>'126','76656_2'=>'126','78283_2'=>'126','79197_2'=>'126','79565_2'=>'126','80228_2'=>'126','81311_2'=>'126','82365_2'=>'126','82836_2'=>'126','82928_2'=>'126','85843_2'=>'126','86025_2'=>'126','86387_2'=>'126','90241_2'=>'126','90500_2'=>'126','93547_2'=>'126','93935_2'=>'126','93971_2'=>'126','95216_2'=>'126','95536_2'=>'126','96364_2'=>'126','96818_2'=>'126','97441_2'=>'126','99263_2'=>'126','100653_2'=>'126','101672_2'=>'126','105962_2'=>'126','117364_2'=>'126','121465_2'=>'126','126170_2'=>'126','128257_2'=>'126','130052_2'=>'126','130084_2'=>'126',
                    '67132_2'=>'84','56731_2'=>'84','62278_2'=>'84','63088_2'=>'84','63111_2'=>'84','62554_2'=>'84','62120_2'=>'84','56627_2'=>'84','63509_2'=>'84','56767_2'=>'84','64714_2'=>'84','62276_2'=>'84','55594_2'=>'84','58682_2'=>'84','41155_2'=>'84','67335_2'=>'84','55657_2'=>'84','55974_2'=>'84','56808_2'=>'84','55660_2'=>'84','55584_2'=>'84','55571_2'=>'84','55576_2'=>'84','55562_2'=>'84','70021_2'=>'84','56108_2'=>'84','73338_2'=>'84','73371_2'=>'84','80033_2'=>'84','81243_2'=>'84','89770_2'=>'84','95842_2'=>'84','95991_2'=>'84','103243_2'=>'84','106557_2'=>'84','115841_2'=>'84','121808_2'=>'84','123574_2'=>'84','126948_2'=>'84','55109_2'=>'84','40052_2'=>'84','41299_2'=>'84','31363_2'=>'84','13886_2'=>'84','5595_2'=>'84','3046_2'=>'84','25339_2'=>'84','1049_2'=>'84','31161_2'=>'84','76198_2'=>'84','54209_2'=>'84','2817_2'=>'84','41255_2'=>'84','45295_2'=>'84','40210_2'=>'84','31329_2'=>'84','71221_2'=>'84','2382_2'=>'84','42445_2'=>'84','19148_2'=>'84','33238_2'=>'84','7505_2'=>'84','1126_2'=>'84','72950_2'=>'84','79416_2'=>'84','84473_2'=>'84','86419_2'=>'84','86939_2'=>'84','94710_2'=>'84','99682_2'=>'84','109739_2'=>'84','112045_2'=>'84','112870_2'=>'84','114474_2'=>'84','114503_2'=>'84','114975_2'=>'84','121946_2'=>'84','121948_2'=>'84','122848_2'=>'84','129364_2'=>'84','135148_2'=>'84','15276_2'=>'84','37993_2'=>'84','40572_2'=>'84','21853_2'=>'84','20322_2'=>'84','40601_2'=>'84','23921_2'=>'84','21249_2'=>'84','42172_2'=>'84','27483_2'=>'84','21638_2'=>'84','32092_2'=>'84','5273_2'=>'84','21571_2'=>'84','37037_2'=>'84','32893_2'=>'84','6496_2'=>'84','20350_2'=>'84','69257_2'=>'84','21399_2'=>'84','20561_2'=>'84','36327_2'=>'84','20474_2'=>'84','60045_2'=>'84','34001_2'=>'84','69737_2'=>'84','11089_2'=>'84','69054_2'=>'84','28373_2'=>'84','27438_2'=>'84','57726_2'=>'84','49054_2'=>'84','46043_2'=>'84','43050_2'=>'84','15554_2'=>'84','20012_2'=>'84','27072_2'=>'84','25579_2'=>'84','15631_2'=>'84','36144_2'=>'84','52282_2'=>'84','15486_2'=>'84','29408_2'=>'84','23467_2'=>'84','32462_2'=>'84','21758_2'=>'84','57537_2'=>'84','20903_2'=>'84','30370_2'=>'84','18220_2'=>'84','37740_2'=>'84','69264_2'=>'84','19543_2'=>'84','55878_2'=>'84','15616_2'=>'84','21239_2'=>'84','32184_2'=>'84','16036_2'=>'84','21236_2'=>'84','17921_2'=>'84','35469_2'=>'84','32434_2'=>'84','21875_2'=>'84','25855_2'=>'84','22181_2'=>'84','15192_2'=>'84','21938_2'=>'84','15546_2'=>'84','55154_2'=>'84','36994_2'=>'84','15574_2'=>'84','39419_2'=>'84','15749_2'=>'84','15565_2'=>'84','21559_2'=>'84','24892_2'=>'84','52486_2'=>'84','55683_2'=>'84','55572_2'=>'84','37351_2'=>'84','50203_2'=>'84','20745_2'=>'84','25505_2'=>'84','39132_2'=>'84','27603_2'=>'84','37088_2'=>'84','20856_2'=>'84','22706_2'=>'84','15186_2'=>'84','32427_2'=>'84','29012_2'=>'84','37026_2'=>'84','36133_2'=>'84','51037_2'=>'84','35175_2'=>'84','41641_2'=>'84','69029_2'=>'84','37117_2'=>'84','15542_2'=>'84','30913_2'=>'84','51268_2'=>'84','28687_2'=>'84','71260_2'=>'84','72709_2'=>'84','72734_2'=>'84','73444_2'=>'84','75633_2'=>'84','76804_2'=>'84','76912_2'=>'84','79724_2'=>'84','80555_2'=>'84','81316_2'=>'84','82406_2'=>'84','92386_2'=>'84','93647_2'=>'84','97481_2'=>'84','103721_2'=>'84','103991_2'=>'84','114149_2'=>'84','115515_2'=>'84','136298_2'=>'84'
                );
                /*$dist_vendor_mapping = array(
                    '624_2'=>'75','398_2'=>'75','453_2'=>'75','212_2'=>'75','860_2'=>'75','816_2'=>'75','777_2'=>'75','512_2'=>'75','241_2'=>'75',
                    '286_2'=>'75','855_2'=>'75','505_2'=>'75',
                    '461_2'=>'78','785_2'=>'78','844_2'=>'78','312_2'=>'78',
                    '744_2'=>'77','470_2'=>'77','584_2'=>'77','555_2'=>'77','700_2'=>'77','522_2'=>'77'                    
                    );
                 * 
                 */
		$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param));
						
		
		$inactive = $this->getInactiveVendors();
		$vends = array();
		$prim_vend = null;
		$primary = null;
		
		if($api_flag){
			$invoiceObj = ClassRegistry::init('Slaves');
			$prodData = $invoiceObj->query("SELECT primary_vendor FROM partner_operator_status WHERE partner_acc_no = 'P000002' AND operator_id = $prodId");
			
			if(!empty($prodData) && !empty($prodData['0']['partner_operator_status']['primary_vendor']) && !in_array($prodData['0']['partner_operator_status']['primary_vendor'],$inactive)){
				$prim_vend = $prodData['0']['partner_operator_status']['primary_vendor'];
			}
		}
		
		$exceptional_vendors = array_merge(array('29'),array_values(array_unique($dist_vendor_mapping)),array_values(array_unique($ret_vendor_mapping)));//b2c modem & local set ups
		
		if(!($api_flag) && isset($additional_param['retailer_created'])){
			$diff = strtotime(date('Y-m-d')) - strtotime($additional_param['retailer_created']);
			
			$prim_vend_newRetailers = array('8'=>'29','9'=>'29','15'=>'29','16'=>'29','17'=>'29','18'=>'29');
			if($diff/(60*60*24) <= 30){
				$prim_vend = (isset($prim_vend_newRetailers[$prodId]) && !empty($prim_vend_newRetailers[$prodId])) ? $prim_vend_newRetailers[$prodId] : 29;//b2c vendor
			}
		}
					
		$exception = null;
		//print_r($inactive);
		//
		$primary_vendors = array();
				
		foreach($info['vendors'] as $vend){
			$circle_yes = $vend['circles_yes'];
			$circle_no = $vend['circles_no'];

			$imp_yes = implode(",",$circle_yes);
			$imp_no = implode(",",$circle_no);
			
			//exception wrt airtel infogem
			/*if($vend['vendor_id'] == 27 && $prodId == 2 && !in_array($vend['vendor_id'],$inactive)){
				$exception = $vend;
			}*/
//                        if($vend['vendor_id'] == 68 && $prodId == 2 && $additional_param['amount'] <= 50){
//                            continue;
//                        }
			if(!empty($imp_yes) && !in_array($prod_d['area'],$circle_yes)){
				continue;
			}
			if(!empty($imp_no) && in_array($prod_d['area'],$circle_no)){
				continue;
			}
			if(!in_array($vend['vendor_id'],$inactive)){
				if($api_flag || !in_array($vend['vendor_id'],$exceptional_vendors)){//removing exceptional/priority modems from normal b2b transactions
					$vends[] = $vend;
				}
				
				if(!empty($prim_vend) && $prim_vend == $vend['vendor_id']){//primary wrt api partner/ new retailers
					$primary = $vend;
					$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::api vendor case ::primary: ".$vend['vendor_id']);
				}
				else if(empty($prim_vend)){
					$val = isset($additional_param['dist_id'])?$additional_param['dist_id']:0;
					$retailer_id_val = isset($additional_param['retailer_id'])?$additional_param['retailer_id']:0;
						
					
					if(!empty($retailer_id_val) && isset($ret_vendor_mapping[$retailer_id_val."_".$prodId]) && $ret_vendor_mapping[$retailer_id_val."_".$prodId] == $vend['vendor_id'] && !in_array($vend['vendor_id'],$non_primary)){
						$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::retid_area case ::primary: ".$vend['vendor_id']);
						$primary_vendors['local_vendor'] = $vend;
					}
					
					if(!empty($val) && isset($dist_vendor_mapping[$val."_".$prodId]) && $dist_vendor_mapping[$val."_".$prodId] == $vend['vendor_id'] && !in_array($vend['vendor_id'],$non_primary)){
						$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::distid_area case ::primary: ".$vend['vendor_id']);
						$primary_vendors['local_vendor'] = $vend;
					}
					if(!isset($primary_vendors['circle']) && !empty($vend['circle']) && $prod_d['area'] == $vend['circle'] && !in_array($vend['vendor_id'],$non_primary)){//circle wise primary vendor logic
						$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::circle_area primary case ::primary: ".$vend['vendor_id']);
						$primary_vendors['circle'] = $vend;
					}if(!empty($additional_param) && isset($additional_param['amount'])){
                                           if($this->check_amount_priority(intval($prodId),intval($vend['vendor_id']),intval($additional_param['amount']))){
                                                $this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::cp airtel lower amount case ::primary: ".$vend['vendor_id']);
                                                $primary_vendors['cp_airtel'] = $vend;
                                           } 
                                           if(in_array($additional_param['amount'],array(10,20,30)) && $prodId == 2 && strtoupper($prod_d['area']) == 'MU' && $vend['vendor_id'] == '8'){
                                               $primary_vendors['cp_airtel_denom'] = $vend;
                                        }
				}
			}
		}
		}

		if(empty($primary)){
                        if(isset($primary_vendors['cp_airtel_denom']))$primary = $primary_vendors['cp_airtel_denom'];
			else if(isset($primary_vendors['local_vendor']))$primary = $primary_vendors['local_vendor'];
			else if(isset($primary_vendors['cp_airtel']))$primary = $primary_vendors['cp_airtel'];
			else if(isset($primary_vendors['circle']))$primary = $primary_vendors['circle'];
		}
		
		$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::final primary: ".$primary['vendor_id']);
						
		if(!empty($primary)){
			array_unshift($vends,$primary);
		}
		
		if(!empty($exception)){
			$vends[] = $exception;	
		}
		
		$info['vendors'] = $vends;

		if(empty($primary) && $primary_flag){
			$primary = $this->findOptimalVendor($vends,$prodId,$info['automate']);
		}
                else if($primary_flag){
                    $this->findOptimalVendor(array($primary),$prodId,$info['automate']);
                }
		
		if(empty($info['vendors'])){
			return array('status'=>'failure','code'=>'','description'=>'','name'=>$info['name'],'info'=>$info);
		}
		else {
			return array('status'=>'success','code'=>'','info'=>$info,'primary'=>$primary);
		}
	}
	
        /**
         * Generate and arrange the vendors mapping in specified structured based on input
         * @return type array
         */
        function generate_local_vendor_map(){
            $mapArr = $this->getLocalVendorsMap();
            $returnArr = array();
            foreach($mapArr as $vendorId=>$opr_dist_Arr){
              foreach($opr_dist_Arr as $oprId=>$dist_Arr){
                foreach($dist_Arr as $dist_Id){
                  $returnArr[$dist_Id."_".$oprId] = $vendorId;	
                }	
              }
            }
            return $returnArr;
        }
        
        /**
         * Get local vendors mapping from memcache
         * @return type array
         */
        function getLocalVendorsMap(){
            $vendors = $this->getMemcache('local_Vendors_map');
            if($vendors === false){
                    $vendors = $this->setLocalVendorsMap();
            }
            return $vendors;
	}
        
        /**
         * SET local vendors mapping in memcache from query result
         * @return type array
         */
        function setLocalVendorsMap(){
            //$slaveObj = ClassRegistry::init('Slaves');
            $slaveObj = ClassRegistry::init('User');
            $vendors = $slaveObj->query("SELECT * FROM local_vendor_mapping where is_deleted = 1 ");
            $vendordata = array();
            foreach($vendors as $localvendor_key=>$localvendor_val){
                $vendor_id = isset($localvendor_val['local_vendor_mapping']['vendor_id']) ? $localvendor_val['local_vendor_mapping']['vendor_id'] : "";
                $operator_id = isset($localvendor_val['local_vendor_mapping']['operator_id']) ? $localvendor_val['local_vendor_mapping']['operator_id'] : "";
                $distributor_id = isset($localvendor_val['local_vendor_mapping']['distributor_id']) ? $localvendor_val['local_vendor_mapping']['distributor_id'] : "";
                $vendordata[$vendor_id][$operator_id] = explode(",",$distributor_id);
            }
            $this->setMemcache('local_Vendors_map',$vendordata);		
            return $vendordata;
	}
        
        /**
         * 
         * Check if there is any amount based priority exist for particular operator on specific vendor
         * @param type $productId
         * @param type $vendorId
         * @param type $Amount
         * @return type boolean
         */
        function check_amount_priority($productId,$vendorId,$Amount){            
            
            $amt_priority = $this->getAmountPriorityMap();
            
            foreach( $amt_priority as $pro_arr ){
                
                if($pro_arr['product_id'] == $productId && $pro_arr['vendor_id'] == $vendorId ){
                    
                    if(!empty($pro_arr['list_amount'])){
                        
                        return in_array($Amount,explode(",",$pro_arr['list_amount']));
                        
                    }elseif(!empty ($pro_arr['min_amount']) && !empty($pro_arr['min_amount']) && $pro_arr['min_amount'] <= $Amount && $pro_arr['max_amount'] >= $Amount){
                        
                        return TRUE;
                        
                    }else{
                        
                        return FALSE;
                        
                    }                    
                }
            }
            return FALSE;
        }
        
        /**
         * Get amount priority map from memcache 
         * @return type array
         */
        function getAmountPriorityMap(){
            
            $amtPriorityMap = $this->getMemcache('amount_priority_map');
            if($amtPriorityMap === false){
                    $amtPriorityMap = $this->setAmountPriorityMap();
            }		
            return $amtPriorityMap;
        }
        
        /**
         * set amount priority map to memcache from query result
         * @return type array
         */
        function setAmountPriorityMap(){
            //$slaveObj = ClassRegistry::init('Slaves');
            $slaveObj = ClassRegistry::init('User');
            $amtPriorityMap = $slaveObj->query("SELECT * FROM amount_priority_mapping where is_deleted = 1 ");
            $amtPriorityMapping = array();
            foreach ($amtPriorityMap as $amtPriority_key=>$amtPriority_val){
                $amtPriorityMapping[] = $amtPriority_val['amount_priority_mapping'];
            }
            $this->setMemcache('amount_priority_map',$amtPriorityMapping,24*60*60);		
            return $amtPriorityMapping;
        }        
        
        
	function checkPossibility($prodId,$mobileNo,$amount,$power,$param=null,$special=null,$api_flag=null){
		$sms = null;
		if(!empty($power)) return $sms;
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($amount == ''){
			$prodData = $invoiceObj->query("SELECT price FROM products_info WHERE product_id = $prodId");
            if(empty($prodData) && count($prodData)>0){
			$amount = $prodData['0']['products_info']['price'];
            } else {
                $amount  = '';
            }
		}
		if(empty($param))$number = $mobileNo;
		else $number = $param;
		
		$mins = TIME_DURATION;
		
		if(empty($prodId) || empty($number) || empty($amount)) return 'Invalid recharge format';
		
		if(!$this->lockTransactionDuplicates($prodId,$number,$amount,$api_flag)){
			if($param == null){
				$msg = "*$prodId*$mobileNo*$amount";
				$sms = "hold: Repeat on $mobileNo of Rs$amount within ".($mins/60)." hours";
			}
			else {
				$msg = "*$prodId*$param*$mobileNo*$amount";
				$sms = "hold: Repeat on $param of Rs$amount within ".($mins/60)." hours";
			}
			
			if(!empty($special) && $special == 1) $msg = $msg . "#";
			
			$sender = $_SESSION['Auth']['mobile'];
			$this->addRepeatTransaction($msg,$sender,2);
			$mail_body = "Request is on hold, Retailer $sender has done duplicate transaction within $mins mins";
			$mail_body .= "<br/>Customer Mobile: $mobileNo/$param, Amount: $amount";
			$this->General->sendMails("Pay1: Same Transaction within $mins mins",$mail_body,array('notifications@mindsarray.com'));
			
			$added = $invoiceObj->query("SELECT id FROM repeated_transactions WHERE msg = '$msg' AND sender='$sender' AND type=2 ORDER BY id desc LIMIT 1");
			if(!empty($added)){
				$id = $added['0']['repeated_transactions']['id'];
				$id = "1" . sprintf('%04d', $id);
				$sms .= "\nTo continue send: #$id via misscall service or SMS to 09004-350-350";
				$sms .= "\nIgnore if don't want to continue";
			}
		}
		
		return $sms;
	}
	
	function addRepeatTransaction($msg,$sender,$type,$added_by=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($added_by == null){
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,type,timestamp) VALUES ('$sender','".addslashes($msg)."',$type,'".date('Y-m-d H:i:s')."')");	
		}
		else {
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,send_flag,type,added_by,processed_by,timestamp) VALUES ('$sender','".addslashes($msg)."',1,$type,$added_by,$added_by,'".date('Y-m-d H:i:s')."')");
		}
	}
	
	/*function getReceiptNumber($id){
		$recObj = ClassRegistry::init('Receipt');
		$data = $recObj->findById($id);
		if($data['Receipt']['receipt_type'] == RECEIPT_INVOICE){
			$suffix1 = "INV";
		}
		else if($data['Receipt']['receipt_type'] == RECEIPT_TOPUP){
			$suffix1 = "TP";
		}
		
		if($data['Receipt']['group_id'] == SUPER_DISTRIBUTOR){
			$suffix2 = "ST";
		}
		else if($data['Receipt']['group_id'] == DISTRIBUTOR){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['Receipt']['group_id'] == RETAILER){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		$ret = $recObj->query("SELECT count(*) as counts FROM receipts WHERE shop_from_id = ".$data['Receipt']['shop_from_id']." and group_id = " . $data['Receipt']['group_id'] . " and id < $id");
		if(isset($parent_comp)){
			$suffix2 = "";
			foreach($parent_comp as $str){
				$suffix2 .= substr($str,0,1);
			}
			$suffix2 = strtoupper($suffix2);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "R/". $suffix1 . "/". $suffix2 . "/" . sprintf('%03d', $number);
	}*/
	
	function getShopData($user_id,$group_id=null){
		if($group_id == null){
			$userData = $this->General->getUserDataFromId($user_id);
			$group_id = $userData['group_id'];
		}
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Slaves');
			$retailers = $userObj->query("select * 
					from retailers as r
					left join unverified_retailers ur on ur.retailer_id = r.id
					where r.user_id = ".$user_id);
			foreach($retailers[0]['ur'] as $key => $row){
        		if(!in_array($key, array('id')))
        			$retailers[0]['r'][$key] = $retailers[0]['ur'][$key];
        	}
        	
			return $retailers[0]['r'];
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
            $bal = $userObj->find('first',array('conditions' => array('Rm.user_id' => $user_id)));
            return $bal['Rm'];
		}
	}
	
	
	function payment_gateway($amount,$via='web'){
		$userObj = ClassRegistry::init('Retailer');
		$data = $userObj->query("SELECT * FROM pg_checks WHERE distributor_id = '".$_SESSION['Auth']['parent_id']."'");
		
		$topup = $amount - $data['0']['pg_checks']['service_charge'];
		
		if(empty($data) || $data['0']['pg_checks']['active_flag'] == 0){
			return array('status' => 'failure','code'=>'43','description'=>$this->errors(43));
		}
		else if($topup < $data['0']['pg_checks']['min_amount']){
			return array('status' => 'failure','code'=>'33','description'=>'Minimum amount allowed for topup is Rs. '.$data['0']['pg_checks']['min_amount']);
		}
		else if($topup > $data['0']['pg_checks']['max_amount']){
			return array('status' => 'failure','code'=>'34','description'=>'Maximum amount allowed for topup is Rs. '.$data['0']['pg_checks']['max_amount']);
		}
		
		$retailer = $userObj->query("SELECT rental_flag from retailers where id = '".$_SESSION['Auth']['id']."'");
		$rental_flag = $retailer['0']['retailers']['rental_flag']; 
		if($rental_flag == 2){
			$amount += $this->General->findVar("OTA_Fee");
		}	
		//make entry in pg_india table
			
		$recId = $this->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$topup,1,$_SESSION['Auth']['id'],8,null,5);
		
		$this->setMemcache("pg_$recId",$via,30*60);
		
		$sm = $userObj->query("SELECT salesmen.id,salesmen.name from salesmen inner join distributors ON (distributors.id = salesmen.dist_id) inner join users ON (users.id = distributors.user_id) where distributors.id = '".$_SESSION['Auth']['parent_id']."' AND users.mobile = salesmen.mobile");
		$userObj->query("INSERT INTO salesman_transactions (shop_tran_id,salesman,payment_mode,payment_type,details,collection_date,created) VALUES ('".$recId."','".$sm['0']['salesmen']['id']."','".MODE_PG."','".TYPE_TOPUP."','','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')");

		$userObj->query("INSERT INTO pg_payuIndia (status,shop_transaction_id,amount,addedon,server_ip) VALUES ('pending','$recId','$amount','".date('Y-m-d H:i:s')."','".$_SERVER['REMOTE_ADDR']."')");
			
		//get shop_transid & pass it to pg_payment function .. this will return payment form.. Which will be shown to user
		
		$shopname = (empty($_SESSION['Auth']['shopname']))? $_SESSION['Auth']['mobile'] : $_SESSION['Auth']['shopname'];
		$data = array('transaction_id'=>$recId,'amount'=>$amount,'category'=>'TOPUP_RETAILER','name'=>$shopname,'email'=>$_SESSION['Auth']['email']);
		
		$pg_form_data = $this->generatePayment_form_retailer($data);
		return array('status' => 'success','code' =>'0','description'=>$pg_form_data);
	}
	
	function generatePayment_form_retailer($data){
		$data2process = $this->payu_setdata2process($data);
        $fdata = array('data2process'=>$data2process,'salt'=>PAYU_SALT);
        $hash = $this->generate_payu_hash($fdata);
        
        $data2process['phone'] = $_SESSION['Auth']['mobile'];
        $data2process['surl'] = PAYU_SUCCESS_URL;
        $data2process['furl']= PAYU_FAILURE_URL;
        $data2process['curl']=PAYU_FAILURE_URL;
        $data2process['touturl']=PAYU_FAILURE_URL;
        $data2process['hash']=$hash;
        $data2process['user_credentials']=PAYU_KEY.":".$_SESSION['Auth']['user_id'];
        
        $pay_page_content = $this->generate_pg_payu_form_content($data2process);
        return $pay_page_content;
	}
	
	function payu_setdata2process($data){
		$data2process['key'] = PAYU_KEY;
        //transaction related detail
        $data2process['txnid'] = $data['transaction_id'];
        $data2process['amount'] = $data['amount'];
        $data2process['productinfo'] = $data['category'];
        //customer detail
        
        $data2process['firstname'] = $data['name'];
        $data2process['email'] = (empty($data['email']))?"":$data['email'];
        return $data2process;
	}
	
 	function generate_payu_hash($data){
        $str = implode("|",array_values($data['data2process']))."|||||||||||".$data['salt'];
        //echo $str;exit();
        return hash("sha512",$str);
    }
	
	function generate_pg_payu_form_content($data){
		$mem_data = $this->getMemcache("pg_" . $data['txnid']);
		if($mem_data == 'web'){
	       	$postdata = http_build_query($data);
			$options = array('http' =>
					array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/x-www-form-urlencoded',
							'content' => $postdata
					)
			);
	
			$context  = stream_context_create($options);
	
			$result = file_get_contents(PANEL_PAYU_SEAMLESS_URL, false, $context);
			
			return $result;
		}
		else {
			$content = "<html>";
	        $content .= '<script> function submitform(){document.getElementById("pg_payu_form").submit();}</script>';
	        $content .= "<body onload=submitform()>";
	        $content .= "<form id='pg_payu_form' action='".PAYU_URL."' id='pg_payu_form' method='post'>";
	        foreach($data as $key=>$value){
	            $content .= "<input type='hidden' name='$key' value='$value'>";
	        }
	        $content .= "</form>";
	        $content .="</body></html>";
	        return $content;
		}
    }
    
    function update_pg_payu($data,$online=true){
    	if($online && !$this->check_payu_hash($data)){
    		$t_data = $data;
    		$t_data['ip']= $_SERVER['REMOTE_ADDR'];
    		$this->General->sendMails("b2b: wrong hash for this transaction please check it", json_encode($t_data),array('ashish@mindsarray.com'),'mail');
    		return array('status'=>'failure','code'=>'','description'=>"Hash not matched");
    	}
    	
	    $data['cardnum'] = isset($data['card_no'])?$data['card_no']:(isset($data['cardnum'])?$data['cardnum']:"");
	    $data['error'] = isset($data['error_code'])?$data['error_code']:(isset($data['error'])?$data['error']:"");
	   	$data['shop_transaction_id'] = $data['txnid'];
	   	
	   	unset($data['card_no']);
	    unset($data['error_code']);
	    unset($data['txnid']);
	    
	    $userObj = ClassRegistry::init('Retailer');
    	$colqry = "SELECT COLUMN_NAME  FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='shops' AND `TABLE_NAME`='pg_payuIndia'";
       	$col_result = $userObj->query($colqry);
       	$col_data = array();
       	
        foreach ($col_result as $k=>$v){
        	$col_data[$v['COLUMNS']['COLUMN_NAME']] = isset($data[$v['COLUMNS']['COLUMN_NAME']])?addslashes($data[$v['COLUMNS']['COLUMN_NAME']]):"";
        }

        $payuQry = "select pg_payuIndia.status,shop_transactions.* from pg_payuIndia left join shop_transactions ON (shop_transactions.id = pg_payuIndia.shop_transaction_id) WHERE shop_transaction_id='".$data['shop_transaction_id']."' AND status = 'pending'";
        $payu_result = $userObj->query($payuQry);
        
        if(empty($payu_result)){
        	$this->General->sendMails("B2B:: Payu data already exists", json_encode($data),array('ashish@mindsarray.com'),'mail');
        }
        else{
        	unset($col_data['id']);
        	unset($col_data['server_ip']);
        	$this->update_payu_Transaction($col_data['shop_transaction_id'],$col_data);
        	
	        if($col_data['status']==="success"){
	            if($payu_result['0']['shop_transactions']['type'] == DIST_RETL_BALANCE_TRANSFER){
	                $result = $this->process_retailer_transfer_after_pg($data);
	            }
	            return $result;
	        }
	        else if($col_data['status']==="failure"){
	        	if($payu_result['0']['shop_transactions']['type'] == DIST_RETL_BALANCE_TRANSFER){
	        		$userObj->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE id = ". $col_data['shop_transaction_id']);
	        	}
	        	return array('status'=>'failure','code'=>'','description'=>json_encode($col_data));
	        }
        }
            
    }
    
    function process_retailer_transfer_after_pg($data){
    	try{
    		$userObj = ClassRegistry::init('Retailer');
    		$txnid = $data['shop_transaction_id'];
    		$amount = $data['amount'];
    		
    		$txndata = $userObj->query("SELECT shop_transactions.amount,shop_transactions.ref1_id,shop_transactions.ref2_id,retailers.shopname,retailers.mobile FROM shop_transactions left join retailers ON (retailers.id = shop_transactions.ref2_id) WHERE shop_transactions.id = $txnid AND shop_transactions.confirm_flag != 1");
    		
    		if(!empty($txndata)){
    			$userObj->query("UPDATE shop_transactions SET note = '".$data['mihpayid']."' WHERE id = $txnid");
    			
    			$txn_amount = $txndata['0']['shop_transactions']['amount'];
    			$dist_id = $txndata['0']['shop_transactions']['ref1_id'];
    			$ret_id = $txndata['0']['shop_transactions']['ref2_id'];
    			$ret_mobile = $txndata['0']['retailers']['mobile'];
    			
    			$bal = $this->shopBalanceUpdate($txn_amount,'subtract',$dist_id,DISTRIBUTOR);
				$bal1 = $this->shopBalanceUpdate($txn_amount,'add',$ret_id,RETAILER);
				
				$this->addOpeningClosing($ret_id,RETAILER,$txnid,$bal1-$txn_amount,$bal1);
				$this->addOpeningClosing($dist_id,DISTRIBUTOR,$txnid,$bal+$txn_amount,$bal);
    			
    			if(!empty($txndata['0']['retailers']['shopname'])){
					$shop_name = substr($txndata['0']['retailers']['shopname'],0,15);
				}
				else $shop_name = $txndata['0']['retailers']['mobile'];

				/*$distributors = $userObj->query("select u.mobile
						from users u
						left join distributors d on d.user_id = u.id
						where d.id = ".$dist_id);
				if($distributors){
	                $message_distributor = "Your account is debited with Rs. ".$txn_amount." to retailer: ".$shop_name;
	                $this->General->sendMessage($distributors['0']['u']['mobile'], $message_distributor, "notify", null, DISTRIBUTOR);
				}*/
				
				$mail_subject = "Amount transferred to retailer via Payment Gateway";
				$mail_body = "Transferred Rs. $txn_amount to Retailer: " . $shop_name;
				
				$mail_body .= "<br/>Payment done by Retailer: ".$amount;
				$mail_body .= "<br/>PayuID: ".$data['mihpayid'];
				
				$msg = "Dear $shop_name,\nYour account is successfully credited with Rs." . $txn_amount. " Via Credit Card/Debit Card/Net Banking. Your reference id is $txnid\nYour current balance is Rs.$bal1";    		
    			$this->General->sendMessage($ret_mobile,$msg,'shops');
    			$this->General->sendMails($mail_subject, $mail_body , array("tadka@mindsarray.com","limits@mindsarray.com"),'mail');
    			return array('status'=>'success','code'=>'','description'=>$msg);
    		}
    	}
    	catch (Exception $e){
    		return array('status'=>'failure','code'=>$e->getCode(),'description'=>$e->getMessage());
    	}
    }
	
    function update_payu_Transaction($trans_id,$data=array()){
        $tablename = "pg_payuIndia";
        $userObj = ClassRegistry::init('Retailer');
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
            	if(empty($val))continue;
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $userObj->query("UPDATE $tablename SET $update_data WHERE shop_transaction_id='$trans_id'");
        }
    }
   
    function check_payu_hash($data){
        $key = PAYU_KEY;
        $salt = PAYU_SALT;
        $status = $this->General->set_defaultBlank($data,'status');
        $email = $this->General->set_defaultBlank($data,'email');
        $firstname = $this->General->set_defaultBlank($data,'firstname');
        $productinfo = $this->General->set_defaultBlank($data,'productinfo');
        $amount = $this->General->set_defaultBlank($data,'amount');
        $txnid = $this->General->set_defaultBlank($data,'txnid');            
        $dstr = $salt."|".$status."|||||||||||".$email."|".$firstname."|".$productinfo."|".$amount."|".$txnid."|".$key;        
        //$dstr = $salt."|".$data['status']."|||||||||||".$data['email']."|".$data['firstname']."|".$data['productinfo']."|".$data['amount']."|".$data['txnid']."|".$key;        
        $internal_hash = hash("sha512",$dstr);
        if($internal_hash == $data['hash']){
            return true;
        }
        return false;
    }
	
	function getLastTransactions($date,$page=1,$service = null,$date2=null,$itemsPerPage=0,$retailerId=null,$operatorId=0,$is_page_wise=1){
		if(empty($date)){
			$date = date('Y-m-d');
		}
		else $date = date('Y-m-d',strtotime($date));

		if(empty($date2)){
			$date2 = $date;
		}
		else $date2 = date('Y-m-d',strtotime($date2));

		if($date2 > date('Y-m-d',strtotime($date . ' + 30 days'))){
			$date2 = date('Y-m-d',strtotime($date . ' + 30 days'));
		}
		$next_day = date('Y-m-d',strtotime($date . ' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));

		if($itemsPerPage == 0){
			$itemsPerPage = PAGE_LIMIT;
		}

		$limit = ($page-1)*$itemsPerPage > 0 ? ($page-1)*$itemsPerPage : 0;

		$retailObj = ClassRegistry::init('Slaves');
		if(is_null($retailerId)){
			$retailer = $_SESSION['Auth']['id'];
		}else{
			$retailer = $retailerId;
		}
		if(empty($operatorId)){
			$operatorIdQry = "";
		}else{
			$operatorIdQry = "AND vendors_activations.product_id = ".$operatorId;
		}
		$qryLimitPart = "";
		if($service == null){
            if($is_page_wise != 0){
               $qryLimitPart =  " LIMIT $limit," . $itemsPerPage;
            }
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id , vendors_activations.cause ,vendors_activations.code, sum(complaints.resolve_flag) as resolve_flag
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					inner join shop_transactions on shop_transactions.id = vendors_activations.shop_transaction_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id where vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry 
					group by vendors_activations.id 
					order by vendors_activations.id desc ".$qryLimitPart);
			$ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , shop_transactions where shop_transactions.id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry ");
			$ret_count = $ret_cnt_res[0][0]['cnt'];
		}else {
			$ret = $retailObj->query("SELECT opening_closing.opening,opening_closing.closing,products.name,services.name,vendors_activations.ref_code,
vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,
					vendors_activations.status,vendors_activations.cause,vendors_activations.code, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					inner join opening_closing on opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where products.service_id = $service AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  
					group by vendors_activations.id 
					order by vendors_activations.id desc,opening_closing.id desc " . $qryLimitPart);
			$ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , opening_closing where opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  ");
			$ret_count = $ret_cnt_res[0][0]['cnt'];
		}
		$more = 0;
		if(count($ret) == $itemsPerPage){
			$more = 1;
		}
               
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day,'more' => $more,'total_count' => $ret_count);
	}
	
	function lastten($service){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,
				vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag
				FROM products
				inner join services on products.service_id = services.id
				inner join vendors_activations on products.id = vendors_activations.product_id
				left join complaints on complaints.vendor_activation_id = vendors_activations.id where products.service_id = $service AND vendors_activations.retailer_id =  $retailer AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."'
				group by vendors_activations.id
				order by vendors_activations.id desc LIMIT 0,10");
		return array('ret' => $ret);
	}
	
	function lastFiveTransactions(){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$transactions = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,
				vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, 
				trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,
				sum(complaints.resolve_flag) as resolve_flag
				FROM products
				inner join services on products.service_id = services.id
				inner join vendors_activations on products.id = vendors_activations.product_id
				left join complaints on complaints.vendor_activation_id = vendors_activations.id 
				where vendors_activations.retailer_id = $retailer 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."'
				group by vendors_activations.id
				order by vendors_activations.id desc LIMIT 0,5");
		return $transactions;
	}
	
	function mobileTransactions($mobile,$service=null){
		if(!in_array($service,array('2','6')))$mobile = substr($mobile,-10);
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = isset($_SESSION['Auth']['id']) ? $_SESSION['Auth']['id'] : "";
		$limit = 0;
		if(empty($service)){
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where vendors_activations.retailer_id = $retailer AND vendors_activations.mobile = '$mobile' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
					group by vendors_activations.id
					order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		else {
			$q = 'vendors_activations.mobile';
			if($service == '2' || $service == '6')
			$q = 'vendors_activations.param';
			
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations use INDEX (idx_ret_date) on products.id = vendors_activations.product_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where products.service_id = $service AND vendors_activations.retailer_id = $retailer AND ".$q." like '%$mobile%' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
					group by vendors_activations.id
					order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		return array('ret' => $ret);
	}
	
	function lastThreeTopups($mobile=null){
		if(empty($mobile)) return null;
		$Retailer = ClassRegistry::init('Slaves');
		
		$transactions = $Retailer->query("select * 
						from shop_transactions st
						left join retailers r on r.id = st.ref2_id
						where st.ref1_id = r.parent_id
						and st.ref2_id = r.id
						and st.type = 2
						and r.mobile = '$mobile'
						order by st.timestamp desc
						limit 3");
		
		return $transactions;
	}
	
	function searchTransactionsHistory($mob_subid, $retailer_id){
		$retailObj = ClassRegistry::init('Slaves');
		$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,
					vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, 
					trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,
					sum(complaints.resolve_flag) as resolve_flag 
				FROM products 
				inner join services on products.service_id = services.id 
				inner join vendors_activations on products.id = vendors_activations.product_id 
				left join complaints on complaints.vendor_activation_id = vendors_activations.id  
				where vendors_activations.retailer_id = $retailer_id 
				AND (vendors_activations.mobile = '$mob_subid' OR vendors_activations.param = '$mob_subid') 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
				order by vendors_activations.id desc");
	
		return $ret;
	}
	
	function searchTransactions($mob_subid,$retailer_id){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.id,vendors_activations.ref_code,vendors_activations.param,
				vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, vendors_activations.timestamp,
				vendors_activations.product_id 
				FROM products,services,vendors_activations 
				where products.service_id = services.id 
				AND products.id = vendors_activations.product_id 
				AND vendors_activations.retailer_id = $retailer_id 
				AND (vendors_activations.mobile = '$mob_subid' OR vendors_activations.param = '$mob_subid') 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
				order by vendors_activations.id desc 
				LIMIT 2");
		
		return $ret;
	}
	
	function complaintStats($retailer_id, $date){
		if(!$date){
			$date = date('Y-m-d');
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));
		$retailObj = ClassRegistry::init('Slaves');
		$complaints = $retailObj->query("SELECT count(complaints.resolve_flag) as count, complaints.resolve_flag
							from complaints  
                         	inner join vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         	inner join retailers  on(vendors_activations.retailer_id=retailers.id) 
                            where complaints.in_date >= '$prev_day'
                            AND vendors_activations.retailer_id = $retailer_id 
                            group by complaints.resolve_flag");	
		return $complaints;
	}
	
	function reversalTransactions($date,$service = null){
        $page = 1;
		if(empty($date)){
			$date = date('Y-m-d');
		}
		
		$next_day = date('Y-m-d',strtotime($date.' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));
		
		//return array('ret' => array(),'today' => $date,'prev' => $prev_day,'next' => $next_day);
		
		$limit = ($page-1)*PAGE_LIMIT;
		
		
		$retailer = $_SESSION['Auth']['id'];
		//$type_sql = " AND (vendors_activations.status = ".TRANS_REVERSE." OR vendors_activations.status = ".TRANS_REVERSE_PENDING." OR vendors_activations.status = ".TRANS_REVERSE_DECLINE.")";
		
		if($service == null || empty($service)){
                                /*"SELECT 
                                    products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id 
                                FROM 
                                    products,services,vendors_activations
                                where 
                                    products.service_id = services.id 
                                AND products.id = vendors_activations.product_id 
                                AND vendors_activations.retailer_id =  $retailer 
                                AND vendors_activations.date='$date' $type_sql 
                                order by 
                                    vendors_activations.id desc
                                "*/
                    $panelQ = "
			SELECT 
                                products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
                                vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
                                	
                         from 
                                        complaints  
                         inner  join    vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         inner  join    retailers  on(vendors_activations.retailer_id=retailers.id) 
                         inner  join    products   on(products.id=vendors_activations.product_id)                                 
                         inner join    services     on(products.service_id=services.id)              
                        
				where   
                                
                                complaints.in_date ='$date'
                                
                                AND vendors_activations.retailer_id =  $retailer 
                                group by vendors_activations.id
                                order by complaints.id desc";
                    
                        //AND complaints.resolve_flag = 0  
		}
		else {
			//$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date='$date' $type_sql order by vendors_activations.id desc");
                        $panelQ = "
			SELECT 
                                products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag
                                	
                         from 
                                        complaints  
                         inner  join    vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         inner  join    retailers  on(vendors_activations.retailer_id=retailers.id) 
                         inner  join    products   on(products.id=vendors_activations.product_id)                                 
                         inner  join    services     on(products.service_id=services.id)                  
                        
				where   
                                
                                complaints.in_date ='$date'
                                AND services.id = $service
                                AND vendors_activations.retailer_id =  $retailer 
                                group by vendors_activations.id
                                order by complaints.id desc";
                    
                }
                $retailObj = ClassRegistry::init('Slaves');
                $ret = $retailObj->query($panelQ);
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day);
	}
	
	function updateSlab($slab_id,$shop_id,$group_id){
		$slabObj = ClassRegistry::init('SlabsUser');
		$slabObj->create();
		$slabData['SlabsUser']['slab_id'] = $slab_id;
		$slabData['SlabsUser']['shop_id'] = $shop_id;
		$slabData['SlabsUser']['group_id'] = $group_id;
		$slabData['SlabsUser']['timestamp'] = date('Y-m-d H:i:s');
		$slabObj->save($slabData);
	}
	
	function getBanks(){
		$data = $this->getMemcache('banks');
		
		if($data === false){
			$retailObj = ClassRegistry::init('Retailer');
			$data = $retailObj->query("Select `bank_name` from bank_details where status = '1'");
			$this->setMemcache('banks',$data,2*24*60*60);
		}
        
		return $data;
	}
	
	function addNewVendorRetailer($vendor_id,$retailer_code){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("INSERT INTO vendors_retailers (vendor_id,retailer_code) VALUES ($vendor_id,'$retailer_code')");
	}
	
	function apiAccessHashCheck($params,$partnerModel){
		$pass = $partnerModel['Partner']['password'];
		$hashGen = sha1($params['partner_id'].$params['trans_id'].$pass);//.$params['info_json']
		if(!empty ($params['hash_code']) && strtolower($params['hash_code']) == strtolower($hashGen)){
			return true;
		}else{
			return false;
		}
	}
    function appRechargeAccessHashCheck($uuid, $mobDthNo , $amt ,$timestamp,$hash){
        $hashGen = sha1($uuid.$mobDthNo.$amt.$timestamp);//.$params['info_json']
		if(!empty ($hash) && strtolower($hash) == strtolower($hashGen)){
			return true;
		}else{
			return false;
		}
	}

	function apiAccessIPCheck($partner){
		$result = array();
		$ipAddrsStr = $partner['Partner']['ip_addrs'];// get partener's valid ip list
		$ipArr = explode(",", $ipAddrsStr);
		$ip = $_SERVER['REMOTE_ADDR'];// get client ip
		if(in_array($ip, $ipArr)){//check client ip is in partner's ips list
			$result = array('access'=>true);
		}else{
			$result = array('access'=>false);
		}
		return $result;
	}
	function apiAccessPartnerOperatorCheck($partnerAccno,$operatorId){//check operator is open for a partner (only for recharge api)
		$result = array();
		$retailObj = ClassRegistry::init('Slaves');
		$result = $retailObj->query("select * from  partner_operator_status where partner_acc_no = '".$partnerAccno."' and operator_id = '$operatorId'");
                
		if(empty($result) || $result[0]['partner_operator_status']['status'] == 0){//check client ip is in partner's ips list
			$result = array('access'=>false);
		}else{
			$result = array('access'=>true);
		}
		return $result;
	}
	
	function checkToken($tokenkey = null) {
		$redis = $this->redis_connect();
		$value = false;
		if ($tokenkey != null) {
			$value = $redis->hexists("token", $tokenkey);
		}
                $redis->quit();
		return $value;
	}
	
	function setToken($tokenkey = null) {
		$redis = $this->redis_connect();
		if ($tokenkey != null) {
			$redis->hset("token", $tokenkey, 1);
		}
                $redis->quit();
	}

	function delToken($tokenvalue) {
		$redis = $this->redis_connect();
		$deltoken = $redis->hdel("token", $tokenvalue);
                $redis->quit();
		return $deltoken;
	}

	/*function partnerSessionInit( $type , $model ){ //$id ,
		// type -> user , partner , retailer , distributor , rm
		// $model -> Model Object of  ( user , partner , retailer , distributor , rm )
		$data = array();
		if($type == "user"){
			$data = $this->User->query("SELECT id FROM users WHERE mobile = '".$params['mobile']."' AND password='$password'");
		}else if($type == "partner"){
			$partnerModel = $model;
			$retailerRegObj = ClassRegistry::init('Retailer');
			$retailerModel = $retailerRegObj->findById($partnerModel['Partner']['retailer_id']);
			if(!empty($retailerModel)){
				$data['user_id'] = $retailerModel['Retailer']['user_id'];
				$data['group_id'] = RETAILER;
			}
		}

		if(empty($data)){// Invalid Partner ( can't find retailer account for partner ).
			return false;
		}else{
			$info = $this->getShopData($data['user_id'],$data['group_id']);
			$info['User']['group_id'] = $data['group_id'];
			$info['User']['id'] = $data['user_id'];
			//$this->SessionComponent->write('Auth',$info);
			$_SESSION['Auth'] = $info;
			if(!empty($_SESSION['Auth']['id'])){
				return true;
			}else{
				return false;
			}
		}
	}*/
    
    /**
     * 
     * @param type $query : string params
     * @param type $vendor_id : modem ID
     */
    function async_request_via_redis($query,$vendor_id){
        $redis = $this->openservice_redis();
        $queuename="updateIncoming_{$vendor_id}";
        $queuevalue = $query;
        $redis->lpush($queuename,$query);
    }
    
    function deleteDocument($src){
    	$Retailer = ClassRegistry::init('Retailer');
    	$objectKey = explode("/", $src);
    	App::import('vendor', 'S3', array('file' => 'S3.php'));
    	$bucket = 'pay1bucket';
    	$s3 = new S3(awsAccessKey, awsSecretKey);
    	if($s3->deleteObject($bucket, $objectKey[3])){
    		if($Retailer->query("DELETE FROM  retailers_details where image_name = '".$src."'")){
    			return "Image deleted";
    		}
    		else 
    			return $src." not deleted from db";
    	}
    	else {
    		return $src." not deleted from aws";
    	}
    }
    
    function deleteImageFromAWS($src){
    	$objectKey = explode("/", $src);
    	App::import('vendor', 'S3', array('file' => 'S3.php'));
    	$bucket = 'pay1bucket';
    	$s3 = new S3(awsAccessKey, awsSecretKey);
    	if($s3->deleteObject($bucket, $objectKey[3]))
    		return true;
    	else 
    		return false;
    }
    
    function removeDocument($retailer_id, $section_id, $verify_flag){
    	$map = $this->kycSectionMap($section_id);
    	$Retailer = ClassRegistry::init('Retailer');
    	if($verify_flag == 1){
    		$verified_documents = $Retailer->query("select * from retailers_docs
    				where retailer_id = ".$retailer_id."
    				and type = '".$map['documents'][0]."'");
    		if(($section_id == 3 && count($verified_documents) > 3) || in_array($section_id, array(1, 2))){
    			$documents = $Retailer->query("select * from retailers_details
    					where image_name = '".$verified_documents[0]['retailers_docs']['src']."'");
    			$Retailer->query("delete from retailers_docs
    					where id = ".$verified_documents[0]['retailers_docs']['id']);
    			if(count($documents) == 0){
    				$this->deleteImageFromAWS($verified_documents[0]['retailers_docs']['src']);
    			}
    		}
    	}
    	else {
    		$documents = $Retailer->query("select * from retailers_details
    				where retailer_id = ".$retailer_id."
    				and type = '".$map['documents'][0]."'");
    		if(($section_id == 3 && count($documents) > 3) || in_array($section_id, array(1, 2))){
    			$verified_documents = $Retailer->query("select * from retailers_docs
    					where src = '".$documents[0]['retailers_details']['image_name']."'");
    			$Retailer->query("delete from retailers_details
    					where id = ".$documents[0]['retailers_details']['id']);
    			if(count($verified_documents) == 0){
    				$this->deleteImageFromAWS($documents[0]['retailers_details']['src']);
    			}
    		}
    	}
    }
    
    /**
     * KYC States
     * 0 => Submitted
     * 1 => Rejected
     * 2 => Approved
     * 3 => Unverified
     */
    
    function setKYCState($retailer_id, $section_id, $state, $reason = null){
    	$Retailer = ClassRegistry::init('Retailer');
    	switch($state){
    		case 0:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(empty($retailers_kyc_states)){
    				$map = $this->kycSectionMap($section_id);
    				$types = implode(",", $map['documents']);
    				$documents = $Retailer->query("select * 
    						from retailers_details rd
    						where rd.type in ('".$types."')
    						and rd.retailer_id = ".$retailer_id);
    				if(!empty($documents)){
    					$Retailer->query("insert into retailers_kyc_states
									(retailer_id, section_id, verified_state, document_state, document_timestamp, document_date)
									values(".$retailer_id.", ".$section_id.", '0', '0', '".date('Y-m-d H:i:s')."', '".date('Y-m-d')."')");
    				}
    			}
    			else {
    				$Retailer->query("update retailers_kyc_states
									set document_state = '0',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								comment = ''
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    			}
    			break;
    		case 1:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(!empty($retailers_kyc_states)){
    				$Retailer->query("update retailers_kyc_states
									set document_state = '1',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								comment = '$reason'
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    			}
    			break;
    		case 2:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(!empty($retailers_kyc_states)){
    				$Retailer->query("update retailers_kyc_states
									set document_state = '2',
    								verified_state = '1',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								verified_timestamp = '".date('Y-m-d H:i:s')."',
									verified_date = '".date('Y-m-d')."',
    								comment = ''
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    				$map = $this->kycSectionMap($section_id);
    				$unverified_retailers = $Retailer->query("select * 
    						from unverified_retailers ur
    						where ur.retailer_id = ".$retailer_id);
    				
    				$update_query = "update retailers set ";
    				foreach($map['fields'] as $field){
    					if(!in_array($field, array('latitude', 'longitude')))
    						$update_query .= " $field = '".$unverified_retailers[0]['ur'][$field]."', "; 
    				}
    				$update_query .= " modified = '".date('Y-m-d H:i:s')."'
    						where id = ".$retailer_id;
    				$Retailer->query($update_query);
    				
    				if(in_array('latitude', $map['fields'])){
    					$user_profile = $Retailer->query("select r.*, up.*
    							from user_profile up
    							join retailers r on r.user_id = up.user_id
    							where r.id = ".$retailer_id." 
    							and up.device_type = 'online'
    							order by updated desc
    							limit 1");
    					if(!empty($user_profile)){
    						$Retailer->query("update user_profile
								set latitude = '".$unverified_retailers[0]['ur']['latitude']."',
								longitude = '".$unverified_retailers[0]['ur']['longitude']."',
								updated = '".date("Y-m-d H:i:s")."'
								where user_id = ".$user_profile[0]['up']['user_id']."
								and device_type = 'online'");
    					}
    					else {
    						$retailers = $Retailer->query("select * 
    								from retailers r
    								where r.id = ".$retailer_id);
    						$Retailer->query("insert into user_profile
								(user_id, gcm_reg_id, uuid, longitude, latitude, location_src, device_type,
								version , manufacturer, created, updated)
								VALUES (".$retailers[0]['r']['user_id'].", '".$retailers[0]['r']['mobile']."',
								'".$retailers[0]['r']['mobile']."', '".$unverified_retailers[0]['ur']['longitude']."',
								'".$unverified_retailers[0]['ur']['latitude']."', '' ,'online' ,'' ,'' ,
								'".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
    					}
    				}
    				
    				$types = implode(",", $map['documents']);
    				$retailers_details = $Retailer->query("select * 
    						from retailers_details rd
    						where retailer_id = ".$retailer_id." 
    						and type in ('".$types."')");
    				$Retailer->query("delete from retailers_docs
    						where retailer_id = ".$retailer_id." 
    						and type in ('".$types."')");
    				foreach($retailers_details as $rd){
    					$Retailer->query("insert into retailers_docs
    							(retailer_id, type, src, uploader_user_id)
    							values (".$rd['rd']['retailer_id'].", '".$rd['rd']['type']."', '".$rd['rd']['image_name']."',
    							".$rd['rd']['uploader_user_id'].")");
    				}
    			}
    			break;
    		case 3:
    			
    			break;
    	}
    	$this->setKYCScore($retailer_id);
    }
    
    function setKYCScore($retailer_id){
    	$Retailer = ClassRegistry::init('Retailer');
    	
    	$retailers_kyc_states = $Retailer->query("select * 
    			from retailers_kyc_states rks
    			where rks.retailer_id = ".$retailer_id);
    	$kyc_score = 0;
    	foreach($retailers_kyc_states as $rks){
    		switch($rks['rks']['section_id']){
    			case "1":
    				$rks['rks']['verified_state'] AND $kyc_score += 35;
    				break;
    			case "2":
    				$rks['rks']['verified_state'] AND $kyc_score += 40;
    				break;
    			case "3":
    				$rks['rks']['verified_state'] AND $kyc_score += 25;
    				break;
    		}
    	}
    	
    	$Retailer->query("update retailers
    			set kyc_score = ".$kyc_score.", 
    			modified = '".date('Y-m-d H:i:s')."'
    			where id = ".$retailer_id);
    }
	
	function errorCodeMapping($vendorId,$errorCode){
		
		$errorcode = array("8" => array(
										"0" => "13",
										"1" => "30",
										"2" => "30",
										"3" => "30",
										"4" => "8",
										"5" => "403",
										"6" => "30",
										"7" => "6",
										"8" => "5",
										"9" => "46",
										"10" => "4",
										"11" => "403",
										"12" => "30",
										"21" => "26",
										"23" => "5",
										"24" => "30",
										"25" =>  "30",
										"30" => "14",
										"32" => "30",
										"33" => "30",
										"37" => "30",
										"45" => "9",
										"50" => "30",
										"52" => "30",
										"53" => "30",
										"54" => "43",
										"81" => "34",
										"82" => "30",
										"88" => "30)",
										"224" => "43",
										"134" => "30",
										"137" => "30"
		                               ),
				            "58" => array(
											"TXN" => "13",
											"TUP" => "31",
											"RPI" => "4",
											"UAD" => "404",
											"IAC" => "30",
											"IAT" => "30",
											"AAB" => "30",
											"IAB" => "26",
											"ISP" => "8",
											"DID" => "30",
											"DTX" => "30",
											"IAN" => "46",
											"IRA" => "6",
											"DTB" => "30",
											"RBT" => "43",
											"SPE" => "43",
											"SPD" => "43",
											"UED" => "30",
											"IEC" => "30",
											"IRT" => "30",
											"ITI" => "30",
											"TSU" => "30",
											"IPE" => "30",
											"ISE" => "30",
											"TRP" => "30",
											"OUI" => "0",
											"ODI" => "30"
							             ),
										 
								"27" => array(
												"100" => "13",
												"99" => "14",
												"101" => "30",
												"102" => "26",
												"103" => "6",
												"104" => "30",
												"105" => "30",
												"106" => "30",
												"107" => "5",
												"110" => "6",
												"111" => "30",
												"121" => "1",
												"123" => "30",
												"165" => "15",
												"170" => "30",	
												"171" => "30",
												"173" => "43",
												"172" => "2",
												"174" => "30"
									          ),
									"24" =>  array(
													"1200" => "15",
													"1201" => "0",
													"1202" => "5",
													"1203" => "6",
													"1204" => "30",
													"1205" => "9",
													"1206" => "30",
													"1207" => "26",
													"1208" => "26",
													"1209" => "30",
													"1210" => "30",
													"1211" => "30",
													"1212" => "0"
												 ),
									"18" => array (
													"0" => "0",
													"1" => "31",
													"501" => "30",
													"502" => "4",
													"503" => "14",
													"504" => "0",
													"505" => "0",	
													"506" => "26",
													"507" => "30",
													"508" => "30",
													"509" => "30",
													"510" => "2"
												  )
								             );
		
		return $errorcode[$vendorId][$errorCode];
				
	
	}

              function isStrongPassword($password)
                    {
                        if(!empty($password)):
                               // if(strlen($password)<5 || !preg_match( '~[A-Z]~', $password) ||  !preg_match( '~[a-z]~', $password) ||  !preg_match( '~[0-9]~', $password)):
				if(in_array($password,array('1010','1111','9999','1234','0000','4321','2222','4444','5555','6666','7777','8888','3333'))):
                                        return false;
                                else:
                                        return true;
                                endif;
                        endif;
                        
                        return false;
                    }


}
?>
