<?php

class ShopComponent extends Object {
	var $components = array('General','B2cextender');
	var $Memcache = null;
	
	function __construct(){
		if(class_exists("Memcache")){
			$memcache = new Memcache;
			if($memcache->pconnect(MEMCACHE_IP,11211)){
				$this->Memcache = $memcache;
			}
		}
	}
	
	function addMemcache($key,$value,$duration){
		if(!empty($this->Memcache)){
			return $this->Memcache->add($key,$value,false,$duration);
		}
		else return false;
	}
	
	function setMemcache($key,$value,$duration=null){
		if(!empty($this->Memcache)){
			if(empty($duration))$duration = 0;
			return $this->Memcache->set($key,$value,false,$duration);
		}
		else return false;
	}
	
	function incrementMemcache($key,$counter=1){
		if(!empty($this->Memcache)){
			return $this->Memcache->increment($key,$counter);
		}
		else return false;
	}
	
	function decrementMemcache($key,$counter=1){
		if(!empty($this->Memcache)){
			return $this->Memcache->decrement($key,$counter);
		}
		else return false;
	}
	
	function getMemcache($key){
		if(!empty($this->Memcache)){
			return $this->Memcache->get($key);
		}
		else return false;
	}
	
	function getMemcacheStats(){
		if(!empty($this->Memcache)){
			return $this->Memcache->getStats();
		}
		else return false;
	}
	
	function getMemcacheKeys($prefix=null){
		if(!empty($this->Memcache)){
			$list = array();
		    $allSlabs = $this->Memcache->getExtendedStats('slabs');
		    $items = $this->Memcache->getExtendedStats('items');
		    $i = 0;
		    foreach($allSlabs as $server => $slabs) {
		        foreach($slabs AS $slabId => $slabMeta) {
		            $cdump = $this->Memcache->getExtendedStats('cachedump',(int)$slabId);
		            foreach($cdump AS $keys => $arrVal) {
		                if (!is_array($arrVal)) continue;
		                foreach($arrVal AS $k => $v) {
		                	if(empty($prefix) || (strpos($k,$prefix) !== false && strpos($k,$prefix) == 0)){                   
		                    	$list[$k] = $this->getMemcache($k);
		                	}
		                }
		            }
		        }
		    }
		    return $list;
		}
		else return false;
	}
	
	function delMemcache($key){
		if(!empty($this->Memcache)){
			return $this->Memcache->delete($key);
		}
		else return false;
	}
	
	function redis_connect(){
		try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->redis = new Predis\Client(array(
                   'host' => REDIS_HOST,
                   'port' => REDIS_PORT 
			));
           
                   //'password' => REDIS_PASSWORD,
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->redis = false;
		}
		return $this->redis;
	}
        
        /**
         * Only for request TPS LINE
         * @return type
         */
        function redis_connector(){
		try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->redis = new Predis\Client(array(
                            'host' => 'redistps.oq14zy.0001.use1.cache.amazonaws.com',
                            'port' => 6300
			));
           
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->redis = false;
		}
		return $this->redis;
	}
		
	function errors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Wrong operator code/Wrong Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Try after some time.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
			'42'=>'Payment Authorization failed',
			'43'=>'Operator is currently down. Try after some time.',
			'44'=>'Please call customer care on 022-67242288 to activate this service.',//Transfer to kit to use this service',
			'45'=>'Complaint already taken',
			'46'=>'Wrong account/phone number',
                        '47'=>'Operator General Error',
			'48'=>'You are using an older version of Pay1. Please update the app to recharge.',
			'55'=>'Kindly create a strong password',
                                                         '60'=>'Exception Occured'   
			);
			return 	$err[$code];
	}
	function apiErrors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Wrong operator code/Wrong Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Please try this transaction after 15 minutes.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
			'42'=>"Are you sure you want to repeat this recharge?",
			'43'=>"Are you sure you want to contnue?",	
			'44'=>"No mobile or password found",
			'45'=>"The new number cannot be registered as a retailer.",
			'46'=>"Invalid mobile or password",
			'47'=>"OTP did not match. Mobile number did not change.",
			'48'=>"OTP expired. Try again.",	
			'49'=>"Your mobile did not match with our records.",	
                        '50'=>"Incorrect MOBILE NUMBER or PIN",
                        '51'=>"Leads is already created",
                        '52'=>"Retailer already exists with this mobile number",
                        '53'=>"Distributor already exists with this mobile number",
                        '54'=>"OTP did not match or expired. Try again.",
                        '55'=>"Lead generated successfully.",
                        '56'=>"Lead not generated.",
                        '57'=>"Incorrect MOBILE NUMBER or OTP",
                        '58'=>"Incorrect MOBILE NUMBER or INTEREST",
                        '59'=>"OTP has been sent to your mobile number",
                        
                        //------API Error Codes-------
                        'E000'=>'Some error occured.',
                        'E001'=>'Invalid Operation Type.',
                        'E002'=>'Account disabled.',
                        'E003'=>'Invalid inputs.',// Empty of insufficient input
                        'E004'=>'Invalid partner ID.',
                        'E005'=>'Authentication Error ( Invalid Access Key ).',
                        'E006'=>'Authentication Error ( Invalid access location ).',
                        'E007'=>'Not enough balance',
                        'E008'=>"Wrong operator code/Wrong Mobile Number.",
						'E009'=>"Wrong operator code/Wrong Subscriber id.",
						'E010'=>"Invalid amount.",
                        'E011'=>"Request already in process",
						'E012'=>"Try after some time",
						'E013'=>"Operator General Error",
						'E014'=>"Duplicate request",
						'E015'=>"Invalid Transaction Id",
						'E016'=>"Invalid operator access",
						'E017'=>"No of transactions should be less or equal to 10.",
						'E018'=>"Complaint already taken.",
                        'E019'=>"Cannot create retailer on this mobile number",
						'E020'=>"Retailer creation limit reached. Cannot create more retailers.",
				        'E021'=>"You cannot create retailer, contact Pay1",
				        'E022'=>"You have 0 kits left. Buy more retailer kits to enjoy this benefit.",
				   		'E023'=>"The Retailer could not be saved. Please, try again.",
						'E024'=>"field left empty",
						'E025'=> "Retailer not found. Cannot send OTP",
						'E026'=>"Incorrect OTP or PIN",
						'E027'=>"Authentication failure. OTP did not match.",
				
				//KYC error codes
				"E101" => "Update KYC to enjoy Toll Free Calling.",
				"E102" => "Your information is rejected.",
				"E103" => "Your information is under review. Wait for 48 hours.",
				"E104" => "Sorry! We are available between 8am to 11pm.",
				"E105" => "Incomplete data. Please update to enjoy Toll Free Calling."
			);
			return 	$err[$code];
	}
	
// 	function retailerTypes($type=null){
// 		$arr = array('1' => 'Telecom',
// 					 '2' => 'Kirana / General Stores',
// 					 '3' => 'Medical Store',
// 					 '4' => 'Cyber Cafes',
// 					 '5' => 'Travel Agency',
// 					 '6' => 'STD / PCO',
// 					 '7' => 'Footwear shops',
// 					 '8' => 'Fast Food Joints / Eateries',
// 					 '9' => 'Freelancer (without shop)',
// 					 '10' => 'Electronics',
// 					 '11' => 'Electrical and Hardware',
// 					 '12' => 'Computer Stationery',
// 					 '13' => 'Xerox and Stationery',
// 					 '14' => 'Estate Agent',
// 					 '15' => 'Clothing',
// 					 '16' => 'Bags and Other Accessories dealers',
// 					 '17' => 'Pan Bidi shop',
// 					 '18' => 'Saloon',
// 					 '19' => 'Tailors',
// 					 '20' => 'Others',
// 					 '21' => 'Mobile Store',
// 					 '22' => 'Stationery Shop',
// 					 '23' => 'Grocery Store',
// 					 '24' => 'Photocopy Store',
// 					 '25' => 'Hardware Shop'	
// 		);
		
// 		if(empty($type)) return $arr;
// 		else return $arr[$type];
// 	}
	
// 	function locationTypes($type=null){
// 		$arr = array('1' => 'Near Station',
// 					 '2' => 'Posh Area',
// 					 '3' => 'Slum Area',
// 					 '4' => 'Residential Area',
// 					 '5' => 'Market',
// 					 '6' => 'Industrial Area',
// 					 '7' => 'Commercial Area',
// 		);
		
// 		if(empty($type)) return $arr;
// 		else return $arr[$type];
// 	}
	
	function retailerTypes($type=null){
		$arr = array(
			'1' => 'Mobile Store',
        	'2' => 'Stationery Shop',
        	'3' => 'Medical Store',
			'4' => 'Grocery Store',
        	'5' => 'Photocopy Store',
			'6' => 'Travel Agency',
        	'7' => 'Hardware Shop',
        	'8'	=> 'Others'
		);
	
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function locationTypes($type=null){
		$arr = array(
			'1' => 'Residential Area',
			'2' => 'Commercial Area',
			'3' => 'Industrial Area'	
		);
	
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function structureTypes($type=null){
		$arr = array(
					'1' => 'Permanent',
					'2' => 'Temporary',
		);
		
		if(empty($type)) return $arr;
		else return $arr[$type];
	}
	
	function kycSectionMap($section_id = null){
		$map = array(
			'1' => array(
				'fields' => array('name'),
				'documents' => array('PAN_CARD')	
			),
			'2' => array(
				'fields' => array('shopname', 'area_id', 'address', 'pin', 'latitude', 'longitude'),
				'documents' => array('ADDRESS_PROOF')
			),
			'3' => array(
				'fields' => array('shop_type', 'shop_type_value', 'location_type'),
				'documents' => array('SHOP_PHOTO')
			)
		);
		
		if(empty($section_id))
			return $map;
		else 
			return $map[$section_id];
	}
	
	function mapApiErrs($code){
		$err = array('1' => 'E002','2' => 'E001','3' => 'E000','4' => 'E003','5' => 'E008','6' => 'E010','7' => 'E003',
		'8' => 'E008', '9' => 'E003','10' => 'E013','11' => 'E013','12' => 'E013','14' => 'E013','16'=>'E013','17'=>'E013','18'=>'E013',
		'19'=>'E013','20'=>'E013','21'=>'E013','22'=>'E013','23'=>'E013','24'=>'E013','25'=>'E013','26'=>'E007','27'=>'E013','29'=>'E013',
		'30'=>'E000','33'=>'E010','34'=>'E010','35'=>'E013','36'=>'E013','37'=>'E012','38'=>'E011','39'=>'E009','40'=>'E013','41'=>'E013','42'=>'E013','43'=>'E016','45'=>'E018');
		return 	$err[$code];
	}
   	function ussdErrors($code){
			$err = array(      
                    "1"=>"Unknown subscriber",
                    "105"=>"SDP:SIM Card data does not exist.",
                    "11"=>"Teleservice not provisioned",
                    "110"=>"Map Dialog P Abort Indication : Source is MAP",
                    "111"=>"Map Dialog P Abort Indication : Source is TCAP",
                    "112"=>"Map Dialog P Abort Indication : Source is Network",
                    "13"=>"CallBarred",
                    "130"=>"Map Dialogue rejected refuse reason :invalid dest reference",
                    "132"=>"Map Dialog Rejected Refuse Reason: Application Context not supported",
                    "137"=>"Map Dialog Rejected Provider Reason: Resource Limitation",
                    "138"=>"Map Dialogue rejected provider reason : Maintenance Activity",
                    "150"=>"Map Dialog User Abort User Reason: User specific reason",
                    "151"=>"MAP_DLGUA_UsrRsn_UsrResourceLimitation",
                    "153"=>"Map Dialog User Abort User Reason: App procedure cancelled",
                    "176"=>"Invalid service code",
                    "177"=>"Access Denied Blacklist",
                    "178"=>"CDREC_IMSIBlackListed",
                    "183"=>"VLR black listed",
                    "192"=>"Unexpected session release server/3rd party APP",
                    "193"=>"No reseponse with configurable time from servers/3rd party application",
                    "197"=>"Application Specific Error from 3rd partyClient",
                    "209"=>"Timer expired for SRISM Response",
                    "210"=>"Timer expired for N/W initaited USSRN",
                    "211"=>"Timer expired for Mobile initaited USSRN",
                    "229"=>"Received Invalid MAP message",
                    "231"=>"IMSI unavailable in SRISM Response",
                    "232"=>"VLR unavailable in SRISM Response",
                    "235"=>"Exit Option Selected By Subscriber",
                    "236"=>"Menu is Subscriber based/but subscriber type could not be resolved at the point in transaction.",
                    "242"=>"Service not configured/Menu Incomplete",
                    "243"=>"Invalid user input",
                    "244"=>"Access Denied Blacklisted",
                    "245"=>"Client not connected",
                    "27"=>"Absent Subscriber",
                    "31"=>"subscriber busy for Mt sms",
                    "34"=>"System failure",
                    "35"=>"dataMissing",
                    "36"=>"unexpectedDataValue",
                    "9"=>"Illegal subscriber",
                    "72"=>"Ussd Busy"
			);
            return 	$err[$code];
        }      
	function smsProdCodes($code){
		$prod = array(		
				'1'=>array(
					'operator'=>'Aircel',
					'params'=>array('method' => 'mobRecharge','operator'=>'1','type'=>'flexi')
				),
				'2'=>array(
					'operator'=>'Airtel',
					'params'=>array('method' => 'mobRecharge','operator'=>'2','type'=>'flexi')
				),
				'3'=>array(
					'operator'=>'BSNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi')
				),
				'4'=>array(
					'operator'=>'Idea',
					'params'=>array('method' => 'mobRecharge','operator'=>'4','type'=>'flexi')
				),
				'5'=>array(
					'operator'=>'Loop/BPL',
					'params'=>array('method' => 'mobRecharge','operator'=>'5','type'=>'flexi')
				),
				'6'=>array(
					'operator'=>'MTS',
					'params'=>array('method' => 'mobRecharge','operator'=>'6','type'=>'flexi')
				),
				'7'=>array(
					'operator'=>'Reliance CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'7','type'=>'flexi')
				),
				'8'=>array(
					'operator'=>'Reliance GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'8','type'=>'flexi')
				),
				'9'=>array(
					'operator'=>'Tata Docomo',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi')
				),
				'10'=>array(
					'operator'=>'Tata Indicom',
					'params'=>array('method' => 'mobRecharge','operator'=>'10','type'=>'flexi')
				),
				'11'=>array(
					'operator'=>'Uninor',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi')
				),
				'12'=>array(
					'operator'=>'Videocon',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi')
				),
				'13'=>array(
					'operator'=>'Virgin CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'13','type'=>'flexi')
				),
				'14'=>array(
					'operator'=>'Virgin GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'14','type'=>'flexi')
				),
				'15'=>array(
					'operator'=>'Vodafone',
					'params'=>array('method' => 'mobRecharge','operator'=>'15','type'=>'flexi')
				),
				'27'=>array(
					'operator'=>'Tata SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi','special'=>1)
				),
				'28'=>array(
					'operator'=>'Videocon SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi','special'=>1)
				),
				'29'=>array(
					'operator'=>'Uninor SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi','special'=>1)
				),
				'30'=>array(
					'operator'=>'MTNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi')
				),
				'31'=>array(
					'operator'=>'MTNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi','special'=>1)
				),
				'34'=>array(
					'operator'=>'BSNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi','special'=>1)
				),
				'16'=>array(
					'operator'=>'Airtel DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'1','type'=>'flexi')
				),
				'17'=>array(
					'operator'=>'Big TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'2','type'=>'flexi')
				),
				'18'=>array(
					'operator'=>'Dish TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'3','type'=>'flexi')
				),
				'19'=>array(
					'operator'=>'Sun TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'4','type'=>'flexi')
				),
				'20'=>array(
					'operator'=>'Tata Sky DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'5','type'=>'flexi')
				),
				'21'=>array(
					'operator'=>'Videocon DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'6','type'=>'flexi')
				),
				'22'=>array(
					'operator'=>'Dil Vil Pyar Vyar',
					'params'=>array('method' => 'vasRecharge','operator'=>'22','type'=>'fix')
				),
				'23'=>array(
					'operator'=>'Naughty Jokes',
					'params'=>array('method' => 'vasRecharge','operator'=>'23','type'=>'fix')
				),
				'24'=>array(
					'operator'=>'PNR Alert',
					'params'=>array('method' => 'vasRecharge','operator'=>'24','type'=>'fix')
				),
				'25'=>array(
					'operator'=>'Instant Cricket',
					'params'=>array('method' => 'vasRecharge','operator'=>'25','type'=>'fix')
				),
				'32'=>array(
					'operator'=>'Chatpati Baate Mini Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'32','type'=>'fix')
				),
				'33'=>array(
					'operator'=>'Chatpati Baate Mega Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'33','type'=>'fix')
				),
				'35'=>array(
					'operator'=>'Ditto TV',
					'params'=>array('method' => 'vasRecharge','operator'=>'35','type'=>'flexi')
				),
                                /*  36 - Docomo Postpaid 
                                    37 - Loop Mobile PostPaid
                                    38 - Cellone PostPaid 
                                    39 - IDEA Postpaid 
                                    40 - Tata TeleServices PostPaid 
                                    41 - Vodafone Postpaid*/
                                '36'=>array(
					'operator'=>'Docomo Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'1','type'=>'flexi')
				),
                                '37'=>array(
					'operator'=>'Loop Mobile PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'2','type'=>'flexi')
				),
                                '38'=>array(
					'operator'=>'Cellone PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'3','type'=>'flexi')
				),
                                '39'=>array(
					'operator'=>'IDEA Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'4','type'=>'flexi')
				),
                                '40'=>array(
					'operator'=>'Tata TeleServices PostPaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'5','type'=>'flexi')
				),
                                '41'=>array(
					'operator'=>'Vodafone Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'6','type'=>'flexi')
				),
				
                                '42'=>array(
					'operator'=>'Airtel Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'7','type'=>'flexi')
				),
				
                                '43'=>array(
					'operator'=>'Reliance Postpaid',
					'params'=>array('method' => 'mobBillPayment','operator'=>'8','type'=>'flexi')
				),
				
                                '44'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'pay1Wallet','operator'=>'1','type'=>'flexi')
				),
								'45'=>array(
					'operator'=>'Reliance Energy (Mumbai)',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'1','type'=>'flexi')
				),
								'46'=>array(
					'operator'=>'BSES Rajdhani',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'2','type'=>'flexi')
				),
								'47'=>array(
					'operator'=>'BSES Yamuna',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'3','type'=>'flexi')
				),
								'48'=>array(
					'operator'=>'North Delhi Power Limited',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'4','type'=>'flexi')
				),
								'49'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'5','type'=>'flexi')
				),
								'50'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'6','type'=>'flexi')
				),
								'51'=>array(
					'operator'=>'Pay1 Wallet',
					'params'=>array('method' => 'utilityBillPayment','operator'=>'7','type'=>'flexi')
				)
			);
			return  isset($prod[$code]['params']) ? $prod[$code]['params'] : array() ;
	}
	
	function getRetailerTrnsDetails($mobile){
		$data = $this->getMemcache($mobile."_retTxn");
		return $data;
	}
	
	function setRetailerTrnsDetails($mobile,$data){
		$this->setMemcache($mobile."_retTxn",$data,3*24*60*60);
		return;
	}
	
	function getSalesmanDeviceData($mobile){
		$data = $this->getMemcache($mobile."_device_data");
		return $data;
	}
	
	function setSalesmanDeviceData($mobile, $data){
		$data['trans_type'] = $data['trans_type'] . "_distributor";
		$this->setMemcache($mobile."_device_data", $data, 3*24*60*60);
		return;
	}
	
	function shopTransactionUpdate($type,$amount,$ref1_id,$ref2_id,$user_id=null,$discount=null,$type_flag = null,$note=null,$dataSource=null){
                                    if(is_null($dataSource)):
                                        
                                 
                                                            $this->data = null;
                                                            $transObj = ClassRegistry::init('ShopTransaction');

                                                            $this->data['ShopTransaction']['ref1_id'] = $ref1_id;
                                                            $this->data['ShopTransaction']['ref2_id'] =  $ref2_id;
                                                            $this->data['ShopTransaction']['amount'] =  $amount;
                                                            $this->data['ShopTransaction']['type'] =  $type;
                                                            $this->data['ShopTransaction']['timestamp'] =  date('Y-m-d H:i:s');
                                                            $this->data['ShopTransaction']['date'] =  date('Y-m-d');
                                                            $this->data['ShopTransaction']['note'] =  $note;
                                                            if($type == RETAILER_ACTIVATION){
                                                                    $this->data['ShopTransaction']['confirm_flag'] =  1;
                                                            }

                                                            if($user_id != null){
                                                                    $this->data['ShopTransaction']['user_id'] =  $user_id;
                                                            }
                                                            if($discount != null){
                                                                    $this->data['ShopTransaction']['discount_comission'] =  $discount;
                                                            }
                                                            if($type_flag != null){
                                                                    $this->data['ShopTransaction']['type_flag'] =  $type_flag;
                                                            }
                                                            $transObj->create();
                                                            if($transObj->save($this->data)){
                                                                    return $transObj->id;
                                                            }
                                                            else return false;
                
                                    else:
                                                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Creating ST ',FILE_APPEND | LOCK_EX);
                                                    $confirm_flag=($type==RETAILER_ACTIVATION)?1:0;
                                                    $user_id=  !is_null($user_id)?$user_id:null;
                                                    $discount_comission=!is_null($discount)?$discount:null;
                                                    $type_flag=!is_null($type_flag)?$type_flag:null;
                                                    $date=date('Y-m-d');
                                                    $timestamp=date('Y-m-d H:i:s');
                                                    $sql=" Insert into shop_transactions(ref1_id,ref2_id,amount,type,timestamp,date,note,confirm_flag,user_id,discount_comission,type_flag)  "
                                                            . "  values('{$ref1_id}','{$ref2_id}','{$amount}','{$type}','{$timestamp}','{$date}','{$note}','{$confirm_flag}','{$user_id}','{$discount_comission}','{$type_flag}' ) ";

                                                   if($dataSource->query($sql)):        
                                                        $lastInserIdQuery = $dataSource->query("SELECT LAST_INSERT_ID() as id FROM shop_transactions limit 1 ");
                                                        //return $dataSource->lastInsertId();
                                                         $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"ST id :  {$lastInserIdQuery[0][0]['id']}",FILE_APPEND | LOCK_EX); 
                                                        return $lastInserIdQuery[0][0]['id'];
                                                   else:
                                                        $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"ST Failed :  {$lastInserIdQuery[0][0]['id']}",FILE_APPEND | LOCK_EX); 
                                                        return false;
                                                   endif;
                                  
                                  endif;
	}
	
	function addAppRequest($method,$mobile,$amount,$opr,$type,$ret_id){
		
		$ret = $this->addMemcache("request_".$mobile."_".$amount."_".$opr."_".$ret_id, 1, 5*60);
		if($ret !== false) return 1;
		else return 0;
		/*$reqObj = ClassRegistry::init('Request');
		
		//$reqObj->query("DELETE FROM requests WHERE timestamp < now() - 300");
		
		$this->data['Request']['method'] = $method;
		$this->data['Request']['mobile'] = $mobile;
		$this->data['Request']['amount'] =  $amount;
		$this->data['Request']['operator'] =  $opr;
		$this->data['Request']['type'] =  $type;
		$this->data['Request']['timestamp'] =  NULL;
		$reqObj->create();
        $insertQry = "INSERT INTO requests (method,mobile,amount,operator,type,timestamp) VALUES ('".$method."','".$mobile."','".$amount."','".$opr."','".$type."',NULL)";
        $lastId = "SELECT LAST_INSERT_ID() as 'insertId'";
        
        if(in_array(strtolower($method),array('cashpgpayment'))){
            if($reqObj->query($insertQry)){
                $lastId_arr = $reqObj->query($lastId);
                return $lastId_arr[0][0]['insertId'];
            }
        }elseif($reqObj->save($this->data)){
			return $reqObj->id;
		}        
		
        $reqObj->query("INSERT INTO requests_dropped (retailer_id,method,mobile,amount,operator,timestamp) VALUES (".$_SESSION['Auth']['id'].",'$method','$mobile',$amount,$opr,'".date('Y-m-d H:i:s')."')");
        return null;*/
		
		
	}
	
	function deleteAppRequest($mobile,$amount,$opr,$ret_id){
		$this->delMemcache("request_".$mobile."_".$amount."_".$opr."_".$ret_id);
		/*$reqObj = ClassRegistry::init('Request');
		
		$reqObj->query("DELETE FROM requests WHERE mobile='$mobile' AND amount=$amount");*/
	}
	
	function addComment($userId,$retId,$transId,$test,$loggedInUser,$name=null,$tag_id,$call_type_id){
		$userObj = ClassRegistry::init('User');
		
// 		if(!empty($test)){
// 			$medium = -1;
// 			if(!empty($transId)){
// 				$medium = $userObj->query("select api_flag from vendors_activations where ref_code = '$transId'");
// 				$medium = $medium[0]['vendors_activations']['api_flag'];
// 			}

			$userObj->query("insert into comments(users_id,retailers_id,mobile,ref_code,comments,created,tag_id,call_type_id,date) values('$userId','$retId','$loggedInUser','$transId','".addslashes($test)."','".date('Y-m-d H:i:s')."','".$tag_id."','".$call_type_id."','".date('Y-m-d')."')");	
			if(!empty($transId)){
				$slaveObj = ClassRegistry::init('Slaves');
				$comments_count = $slaveObj->query("select ref_code, count from comments_count where ref_code = '$transId' and date = '".date('Y-m-d')."'");
				$va = $slaveObj->query("select vendor_id, product_id, api_flag from vendors_activations where ref_code = '$transId'");
				
				if($comments_count)
					$userObj->query("update comments_count set count = count + 1 where ref_code = '$transId' and date = '".date('Y-m-d')."'");
				else
					$userObj->query("insert into comments_count(ref_code, count, vendor_id, product_id, retailer_id, medium, date) values('$transId', 1, ".$va[0]['vendors_activations']['vendor_id'].", ".$va[0]['vendors_activations']['product_id'].", '$retId', ".$va[0]['vendors_activations']['api_flag'].", '".date('Y-m-d')."')");
			}
			/*$user = $this->General->getUserDataFromMobile($loggedInUser);
			$data['user'] = $user['name'];
			if(!empty($transId)){
				$txid = $transId;
				$data['txtype'] = 1;
				
				$extra = $userObj->query("SELECT vendors.shortForm,vendors_activations.amount,products.name FROM vendors_activations,vendors,products WHERE products.id = product_id AND vendors.id = vendor_id AND ref_code = '$transId'");	
				$data['extra'] = $extra['0']['vendors']['shortForm'] . " | " . $extra['0']['products']['name'] . " | " . $extra['0']['vendors_activations']['amount'];
			}
			else if(!empty($retId)){
				$retData = $userObj->query("SELECT mobile FROM retailers WHERE id = $retId");	
		
				$txid = $retData['0']['retailers']['mobile'];
				$data['txtype'] = 2;
			}
			else {
				$user = $this->General->getUserDataFromId($userId);
				$txid = $user['mobile'];
				$data['txtype'] = 3;
			}
			$data['txid'] = $txid;
			$data['process'] = 'cc';
			$data['msg'] = $test;
			$this->General->curl_post_async("http://pay1.in/cclive/server.php",$data);
			*/
		
// 		}
	}
	
	function shopCreditDebitUpdate($type,$amount,$from,$to,$to_groupId,$desc,$numbering,$api_flag=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		if($api_flag == null)$api_flag = 0;
		if(!empty($from))
			$transObj->query("INSERT INTO shop_creditdebit (from_id,to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($from,$to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
		else 
			$transObj->query("INSERT INTO shop_creditdebit (to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
	}
	
	function transactionsOnRetailerActivation($retailer_id,$product,$user_id,$amount,$ip=NULL,$apiflag=NULL,$dataSource=null){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->recursive = -1;
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		
		//transaction entries
		$perData = $this->getCommissionPercent($ret_shop['slab_id'],$product);
		$percent = (isset($perData['percent']))?$perData['percent']:0;
		$trans_id = $this->shopTransactionUpdate(RETAILER_ACTIVATION,$amount,$retailer_id,$product,$user_id,$percent,null,$ip,$dataSource);
		if($trans_id === false) return false;
		
		$commission_retailer = $percent*$amount/100;
		//$commission_retailer = $this->calculateCommission($retailer_id,RETAILER,$product,$amount,$ret_shop['slab_id']);
		
		
		$distributor_id = $ret_shop['parent_id'];
		$dist_shop = $this->getShopDataById($distributor_id,DISTRIBUTOR);
		/*
		$superdistributor_id = $dist_shop['parent_id'];
		$superdist_shop = $this->getShopDataById($superdistributor_id,SUPER_DISTRIBUTOR);
		
		$commission_distributor = $this->calculateCommission($distributor_id,DISTRIBUTOR,$product,$amount);
		
		$commission_superdistributor = $this->calculateCommission($superdistributor_id,SUPER_DISTRIBUTOR,$product,$amount);
		$sd_percent = $this->getCommissionPercent($superdist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$commission_superdistributor,$superdistributor_id,$trans_id,null,$sd_percent);
		
		if(TAX_MODEL == 0){
			$tds_superdistributor = $this->calculateTDS($commission_superdistributor);
			$this->shopTransactionUpdate(TDS_SUPERDISTRIBUTOR,$tds_superdistributor,$superdistributor_id,$trans_id);
			$commission_superdistributor -= $tds_superdistributor;
		}
		$d_percent = $this->getCommissionPercent($dist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$commission_distributor,$distributor_id,$trans_id,null,$d_percent);
		if($superdist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
			$tds_distributor = $this->calculateTDS($commission_distributor);
			$this->shopTransactionUpdate(TDS_DISTRIBUTOR,$tds_distributor,$distributor_id,$trans_id);
			$commission_distributor -= $tds_distributor;
		}*/
		
		if($commission_retailer > 0){ 
			$this->shopTransactionUpdate(COMMISSION_RETAILER,$commission_retailer,$retailer_id,$trans_id,null,$percent,$dataSource);
			if($dist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
				$tds_retailer = $this->calculateTDS($commission_retailer);
				$this->shopTransactionUpdate(TDS_RETAILER,$tds_retailer,$retailer_id,$trans_id,$dataSource);
				$commission_retailer -= $tds_retailer;	
			}
		}
		
		$service_charge = (isset($perData['service_charge']) && !empty($perData['service_charge']))? ($perData['service_charge'] * $amount/100): (in_array($apiflag,array(0,2)) ? 0.2 : 0);
                if($product > 35){
                    $this->General->logData('/mnt/logs/commisionlog.txt',  json_encode($perData)." | service charge : ".$service_charge." | product :".$product."| amount :".$amount);
                }
		if($service_charge > 0){
			if($perData['service_tax'] == 1 && !empty($perData['service_charge'])){
				$service_charge = ($service_charge<5)? 5 : $service_charge;
                                
                                $service_charge = $service_charge + ($service_charge * 14.5/100);
				//$service_charge = $service_charge*114.5/100;
			}
			$this->shopTransactionUpdate(SERVICE_CHARGE,$service_charge,$retailer_id,$trans_id,RETAILER,$dataSource);
		}
			
		if($commission_retailer > 0 && $commission_retailer < $service_charge){
			$service_charge = 0;
		}
		
		$service_charge = round($service_charge,2);
		//balance update
		//$amount_distributor = $commission_distributor - $commission_retailer;
		//$amount_superdistributor = $commission_superdistributor - $commission_distributor;
		$amount_retailer = $amount - $commission_retailer + $service_charge;
		
		$bal = $this->shopBalanceUpdate($amount_retailer,'subtract',$retailer_id,RETAILER,$dataSource);
                
                                    if($bal<0){
                                         $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Negative bal encountered Rolling Back  : {$bal}",FILE_APPEND | LOCK_EX); 
                                         return false;
                                    }
                                    
		$this->addOpeningClosing($retailer_id,RETAILER,$trans_id,$bal+$amount_retailer,$bal,$dataSource);
		//$this->shopBalanceUpdate($amount_distributor,'add',$distributor_id,DISTRIBUTOR);
		//$this->shopBalanceUpdate($amount_superdistributor,'add',$superdistributor_id,SUPER_DISTRIBUTOR);
		return array($bal,$trans_id,$service_charge);
	}
	
	function addOpeningClosing($shop_id,$group_id,$shoptrans_id,$opening,$closing,$dataSource=null){
		$prodObj = is_null($dataSource)?ClassRegistry::init('Product'):$dataSource;
		$prodObj->query("INSERT INTO opening_closing (shop_id,group_id,shop_transaction_id,opening,closing,timestamp) VALUES ($shop_id,$group_id,$shoptrans_id,$opening,$closing,'".date('Y-m-d H:i:s')."')");
                                     $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Adding Opening Closing entry  : ",FILE_APPEND | LOCK_EX); 
	}
	
	function createVendorActivation($vendor_id,$product_id,$api_flag,$mobile,$amount,$shop_trans_id,$ret_id,$param = null,$dataSource=null){
                
		   if(empty($api_flag))$api_flag = 0;
		
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$this->data = null;
		$this->data['VendorsActivation']['vendor_id'] = $vendor_id;
		$this->data['VendorsActivation']['product_id'] =  $product_id;
		$this->data['VendorsActivation']['mobile'] =  $mobile;
		$this->data['VendorsActivation']['amount'] =  $amount;
		$this->data['VendorsActivation']['shop_transaction_id'] =  $shop_trans_id;
		$this->data['VendorsActivation']['retailer_id'] =  $ret_id;
		$this->data['VendorsActivation']['timestamp'] =  date('Y-m-d H:i:s');
		$this->data['VendorsActivation']['date'] =  date('Y-m-d');
		$this->data['VendorsActivation']['api_flag'] =  $api_flag;
                $this->data['VendorsActivation']['ref_code'] =  uniqid(rand(0, 1000));
                //$this->data['VendorsActivation']['hour'] =  date('H');
		if($param != null){
			$this->data['VendorsActivation']['param'] =  $param;
		}
		
		$data = $vendorActObj->query("SELECT discount_commission FROM vendors_commissions WHERE vendor_id=$vendor_id AND product_id=$product_id");
		$this->data['VendorsActivation']['discount_commission'] = $data['0']['vendors_commissions']['discount_commission'];
                		                
                                    $timestamp=date('Y-m-d H:i:s');
                                    $date=date('Y-m-d');
                                    $ref_code=uniqid(rand(0, 1000));
                                    $param=!is_null($param)?$param:null;
                                    $sql="Insert into vendors_activations(vendor_id,product_id,mobile,amount,shop_transaction_id,retailer_id,timestamp,date,api_flag,ref_code,param,discount_commission)"
                                            . "  values('{$vendor_id}','{$product_id}','{$mobile}','{$amount}','{$shop_trans_id}','{$ret_id}','{$timestamp}','{$date}','{$api_flag}','{$ref_code}','{$param}','{$data['0']['vendors_commissions']['discount_commission']}') ";
                                    
		//$vendorActObj->create();
                                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Creating VA entry ',FILE_APPEND | LOCK_EX);
                                            //if($vendorActObj->save($this->data)){
                                            if($dataSource->query($sql)){
                                                        $lastInserIdQuery = $dataSource->query("SELECT LAST_INSERT_ID() as id FROM vendors_activations limit 1 ");
			//$id = $dataSource->lastInsertId();
                                                        $id = $lastInserIdQuery[0][0]['id'];
                                                        $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA id : {$id} ",FILE_APPEND | LOCK_EX);
                                                        
			$ref_code =  3022*100000000 + intval($id);
			//$ref_code =  "3022" . sprintf('%08d', $id);
			if($dataSource->query("UPDATE vendors_activations SET ref_code = '$ref_code' WHERE id=$id")){
			//if($vendorActObj->updateAll(array('VendorsActivation.ref_code' => $ref_code), array('VendorsActivation.id' => $id))){
                                                            $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," Updating ref code in VA refcode: {$ref_code} ",FILE_APPEND | LOCK_EX);
				return $ref_code;
			}
			else {
                file_put_contents('/mnt/logs/vendor_activation_failure.log', " error in updating vendor_activation \n" , FILE_APPEND | LOCK_EX);
				//$dataSource->query("DELETE FROM vendors_activations WHERE id = $id");
                                                                  $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," Updating ref code in VA failed ",FILE_APPEND | LOCK_EX);
				return false;
			}
		}else{
            file_put_contents('/mnt/logs/vendor_activation_failure.log', " error in saving vendoractivation object : data provided : (".json_encode($this->data).") \n" , FILE_APPEND | LOCK_EX);
            $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA query Failed  ",FILE_APPEND | LOCK_EX);
        }
		
		return false;
	}
	
	function calculateExpectedEarning($data){
		if($data['vendors']['update_flag'] == 1){
			$exp_earn = $data['earnings_logs']['incoming'] - $data['earnings_logs']['invested'];
		}
		else if($data['earnings_logs']['vendor_id'] == 36){
			$exp_earn = intval($data['earnings_logs']['invested']*3.7/100);
		}
		else if($data['earnings_logs']['vendor_id'] == 62){
			$exp_earn = intval($data['earnings_logs']['invested']*3.5/100);
		}
		else $exp_earn = $data['earnings_logs']['expected_earning'];
		
		return $exp_earn;
	}
	
	function updateVendorActivation($ref_code,$trans_id,$status,$code,$cause,$extra,$opr_id,$vendor_id,$discount_comm=null){
		//$vendorActObj = ClassRegistry::init('VendorsActivation');
		//$vendorActObj->recursive = -1;
		
		//$vendorActObj->query("update vendors_activations set vendor_refid = '".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id' where ref_code='".$ref_code."'");
       	if(empty($vendor_id))return;
		
		if(empty($discount_comm)){
        		$this->General->update_in_vendors_activations(array('vendor_refid'=>$trans_id,'code'=>$code,'cause'=>addslashes($cause),'status'=>$status,'extra'=>addslashes($extra),'operator_id'=>$opr_id,'vendor_id'=>$vendor_id),array('ref_code'=>$ref_code));
                        //$this->General->update_in_vendors_activations("vendor_refid='".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id',vendor_id='$vendor_id'","ref_code='$ref_code'");
       	}
       	else {
       		$this->General->update_in_vendors_activations(array('vendor_refid'=>$trans_id,'code'=>$code,'cause'=>addslashes($cause),'status'=>$status,'extra'=>addslashes($extra),'operator_id'=>$opr_id,'vendor_id'=>$vendor_id,'discount_commission'=>$discount_comm),array('ref_code'=>$ref_code));
                //$this->General->update_in_vendors_activations("vendor_refid='".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id',vendor_id='$vendor_id',discount_commission='$discount_comm'","ref_code='$ref_code'");
       	}
      }
	
	function createTransaction($prodId,$venId,$api_flag,$mobNo,$amt,$custId=null,$ip=null){
		//$shopData = $this->getShopDataById($_SESSION['Auth']['id'],$_SESSION['Auth']['User']['group_id']);
            
		if($api_flag != 4){//api partner
		$check_wait_time = $this->addMemcache("requested_user_".$_SESSION['Auth']['id'],$_SESSION['Auth']['id'] , 6);
                
                if(!$check_wait_time){
                    return array('status'=>'failure','code'=>'37','description'=>$this->errors(37));	
                }
		}
                
                try{
                
                $balance = $this->getBalance($_SESSION['Auth']['id'],$_SESSION['Auth']['User']['group_id']);
		
		if($balance < $amt){
                                                        $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
			return array('status'=>'failure','code'=>'26','description'=>$this->errors(26));	
		}
		else {
                                                        /*
                                                        * Create Transaction
                                                        * Start
                                                        */
                                                       $userObj = ClassRegistry::init('User');
                                                       $dataSource=$userObj->getDataSource();
                                                       $dataSource->begin();
                                                       $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','Created DataSource Object ',FILE_APPEND | LOCK_EX);
                                                       
                                                       $exists = $this->General->checkIfUserExists($mobNo);
			if(!$exists){
				$user = $this->General->registerUser($mobNo,RETAILER_REG);
				$user = $user['User'];
			}
			else {
				$user = $this->General->getUserDataFromMobile($mobNo);
			}
			$ret = $this->transactionsOnRetailerActivation($_SESSION['Auth']['id'],$prodId,$user['id'],$amt,$ip,$api_flag,$dataSource);
			if($ret === false){
                                                                         $dataSource->rollback();
                                                                            $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
				return array('status'=>'failure','code'=>'30','description'=>$this->errors(30));
			}            
			$ref_id = $this->createVendorActivation($venId,$prodId,$api_flag,$mobNo,$amt,$ret[1],$_SESSION['Auth']['id'],$custId,$dataSource);
			if($ref_id === false || empty($ref_id)){                
                $req_content = $venId."|".$prodId."|".$api_flag."|".$mobNo."|".$amt."|".$ret[1]."|".$_SESSION['Auth']['id']."|".$custId;
                file_put_contents('/mnt/logs/vendor_activation_failure.log', "vendor_act param : ".$req_content." : result_ref_id - ".$ref_id."  var_dump : ". json_encode($ref_id)." \n" , FILE_APPEND | LOCK_EX);
				$shop_transid = $ret[1];
                                                                           $dataSource->rollback();
                                                                             $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," VA failed",FILE_APPEND | LOCK_EX);
				/*
                                                                           $vendorActObj = ClassRegistry::init('VendorsActivation');
				$vendorActObj->query("DELETE FROM shop_transactions WHERE id = '$shop_transid'");
				$vendorActObj->query("DELETE FROM opening_closing WHERE shop_transaction_id	= '$shop_transid'");
				$vendorActObj->query("UPDATE retailers SET balance = balance + $amt, modified = '".date('Y-m-d H:i:s')."' WHERE id= '".$_SESSION['Auth']['id']."'");
				*/
                                                                          $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);   
				return array('status'=>'failure','code'=>'30','description'=>$this->errors(30));
			}
                                                        else 
                                                    { 
                                                            $dataSource->commit(); 
                                                               $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log'," DONE ! ! !  ",FILE_APPEND | LOCK_EX);
                                                               $this->delMemcache("requested_user_".$_SESSION['Auth']['id']);
                                                            return array('status'=>'success','tranId'=>$ref_id,'balance'=>$ret[0],'service_charge'=>$ret[2]);
                                                     }		
		}
                }
                catch(Exception $e)
                {
                    
                    $dataSource->rollback();
                    $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log','in Catch Exception occured ',FILE_APPEND | LOCK_EX);
                    $this->General->logData('/mnt/log/ExceptionEncountered : '.date('Y-m-d').'.log',json_encode($e),FILE_APPEND | LOCK_EX);
                    return array('status'=>'failure','code'=>'60','description'=>$this->errors(60));
                }
                
	}
	
	
	function setProdVendorHealth($vendor_id,$prod_id,$status){
		$info = $this->getVendorInfo($vendor_id);
		
		$map_arr = array('9'=>array(9,10,27),'10'=>array(9,10,27),'27'=>array(9,10,27),'11'=>array(11,29),'29'=>array(11,29),'7'=>array(7,8),'8'=>array(7,8),'7'=>array(7,8),'12'=>array(12,28),'28'=>array(12,28),'3'=>array(3,34),'34'=>array(3,34),'30'=>array(30,31),'31'=>array(30,31));
		$prod_arr = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
		        		
		$prod_id = isset($prod_arr[$prod_id]) ? $prod_arr[$prod_id] : $prod_id;
		$key = "health_$vendor_id"."_".$prod_id."_".date('Ymd');
        
        $cap_inprocess_limit = $this->getMemcache("cap_inprocess_$prod_id"."_$vendor_id");        
		$max_cap = (!empty($cap_inprocess_limit)) ? ($cap_inprocess_limit * 4) :50;
        
		$get = $this->getMemcache($key);
		$prods = isset($map_arr[$prod_id]) ? $map_arr[$prod_id] : array($prod_id);
		
		if($info['update_flag'] == 0){
			if($status == 1){
				if($get === false){
					$this->setMemcache($key,0,2*60*60);
				}
				else if($get > 0){
                    $health_percent = 45;
					$val = $this->decrementMemcache($key);
					if($val < ($max_cap * $health_percent)/100 && $this->getMemcache("disabled_$vendor_id"."_".$prod_id)){
						$vendorActObj = ClassRegistry::init('VendorsActivation');
						
						$prod_name = array();
						foreach($prods as $p){
							$ex_data = $vendorActObj->query("SELECT oprDown FROM vendors_commissions WHERE vendor_id = $vendor_id AND product_id = $p");
							if($ex_data[0]['vendors_commissions']['oprDown'] == 1){
								$vendorActObj->query("UPDATE vendors_commissions SET oprDown = 0 WHERE vendor_id = $vendor_id AND product_id = $p AND oprDown = 1");
								$prodInfo = $this->setProdInfo($p);
								$prod_name[] = $prodInfo['name'];
							}
						}
						if(!empty($prod_name)){
							$this->General->sendMails("(SOS)Enabling ".implode(",", $prod_name) ." of vendor ".$info['company'],"Transactions cleared so activated it automatically",array('backend@mindsarray.com'),'mail');
							$this->delMemcache("disabled_$vendor_id"."_".$prod_id);
	                        $this->General->logData('/mnt/logs/vendor_status.txt',"Enabling : $vendor_id"."_".$prod_id);
						}
                        //$this->setMemcache($key,0,30*60);
					}
                    
					$this->General->logData('/mnt/logs/vendor_status.txt',"decrement:: $key: $val");
				}
			}
			else if($status == 0){
				if($get === false){
					$this->setMemcache($key,1,2*60*60);
				}
				else {
					$val = $this->incrementMemcache($key);
					if($val > $max_cap){
						$vendorActObj = ClassRegistry::init('VendorsActivation');
						$prod_name = array();
						foreach($prods as $p){
							$ex_data = $vendorActObj->query("SELECT oprDown FROM vendors_commissions WHERE vendor_id = $vendor_id AND product_id = $p");
							if($ex_data[0]['vendors_commissions']['oprDown'] == 0){
								$vendorActObj->query("UPDATE vendors_commissions SET oprDown = 1 WHERE vendor_id = $vendor_id AND product_id = $p AND oprDown = 0");
								$prodInfo = $this->setProdInfo($p);
								$prod_name[] = $prodInfo['name'];
							}
						}
						if(!empty($prod_name)){
							$this->General->sendMails("(SOS)Disabling ".implode(",", $prod_name) ." of vendor ".$info['company'],"Lot of transactions are going in process. Kindly check and activate it manually",array('backend@mindsarray.com'),'mail');
							//$this->setMemcache($key,0);
							$this->setMemcache("disabled_$vendor_id"."_".$prod_id,1);
	                        $this->General->logData('/mnt/logs/vendor_status.txt',"Disabling : $vendor_id"."_".$prod_id);
						}
					}
					$this->General->logData('/mnt/logs/vendor_status.txt',"increment:: $key: $val");
				}
			}
		}
	}
	
	function updateTransaction($tranId,$venTranId,$status,$code,$desc,$extra=null,$opr_id=null,$vendor_id=null,$prod_id=null,$discount_comm=null){
		if($status == 'success') {
			$status = TRANS_SUCCESS;
		}
		else if($status == 'failure') {
			$status = TRANS_REVERSE;
		}
		else if($status == 'pending') {
			$status = 0;
		}
		if(!empty($vendor_id) && !empty($prod_id))$this->setProdVendorHealth($vendor_id,$prod_id,$status);
		$this->unlockTransaction($tranId);
		if(is_null($extra))
		$extra = '';
		
		$this->updateVendorActivation($tranId,$venTranId,$status,$code,$desc,$extra,$opr_id,$vendor_id,$discount_comm);
		if($status == TRANS_REVERSE){
			//$err = ($code == 5) ? 4 : (($code == 6) ? 3 : null);
			$this->reverseTransaction($tranId,1,$code);
		}
	}
	
	function checkStatus($transId,$vendor_id){
		$vendors = $this->addMemcache("transCheck$transId@$vendor_id",1,5*60);
		//$this->General->logData("/tmp/status.txt",date('Y-m-d H:i:s')."::$transId: ".json_encode($vendors));
		
		if($vendors === false){
			return false;
		}
		else {
			$this->delMemcache("transCheck$transId@$vendor_id");
			return true;
		}
		/*if(!in_array($vendor_id,$vendors)){
			return true;
		}*/
	}
	
	
	function autopullbackTransaction($transId,$vendorId){
		if(!$this->checkStatus($transId,$vendorId)){
			return;
		}
		$vendorActObj = ClassRegistry::init('VendorsActivation');

		$result = $vendorActObj->query("SELECT vendors_activations.*,retailers.mobile,shop_transactions.* FROM vendors_activations inner join shop_transactions ON (shop_transactions.ref2_id = vendors_activations.shop_transaction_id AND type = " . REVERSAL_RETAILER.") inner join retailers ON (retailers.id = retailer_id) WHERE ref_code = '$transId'");
                
                if(empty($result)){
                    return;
                }
                
		$shop_id = 	$result['0']['vendors_activations']['shop_transaction_id'];
		
		$vendorActObj->query("UPDATE vendors_activations SET vendor_id=$vendorId,prevStatus=status,status=1, complaintNo='' WHERE ref_code='$transId'");
		$vendorActObj->query("UPDATE shop_transactions SET confirm_flag=1 WHERE id=$shop_id");
		 
		$vendorActObj->query("INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$vendorId."','13','Auto Pulled back by System','success','".date("Y-m-d H:i:s")."')");
		$this->addStatus($transId,$vendorId);
		//$this->General->logData("/var/www/html/shops/status.txt",$result['0']['vendors_activations']['ref_code'] . "::pullback: "."INSERT into vendors_messages(shop_tran_id,vendor_refid,service_id,service_vendor_id,internal_error_code,response,status,timestamp) values ('".$result['0']['vendors_activations']['ref_code']."','".$result['0']['vendors_activations']['vendor_refid']."','".$result['0']['products']['service_id']."','".$result['0']['vendors_activations']['vendor_id']."','13','Pulled back by " . $_SESSION['Auth']['User']['name']."','success','".date("Y-m-d H:i:s")."')");

		//$result1 = $vendorActObj->query("SELECT ref1_id,amount FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
        
		$amt = $result['0']['shop_transactions']['amount'];
		$ret_id = $result['0']['shop_transactions']['ref1_id'];


		$bal = $this->shopBalanceUpdate($amt,'subtract',$ret_id,RETAILER);
		$this->addOpeningClosing($ret_id,RETAILER,$shop_id,$bal+$amt,$bal);
		
		$this->unlockReverseTransaction($shop_id);
		$vendorActObj->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_id AND type = " . REVERSAL_RETAILER);
	
		$vendorActObj->query("INSERT INTO trans_pullback (id,vendors_activations_id,vendor_id,status,timestamp,pullback_by,pullback_time,reported_by,date) values('','".$result['0']['vendors_activations']['id']."','".$vendorId."','1','".date('Y-m-d H:i:s')."','','".date('Y-m-d H:i:s')."','Auto-pullback','".date('Y-m-d')."')");
		$paramdata = array();
		if(empty($result['0']['vendors_activations']['param']))
		{
			$paramdata['PULLBACKTO'] ="Mobile: ".$result['0']['vendors_activations']['mobile']."\n";
		}
		else {
			$paramdata['PULLBACKTO'] ="Subscriber Id: ".$result['0']['vendors_activations']['param']."\n";
		}
		
		$paramdata['PULLED_AMOUNT'] = $amt;
		$paramdata['TRANSID'] = substr($transId,-5);
		$paramdata['AMOUNT'] = $result['0']['vendors_activations']['amount'];
		$paramdata['BALANCE'] = $bal;
		
		$MsgTemplate = $this->General->LoadApiBalance();
		$content =  $MsgTemplate['Panels_Pullback_MSG'];
		$msg = $this->General->ReplaceMultiWord($paramdata,$content);

		$this->General->sendMessage($result['0']['retailers']['mobile'],$msg,'notify');
	}
	
	function addStatus($transId,$vendor_id){
		$vendors = $this->setMemcache("transCheck$transId@$vendor_id",1,10*60);
		$this->General->logData("/mnt/logs/status.txt",date('Y-m-d H:i:s')."::$transId: 1".json_encode($vendors));
		/*if($vendors === false){
			
		}
		//$vendors[] = $vendor_id;
		$ret = $this->setMemcache("transCheck$transId",$vendors,10*60);	
		$this->General->logData("/var/www/html/shops/status.txt","$transId: ".json_encode($ret));
		*/
	}
	
	function deleteStatus($transId,$vendor_id){
		$vendors = $this->delMemcache("transCheck$transId@$vendor_id");
		$this->General->logData("/mnt/logs/status.txt",date('Y-m-d H:i:s')."delete status::$transId: 1".json_encode($vendors));
	}
	
	function lockTransactionDuplicates($prod,$number,$amount,$api_flag){
		$prod = $this->getOtherProds($prod);
		$prod = str_replace(",", "_", $prod);
		$ret = false;
		$ret1 = $this->addMemcache("dup_$number"."_".$prod."_$amount",$number,TIME_DURATION*60);
		if($ret1 !== false)$ret = true;
		
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_repeattxn VALUES (NULL,'$number','".addslashes($prod)."','$amount','$api_flag','".date('Y-m-d H:i:s',strtotime('+ '.TIME_DURATION.' minutes'))."')")){
			$ret = true;
		}*/
			
		return $ret;
	}
	
	/*function lockTransactionVendor($transId,$vendor_name,$vendor_id){
		//$ret = $this->addMemcache("txn$transId"."_$vendor_id",1,10*60);
		
		$ret1 = false;
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_transaction VALUES (NULL,'$transId','$vendor_name','$vendor_id','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
		else $ret1 = false;
		
		return $ret1;
	}*/
	
	function lockTransaction($transId){
		$ret1 = false;
		$ret = $this->addMemcache("temp$transId",1,10*60);
		if($ret !== false){
			$ret1 = true;
		}
		//else $ret1 = false;
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_txn VALUES (NULL,'$transId','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
		else $ret1 = false;*/
		
		return $ret1;
	}
	
	function lockReverseTransaction($transId){
		//$ret = $this->addMemcache("rev$transId",1,2*24*60*60);
		
		$ret1 = false;
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		if($vendorActObj->query("INSERT INTO temp_reversed VALUES (NULL,'$transId','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')")){
			$ret1 = true;
		}
			
		return $ret1;
	}
	
	function lockBankTransaction($bank,$transId){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$ret = false;
		if($vendorActObj->query("INSERT INTO bank_transactions VALUES (NULL,'$bank','".addslashes($transId)."')")){
			$ret = true;
		}
		
		return $ret;
	}
	
	function unlockReverseTransaction($transId){
		//$this->delMemcache("rev$transId");
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_reversed WHERE shoptrans_id = '$transId'");
		return true;
	}
	
	function unlockTransactionDuplicates($prod,$number,$amount){
		$prod = $this->getOtherProds($prod);
		$prod = str_replace(",", "_", $prod);
		$this->delMemcache("dup_$number"."_".$prod."_$amount");
		/*$amount = intval($amount);
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_repeattxn WHERE (number='$number' AND amount='$amount' AND prods='$prod') OR (timestamp < '".date('Y-m-d H:i:s')."')");
		*/
		return true;
	}
	
	function unlockTransaction($transId){
		$ret = $this->delMemcache("temp$transId");
		/*$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->query("DELETE FROM temp_txn WHERE ref_code='$transId'");*/
			
		return true;
	}
	
	function reversalDeclined($ref_id,$send){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$data = $vendorActObj->query("SELECT vendors_activations.id,vendors_activations.status,vendors_activations.mobile,vendors_activations.product_id,vendors_activations.retailer_id,resolve_flag FROM vendors_activations left join complaints ON (vendors_activations.id = vendor_activation_id AND resolve_flag = 0) where ref_code='".$ref_id."'");


		if($data['0']['vendors_activations']['status'] == TRANS_REVERSE_PENDING || $data['0']['complaints']['resolve_flag'] == '0') {
			$vendorActObj->query("update vendors_activations set status='".TRANS_REVERSE_DECLINE."',complaintNo='".$_SESSION['Auth']['User']['id']."' where ref_code='".$ref_id."'");
			$ret = $vendorActObj->query("SELECT retailers.mobile,vendors_activations.*,products.service_id,products.name FROM vendors_activations join retailers on ( vendors_activations.retailer_id = retailers.id) join products on (vendors_activations.product_id = products.id) where vendors_activations.ref_code='".$ref_id."'");
			$vendorActObj->query("UPDATE complaints SET resolve_flag=1,closedby='".$_SESSION['Auth']['User']['id']."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

			$msg = 'Complaint for Trans Id '.substr($ret['0']['vendors_activations']['ref_code'],-5).' is resolved';
			$msg .= "\nTransaction is successful";
			$msg .= "\nDate: " . date('j M, g:i A',strtotime($ret['0']['vendors_activations']['timestamp']));
			if($ret['0']['products']['service_id'] == 2){
				$msg .= "\nSub Id: " .  $ret['0']['vendors_activations']['param'];
                                                                                                $msg .= "\nOprId: " .  $ret['0']['vendors_activations']['operator_id'];
			}
			$msg .= "\nMob: " .  $ret['0']['vendors_activations']['mobile'];
			$msg .= "\nOpr: " .  $ret['0']['products']['name'];
                                                                        $msg .= "\nAmt: " .  $ret['0']['vendors_activations']['amount'];

			if($send == 1)
			$this->General->sendMessage($ret['0']['retailers']['mobile'],$msg,'notify');
				
				
			if($data['0']['vendors_activations']['retailer_id'] == B2C_RETAILER){
				$partnerLogs =  $vendorActObj->query("SELECT partners_log.partner_req_id FROM partners_log left join vendors_activations ON (partners_log.vendor_actv_id = vendors_activations.ref_code) WHERE vendors_activations.ref_code = '$ref_id'");

				if(empty($partnerLogs))return;
				$ip = $this->General->findVar("b2c_ip");
				$url = "http://$ip/index.php/api_new/action/actiontype/resolve_complain/api/true";
				$data = array('transaction_id'=>$partnerLogs['0']['partners_log']['partner_req_id']);
				$this->General->curl_post_async($url,$data);
			}
		}
		return;
	}
	
	function reverseTransaction($tranId,$update=null,$error=null,$group_id=null,$user_id=null){
		$filename = "reverse_transaction_".date('Ymd').".txt";
		
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		//$slaveObj = ClassRegistry::init('Slaves');
		//$vendorActObj->recursive = -1;

		$data = $vendorActObj->query("SELECT vendors_activations.*,products.service_id,products.name,vendors.update_flag,vendors.ip,vendors.port,shop_transactions.* FROM vendors_activations,products,vendors,shop_transactions WHERE vendors.id = vendor_id AND products.reverse_flag = 1 AND products.id = product_id AND ref_code = '$tranId' AND shop_transaction_id = shop_transactions.id");
		if(empty($data)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId:Either data is not present or its a cash pg transactions");
			return;
		}
		$shop_trans_id = $data['0']['vendors_activations']['shop_transaction_id'];
		
		/*$checkforApiTrans = $vendorActObj->query("Select SUM(amount) as amt  from vendors_activations inner join vendors on vendors_activations.vendor_id = vendors.id where vendors.update_flag!=1 and vendors_activations.date = '".$data[0]['vendors_activations']['date']."' AND vendors_activations.complaintNo = '".$_SESSION['Auth']['User']['id']."'");
		
		if($data[0]['vendors']['update_flag']!=1 && $checkforApiTrans[0]['amt'] >=MAX_API_REVERSAL_AMT && $group_id!=ADMIN){
			$this->General->logData("/var/log/pay1_reverse.log","You can not reverse the transction more than".MAX_API_REVERSAL_AMT);
			return;
		}*/
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: Got shoptransid as $shop_trans_id");
		$date = $data['0']['vendors_activations']['date'];
		
		if ($data['0']['vendors_activations']['status'] == TRANS_SUCCESS && $data['0']['vendors']['update_flag'] == 1 && !empty($data['0']['vendors_activations']['operator_id'])) {
			$this->General->logData("/var/log/pay1_reverse.log", "Reversal of Successful txn of modems cannot be done");
			return;
		}
		
		if($date < date('Y-m-d',strtotime('-30 days'))){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 30 days from backend admin");
			return;
		}//
		else if($date < date('Y-m-d',strtotime('-7 days')) && $date >= date('Y-m-d',strtotime('-30 days'))&& $group_id != BACKEND_ADMIN){ // backend operation expert 
			
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days from backend operation expert");
			return;
		}
		else if($date < date('Y-m-d',strtotime('-3 days')) && $date >= date('Y-m-d',strtotime('-7 days'))&& !in_array($group_id,array(BACKEND_OPERATION_EXPERT,BACKEND_ADMIN))){ // backend operation expert 
			
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days from backend operation expert");
			return;
		}
		else { // other group and cc after 30 mins.
			
			$currtime = date('Y-m-d H:i:s',strtotime('-30 minutes'));
			$timestamp = $data['0']['vendors_activations']['timestamp'];
			
			if($group_id == CUSTCARE && $timestamp > $currtime){
				$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is less than 30 minutes");
				return;
			}
		}

		/*else if($date >= date('Y-m-d',strtotime('-7 days')) || $user_id == 1){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is less than 7 days or from admin user");
		}elseif($date >= date('Y-m-d',strtotime('-30 days')) && $group_id == ADMIN){
            $this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is more than 7 days or from admin user");
        }
		else {
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: txn is returned from here, group_id = $group_id, user_id = $user_id");
			return;
		}*/
		
		if($group_id != SUPER_ADMIN && empty($update)){
			if(!$this->lockTransaction($tranId)){
				$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: transaction is locked");
				return;
			}
			$this->unlockTransaction($tranId);
		}
		
		$this->unlockTransactionDuplicates($data['0']['vendors_activations']['product_id'],!empty($data['0']['vendors_activations']['param'])?$data['0']['vendors_activations']['param']:$data['0']['vendors_activations']['mobile'],$data['0']['vendors_activations']['amount']);
		
		if(!$this->lockReverseTransaction($shop_trans_id)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: is already reversed so just changing the status only");
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."' WHERE ref_code= '".$tranId."'");
			return;
		}
		
		if($update == null && ($data['0']['vendors_activations']['status'] == TRANS_REVERSE)){
			$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: current status or previous status is reversed so cannot be reversed from here");
			return;
		}

		
		$ret_id = $data['0']['shop_transactions']['ref1_id'];
		$amount = intval($data['0']['shop_transactions']['amount']);

		if($data['0']['products']['service_id'] == 4 && $data['0']['vendors_activations']['api_flag'] != 4){
			$msg_user = "Dear User\nYour request of bill payment of Rs $amount declined from your operator. Please take your money back if already paid to your retailer\nYour pay1 txnid: $tranId";
			$this->General->sendMessage(array($data['0']['vendors_activations']['mobile']),$msg_user,'shops');
		}
		
		$allRecords = $vendorActObj->query("SELECT * FROM shop_transactions WHERE ref2_id = '$shop_trans_id'");
		$ret_amount = 0;
		foreach($allRecords as $record){
			if($record['shop_transactions']['type'] == COMMISSION_RETAILER){
				$ret_amount = $ret_amount + $record['shop_transactions']['amount'];
			}
			else if($record['shop_transactions']['type'] == TDS_RETAILER){
				$ret_amount = $ret_amount - $record['shop_transactions']['amount'];
			}
			else if($record['shop_transactions']['user_id'] == RETAILER && $record['shop_transactions']['type'] == SERVICE_CHARGE){
				$ret_amount = $ret_amount - $record['shop_transactions']['amount'];
			}
		}

		$ret_amount = $amount - $ret_amount;

		if($date != date('Y-m-d')) $type_flag = 1;
		else $type_flag = 0;
		
		if($data['0']['vendors_activations']['status'] == TRANS_SUCCESS && $data['0']['vendors']['update_flag'] == 1){
			$this->General->sendMails('Modem: Txn failed after success',$tranId,array('ashish@mindsarray.com','chirutha@mindsarray.com','backend@mindsarray.com'),'mail');
		}
		
		$this->shopTransactionUpdate(REVERSAL_RETAILER,$ret_amount,$ret_id,$shop_trans_id,$user_id,null,$type_flag);
		
		
		$vendorActObj->query("UPDATE shop_transactions SET confirm_flag = 0,type_flag=$type_flag WHERE id = $shop_trans_id");

		$bal = $this->shopBalanceUpdate($ret_amount,'add',$ret_id,RETAILER);
		$this->addOpeningClosing($ret_id,RETAILER,$shop_trans_id,$bal-$ret_amount,$bal);
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: reversed successfully");
			

		if($date <= date('Y-m-d',strtotime('-1 days')) && $data['0']['vendors']['update_flag'] == 1){
			$this->updateDeviceData($tranId,$data['0']['vendors_activations']['vendor_id'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['amount']);
		}
		
		$bal = round($bal,2);
		//inform retailer by sms
		$shop_data = $this->getShopDataById($ret_id,RETAILER);

		if(empty($user_id))$user_id = 0;
		
		$vendorActObj->query("UPDATE complaints SET resolve_flag=1,closedby='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : $tranId: updating complaints table::UPDATE complaints SET resolve_flag=1,closedby='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);
		
		$vendorActObj->query("UPDATE api_transactions SET flag=2 WHERE txn_id = '$tranId' AND vendor_id = ".$data['0']['vendors_activations']['vendor_id']);
		$this->deleteStatus($tranId,$data['0']['vendors_activations']['vendor_id']);

    	$err_code = "E013";
		
		$error = empty($error) ? 47 : $error;
		
		if($error==37){
			$err_code = 'E012';
		}
		else if($error==5){
			if($data['0']['products']['service_id'] == 1)
			$err_code = 'E008';
			else if($data['0']['products']['service_id'] == 2)
			$err_code = 'E009';
		}else if($error==6){
			$err_code = 'E010';
		}  else {
			$err_code = 'E013';
		}
		
		
		if(empty($data['0']['vendors_activations']['prevStatus'])){
			$vendorActObj->query("UPDATE vendors_activations SET prevStatus=status,status = '".TRANS_REVERSE."',code='$error',cause='".addslashes($this->errors($error))."', complaintNo='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."'  WHERE ref_code= '".$tranId."'");
		} else {
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."',code='$error',cause='".addslashes($this->errors($error))."' , complaintNo='".(empty($_SESSION['Auth']['User']['id'])? $user_id :$_SESSION['Auth']['User']['id'])."' WHERE ref_code= '".$tranId."'");
		}

		if($data['0']['vendors_activations']['api_flag'] == 4){
			//update partners log entry
			$vendorActObj->query("UPDATE partners_log SET err_code= '$err_code',description = '".$this->apiErrors($err_code)."' WHERE vendor_actv_id= '".$tranId."'");
			 
			//call status_update url of api vendors ( pay1 api client )
			//$partnerRegObj = ClassRegistry::init('Partner');
			$partnerLog = $vendorActObj->query("SELECT partners_log.id,partners_log.partner_req_id,partners.status_update_url,partners.acc_id,partners.password FROM partners,partners_log WHERE partners.id = partners_log.partner_id AND partners_log.vendor_actv_id = '$tranId'");
			$status_update_url = $partnerLog[0]['partners']['status_update_url'];
			if(!empty($status_update_url)){

				$partnerId = $partnerLog['0']['partners']['acc_id'];
				$pay1_trans_id = "2082" . sprintf('%06d', $partnerLog['0']['partners_log']['id']);
				$client_trans_id = $partnerLog['0']['partners_log']['partner_req_id'];
				$open_bal = $bal-$ret_amount;
				$close_bal = $bal;

				$hash = sha1($partnerId.$client_trans_id.$partnerLog['0']['partners']['password']); // order -> acc_id + transId + refCode + password
				
				$sdata = array('open_bal'=>$open_bal,'close_bal'=>$close_bal,'trans_id'=>$pay1_trans_id,'client_req_id'=>$client_trans_id,'status'=>'failure','err_code'=>$err_code,'description'=>$this->apiErrors($err_code),'hash_code'=>$hash,'vendor_code' => 'Signal7');
				//$this->General->curl_post_async($status_update_url, $sdata);
                                $this->General->curl_post($status_update_url, $sdata);
				
				/*$fh = fopen('/var/www/html/shops/abc.txt','a+');
				fwrite($fh,$status_update_url . " " . json_encode($sdata));
				fclose($fh);*/
			}
		}else{
			$tranId = substr($tranId,-5);
			$msg = "";
			if(!empty($error)){
				$msg .= "Reason:".$this->apiErrors($err_code)."\n";
			}
			$msg .= "Reversal of Rs.".$ret_amount." is done.\n";

			$msg .="Trans Id: $tranId\n";
            $msg .="Operator: ". $data['0']['products']['name']. "\n";
			if( in_array( $data['0']['products']['service_id'] , array(1,3,4,5))  ){
				$msg .="Mobile: " . $data['0']['vendors_activations']['mobile'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['mobile'],$data['0']['vendors_activations']['amount'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['retailer_id']);
			}
			else if($data['0']['products']['service_id'] == 2 || $data['0']['products']['service_id'] == 6){
				$msg .="Subscriber Id: " . $data['0']['vendors_activations']['param'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['param'],$data['0']['vendors_activations']['amount'],$data['0']['vendors_activations']['product_id'],$data['0']['vendors_activations']['retailer_id']);
				
				//$this->General->logData("deleteappreq.txt","mobile =>".$data['0']['vendors_activations']['param']."<br/>"."amount=>".$data['0']['vendors_activations']['amount']."</br>"."operator=>".$data['0']['vendors_activations']['product_id']."<br/>"."retailerid=>".$data['0']['vendors_activations']['retailer_id']."msg=>request deleted successfully!!!!");
			}
			$msg .="Amount: ".$data['0']['vendors_activations']['amount']."\nYour current balance is Rs.".$bal;
			$this->General->sendMessage($shop_data['mobile'],$msg,'notify');
            /*-------------------*/
            //if($data['0']['vendors_activations']['mobile'] == '9819032643'){
				$req_reverse_data = array('mobile'=>$data['0']['vendors_activations']['mobile'],'trans_id'=>$data['0']['vendors_activations']['ref_code'],'amount'=>$data['0']['vendors_activations']['amount']);
            	$this->B2cextender->manage_request_from_b2c_user($req_reverse_data,'failure');
            //}
            /*-------------------*/
		}
		
		$this->General->logData("/var/log/pay1_reverse.log", date('Y-m-d H:i:s')." : just before memcache for repeat recharge delete");
		
		$this->delMemcache("recharge_".$data['0']['vendors_activations']['retailer_id']."_".$data['0']['vendors_activations']['product_id']."_".$data['0']['vendors_activations']['mobile']."_".$data['0']['vendors_activations']['amount']);
	}
	
	function updateDeviceData($transId,$vendor_id,$product_id,$amount,$reverse_flag=true){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		$data = $vendorActObj->query("SELECT sim_num,date FROM  `vendors_transactions` WHERE  `ref_id` LIKE  '$transId' AND vendor_id = $vendor_id");
		if(!empty($data)){
			$sim_num = $data[0]['vendors_transactions']['sim_num'];
			$date = $data[0]['vendors_transactions']['date'];
			$prod_id = $this->getOtherProds($product_id);
			$str = ($reverse_flag) ? "+" : "-";
			$vendorActObj->query("UPDATE devices_data SET server_diff = server_diff $str $amount WHERE opr_id in ($prod_id) AND mobile='$sim_num' AND vendor_id = $vendor_id AND sync_date='$date'");
		}
	}
	
    function openservice_redis(){
        try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->openredis = new Predis\Client(array(
                   'host' => '107.22.176.158',
                   'password' => REDIS_PASSWORD,
                   'port' => REDIS_PORT 
			));
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->openredis = false;
		}
		return $this->openredis;
    }
    
	function modemRequest_by_redis($query,$vendor=4,$data=null,$timeout=45){
		if(empty($data)) $data = $this->getVendorInfo($vendor);
		$uuid = $vendor."_".time().rand(1,99999);
		$query .= "&vendor_id=$vendor&uuid=$uuid&reqtime=".time();
		parse_str($query,$params);
        $arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
	    
        if(isset($params['oprId'])){
        	$opr_new = isset($arr_map[$params['oprId']]) ? $arr_map[$params['oprId']] : $params['oprId'];
			$log_file = '/mnt/logs/modemRequest_via_redis_'.$vendor.'_'.$opr_new.'_'.date('Y-m-d').'.log';
        }
        else {
        	$log_file = '/mnt/logs/modemRequest_via_redis_'.$vendor.'_'.date('Y-m-d').'.log';
        }
		//$logger = $this->General->dumpLog('modemRequest_by_redis', 'modemRequest_via_redis_'.$vendor.'_'.$opr_new);
        //$logger->info("Received param : data after getting from memcach : ".$query." | ".$vendor." | ".json_encode($data)." | ". $timeout );
        
		file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after getting from memcach  :".json_encode($data), FILE_APPEND | LOCK_EX);
        
		if(empty($data['machine_id'])){
			$data = $this->setVendorInfo($vendor);
            //$logger->info("$vendor machine Id after setting in memcach : ".json_encode($data));
			file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after setting in memcach  :".json_encode($data), FILE_APPEND | LOCK_EX);
			if(empty($data['machine_id'])){
				$vendorActObj = ClassRegistry::init('Slaves');
				$data = $vendorActObj->query("SELECT * FROM vendors WHERE id = $vendor");
				$data = $data[0]['vendors'];
                //$logger->info("$vendor machine Id after setting in memcach again : ".json_encode($data));
				file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." : $vendor machine Id after setting in memcach again  :".json_encode($data), FILE_APPEND | LOCK_EX);
			}
		}
		
		$errno = '';
        $errstr = '';        
        
        $vendor_q = $vendor."_".$data['machine_id'];
        $response = "";
        $max_limit = 30;
        $open_redis = $this->openservice_redis();
        
        if($open_redis == FALSE){
            file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." error in redis connection ", FILE_APPEND | LOCK_EX);
            //$logger->error("error in redis connection");
            return array('status'=>'failure','errno'=>515,'error'=>'error in redis connection');
        }
        //$logger->error("error in redis connection");
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." checking queue length ", FILE_APPEND | LOCK_EX);
        
        $vendor_q_lenght = $open_redis->llen($vendor_q);
        //$logger->info("comparing vendor Q len with max : ".$vender_q_lnght." | ".$max_limit);
        
        if($vendor_q_lenght > $max_limit){
            $this->unHealthyVendor($vendor,$max_limit);
            return array('status'=>'failure','errno'=>515,'error'=>'max thresh limit reached');
        }
        
            
        if($params['query'] == 'recharge'){
	        $key_vendor_opr = "queue_".$vendor."_".$opr_new;
	        $vendor_q_opr = "Trans_". $vendor."_".$opr_new;
            
            //$logger->info("::queue test::".$params['transId']."::$key_vendor_opr::$vendor_q_opr::".var_dump($open_redis->exists($key_vendor_opr))."::queue size:".$open_redis->hlen($vendor_q_opr)."::max length:".$open_redis->get($key_vendor_opr));
	        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")."::queue test::".$params['transId']."::$key_vendor_opr::$vendor_q_opr::".var_dump($open_redis->exists($key_vendor_opr))."::queue size:".$open_redis->hlen($vendor_q_opr)."::max length:".$open_redis->get($key_vendor_opr), FILE_APPEND | LOCK_EX);
        
	        if($open_redis->exists($key_vendor_opr)){
	        	if($open_redis->hlen($vendor_q_opr) >= $open_redis->get($key_vendor_opr)){
	        		return array('status'=>'failure','errno'=>515,'error'=>'Dropped due to lot of pending requests - Server');
	        	}
	        	else {
	        		$open_redis->hset($vendor_q_opr,$params['transId'],time()+120);
	        	}
	        }
	        else{
	        	return array('status'=>'failure','errno'=>515,'error'=>'No active sim/no balance in sims - Server');
	        }
        }
        /** **/

        //$logger->info(" inserting data in Queue : $vendor_q | data : ". $query);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." inserting data in Queue : $vendor_q | data : ". $query, FILE_APPEND | LOCK_EX);
        
        $open_redis->set($uuid,"1");
        $open_redis->expire($uuid,20);       
        $open_redis->lpush($vendor_q,$query);
        
        //$logger->info(" waiting for response from hash : dynamic_service : uuid ". $uuid);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." waiting for response " , FILE_APPEND | LOCK_EX);
        $response_status = $open_redis->hexists('dynamic_service',$uuid);
        $counter = 1;
		while(!$response_status){
            if($counter > $timeout*5){                
                $this->unHealthyVendor($vendor);
                break;
            }
            usleep(200000);
            $response_status = $open_redis->hexists('dynamic_service',$uuid);                      
            $counter++;
        }
        if($response_status){
            $response = $open_redis->hget('dynamic_service',$uuid);
            
            //$logger->info("removing key from hash | key : ". $uuid);
            file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." | removing key from hash | key : ". $uuid, FILE_APPEND | LOCK_EX);
            $open_redis->hdel('dynamic_service',$uuid);
        }
        //$logger->info(" Response : ". $response);
        file_put_contents($log_file, "\n".Date("Y-m-d H:i:s -- ")." | Response : ". $response, FILE_APPEND | LOCK_EX);
        return array('status'=>'success','data'=>$response);
	}
	
    function modemRequest($query,$vendor=4,$data=null,$timeout=45){
    	//if(in_array($vendor,array(4,10,31)))
         return $this->modemRequest_by_redis($query,$vendor,$data,$timeout);
    	
		if(empty($data)) $data = $this->getVendorInfo($vendor);
		
		$ip = $data['ip'];
		$port = $data['port'];
		if($data['bridge_flag'] == 1 && !empty($data['bridge_ip'])){
			$ip = $data['bridge_ip'];
		}
		
		$url = "http://$ip:$port/start.php";
		$errno = '';
        $errstr = '';
		$query .= "&vendor_id=$vendor";
        $Rec_Data = $this->General->curl_post($url,$query,'POST',$timeout);
		
		if(!$Rec_Data['success']){
			$this->unHealthyVendor($vendor);	
			if($Rec_Data['timeout']){
				return array('status'=>'failure','errno'=>$errno,'error'=>$errstr);
			}
		}
        
		return array('status'=>'success','data'=>$Rec_Data['output']);
	}
    
	function healthyVendor($vendor,$ip=null){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$data1 = $this->getVendorInfo($vendor);
		$name = $data1['company'];
		
		$data = array();
		
		$set = false;
		$ip_flag = false;
		if(!empty($ip) && $ip != $data1['ip']){
			$data['ip'] = $ip;
			$data['update_time'] = date('Y-m-d H:i:s');
			$set = true;
			$ip_flag = true;
		}
		
		if($data1['active_flag'] == 0){
			$data['active_flag'] = 1;
			$data['health_factor'] = 0;
			$set = true;
            $this->General->logData("/mnt/logs/log.txt",date('Y-m-d H:i:s').": Enabling $name Vendor : $vendor");
			$this->General->sendMails("(SOS)Enabling $name Vendor : $vendor","Got the connectivity. So enabling the vendor",array('backend@mindsarray.com','chetan@mindsarray.com'),'mail');			
		}
		
		if($data1['health_factor'] > 5){
			$data['health_factor'] = 0;
			$set = true;
		}
		
		if($set){
			$this->setVendorInfo($vendor,$data);	
			if($ip_flag)$this->setVendors();
			$this->setInactiveVendors();
		}
	}
	
	
	function unHealthyVendor($vendor,$health_factor=null){
		if(empty($vendor))return;
		
		$data = $this->getVendorInfo($vendor);
		$name = $data['company'];
		
		$set = false;
		$data1 = array();
		if($data['active_flag'] ==1){
			if(is_null($health_factor))$data1['health_factor'] = $data['health_factor'] + 1;
			else $data1['health_factor'] = $health_factor;
			
			if($data['health_factor'] >= 20){
				$data1['active_flag'] = 0;
				$this->General->sendMails("(SOS)Disabling $name Vendor : $vendor","Connectivity issues. It will be activated once we get the connectivity",array('backend@mindsarray.com','chetan@mindsarray.com'),'mail');
				
				$this->General->logData("/mnt/logs/log.txt",date('Y-m-d H:i:s').": Disabling $name Vendor : $vendor");
				$set = true;
			}
			$this->setVendorInfo($vendor,$data1);	
			if($set)$this->setInactiveVendors();	
		}
	}
	
	
	function earnings($params){
		$pageNo = empty($params['page_no'])?0:$params['page_no'];
		$itemsPerPage = empty($params['items_per_page'])?0:$params['items_per_page'];

		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if(date('Y-m-d',  strtotime($date_to)) < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));
			}
			else $next_week = "";
		}
		 
		if($itemsPerPage <= 0 || $pageNo <= 0){
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
			//$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date";
			$query = "SELECT trim(sale) as amount, trim(earning) as income,trim(date) as date FROM retailers_logs WHERE retailer_id = ".$_SESSION['Auth']['id'] . " AND date >= '$date_from' AND date <= '$date_to'";
		}else{
			$ll = $itemsPerPage * ( $pageNo - 1 ) ;//+ 1; // lower limit
			$ul = $itemsPerPage * $pageNo ;// upper limit
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date limit $ll , $ul ";
			//$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date limit $ll , $ul";
			$query = "SELECT trim(sale) as amount, trim(earning) as income,trim(date) as date FROM retailers_logs WHERE retailer_id = ".$_SESSION['Auth']['id'] . " AND date >= '$date_from' AND date <= '$date_to' limit $ll , $ul";
		}
		$vendorActObj = ClassRegistry::init('Slaves');
		//$vendorActObj->recursive = -1;

		$data = $vendorActObj->query($query);
		$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date = '".date('Y-m-d')."'";
		$today = $vendorActObj->query($query);
		//$today = $vendorActObj->query("SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.") WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date = '".date('Y-m-d')."'");
		//print_r($data);
		return array($data,$today['0'],$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function topups($params){
		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);	
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if($date_to < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));	
			}
			else $next_week = "";	
		}
		
		$query = "SELECT sum(st1.amount) as amount, date(st1.timestamp) as day FROM shop_transactions as st1 WHERE st1.confirm_flag != 1 AND st1.type = ".DIST_RETL_BALANCE_TRANSFER." AND st1.ref2_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
		$vendorActObj = ClassRegistry::init('Slaves');
		$vendorActObj->recursive = -1;
		
		$data = $vendorActObj->query($query);
		return array($data,$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function verifyParams($params,$mapping){
		$ret = true;
		$msg = "";
		foreach($mapping['allParams']['param'] as $param){
			$field = trim($param['field']);
			if(!isset($params[$field])){
				$ret = false;
				$msg = $field . " not entered";
				break;
			}
			else if(strlen($params[$field]) >  $param['length'] || empty($params[$field])){
				$msg = $field . ": Enter valid value";
				$ret = false;
				break;
			}
			else if($field == 'Mobile' || $field == 'PNR' || $field == 'Amount'){
				if(strlen($params[$field]) != $param['length']){
					$msg = $field . ": Enter valid value";
					$ret = false;
					break;
				}
			}
		}
		if($ret){
			return array('status'=>'success');
		}
		else {
			return array('status'=>'failure','description' => $msg);	
		}
	}
	
	function mailToSuperDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$superdist_shop = $this->getShopDataById($dist_shop['parent_id'],SUPER_DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($superdist_shop['email']));
	}
	
	function mailToDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($dist_shop['email']));
	}
	
	/*function calculateCommission($id,$group_id,$product,$amount,$slab){
		//$shop = $this->getShopDataById($id,$group_id);
		//$slab = $shop['slab_id'];
		$commission = $this->getMemcache("commission_".$product."_".$slab);
		if($commission === false){
			$slabObj = ClassRegistry::init('SlabsUser');
			$prodData =  $slabObj->query("SELECT percent FROM slabs_products INNER JOIN products ON (product_id = products.id) WHERE slab_id = $slab AND product_id = $product");
			if(!empty($prodData))
				$commission = $amount*$prodData['0']['slabs_products']['percent']/100;
			else $commission = 0;
			$this->setMemcache("commission_".$product."_".$slab,$commission,24*60*60);
		}
		
		return $commission;
	}*/
	
	function getCommissionPercent($slab_id,$product){
		$commission = $this->getMemcache("commission_".$product."_".$slab_id);
		if($commission === false){
			$slabObj = ClassRegistry::init('Slaves');
			$prodData =  $slabObj->query("SELECT percent,service_charge,service_tax FROM slabs_products WHERE slab_id = $slab_id AND product_id = $product");
	        if(isset($prodData) && !empty($prodData)){
				$commission = $prodData['0']['slabs_products'];
	        }
	        else $commission = array();
	        $this->setMemcache("commission_".$product."_".$slab_id,$commission,24*60*60);
		}
		
		return $commission;
	}
	
	function getAllCommissions($id,$group_id,$slab,$service=null){
		$qryStr = '';
		if(!is_null($service)) $qryStr = ' and products.service_id = '.$service.' ';
		$slabObj = ClassRegistry::init('SlabsUser');
		$table = array();
		/*if($group_id == SUPER_DISTRIBUTOR){
			$slab = SDIST_SLAB;			
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['SD'] = $prodData;
		}
		
		if($group_id <= DISTRIBUTOR){
			$slab = DIST_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['D'] = $prodData;
		}*/
		
		if($group_id <= RETAILER){
			//$slab = RET_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['R'] = $prodData;
		}
		
		return $table;
	}
	
	function calculateTDS($commission){
		return ($commission*TDS_PERCENT/100);
	}
	
	function shopBalanceUpdate($price,$type,$id,$group_id,$dataSource=null){
            
            
		$userObj = is_null($dataSource)?ClassRegistry::init('User'):$dataSource;
		
		if($group_id == SUPER_DISTRIBUTOR){
			$table = 'super_distributors';
		}
		else if($group_id == DISTRIBUTOR){
			$table = 'distributors';
		}
		else if($group_id == RETAILER){
			$table = 'retailers';
		}
		
		if($type == 'subtract'){
			$userObj->query("UPDATE $table SET balance = balance - $price WHERE id = $id");	
		}
		else if($type == 'add'){
			$userObj->query("UPDATE $table SET balance = balance + $price WHERE id = $id");
		}
		
		     $qry = "SELECT ".$table.".balance,users.mobile FROM $table left join users ON (users.id = user_id) WHERE $table".".id = $id";
		$bal = 	$userObj->query($qry);
                
		$final_bal = sprintf('%.2f', $bal['0'][$table]['balance']);
                                     $this->General->logData('/mnt/logs/TranQuery'.date('Y-m-d').'.log',"Retailer Final Bal :  {$final_bal}",FILE_APPEND | LOCK_EX); 
		if($final_bal < 0){
			$this->General->logData('/mnt/logs/negative_bal.txt',"$table ID : $id | Final balance : $final_bal | Mobile number: ".$bal['0']['users']['mobile']);
			$this->General->sendMails("Alert : Negative Balance","$table ID : $id <br/> Final balance : $final_bal <br/> Mobile number: ".$bal['0']['users']['mobile'],array('nandan@mindsarray.com','ashish@mindsarray.com'),'mail');
			$this->General->sendMessage('9819032643,9820595052,9967054833,9004895333',"Negative balance Alert, $table ID : $id | Final balance : $final_bal | Mobile number: ".$bal['0']['users']['mobile'],'shops');
		}
		return sprintf('%.2f', $bal['0'][$table]['balance']);
	}
	
	function getNewInvoice($shop_id,$from_id,$group_id,$invoice_type){
		$invoiceObj = ClassRegistry::init('Invoice');
		$this->data['Invoice']['ref_id'] = $shop_id;
		$this->data['Invoice']['from_id'] = $from_id;
		$this->data['Invoice']['group_id'] = $group_id;
		$this->data['Invoice']['invoice_type'] =  $invoice_type;
		$this->data['Invoice']['timestamp'] =  date('Y-m-d H:i:s');

		$invoiceObj->create();
		$invoiceObj->save($this->data);

		return $invoiceObj->id;
	}
	
	function addToInvoice($invoice_id,$trans_id){
		$invoiceObj = ClassRegistry::init('Invoice');
		$invoiceObj->query("INSERT INTO invoices_transactions (invoice_id,shoptransaction_id,timestamp) VALUES ($invoice_id,$trans_id,'".date('Y-m-d H:i:s')."')");
	}
	
	/*function getInvoiceAmount($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		$invoiceObj->recursive = -1;
		$invoice = $invoiceObj->findById($invoice_id);
		$id = $invoice['Invoice']['ref_id'];
		$group_id = $invoice['Invoice']['group_id'];
		$invoice_type = $invoice['Invoice']['invoice_type'];
		$data['date'] = date('Y-m-d', strtotime($invoice['Invoice']['timestamp']));
		
		$shop = $this->getShopDataById($id,$group_id);
		if($group_id != SUPER_DISTRIBUTOR){
			$parent = $shop['parent_id'];
			if($group_id == DISTRIBUTOR){
				$parentShop = $this->getShopDataById($parent,SUPER_DISTRIBUTOR);
			}
			else if($group_id == RETAILER){
				$parentShop = $this->getShopDataById($parent,DISTRIBUTOR);
			}
			$tds = $parentShop['tds_flag'];
		}
		else {
			$tds = 1;
		}
		
		$Totcommission = 0;
		$Totamount = 0;
		
		$transactions = $invoiceObj->query("SELECT ref2_id,shop_transactions.id FROM invoices_transactions,shop_transactions WHERE invoice_id = $invoice_id AND shop_transactions.id = invoices_transactions.shoptransaction_id");	
		if($invoice_type == DISTRIBUTOR_ACTIVATION){
			$coupons = array();
			foreach($transactions as $transaction){
				$coupons = array_merge($coupons,explode(",",$transaction['shop_transactions']['ref2_id']));
			}
			$couponObj = ClassRegistry::init('Coupon');
			$couponObj->recursive = -1;
			$prodData = $couponObj->find('all',array('fields' => array('GROUP_CONCAT(Coupon.serialNumber) as serials','COUNT(Coupon.id) as quantity','Product.id','Product.name','Product.price'), 'conditions' => array('Coupon.id in (' .implode(",",$coupons). ')' ),
					'joins' => array(
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Coupon.product_id = Product.id')
						)
					),
					'group' => array('Coupon.product_id')
			));
			foreach($prodData as $prod){
				$Totamount += $prod['0']['quantity']*$prod['Product']['price'];
				$product = array();
				$product[$prod['Product']['id']] = $prod['0']['quantity'];
				$Totcommission += $this->calculateCommission($id,$group_id,$product,$date);
			}
		}
		else {
			$products = array();
			$prodids = array();
			foreach($transactions as $transaction){
				$prod_id = $transaction['shop_transactions']['ref2_id'];
				$transObj = ClassRegistry::init('ShopTransaction');
				$transObj->recursive = -1;
			
				if($group_id == RETAILER){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_RETAILER)));
				}
				else if($group_id == DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_DISTRIBUTOR)));
				}
				else if($group_id == SUPER_DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_SUPERDISTRIBUTOR)));
				}
				$commission = $commission['ShopTransaction']['amount'];
				
				if(!isset($products[$prod_id])){
					$products[$prod_id]['quantity'] = 1;
					$products[$prod_id]['commission'] = $commission;
					$prodids[] = $prod_id;
				}
				else {
					$products[$prod_id]['quantity'] = $products[$prod_id]['quantity'] + 1;
					$products[$prod_id]['commission'] = $products[$prod_id]['commission'] + $commission;
				}
			}
			
			$prodObj = ClassRegistry::init('Product');
			$prodObj->recursive = -1;
			$prodData = $prodObj->find('all',array('fields' => array('Product.id','Product.name','Product.price'), 'conditions' => array('Product.id in (' .implode(",",$prodids). ')' )));	
			foreach($prodData as $prod){
				$Totamount += $products[$prod['Product']['id']]['quantity'] * $prod['Product']['price'];
				$Totcommission += $products[$prod['Product']['id']]['commission'];
			}
		}
		if($tds == 1){
			 $tds = $this->calculateTDS($Totcommission);
			 $Totcommission -= $tds;
		}
		
		$net = sprintf('%.2f',$Totamount - $Totcommission);
		return round($net);
	}*/
	
	function getOutstandingBalance($id,$type){
		$rcptObj = ClassRegistry::init('Receipt');
		$data = $rcptObj->find('all',array('fields' => array('min(Receipt.os_amount) as os'), 'conditions' => array('Receipt.receipt_ref_id' => $id,'Receipt.receipt_type' => $type)));
			
		if(empty($data['0']['0']['os'])){
			if($type == RECEIPT_INVOICE){
				$invObj = ClassRegistry::init('Invoice');
				$amount = $invObj->findById($id);
				$amount = $amount['Invoice']['amount'];
			}
			else if($type == RECEIPT_TOPUP){
				$transObj = ClassRegistry::init('ShopTransaction');
				$amount = $transObj->findById($id);
				$amount = $amount['ShopTransaction']['amount'];
			}
		}
		else {
			$amount = $data['0']['0']['os'];
		}
		return $amount;
	}
	
	function generateInvoice($id,$group_id,$type,$date=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		$last_trans = $transObj->query("SELECT max(shoptransaction_id) as trans_id FROM invoices_transactions WHERE invoice_id IN (SELECT max(id) FROM invoices WHERE ref_id=$id AND group_id = $group_id AND invoice_type = $type)");
		$trans_id = $last_trans['0']['0']['trans_id'];
		
		if($group_id == RETAILER && $type == RETAILER_ACTIVATION)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == DISTRIBUTOR && $type == RETAILER_ACTIVATION)
		{	
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_DISTRIBUTOR;
		}
		else if($group_id == DISTRIBUTOR && $type == DISTRIBUTOR_ACTIVATION){
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == SUPER_DISTRIBUTOR)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_SUPERDISTRIBUTOR;
		}
		
		if(!empty($trans_id)){
			$query .= " AND st1.id > $trans_id";
		}
		if($date != null){
			$query .= " AND Date(st1.timestamp) = '$date'";
		}
		$data = $transObj->query($query);
		if(!empty($data)){
			if($group_id == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($group_id == DISTRIBUTOR || $group_id == RETAILER){
				$sData = $this->getShopDataById($id,$group_id);
				$parent = $sData['parent_id'];
			}
			$new_invoice = $this->getNewInvoice($id,$parent,$group_id,$type);
			foreach($data as $trans){
				$this->addToInvoice($new_invoice,$trans['st1']['id']);
			}
			$amount = $this->getInvoiceAmount($new_invoice,date('Y-m-d'));
			$number = $this->getInvoiceNumber($new_invoice,date('Y-m-d'));
			$transObj->query("UPDATE invoices SET amount = $amount,invoice_number='$number' WHERE id = $new_invoice");
		}
	}
	
	
	
	
	function getShopDataById1($id,$group_id){
		$info = $this->getMemcache("shop".$id."_".$group_id);
		if($info === false){
			if($group_id == SUPER_DISTRIBUTOR){
				$userObj = ClassRegistry::init('SuperDistributor');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['SuperDistributor'];
			}
			else if($group_id == DISTRIBUTOR){
				$userObj = ClassRegistry::init('Distributor');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['Distributor'];
			}
			else if($group_id == RETAILER){
				$userObj = ClassRegistry::init('Retailer');
				$userObj->recursive = -1;
				$bal = $userObj->query("select * from retailers 
						left join unverified_retailers ur on ur.retailer_id = retailers.id
						where retailers.id = $id");
				
				foreach($bal[0]['ur'] as $key => $row){
					if(!in_array($key, array('id')))
						$bal[0]['retailers'][$key] = $bal[0]['ur'][$key];
				}
				
				$info = $bal['0']['retailers'];
			}else if($group_id == RELATIONSHIP_MANAGER){
				$userObj = ClassRegistry::init('Rm');
				$userObj->recursive = -1;
				$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
				$info = $bal['Rm'];
			}
			
			$this->setMemcache("shop".$id."_".$group_id,$info,60*60*3);
		}
		return $info;
	}
	
	function getBalance($id,$group_id){
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from super_distributors where id=$id");
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			if(!empty($bal))return $bal['0']['super_distributors']['balance'];
			else return 0;
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from distributors where id=$id");
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			if(!empty($bal))return $bal['0']['distributors']['balance'];
			else return 0;
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from retailers where id=$id");
			if(!empty($bal))return $bal['0']['retailers']['balance'];
			else return 0;
		}
		else if($group_id == SALESMAN){
			$userObj = ClassRegistry::init('Salesman');
			//$userObj->recursive = -1;
			$bal = $userObj->query("select balance from salesmen where id=$id");
			if(!empty($bal))return $bal['0']['salesmen']['balance'];
			else return 0;
		}
	}
	
	function getShopDataById($id,$group_id){
		
		if($group_id == SUPER_DISTRIBUTOR){
                                        	$userObj = ClassRegistry::init('SuperDistributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['SuperDistributor'];
                                     }
		else if($group_id == DISTRIBUTOR){
                   		$userObj = ClassRegistry::init('Distributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Distributor'];
                   	}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			
			$bal = $userObj->query("select * from retailers
					left join unverified_retailers ur on ur.retailer_id = retailers.id
					where retailers.id = $id");
			
			foreach($bal[0]['ur'] as $key => $row){
				if(!in_array($key, array('id')))
					$bal[0]['retailers'][$key] = $bal[0]['ur'][$key];
			}
			
			return $bal['0']['retailers'];
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			//return $bal['Retailer'];
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Rm'];
                   	}
	}
	
	/*function disabledApps($uid){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			$dis = $userObj->query('select service_id from services_disabled where user_id = '.$uid);
			$str = '';
			foreach($dis as $d)
			$str .= $d['services_disabled']['service_id'].",";
			
			if($str == '')return $str;
			else return substr($str,0,-1);
	}*/
	
	function shortSerials($serials){
		$serials = explode(",",$serials);
		sort($serials);
		$serial_array = array();
		
		$min = $serials[0];
		$last = $serials[0];
		$serial_array[$min] = 1;
		foreach($serials as $serial){
			if($serial == $last + 1){
				$serial_array[$min] = $serial_array[$min] + 1;
				$last = $serial;
			}
			else if($serial != $min){
				$min = $serial;
				$last = $serial;
				$serial_array[$min] = 1;
			}
		}
		$strings = array();
		foreach($serial_array as $key=>$value){
			$string = $key;
			if($value > 1){
				$string .= "-" . ($key + $value - 1);
			}
			$strings[] = $string;
		}
		return implode(", ", $strings);
	}
	
	/*function getInvoiceNumber($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		$data = $invoiceObj->findById($invoice_id);
		if($data['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == RETAILER){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		$number1 = $ret_to['0']['0']['counts'] + 1;
		return "INV-" . $suffix1 . "/" . $suffix2 . "/" . date('Y',strtotime($date)). "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}
	
	function getTopUpReceiptNumber($transaction_id){
		$transObj = ClassRegistry::init('ShopTransaction');
		$data = $transObj->findById($transaction_id);
		if($data['ShopTransaction']['type'] == ADMIN_TRANSFER){
			$suffix1 = "ST";
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],SUPER_DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);	
		}
		else if($data['ShopTransaction']['type'] == SDIST_DIST_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['ShopTransaction']['type'] == DIST_RETL_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],RETAILER);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		$ret = $transObj->query("SELECT count(*) as counts FROM shop_transactions WHERE type = ".$data['ShopTransaction']['type']." and ref1_id = ".$data['ShopTransaction']['ref1_id']." and id < $transaction_id");
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "TP-". $suffix1 . "/". $suffix2 . "/" . sprintf('%06d', $number);
	}
	
	function getCreditDebitNumber($to_id,$from_id,$to_groupid,$type,$id=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		$query = "";
		if($id != null){
			$query = " AND id < $id";
		}
		if(empty($from_id)) $from = " is null";
		else $from = " = $from_id";
		
		$ret = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE to_groupid = $to_groupid AND from_id $from AND to_id = $to_id AND type=$type $query");
		$retAll = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE from_id $from AND type=$type $query");
		
		$shop = $this->getShopDataById($to_id,$to_groupid);
		if(!empty($from_id)){
			$parent_shop = $this->getShopDataById($from_id,$to_groupid-1);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		if(isset($shop['company']))
			$comp = explode(" ",$shop['company']);
		else 
			$comp = explode(" ",$shop['shopname']);

		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $retAll['0']['0']['counts'] + 1;
		$number1 = $ret['0']['0']['counts'] + 1;
		if($type == 0) $suffix0 = "CN";
		else if($type == 1) $suffix0 = "DN";
		return "$suffix0-" . $suffix1 . "/" . $suffix2 . "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}*/
	
	function getOtherProds($prodId){
		if($prodId == 3 || $prodId == 34) $prodId1 = "3,34";
		else if($prodId == 7 || $prodId == 8) $prodId1 = "7,8";
		else if($prodId == 9 || $prodId == 27 || $prodId == 10) $prodId1 = "9,10,27";
		else if($prodId == 11 || $prodId == 29) $prodId1 = "11,29";
		else if($prodId == 12 || $prodId == 28) $prodId1 = "12,28";
		else if($prodId == 30 || $prodId == 31) $prodId1 = "30,31";
		else $prodId1 = $prodId;
		
		return $prodId1;
	}
	
	function getParentProd($prodId){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3');
	    if(isset($arr_map[$prodId]))return $arr_map[$prodId];
	    else return $prodId;
	}
	
	function getProdInfo($prodId){
		$info = $this->getMemcache("prod$prodId");
		if($info === false){
			$info = $this->setProdInfo($prodId);
		}
		
		return $info;
	}
	
	function getVendorInfo($vendorId){
		$info = $this->getMemcache("vendor$vendorId");
		if($info === false){
			$info = $this->setVendorInfo($vendorId);
		}
		
		return $info;
	}
	
	function setVendorInfo($vendorId,$info=null){
		$invoiceObj = ClassRegistry::init('Invoice');
			
		$info1 = $invoiceObj->query("SELECT * FROM vendors WHERE id = $vendorId");
		$info1 = $info1['0']['vendors'];
        if(empty($info1)){
            return FALSE;
        }
		if(!empty($info)){
			$arr = array();
			foreach($info as $k => $v){
				if($k == 'machine_id') continue;
				$arr[] = "$k='$v'";
				$info1[$k] = $v;
			}
			$invoiceObj->query("UPDATE vendors SET ".implode(",",$arr)." WHERE id = $vendorId");
		}
		$this->setMemcache("vendor$vendorId",$info1,30*60);
		return $info1;
	}
	
	function getVMNList($src=null){
               //$ret  = array();
               if( $src == 'fromLogin'){//to send vmn list with loginResponse                   
                   $ret = $this->General->findVar('VMNList');
                   $ret = json_decode($ret,true);
                   $res = array();
                    if(count($ret)>0){
                   foreach ($ret as $key=>$value) {
                       array_push($res,$value['no']);
                   }
                   $ret = $res;
                    }
               }else{
                   $ret = $this->General->findVar('VMNList');
                   $ret = json_decode($ret,true);
               }
              return $ret;
	}
	
	/*
	function setProdInfo($prodId){
		$invoiceObj = ClassRegistry::init('Invoice');
		$id = $invoiceObj->query("SELECT services.id,services.name,vendors_commissions.vendor_id, vendors_commissions.discount_commission, products.circle_yes, products.circle_no, products.automate_flag, vendors.shortForm, vendors.update_flag, products.price, products.oprDown, products.down_note , products.name,products.min,products.max,products.invalid,products.blocked_slabs,vendors_commissions.circles_yes,vendors_commissions.circle, vendors_commissions.circles_no FROM products left join vendors_commissions ON (vendors_commissions.product_id= products.id AND vendors_commissions.oprDown = 0) left join vendors on (vendors_commissions.vendor_id=vendors.id) join services ON (products.service_id = services.id) WHERE products.id=$prodId order by vendors_commissions.active desc,discount_commission desc");
		$info = array();
		$info['vendors'] = array();
		$vendor_ids = array();
		$vendors_n = array();
		
		foreach($id as $res){
			$info['service_id'] = $res['services']['id'];
			$info['service_name'] = $res['services']['name'];
			$info['min'] = $res['products']['min'];
			$info['max'] = $res['products']['max'];
			$info['automate'] = $res['products']['automate_flag'];
			$info['price'] = $res['products']['price'];
			$info['invalid'] = $res['products']['invalid'];
			$info['name'] = $res['products']['name'];
			$info['oprDown'] = $res['products']['oprDown'];
			$info['down_note'] = $res['products']['down_note'];
			$info['circles_yes'] = strtoupper(trim($res['products']['circle_yes']));
			$info['circles_no'] = strtoupper(trim($res['products']['circle_no']));
			$info['blocked_slabs'] = explode(",",trim($res['products']['blocked_slabs']));
			if(!empty($res['vendors_commissions']['vendor_id'])){
				$info['vendors'][] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);	
				$vendor_ids[] = $res['vendors_commissions']['vendor_id'];
				$vendors_n[$res['vendors_commissions']['vendor_id']] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);
			}
		}
		
		
		if($info['automate'] == 1){
			$opr_ids = $this->getOtherProds($prodId);
			//$vendors_data = $invoiceObj->query("SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having sims > 0");
			
			$vendors_data = $invoiceObj->query("select vendor_id,sum(planned_sale) as planned_sale,sum(base_amount) as base_amount,sum(totalsale) as totalsale,sum(sims) as sims from (SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)) as t group by t.vendor_id");
			$priority_1 = array();
			$priority_2 = array();
			$cut_off = array();
			foreach ($vendors_data as $vendor){
				if(!in_array($vendor['t']['vendor_id'],$vendor_ids))continue;
				
				$r2_sale = $vendor['0']['base_amount'] - $vendor['0']['totalsale'];
				$r1_sale = $vendor['0']['planned_sale'] - $vendor['0']['totalsale'];
				if($vendor['0']['sims'] > 0 && $r1_sale > 0){
					$weight = $r1_sale/$vendor['0']['sims'];
					$priority_1[$weight] = $vendor['t']['vendor_id'];
				}
				else if($vendor['0']['sims'] > 0 && $r2_sale > 0){
					$weight = $r2_sale/$vendor['0']['sims'];
					$priority_2[$weight] = $vendor['t']['vendor_id'];
				}
				
				if($r1_sale < 0 && $r2_sale < 0){
					$cut_off[] = $vendor['t']['vendor_id'];
				}
			}
			
			//get other vendors as well
			$vendors_data = $invoiceObj->query("SELECT vendors.id,base_amount,planned_sale FROM `inv_planning_sheet` as ips inner join inv_supplier_operator as iso ON (ips.supplier_operator_id = iso.id) inner join inv_supplier_vendor_mapping as isvm ON (isvm.supplier_id = iso.supplier_id) inner join vendors ON (vendors.id = isvm.vendor_id) WHERE vendors.update_flag = 0 AND operator_id in ($opr_ids)");
			
			foreach($vendors_data as $vd){
				if(!in_array($vd['vendors']['id'],$vendor_ids))continue;

				$prod = $this->getParentProd($prodId);
                
				$cap_per_min = $this->getMemcache("cap_api_".$prod."_".$vd['vendors']['id']);
                
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prod:: ".$vd['vendors']['id']." ::cap_per_min $cap_per_min :: "."cap_api_".$prod."_".$vd['vendors']['id']);
            
				if($cap_per_min !== false && $cap_per_min > 0){
					//$sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
                    $sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
					$r2_sale = $vd['ips']['base_amount'] - $sale;
					$r1_sale = $vd['ips']['planned_sale'] - $sale;
	
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: ".$vd['vendors']['id']." ::r1_sale $r1_sale :: r2_sale $r2_sale :: $cap_per_min");
            
                    
					if($r1_sale > 0){
						$weight = $r1_sale/$cap_per_min;	
						$priority_1[$weight] = $vd['vendors']['id'];
					}
					else if($r2_sale > 0){
						$weight = $r2_sale/$cap_per_min;	
						$priority_2[$weight] = $vd['vendors']['id'];
					}
					
					if($r1_sale < 0 && $r2_sale < 0){
						$cut_off[] =$vd['vendors']['id'];
					}
				}
				
			}
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			krsort($priority_1);
			krsort($priority_2);
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: after sort :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: cutoff vendors :: $prodId:: ".  json_encode($cut_off));
            
			$info['vendors'] = array();
			foreach($priority_1 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			foreach($priority_2 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			
			foreach($vendors_n as $k => $v){
				if(in_array($k,$final))continue;
				if(in_array($k,$cut_off))continue;
				$info['vendors'][] = $v;
			}
            
            $this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: final :: $prodId :: ". json_encode($info['vendors']));
		}
        
		$this->setMemcache("prod$prodId",$info,5*60);
		return $info;
	}*/
        
	function setProdInfo($prodId){
		$invoiceObj = ClassRegistry::init('Slaves');
		$id = $invoiceObj->query("SELECT services.id,services.name,vendors_commissions.vendor_id, vendors_commissions.discount_commission, products.circle_yes, products.circle_no, products.automate_flag, vendors.shortForm, vendors.update_flag, products.price, products.oprDown, products.down_note , products.name,products.min,products.max,products.invalid,products.blocked_slabs,vendors_commissions.circles_yes,vendors_commissions.circle, vendors_commissions.circles_no FROM products left join vendors_commissions ON (vendors_commissions.product_id= products.id AND vendors_commissions.oprDown = 0) left join vendors on (vendors_commissions.vendor_id=vendors.id) join services ON (products.service_id = services.id) WHERE products.id=$prodId order by vendors_commissions.active desc,discount_commission desc");
		$info = array();
		$info['vendors'] = array();
		$vendor_ids = array();
		$vendors_n = array();
		
		foreach($id as $res){
			$info['service_id'] = $res['services']['id'];
			$info['service_name'] = $res['services']['name'];
			$info['min'] = $res['products']['min'];
			$info['max'] = $res['products']['max'];
			$info['automate'] = $res['products']['automate_flag'];
			$info['price'] = $res['products']['price'];
			$info['invalid'] = $res['products']['invalid'];
			$info['name'] = $res['products']['name'];
			$info['oprDown'] = $res['products']['oprDown'];
			$info['down_note'] = $res['products']['down_note'];
			$info['circles_yes'] = strtoupper(trim($res['products']['circle_yes']));
			$info['circles_no'] = strtoupper(trim($res['products']['circle_no']));
			$info['blocked_slabs'] = explode(",",trim($res['products']['blocked_slabs']));
			if(!empty($res['vendors_commissions']['vendor_id'])){
				$info['vendors'][] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);	
				$vendor_ids[] = $res['vendors_commissions']['vendor_id'];
				$vendors_n[$res['vendors_commissions']['vendor_id']] = array('vendor_id'=>$res['vendors_commissions']['vendor_id'],'shortForm'=>$res['vendors']['shortForm'],'discount_commission'=>$res['vendors_commissions']['discount_commission'],'circles_yes'=>explode(",",$res['vendors_commissions']['circles_yes']),'circles_no'=>explode(",",$res['vendors_commissions']['circles_no']),'circle'=>trim($res['vendors_commissions']['circle']),'update_flag'=>$res['vendors']['update_flag']);
			}
		}
		
		
		if($info['automate'] == 1){
			$opr_ids = $this->getOtherProds($prodId);
			$non_primary = array();
			$cap_non_primary = ($prodId == 15 ) ? 75 : 100; // 75% for vodafone and others 100%
			//$vendors_data = $invoiceObj->query("select vendor_id,sum(planned_sale) as planned_sale,sum(base_amount) as base_amount,sum(totalsale) as totalsale,sum(sims) as sims from (SELECT devices_data.vendor_id,inv_planning_sheet.planned_sale,inv_planning_sheet.base_amount,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance > 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims FROM inv_planning_sheet INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id) WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)) as t group by t.vendor_id");
			
			$vendors_data = $invoiceObj->query("SELECT devices_data.vendor_id,devices_data.supplier_operator_id,devices_data.inv_supplier_id,inv_planning_sheet.base_amount,inv_planning_sheet.max_sale_capacity,sum(devices_data.sale) as totalsale,sum(if(devices_data.device_id = devices_data.par_bal AND devices_data.block = '0' AND devices_data.stop_flag=0 AND devices_data.balance >= 10 AND devices_data.recharge_flag = 1 AND devices_data.active_flag = 1, 1, 0)) as sims, inv_modem_planning_sheet.target
FROM inv_planning_sheet 
INNER JOIN devices_data ON (devices_data.supplier_operator_id = inv_planning_sheet.supplier_operator_id)
INNER JOIN inv_modem_planning_sheet ON (inv_modem_planning_sheet.supplier_operator_id = devices_data.supplier_operator_id AND devices_data.vendor_id =inv_modem_planning_sheet.vendor_id)
WHERE devices_data.sync_date = '".date('Y-m-d')."' AND devices_data.opr_id in ($opr_ids) group by devices_data.vendor_id,devices_data.supplier_operator_id having (sims > 0)");
			
			
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." ::$prodId:: modem_vendor_data:: ".  json_encode($vendors_data));
            
			$target_sale_supplier = array();
			$target_sale_vendor = array();
			foreach ($vendors_data as $vendor){
				if(!isset($target_sale_supplier[$vendor['devices_data']['supplier_operator_id']])) {
					$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['target'] = 0;
					$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['base'] = 0;
				}
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['target'] += $vendor['inv_modem_planning_sheet']['target'];
				
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['base'] = $vendor['inv_planning_sheet']['max_sale_capacity'];
				$target_sale_supplier[$vendor['devices_data']['supplier_operator_id']]['supplier_id'] = $vendor['devices_data']['inv_supplier_id'];
				
				
				if(!isset($target_sale_vendor[$vendor['devices_data']['vendor_id']])){
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['target'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sims'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sale'] = 0;
					$target_sale_vendor[$vendor['devices_data']['vendor_id']]['base'] = 0;
				}
				
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['target'] += $vendor['inv_modem_planning_sheet']['target'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sims'] += $vendor['0']['sims'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['sale'] += $vendor['0']['totalsale'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['base'] += $vendor['inv_planning_sheet']['max_sale_capacity'];
				$target_sale_vendor[$vendor['devices_data']['vendor_id']]['data'][] = $vendor;
			}
			
			$priority_1 = array();
			$priority_2 = array();
			$cut_off = array();
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." ::$prodId:: target_sale_vendor:: ".  json_encode($target_sale_vendor));
            
			
			foreach ($target_sale_vendor as $vendor_id=>$vendor){
				if(!in_array($vendor_id,$vendor_ids))continue;
			
				$r2_sale = $vendor['base'] - $vendor['sale'];
				$r1_sale = $vendor['target'] - $vendor['sale'];
			
				if($vendor['sale']*100/$vendor['target'] > $cap_non_primary){
					$non_primary[] = $vendor_id;
				}
				if($vendor['sims'] > 0 && $r1_sale > 0){
					$weight = $r1_sale/$vendor['sims'];
					$priority_1[$weight] = $vendor_id;
				}
				else if($vendor['sims'] > 0 && $r2_sale > 0){
					$weight = $r2_sale/$vendor['sims'];
					$priority_2[$weight] = $vendor_id;
				}
				
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::r1_sale $r1_sale :: r2_sale $r2_sale");
            
				
				//$modem_cutoff_arr = array();
				foreach($vendor['data'] as $dt){
					$supplier = $dt['devices_data']['supplier_operator_id'];
					$target_supp = $target_sale_supplier[$supplier]['target'];
					$base_supp = $target_sale_supplier[$supplier]['base'];
					
					$ratio = ($base_supp < $target_supp) ? 1 :  ($base_supp/$target_supp);
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id :: ".$target_sale_supplier[$supplier]['supplier_id']." :: base $base_supp:: target_supp ::$target_supp:: vendor_supp_target:: ".$dt['inv_modem_planning_sheet']['target']."::vendor_supp_sale::".$dt['0']['totalsale']." :: ratio $ratio");
            
					if($dt['0']['totalsale'] > $ratio*$dt['inv_modem_planning_sheet']['target']){
						$query = "query=salestop&supplier_id=".$target_sale_supplier[$supplier]['supplier_id']."&product=$opr_ids&flag=1";
						$this->async_request_via_redis($query,$vendor_id);
						$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::cutoff query :: $query");
					}
				}

				
				if($r1_sale < 0 && $r2_sale < 0){
					$cut_off[] = $vendor_id;
				}
				
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: $vendor_id ::vendors cutoffs  :: ".json_encode($cut_off));
			}
			
			
			//get other vendors as well
			$vendors_data = $invoiceObj->query("SELECT vendors.id,base_amount,max_sale_capacity,imps.target FROM `inv_planning_sheet` as ips inner join inv_supplier_operator as iso ON (ips.supplier_operator_id = iso.id) inner join inv_modem_planning_sheet as imps ON (imps.supplier_operator_id = iso.id) inner join inv_supplier_vendor_mapping as isvm ON (isvm.supplier_id = iso.supplier_id) inner join vendors ON (vendors.id = isvm.vendor_id) WHERE vendors.update_flag = 0 AND operator_id in ($opr_ids)");
			
			foreach($vendors_data as $vd){
				if(!in_array($vd['vendors']['id'],$vendor_ids))continue;

				$prod = $this->getParentProd($prodId);
                
				$cap_per_min = $this->getMemcache("cap_api_".$prod."_".$vd['vendors']['id']);
                
				$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prod:: ".$vd['vendors']['id']." ::cap_per_min $cap_per_min :: "."cap_api_".$prod."_".$vd['vendors']['id']);
            
				if($cap_per_min !== false && $cap_per_min > 0){
					//$sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
                                        $sale = $this->getMemcache("sale_".$prodId."_".$vd['vendors']['id']);
					$r2_sale = $vd['ips']['max_sale_capacity'] - $sale;
					$r1_sale = $vd['imps']['target'] - $sale;
	
					if($sale*100/$vd['imps']['target'] > $cap_non_primary){
						$non_primary[] = $vd['vendors']['id'];
					}
					
					$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: ".$vd['vendors']['id']." ::r1_sale $r1_sale :: r2_sale $r2_sale :: $cap_per_min");
            
                    
					if($r1_sale > 0){
						$weight = $r1_sale/$cap_per_min;	
						$priority_1[$weight] = $vd['vendors']['id'];
					}
					else if($r2_sale > 0){
						$weight = $r2_sale/$cap_per_min;	
						$priority_2[$weight] = $vd['vendors']['id'];
					}
					
					if($r1_sale < 0 && $r2_sale < 0){
						$cut_off[] =$vd['vendors']['id'];
					}
				}
				
			}
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			krsort($priority_1);
			krsort($priority_2);
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: after sort :: $prodId:: priority1 :: ".  json_encode($priority_1)." :: priority2 :: ". json_encode($priority_2));
            
			$this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: cutoff vendors :: $prodId:: ".  json_encode($cut_off));
            
			$info['vendors'] = array();
			foreach($priority_1 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			foreach($priority_2 as $k => $v){
				$info['vendors'][] = $vendors_n[$v];
				$final[] = $v;
			}
			
			foreach($vendors_n as $k => $v){
				if(in_array($k,$final))continue;
				if(in_array($k,$cut_off))continue;
				$info['vendors'][] = $v;
			}
            
                        /**
                         * adhock for all transaction between 23:45 - 24:00 Hrs priority vendors will be in reverse order, so that if api vendor 
                         * unable to clear txn modem should able to clear them.
                         */
                        if(date('H') == "23" && date("i") >= 45){
                            $info['vendors'] = array_reverse($info['vendors'],true);
                        }
                        
			$info['non_primary'] = $non_primary;
                        $this->General->logData("/mnt/logs/prioritylog.txt",date("Y-m-d H:i:s")." :: final :: $prodId :: ". json_encode($info['vendors']) . "::non-primary vendors::".json_encode($info['non_primary']));
		}
        
		$this->setMemcache("prod$prodId",$info,5*60);
		return $info;
	}
		
	function getVendors(){
		$vendors = $this->getMemcache('vendors');
		if(empty($vendors)){
			$vendors = $this->setVendors();
		}
		
		return $vendors;
	}
	
	function setVendors(){
		$invoiceObj = ClassRegistry::init('Slaves');
		$vendors = $invoiceObj->query("SELECT id,shortForm,company,balance,user_id,update_flag,ip,port FROM vendors WHERE show_flag = 1 order by company");	
		$this->setMemcache('vendors',$vendors,60*60*24);
		
		return $vendors;
	}
	
	function getInactiveVendors(){
		$vendors = $this->getMemcache('inactiveVendors');
		if($vendors === false){
			$vendors = $this->setInactiveVendors();
		}
		
		return $vendors;
	}
	
	function setInactiveVendors(){
		$invoiceObj = ClassRegistry::init('Slaves');
		$vendors = $invoiceObj->query("SELECT group_concat(id) as ids FROM vendors WHERE active_flag != 1");
		$vendors = explode(",",$vendors['0']['0']['ids']);
		$this->setMemcache('inactiveVendors',$vendors,2*60);
		
		return $vendors;
	}
	
	function getProducts(){
		$products = $this->getMemcache('products');
		if(empty($products)){
			$invoiceObj = ClassRegistry::init('Slaves');
			$products = $invoiceObj->query("SELECT * FROM products");	
			$this->setMemcache('products',$products,60*60);
		}
		
		return $products;
	}
	
	function findOptimalVendor($vendors,$prodId,$automate_flag,$existing=array(),$transId=null){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
		
		foreach($vendors as $vend){
			$vendor_id = $vend['vendor_id'];
			if(in_array($vendor_id,$existing))continue;
			
			if($vend['update_flag'] == 0){	
				$prod = (isset($arr_map[$prodId]))? $arr_map[$prodId] :  $prodId;
				$data = $this->getMemcache("status_$prod"."_$vendor_id");

				$this->General->logData("/mnt/logs/abc_$vendor_id.txt","before decrement::$transId::$prod::$vendor_id::$data");
					
				if($data !== false && $data > 0){
					$data = $this->decrementMemcache("status_$prod"."_$vendor_id");
					if($data >= 0){
					$this->General->logData("/mnt/logs/abc_$vendor_id.txt","after decrement::$transId::$prod::$vendor_id::$data");
					$vend['key_vendor'] = "status_$prod"."_$vendor_id";
					return $vend;
				}
				}
					
				if($data === false){
					return $vend;
				}
			}
			else return $vend;
		}
	}
	
	function getActiveVendor($prodId,$mobile=null,$apiPartner=null,$primary_flag=true,$additional_param=array()){
		$success = true;

		$info = $this->getProdInfo($prodId);
		if(empty($info['vendors'])){
			$info = $this->setProdInfo($prodId);
		}
		
		$non_primary = (isset($info['non_primary'])) ? $info['non_primary'] : array();
		$api_flag = false;
		if($mobile != null){
			$prod_d = $this->General->getMobileDetailsNew($mobile);// need to open for mobile numbering changes
                        //$prod_d = $this->General->getMobileDetails($mobile);
                        
			if(!$api_flag && !empty($info['circles_yes']) && !in_array($prod_d['area'],explode(",",$info['circles_yes']))){
				return array('status'=>'failure','code'=>'','description'=>'Cannot recharge on '.$prod_d['area'].' circle','name'=>$info['name']);
			}

			if(!$api_flag && !empty($info['circles_no']) && in_array($prod_d['area'],explode(",",$info['circles_no']))){
				return array('status'=>'failure','code'=>'','description'=>'Cannot recharge on '.$prod_d['area'].' circle','name'=>$info['name']);
			}
		}
                
		if($apiPartner == 6) {//b2c api partner
			$api_flag = true;
		}
		//distributor id & vendor id mapping, key will be distid_prodid & value will be vendorid 
                // 75 - cotton_green 71-kandivali 10-GSM modem2
                
                $dist_vendor_mapping = $this->generate_local_vendor_map();
                /*$dist_vendor_mapping = array(
                    '624_2'=>'75','398_2'=>'75','453_2'=>'75','212_2'=>'75','860_2'=>'75','816_2'=>'75','777_2'=>'75','512_2'=>'75','241_2'=>'75',
                    '286_2'=>'75','855_2'=>'75','505_2'=>'75',
                    '461_2'=>'78','785_2'=>'78','844_2'=>'78','312_2'=>'78',
                    '744_2'=>'77','470_2'=>'77','584_2'=>'77','555_2'=>'77','700_2'=>'77','522_2'=>'77'                    
                    );
                 * 
                 */
		$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param));
						
		
		$inactive = $this->getInactiveVendors();
		$vends = array();
		$prim_vend = null;
		$primary = null;
		
		if($api_flag){
			$invoiceObj = ClassRegistry::init('Slaves');
			$prodData = $invoiceObj->query("SELECT primary_vendor FROM partner_operator_status WHERE partner_acc_no = 'P000002' AND operator_id = $prodId");
			
			if(!empty($prodData) && !empty($prodData['0']['partner_operator_status']['primary_vendor']) && !in_array($prodData['0']['partner_operator_status']['primary_vendor'],$inactive)){
				$prim_vend = $prodData['0']['partner_operator_status']['primary_vendor'];
			}
		}
		
		$exceptional_vendors = array_merge(array('29'),array_values(array_unique($dist_vendor_mapping)));//b2c modem & local set ups
		
		if(!($api_flag) && isset($additional_param['retailer_created'])){
			$diff = strtotime(date('Y-m-d')) - strtotime($additional_param['retailer_created']);
			
			$prim_vend_newRetailers = array('8'=>'29','9'=>'29','15'=>'29','16'=>'29','17'=>'29','18'=>'29');
			if($diff/(60*60*24) <= 30){
				$prim_vend = (isset($prim_vend_newRetailers[$prodId]) && !empty($prim_vend_newRetailers[$prodId])) ? $prim_vend_newRetailers[$prodId] : 29;//b2c vendor
			}
		}
					
		$exception = null;
		//print_r($inactive);
		//
		$primary_vendors = array();
				
		foreach($info['vendors'] as $vend){
			$circle_yes = $vend['circles_yes'];
			$circle_no = $vend['circles_no'];

			$imp_yes = implode(",",$circle_yes);
			$imp_no = implode(",",$circle_no);
			
			//exception wrt airtel infogem
			/*if($vend['vendor_id'] == 27 && $prodId == 2 && !in_array($vend['vendor_id'],$inactive)){
				$exception = $vend;
			}*/
                        if($vend['vendor_id'] == 68 && $prodId == 2 && $additional_param['amount'] <= 50){
                            continue;
                        }
			if(!empty($imp_yes) && !in_array($prod_d['area'],$circle_yes)){
				continue;
			}
			if(!empty($imp_no) && in_array($prod_d['area'],$circle_no)){
				continue;
			}
			if(!in_array($vend['vendor_id'],$inactive)){
				if($api_flag || !in_array($vend['vendor_id'],$exceptional_vendors)){//removing exceptional/priority modems from normal b2b transactions
					$vends[] = $vend;
				}
				
				if(!empty($prim_vend) && $prim_vend == $vend['vendor_id']){//primary wrt api partner/ new retailers
					$primary = $vend;
					$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::api vendor case ::primary: ".$vend['vendor_id']);
				}
				else if(empty($prim_vend)){
					$val = isset($additional_param['dist_id'])?$additional_param['dist_id']:0;
					if(!empty($val) && isset($dist_vendor_mapping[$val."_".$prodId]) && $dist_vendor_mapping[$val."_".$prodId] == $vend['vendor_id'] && !in_array($vend['vendor_id'],$non_primary)){
						$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::distid_area case ::primary: ".$vend['vendor_id']);
						$primary_vendors['local_vendor'] = $vend;
					}
					if(!empty($vend['circle']) && $prod_d['area'] == $vend['circle'] && !in_array($vend['vendor_id'],$non_primary)){//circle wise primary vendor logic
						$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::circle_area primary case ::primary: ".$vend['vendor_id']);
						$primary_vendors['circle'] = $vend;
					}if(!empty($additional_param) && isset($additional_param['amount'])){
                                           if($this->check_amount_priority(intval($prodId),intval($vend['vendor_id']),intval($additional_param['amount']))){
                                                $this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::cp airtel lower amount case ::primary: ".$vend['vendor_id']);
                                                $primary_vendors['cp_airtel'] = $vend;
                                           } 
                                           if(in_array($additional_param['amount'],array(10,20,30)) && $prodId == 2 && strtoupper($prod_d['area']) == 'MU'){
                                               $primary_vendors['cp_airtel'] = $vend;
                                        }
				}
			}
		}
		}

		if(empty($primary)){
			if(isset($primary_vendors['local_vendor']))$primary = $primary_vendors['local_vendor'];
			else if(isset($primary_vendors['cp_airtel']))$primary = $primary_vendors['cp_airtel'];
			else if(isset($primary_vendors['circle']))$primary = $primary_vendors['circle'];
		}
		
		$this->General->logData('/mnt/logs/active_vendor.txt',"product:$prodId, mobile:$mobile, additional params:".json_encode($additional_param) . " ::final primary: ".$primary['vendor_id']);
						
		if(!empty($primary)){
			array_unshift($vends,$primary);
		}
		
		if(!empty($exception)){
			$vends[] = $exception;	
		}
		
		$info['vendors'] = $vends;

		if(empty($primary) && $primary_flag){
			$primary = $this->findOptimalVendor($vends,$prodId,$info['automate']);
		}
                else if($primary_flag){
                    $this->findOptimalVendor(array($primary),$prodId,$info['automate']);
                }
		
		if(empty($info['vendors'])){
			return array('status'=>'failure','code'=>'','description'=>'','name'=>$info['name'],'info'=>$info);
		}
		else {
			return array('status'=>'success','code'=>'','info'=>$info,'primary'=>$primary);
		}
	}
	
        /**
         * Generate and arrange the vendors mapping in specified structured based on input
         * @return type array
         */
        function generate_local_vendor_map(){
            $mapArr = $this->getLocalVendorsMap();
            $returnArr = array();
            foreach($mapArr as $vendorId=>$opr_dist_Arr){
              foreach($opr_dist_Arr as $oprId=>$dist_Arr){
                foreach($dist_Arr as $dist_Id){
                  $returnArr[$dist_Id."_".$oprId] = $vendorId;	
                }	
              }
            }
            return $returnArr;
        }
        
        /**
         * Get local vendors mapping from memcache
         * @return type array
         */
        function getLocalVendorsMap(){
            $vendors = $this->getMemcache('local_Vendors_map');
            if($vendors === false){
                    $vendors = $this->setLocalVendorsMap();
            }
            return $vendors;
	}
        
        /**
         * SET local vendors mapping in memcache from query result
         * @return type array
         */
        function setLocalVendorsMap(){
            //$slaveObj = ClassRegistry::init('Slaves');
            $slaveObj = ClassRegistry::init('User');
            $vendors = $slaveObj->query("SELECT * FROM local_vendor_mapping where is_deleted = 1 ");
            $vendordata = array();
            foreach($vendors as $localvendor_key=>$localvendor_val){
                $vendor_id = isset($localvendor_val['local_vendor_mapping']['vendor_id']) ? $localvendor_val['local_vendor_mapping']['vendor_id'] : "";
                $operator_id = isset($localvendor_val['local_vendor_mapping']['operator_id']) ? $localvendor_val['local_vendor_mapping']['operator_id'] : "";
                $distributor_id = isset($localvendor_val['local_vendor_mapping']['distributor_id']) ? $localvendor_val['local_vendor_mapping']['distributor_id'] : "";
                $vendordata[$vendor_id][$operator_id] = explode(",",$distributor_id);
            }
            $this->setMemcache('local_Vendors_map',$vendordata);		
            return $vendordata;
	}
        
        /**
         * 
         * Check if there is any amount based priority exist for particular operator on specific vendor
         * @param type $productId
         * @param type $vendorId
         * @param type $Amount
         * @return type boolean
         */
        function check_amount_priority($productId,$vendorId,$Amount){            
            
            $amt_priority = $this->getAmountPriorityMap();
            
            foreach( $amt_priority as $pro_arr ){
                
                if($pro_arr['product_id'] == $productId && $pro_arr['vendor_id'] == $vendorId ){
                    
                    if(!empty($pro_arr['list_amount'])){
                        
                        return in_array($Amount,explode(",",$pro_arr['list_amount']));
                        
                    }elseif(!empty ($pro_arr['min_amount']) && !empty($pro_arr['min_amount']) && $pro_arr['min_amount'] <= $Amount && $pro_arr['max_amount'] >= $Amount){
                        
                        return TRUE;
                        
                    }else{
                        
                        return FALSE;
                        
                    }                    
                }
            }
            return FALSE;
        }
        
        /**
         * Get amount priority map from memcache 
         * @return type array
         */
        function getAmountPriorityMap(){
            
            $amtPriorityMap = $this->getMemcache('amount_priority_map');
            if($amtPriorityMap === false){
                    $amtPriorityMap = $this->setAmountPriorityMap();
            }		
            return $amtPriorityMap;
        }
        
        /**
         * set amount priority map to memcache from query result
         * @return type array
         */
        function setAmountPriorityMap(){
            //$slaveObj = ClassRegistry::init('Slaves');
            $slaveObj = ClassRegistry::init('User');
            $amtPriorityMap = $slaveObj->query("SELECT * FROM amount_priority_mapping where is_deleted = 1 ");
            $amtPriorityMapping = array();
            foreach ($amtPriorityMap as $amtPriority_key=>$amtPriority_val){
                $amtPriorityMapping[] = $amtPriority_val['amount_priority_mapping'];
            }
            $this->setMemcache('amount_priority_map',$amtPriorityMapping,24*60*60);		
            return $amtPriorityMapping;
        }        
        
        
	function checkPossibility($prodId,$mobileNo,$amount,$power,$param=null,$special=null,$api_flag=null){
		$sms = null;
		if(!empty($power)) return $sms;
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($amount == ''){
			$prodData = $invoiceObj->query("SELECT price FROM products_info WHERE product_id = $prodId");
            if(empty($prodData) && count($prodData)>0){
			$amount = $prodData['0']['products_info']['price'];
            } else {
                $amount  = '';
            }
		}
		if(empty($param))$number = $mobileNo;
		else $number = $param;
		
		$mins = TIME_DURATION;
		
		if(empty($prodId) || empty($number) || empty($amount)) return 'Invalid recharge format';
		
		if(!$this->lockTransactionDuplicates($prodId,$number,$amount,$api_flag)){
			if($param == null){
				$msg = "*$prodId*$mobileNo*$amount";
				$sms = "hold: Repeat on $mobileNo of Rs$amount within ".($mins/60)." hours";
			}
			else {
				$msg = "*$prodId*$param*$mobileNo*$amount";
				$sms = "hold: Repeat on $param of Rs$amount within ".($mins/60)." hours";
			}
			
			if(!empty($special) && $special == 1) $msg = $msg . "#";
			
			$sender = $_SESSION['Auth']['mobile'];
			$this->addRepeatTransaction($msg,$sender,2);
			$mail_body = "Request is on hold, Retailer $sender has done duplicate transaction within $mins mins";
			$mail_body .= "<br/>Customer Mobile: $mobileNo/$param, Amount: $amount";
			$this->General->sendMails("Pay1: Same Transaction within $mins mins",$mail_body,array('notifications@mindsarray.com'));
			
			$added = $invoiceObj->query("SELECT id FROM repeated_transactions WHERE msg = '$msg' AND sender='$sender' AND type=2 ORDER BY id desc LIMIT 1");
			if(!empty($added)){
				$id = $added['0']['repeated_transactions']['id'];
				$id = "1" . sprintf('%04d', $id);
				$sms .= "\nTo continue send: #$id via misscall service or SMS to 09004-350-350";
				$sms .= "\nIgnore if don't want to continue";
			}
		}
		
		return $sms;
	}
	
	function addRepeatTransaction($msg,$sender,$type,$added_by=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($added_by == null){
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,type,timestamp) VALUES ('$sender','".addslashes($msg)."',$type,'".date('Y-m-d H:i:s')."')");	
		}
		else {
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,send_flag,type,added_by,processed_by,timestamp) VALUES ('$sender','".addslashes($msg)."',1,$type,$added_by,$added_by,'".date('Y-m-d H:i:s')."')");
		}
	}
	
	/*function getReceiptNumber($id){
		$recObj = ClassRegistry::init('Receipt');
		$data = $recObj->findById($id);
		if($data['Receipt']['receipt_type'] == RECEIPT_INVOICE){
			$suffix1 = "INV";
		}
		else if($data['Receipt']['receipt_type'] == RECEIPT_TOPUP){
			$suffix1 = "TP";
		}
		
		if($data['Receipt']['group_id'] == SUPER_DISTRIBUTOR){
			$suffix2 = "ST";
		}
		else if($data['Receipt']['group_id'] == DISTRIBUTOR){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['Receipt']['group_id'] == RETAILER){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		$ret = $recObj->query("SELECT count(*) as counts FROM receipts WHERE shop_from_id = ".$data['Receipt']['shop_from_id']." and group_id = " . $data['Receipt']['group_id'] . " and id < $id");
		if(isset($parent_comp)){
			$suffix2 = "";
			foreach($parent_comp as $str){
				$suffix2 .= substr($str,0,1);
			}
			$suffix2 = strtoupper($suffix2);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "R/". $suffix1 . "/". $suffix2 . "/" . sprintf('%03d', $number);
	}*/
	
	function getShopData($user_id,$group_id=null){
		if($group_id == null){
			$userData = $this->General->getUserDataFromId($user_id);
			$group_id = $userData['group_id'];
		}
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Slaves');
			$retailers = $userObj->query("select * 
					from retailers as r
					left join unverified_retailers ur on ur.retailer_id = r.id
					where r.user_id = ".$user_id);
			foreach($retailers[0]['ur'] as $key => $row){
        		if(!in_array($key, array('id')))
        			$retailers[0]['r'][$key] = $retailers[0]['ur'][$key];
        	}
        	
			return $retailers[0]['r'];
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
            $bal = $userObj->find('first',array('conditions' => array('Rm.user_id' => $user_id)));
            return $bal['Rm'];
		}
	}
	
	
	function payment_gateway($amount,$via='web'){
		$userObj = ClassRegistry::init('Retailer');
		$data = $userObj->query("SELECT * FROM pg_checks WHERE distributor_id = '".$_SESSION['Auth']['parent_id']."'");
		
		$topup = $amount - $data['0']['pg_checks']['service_charge'];
		
		if(empty($data) || $data['0']['pg_checks']['active_flag'] == 0){
			return array('status' => 'failure','code'=>'43','description'=>$this->errors(43));
		}
		else if($topup < $data['0']['pg_checks']['min_amount']){
			return array('status' => 'failure','code'=>'33','description'=>'Minimum amount allowed for topup is Rs. '.$data['0']['pg_checks']['min_amount']);
		}
		else if($topup > $data['0']['pg_checks']['max_amount']){
			return array('status' => 'failure','code'=>'34','description'=>'Maximum amount allowed for topup is Rs. '.$data['0']['pg_checks']['max_amount']);
		}
		
		$retailer = $userObj->query("SELECT rental_flag from retailers where id = '".$_SESSION['Auth']['id']."'");
		$rental_flag = $retailer['0']['retailers']['rental_flag']; 
		if($rental_flag == 2){
			$amount += $this->General->findVar("OTA_Fee");
		}	
		//make entry in pg_india table
			
		$recId = $this->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$topup,1,$_SESSION['Auth']['id'],8,null,5);
		
		$this->setMemcache("pg_$recId",$via,30*60);
		
		$sm = $userObj->query("SELECT salesmen.id,salesmen.name from salesmen inner join distributors ON (distributors.id = salesmen.dist_id) inner join users ON (users.id = distributors.user_id) where distributors.id = '".$_SESSION['Auth']['parent_id']."' AND users.mobile = salesmen.mobile");
		$userObj->query("INSERT INTO salesman_transactions (shop_tran_id,salesman,payment_mode,payment_type,details,collection_date,created) VALUES ('".$recId."','".$sm['0']['salesmen']['id']."','".MODE_PG."','".TYPE_TOPUP."','','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')");

		$userObj->query("INSERT INTO pg_payuIndia (status,shop_transaction_id,amount,addedon,server_ip) VALUES ('pending','$recId','$amount','".date('Y-m-d H:i:s')."','".$_SERVER['REMOTE_ADDR']."')");
			
		//get shop_transid & pass it to pg_payment function .. this will return payment form.. Which will be shown to user
		
		$shopname = (empty($_SESSION['Auth']['shopname']))? $_SESSION['Auth']['mobile'] : $_SESSION['Auth']['shopname'];
		$data = array('transaction_id'=>$recId,'amount'=>$amount,'category'=>'TOPUP_RETAILER','name'=>$shopname,'email'=>$_SESSION['Auth']['email']);
		
		$pg_form_data = $this->generatePayment_form_retailer($data);
		return array('status' => 'success','code' =>'0','description'=>$pg_form_data);
	}
	
	function generatePayment_form_retailer($data){
		$data2process = $this->payu_setdata2process($data);
        $fdata = array('data2process'=>$data2process,'salt'=>PAYU_SALT);
        $hash = $this->generate_payu_hash($fdata);
        
        $data2process['phone'] = $_SESSION['Auth']['mobile'];
        $data2process['surl'] = PAYU_SUCCESS_URL;
        $data2process['furl']= PAYU_FAILURE_URL;
        $data2process['curl']=PAYU_FAILURE_URL;
        $data2process['touturl']=PAYU_FAILURE_URL;
        $data2process['hash']=$hash;
        $data2process['user_credentials']=PAYU_KEY.":".$_SESSION['Auth']['user_id'];
        
        $pay_page_content = $this->generate_pg_payu_form_content($data2process);
        return $pay_page_content;
	}
	
	function payu_setdata2process($data){
		$data2process['key'] = PAYU_KEY;
        //transaction related detail
        $data2process['txnid'] = $data['transaction_id'];
        $data2process['amount'] = $data['amount'];
        $data2process['productinfo'] = $data['category'];
        //customer detail
        
        $data2process['firstname'] = $data['name'];
        $data2process['email'] = (empty($data['email']))?"":$data['email'];
        return $data2process;
	}
	
 	function generate_payu_hash($data){
        $str = implode("|",array_values($data['data2process']))."|||||||||||".$data['salt'];
        //echo $str;exit();
        return hash("sha512",$str);
    }
	
	function generate_pg_payu_form_content($data){
		$mem_data = $this->getMemcache("pg_" . $data['txnid']);
		if($mem_data == 'web'){
	       	$postdata = http_build_query($data);
			$options = array('http' =>
					array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/x-www-form-urlencoded',
							'content' => $postdata
					)
			);
	
			$context  = stream_context_create($options);
	
			$result = file_get_contents("http://panel.pay1.in/apis/pgPayUSeamless", false, $context);
			
			return $result;
		}
		else {
			$content = "<html>";
	        $content .= '<script> function submitform(){document.getElementById("pg_payu_form").submit();}</script>';
	        $content .= "<body onload=submitform()>";
	        $content .= "<form id='pg_payu_form' action='".PAYU_URL."' id='pg_payu_form' method='post'>";
	        foreach($data as $key=>$value){
	            $content .= "<input type='hidden' name='$key' value='$value'>";
	        }
	        $content .= "</form>";
	        $content .="</body></html>";
	        return $content;
		}
    }
    
    function update_pg_payu($data,$online=true){
    	if($online && !$this->check_payu_hash($data)){
    		$t_data = $data;
    		$t_data['ip']= $_SERVER['REMOTE_ADDR'];
    		$this->General->sendMails("b2b: wrong hash for this transaction please check it", json_encode($t_data),array('ashish@mindsarray.com'),'mail');
    		return array('status'=>'failure','code'=>'','description'=>"Hash not matched");
    	}
    	
	    $data['cardnum'] = isset($data['card_no'])?$data['card_no']:(isset($data['cardnum'])?$data['cardnum']:"");
	    $data['error'] = isset($data['error_code'])?$data['error_code']:(isset($data['error'])?$data['error']:"");
	   	$data['shop_transaction_id'] = $data['txnid'];
	   	
	   	unset($data['card_no']);
	    unset($data['error_code']);
	    unset($data['txnid']);
	    
	    $userObj = ClassRegistry::init('Retailer');
    	$colqry = "SELECT COLUMN_NAME  FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='shops' AND `TABLE_NAME`='pg_payuIndia'";
       	$col_result = $userObj->query($colqry);
       	$col_data = array();
       	
        foreach ($col_result as $k=>$v){
        	$col_data[$v['COLUMNS']['COLUMN_NAME']] = isset($data[$v['COLUMNS']['COLUMN_NAME']])?addslashes($data[$v['COLUMNS']['COLUMN_NAME']]):"";
        }

        $payuQry = "select pg_payuIndia.status,shop_transactions.* from pg_payuIndia left join shop_transactions ON (shop_transactions.id = pg_payuIndia.shop_transaction_id) WHERE shop_transaction_id='".$data['shop_transaction_id']."' AND status = 'pending'";
        $payu_result = $userObj->query($payuQry);
        
        if(empty($payu_result)){
        	$this->General->sendMails("B2B:: Payu data already exists", json_encode($data),array('ashish@mindsarray.com'),'mail');
        }
        else{
        	unset($col_data['id']);
        	unset($col_data['server_ip']);
        	$this->update_payu_Transaction($col_data['shop_transaction_id'],$col_data);
        	
	        if($col_data['status']==="success"){
	            if($payu_result['0']['shop_transactions']['type'] == DIST_RETL_BALANCE_TRANSFER){
	                $result = $this->process_retailer_transfer_after_pg($data);
	            }
	            return $result;
	        }
	        else if($col_data['status']==="failure"){
	        	if($payu_result['0']['shop_transactions']['type'] == DIST_RETL_BALANCE_TRANSFER){
	        		$userObj->query("UPDATE shop_transactions SET confirm_flag = 1 WHERE id = ". $col_data['shop_transaction_id']);
	        	}
	        	return array('status'=>'failure','code'=>'','description'=>json_encode($col_data));
	        }
        }
            
    }
    
    function process_retailer_transfer_after_pg($data){
    	try{
    		$userObj = ClassRegistry::init('Retailer');
    		$txnid = $data['shop_transaction_id'];
    		$amount = $data['amount'];
    		
    		$txndata = $userObj->query("SELECT shop_transactions.amount,shop_transactions.ref1_id,shop_transactions.ref2_id,retailers.shopname,retailers.mobile FROM shop_transactions left join retailers ON (retailers.id = shop_transactions.ref2_id) WHERE shop_transactions.id = $txnid AND shop_transactions.confirm_flag != 1");
    		
    		if(!empty($txndata)){
    			$userObj->query("UPDATE shop_transactions SET note = '".$data['mihpayid']."' WHERE id = $txnid");
    			
    			$txn_amount = $txndata['0']['shop_transactions']['amount'];
    			$dist_id = $txndata['0']['shop_transactions']['ref1_id'];
    			$ret_id = $txndata['0']['shop_transactions']['ref2_id'];
    			$ret_mobile = $txndata['0']['retailers']['mobile'];
    			
    			$bal = $this->shopBalanceUpdate($txn_amount,'subtract',$dist_id,DISTRIBUTOR);
				$bal1 = $this->shopBalanceUpdate($txn_amount,'add',$ret_id,RETAILER);
				
				$this->addOpeningClosing($ret_id,RETAILER,$txnid,$bal1-$txn_amount,$bal1);
				$this->addOpeningClosing($dist_id,DISTRIBUTOR,$txnid,$bal+$txn_amount,$bal);
    			
    			if(!empty($txndata['0']['retailers']['shopname'])){
					$shop_name = substr($txndata['0']['retailers']['shopname'],0,15);
				}
				else $shop_name = $txndata['0']['retailers']['mobile'];

				/*$distributors = $userObj->query("select u.mobile
						from users u
						left join distributors d on d.user_id = u.id
						where d.id = ".$dist_id);
				if($distributors){
	                $message_distributor = "Your account is debited with Rs. ".$txn_amount." to retailer: ".$shop_name;
	                $this->General->sendMessage($distributors['0']['u']['mobile'], $message_distributor, "notify", null, DISTRIBUTOR);
				}*/
				
				$mail_subject = "Amount transferred to retailer via Payment Gateway";
				$mail_body = "Transferred Rs. $txn_amount to Retailer: " . $shop_name;
				
				$mail_body .= "<br/>Payment done by Retailer: ".$amount;
				$mail_body .= "<br/>PayuID: ".$data['mihpayid'];
				
				$msg = "Dear $shop_name,\nYour account is successfully credited with Rs." . $txn_amount. " Via Credit Card/Debit Card/Net Banking. Your reference id is $txnid\nYour current balance is Rs.$bal1";    		
    			$this->General->sendMessage($ret_mobile,$msg,'shops');
    			$this->General->sendMails($mail_subject, $mail_body , array("tadka@mindsarray.com","limits@mindsarray.com"),'mail');
    			return array('status'=>'success','code'=>'','description'=>$msg);
    		}
    	}
    	catch (Exception $e){
    		return array('status'=>'failure','code'=>$e->getCode(),'description'=>$e->getMessage());
    	}
    }
	
    function update_payu_Transaction($trans_id,$data=array()){
        $tablename = "pg_payuIndia";
        $userObj = ClassRegistry::init('Retailer');
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
            	if(empty($val))continue;
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $userObj->query("UPDATE $tablename SET $update_data WHERE shop_transaction_id='$trans_id'");
        }
    }
   
    function check_payu_hash($data){
        $key = PAYU_KEY;
        $salt = PAYU_SALT;
        $status = $this->General->set_defaultBlank($data,'status');
        $email = $this->General->set_defaultBlank($data,'email');
        $firstname = $this->General->set_defaultBlank($data,'firstname');
        $productinfo = $this->General->set_defaultBlank($data,'productinfo');
        $amount = $this->General->set_defaultBlank($data,'amount');
        $txnid = $this->General->set_defaultBlank($data,'txnid');            
        $dstr = $salt."|".$status."|||||||||||".$email."|".$firstname."|".$productinfo."|".$amount."|".$txnid."|".$key;        
        //$dstr = $salt."|".$data['status']."|||||||||||".$data['email']."|".$data['firstname']."|".$data['productinfo']."|".$data['amount']."|".$data['txnid']."|".$key;        
        $internal_hash = hash("sha512",$dstr);
        if($internal_hash == $data['hash']){
            return true;
        }
        return false;
    }
	
	function getLastTransactions($date,$page=1,$service = null,$date2=null,$itemsPerPage=0,$retailerId=null,$operatorId=0,$is_page_wise=1){
		if(empty($date)){
			$date = date('Y-m-d');
		}
		else $date = date('Y-m-d',strtotime($date));

		if(empty($date2)){
			$date2 = $date;
		}
		else $date2 = date('Y-m-d',strtotime($date2));

		if($date2 > date('Y-m-d',strtotime($date . ' + 30 days'))){
			$date2 = date('Y-m-d',strtotime($date . ' + 30 days'));
		}
		$next_day = date('Y-m-d',strtotime($date . ' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));

		if($itemsPerPage == 0){
			$itemsPerPage = PAGE_LIMIT;
		}

		$limit = ($page-1)*$itemsPerPage > 0 ? ($page-1)*$itemsPerPage : 0;

		$retailObj = ClassRegistry::init('Slaves');
		if(is_null($retailerId)){
			$retailer = $_SESSION['Auth']['id'];
		}else{
			$retailer = $retailerId;
		}
		if(empty($operatorId)){
			$operatorIdQry = "";
		}else{
			$operatorIdQry = "AND vendors_activations.product_id = ".$operatorId;
		}
		$qryLimitPart = "";
		if($service == null){
            if($is_page_wise != 0){
               $qryLimitPart =  " LIMIT $limit," . $itemsPerPage;
            }
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id , vendors_activations.cause ,vendors_activations.code, sum(complaints.resolve_flag) as resolve_flag
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					inner join shop_transactions on shop_transactions.id = vendors_activations.shop_transaction_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id where vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry 
					group by vendors_activations.id 
					order by vendors_activations.id desc ".$qryLimitPart);
			$ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , shop_transactions where shop_transactions.id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry ");
			$ret_count = $ret_cnt_res[0][0]['cnt'];
		}else {
			$ret = $retailObj->query("SELECT opening_closing.opening,opening_closing.closing,products.name,services.name,vendors_activations.ref_code,
vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,
					vendors_activations.status,vendors_activations.cause,vendors_activations.code, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					inner join opening_closing on opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where products.service_id = $service AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  
					group by vendors_activations.id 
					order by vendors_activations.id desc,opening_closing.id desc " . $qryLimitPart);
			$ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , opening_closing where opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  ");
			$ret_count = $ret_cnt_res[0][0]['cnt'];
		}
		$more = 0;
		if(count($ret) == $itemsPerPage){
			$more = 1;
		}
               
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day,'more' => $more,'total_count' => $ret_count);
	}
	
	function lastten($service){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,
				vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag
				FROM products
				inner join services on products.service_id = services.id
				inner join vendors_activations on products.id = vendors_activations.product_id
				left join complaints on complaints.vendor_activation_id = vendors_activations.id where products.service_id = $service AND vendors_activations.retailer_id =  $retailer AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."'
				group by vendors_activations.id
				order by vendors_activations.id desc LIMIT 0,10");
		return array('ret' => $ret);
	}
	
	function lastFiveTransactions(){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$transactions = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,
				vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, 
				trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,
				sum(complaints.resolve_flag) as resolve_flag
				FROM products
				inner join services on products.service_id = services.id
				inner join vendors_activations on products.id = vendors_activations.product_id
				left join complaints on complaints.vendor_activation_id = vendors_activations.id 
				where vendors_activations.retailer_id = $retailer 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."'
				group by vendors_activations.id
				order by vendors_activations.id desc LIMIT 0,5");
		return $transactions;
	}
	
	function mobileTransactions($mobile,$service=null){
		if(!in_array($service,array('2','6')))$mobile = substr($mobile,-10);
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = isset($_SESSION['Auth']['id']) ? $_SESSION['Auth']['id'] : "";
		$limit = 0;
		if(empty($service)){
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations on products.id = vendors_activations.product_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where vendors_activations.retailer_id = $retailer AND vendors_activations.mobile = '$mobile' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
					group by vendors_activations.id
					order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		else {
			$q = 'vendors_activations.mobile';
			if($service == '2' || $service == '6')
			$q = 'vendors_activations.param';
			
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,
					vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
					FROM products 
					inner join services on products.service_id = services.id
					inner join vendors_activations use INDEX (idx_ret_date) on products.id = vendors_activations.product_id
					left join complaints on complaints.vendor_activation_id = vendors_activations.id 
					where products.service_id = $service AND vendors_activations.retailer_id = $retailer AND ".$q." like '%$mobile%' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
					group by vendors_activations.id
					order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		return array('ret' => $ret);
	}
	
	function lastThreeTopups($mobile=null){
		if(empty($mobile)) return null;
		$Retailer = ClassRegistry::init('Slaves');
		
		$transactions = $Retailer->query("select * 
						from shop_transactions st
						left join retailers r on r.id = st.ref2_id
						where st.ref1_id = r.parent_id
						and st.ref2_id = r.id
						and st.type = 2
						and r.mobile = '$mobile'
						order by st.timestamp desc
						limit 3");
		
		return $transactions;
	}
	
	function searchTransactionsHistory($mob_subid, $retailer_id){
		$retailObj = ClassRegistry::init('Slaves');
		$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,
					vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, 
					trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,
					sum(complaints.resolve_flag) as resolve_flag 
				FROM products 
				inner join services on products.service_id = services.id 
				inner join vendors_activations on products.id = vendors_activations.product_id 
				left join complaints on complaints.vendor_activation_id = vendors_activations.id  
				where vendors_activations.retailer_id = $retailer_id 
				AND (vendors_activations.mobile = '$mob_subid' OR vendors_activations.param = '$mob_subid') 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
				order by vendors_activations.id desc");
	
		return $ret;
	}
	
	function searchTransactions($mob_subid,$retailer_id){
		$retailObj = ClassRegistry::init('Slaves');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.id,vendors_activations.ref_code,vendors_activations.param,
				vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, vendors_activations.timestamp,
				vendors_activations.product_id 
				FROM products,services,vendors_activations 
				where products.service_id = services.id 
				AND products.id = vendors_activations.product_id 
				AND vendors_activations.retailer_id = $retailer_id 
				AND (vendors_activations.mobile = '$mob_subid' OR vendors_activations.param = '$mob_subid') 
				AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' 
				order by vendors_activations.id desc 
				LIMIT 2");
		
		return $ret;
	}
	
	function complaintStats($retailer_id, $date){
		if(!$date){
			$date = date('Y-m-d');
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));
		$retailObj = ClassRegistry::init('Slaves');
		$complaints = $retailObj->query("SELECT count(complaints.resolve_flag) as count, complaints.resolve_flag
							from complaints  
                         	inner join vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         	inner join retailers  on(vendors_activations.retailer_id=retailers.id) 
                            where complaints.in_date >= '$prev_day'
                            AND vendors_activations.retailer_id = $retailer_id 
                            group by complaints.resolve_flag");	
		return $complaints;
	}
	
	function reversalTransactions($date,$service = null){
        $page = 1;
		if(empty($date)){
			$date = date('Y-m-d');
		}
		
		$next_day = date('Y-m-d',strtotime($date.' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));
		
		//return array('ret' => array(),'today' => $date,'prev' => $prev_day,'next' => $next_day);
		
		$limit = ($page-1)*PAGE_LIMIT;
		
		
		$retailer = $_SESSION['Auth']['id'];
		//$type_sql = " AND (vendors_activations.status = ".TRANS_REVERSE." OR vendors_activations.status = ".TRANS_REVERSE_PENDING." OR vendors_activations.status = ".TRANS_REVERSE_DECLINE.")";
		
		if($service == null || empty($service)){
                                /*"SELECT 
                                    products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id 
                                FROM 
                                    products,services,vendors_activations
                                where 
                                    products.service_id = services.id 
                                AND products.id = vendors_activations.product_id 
                                AND vendors_activations.retailer_id =  $retailer 
                                AND vendors_activations.date='$date' $type_sql 
                                order by 
                                    vendors_activations.id desc
                                "*/
                    $panelQ = "
			SELECT 
                                products.name,services.name,vendors_activations.ref_code,vendors_activations.param,
                                vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag 
                                	
                         from 
                                        complaints  
                         inner  join    vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         inner  join    retailers  on(vendors_activations.retailer_id=retailers.id) 
                         inner  join    products   on(products.id=vendors_activations.product_id)                                 
                         inner join    services     on(products.service_id=services.id)              
                        
				where   
                                
                                complaints.in_date ='$date'
                                
                                AND vendors_activations.retailer_id =  $retailer 
                                group by vendors_activations.id
                                order by complaints.id desc";
                    
                        //AND complaints.resolve_flag = 0  
		}
		else {
			//$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date='$date' $type_sql order by vendors_activations.id desc");
                        $panelQ = "
			SELECT 
                                products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id,sum(complaints.resolve_flag) as resolve_flag
                                	
                         from 
                                        complaints  
                         inner  join    vendors_activations USE INDEX (PRIMARY) ON (complaints.vendor_activation_id = vendors_activations.id) 
                         inner  join    retailers  on(vendors_activations.retailer_id=retailers.id) 
                         inner  join    products   on(products.id=vendors_activations.product_id)                                 
                         inner  join    services     on(products.service_id=services.id)                  
                        
				where   
                                
                                complaints.in_date ='$date'
                                AND services.id = $service
                                AND vendors_activations.retailer_id =  $retailer 
                                group by vendors_activations.id
                                order by complaints.id desc";
                    
                }
                $retailObj = ClassRegistry::init('Slaves');
                $ret = $retailObj->query($panelQ);
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day);
	}
	
	function updateSlab($slab_id,$shop_id,$group_id){
		$slabObj = ClassRegistry::init('SlabsUser');
		$slabObj->create();
		$slabData['SlabsUser']['slab_id'] = $slab_id;
		$slabData['SlabsUser']['shop_id'] = $shop_id;
		$slabData['SlabsUser']['group_id'] = $group_id;
		$slabData['SlabsUser']['timestamp'] = date('Y-m-d H:i:s');
		$slabObj->save($slabData);
	}
	
	function getBanks(){
		$data = $this->getMemcache('banks');
		
		if($data === false){
			$retailObj = ClassRegistry::init('Retailer');
			$data = $retailObj->query("Select `bank_name` from bank_details where status = '1'");
			$this->setMemcache('banks',$data,2*24*60*60);
		}
        
		return $data;
	}
	
	function addNewVendorRetailer($vendor_id,$retailer_code){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("INSERT INTO vendors_retailers (vendor_id,retailer_code) VALUES ($vendor_id,'$retailer_code')");
	}
	
	function apiAccessHashCheck($params,$partnerModel){
		$pass = $partnerModel['Partner']['password'];
		$hashGen = sha1($params['partner_id'].$params['trans_id'].$pass);//.$params['info_json']
		if(!empty ($params['hash_code']) && strtolower($params['hash_code']) == strtolower($hashGen)){
			return true;
		}else{
			return false;
		}
	}
    function appRechargeAccessHashCheck($uuid, $mobDthNo , $amt ,$timestamp,$hash){
        $hashGen = sha1($uuid.$mobDthNo.$amt.$timestamp);//.$params['info_json']
		if(!empty ($hash) && strtolower($hash) == strtolower($hashGen)){
			return true;
		}else{
			return false;
		}
	}

	function apiAccessIPCheck($partner){
		$result = array();
		$ipAddrsStr = $partner['Partner']['ip_addrs'];// get partener's valid ip list
		$ipArr = explode(",", $ipAddrsStr);
		$ip = $_SERVER['REMOTE_ADDR'];// get client ip
		if(in_array($ip, $ipArr)){//check client ip is in partner's ips list
			$result = array('access'=>true);
		}else{
			$result = array('access'=>false);
		}
		return $result;
	}
	function apiAccessPartnerOperatorCheck($partnerAccno,$operatorId){//check operator is open for a partner (only for recharge api)
		$result = array();
		$retailObj = ClassRegistry::init('Slaves');
		$result = $retailObj->query("select * from  partner_operator_status where partner_acc_no = '".$partnerAccno."' and operator_id = '$operatorId'");
                
		if(empty($result) || $result[0]['partner_operator_status']['status'] == 0){//check client ip is in partner's ips list
			$result = array('access'=>false);
		}else{
			$result = array('access'=>true);
		}
		return $result;
	}
	
	function checkToken($tokenkey = null) {
		$redis = $this->redis_connect();
		$value = false;
		if ($tokenkey != null) {
			$value = $redis->hexists("token", $tokenkey);
		}
                $redis->quit();
		return $value;
	}
	
	function setToken($tokenkey = null) {
		$redis = $this->redis_connect();
		if ($tokenkey != null) {
			$redis->hset("token", $tokenkey, 1);
		}
                $redis->quit();
	}

	function delToken($tokenvalue) {
		$redis = $this->redis_connect();
		$deltoken = $redis->hdel("token", $tokenvalue);
                $redis->quit();
		return $deltoken;
	}

	/*function partnerSessionInit( $type , $model ){ //$id ,
		// type -> user , partner , retailer , distributor , rm
		// $model -> Model Object of  ( user , partner , retailer , distributor , rm )
		$data = array();
		if($type == "user"){
			$data = $this->User->query("SELECT id FROM users WHERE mobile = '".$params['mobile']."' AND password='$password'");
		}else if($type == "partner"){
			$partnerModel = $model;
			$retailerRegObj = ClassRegistry::init('Retailer');
			$retailerModel = $retailerRegObj->findById($partnerModel['Partner']['retailer_id']);
			if(!empty($retailerModel)){
				$data['user_id'] = $retailerModel['Retailer']['user_id'];
				$data['group_id'] = RETAILER;
			}
		}

		if(empty($data)){// Invalid Partner ( can't find retailer account for partner ).
			return false;
		}else{
			$info = $this->getShopData($data['user_id'],$data['group_id']);
			$info['User']['group_id'] = $data['group_id'];
			$info['User']['id'] = $data['user_id'];
			//$this->SessionComponent->write('Auth',$info);
			$_SESSION['Auth'] = $info;
			if(!empty($_SESSION['Auth']['id'])){
				return true;
			}else{
				return false;
			}
		}
	}*/
    
    /**
     * 
     * @param type $query : string params
     * @param type $vendor_id : modem ID
     */
    function async_request_via_redis($query,$vendor_id){
        $redis = $this->openservice_redis();
        $queuename="updateIncoming_{$vendor_id}";
        $queuevalue = $query;
        $redis->lpush($queuename,$query);
    }
    
    function deleteDocument($src){
    	$Retailer = ClassRegistry::init('Retailer');
    	$objectKey = explode("/", $src);
    	App::import('vendor', 'S3', array('file' => 'S3.php'));
    	$bucket = 'pay1bucket';
    	$s3 = new S3(awsAccessKey, awsSecretKey);
    	if($s3->deleteObject($bucket, $objectKey[3])){
    		if($Retailer->query("DELETE FROM  retailers_details where image_name = '".$src."'")){
    			return "Image deleted";
    		}
    		else 
    			return $src." not deleted from db";
    	}
    	else {
    		return $src." not deleted from aws";
    	}
    }
    
    function deleteImageFromAWS($src){
    	$objectKey = explode("/", $src);
    	App::import('vendor', 'S3', array('file' => 'S3.php'));
    	$bucket = 'pay1bucket';
    	$s3 = new S3(awsAccessKey, awsSecretKey);
    	if($s3->deleteObject($bucket, $objectKey[3]))
    		return true;
    	else 
    		return false;
    }
    
    function removeDocument($retailer_id, $section_id, $verify_flag){
    	$map = $this->kycSectionMap($section_id);
    	$Retailer = ClassRegistry::init('Retailer');
    	if($verify_flag == 1){
    		$verified_documents = $Retailer->query("select * from retailers_docs
    				where retailer_id = ".$retailer_id."
    				and type = '".$map['documents'][0]."'");
    		if(($section_id == 3 && count($verified_documents) > 3) || in_array($section_id, array(1, 2))){
    			$documents = $Retailer->query("select * from retailers_details
    					where image_name = '".$verified_documents[0]['retailers_docs']['src']."'");
    			$Retailer->query("delete from retailers_docs
    					where id = ".$verified_documents[0]['retailers_docs']['id']);
    			if(count($documents) == 0){
    				$this->deleteImageFromAWS($verified_documents[0]['retailers_docs']['src']);
    			}
    		}
    	}
    	else {
    		$documents = $Retailer->query("select * from retailers_details
    				where retailer_id = ".$retailer_id."
    				and type = '".$map['documents'][0]."'");
    		if(($section_id == 3 && count($documents) > 3) || in_array($section_id, array(1, 2))){
    			$verified_documents = $Retailer->query("select * from retailers_docs
    					where src = '".$documents[0]['retailers_details']['image_name']."'");
    			$Retailer->query("delete from retailers_details
    					where id = ".$documents[0]['retailers_details']['id']);
    			if(count($verified_documents) == 0){
    				$this->deleteImageFromAWS($documents[0]['retailers_details']['src']);
    			}
    		}
    	}
    }
    
    /**
     * KYC States
     * 0 => Submitted
     * 1 => Rejected
     * 2 => Approved
     * 3 => Unverified
     */
    
    function setKYCState($retailer_id, $section_id, $state, $reason = null){
    	$Retailer = ClassRegistry::init('Retailer');
    	switch($state){
    		case 0:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(empty($retailers_kyc_states)){
    				$map = $this->kycSectionMap($section_id);
    				$types = implode(",", $map['documents']);
    				$documents = $Retailer->query("select * 
    						from retailers_details rd
    						where rd.type in ('".$types."')
    						and rd.retailer_id = ".$retailer_id);
    				if(!empty($documents)){
    					$Retailer->query("insert into retailers_kyc_states
									(retailer_id, section_id, verified_state, document_state, document_timestamp, document_date)
									values(".$retailer_id.", ".$section_id.", '0', '0', '".date('Y-m-d H:i:s')."', '".date('Y-m-d')."')");
    				}
    			}
    			else {
    				$Retailer->query("update retailers_kyc_states
									set document_state = '0',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								comment = ''
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    			}
    			break;
    		case 1:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(!empty($retailers_kyc_states)){
    				$Retailer->query("update retailers_kyc_states
									set document_state = '1',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								comment = '$reason'
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    			}
    			break;
    		case 2:
    			$retailers_kyc_states = $Retailer->query("select *
								from retailers_kyc_states
								where retailer_id = ".$retailer_id."
								and section_id = ".$section_id);
    			if(!empty($retailers_kyc_states)){
    				$Retailer->query("update retailers_kyc_states
									set document_state = '2',
    								verified_state = '1',
									document_timestamp = '".date('Y-m-d H:i:s')."',
									document_date = '".date('Y-m-d')."',
    								verified_timestamp = '".date('Y-m-d H:i:s')."',
									verified_date = '".date('Y-m-d')."',
    								comment = ''
									where retailer_id = ".$retailer_id."
									and section_id = ".$section_id);
    				$map = $this->kycSectionMap($section_id);
    				$unverified_retailers = $Retailer->query("select * 
    						from unverified_retailers ur
    						where ur.retailer_id = ".$retailer_id);
    				
    				$update_query = "update retailers set ";
    				foreach($map['fields'] as $field){
    					if(!in_array($field, array('latitude', 'longitude')))
    						$update_query .= " $field = '".$unverified_retailers[0]['ur'][$field]."', "; 
    				}
    				$update_query .= " modified = '".date('Y-m-d H:i:s')."'
    						where id = ".$retailer_id;
    				$Retailer->query($update_query);
    				
    				if(in_array('latitude', $map['fields'])){
    					$user_profile = $Retailer->query("select r.*, up.*
    							from user_profile up
    							join retailers r on r.user_id = up.user_id
    							where r.id = ".$retailer_id." 
    							and up.device_type = 'online'
    							order by updated desc
    							limit 1");
    					if(!empty($user_profile)){
    						$Retailer->query("update user_profile
								set latitude = '".$unverified_retailers[0]['ur']['latitude']."',
								longitude = '".$unverified_retailers[0]['ur']['longitude']."',
								updated = '".date("Y-m-d H:i:s")."'
								where user_id = ".$user_profile[0]['up']['user_id']."
								and device_type = 'online'");
    					}
    					else {
    						$retailers = $Retailer->query("select * 
    								from retailers r
    								where r.id = ".$retailer_id);
    						$Retailer->query("insert into user_profile
								(user_id, gcm_reg_id, uuid, longitude, latitude, location_src, device_type,
								version , manufacturer, created, updated)
								VALUES (".$retailers[0]['r']['user_id'].", '".$retailers[0]['r']['mobile']."',
								'".$retailers[0]['r']['mobile']."', '".$unverified_retailers[0]['ur']['longitude']."',
								'".$unverified_retailers[0]['ur']['latitude']."', '' ,'online' ,'' ,'' ,
								'".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
    					}
    				}
    				
    				$types = implode(",", $map['documents']);
    				$retailers_details = $Retailer->query("select * 
    						from retailers_details rd
    						where retailer_id = ".$retailer_id." 
    						and type in ('".$types."')");
    				$Retailer->query("delete from retailers_docs
    						where retailer_id = ".$retailer_id." 
    						and type in ('".$types."')");
    				foreach($retailers_details as $rd){
    					$Retailer->query("insert into retailers_docs
    							(retailer_id, type, src, uploader_user_id)
    							values (".$rd['rd']['retailer_id'].", '".$rd['rd']['type']."', '".$rd['rd']['image_name']."',
    							".$rd['rd']['uploader_user_id'].")");
    				}
    			}
    			break;
    		case 3:
    			
    			break;
    	}
    	$this->setKYCScore($retailer_id);
    }
    
    function setKYCScore($retailer_id){
    	$Retailer = ClassRegistry::init('Retailer');
    	
    	$retailers_kyc_states = $Retailer->query("select * 
    			from retailers_kyc_states rks
    			where rks.retailer_id = ".$retailer_id);
    	$kyc_score = 0;
    	foreach($retailers_kyc_states as $rks){
    		switch($rks['rks']['section_id']){
    			case "1":
    				$rks['rks']['verified_state'] AND $kyc_score += 35;
    				break;
    			case "2":
    				$rks['rks']['verified_state'] AND $kyc_score += 40;
    				break;
    			case "3":
    				$rks['rks']['verified_state'] AND $kyc_score += 25;
    				break;
    		}
    	}
    	
    	$Retailer->query("update retailers
    			set kyc_score = ".$kyc_score.", 
    			modified = '".date('Y-m-d H:i:s')."'
    			where id = ".$retailer_id);
    }
	
	function errorCodeMapping($vendorId,$errorCode){
		
		$errorcode = array("8" => array(
										"0" => "13",
										"1" => "30",
										"2" => "30",
										"3" => "30",
										"4" => "8",
										"5" => "403",
										"6" => "30",
										"7" => "6",
										"8" => "5",
										"9" => "46",
										"10" => "4",
										"11" => "403",
										"12" => "30",
										"21" => "26",
										"23" => "5",
										"24" => "30",
										"25" =>  "30",
										"30" => "14",
										"32" => "30",
										"33" => "30",
										"37" => "30",
										"45" => "9",
										"50" => "30",
										"52" => "30",
										"53" => "30",
										"54" => "43",
										"81" => "34",
										"82" => "30",
										"88" => "30)",
										"224" => "43",
										"134" => "30",
										"137" => "30"
		                               ),
				            "58" => array(
											"TXN" => "13",
											"TUP" => "31",
											"RPI" => "4",
											"UAD" => "404",
											"IAC" => "30",
											"IAT" => "30",
											"AAB" => "30",
											"IAB" => "26",
											"ISP" => "8",
											"DID" => "30",
											"DTX" => "30",
											"IAN" => "46",
											"IRA" => "6",
											"DTB" => "30",
											"RBT" => "43",
											"SPE" => "43",
											"SPD" => "43",
											"UED" => "30",
											"IEC" => "30",
											"IRT" => "30",
											"ITI" => "30",
											"TSU" => "30",
											"IPE" => "30",
											"ISE" => "30",
											"TRP" => "30",
											"OUI" => "0",
											"ODI" => "30"
							             ),
										 
								"27" => array(
												"100" => "13",
												"99" => "14",
												"101" => "30",
												"102" => "26",
												"103" => "6",
												"104" => "30",
												"105" => "30",
												"106" => "30",
												"107" => "5",
												"110" => "6",
												"111" => "30",
												"121" => "1",
												"123" => "30",
												"165" => "15",
												"170" => "30",	
												"171" => "30",
												"173" => "43",
												"172" => "2",
												"174" => "30"
									          ),
									"24" =>  array(
													"1200" => "15",
													"1201" => "0",
													"1202" => "5",
													"1203" => "6",
													"1204" => "30",
													"1205" => "9",
													"1206" => "30",
													"1207" => "26",
													"1208" => "26",
													"1209" => "30",
													"1210" => "30",
													"1211" => "30",
													"1212" => "0"
												 ),
									"18" => array (
													"0" => "0",
													"1" => "31",
													"501" => "30",
													"502" => "4",
													"503" => "14",
													"504" => "0",
													"505" => "0",	
													"506" => "26",
													"507" => "30",
													"508" => "30",
													"509" => "30",
													"510" => "2"
												  )
								             );
		
		return $errorcode[$vendorId][$errorCode];
				
	
	}

              function isStrongPassword($password)
                    {
                        if(!empty($password)):
                               // if(strlen($password)<5 || !preg_match( '~[A-Z]~', $password) ||  !preg_match( '~[a-z]~', $password) ||  !preg_match( '~[0-9]~', $password)):
				if(in_array($password,array('1010','1111','9999','1234','0000','4321','2222','4444','5555','6666','7777','8888','3333'))):
                                        return false;
                                else:
                                        return true;
                                endif;
                        endif;
                        
                        return false;
                    }


}
?>
