<?php
 
class MposController extends AppController{
	
	public $helpers = array();
	public $components = array('Shop', 'Auth', 'General');
	public $uses = array('User', 'Retailer', 'Slaves');	
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('*');
	}
	
	function mpos_columns(){
		$mpos_columns = $this->Shop->getMemcache("mpos_transactions_table_columns");
		if(empty($mpos_columns)){
			$mpos_transactions_table_columns = $this->User->query("select column_name
														from information_schema.columns
														where table_schema = 'shops'
		    											and table_name = 'mpos_transactions';");
			$mpos_columns = array();
			foreach($mpos_transactions_table_columns as $columns){
				$mpos_columns[] = $columns['columns']['column_name'];
			}
				
			$this->Shop->setMemcache("mpos_transactions_table_columns", $mpos_columns, 24 * 60 * 60);
		}
		
		return $mpos_columns;
	}
	
	function api(){
		$method = $this->params["pass"][0];
		
		$this->log();
		
		$this->send($this->$method());
		
		$this->autoRender = false;
	}
	
	function send($response){
		echo json_encode($response);
	}
	
	function log(){
		$filename = "EzetapNotificationAPI_".date('Ymd').".txt";
		$this->General->logData('/mnt/logs/'.$filename, "Request::".json_encode($_REQUEST));
		
		$request_body = file_get_contents('php://input');
		
		$this->General->logData('/mnt/logs/'.$filename, "Request Body::".$request_body);
	}
	
	function notification(){
		$dataSource = $this->User->getDataSource();
		
		$request_body = file_get_contents('php://input');
		
		$response = json_decode($request_body, true);
		$response["ezetap_id"] = $response["id"];
		unset($response["id"]);
		$response["setting"] = json_encode($response["setting"]);
		$response["apps"] = json_encode($response["apps"]);
		$response["taskList"] = json_encode($response["taskList"]);
		$response["states"] = json_encode($response["states"]);
		
		$mpos_transactions = $this->User->query("select * 
								from mpos_transactions 
								where shop_transaction_id = ".$response['externalRefNumber']);
		if(!empty($mpos_transactions)){
			if(empty($mpos_transactions[0]['mpos_transactions']['ezetap_id'])){
				try{
					$dataSource->begin();
					
					$update_query = "update mpos_transactions set ";
					foreach($response as $key => $r){
						if(in_array($key, $this->mpos_columns()))
							$update_query .= $key." = '".$r."', ";
					}
					$update_query .= " created = '".date('Y-m-d H:i:s')."' 
							where shop_transaction_id = ".$response['externalRefNumber'];
					$this->User->query($update_query);
					
					$dataSource->commit();
				}
				catch(Exception $e){
					$dataSource->rollback();
				}
			}	
		}
		
		return $response;
	}
	
	function updateBalance($retailer_id, $amount, $shop_transaction_id){
		$filename = "EzetapNotificationAPI_".date('Ymd').".txt";
		$this->General->logData('/mnt/logs/'.$filename, "inside updateBalance::".json_encode(array($retailer_id, $amount, $shop_transaction_id)));
		$shop_transactions = $this->User->query("select *
						from shop_transactions st
						where st.id = ".$shop_transaction_id);
		if($shop_transactions[0]['st']['ref2_id'] == '57'){
			$cash_out_incentive = $this->General->findVar("MPOS_CASH_OUT_INCENTIVE");
			$this->Shop->shopTransactionUpdate(MPOS_INCENTIVE, $cash_out_incentive, $retailer_id, $shop_transaction_id, null, null, null, $shop_transactions[0]['st']['note']);
			
			$amount_to_retailer = $amount + $cash_out_incentive;
			$balance = $this->Shop->shopBalanceUpdate($amount_to_retailer, 'add', $retailer_id, RETAILER);
			$this->Shop->addOpeningClosing($retailer_id, RETAILER, $shop_transaction_id, $balance - $amount_to_retailer, $balance);
			$message = "Cash Out successful. Rs. ".$amount." is added to your balance.";
		}
		else {
			$message = "Sale complete. Rs. ".$amount." will be transferred to your bank account in 48 Hrs.";
		}
		$this->General->logData('/mnt/logs/'.$filename, "inside updateBalance message::".$message);
		$retailers = $this->User->query("select mobile from retailers where id = ".$retailer_id);
		$this->General->logData('/mnt/logs/'.$filename, "inside updateBalance retailers::".json_encode($retailers));
		$this->General->sendMessage($retailers[0]['retailers']['mobile'], $message, 'payone');
		$this->General->logData('/mnt/logs/'.$filename, "inside updateBalance after send message::".$retailers[0]['retailers']['mobile']);
		return;
	}
	
	function createTransaction($params){
		$dataSource = $this->User->getDataSource();
		
		$filename = "EzetapNotificationAPI_".date('Ymd').".txt";
		$this->General->logData('/mnt/logs/'.$filename, "App response after transaction::".json_encode($params));
		
		$retailer_id = $this->Session->read('Auth.id');
		
		$product_id = $params['product_id'];
		$amount = $params['amount'];
		$type_flag = 0;
		
		if($product_id == '56')
			$type_flag = 2;	
		
		$retailers = $this->User->query("select *
				from retailers r
				left join retailers_services rs on rs.retailer_id = r.id and rs.service_id = 8
				where r.id = ".$retailer_id."
				group by r.id");
		
		if(empty($retailers[0]['rs']['service_id'])){
			return array("status" => "failure", "description" => "Mini-ATM service is deactivated.");
		}
		if(empty($retailers[0]['r']['device_serial_no'])){
			return array("status" => "failure", "description" => "Device serial no. not found.");
		}
		
		$dataSource->begin();
		try{
			$this->User->query("insert into shop_transactions
				(ref1_id, ref2_id, amount, type, type_flag, timestamp, date)
				values ('$retailer_id', '$product_id', '$amount', '".MPOS_TRANSFER."', '$type_flag', '".date('Y-m-d H:i:s')."', '".date('Y-m-d')."')");
			$this->General->logData('/mnt/logs/'.$filename, "after insert into shop_transactions query::"."insert into shop_transactions
				(ref1_id, ref2_id, amount, type, type_flag, timestamp, date)
				values ('$retailer_id', '$product_id', '$amount', '".MPOS_TRANSFER."', '$type_flag', '".date('Y-m-d H:i:s')."', '".date('Y-m-d')."')");
			$shop_transactions = $this->User->query("SELECT LAST_INSERT_ID() as id");
			
			$this->User->query("insert into mpos_transactions
				(shop_transaction_id, status)
				values('".$shop_transactions[0][0]['id']."', 'INITIATED')");
			$this->General->logData('/mnt/logs/'.$filename, "insert into mpos_transactions::".json_encode($shop_transactions));
			$mpos_transactions = $this->User->query("select id
					from mpos_transactions
					where shop_transaction_id = ".$shop_transactions[0][0]['id']);
			
			$dataSource->commit();
			$this->General->logData('/mnt/logs/'.$filename, "before response::".json_encode(array(	"status" => "success", 
							"description" => array(
								"shop_transaction_id" => $shop_transactions[0][0]['id'],
								"mpos_transaction_id" => $mpos_transactions[0]['mpos_transactions']['id']
							)					
						)));
			return array(	"status" => "success", 
							"description" => array(
								"shop_transaction_id" => $shop_transactions[0][0]['id'],
								"mpos_transaction_id" => $mpos_transactions[0]['mpos_transactions']['id']
							)					
						);
		}
		catch(Exception $e){
			$dataSource->rollback();
			
			return array("status" => "failure", "description" => "Could not initiate transaction. Try again.");
		}
		
		$this->autoRender = false;
	}
	
	function completeTransaction($params){
		$filename = "EzetapNotificationAPI_".date('Ymd').".txt";
		$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction::".json_encode($params));
		
		$dataSource = $this->User->getDataSource();
		
		$retailer_id = $_SESSION['Auth']['id'];
		$shop_transaction_id = $params['shop_transaction_id'];
		$mpos_response = json_decode($params['card_transaction_response'], true);
		$confirm_flag = 0;
		
		$dataSource->begin();
		try{
			$response = array();
			
			if($mpos_response['status'] == 'success'){
				$response['success'] = true;
				$response['chargeSlipDate'] = $mpos_response['result']['receipt']['receiptDate'];
				$response['receiptUrl'] = $mpos_response['result']['receipt']['receiptUrl'];
				$response['customerEmail'] = $mpos_response['result']['customer']['email'];
				$response['customerMobile'] = $mpos_response['result']['customer']['mobileNo'];
				$response['customerName'] = $mpos_response['result']['customer']['name'];
				$response['amount'] = $mpos_response['result']['txn']['amount'];
				$response['paymentMode'] = $mpos_response['result']['txn']['paymentMode'];
				$response['authCode'] = $mpos_response['result']['txn']['authCode'];
				$response['currencyCode'] = $mpos_response['result']['txn']['currencyCode'];
				$response['mid'] = $mpos_response['result']['txn']['mid'];
				$response['postingDate'] = $mpos_response['result']['txn']['txnDate'];
				$response['deviceSerial'] = $mpos_response['result']['txn']['deviceSerial'];
				$response['merchantName'] = $mpos_response['result']['merchant']['merchantName'];
				$response['merchantCode'] = $mpos_response['result']['merchant']['merchantCode'];
				$response['formattedPan'] = $mpos_response['result']['cardDetails']['maskedCardNo'];
				$response['paymentCardBrand'] = $mpos_response['result']['cardDetails']['cardBrand'];
				$response['status'] = "ACCEPTED";

				$confirm_flag = 1;
			}
			else {
				$response['success'] = false;
				$response['error']= json_encode($mpos_response['error']);
				$response['status'] = $mpos_response['status'];
			}
			
			$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction::".json_encode($response));
			
			$update_query = "update mpos_transactions set ";
			foreach($response as $key => $r){
				if(in_array($key, $this->mpos_columns()))
					$update_query .= $key." = '".$r."', ";
			}
			$update_query .= " created = '".date('Y-m-d H:i:s')."' 
					where shop_transaction_id = ".$shop_transaction_id;
			$this->User->query($update_query);
			$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction after update::".$update_query);
			
			$this->User->query("update shop_transactions st
					left join mpos_transactions mt on mt.shop_transaction_id = st.id
					set st.note = mt.id,
					st.confirm_flag = $confirm_flag
					where st.id = ".$shop_transaction_id);
			
			if($mpos_response['status'] == 'success'){
				$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction before update balance::".json_encode(array($retailer_id, $response['amount'], $shop_transaction_id)));
				$this->updateBalance($retailer_id, $response['amount'], $shop_transaction_id);
			}
			$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction after update balance::".json_encode($mpos_transactions));
			$dataSource->commit();
			$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction after update::".json_encode($mpos_transactions));
			
			if($mpos_response['status'] == 'success'){
				return array("status" => "success", "description" => "Transaction completed successfully.");
			}
			else {
				return array("status" => "failure", "description" => $mpos_response['error']['message']);
			}
		}
		catch(Exception $e){
			$dataSource->rollback();
			$this->General->logData('/mnt/logs/'.$filename, "inside completeTransaction exception::".$e->getMessage());
			return array("status" => "failure", "description" => "Could not complete transaction. Try again.");
		}
	}
	
	function transactionsHistory($params){
		$filename = "EzetapNotificationAPI_".date('Ymd').".txt";
		$this->General->logData('/mnt/logs/'.$filename, "inside transactionsHistory::".json_encode($params));
		
		$retailer_id = $_SESSION['Auth']['id'];
		$date = $params['date'];
		
		$transactions = $this->User->query("select * 
				from shop_transactions st
				left join mpos_transactions mt on mt.shop_transaction_id = st.id
				where st.ref1_id = ".$retailer_id."
				and st.date = '$date'
				and type = ".MPOS_TRANSFER."
				order by st.timestamp desc");
		
		$this->General->logData('/mnt/logs/'.$filename, "inside transactionsHistory query::"."select * 
				from shop_transactions st
				left join mpos_transactions mt on mt.shop_transaction_id = st.id
				where st.ref1_id = ".$retailer_id."
				and st.date = '$date'
				and type = ".MPOS_TRANSFER."
				order by st.timestamp desc");
		
		if(empty($transactions)){
			return array("status" => "failure", "description" => "No transactions found.");
		}
		
		$report = array();
		foreach($transactions as $t){
			$transaction = array();
			$transaction['transaction_id'] = $t['st']['id'];
			$transaction['amount'] = $t['st']['amount'];
			$transaction['datetime'] = $t['st']['timestamp'];
			$transaction['txnType'] = $t['mt']['txnType'];
			$transaction['cardType'] = $t['mt']['cardType'];
			$transaction['issuerCode'] = $t['mt']['issuerCode'];
			$transaction['receiptUrl'] = $t['mt']['receiptUrl'];
			$transaction['status'] = $t['mt']['status'];
			if($t['st']['ref2_id'] == "56"){
				$transaction['txnMode'] = 1;
			}
			else {
				$transaction['txnMode'] = 0;
			}
			if(in_array($t['mt']['status'], array("ACCEPTED", "AUTHORIZED", "SUCCESS"))){
				$transaction['status_flag'] = 1;
			}
			else {
				$transaction['status_flag'] = 0;
			}
			
			$report[] = $transaction;
		}
		$this->General->logData('/mnt/logs/'.$filename, "inside transactionsHistory::".json_encode($report));
		return array("status" => "success", "description" => $report);
	}
	
	function settle(){
		if(!isset($_SESSION['Auth']['User'])){
		
		  $this->redirect('/');
		}
		$read_date = $_POST['date'] ? $_POST['date'] : date('d-m-Y');
		$date = date('Y-m-d', strtotime($read_date));
		
		$transactions = $this->Slaves->query("select *
				from shop_transactions as st
				left join shop_transactions as sst ON (st.id = sst.ref2_id)
				left join mpos_transactions as mt ON (st.id = mt.shop_transaction_id)
				left join retailers r on r.id = st.ref1_id
				left join unverified_retailers ur on ur.retailer_id = r.id
				left join distributors d on d.id = r.parent_id
				where st.date = '$date'
				and st.type = ".MPOS_TRANSFER."
				and mt.status in ('ACCEPTED', 'AUTHORIZED', 'SUCCESS')");
			
		$this->set('transactions', $transactions);
		$this->set('date', $read_date);	
		
		$this->layout = 'sims';
	}
}
?>	