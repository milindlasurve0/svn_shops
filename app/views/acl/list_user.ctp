<link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" />
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>  
<script>$(document).ready(function() {$('#listusers').DataTable();} );</script>
<style>div#listusers_filter{float: right;font-size: 12px;}div#listusers_length,div#listusers_info{font-size: 12px;}select.input-sm{height:22px;padding:0px;}</style>
<div class="col-lg-12">
        <div class="panel panel-pay1">
            <div class="panel-heading">List User</div>
            <div class="panel-body">
                <table class="table table-condensed table-hover table-striped table-bordered" id="listusers">
                    <thead>
                    <tr>
                    <th>Id</th>    
                    <th>User Name</th>
                     <th>Mobile Number</th>
                    <th>Group</th>
                    <th><a  href="/acl/add/">Add User</a> | <a  href="/acl/addGroup/">Add Group</a> </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($userData as $val): ?>

                    <tr>

                    <td><?php echo $val['users']['id'] ?></td>
                    <td><?php echo $val['users']['username'] ?></td>
                    <td><?php echo $val['users']['mobile'] ?></td>
                    <td><?php echo $val[0]['groups'] ?></td>
                    <td><a href="/acl/edit/<?php echo $val['users']['id'];?>">Edit</a>
                    </td>
                    </tr>

                    <?php endforeach; ?>


                    </tbody>

                    </table>
            </div>
        </div>
  </div>