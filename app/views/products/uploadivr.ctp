<?php echo  $this->element('product_sidebar'); ?>
<div class="padding-top-10" style="float:left; width: 75%; margin-left: 20px;">
    <?php echo $this->Session->flash('form1');?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Upload IVR MP3 File
        </div>
        <div class="container" style="margin:10px">
            <form id="vendor_operator_map" class="form" role="form" action="" method="post" enctype="multipart/form-data">
                <div class="form-group" style="width:50%">
                  <label for="ivrfile">IVR MP3 File (silence) </label>
                 <input type="file" name="ivrfile" id="ivrfile"  class="form-control" accept=".mp3"/>
                </div>
                <div class="form-group" style="width:50%">
                  <label for="ivrfile2">IVR MP3 File (silence1) </label>
                 <input type="file" name="ivrfile2" id="ivrfile2"  class="form-control" accept=".mp3"/>
                </div>
                <div class="form-group" style="width:50%">
                  <label for="ivrfile">IVR config File </label>
                 <input type="file" name="ivrconf" id="ivrconf"  class="form-control" accept=".conf"/>
                </div>
                <button type="submit" class="btn-lg btn-primary">Submit</button>
<!--                <a href="/products/downloadivr" target="_blank" class="btn btn-info btn-md" style="margin-left: 10px" title="Download IVR File to edit"><span class="glyphicon glyphicon-download-alt"> </span></a>-->
            </form>
            
        </div>
    </div>
</div>

