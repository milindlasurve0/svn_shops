
	  			<fieldset style="padding:0px;border:0px;margin:0px;">
	  				
				<div class="appTitle" style="margin-top:10px;">All <?php echo $modelName; ?>s</div>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="ListTable" summary="Transactions">
        			<thead>
        			<?php $refund =0; ?>
			          <tr class="noAltRow altRow" >
			          	<th style="width:20px;">Sr. No.</th>
                                        <th style="width:20px;">ID</th>
			            <th style="width:135px;">Name</th>
			            <th style="width:40px;">Mobile</th>
						 <th style="width:40px;">Alternate Mobile</th>
                                    <th style="width:40px;">Reference</th>             
			             <?php //if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) {?>
			            <!--<th style="width:40px;">Type</th>-->
			            <?php //} ?>
			            <th class="number" style="width:40px;">Margin(%)</th>
			            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
			            <th class="number" style="width:75px">RM Name</th>
			            <?php } ?>
                                    <th class="number" style="width:105px">Opening Balance &nbsp;&nbsp;&nbsp;&nbsp;<span>(<img align="absMiddle" style="margin-bottom: 3px;" src="/img/rs.gif">)</span></th>
			            <th class="number" style="width:85px">Transferred Today <span>(<img align="absMiddle" style="margin-bottom: 3px;" src="/img/rs.gif">)</span></th>
			            <th class="number" style="width:100px">Current Balance &nbsp;&nbsp;&nbsp;&nbsp;<span>(<img align="absMiddle" style="margin-bottom: 3px;" src="/img/rs.gif">)</span></th>
			            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
			            <th class="number" style="width:50px">Kits Left</th>
			            <th  style="width:50px">Slab</th>
			           <th style="width:75px">Area</th>
                       <th style="width:75px">Created</th>
			           <th style="width:40px;">&nbsp;</th>
			            <th style="width:40px;">&nbsp;</th>
			            <?php } ?>
			          </tr>
			        </thead>
                    <tbody>
                    <?php $i=0; $totBal = 0; $totTran = 0; $totSale = 0;$totAvg = 0;
                    foreach($records as $rec){ 
                    	if($i%2 == 0)$class = '';
                    	else $class = 'altRow';
                    	                    
			            $type = 'd';
		            	$totBal = $totBal + $rec[$modelName]['balance'];
		            	$totTran = $totTran + $rec[0]['xfer'] + ( isset($datas[$rec[$modelName]['id']])?$datas[$rec[$modelName]['id']]:0 );
		            	$totSale = $totSale + $rec[$modelName]['opening_balance'];
		            	if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {
		            		$refund = $refund + $rec[$modelName]['discounted_money'];
		            	}
			        ?>   			            
                        <tr class="<?php echo $class; ?>"  <?php if ($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR && $rec[$modelName]['active_flag'] == 0 )  echo "style='text-decoration:line-through'"; ?> >
                                    <td><?php echo ($i+1); ?></td>
                                    <td><?php echo $rec[$modelName]['id']; ?></td>
			            <td><a href="<?php echo $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER ? "#" : "/shops/showDetails/".$type."/".$rec[$modelName]['id']; ?>"><?php if($modelName == "Distributor" || $modelName == "SuperDistributor")echo $rec[$modelName]['company']; else echo $rec[$modelName]['shopname']; ?></a></td>
			            <td><?php echo $rec['users']['mobile']; ?></td>
						 <td><?php echo $rec[$modelName]['alternate_number']; ?></td>
                                    <td><?php echo $rec[$modelName]['reference']; ?></td>             
			             <?php //if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) {?>
			            <!--<td class="number"><?php if($rec[$modelName]['level'] == 1) echo "Distributor"; else if($rec[$modelName]['level'] == 2) echo "Star Agent"; else if($rec[$modelName]['level'] == 3) echo "Agent";?></td>-->
			            <?php //}?>
			            <td class="number"><?php echo $rec[$modelName]['margin']; ?></td>
			            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
			            <td class="number"><?php echo empty ($rec['rm']['name']) ?"--" :$rec['rm']['name']; ?></td>
			            <?php }?>
                                    <td class="number"><?php echo $rec[$modelName]['opening_balance']; ?></td>
			            <td class="number"><?php echo sprintf('%.2f', $rec[0]['xfer'] + ( isset( $datas[$rec[$modelName]['id']] ) ? $datas[$rec[$modelName]['id']] : 0 ) ); ?></td>
			            <td class="number"><?php echo $rec[$modelName]['balance']; ?></td>
			            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
			            <td class="number"><?php echo $rec[$modelName]['kits']; ?></td>
			            <td class="number"><?php echo $rec["slabs"]['name']; ?></td>
			            <td><?php echo $rec[$modelName]['area_range'] . " - " . $rec[$modelName]['city']; ?></td>
                        <td class="number"><?php echo $rec[$modelName]['created']; ?></td>
                        <td class="number"><a target="Sale Report" href="/shops/graphRetailer/?type=<?php echo $type; ?>&id=<?php echo $rec[$modelName]['id']; ?>">Analyze</a></td>            
                                    <?php if($_SESSION['Auth']['User']['group_id'] != RELATIONSHIP_MANAGER){?>
                                    <td class="number"><a href="/shops/editRetailer/<?php echo $type; ?>/<?php echo $rec[$modelName]['id']; ?>">edit</a></td>
                                    
    			    	<?php } } ?>
    			    </tr>
    			    <?php $i++; } ?> 					    			      
			         </tbody>
			         <tfoot>
			         	<tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
                                            <td></td>
                                            <?php }?>
                                            <td class="number"><?php echo $totSale; ?></td>
                                            <td class="number"><?php echo sprintf('%.2f',$totTran); ?></td>
                                            <td class="number"><?php echo $totBal; ?></td>
                                            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER) {?>
                                            <td class="number"></td>
                                            <td class="number"><?php echo $refund; ?></td>
                                            
                                            <td class="numberr"></td>
                                            <td class="number"></td>
                                           <td></td>
                                            
                                        <?php  } ?>
                                            
                                            
                                            <!--
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="number"><?php echo $totSale; ?></td>
                                            <td class="number"><?php echo $totTran; ?></td>
                                            <td class="number"><?php echo $totBal; ?></td>
                                            <?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER ) {?>
                                            <td></td><td class="number"><?php echo $refund; ?></td>
                                            <?php } ?>
                                            <td></td>
                                            <td></td>-->
                                            
			         	</tr>
			         </tfoot>         
			   	</table>
			</fieldset>