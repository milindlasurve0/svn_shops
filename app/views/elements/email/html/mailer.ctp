<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mailer</title>
</head>
<body>
<div style="width:620px;">
  <div style="border:1px solid #d9dcd1; background-color:#fff; padding:3px;">
    <table cellpadding="0" cellspacing="0" bgcolor="#6b1a1f" background="http://www.smstadka.com/img/mailer_images/mailer_bg.jpg" width="100%" >
      <tr>
        <td height="523px" style="padding:5px;" valign="top"><div style="border:2px solid #fff;height:509px;">
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td style="padding-left:13px; padding-top:5px;"><a href="http://www.smstadka.com/?utm_source=newsletter&utm_medium=email&utm_campaign=logo"><img src="http://www.smstadka.com/img/mailer_images/logo.png" alt="SMSTadka: Bring mobile closer to your life" title="SMSTadka: Bring mobile closer to your life" border="0" /></a></td>
              </tr>
              <tr>
                <td style="height:13px; font-size:0px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="padding-left:21px;"><img src="http://www.smstadka.com/img/mailer_images/wish.png" alt="We wish you a very Happy & Prosperous New year " title="We wish you a very Happy & Prosperous New year" /></td>
              </tr>
              <tr>
                <td><table cellpadding="0" cellspacing="0" >
                    <tr>
                      <td valign="top" width="422px"><div style="padding-top:30px; padding-bottom:17px;"><img src="http://www.smstadka.com/img/mailer_images/design1.png" /></div>
                        <div style="padding-left:13px;">
                          <div style="font-size:24px; color:#fdd4c8;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;">A Revolutionized SMS Service</div>
                          <div style="font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif; font-size:15px; color:#fdd4c8; padding-top:4px;">Subscribe to SMS packages on your favorite topics from over 100 choices. <a href="http://www.smstadka.com/?utm_source=newsletter&utm_medium=email&utm_campaign=mostpopular" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="Most popular packages" title="Most popular packages">Check out our most popular packages...</a></div>
                          <div style="padding-top:7px;">
                            <table cellpadding="0" cellspacing="0">
                              <tr>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/santa-banta-jokes?utm_source=newsletter&utm_medium=email&utm_campaign=sntbnt"><img src="http://www.smstadka.com/img/mailer_images/santa-banta.gif" width="45px" height="45px" alt="Santa Banta Jokes" title="Santa Banta Jokes" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/cricket-live-score?utm_source=newsletter&utm_medium=email&utm_campaign=scrckt"><img src="http://www.smstadka.com/img/mailer_images/live_score.gif" width="45px" height="45px" alt="Cricket Live Score" title="Cricket Live Score" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/daily-top-stories?utm_source=newsletter&utm_medium=email&utm_campaign=dtsnws"><img src="http://www.smstadka.com/img/mailer_images/news.gif" width="45px" height="45px" alt="Daily Top Stories" title="Daily Top Stories" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/love-shayari?utm_source=newsletter&utm_medium=email&utm_campaign=lovshy"><img src="http://www.smstadka.com/img/mailer_images/shayari.gif" width="45px" height="45px" alt="Love Shayari" title="Love Shayari" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/beauty-tips?utm_source=newsletter&utm_medium=email&utm_campaign=btytps"><img src="http://www.smstadka.com/img/mailer_images/tips.gif" width="45px" height="45px" alt="Beauty Tips" title="Beauty Tips" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/friendship-quotes?utm_source=newsletter&utm_medium=email&utm_campaign=frnqts"><img src="http://www.smstadka.com/img/mailer_images/Untitled-1.gif" width="45px" height="45px" alt="Friendship Quotes" title="Friendship Quotes" border="0" /></a></td>
                                <td style="padding-right:10px"><a href="http://www.smstadka.com/packages/view/katrina-kaif?utm_source=newsletter&utm_medium=email&utm_campaign=katbwd"><img src="http://www.smstadka.com/img/mailer_images/katrina.gif" width="45px" height="45px" alt="Katrina Kaif" title="Katrina Kaif" border="0" /></a></td>
                              </tr>
                            </table>
                          </div>
                          <div style="font-size:15px; color:#fdd4c8; padding-top:21px;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;">Send <a href="http://www.smstadka.com/categories/view/sms-wishes/happy-new-year-wishes?utm_source=newsletter&utm_medium=email&utm_campaign=newyearwish" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="New Year Wishes" title="New Year Wishes">New Year Wishes</a>, <a href="http://www.smstadka.com/categories/view/sms-jokes?utm_source=newsletter&utm_medium=email&utm_campaign=jokes" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="Hilarious Jokes" title="Hilarious Jokes">Hilarious Jokes</a>, <a href="http://www.smstadka.com/categories/view/sms-shayari/love-shayari-sms?utm_source=newsletter&utm_medium=email&utm_campaign=shayaris" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="Heart-touching Shayaris" title="Heart-touching Shayaris">Heart-touching Shayaris</a>, <a href="http://www.smstadka.com/categories/view/sms-quotes/attitude-quotes-sms?utm_source=newsletter&utm_medium=email&utm_campaign=quotes" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="Smart Quotes" title="Smart Quotes">Smart Quotes</a> and much more directly from <a href="http://www.smstadka.com/?utm_source=newsletter&utm_medium=email&utm_campaign=msg" style="color:#fff;font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;" alt="Visit SMSTadka.com" title="Visit SMSTadka.com">SMSTadka</a></div>
                        </div></td>
                      <td><a href="http://www.smstadka.com/?utm_source=newsletter&utm_medium=email&utm_campaign=register"><img src="http://www.smstadka.com/img/mailer_images/20rs.png" alt="Register and Get Rs 20 credits FREE!!! Register Now" title="Register and Get Rs 20 credits FREE!!! Register Now" border="0" /></a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td style="height:20px; font-size:0px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="padding-left:13px;"><table cellpadding="0" cellspacing="0" >
                    <tr>
                      <td style="padding:5px; background-color:#702828;"><a href="http://www.smstadka.com/apps/view/bhulakkad?utm_source=newsletter&utm_medium=email&utm_campaign=apprem" alt="Set Wishes and Forget :)" title="Set Wishes and Forget :)">
                        <table cellpadding="0" cellspacing="0" width="269px" >
                          <tr>
                            <td><img src="http://www.smstadka.com/img/mailer_images/bhullakkad.gif" alt="Set wishes and forget" title="Set wishes and forget" border="0" /></td>
                            <td style="padding-left:8px; font-size:13px; font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;color:#ffab9b;" valign="top">Now, never miss a thing. Wish your dear ones on-time. Set alerts using Bhulakkad.</td>
                          </tr>
                        </table>
                        </a> </td>
                      <td style="width:15px; font-size:0px;"></td>
                      <td style="padding:5px; background-color:#702828;"><a href="http://www.smstadka.com/apps/view/stock-tracker?utm_source=newsletter&utm_medium=email&utm_campaign=appsttr"alt="Track Your Stocks"  title="Track Your Stocks">
                        <table cellpadding="0" cellspacing="0" width="269px" >
                          <tr>
                            <td><img src="http://www.smstadka.com/img/mailer_images/stock.gif" alt="Track Your Stocks"  title="Track Your Stocks" border="0" /></td>
                            <td style="padding-left:8px; font-size:13px; font-family:\'Trebuchet MS\',Arial, Helvetica, sans-serif;color:#ffab9b;" valign="top">Too busy to track your stocks? Track them on SMS using Stock Tracker.</td>
                          </tr>
                        </table>
                        </a> </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
          </div></td>
      </tr>
    </table>
  </div>
  <div style="width:620px; font-family:verdana,Arial, Helvetica, sans-serif; font-weight:bold; color:#9c4d48; padding-top:5px; text-align:right; font-size:11px;">[SERVICES AVAILABLE IN INDIA ONLY]</div>
  <div style="width:620px; font-family:verdana,Arial, Helvetica, sans-serif; font-weight:bold; color:#7d7d7d; padding-top:30px; text-align:left; font-size:10px;">All Rights Reserved © 2012 Active Stores <br/>
    Please do not reply to this mail.This is sent from an unattended mail box. Please mark all your queries/responses to <a href="mailto:support@smstadka.com" style="color:#000;font-family:verdana,Arial, Helvetica, sans-serif;">support@smstadka.com</a></div>
</div>
</body>
</html>