
<!DOCTYPE html>
<html>

  <head>
    <title>Happy Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--<link href="css/bootstrap.css" rel="stylesheet">-->
    <!--<link href="css/my-styles.css" rel="stylesheet" media="screen">-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="padding-top: 10px; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.42857143; margin: 0;color:#333;" bgcolor="transparent !important">
<style type="text/css">
a:active {
outline: 0;
}
a:hover {
outline: 0;
}
.btn-default:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn-primary:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn-success:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn-info:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn-warning:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn-danger:active {
-webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125); box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
}
.btn:active {
background-image: none;
}
.btn-default:hover {
background-color: #e0e0e0; background-position: 0 -15px;
}
.btn-default:focus {
background-color: #e0e0e0; background-position: 0 -15px;
}
.btn-default:active {
background-color: #e0e0e0; border-color: #dbdbdb;
}
.btn-primary:hover {
background-color: #1467D2; background-position: 0 -15px;
}
.btn-primary:focus {
background-color: #1467D2; background-position: 0 -15px;
}
.btn-primary:active {
background-color: #1467D2; border-color: #1467D2;
}
.btn-success:hover {
background-color: #34AD34; background-position: 0 -15px;
}
.btn-success:focus {
background-color: #34AD34; background-position: 0 -15px;
}
.btn-success:active {
background-color: #34AD34; border-color: #34AD34;
}
.btn-info:hover {
background-color: #0a95cc; background-position: 0 -15px;
}
.btn-info:focus {
background-color: #0a95cc; background-position: 0 -15px;
}
.btn-info:active {
background-color: #0a95cc; border-color: #0a95cc;
}
.btn-warning:hover {
background-color: #ea8c09; background-position: 0 -15px;
}
.btn-warning:focus {
background-color: #ea8c09; background-position: 0 -15px;
}
.btn-warning:active {
background-color: #ea8c09; border-color: #ea8c09;
}
.btn-danger:hover {
background-color: #e31510; background-position: 0 -15px;
}
.btn-danger:focus {
background-color: #e31510; background-position: 0 -15px;
}
.btn-danger:active {
background-color: #e31510; border-color: #e31510;
}
.dropdown-menu > li > a:hover {
background-color: #e8e8e8; background-image: linear-gradient(to bottom,#f5f5f5 0%,#e8e8e8 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5',endColorstr='#ffe8e8e8',GradientType=0); background-repeat: repeat-x;
}
.dropdown-menu > li > a:focus {
background-color: #e8e8e8; background-image: linear-gradient(to bottom,#f5f5f5 0%,#e8e8e8 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5',endColorstr='#ffe8e8e8',GradientType=0); background-repeat: repeat-x;
}
.dropdown-menu > .active > a:hover {
background-color: #357ebd; background-image: linear-gradient(to bottom,#4799F8 0%,#357ebd 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4799F8',endColorstr='#ff357ebd',GradientType=0); background-repeat: repeat-x;
}
.dropdown-menu > .active > a:focus {
background-color: #357ebd; background-image: linear-gradient(to bottom,#4799F8 0%,#357ebd 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4799F8',endColorstr='#ff357ebd',GradientType=0); background-repeat: repeat-x;
}
.list-group-item.active:hover {
text-shadow: 0 -1px 0 #1467d2; background-image: linear-gradient(to bottom,#4799F8 0%,#3278b3 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4799F8',endColorstr='#ff3278b3',GradientType=0); background-repeat: repeat-x; border-color: #3278b3;
}
.list-group-item.active:focus {
text-shadow: 0 -1px 0 #1467d2; background-image: linear-gradient(to bottom,#4799F8 0%,#3278b3 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4799F8',endColorstr='#ff3278b3',GradientType=0); background-repeat: repeat-x; border-color: #3278b3;
}
a:hover {
color: #1467d2;
}
a:focus {
color: #1467d2;
}
.pagination > .active > a:hover {
background-color: #1e7ae3; border-color: #1e7ae3;
}
.pagination > .active > span:hover {
background-color: #1e7ae3; border-color: #1e7ae3;
}
.pagination > .active > a:focus {
background-color: #1e7ae3; border-color: #1e7ae3;
}
.pagination > .active > span:focus {
background-color: #1e7ae3; border-color: #1e7ae3;
}
.nav-pills > li.active > a:hover {
color: #fff; background-color: #1e7ae3;
}
.nav-pills > li.active > a:focus {
color: #fff; background-color: #1e7ae3;
}
.text-muted:hover {
color: #555;
}
a.text-primary:hover {
color: #1467d2;
}
a.text-success:hover {
color: #34ad34;
}
a.text-info:hover {
color: #0a95cc;
}
a.text-warning:hover {
color: #ea8c09;
}
a.text-danger:hover {
color: #e31510;
}
a.bg-primary:hover {
background-color: #1467d2; color: #fff;
}
a.bg-success:hover {
background-color: #acdb98; color: #3c763d;
}
a.bg-info:hover {
background-color: #92c5de; color: #1f5570;
}
a.bg-warning:hover {
background-color: #efdf8f; color: #8a6d3b;
}
a.bg-danger:hover {
background-color: #ec9393; color: #a3171c;
}
.btn.action:active {
box-shadow: none;
}
.bootstrap-admin-navbar .navbar-nav > li > a:focus {
background-color: #e7e7e7; color: #222;
}
.bootstrap-admin-navbar .navbar-nav > li > a:hover {
background-color: #e7e7e7; color: #222;
}
.bootstrap-admin-navbar-side .active a:hover {
background-color: #1e7ae3; color: #fff;
}
.bootstrap-admin-navbar-side > li > a:hover {
background-color: #f5f5f5;
}
.bootstrap-admin-navbar-side a:hover .glyphicon-chevron-right {
opacity: .5;
}
.bootstrap-admin-navbar-side .active a:hover .glyphicon-chevron-right {
opacity: 1;
}
.nav-tabs > li > a:hover {
background-image: linear-gradient(to bottom, #ffffff 0%, #f8f8f8 100%); background-repeat: repeat-x; border: 1px solid #e0e0e0; border-bottom-color: rgba(0, 0, 0, 0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.nav-tabs > li > a:focus {
background-image: linear-gradient(to bottom, #ffffff 0%, #f8f8f8 100%); background-repeat: repeat-x; border: 1px solid #e0e0e0; border-bottom-color: rgba(0, 0, 0, 0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.nav-tabs > li.active > a:hover {
background-image: linear-gradient(to bottom, #ffffff 0%, #f8f8f8 100%); background-repeat: repeat-x; border: 1px solid #e0e0e0; border-bottom-color: rgba(0, 0, 0, 0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.nav-tabs > li.active > a:focus {
background-image: linear-gradient(to bottom, #ffffff 0%, #f8f8f8 100%); background-repeat: repeat-x; border: 1px solid #e0e0e0; border-bottom-color: rgba(0, 0, 0, 0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
@media print {
  a:visited {
    text-decoration: underline;
  }
  a[href]:after {
    content: " (" attr(href) ")";
  }
  abbr[title]:after {
    content: " (" attr(title) ")";
  }
  a[href^="javascript:"]:after {
    content: "";
  }
  a[href^="#"]:after {
    content: "";
  }
}
@media (min-width:768px) {
  .lead {
    font-size: 21px;
  }
  .dl-horizontal dt {
    float: left; width: 160px; clear: left; text-align: right; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;
  }
  .dl-horizontal dd {
    margin-left: 180px;
  }
  .container {
    width: 600px;
  }
  .col-sm-1 {
    float: left;
  }
  .col-sm-2 {
    float: left;
  }
  .col-sm-3 {
    float: left;
  }
  .col-sm-4 {
    float: left;
  }
  .col-sm-5 {
    float: left;
  }
  .col-sm-6 {
    float: left;
  }
  .col-sm-7 {
    float: left;
  }
  .col-sm-8 {
    float: left;
  }
  .col-sm-9 {
    float: left;
  }
  .col-sm-10 {
    float: left;
  }
  .col-sm-11 {
    float: left;
  }
  .col-sm-12 {
    float: left;
  }
  .col-sm-12 {
    width: 100%;
  }
  .col-sm-11 {
    width: 91.66666667%;
  }
  .col-sm-10 {
    width: 83.33333333%;
  }
  .col-sm-9 {
    width: 75%;
  }
  .col-sm-8 {
    width: 66.66666667%;
  }
  .col-sm-7 {
    width: 58.33333333%;
  }
  .col-sm-6 {
    width: 50%;
  }
  .col-sm-5 {
    width: 41.66666667%;
  }
  .col-sm-4 {
    width: 33.33333333%;
  }
  .col-sm-3 {
    width: 25%;
  }
  .col-sm-2 {
    width: 16.66666667%;
  }
  .col-sm-1 {
    width: 8.33333333%;
  }
  .col-sm-pull-12 {
    right: 100%;
  }
  .col-sm-pull-11 {
    right: 91.66666667%;
  }
  .col-sm-pull-10 {
    right: 83.33333333%;
  }
  .col-sm-pull-9 {
    right: 75%;
  }
  .col-sm-pull-8 {
    right: 66.66666667%;
  }
  .col-sm-pull-7 {
    right: 58.33333333%;
  }
  .col-sm-pull-6 {
    right: 50%;
  }
  .col-sm-pull-5 {
    right: 41.66666667%;
  }
  .col-sm-pull-4 {
    right: 33.33333333%;
  }
  .col-sm-pull-3 {
    right: 25%;
  }
  .col-sm-pull-2 {
    right: 16.66666667%;
  }
  .col-sm-pull-1 {
    right: 8.33333333%;
  }
  .col-sm-pull-0 {
    right: 0;
  }
  .col-sm-push-12 {
    left: 100%;
  }
  .col-sm-push-11 {
    left: 91.66666667%;
  }
  .col-sm-push-10 {
    left: 83.33333333%;
  }
  .col-sm-push-9 {
    left: 75%;
  }
  .col-sm-push-8 {
    left: 66.66666667%;
  }
  .col-sm-push-7 {
    left: 58.33333333%;
  }
  .col-sm-push-6 {
    left: 50%;
  }
  .col-sm-push-5 {
    left: 41.66666667%;
  }
  .col-sm-push-4 {
    left: 33.33333333%;
  }
  .col-sm-push-3 {
    left: 25%;
  }
  .col-sm-push-2 {
    left: 16.66666667%;
  }
  .col-sm-push-1 {
    left: 8.33333333%;
  }
  .col-sm-push-0 {
    left: 0;
  }
  .col-sm-offset-12 {
    margin-left: 100%;
  }
  .col-sm-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-sm-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-sm-offset-9 {
    margin-left: 75%;
  }
  .col-sm-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-sm-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-sm-offset-6 {
    margin-left: 50%;
  }
  .col-sm-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-sm-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-sm-offset-3 {
    margin-left: 25%;
  }
  .col-sm-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-sm-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-sm-offset-0 {
    margin-left: 0;
  }
  .form-inline .form-group {
    display: inline-block; margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .form-control {
    display: inline-block; width: auto; vertical-align: middle;
  }
  .form-inline .input-group>.form-control {
    width: 100%;
  }
  .form-inline .control-label {
    margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .radio {
    display: inline-block; margin-top: 0; margin-bottom: 0; padding-left: 0; vertical-align: middle;
  }
  .form-inline .checkbox {
    display: inline-block; margin-top: 0; margin-bottom: 0; padding-left: 0; vertical-align: middle;
  }
  .form-inline .radio input[type=radio] {
    float: none; margin-left: 0;
  }
  .form-inline .checkbox input[type=checkbox] {
    float: none; margin-left: 0;
  }
  .form-inline .has-feedback .form-control-feedback {
    top: 0;
  }
  .form-horizontal .control-label {
    text-align: right;
  }
  .navbar-right .dropdown-menu {
    left: auto; right: 0;
  }
  .navbar-right .dropdown-menu-left {
    left: 0; right: auto;
  }
  .nav-tabs.nav-justified>li {
    display: table-cell; width: 1%;
  }
  .nav-tabs.nav-justified>li>a {
    margin-bottom: 0;
  }
  .nav-tabs.nav-justified>li>a {
    border-bottom: 1px solid #ddd; border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified>.active>a {
    border-bottom-color: #fff;
  }
  .nav-tabs.nav-justified>.active>a:hover {
    border-bottom-color: #fff;
  }
  .nav-tabs.nav-justified>.active>a:focus {
    border-bottom-color: #fff;
  }
  .nav-justified>li {
    display: table-cell; width: 1%;
  }
  .nav-justified>li>a {
    margin-bottom: 0;
  }
  .nav-tabs-justified>li>a {
    border-bottom: 1px solid #ddd; border-radius: 4px 4px 0 0;
  }
  .nav-tabs-justified>.active>a {
    border-bottom-color: #fff;
  }
  .nav-tabs-justified>.active>a:hover {
    border-bottom-color: #fff;
  }
  .nav-tabs-justified>.active>a:focus {
    border-bottom-color: #fff;
  }
  .navbar {
    border-radius: 4px;
  }
  .navbar-header {
    float: left;
  }
  .navbar-collapse {
    width: auto; border-top: 0; box-shadow: none;
  }
  .navbar-collapse.collapse {
    display: block !important; height: auto !important; padding-bottom: 0; overflow: visible !important;
  }
  .navbar-collapse.in {
    overflow-y: visible;
  }
  .navbar-fixed-top .navbar-collapse {
    padding-left: 0; padding-right: 0;
  }
  .navbar-static-top .navbar-collapse {
    padding-left: 0; padding-right: 0;
  }
  .navbar-fixed-bottom .navbar-collapse {
    padding-left: 0; padding-right: 0;
  }
  .container>.navbar-header {
    margin-right: 0; margin-left: 0;
  }
  .container-fluid>.navbar-header {
    margin-right: 0; margin-left: 0;
  }
  .container>.navbar-collapse {
    margin-right: 0; margin-left: 0;
  }
  .container-fluid>.navbar-collapse {
    margin-right: 0; margin-left: 0;
  }
  .navbar-static-top {
    border-radius: 0;
  }
  .navbar-fixed-top {
    border-radius: 0;
  }
  .navbar-fixed-bottom {
    border-radius: 0;
  }
  .navbar>.container .navbar-brand {
    margin-left: -15px;
  }
  .navbar>.container-fluid .navbar-brand {
    margin-left: -15px;
  }
  .navbar-toggle {
    display: none;
  }
  .navbar-nav {
    float: left; margin: 0;
  }
  .navbar-nav>li {
    float: left;
  }
  .navbar-nav>li>a {
    padding-top: 15px; padding-bottom: 15px;
  }
  .navbar-nav.navbar-right:last-child {
    margin-right: -15px;
  }
  .navbar-left {
    float: left !important;
  }
  .navbar-right {
    float: right !important;
  }
  .navbar-form .form-group {
    display: inline-block; margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .form-control {
    display: inline-block; width: auto; vertical-align: middle;
  }
  .navbar-form .input-group>.form-control {
    width: 100%;
  }
  .navbar-form .control-label {
    margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .radio {
    display: inline-block; margin-top: 0; margin-bottom: 0; padding-left: 0; vertical-align: middle;
  }
  .navbar-form .checkbox {
    display: inline-block; margin-top: 0; margin-bottom: 0; padding-left: 0; vertical-align: middle;
  }
  .navbar-form .radio input[type=radio] {
    float: none; margin-left: 0;
  }
  .navbar-form .checkbox input[type=checkbox] {
    float: none; margin-left: 0;
  }
  .navbar-form .has-feedback .form-control-feedback {
    top: 0;
  }
  .navbar-form {
    width: auto; border: 0; margin-left: 0; margin-right: 0; padding-top: 0; padding-bottom: 0; -webkit-box-shadow: none; box-shadow: none;
  }
  .navbar-form.navbar-right:last-child {
    margin-right: -15px;
  }
  .navbar-text {
    float: left; margin-left: 15px; margin-right: 15px;
  }
  .navbar-text.navbar-right:last-child {
    margin-right: 0;
  }
  .modal-dialog {
    width: 600px; margin: 30px auto;
  }
  .modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5); box-shadow: 0 5px 15px rgba(0,0,0,.5);
  }
  .modal-sm {
    width: 300px;
  }
}
@media (min-width:992px) {
  .container {
    width: 970px;
  }
  .col-md-1 {
    float: left;
  }
  .col-md-2 {
    float: left;
  }
  .col-md-3 {
    float: left;
  }
  .col-md-4 {
    float: left;
  }
  .col-md-5 {
    float: left;
  }
  .col-md-6 {
    float: left;
  }
  .col-md-7 {
    float: left;
  }
  .col-md-8 {
    float: left;
  }
  .col-md-9 {
    float: left;
  }
  .col-md-10 {
    float: left;
  }
  .col-md-11 {
    float: left;
  }
  .col-md-12 {
    float: left;
  }
  .col-md-12 {
    width: 100%;
  }
  .col-md-11 {
    width: 91.66666667%;
  }
  .col-md-10 {
    width: 83.33333333%;
  }
  .col-md-9 {
    width: 75%;
  }
  .col-md-8 {
    width: 66.66666667%;
  }
  .col-md-7 {
    width: 58.33333333%;
  }
  .col-md-6 {
    width: 50%;
  }
  .col-md-5 {
    width: 41.66666667%;
  }
  .col-md-4 {
    width: 33.33333333%;
  }
  .col-md-3 {
    width: 25%;
  }
  .col-md-2 {
    width: 16.66666667%;
  }
  .col-md-1 {
    width: 8.33333333%;
  }
  .col-md-pull-12 {
    right: 100%;
  }
  .col-md-pull-11 {
    right: 91.66666667%;
  }
  .col-md-pull-10 {
    right: 83.33333333%;
  }
  .col-md-pull-9 {
    right: 75%;
  }
  .col-md-pull-8 {
    right: 66.66666667%;
  }
  .col-md-pull-7 {
    right: 58.33333333%;
  }
  .col-md-pull-6 {
    right: 50%;
  }
  .col-md-pull-5 {
    right: 41.66666667%;
  }
  .col-md-pull-4 {
    right: 33.33333333%;
  }
  .col-md-pull-3 {
    right: 25%;
  }
  .col-md-pull-2 {
    right: 16.66666667%;
  }
  .col-md-pull-1 {
    right: 8.33333333%;
  }
  .col-md-pull-0 {
    right: 0;
  }
  .col-md-push-12 {
    left: 100%;
  }
  .col-md-push-11 {
    left: 91.66666667%;
  }
  .col-md-push-10 {
    left: 83.33333333%;
  }
  .col-md-push-9 {
    left: 75%;
  }
  .col-md-push-8 {
    left: 66.66666667%;
  }
  .col-md-push-7 {
    left: 58.33333333%;
  }
  .col-md-push-6 {
    left: 50%;
  }
  .col-md-push-5 {
    left: 41.66666667%;
  }
  .col-md-push-4 {
    left: 33.33333333%;
  }
  .col-md-push-3 {
    left: 25%;
  }
  .col-md-push-2 {
    left: 16.66666667%;
  }
  .col-md-push-1 {
    left: 8.33333333%;
  }
  .col-md-push-0 {
    left: 0;
  }
  .col-md-offset-12 {
    margin-left: 100%;
  }
  .col-md-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-md-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-md-offset-9 {
    margin-left: 75%;
  }
  .col-md-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-md-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-md-offset-6 {
    margin-left: 50%;
  }
  .col-md-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-md-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-md-offset-3 {
    margin-left: 25%;
  }
  .col-md-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-md-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-md-offset-0 {
    margin-left: 0;
  }
  .modal-lg {
    width: 900px;
  }
}
@media (min-width:1200px) {
  .container {
    width: 1170px;
  }
  .col-lg-1 {
    float: left;
  }
  .col-lg-2 {
    float: left;
  }
  .col-lg-3 {
    float: left;
  }
  .col-lg-4 {
    float: left;
  }
  .col-lg-5 {
    float: left;
  }
  .col-lg-6 {
    float: left;
  }
  .col-lg-7 {
    float: left;
  }
  .col-lg-8 {
    float: left;
  }
  .col-lg-9 {
    float: left;
  }
  .col-lg-10 {
    float: left;
  }
  .col-lg-11 {
    float: left;
  }
  .col-lg-12 {
    float: left;
  }
  .col-lg-12 {
    width: 100%;
  }
  .col-lg-11 {
    width: 91.66666667%;
  }
  .col-lg-10 {
    width: 83.33333333%;
  }
  .col-lg-9 {
    width: 75%;
  }
  .col-lg-8 {
    width: 66.66666667%;
  }
  .col-lg-7 {
    width: 58.33333333%;
  }
  .col-lg-6 {
    width: 50%;
  }
  .col-lg-5 {
    width: 41.66666667%;
  }
  .col-lg-4 {
    width: 33.33333333%;
  }
  .col-lg-3 {
    width: 25%;
  }
  .col-lg-2 {
    width: 16.66666667%;
  }
  .col-lg-1 {
    width: 8.33333333%;
  }
  .col-lg-pull-12 {
    right: 100%;
  }
  .col-lg-pull-11 {
    right: 91.66666667%;
  }
  .col-lg-pull-10 {
    right: 83.33333333%;
  }
  .col-lg-pull-9 {
    right: 75%;
  }
  .col-lg-pull-8 {
    right: 66.66666667%;
  }
  .col-lg-pull-7 {
    right: 58.33333333%;
  }
  .col-lg-pull-6 {
    right: 50%;
  }
  .col-lg-pull-5 {
    right: 41.66666667%;
  }
  .col-lg-pull-4 {
    right: 33.33333333%;
  }
  .col-lg-pull-3 {
    right: 25%;

  }
  .col-lg-pull-2 {
    right: 16.66666667%;
  }
  .col-lg-pull-1 {
    right: 8.33333333%;
  }
  .col-lg-pull-0 {
    right: 0;
  }
  .col-lg-push-12 {
    left: 100%;
  }
  .col-lg-push-11 {
    left: 91.66666667%;
  }
  .col-lg-push-10 {
    left: 83.33333333%;
  }
  .col-lg-push-9 {
    left: 75%;
  }
  .col-lg-push-8 {
    left: 66.66666667%;
  }
  .col-lg-push-7 {
    left: 58.33333333%;
  }
  .col-lg-push-6 {
    left: 50%;
  }
  .col-lg-push-5 {
    left: 41.66666667%;
  }
  .col-lg-push-4 {
    left: 33.33333333%;
  }
  .col-lg-push-3 {
    left: 25%;
  }
  .col-lg-push-2 {
    left: 16.66666667%;
  }
  .col-lg-push-1 {
    left: 8.33333333%;
  }
  .col-lg-push-0 {
    left: 0;
  }
  .col-lg-offset-12 {
    margin-left: 100%;
  }
  .col-lg-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-lg-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-lg-offset-9 {
    margin-left: 75%;
  }
  .col-lg-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-lg-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-lg-offset-6 {
    margin-left: 50%;
  }
  .col-lg-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-lg-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-lg-offset-3 {
    margin-left: 25%;
  }
  .col-lg-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-lg-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-lg-offset-0 {
    margin-left: 0;
  }
  .visible-lg {
    display: block !important;
  }
  table.visible-lg {
    display: table;
  }
  tr.visible-lg {
    display: table-row !important;
  }
  th.visible-lg {
    display: table-cell !important;
  }
  td.visible-lg {
    display: table-cell !important;
  }
  .hidden-lg {
    display: none !important;
  }
}
@media (max-width:767px) {
  .table-responsive {
    width: 100%; margin-bottom: 15px; overflow-y: hidden; overflow-x: scroll; -ms-overflow-style: -ms-autohiding-scrollbar; border: 1px solid #ddd; -webkit-overflow-scrolling: touch;
  }
  .table-responsive>.table {
    margin-bottom: 0;
  }
  .table-responsive>.table>thead>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table>tbody>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table>tfoot>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table>thead>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table>tbody>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table>tfoot>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table-bordered {
    border: 0;
  }
  .table-responsive>.table-bordered>thead>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>thead>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>thead>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>thead>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tbody>tr:last-child>th {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr:last-child>th {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tbody>tr:last-child>td {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr:last-child>td {
    border-bottom: 0;
  }
  .navbar-nav .open .dropdown-menu {
    position: static; float: none; width: auto; margin-top: 0; background-color: transparent; border: 0; box-shadow: none;
  }
  .navbar-nav .open .dropdown-menu>li>a {
    padding: 5px 15px 5px 25px;
  }
  .navbar-nav .open .dropdown-menu .dropdown-header {
    padding: 5px 15px 5px 25px;
  }
  .navbar-nav .open .dropdown-menu>li>a {
    line-height: 20px;
  }
  .navbar-nav .open .dropdown-menu>li>a:hover {
    background-image: none;
  }
  .navbar-nav .open .dropdown-menu>li>a:focus {
    background-image: none;
  }
  .navbar-form .form-group {
    margin-bottom: 5px;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a {
    color: #777;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a:hover {
    color: #333; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #333; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a:hover {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a:focus {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a {
    color: #ccc; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:hover {
    color: #ccc; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:focus {
    color: #ccc; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.dropdown-header {
    border-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu .divider {
    background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
    color: #999;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a:hover {
    color: #fff; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #fff; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:hover {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:focus {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a {
    color: #444; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:hover {
    color: #444; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:focus {
    color: #444; background-color: transparent;
  }
  .visible-xs {
    display: block !important;
  }
  table.visible-xs {
    display: table;
  }
  tr.visible-xs {
    display: table-row !important;
  }
  th.visible-xs {
    display: table-cell !important;
  }
  td.visible-xs {
    display: table-cell !important;
  }
  .hidden-xs {
    display: none !important;
  }
}
@media screen and (min-width:768px) {
  .jumbotron {
    padding-top: 48px; padding-bottom: 48px;
  }
  .container .jumbotron {
    padding-left: 60px; padding-right: 60px;
  }
  .jumbotron h1 {
    font-size: 63px;
  }
  .jumbotron .h1 {
    font-size: 63px;
  }
  .carousel-control .glyphicon-chevron-left {
    width: 30px; height: 30px; margin-top: -15px; margin-left: -15px; font-size: 30px;
  }
  .carousel-control .glyphicon-chevron-right {
    width: 30px; height: 30px; margin-top: -15px; margin-left: -15px; font-size: 30px;
  }
  .carousel-control .icon-prev {
    width: 30px; height: 30px; margin-top: -15px; margin-left: -15px; font-size: 30px;
  }
  .carousel-control .icon-next {
    width: 30px; height: 30px; margin-top: -15px; margin-left: -15px; font-size: 30px;
  }
  .carousel-caption {
    left: 20%; right: 20%; padding-bottom: 30px;
  }
  .carousel-indicators {
    bottom: 20px;
  }
}
@media (min-width:768px) and (max-width:991px) {
  .visible-sm {
    display: block !important;
  }
  table.visible-sm {
    display: table;
  }
  tr.visible-sm {
    display: table-row !important;
  }
  th.visible-sm {
    display: table-cell !important;
  }
  td.visible-sm {
    display: table-cell !important;
  }
  .hidden-sm {
    display: none !important;
  }
}
@media (min-width:992px) and (max-width:1199px) {
  .visible-md {
    display: block !important;
  }
  table.visible-md {
    display: table;
  }
  tr.visible-md {
    display: table-row !important;
  }
  th.visible-md {
    display: table-cell !important;
  }
  td.visible-md {
    display: table-cell !important;
  }
  .hidden-md {
    display: none !important;
  }
}
/* For width 768px and smaller: */
@media only screen and (min-device-width: 500px) and (max-device-width: 1024px)  {
		span[id=switcher] {display:block;
		background-image: url(imgs/birthday-feature.png) !important;
		background-repeat: no-repeat !important;
		background-position: center !important;
		width: 600px !important;
		height: 400px !important; }
		img[id=feature] {display: none !important; }
	}
	@media only screen and (max-device-width: 489px) {
		span[id=switcher] {display:block;
		background-image: url(imgs/birthday-feature-responsive.png) !important;
		background-repeat: no-repeat !important;
		background-position: center !important;
		width: 320px !important;
		height: 708px !important; }
		img[id=feature] {display: none !important; }
	}
</style>
  
  <body>
    <div class="container" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 15px; padding-right: 15px;">
    
     <div class="row" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
        <div class="col-sm-12" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding: 20px 20px 5px; margin-bottom:30px;" align="center"><img src="imgs/birthday-wishes.png" alt="Happy Birthday from Pay1" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; page-break-inside: avoid; max-width: 100% !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; border: 0;"></div>
        </div><!--end row-->
        
        <!-- birthday feature -->        
    	<div class="row" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
      	<!-- distributor name -->
    		<h2 class="col-sm-12" style="color:#231f20;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:16px; text-align:center;">Dear Mr. <?php echo $user['distributors']['name']; ?></h2>
		<!-- end distributor name -->
        	<!-- spacer styles -->
            <div class="col-sm-12" style="display:block; margin-bottom:30px;">
            <!-- spacer styles -->
    	</div><!--end row-->
        
        <div class="row" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
        <div class="col-sm-12 feature" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding: 20px 20px 5px;" align="center">
        <span id="switcher">
        <img id="feature" src="imgs/birthday-feature.png" alt="Happy Birthday from Pay1" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; page-break-inside: avoid; max-width: 100% !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; border: 0;">
        </span>
        </div>
        </div><!--end row-->
        <!-- end birthday feature -->
        
        <!-- social media links -->
        	 <div class="row" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
        	<div class="col-sm-6 col-sm-offset-3" style="text-shadow: none !important;  background-color: #eaeeef !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;">
        		<div class="col-sm-4 text-center" style="padding-top: 15px; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; padding-bottom:10px;" align="center">
            <a href="https://www.facebook.com/pay1store" target="blank">  
              <img src="imgs/facebook.png" alt="Like us our Facebook page" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; page-break-inside: avoid; max-width: 100% !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; border: 0; margin: 0 0 10px;">
            </a>
              <p style="font-size: 15px;  color:#231f20; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; orphans: 3; widows: 3; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px;">Like us </p>
</div>
        		<div class="col-sm-4 text-center" style="padding-top: 15px; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;" align="center">
              <a href="https://www.youtube.com/c/Pay1Inapp" target="blank">
                <img src="imgs/youtube.png" alt="Subscribe to our YouTube channel" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; page-break-inside: avoid; max-width: 100% !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; border: 0; margin: 0 0 10px;">
              </a>
                <p style="font-size: 15px; color:#231f20; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; orphans: 3; widows: 3; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px;">Subscribe us </p>
</div>
        		<div class="col-sm-4 text-center" style="padding-top: 15px; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;" align="center">
             <a href="http://pay1.in/partners-blog/" target="blank"> 
              <img src="imgs/wordpress.png" alt="Read our Blog" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; page-break-inside: avoid; max-width: 100% !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; border: 0; margin: 0 0 10px;">
             </a> 
              <p style="font-size: 15px;  color:#231f20; text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; orphans: 3; widows: 3; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px;">Read our Blog </p>
</div>
        	</div>
        </div>
        <!-- end social media links -->
        
        <!-- pay1 logo -->
        <div class="row" style="text-shadow: none !important;  background-color: transparent !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
        <div class="col-sm-12 text-center" style="text-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center; position: relative; min-height: 1px; padding: 9px;color:white;" align="center"><img src="imgs/pay1_logo.png" alt="Pay1 Logo"></div></div>
         </div>
         <!-- end pay1 logo -->    
           
    </div><!--end container-->