<div id="operators_view" class="tab-pane fade in">

    <div class="row">
        <div class="col-lg-12" id="successfailurereportsdiv">
           
        </div>
    </div>

    <div class="row">
          
          <p class="text-left"><button class="btn btn-default disabled" id="btn-operator-refresh"><i  class="glyphicon glyphicon-refresh"></i></button></p>
 
            <table class="table table-condensed table-hover" id="operator_view_table">
             <thead>
                 <tr>
                     <th>&nbsp;</th>
                     <th>Sims</th>
                     <th>Requests/min</th>
                     <th>Block/Stop</th>
                     <th>Cur Balance</th>
                     <th>Opening</th>
                     <th>Closing</th>
                     <th>Incoming</th>
                     <th>Sale</th>
                     <th>Diff</th>
                     <th>Blocked Balance</th>
                  </tr>
             </thead>

             <tbody>

             </tbody>

        </table>
          
      </div>
    

</div>