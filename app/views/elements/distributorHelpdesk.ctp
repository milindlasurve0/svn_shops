<fieldset style="border:0px;margin:0px;width:1070px">
        <div class="appTitle">Distributor Help Desk</div>

                <div class="field" style="padding-top:10px;">
                    <div class="fieldDetail leftFloat" >
                        <div class="fieldLabel1 leftFloat"><label for="contactno">Contact number : </label></div>
                        <div class="fieldLabelSpace1">
                            <p>+91 887 964 7664 / +91 887 920 2843</p>
                        </div>                     
                    </div>
                </div>
        
                <div class="field" style="padding-top:25px;">
                    <div class="fieldDetail leftFloat">
                         <div class="fieldLabel1 leftFloat"><label for="email">Email ID : </label></div>
                         <div class="fieldLabelSpace1">
                            <p>channel@pay1.in / sales@pay1.in (For Feedback)</p>
                         </div> 
                    </div>
                </div>
        
                <div class="field"  style="padding-left: 5px;padding-top: 25px">
                    <div class="fieldDetail leftFloat" >
                        <div>
                            <small class="text-muted">(<b>Note :</b> Please do not share this information with your retail partner.)</small>
                        </div>
                    </div>                     
                </div>
</fieldset>