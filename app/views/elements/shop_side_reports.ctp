
    <div class="leftFloat dashboardPack">
      <div class="catList">
        <ul id='innerul'>
        	<?php if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER || $_SESSION['Auth']['User']['group_id'] == ACCOUNTS) { 
        	
        	 if($_SESSION['Auth']['User']['group_id'] == ADMIN){
        	 	$report = "AD";
        	 	$report_child = "SuperDistributor";
        	 }
        	 else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
        	 	$report = "SD";
        	 	$report_child = "Distributor";
        	 }
        	 else if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
        	 	$report = "Distributor";
        	 	$report_child = "Retailer";
        	 }else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
        	 	$report = "RM";
        	 	$report_child = "Distributor";
        	 }
       
        	 ?>
        	 <li>
        	 <a class="hList" href="javascript:void(0);"><p>
        	 	<?php echo 'Accounts';?>
        	 	</p>
        	 </a>
          	
        	<div class="sublist">
        		<ul>
					<?php if($_SESSION['Auth']['User']['group_id']!=ACCOUNTS){ ?>
        			<li name='innerli'>
        				<a href="/shops/mainReport" class="<?php if($side_tab == 'main') echo 'sel';?>">Main Report</a>
        			</li>
					<?php } ?>
        			<?php if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){ ?>
        			<li name='innerli'>
        				<a href="/shops/accountHistory" class="<?php if($side_tab == 'history') echo 'sel';?>">Account History</a>
        			</li>
        			<?php } ?>
        			<?php if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){ ?>
        			<li name='innerli'>
        				<a href="/shops/sReport" class="<?php if($side_tab == 'sale') echo 'sel';?>">Sale Report</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/overallReport" class="<?php if($side_tab == 'overall') echo 'sel';?>">Overall Report</a>
        			</li>
        			<?php } ?>
        			<?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) { ?>
        			<li name='innerli'>
        				<a href="/shops/topup" class="<?php if($side_tab == 'topup') echo 'sel';?>"><?php echo "Buy Report"; ?></a>
        			</li>
					<li name='innerli'>
								<a href="/shops/distributorsMonthReport" class="<?php if ($side_tab == 'distributors_month_report') echo 'sel'; ?>"><?php echo "Distributor Sale"; ?></a>
					</li>
        			<?php } ?>
        			<?php if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) { ?>
        			<li name='innerli'>
        				<a href="/shops/topup" class="<?php if($side_tab == 'topup') echo 'sel';?>"><?php echo "Buy Report"; ?></a>
        			</li>
                                <li name='innerli'>
        				<a href="/shops/overallReport" class="<?php if($side_tab == 'overall') echo 'sel';?>">Retailers Sale Report</a>
        			</li>
        			<?php } else {
						 if($_SESSION['Auth']['User']['group_id']!=ACCOUNTS){?>
        			<li name='innerli'>
        				<a href="/shops/topupDist" class="<?php if($side_tab == 'topup') echo 'sel';?>"><?php echo "Balance Transfer Report"; ?></a>
        			</li>
						 <?php } } ?>
        			<!--<li name='innerli'>
        				<a href="/shops/saleReport" class="<?php //if($side_tab == 'sale') echo 'sel';?>">Sale Report</a>
        			</li>-->
        			<?php if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) { ?>
        			<li name='innerli'>
        				<a href="/shops/salesmanReport" class="<?php if($side_tab == 'salesman') echo 'sel';?>">Salesman Report</a>
        			</li>
                                <li name='innerli'>
        				<a href="/shops/allRetailerTrans" class="<?php if($side_tab == 'allRetailerTrans') echo 'sel';?>">Retailer Transactions</a>
        			</li>
                    </li>
                      
        			<?php } ?>
        			<?php if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == ACCOUNTS ){?>
<!--        			<li name='innerli'>
        				<a href="/shops/earningReport" class="<?php if($side_tab == 'earning') echo 'sel';?>">Earning Report</a>
        			</li>-->
        			<li name='innerli'>
        				<a href="/shops/floatReport" class="<?php if($side_tab == 'float') echo 'sel';?>">Float Report</a>
        			</li>
                                <li name='innerli'>
        				<a href="/shops/floatGraph" class="<?php if($side_tab == 'float_graph') echo 'sel';?>">Float Graph</a>
        			</li>
					<?php } if($_SESSION['Auth']['User']['group_id'] == ADMIN){ ?>
					<li name='innerli'>
        				<a href="/shops/incentivePullback" class="<?php if($side_tab == 'incentive_pullback') echo 'sel';?>">Incentive Pullback</a>
        			</li>
					<li name='innerli'>
        				<a href="/shops/recheckTrans" class="<?php if($side_tab == 'recheckTrans') echo 'sel';?>">Recheck Transactions</a>
        			</li>
					<?php } ?>
        			
        		</ul>
        	</div>
        	</li>
        	<?php } else if($_SESSION['Auth']['User']['group_id'] == RETAILER) { ?>
        		<li>
        	 <a class="hList" href="javascript:void(0);"><p>
        	 	<?php echo 'Retailer';?>
        	 	</p>
        	 </a>
          	
        	<div class="sublist">
        		<ul>
        			<li name='innerli'>
        				<a href="/shops/accountHistory" class="<?php if($side_tab == 'transfer') echo 'sel';?>">Ledger Balance</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/saleReport" class="<?php if($side_tab == 'sale') echo 'sel';?>">Sale Report</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/lastTransactions" class="<?php if($side_tab == 'trans') echo 'sel';?>">Last Transactions</a>
        			</li>
        			<!--<li name='innerli'>
        				<a href="/shops/invoices" class="<?php if($side_tab == 'invoice') echo 'sel';?>">Retailer Reports</a>
        			</li>-->
        			<li name='innerli'>
        				<a href="/shops/getCreditDebitNotes" class="<?php if($side_tab == 'credit') echo 'sel';?>">Credit / Debit Note</a>
        			</li>
        		</ul>
        	</div>
        	</li>
        	<?php } ?>
        	
		</ul>
    </div>
  </div>  
  