<?php echo $form->create('shop'); ?>
     	<fieldset class="fields1" style="border:0px;margin:0px;">
			<div class="appTitle">New Retailer</div>
				<div>
				<div class="field" style="padding-top:5px;">
					<div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="mobile" class="compulsory">Mobile</label></div>
                         <div class="fieldLabelSpace1">
                             <input tabindex="1" type="text" id="mobile" maxlength="10" name="data[Retailer][mobile]" value ="<?php if(isset($data))echo $data['Retailer']['mobile']; ?>"/>
                         </div>                     
                 	</div>  
                 	<div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="shopname" class="compulsory"> Shop Name </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="2" type="text" id="shopname" name="data[Retailer][shopname]" value="<?php if(isset($data))echo $data['Retailer']['shopname']; ?>"/>
                         </div>                    
                 	</div><div class="clearLeft">&nbsp;</div>
            	 </div>
            	 </div>
            	 <input type="hidden" name="data[Retailer][rental_flag]" value="1" />
            	 <div class="field">               		
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat">&nbsp;</div>
                         <div class="fieldLabelSpace1" id="sub_butt">
                         	<?php echo $ajax->submit('Create Retailer', array('id' => 'sub', 'tabindex'=>'3','url'=> array('controller'=>'shops', 'action'=>'createRetailer'), 'class' => 'retailBut enabledBut', 'after' => 'showLoader2("sub_butt");', 'update' => 'innerDiv')); ?>
                         </div>                         
                    </div>
                    <div class="clearLeft">&nbsp;</div>
                </div>
                <div class="field">    
                    <div class="fieldDetail">                         
                         <div>
                            <?php echo $this->Session->flash();?>
                         </div>   
                    </div>
            	 </div>	
		</fieldset>
<?php echo $form->end(); ?>
