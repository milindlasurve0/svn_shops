<div class="field">

<fieldset>
<div class="box2" style="margin-top:0px;">
	<div class="header">Cheque/DD</div>
    <div class="Blanstate">&nbsp;</div>
    <div style="padding:10px;">
    	<p class="field">All cheques/demand drafts should be drawn in favour of  <span class="highlight">"Mindsarray Technologies Pvt. Ltd."</span>.</p>
		<p class="field">Deposit your cheques at your nearest ICICI BANK.<br> <b>Our account number : 019805004392</b></p>
		<p class="field">Email us your cheque details at <a href="mailto:billing@smstadka.com">billing@smstadka.com</a>. Mention your name(Account holder's name), registered mobile number, cheque amount, cheque date, bank details, cheque number in your email. We will recharge your account on successful clearance of your cheque.</p>
		<p>You can courier the cheque/DD at the below mentioned address.<br><br></p>
		<div class="title5 strng">Address:</div>
		<p><span class="strng">Mindsarray Technologies Pvt. Ltd.</span><br>
				 528, Raheja's Metroplex (IJMIMA),<br/>
				 Link Road, Malad (W),<br/> 
				Mumbai - 400064, Maharashtra.</p><br>
		<p class="field">Thank You.</p>
		
</div>
</div>
</fieldset>

</div>