<script>

function saveMaintenanceSm(rid,obj)
{
	var salesManId=obj.options[obj.selectedIndex].value;
		
	var r=confirm("You sure?");
	if(r==true){
		var url = '/salesmen/mapSalesman';
		var params = {'rid' : rid,'sid':salesManId};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{			
					alert('done');
				}
		});
	}
}

function changeDistributor(rid,obj)
{
	var distId=obj.options[obj.selectedIndex].value;
		
	var r=confirm("Please check if retailer pending is cleared from last distributor. If everything is ok, go ahead");
	if(r==true){
		var url = '/panels/changeDistributor';
		var params = {'rid' : rid,'sid':distId};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{			
					alert('done');
				}
		});
	}
}

function shiftRental(rid,obj,mob)
{       
	var flag=obj.options[obj.selectedIndex].value;
	var r=confirm("You sure?");
	if(r==true){
		var url = '/salesmen/rentalRetailer';
		var params = {'rid' : rid,'flag':flag , 'mobile' : mob};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{			
					if(transport.responseText == 'success'){
						alert('done');
					}else{
						alert(transport.responseText);
					}
				}
		});
		
	}
}

function retEnable(rid,obj)
{
	var flag=obj.options[obj.selectedIndex].value;
	var r=confirm("You sure?");
	if(r==true){
		var url = '/salesmen/blockRetailer';
		var params = {'rid' : rid,'flag':flag};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{			
					if(transport.responseText == 'success'){
						alert('done');
					}else{
						alert('Try again');
					}
				}
		});
		
	}
}

function findRet(flag,obj)
{
	if(flag==1){
		var distId=obj.options[obj.selectedIndex].value;
		var url="/panels/retColl/"+distId;
	}
	else {
		var salesManId=obj.options[obj.selectedIndex].value;
		var distId=$('distributor').options[$('distributor').selectedIndex].value;
		var url="/panels/retColl/"+distId+"/"+salesManId;
	}
	window.location.href = url;
}
function delRetailer(typ , rid , flag){
        var toShow , block , msg;
        if(flag == 'delete'){
                toShow = 0 ;
                block = 1;                
                
        }else if(flag == 'revert'){
                toShow = 1;
                block = 0;
        }else{
            return false;
        }
       
	if(confirm("Do you want to "+flag+" these retailer ?")){
          
            var url = '/shops/deleteRetailer';
            var params = {'id' : rid,'type':typ , 'toShow' : toShow , 'block' : block };
            var myAjax = new Ajax.Request(url, {method: 'post',type: 'JSON', parameters: params,
                    onSuccess:function(transport)
                            {
                                    if(transport.status == 200){
                                        $("ret_"+rid).hide();
                                    }
                            }
            });
            
        }
}

function goToPage($page){
	$('page').value = $page;

	$('form').submit();
}
</script>


<form id="form" name="retailerCollection" method="POST" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
<input type="hidden" id="page" name="page" value="<?php if(isset($page)) echo $page ?>" >
Distributor : <select name="distributor" id="distributor" onChange="findRet(1,this)">
	<?php
		foreach($distList as $d)
					{										
						$sel = '';
						if($distId == $d['distributors']['id'])
						{
							$sel = 'selected';							
						}
				 		echo "<option ".$sel." value='".$d['distributors']['id']."' >".$d['distributors']['company']."-".$d['users']['mobile']."</option>";
					}
	?>
					</select>
					
Maintenance Salesman : <select name="maint_salesman_retailer" id="maint_salesman_retailer" onChange="findRet(2,this)">
	<?php
		echo "<option value='0'>None</option>";
		foreach($salesmenList as $d)
					{
						$sel = '';
						if($sid == $d['salesmen']['id'])
						{
							$sel = 'selected';								
						} 
				 		echo "<option ".$sel." value='".$d['salesmen']['id']."' >".$d['salesmen']['name']."-".$d['salesmen']['mobile']."-".$d['salesmen']['id']."</option>";
					}
	?>
					</select>
<input type="text" placeholder="Search by retailer mobile or shop name" name="search_term" id="search_term" value="<?php echo $search_term ?>">
<input type="submit" onclick="goToPage(1)" value="Submit"/> 
<!-- Acquisition Salesman : <select name="acq_salesman_retailer" id="acq_salesman_retailer" onChange="findRet('2',this)">
	<?php
		/*echo "<option value='0'>None</option>";
		foreach($salesmenList as $d)
		{
			$sel = '';
			if($sid == $d['salesmen']['id'] && $flag == 2)
			$sel = 'selected';			
	 		echo "<option ".$sel." value='".$d['salesmen']['id']."' >".$d['salesmen']['name']."-".$d['salesmen']['mobile']."-".$d['salesmen']['id']."</option>";
		}*/
	?>	
						</select> -->
</form>

<br/><br/>
<table border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td>Index</td><td>Retailer Mobile</td><td>Shop Name</td><td>Days old</td><td>Salesman</td><td>Balance</td><td>Overall</td><td>Deleted</td><td>Rental/Kit</td><td>Block/Unblock</td><td>Maintenance Salesmen</td><td>Shift</td>
	</tr>
	<?php
		$i = 1;
		$amtTran = 0;
		$amtBal = 0;
		$setupColl = 0;
		$avg1 = 0;
		$avg2 = 0;
        $color = '';
		
		foreach($amountTransferred as $at){
			$date1 = new DateTime(date('Y-m-d'));
			$date2 = new DateTime($at['0']['created']);
			$interval = $date1->diff($date2);
			$days = $interval->y . " yr, " . $interval->m." mths, ".$interval->d." days ";
			
            echo "<tr style='color:".$color."'><td>".$i."</td><td><a href='/panels/retInfo/".$at['ret']['mobile']."'>".$at['ret']['mobile']."</a>&nbsp;</td><td><a href='/panels/retInfo/".$at['ret']['mobile']."'>".$at['ret']['shopname']."</a>&nbsp;</td><td>".$days."&nbsp;</td><td>".$at['salesmen']['name']."&nbsp;</td>";
            
            echo "<td>".$at['ret']['balance']."&nbsp;</td>";
            echo "<td>".$at['0']['sm']."&nbsp;</td>";
			
                        if($at['ret']['toShow'] == 0)
                            echo "<td><a id=\"ret_".$at['ret']['id']."\" href=\"javascript:void(0);\" title=\"Revert deletion of this retailer \" onclick=\"delRetailer('r',".$at['ret']['id'].",'revert');\">Revert</a></td>" ;
                        else 
                            echo "<td>-</td>";
			echo "<td>";
			echo '<select name="rental_ret" id="rental_ret" onChange="shiftRental('.$at['ret']['id'].',this,'.$at['ret']['mobile'].')">';
			if($at['ret']['rental_flag'] == 0){
				echo '<option value="0" selected>Kit</option>';
				echo '<option value="1">Rental</option>';
			}
			else if($at['ret']['rental_flag'] == 1 || $at['ret']['rental_flag'] == 2){
				echo '<option value="0">Kit</option>';
				echo '<option value="1" selected>Rental</option>';
			}
			echo '</select>';
			echo "</td>";
			
			echo "<td>";
			
			echo '<select name="block_salesmanDD" id="block_salesmanDD" onChange="retEnable('.$at['ret']['id'].',this)">';
			if($at['ret']['block_flag'] == '0'){
				echo '<option value="0" selected>None</option>';
				echo '<option value="1">Partially Blocked</option>';
				echo '<option value="2">Fully Blocked</option>';
			}
			else if($at['ret']['block_flag'] == '1'){
				echo '<option value="0">None</option>';
				echo '<option value="1" selected>Partially Blocked</option>';
				echo '<option value="2">Fully Blocked</option>';
			}
			if($at['ret']['block_flag'] == '2'){
				echo '<option value="0">None</option>';
				echo '<option value="1">Partially Blocked</option>';
				echo '<option value="2" selected>Fully Blocked</option>';
			}
			
			echo '</select>';
			echo "</td>";
		
			echo "<td>";
				echo '<select name="maintenance_salesmanDD" id="maintenance_salesmanDD" onChange="saveMaintenanceSm('.$at['ret']['id'].',this)">';
					echo '<option value="0">None</option>';
					foreach($salesmenList as $d)
					{		
								$sel = '';
								if($at['ret']['maint_salesman'] == $d['salesmen']['id'])
								$sel = 'selected';
								
						 		echo "<option ".$sel." value='".$d['salesmen']['id']."' >".$d['salesmen']['name']." (".$d['salesmen']['mobile'].")</option>";
						 		
					}
												
					
			echo '</select>';
			echo "</td>";
			
			echo "<td>";
				echo '<select name="distDD" id="distDD" onChange="changeDistributor('.$at['ret']['id'].',this)">';
					foreach($distList as $d)
					{		
								$sel = '';
								if($at['ret']['parent_id'] == $d['distributors']['id'])
								$sel = 'selected';
								
						 		echo "<option ".$sel." value='".$d['distributors']['id']."' >".$d['distributors']['company']."</option>";
						 		
					}
												
					
			echo '</select>';
			echo "</td>";
		
			echo "</tr>";
			
			$amtTran = $amtTran + 	$at['0']['sm'];
            if(isset($at['ret']['balance']))
			$amtBal = $amtBal + $at['ret']['balance'];
			//$amtColl = $amtColl + 	$amountCollected[$at['st']['ref2_id']];
          // if(isset($setupCollected[$at['st']['ref2_id']]))
			//$setupColl = $setupColl + 	$setupCollected[$at['st']['ref2_id']];
           // if(isset($amountCollected[$at['st']['ref2_id']]['average']))
			//$avg1 = $avg1 + $amountCollected[$at['st']['ref2_id']]['average'];
           // if(isset($amountCollected[$at['st']['ref2_id']]['average1']))
			//$avg2 = $avg2 + $amountCollected[$at['st']['ref2_id']]['average1'];			
			$i++;
		}
		echo "<tr><td colspan='5' align='right'><b>Total</b>&nbsp;</td><td colspan='2'><b>".$amtBal."</b></td></tr>";
	?>
</table>

<?php echo $this->element('pagination');?>

<style>
.pagination {
	list-style:none;
	display: inline-block;
	padding-left: 0px;
	margin: 20px 0px;
	border-radius: 4px;
}
.pagination li {
	border: 1px solid #DDD;
	padding: 10px;
}
.pagination .active {
	background-color: #428BCA;
}
.pagination .active a {
	z-index: 2;
	color: #FFF;
	background-color: #428BCA;
	border-color: #428BCA;
	cursor: default;
}
.pagination .disabled a {
	pointer-events: none;
    color: #999;
	background-color: #FFF;
	border-color: #DDD;
	cursor: not-allowed;
}
</style>