 		
<script>
function setAction(){
        var sel=$('vendorDD');
        var vendorDD=sel.options[sel.selectedIndex].value;
        
	document.tranReversal.action="/panels/tranReversal/"+$('from').value+"/"+$('to').value+"/"+vendorDD;
	document.tranReversal.submit();
}


</script>

<form name="tranReversal" method="POST" onSubmit="setAction()">
From Date <input type="text" name="from" id="from"  onmouseover="fnInitCalendar(this, 'from','close=true')" value="<?php if(!is_null($frm))echo $frm;?>" />
To Date: <input type="text" name="to" id="to" onmouseover="fnInitCalendar(this, 'to','close=true')" value="<?php if(isset($to))echo $to;?>" />

Vendors: 

<select name="vendorDD" id="vendorDD"  >
	<?php
			
			echo "<option value='0' >All</option>";
			
			foreach($vendorDDResult as $tr)
			{
				$sel='';
				if($vendor==$tr['vendors']['id'])
					$sel='selected';
			
				echo "<option ".$sel." value='".$tr['vendors']['id']."' >".$tr['vendors']['company']."</option>";
			}
			
	?>
	
</select>
<input type="checkbox" name ="b2c_flag" <?php if($b2c_flag=="true"){ ?> checked="checked"<?php } ?> value="true">B2C
<input type="button" value="Submit" onclick="setAction()">
</form>
</br>



<table width="100%" border="0">
<tr>
<td valign="top" >
	<h3>Complaints in Process</h3>
	<h3><a target="_blank" href="/panels/closedComplaints/<?php echo $frm."/".$to."/".$vendor."/".$b2c_flag ?>">Closed complaints</a>
	 (<?php echo $closed_count ?>)</h3>
	<table border="1" cellpadding="0" cellspacing="0" style="text-align:center">
					<tr> 
						<th>Index</th>
						<th>Tran Id</th>
						<!--	<th>Retailer Name/ShopName</th> -->
                                                <th>VTransID</th>
						<th>Vendor</th>
						<!--	<th>Retailer Mobile</th> -->
	  					<th>Cust Mob</th>
	  					<th>Operator</th>
	  					<th>Amt</th>
                                                <th>Status</th>
                                                <th>Complaint Date</th> 
	  					<th>Trans Date</th>
	  					<th>Difference</th>
	  					<th>Time Left</th>
                                                <th>Complaint Tag</th>
	  				</tr>
	  		
	  		<?php 
	  		$i=1;

	  		foreach($success as $d){
	  		if(strcmp($d['r']['name'],'')!=0){
	  		$retailerLink=$d['r']['name'];
	  		}
	  		else{
	  		$retailerLink=$d['r']['mobile'];
	  		}
	  		
	  		if($d['r']['id'] == 13)$color = '#DBEB23';
			else if(in_array($d['r']['id'],$retailerData))$color = '#E6BE8A';
	  		else if(!empty($d['complaints']['takenby']))$color = '#99ff99';
	  		else $color = '';
	  		
	  		echo "<tr bgcolor='$color'>";
	  		echo "<td>".$i."</td>";
	  		echo "<td><a href='/panels/transaction/".$d['va']['ref_code']."' >".$d['va']['ref_code']."</a></td>";
	  		//echo "<td><a href='/panels/retInfo/".$d['r']['mobile']."' >".$retailerLink."</br>".$d['r']['shopname']."</td>";
                        echo "<td>".$d['va']['vendor_refid']."</td>";
	  		// echo "<td><a href='/panels/retInfo/".$d['r']['mobile']."' >".$d['r']['name']."</a></td>";
	  		echo "<td>".$d['v']['shortForm']."</td>";
	  		echo "<td><a href='/panels/userInfo/".$d['va']['mobile']."' >".$d['va']['mobile']."</a></td>";
	  		echo "<td>".$d['p']['name']."</td>";
	  		echo "<td>".$d['va']['amount']."</td>";
	  		
	  		$ps = '';
	  		if($d['va']['status'] == '0'){
				$ps = 'In Process';
			}else if($d['va']['status'] == '1'){
				$ps = 'Successful';
			}else if($d['va']['status'] == '2'){
				$ps = 'Failed';
			}else if($d['va']['status'] == '3'){
				$ps = 'Reversed';
			}else if($d['va']['status'] == '4'){
				$ps = 'Complaint taken';
			}else if($d['va']['status'] == '5'){
				$ps = 'Complaint declined';
			}
	  		echo "<td>".$ps."</td>";
	  		echo "<td>".$d['complaints']['in_date']." ".$d['complaints']['in_time']."</td>";   		
	  		echo "<td>".$d['va']['timestamp']."</td>";
                        $diff = strtotime($d['complaints']['in_date']." ".$d['complaints']['in_time'])-strtotime($d['va']['timestamp']);
	  		echo "<td>".floor($diff/3600)." hrs, ".floor(($diff/60)%60)." mins, ".($diff%60)." secs"."</td>";
	  		/*if($d['va']['status']=='0' || $d['va']['status']== '4'){
	  			echo "<td><a href=''>Accept</a></br></br><a href=''>Decline</a></td>";
	  		}else{
	  			echo "<td>FAILURE</td>";
	  		}*/
	  		if(strtotime($d['complaints']['turnaround_time']) > 0){
	  			$secs = (strtotime($d['complaints']['turnaround_time']) - time());
	  			$mins = $secs / 60;
	  			if($secs < 60){
	  				if($mins < 0){
	  					$hours = round(-$mins / 60);
	  					$mins = round(-$mins % 60);
	  					echo "<td style='color:red'>".$hours." Hrs ".$mins." mins delayed </td>";
	  				}
	  				else
	  					echo "<td style='color:orange'>".round($secs)." secs left </td>";
	  			}	
	  			else {
	  				$hours = intval($mins / 60);
	  				$mins = intval($mins % 60);
	  				echo "<td>".$hours." Hrs ".$mins." mins left </td>";
	  			}
	  		}	
	  		else 
// 	  			echo "<td><div>
// 							<select id='tat_hr_".$d['complaints']['id']."'>";
// 								for($i = 0; $i < 25; $i++){ 
// 								 	echo "<option value='";
// 								 	echo $i."'>";
// 								 	if($i < 10){ echo "0".$i; }else{ echo $i; } 
// 								 		echo "</option>";
// 								}
// 								echo "
// 								<option value='48'>48</option>
// 							</select> Hr 
// 							<select id='tat_min_".$d['complaints']['id']."'>
// 								<option value='0'>00</option>
// 								<option value='0.5'>30</option>
// 							</select> Min
// 	  						<button value='Set' onclick='setTAT(".$d['complaints']['id'].");'>Set</button>
// 							</div></td>";
				echo "<td></td>";
                                echo "<td>".$d['t']['name']."</td>";

	  		$i++;	
	  		echo "</tr>";
	  		}
	  		echo "Total complaints:".($i-1)."</br></br>";
	  		//echo "Sales result array :".$salesResultArray;
		 ?> 
		</table>
</td>

<td valign="top" >
	
	
</td>	

</tr>
</table>