<div class="appColRight2">
	<div style="margin-bottom:20px;">
		<div class="appTitle">Our top selling products</div>
		<div class="retailImage"><img src="/img/spacer.gif" class="img_2"></div>
		<div class="retailImage"><img src="/img/spacer.gif" class="img_1"></div>
		<div class="retailImage"><img src="/img/spacer.gif" class="img_8"></div>
	</div>
	<div class="appColLeftBox" style="margin-bottom:10px;">
		<div class="appTitle">Become a Business Partner</div>
		<div>To become a Business Partner click on the button 'Become a Partner'</div>
		
		<div class="field" id="sendButt" style="margin-top:10px;">
         	<a onclick="window.scrollTo(0,0);$('becomeRetailer').simulate('click');" href="javascript:void(0);"><img class="otherSprite oSPos31" src="/img/spacer.gif"></a>  
       	</div>       	
		<div>Or <strong>Call us on 09769597418</strong></div>
	</div>
</div>
<div class="appColLeft2">
	<div class="title6"><span class="fntSz19 strng">Business Partner - Benefits of becoming Distributor / Retailer </span></div>
	<div class="info">
		<span class="strng">About SMSTadka Retail Products</span>
		<p>SMSTadka has a wide range of products which can be offered as an "over the counter" retail product in the form of physical cards or E-Vouchers. SMSTadka products provide a mix of fun and information to its users and additional consistent source of revenue to its business partners across the nation. Become our business partners at various levels and be part of the success story.</p>
		<p>SMSTadka is seeking to appoint retailers, distributors and affiliates across India. Write us at <a href="mailto:business@smstadka.com">business@smstadka.com </a> about your interest and we will contact you back.</p>
		<span class="strng">Key Benefits to our Business Partner</span>
		<ul class="info" style="margin-left:20px; margin-bottom:20px;">
			<li> Add-on business for constant additional income</li>
			<li>Simple and hassle free business</li>
			<li>Customer satisfaction - More repeat business</li>
			<li>Additional footfall on your store</li>
	        <li>Attractive Commission and Perks</li>
	        <li>An easy to use online tool.</li>
			<p><b>For Retailers:</b><br><img src="/img/retailer2.gif"></p>
	        <p><b>For Distributors:</b><img src="/img/retailer1.gif"></p>    
		</ul>

	</div>
<div class="rowDividerIn">
		Contact us now to know more or <a onclick="window.scrollTo(0,0);$('becomeRetailer').simulate('click');" href="javascript:void(0);"><img class="otherSprite oSPos31" src="/img/spacer.gif" valign="absbottom"></a>
	</div>
	<br>
</div>
<div class="clearRight">&nbsp;</div>