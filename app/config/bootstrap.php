<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php
 *
 * This is an application wide file to load any function that is not used within a class
 * define. You can also use this to include or require any files in your application.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * App::build(array(
 *     'plugins' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'models' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'views' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'controllers' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'datasources' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'behaviors' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'components' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'helpers' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'vendors' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'shells' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */
//error_reporting(E_ALL ^ E_STRICT);
putenv('TZ=Asia/Calcutta');
define('encKey','PuTyOuRK3yHeReeswrwerwr');

define('SITE_NAME','http://www.ashops.com/');
//define('SERVER_BACKUP','http://www.ashops.com/');
define('SERVER_BACKUP','http://107.22.174.199/');
define('SERVER_MAIL','http://ec2-174-129-120-63.compute-1.amazonaws.com/');
define('SERVER_PROTECTED','http://54.235.195.140/');


define('MEMCACHE_IP','localhost');
//define('MEMCACHE_IP','54.235.195.140');
define('SMS_FLAG','0');
define('MAIL_FLAG','0');
define('TAX_MODEL','1');//1 for discount model & 0 for service tax model
/* CRon Passwords*/
define('CRON_USERNAME','admin');
define('CRON_PASSWORD','5m5cr0n');

define('KYC_AMOUNT',50000);
define('PAGE_LIMIT',15);

define('BITLY_USER','smstadka'); 
define('BITLY_KEY','R_1740de7d233b01239b119b0f3891d521');

/* Version Numbers */
define('STYLE_CSS_VERSION','998');
define('STYLE_CSS_IE_VERSION','998');
define('M_STYLE_CSS_VERSION','980');
define('RETAIL_STYLE_CSS_VERSION','990');
define('SCRIPT_APP_JS_VERSION','198'); //script & app
define('MERGE_JS_VERSION','980'); //prototype, effects, carousel, dpEncodeRequest
define('MERGE_1_JS_VERSION','980'); //scriptaculous, calendar, controls
define('NETWORK_JS_VERSION','1'); //networks

/* File Types */
define('JS_TYPE','js');
define('CSS_TYPE','css');

define('BASE_DIR','D:\workspace\myproject\shops\app\webroot\apps');
define('DISTRIBUTOR_APP_FILE_1','pay1.jar');
define('RETAILER_APP_FILE_1','pay1.apk');
define('RETAILER_APP_FILE_2','pay1.jad');

define('MEMBER','1');
define('ADMIN','2');
define('SUPER_DISTRIBUTOR','4');
define('DISTRIBUTOR','5');
define('RETAILER','6');
define('CUSTCARE','7');
define('RELATIONSHIP_MANAGER','8');
define('VENDOR','9');

//salesmen TRIAL & SETUP period
define('TRIAL',7);
define('SETUP',30);


//Our SDs & Ds
define('SDISTS','3');
define('DISTS','11111');//1,3,18,29,87
//define('RETS','19');

define('TIME_DURATION','60');

/* User Registration Types */
define('ONLINE_REG',1);
define('MISSCALL_REG',2);
define('RETAILER_REG',3);

/* Default Slabs */
define('SDIST_SLAB',1);
define('DIST_SLAB',2);
define('RET_SLAB',3);

//Receipt Type
define('RECEIPT_INVOICE','1');
define('RECEIPT_TOPUP','2');

define('NUM_PRODUCTS','15');


define('PAYT_MIN',35000);
define('PPI_MIN',35000);
define('CP_MIN',35000);
define('OSS_MIN',15000);

//Retailer Transaction Types
define('ADMIN_TRANSFER','0');//ref1 is admin_id & ref2 is super distributor id
define('SDIST_DIST_BALANCE_TRANSFER','1');//ref1 is superdistributor id and ref2 is distributor id
define('DIST_RETL_BALANCE_TRANSFER','2');//ref1 is distributor id and ref2 is retailer id
define('DISTRIBUTOR_ACTIVATION','3');//ref1 is distributor id and ref2 is retailersCouponids seperated by commas
define('RETAILER_ACTIVATION','4');//ref1 is retailer id and ref2 is product id
define('COMMISSION_SUPERDISTRIBUTOR','5');//ref1 is superdistributor id and ref2 is parent id
define('COMMISSION_DISTRIBUTOR','6');//ref1 is distributor id and ref2 is parent id
define('COMMISSION_RETAILER','7');//ref1 is retailer id and ref2 is parent id
define('TDS_SUPERDISTRIBUTOR','8');	//ref1 is superdistributor id and ref2 is parent id
define('TDS_DISTRIBUTOR','9');//ref1 is distributor id and ref2 is parent id
define('TDS_RETAILER','10');//ref1 is retailer id and ref2 is parent id
define('REVERSAL_RETAILER','11');//ref1 is retailer id and ref2 is parent id
define('REVERSAL_DISTRIBUTOR','12');//ref1 is distributor id and ref2 is parent id
define('REVERSAL_SUPERDISTRIBUTOR','13');//ref1 is superdistributor id and ref2 is parent id
//define('REVERSED','14');//ref1 is distributor id and ref2 is parent id
define('DEBIT_NOTE','16');//ref1 is distributor id and ref2 is parent id
define('CREDIT_NOTE','17');//ref1 is distributor id and ref2 is parent id
define('SETUP_FEE','18');//ref1 is dist id and ref2 is ret id
define('REFUND','19');//ref1 is retid/distid/superdistid and ref2 is groupid
define('RENTAL','20');//ref1 is retid/distid/superdistid and ref2 is groupid
define('PULLBACK_RETAILER','21');//ref1 is retid and ref2 is parent_id and user_id is user_id of distributor
define('PULLBACK_DISTRIBUTOR','22');//ref1 is distributor id and ref2 is parent_id and user_id is user_id of super distributor
define('PULLBACK_SUPERDISTRIBUTOR','23');//ref1 is superdistributor id and ref2 is parent_id and user_id is user_id of user/admin
define('SERVICE_CHARGE','24');//ref1 is retid/distid/superdistid and ref2 is parent_id and user_id is group_id


//Calculation Constants
define('TDS_PERCENT','10');
define('SERVICE_TAX_PERCENT','10.3');

define('SMSTADKA_COMPANY','Mindsarray Technologies Pvt. Ltd.');
define('SMSTADKA_ADDRESS','A - 108, Kaveri, 1st Floor,
Chincholi Bunder Rd, Malad (W),
Mumbai - 400064, Maharashtra.');
define('SMSTADKA_CONTACT','+919769597418');

define('TRANS_SUCCESS',1);
define('TRANS_FAIL',2);
define('TRANS_REVERSE',3);
define('TRANS_REVERSE_PENDING',4);
define('TRANS_REVERSE_DECLINE',5);


//payment modes
define('MODE_CASH',1);
define('MODE_CHEQUE',2);
define('MODE_NEFT',3);
define('MODE_DD',4);
define('MODE_PG',5);

//payment types
define('TYPE_SETUP',1);
define('TYPE_TOPUP',2);

define('default_passwd','7de444954a802ec11b4cbb3bda06b85f8f1e0683'); //asdf1234

//oss test Credentials
/*define('OSS_DIST_CUST_CODE','MR00000156');
define('OSS_TRAN_DIST_CUST_CODE','BP00000021');
define('OSS_TRAN_PASSWORD','bp21@oss');
define('OSS_PG_CODE','OSSPG');
define('OSS_INTERFACE_CODE','P9EJGDH3BF');
define('OSS_INTERFACE_AUTH_KEY','MRMindsArray');
define('OSS_SOAP_URL',"http://demo.mobileseva.in/mrservice.asmx");
*/

//oss live Credentials
define('OSS_DIST_CUST_CODE','MR00117675');
define('OSS_TRAN_DIST_CUST_CODE','BP01093697');
define('OSS_TRAN_PASSWORD','a12active#');
define('OSS_PG_CODE','OSSPG');
define('OSS_INTERFACE_CODE','IDVYRVA4PZ');
define('OSS_INTERFACE_AUTH_KEY','MRMindsArray');
define('OSS_SOAP_URL',"http://www.mobileseva.in/MRservice.asmx");

//ppi test Credentials
/*define('PPI_USER_CODE','6BB77DE7');
define('PPI_PASSWORD','123456');
define('PPI_AUTH_CODE','mind001');
define('PPI_SOAP_URL',"http://203.115.69.146/PayPointAPI/ERechargeAPI.asmx");
*/

//ppi live Credentials
define('PPI_USER_CODE','95743989');
define('PPI_PASSWORD','123456');
define('PPI_AUTH_CODE','mind001');
define('PPI_SOAP_URL',"http://api.paypointindia.co.in/ERechargeAPI.asmx");

//paytronics live Credentials
define('PAYT_USER_CODE','1250612001');
define('PAYT_PASSWORD','P@YPASSP@YON1');
define('PAYT_URL','http://107.23.19.91:8080');

//smstadka credentials
define('SMSTADKA_USER','Active');
define('SMSTADKA_PASSWORD','1768lkaj');

//chat credentials
define('CHAT_SOAP_URL',"http://221.135.137.133/VoiceServices/UserDetails.asmx");

/* database connection */
define('DB_HOST','localhost'); 
define('DB_USER','root');
define('DB_PASS','');
define('DB_DB','shops'); 

//retailer type
define('SETUP_FEE_AMT',500);

/* App types*/
define('APP_JAVA',1); 
define('APP_ANDROID',2);
define('APP_SMS',3);
define('APP_USSD',4); 

/* B2C Parameters */
define('B2C_RETAILER',13); //retailer id of b2c
define('WALLET_ID',44);
define('WALLET_VENDOR',22);

/* Inventory Recepients for block SMS message */
Configure::write('blocksmsrecepients',array('8082011232'));

/**
 * As of 1.3, additional rules for the inflector are added below
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */
