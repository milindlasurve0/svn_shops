<?php 


		$config['acl']['modules'] = array(
	"Retailer_Retention" => array(
		"list" => array(
			"0" => array(
				"controller" => "alerts",
				"action" => array(
					"0" => array(
						'index',
						'downloadDumpData',
						'retrieveCommentDetails'
					) ,
					"1" => array(
						'insertCommentData'
					)
				)
			)
		) ,
		"url" => array(
			"alerts/index"
		)
	) ,

	"C2D" => array(
		"list" => array(
			"0" => array(
				"controller" => "cashpayment",
				"action" => array(
					"0" => array(
						'index',
						'loadTransactionData',
						'loadTransactionData',
						'loadSettlementData'
					) ,
					"1" => array(
						'insertSettlementData',
						'create_cashpayment_client'
					)
				)
			)
		) ,
		"url" => array(
			"cashpayment/index"
		)
	) ,
"Calls_Dropped" => array(
		"list" => array(
			"0" => array(
				"controller" => "cc",
				"action" => array(
					"0" => array(
						'panel',
					) ,
					"1" => array(
						'callDone',
						'callNotPicked'
					)
				)
			)
		) ,
		"url" => array(
			"cc/panel"
		)
	) ,
	"Distributor_Calls" => array(
		"list" => array(
			"0" => array(
				"controller" => "cc",
				"action" => array(
					"0" => array(
						'panel',
					) ,
					"1" => array(
						'callDone'
					)
				)
			)
		) ,
		"url" => array(
			"cc/panel/Distributor"
		)
	) ,
"Chat_Report" => array(
		"list" => array(
			"0" => array(
				"controller" => "chats",
				"action" => array(
					"0" => array(
						'generateReport',
						'report',
						'conversation'
					) ,
					"1" => array(
						)
				)
			)
		) ,
		"url" => array(
			"chats/report"
		)
	) ,
	"Plans" => array(
		"list" => array(
			"0" => array(
				"controller" => "circles",
				"action" => array(
					"0" => array(
						'index',
						'searchCircles',
						'searchPlans'
					) ,
					"1" => array(
						'deletePlan',
						'newPlanEntry',
						'editPlanForm',
						'editPlanEntry'
						)
				)
			)
		) ,
		"url" => array(
			"circles/index"
		)
	) ,
	"Monitor" => array(
		"list" => array(
			"0" => array(
				"controller" => "monitor",
				"action" => array(
					"0" => array(
						'index',
						'smsIncomingMonitoring',
						'smsOutgoingMonitoring',
						'USSDMonitoring'
					) ,
					"1" => array(
						'switch_ussd',
						'setType',
						'setQueue',
						'unsetQueue'
						)
				)
			)
		) ,
		"url" => array(
			"monitor/smsIncomingMonitoring"
		)
	) ,
	"Distributor_Module" => array(
		"list" => array(
			"0" => array(
				"controller" => "salesmen",
				"action" => array(
					"0" => array(
					) ,
					"1" => array(
						'blockSalesman',
                          'mapSalesman',
							'blockRetailer'
					)
				)
			),
			"1" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'autoCompleteSubarea',
						'backDistEdit',
						'allRetailer',
						'retFilter',
						'deletedRetailer',
						'salesmanListing',
						'editSalesman',
						'editRetailer',
						'showDetails',
						'formRetailer',
						'formSalesman',
						'formSetUpFee',
						'backRetailer',
						'backSalesman',
						'backSetup',
						'salesmanTran',
						'topup',
						'calculateCommission',
						'transfer',
						'backTransfer',
						'mainReport',
						'overallReport',
						'distTopUpRequest',
						'allRetailerTrans',
						'lastTransferred',
						'graphMainReport'
						

					) ,
					"1" => array(
						'deleteSubarea',
						'saveEditSm',
						'deleteRetailer',
						'editRetValidation',
						'addSetUpFee',
						'createSalesman',
						'addSalesmanCollection'
					)
				)
			),
		    "2" => array("controller" => "panels",
                         "action" => array(
								"0" => array(
								) ,
								"1" => array(
									'addNewNumber'
												
								)
				             )
			
		                    ) ,
		"url" => array(
			""
		)
	)) ,
	"SD_Module" => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'formDistributor',
						'backDistributor',
						'backDistEdit',
						'allDistributor',
						'allRetailer',
						'retFilter',
'deletedRetailer',
'editRetailer',
'showDetails',
	'formRm',
	'topup',
	'topupDist',
	'calculateCommission',
	'transfer',
	'backTransfer',
	'kitsTransfer',
	'backKitTransfer',
	'mainReport',
	'overallReport',
	'sReport',
	'lastTransferred',
	'graphMainReport',
	'distributorsMonthReport'
					) ,
					"1" => array(
						'autoCompleteSubarea',
						'createDistributor',
							'formDistributor',
'deleteRetailer',
'editDistValidation',
'createRm',
'transferKits'
					)
				)
			)
			
		) ,
		"url" => array(
			""
		)
	) ,
	"RM_Module" => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'allDistributor',
						'showDetails',
						'topupDist',
						'mainReport',
						'overallReport',
						'sReport',
						'graphMainReport',
						'allRetailer',
						'createRM'
							
					) ,
					"1" => array(
					)
				)
			)
			
		) ,
		"url" => array(
			""
		)
	) ,
"Admin_Module" => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'allDistributor',
						'topupDist',
						'transfer',
						'backTransfer',
						'mainReport',
						'overallReport',
						'sReport',
						'lastTransferred',
						'recheckTrans',
						'incentivePullback',
						'graphMainReport'
						
					) ,
					"1" => array(
						'pullBackApproval',
						'pullbackRefund'
					)
				)
			),
                        "1" => array(
				"controller" => "users",
				"action" => array(
					"0" => array(
                                            
					) ,
					"1" => array(
                                            'add',
                                            'edit',
                                            'delete'
					)
				)
			)
			
		) ,
		"url" => array(
			""
		)
	) ,
	'Limit_Tfr_File' => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'limitTransfer'						
					) ,
					"1" => array(
						
					)
				)
			)
			
		) ,
		"url" => array(
			"shops/limitTransfer"
		)
	),
	"Sim_Panel" => array(
		"list" => array(
			"0" => array(
				"controller" => "sims",
				"action" => array(
					"0" => array(
						'getOperatorsViewJSON',
						'index',
						'getModemsimsDetails',
						'getOperatorWiseSuccessFailureReports',
						'getyellowsimsbymodemid',
						'getHighlights',
						'lastModemTransactions',
						'lastModemSMSes',
						'allBalance',
						'checkSimStatus',
                                                                                                                                                'checkBalance',
                                                                                                                                                'removeSim',
                                                                                                                                                'rechargeType',
                                                                                                                                                'addBlockSimsData',
                                                                                                                                                'checkBlocksimStatus',
                                                                                                                                                'resetSimStatus',
                                                                                                                                                'addNewBlockSimsData'
					) ,
					"1" => array(
						'updateSimData',
						'checkPassword',
						'shiftSims',
						'updateIncomingManually',
						'adjustPendings',
						'storeIncomingRequestinRedis',
						'updateClosing',
						'updateBalance',
						'storeBalanceRequestinRedis',
						'storeClosingRequestinRedis',
						'resetModemDevice',
						'stopModemDevice',
						'checkNegDiff',
						'sendBlockSms'
						
					)
				)
			),
				"1" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array('modemRequest'
						
					) ,
					"1" => array(
						'modemRequest',
						'shiftbalance'
					)
				)
			)
		) ,
		"url" => array(
			"sims/index"
		)
	) ,
	"Search_Module" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'search',
						'showComments',
						'userInfo',
						'retInfo',
						'ussdLogs',
						'appNotificationLogs',
						'transaction',
						'openTransaction',
						'tranDate',
						'showTransaction'
					) ,
					"1" => array(
						'createTag',
						'manualRequest',
						'updateCommentsForReversalNew',
						'reverseTransaction',
						'reversalDeclined',
						'regReversal',
						'editRetailer',
						'addComment',
						'pullback',
                                                'addNewNumber',
                                                'updateCallComplain'
					)
				)
			),
			"1" => array(
				"controller" => "cc",
				"action" => array(
					"0" => array(
						'checkPendingCalls'
					) ,
					"1" => array(
						)
				)
			)

		) ,
		"url" => array(
			"panels/search"
		)
	) ,
	"inprocess_txns" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'inProcessTransactions'
					) ,
					"1" => array(
						'manualSuccess',
						'manualFailure',
						'reverseTransaction'
					)
				)
			)
		) ,
		"url" => array(
			"panels/inProcessTransactions"
		)
	) ,
	"notifications" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'retMsg'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/retMsg"
		)
	) ,
	"allRetailers" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'retColl'
					) ,
					"1" => array(
						'changeDistributor'
					)
				)
			),
			"1" => array(
				"controller" => "salesmen",
				"action" => array(
					"0" => array(
					) ,
					"1" => array(
						'blockRetailer',
						'mapSalesman',
						'rentalRetailer'
						)
				)
			),
			"2" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
					) ,
					"1" => array(
						'deleteRetailer'
					)
				)
			)
		) ,
		"url" => array(
			"panels/retColl"
		)
	),
	"sale_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'retailerSale'
					) ,
					"1" => array(
						
					)
				)
			)
		) ,
		"url" => array(
			"panels/retailerSale"
		)
	), 
	"complaints" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'tranReversal',
						'closedComplaints'
					) ,
					"1" => array(
						
					)
				)
			)
		) ,
		"url" => array(
			"panels/tranReversal"
		)
	), 
	"provider_switching" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'prodVendor',
					) ,
					"1" => array(
                                            'disableVendor',
                                            'blockSlab',
                                            'refreshCache',
                                            'deactivateVendor',
                                            'oprEnable',
                                            'show',
                                            'hide'
					)
				)
			)
		) ,
		"url" => array(
			"panels/prodVendor"
		)
	), 
	"from_to_txn" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'tranRange'
					) ,
					"1" => array(	
					)
				)
			)
		) ,
		"url" => array(
			"panels/tranRange"
		)
	), 
"modem_reconcilation" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'reconsile'
					) ,
					"1" => array(	
						'update_reconsile'
					)
				)
			)
		) ,
		"url" => array(
			"panels/reconsile"
		)
	),  
"pullback_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'pullbackReport',	
					) ,
					"1" => array(	
						'pullback'
					)
				)
			)
		) ,
		"url" => array(
			"panels/pullbackReport"
		)
	), 
"manualReversalReport_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'manualReversalReport'
					) ,
					"1" => array(	

					)
				)
			)
		) ,
		"url" => array(
			"panels/manualReversalReport"
		)
	), 
"complain_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'complainReport',
						'reOpenDetails',
						'exceedComplainDetails'
					) ,
					"1" => array(	
						
					)
				)
			)
		) ,
		"url" => array(
			"panels/complainReport"
		)
	), 
"salesman_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'salesmanReport'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/salesmanReport"
		)
	), 
"retatiler_kyc" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'retailers',
						'verifySection',
						'rejectSection',
					) ,
					"1" => array(
						'setVerifyFlag',
						'toggleTrained',
						'retailerVerification',
						'verifyDocuments',
						'rejectDocument',
						'deleteDocument',
						'activateMPOS',
						'updateDSN'
					)
				)
			)
		) ,
		"url" => array(
			"panels/retailers"
		)
	), 
"inproccess_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'inprocessReport',
                                                'inProcessTransactionList'
					) ,
					"1" => array(

					)
				)
			)
		) ,
		"url" => array(
			"panels/inprocessReport"
		)
	), 
				"inproccess_report_mongo" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'inprocessReportMongo'
					) ,
					"1" => array(

					)
				)
			)
		) ,
		"url" => array(
			"panels/inprocessReportMongo"
		)
	),
"online_lead" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'leads'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/leads"
		)
	), 
"vendor_product_mapping" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'vendorsCommissions'
					) ,
					"1" => array(
						'saveVendorCommission'
					)
				)
			)
		) ,
		"url" => array(
			"panels/vendorsCommissions"
		)
	), 
"api_recon" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'apiRecon'
					) ,
					"1" => array(
						'saveVendorCommission',
						'check_current_api_txn_status'
					)
				)
			)
		) ,
		"url" => array(
			"panels/apiRecon"
		)
	), 
"cc_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'ccReport',
						'tagReport',
						'removeTag'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/ccReport"
		)
	),  
"retailerRegistrationReport" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'retailerRegistrationReport',
						'graphReport',
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/retailerRegistrationReport"
		)
	), 
"process_time_and_failer_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						'getProcessTime',
						'failureInfo',
						'graphReport',
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			"panels/getProcessTime"
		)
	), 
        "new_lead" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						
					) ,
					"1" => array(
						'newLead'
					)
				)
			)
		) ,
		"url" => array(
			"panels/newLead"
		)
	), 
        "txn_diff_report" => array(
		"list" => array(
			"0" => array(
				"controller" => "panels",
				"action" => array(
					"0" => array(
						
					) ,
					"1" => array(
						'tranDiffReport'
					)
				)
			)
		) ,
		"url" => array(
			"panels/tranDiffReport"
		)
	), 
	'investment_Report' => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'investmentReport'
					) ,
					"1" => array(
						'addInvestmentEntry',
						'addInvestedAmount'
					)
				)
			)
		) ,
		"url" => array(
			"shops/investmentReport"
		)
	),
'float_Report' => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'floatReport'
					) ,
					"1" => array(
								)
				)
			)
		) ,
		"url" => array(
			'shops/floatReport'
		)
) ,
'earning_Report' => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'earningReport'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			'shops/earningReport'
		)
) ,
'float_graph' => array(
		"list" => array(
			"0" => array(
				"controller" => "shops",
				"action" => array(
					"0" => array(
						'floatGraph'
					) ,
					"1" => array(
					)
				)
			)
		) ,
		"url" => array(
			'shops/floatGraph'
		)
),
		'acl_module' => array(
		"list" => array(
			"0" => array(
				"controller" => "acl",
				"action" => array(
					"0" => array(
						'listUser',
						'addUser',
						'listGroup',
                                                                                                                'add',
                                                                                                                'edit'
                                                                                                         
					) ,
					"1" => array('module','addGroup')
				)
			)
		) ,
		"url" => array(
			'acl/module'
		)
),
     'list_distributors' => array("list" => array(
						"0" => array(
								"controller" => "shops",
								"action" => array("0" => array("allDistributor"))
						         )
				              ),"url" => array(
								'shops/allDistributor'
							)),
				
		'incentive_retailer' => array("list" => array(
						"0" => array(
								"controller" => "shops",
								"action" => array("0" => array("refundRetailer"))
						         )
				              ),"url" => array(
								'shops/refundRetailer'
							)),
					'incentive_distributor' => array("list" => array(
						"0" => array(
								"controller" => "shops",
								"action" => array("0" => array("incentiveDistributor"))
						         )
				              ),"url" => array(
								'shops/incentiveDistributor'
							)),
				'changeDistibutor_MobileNo' => array("list" => array(
						"0" => array(
								"controller" => "shops",
								"action" => array("0" => array("changeDistributorMobileNo"))
						         )
				              ),"url" => array(
								'shops/changeDistributorMobileNo'
							)),
                    
                    "Wholesaler registration"=>array(
                                                     "list"=>array(
                                                                    "0"=>array(
                                                                     "controller"=>"wsregister",
                                                                     "action"=>array(
                                                                      "0"=>array(
                                                                                 "index",
                                                                                 "checkUnique",
                                                                                  "listws"
                                                                                ),
                                                                       "1"=>array(
                                                                                  "registerWholesaler"
                                                                                  )) )
                                                                        ),"url"=>array(
                                                                                   'wsregister/index'
                                                                            )
                                                        ),
				
				 "Products"=>array(
                                "list"=>array(
                                            "0"=>array(
                                                "controller"=>"products",
                                                  "action"=>array(
                                                      "0"=>array(
                                                            "index",
															   "listing_apm",
																"listing_lvm",
															  "a_p_mapping",
															  "local_vendor_mapping",
                                                                                                                         
                                                      ),
                                                      "1"=>array(
                                                          "edit","editFormEntry","l_v_m_entry","a_p_m_entry"
                                                      )
                                                  )  
                                            )
                                ),
						       "url" => array("products/index")
                    ),                     
                     "Ivruploads"=>array(
                                "list"=>array(
                                            "0"=>array(
                                                "controller"=>"products",
                                                  "action"=>array(
                                                      "0"=>array(
                                                                 "uploadivr"
                                                      ),
                                                      "1"=>array(
                                                          
                                                      )
                                                  )  
                                            )
                                ),
						       "url" => array("products/uploadivr")
                                ),
                                "Heatmap" => array(
                                        "list" => array(
                                                "0" => array(
                                                        "controller" => "heatmap",
                                                                "action" => array(
                                                                    "0" => array(
                                                                            "index", "filterCityArea"
                                                                    ),
                                                                    "1" => array()
                                                        )  
                                                )
                                        ),
                                       "url" => array("heatmap/index")
                                ),
                    
                                "C2D Reports" => array(
                                        "list" => array(
                                                "0" => array(
                                                        "controller" => "c2d",
                                                                "action" => array(
                                                                    "0" => array(
                                                                            "clickToCallListing",
                                                                            "postInterestListing",
                                                                            "c2dPost",
                                                                            "viewComment"
                                                                    ),
                                                                    "1" => array(
                                                                            "addOrderTag",
                                                                            "addComment"
                                                                    )
                                                        )  
                                                )
                                        ),
                                       "url" => array("c2d/clickToCallListing")
                                ),
                    
                                "Marketing Notification" => array(
                                        "list" => array(
                                                "0" => array(
                                                        "controller" => "events",
                                                                "action" => array(
                                                                    "0" => array(
                                                                            "index",
                                                                            "callEvent",
                                                                            "serviceAlert"
                                                                    ),
                                                                    "1" => array(
                                                                            "addEvent",
                                                                            "uploadImage",
                                                                            "generateServiceAlert"
                                                                    )
                                                        )  
                                                )
                                        ),
                                       "url" => array("events/index")
                                )
				
				);
                                                        




				
				
				
				$config['acl']['bypass'] =  array(
											'alerts' => array(
												'salesDownDistributor',
												'salesDownRetailer',
												'salesDownState',
												'salesForecast',
												'retailersGraduallyDropped',
												'retailersDroppedOut',
												'distributorRetailersCount',
												'insertRetailersGradualDropData',
												'insertRetailersDroppedOutData',
												'insertDataPostCallDate',
												'helperInsertDataPostCallDate',
												'systemAlerts',
										        'retailerSales'
	                                       ) ,
											'apis' => array(
													'*'
												) ,
											'b2cextender' => array(
													'*'
												),
											'cc' => array(
											'retMisscall',
											'test',
											'checkPendingCalls'		
										),
						                'cashpayment' => array(
												'collection_req',
								                 'check_status',
												 'get_transaction_list',
												 'cancel_transaction',
												 'update_callback_api',
												 'get_client_detail_by_ref_id',
												 'transaction_detail_by_request_id',
												 'cashpayment_api_manager',
												 'get_pending_request_by_mobile',
																			
																	) ,
																	'crons' => array(
																		'*'
																	) ,
																	'distributors' => array(
																		'*'
																	) ,	
																	
													'invs' => array(
														'*'
													) ,
													'ivr' => array(
														'*'
													) ,
													'promotions' => array(
														'*'
													) ,
													'mpos' => array(
														'*'
													),
													'retailers'=> array(
														'*'
													),
									'salesmen'=> array(
										'updateRetailerLogs',
										'updateDistributorLogs',
										'correctOldEntries',
										'updateDistributorsLogsQuarter',
									) ,
								'ussdapis'=> array(
										'*'
									) ,
								'recharges'=> array(
										'*'
									) ,
								'users'=> array(
										'*'
									) ,
						'modemalerts'=> array(
								'*'
						) ,
									'panels'=> array(
										'view',
										'errorMsg',
										'request'
									) ,
									'chats'=> array(
										'generateReport'
									) ,
									'shops'=> array(
										'index',
								        'view',
								'createRetailer',
								'createRetailerApp',
								'changePassword',
								'accountHistory',
								'saleReport',
								'salesmanReport',
								'pullback',
								'amountTransfer',
								'logout',
								'initializeOpeningBalance',
								'graphRetailer',
								'bankDetails',
								'distributorsHelpDesk',
								'limitDepartmentDetails',
								'customerCare'
								
								),
						       'acl'=>array('setUserAccess','insertModule','insertExistingUser')
                                 );



		
