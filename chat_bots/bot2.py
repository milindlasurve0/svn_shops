# Author : Nandansingh Rana
# before running check where are python packages are available or not from below import list
# chat_response_parser is a supported file not a package to also check for packages included in this file
#

import socket
import chat_response_parser as cparser
import time
import re
import random
import redis

class xmpp_bot:
  def create_sock_connection(self,HOST,PORT):
    self.host = HOST
    self.port = PORT
    connection = socket.socket()
    connection.connect((HOST,PORT))
    self.con = connection
    ##print "connection establised %s "%(str(self.con))
    return

  def rconnection(self):
    global rclient
    try:
      rclient = redis.Redis(host='smstadka.com',port=6300,password='$avail$p@y!',db=6)
    except Exception, ex:
      print str(ex)
      rclient = False

  def rconnection(self):
    global rclient
    try:
      rclient = redis.Redis(host='smstadka.com',port=6300,password='$avail$p@y!',db=6)
    except Exception, ex:
      print str(ex)
      rclient = False

  def connect_to_xmppserver(self):
    req_template = """<?xml version='1.0'?>
                   <stream:stream 
                           xmlns:stream=\"http://etherx.jabber.org/streams\" 
                           to=\"%s\" 
                           xmlns=\"jabber:client\">""" % (self.host)
    ##print "sending connection xml : %s "%(req_template)
    self.con.send(req_template)
    ##print "\nrespose received : %s" %(str(self.con.recv(1024)))
    return

  def login_xmppserver(self,username,password):
    login_template = """<iq type='set' id='auth'>
                        <query xmlns='jabber:iq:auth'>
                          <username>%s</username>
                          <password>%s</password>
                          <resource>support</resource>
                        </query>
                      </iq>""" % (username,password)
    login_template += "<presence/>"
    ##print "sending login xml :  %s" % (login_template)
    self.con.send(login_template)
    ##print "\nrespose received : %s" %(str(self.con.recv(1024)))
    return

  def send_message(self,to,msg):
    message_template = """<message to=\"%s\" type=\"chat\">
                            <body>%s</body>
                        </message>""" % (to,msg)
    ##print "sending send message : %s "%(message_template)
    self.con.send(message_template)
    return

  def receive_msg(self):
    self.send_message("7738832731@192.168.0.38","welcome to payone")
    while(True):
      ##print self.con.recv(0)
      self.send_message("7738832731@192.168.0.38"," ----------- ")

  def receive_message(self):
    #self.send_message("7738832731@192.168.0.38","welcome to payone")
    self.agentList = []
    self.agent_global_id = 0
    try:
      while(True):
        responseXML = self.con.recv(2048)
        #print "------------"
        #print str(responseXML)
        #print "------------"
        self.response_manager(responseXML)
    except Exception,e:
        print "Exception in Receive message : %s"%(str(e))
        pass

  def response_manager(self, outputstream):
    try:
      if((re.search( r'<presence', outputstream, re.M|re.I) is not None)):
        self.presence_handler(outputstream)
      if((re.search( r'<message', outputstream, re.M|re.I) is not None)):
        self.message_handler(outputstream)
      if((re.search( r'<iq ', outputstream, re.M|re.I) is not None)):
        self.iq_handler(outputstream)
    except Exception, e:
      print "Exception in response manager : "+str(e)
      pass
    return

  def presence_handler(self,outputstream):
    try:
        outputstr = outputstream.replace("<presence","|<>|<presence")
        resXML_list = outputstr.split('|<>|')
        for tag in resXML_list:
          if (len(tag) > 1):
            parsedObj = cparser.RequestParser(tag)
            TagName = parsedObj.getTagName()
            Type = parsedObj.getType()
            SenderID = parsedObj.getSenderID()
            if ( Type == "") and SenderID not in ['adminsupport@dev.pay1.in/support']: self.agentList.append(SenderID)
            if ( Type == "unavailable") :
              if (SenderID in self.agentList): self.agentList.remove(SenderID)
    except Exception, e:
        print "Exception in presence handler : "+str(e)
        pass

  def message_handler(self,outputstream):
    try:
        self.get_current_agents()
        parsedObj1 = cparser.RequestParser(outputstream)
        #print self.agentList 
        if len(self.agentList) < 1:
          msgTosend = "Sorry, No agent is online"
        else:
          last_support_agent = rclient.hget(CUSERLIST,parsedObj1.getSenderID())
          if(last_support_agent is not None and last_support_agent  in self.agentList):
            msgTosend = "1|"+str(last_support_agent)
            self.agent_global_id = 0
          elif (self.agent_global_id == 0):
            agentid = len(self.agentList) - 1
            self.agent_global_id = agentid
            msgTosend = "1|"+str(self.agentList[agentid])
            rclient.hset(CUSERLIST,parsedObj1.getSenderID(),str(self.agentList[agentid]))
          elif (self.agent_global_id < len(self.agentList)):
            agentid = self.agent_global_id - 1
            self.agent_global_id = agentid
            msgTosend = "1|"+str(self.agentList[agentid])
            rclient.hset(CUSERLIST,parsedObj1.getSenderID(),str(self.agentList[agentid]))
        self.send_message(parsedObj1.getSenderID(),msgTosend)
    except Exception, e:
        print "Exception in message handler: "+str(e)
        pass

  def iq_handler(self,outputstream):
    try:
        parsedObj = cparser.RequestParser(outputstream)
        Type = parsedObj.getType()
        SenderID = parsedObj.getSenderID()
        msgID = parsedObj.getMsgID()
        if SenderID == self.host and Type == "get" :
           self.reply_server(msgID)
    except Exception, e:
        print "Exception in iq handler : "+str(e)
        pass

  def reply_server(self,reply_id):
    server_reply_tmpl2 = "<iq to='%s' id='%s' type='result'/>"%(str(self.host),str(reply_id))
    self.con.send(server_reply_tmpl2)

  def get_current_agents(self):
    self.con.send("<presence />")
    #print self.response_manager(self.con.recv(1024))

if ( __name__ == "__main__"):
  bot1 = xmpp_bot()
  global CUSERLIST
  CUSERLIST = "chatusers_list"
  bot1.rconnection()
  bot1.create_sock_connection("dev.pay1.in",5222)
  bot1.connect_to_xmppserver()
  bot1.login_xmppserver("adminsupport","@pay1cc@support")
  #bot1.login_xmppserver("umesh","123456")
  bot1.receive_message()
