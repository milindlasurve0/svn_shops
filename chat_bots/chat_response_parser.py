from xml.dom.minidom import parse
import xml.dom.minidom

class RequestParser:

  def __init__(self,responseString):
    try:
      if (responseString == ""):
        return False
      self.DOMTRee = xml.dom.minidom.parseString(responseString)
      self.collection = self.DOMTRee.documentElement
    except Exception,e:
      print "Exception in requestparser : "+str(e)+" --- "+str(reponseString)
    return

  def getTagName(self):
    return str(self.collection._get_tagName())

  def getSenderID(self):
    return str(self.collection.getAttribute("from"))

  def getMessage(self):
    return str(self.collection.getElementsByTagName("body")[0].childNodes[0].data)

  def getType(self):
    return str(self.collection.getAttribute("type"))

  def getMsgID(self):
    return str(self.collection.getAttribute("id"))
